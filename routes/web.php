<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;

/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get(
            '/investor',
            function () {
                return view('admin.investor.index', ['data' => array('key' => 'value')]);
            }
        );
    }
);

Auth::routes(['verify' => true]);
Route::get(
    '/',
    function () {
        if (auth()->check()) {
            if (auth()->user()->hasRole(['Emploee Staff', 'Emploee Invest']) && auth()->user()->approved) {
                return redirect(route('employee.documents'));
            }
            return redirect('/profile');
        }
        return redirect('/login');
    }
);
Route::get(
    '/admin',
    function () {
        if (auth()->check()) {
            $user = auth()->user()->roles;
            if (auth()->user()->hasRole('Employee Staff')) {
                if (auth()->user()->employee->user_status_id > 3) {
                    return redirect(route('admin/packages'));
                }
                return redirect(route('employee.documents'));
            }
            if (auth()->user()->hasRole('Employee Invest')) {
                if (auth()->user()->employee->user_status_id > 3) {
                    return redirect(route('admin/invest-tasks'));
                }
                return redirect(route('employee.documents'));
            }
            if (auth()->user()->hasRole('Administrator')) {
                return redirect('/admin/statistics');
            }
            if (auth()->user()->hasRole('HR Manager Invest')) {
                return redirect('/admin/roles/6/users');
            }
            if (auth()->user()->hasRole('HR Manager Staff')) {
                return redirect('/admin/roles/5/users');
            }

            return redirect('/profile');
        }
        return redirect('/login');
    }
);
Route::prefix('employee')->middleware('auth')->group(
    function () {
        Route::get('/documents', 'ProfileController@documents')->name('employee.documents');
        Route::post('/interview/phone', 'ProfileController@interviewByPhone')->name('employee.interview.phone');
        Route::get('/interview', 'ProfileController@interview')->name('employee.interview');
    }
);
Route::get('/projects/{project}/register', 'Auth\RegisterController@showRegistrationForm');
Route::post('/register/verification/send', 'Auth\RegisterController@sendVerificationCode')->name('verification.send');
Route::post('/register/phone/verify', 'Auth\RegisterController@verifyPhone')->name('verification.code');
Route::get('/chat/{chatroom?}', 'Admin\ChatController@index')->name('chat');
Route::get('/profile', 'ProfileController@index')->name('profile.index')->middleware('auth');
Route::post('/test/send_custom', 'Admin\MailClientControllerCustom@send')->middleware('auth');


Route::get('/users/{user}/forms/{form}', 'Admin\UsersController@getForm')->name('user.form')->middleware('auth');
Route::post('/users/{user}/forms/{form}', 'Admin\UsersController@saveForm')->name('user.form.save')->middleware('auth');

Route::post('/users/{user}/employee/interview', 'Admin\UsersController@saveInterview')->name(
    'user.employee.interview'
)->middleware('auth');
Route::post('/profile/media/{mediaCollection}', 'ProfileController@uploadMedia')->name(
    'profile.media.upload'
)->middleware('auth');
Route::get('/profile/media/{mediaCollection}/{media}/{user?}', 'ProfileController@downloadMedia')->name(
    'profile.media.download'
)->middleware('auth');
Route::get('/profile/media/{mediaCollection}', 'ProfileController@getMedia')->name('profile.media.index')->middleware(
    'auth'
);
Route::get('/tasks', 'Admin\TaskController@authUserTasks')->middleware('auth');
Route::get('/employees', 'EmployeeController@index')->middleware('auth');
Route::put('/employees/{employee}/sync', 'EmployeeController@syncRelations')->middleware('auth');

Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/notifications', 'Admin\NotificationController@index');
    }
);

Route::get('/users/{user}/contacts', 'ContactController@index')->middleware('auth');
Route::get('/chat/filters/options', 'Admin\ChatController@getFiltersOptions')->middleware('auth');

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/admin/admin-users', 'Admin\AdminUsersController@index');
        Route::get('/admin/admin-users/create', 'Admin\AdminUsersController@create');
        Route::post('/admin/admin-users', 'Admin\AdminUsersController@store');
        Route::get('/admin/admin-users/{adminUser}/edit', 'Admin\AdminUsersController@edit')->name(
            'admin/admin-users/edit'
        );
        Route::post('/admin/admin-users/{adminUser}', 'Admin\AdminUsersController@update')->name(
            'admin/admin-users/update'
        );
        Route::delete('/admin/admin-users/{adminUser}', 'Admin\AdminUsersController@destroy')->name(
            'admin/admin-users/destroy'
        );
        Route::get(
            '/admin/admin-users/{adminUser}/resend-activation',
            'Admin\AdminUsersController@resendActivationEmail'
        )->name('admin/admin-users/resendActivationEmail');
    }
);

/* Auto-generated profile routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/admin/profile', 'Admin\ProfileController@editProfile');
        Route::post('/admin/profile', 'Admin\ProfileController@updateProfile');
        Route::get('/admin/password', 'Admin\ProfileController@editPassword');
        Route::post('/admin/password', 'Admin\ProfileController@updatePassword');
    }
);

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::post('/admin/users/gotochat', 'Admin\UsersController@gotoChat');
        Route::get('/admin/users/needcalls', 'Admin\UsersController@getNeedCallsList');
        Route::get('/admin/users', 'Admin\UsersController@index')->name('admin.users.index');
        Route::get('/admin/users/create', 'Admin\UsersController@create');
        Route::post('/admin/users', 'Admin\UsersController@store');
        Route::post('/admin/users/{user}/approved', 'Admin\UsersController@updateApproved');
        Route::get('/admin/users/{user}/edit', 'Admin\UsersController@edit')->name('admin/users/edit');
        Route::get('/admin/users/{user}', 'Admin\UsersController@show')->name('admin/users/show');
        Route::get('/statistics', 'Admin\UsersController@statistics')->name('user.statistics')->middleware(
            'can:user.statistics'
        );
        Route::get('/users/{user}/salary', 'Admin\UsersController@salary')->name('admin/users/salary')->middleware(
            'can:user.salary,user'
        );
        Route::post('/admin/users/{user}', 'Admin\UsersController@update')->name('admin/users/update');
        Route::put('/admin/users/{user}', 'Admin\UsersController@updateStatus');
        Route::post('/admin/users/{user}/take', 'Admin\UsersController@takeEmployee');
        Route::post('/admin/pain_row', 'Admin\UsersController@painRow');
        Route::delete('/admin/users/{user}', 'Admin\UsersController@destroy')->name('admin/users/destroy');
        Route::get('/admin/users/{user}/media/{mediaCollection}/{media}', 'Admin\UsersController@downloadMedia');
        Route::get('/admin/users/{user}/media/{mediaCollection}', 'Admin\UsersController@getMedia');
        Route::post('/admin/users/{user}/blacklist', 'Admin\UsersController@toBlacklist');
        Route::get('/admin/roles/{role}/users', 'Admin\UsersController@getUsersByRole')->name('admin.role.user');
        Route::get('/admin/users/{employee}/items/available', 'Admin\UsersController@getAvailableItems');
        Route::get('/admin/users/{employee}/{package}/package-items/available', 'Admin\UsersController@getPackageItems');
    }
);

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/admin/projects', 'Admin\ProjectsController@index');
        Route::get('/admin/projects/create', 'Admin\ProjectsController@create');
        Route::post('/admin/projects', 'Admin\ProjectsController@store');
        Route::get('/admin/projects/{project}/edit', 'Admin\ProjectsController@edit')->name('admin/projects/edit');
        Route::post('/admin/projects/{project}', 'Admin\ProjectsController@update')->name('admin/projects/update');
        Route::delete('/admin/projects/{project}', 'Admin\ProjectsController@destroy')->name('admin/projects/destroy');
        Route::get('/projects/{project}/employees', 'Admin\ProjectsController@getEmployees');
    }
);


/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'web'])->group(
    function () {
        Route::get('/admin/mailsettings', 'Admin\MailsettingsController@index');
        Route::get('/admin/mailsettings/create', 'Admin\MailsettingsController@create');
        Route::post('/admin/mailsettings/names', 'Admin\MailsettingsController@names');
        Route::post('/admin/mailsettings', 'Admin\MailsettingsController@store');
        Route::get('/admin/mailsettings/{mailsetting}/edit', 'Admin\MailsettingsController@edit')->name(
            'admin/mailsettings/edit'
        );
        Route::post('/admin/mailsettings/{mailsetting}', 'Admin\MailsettingsController@update')->name(
            'admin/mailsettings/update'
        );
        Route::delete('/admin/mailsettings/{mailsetting}', 'Admin\MailsettingsController@destroy')->name(
            'admin/mailsettings/destroy'
        );
    }
);

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'web'])->group(
    function () {
        Route::get('/admin/mails/create', 'Admin\MailsController@create');
        Route::get('/admin/mails', 'Admin\MailsController@index');
        Route::get(
            '/admin/mailsettings/{mailsettingsId}/folder/{folder}/mails/{id}',
            'Admin\MailsController@show'
        )->name('admin/mails/show');
        Route::post('/admin/mails', 'Admin\MailsController@store');
        Route::get('/admin/mails/{mail}/edit', 'Admin\MailsController@edit')->name('admin/mails/edit');
        Route::post('/admin/mails/{mail}', 'Admin\MailsController@update')->name('admin/mails/update');
        Route::delete('/admin/mails/{mail}', 'Admin\MailsController@destroy')->name('admin/mails/destroy');
    }
);

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/admin/roles', 'Admin\RolesController@index');
        Route::get('/admin/roles/create', 'Admin\RolesController@create');
        Route::post('/admin/roles', 'Admin\RolesController@store');
        Route::get('/admin/roles/{role}/edit', 'Admin\RolesController@edit')->name('admin/roles/edit');
        Route::post('/admin/roles/{role}', 'Admin\RolesController@update')->name('admin/roles/update');
        Route::delete('/admin/roles/{role}', 'Admin\RolesController@destroy')->name('admin/roles/destroy');
    }
);

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/admin/warehouses', 'Admin\WarehouseController@index');
        Route::get('/admin/warehouses/create', 'Admin\WarehouseController@create');
        Route::post('/admin/warehouses', 'Admin\WarehouseController@store');
        Route::get('/admin/warehouses/{warehouse}/edit', 'Admin\WarehouseController@edit')->name(
            'admin/warehouses/edit'
        );
        Route::post('/admin/warehouses/{warehouse}', 'Admin\WarehouseController@update')->name(
            'admin/warehouses/update'
        );
        Route::delete('/admin/warehouses/{warehouse}', 'Admin\WarehouseController@destroy')->name(
            'admin/warehouses/destroy'
        );
        Route::get('/admin/warehouses/{warehouse}/items/available', 'Admin\WarehouseController@getAvailableItems');
    }
);

Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/warehouses/{warehouse}/items', 'Admin\WarehousablesController@index');
        Route::get('/warehouses/{warehouse}/items/{item}', 'Admin\WarehousablesController@itemInfo');
    }
);
/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/admin/tasks', 'Admin\TaskController@index');
        Route::get('/admin/users/{user}/tasks', 'Admin\TaskController@userTasks');
        Route::get('/admin/tasks/create', 'Admin\TaskController@create');
        Route::get('/admin/tasks/create-from-package/{package}', 'Admin\TaskController@createFromPackage')->name('task.crate.from.package');
        Route::post('/admin/tasks', 'Admin\TaskController@store');
        Route::get('/admin/tasks/{task}/edit', 'Admin\TaskController@edit')->name('admin/tasks/edit');
        Route::post('/admin/tasks/csv', 'Admin\TaskController@createCsv');
        Route::post('/admin/tasks/{task}', 'Admin\TaskController@update')->name('admin/tasks/update');
        Route::delete('/admin/tasks/{task}', 'Admin\TaskController@destroy')->name('admin/tasks/destroy');
        Route::get('/admin/tasks/{task}', 'Admin\TaskController@show');
        Route::post('/admin/tasks/{task}/forcedelivered', 'Admin\TaskController@forceDelivered');

    }
);

Route::post('/tasks/{task}/like', 'Admin\TaskController@like')->middleware('can:task.like,task');

/* Auto-generated admin routes */
Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/admin/packages', 'Admin\PackageController@index')->name('admin/packages');
        Route::get('/admin/packages/create', 'Admin\PackageController@create');
        Route::post('/admin/packages', 'Admin\PackageController@store');
        Route::get('/admin/packages/{package}', 'Admin\PackageController@show');
        Route::get('/admin/packages/{package}/edit', 'Admin\PackageController@edit')->name('admin/packages/edit');
        Route::post('/admin/packages/{package}', 'Admin\PackageController@update')->name('admin/packages/update');
        Route::post('/admin/packages/{package}/forcedelivered', 'Admin\PackageController@forceDelivered');
        Route::delete('/admin/packages/{package}', 'Admin\PackageController@destroy')->name('admin/packages/destroy');

        Route::get('/admin/help-videos', 'Admin\HelpVideosController@index');
        Route::get('/admin/help-videos/create', 'Admin\HelpVideosController@create');
        Route::post('/admin/help-videos', 'Admin\HelpVideosController@store');
        Route::get('/admin/help-videos/{helpVideo}/edit', 'Admin\HelpVideosController@edit')->name(
            'admin/help-videos/edit'
        );
        Route::post('/admin/help-videos/{helpVideo}', 'Admin\HelpVideosController@update')->name(
            'admin/help-videos/update'
        );
        Route::delete('/admin/help-videos/{helpVideo}', 'Admin\HelpVideosController@destroy')->name(
            'admin/help-videos/destroy'
        );
    }
);

Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/twilio/token', 'SipController@newToken');
        Route::get('/twilio/callerinfo', 'SipController@callerInfo');
        Route::post('/twilio/missedcall', 'SipController@notifyCenter');
    }
);

Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::post('/sms/custom', 'SmsController@sendCustom');
    }
);

Route::namespace('\Brackets\Media\Http\Controllers')->group(
    function () {
        Route::get('view', 'FileViewController@view')->name('brackets/media::view');
    }
);

Route::post('upload', 'FileUploadController@upload')->name('brackets/media::upload');

/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/mail-templates', 'Admin\MailTemplatesController@index');
        Route::get('/mail-templates/create', 'Admin\MailTemplatesController@create');
        Route::post('/mail-templates', 'Admin\MailTemplatesController@store');
        Route::get('/mail-templates/{mailTemplate}/edit', 'Admin\MailTemplatesController@edit')->name(
            'admin/mail-templates/edit'
        );
        Route::post('/mail-templates/{mailTemplate}', 'Admin\MailTemplatesController@update')->name(
            'admin/mail-templates/update'
        );
        Route::delete('/mail-templates/{mailTemplate}', 'Admin\MailTemplatesController@destroy')->name(
            'admin/mail-templates/destroy'
        );
        Route::get('/mail-templates/{id}/test', 'Admin\MailTemplatesController@testTemplatization');
    }
);

// Route::get('/testsms', function (){
// Auth::user()->notify(new \App\Notifications\CustomSms('some custom text'));
// });
//Route::get('/testdial', function (){
//
//});
//Route::get('/testmail', function (){
//    $template = \App\MailTemplate::first();
//    $content = $template->getTemplatized(['recipient' => Auth::user()]);
//    Auth::user()->notify(new \App\Notifications\TemplatizedMail($template, $content));
//});

Route::middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/forms/{form}/fields', 'Admin\FieldsController@index')->name('admin/fields/index');
        Route::get('/forms/{form}/fields/create', 'Admin\FieldsController@create')->name('admin/fields/create');
        Route::post('/forms/{form}/fields', 'Admin\FieldsController@store');
        Route::get('/forms/{form}/fields/{field}/edit', 'Admin\FieldsController@edit')->name('admin/fields/edit');
        Route::post('/forms/{form}/fields/{field}', 'Admin\FieldsController@update')->name('admin/fields/update');
        Route::delete('/forms/{form}/fields/{field}', 'Admin\FieldsController@destroy')->name('admin/fields/destroy');
    }
);

/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/history', 'Admin\ActivityLogController@index');
        Route::post('/history', 'Admin\ActivityLogController@store');
    }
);

/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/forms', 'Admin\FormsController@index');
        Route::get('/forms/create', 'Admin\FormsController@create');
        Route::post('/forms', 'Admin\FormsController@store');
        Route::get('/forms/{form}/edit', 'Admin\FormsController@edit')->name('admin/forms/edit');
        Route::post('/forms/{form}', 'Admin\FormsController@update')->name('admin/forms/update');
        Route::delete('/forms/{form}', 'Admin\FormsController@destroy')->name('admin/forms/destroy');
    }
);


/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/mailings', 'Admin\MailingsController@index');
        Route::get('/mailings/create', 'Admin\MailingsController@create');
        Route::post('/mailings', 'Admin\MailingsController@store');
        Route::get('/mailings/{mailing}/edit', 'Admin\MailingsController@edit')->name('admin/mailings/edit');
        Route::post('/mailings/{mailing}', 'Admin\MailingsController@update')->name('admin/mailings/update');
        Route::post('/mailings/{mailing}/apply', 'Admin\MailingsController@applyDelivery')->name('admin/mailings/apply');
        Route::post('/mailings/{mailing}/applynow', 'Admin\MailingsController@applyDeliveryNow');
        Route::post('/mailings/{mailing}/resend', 'Admin\MailingsController@resendMailing')->name('admin/mailings/resend');
        Route::post('/mailings/{mailing}/test', 'Admin\MailingsController@testMailingTemplate')->name('admin/mailings/test');
        Route::post('/mailings/{mailing}/stopsending', 'Admin\MailingsController@stopSending');
        Route::delete('/mailings/{mailing}', 'Admin\MailingsController@destroy')->name('admin/mailings/destroy');
    }
);

/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/email-groups', 'Admin\EmailGroupsController@index');
        Route::get('/email-groups/create', 'Admin\EmailGroupsController@create');
        Route::post('/email-groups', 'Admin\EmailGroupsController@store');
        Route::post('/email-groups/csv', 'Admin\EmailGroupsController@parseCsv');
        Route::get('/email-groups/{emailGroup}/edit', 'Admin\EmailGroupsController@edit')->name(
            'admin/email-groups/edit'
        );
        Route::post('/email-groups/{emailGroup}', 'Admin\EmailGroupsController@update')->name(
            'admin/email-groups/update'
        );
        Route::delete('/email-groups/{emailGroup}', 'Admin\EmailGroupsController@destroy')->name(
            'admin/email-groups/destroy'
        );
    }
);

/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/email-mailings', 'Admin\EmailMailingsController@index');
        Route::get('/email-mailings/create', 'Admin\EmailMailingsController@create');
        Route::post('/email-mailings', 'Admin\EmailMailingsController@store');
        Route::get('/email-mailings/{emailMailing}/edit', 'Admin\EmailMailingsController@edit')->name(
            'admin/email-mailings/edit'
        );
        Route::post('/email-mailings/{emailMailing}', 'Admin\EmailMailingsController@update')->name(
            'admin/email-mailings/update'
        );
        Route::delete('/email-mailings/{emailMailing}', 'Admin\EmailMailingsController@destroy')->name(
            'admin/email-mailings/destroy'
        );
    }
);


Route::prefix('admin')->group(
    function () {
        Route::get('/businessprocesses/roles', 'BusinessProcessController@getRoles');
        Route::get('/businessprocesses/events', 'BusinessProcessController@getEvents');
        Route::get('/businessprocesses/mail_templates', 'BusinessProcessController@getMailTemplates');
        Route::get('/businessprocesses/actions', 'BusinessProcessController@getActions');
        Route::get('/businessprocesses/users', 'BusinessProcessController@getUsers');
        Route::post('/businessprocesses', 'BusinessProcessController@store');
        Route::get('/businessprocesses', 'BusinessProcessController@index')->name('admin.businessprocess.index');
        Route::delete('/businessprocesses/{businessProcess}', 'BusinessProcessController@destroy');
        Route::get('/businessprocesses/create', 'BusinessProcessController@create');
        Route::get('/businessprocesses/{businessProcess}/edit', 'BusinessProcessController@edit');
        Route::post('/businessprocesses/{businessProcess}', 'BusinessProcessController@update');
        Route::post('/businessprocesses/{businessProcess}/active', 'BusinessProcessController@active');
    }
);

/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/settings', 'Admin\SettingsController@index');
        Route::get('/settings/create', 'Admin\SettingsController@create');
        Route::post('/settings', 'Admin\SettingsController@store');
        Route::get('/settings/{setting}/edit', 'Admin\SettingsController@edit')->name('admin/settings/edit');
        Route::post('/settings/{setting}', 'Admin\SettingsController@update')->name('admin/settings/update');
        Route::delete('/settings/{setting}', 'Admin\SettingsController@destroy')->name('admin/settings/destroy');
    }
);


/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/statistics', 'Admin\StatisticsController@index');
        Route::get('/statistics/create', 'Admin\StatisticsController@create');
        Route::post('/statistics', 'Admin\StatisticsController@store');
        Route::get('/statistics/{statistic}/edit', 'Admin\StatisticsController@edit')->name('admin/statistics/edit');
        Route::post('/statistics/{statistic}', 'Admin\StatisticsController@update')->name('admin/statistics/update');
        Route::delete('/statistics/{statistic}', 'Admin\StatisticsController@destroy')->name(
            'admin/statistics/destroy'
        );
    }
);


/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/mailings/{mailing}/history-mailings', 'Admin\HistoryMailingsController@index');
    }
);


/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/items', 'Admin\ItemsController@index');
        Route::get('/packages/{package}/items', 'Admin\ItemsController@getPackageItems');
        Route::get('/tasks/{task}/items', 'Admin\ItemsController@getTaskItems');
        Route::get('/items/unused', 'Admin\ItemsController@getUnusedItems');
        Route::get('/items/delivered', 'Admin\ItemsController@getDeliveredItems');
        Route::get('/items/create', 'Admin\ItemsController@create');
        Route::post('/items', 'Admin\ItemsController@store');
        Route::get('/items/{item}/edit', 'Admin\ItemsController@edit')->name('admin/items/edit');
        Route::post('/items/{item}', 'Admin\ItemsController@update')->name('admin/items/update');
        Route::delete('/items/{item}', 'Admin\ItemsController@destroy')->name('admin/items/destroy');
    }
);

Route::middleware('web', 'auth')->group(
    function () {
        Route::get('/users/{id}/notifications', 'NotificationController@index')->name(
            'codersstudio.notifications.index'
        );
        Route::post('/resend-verification-email', 'ProfileController@resendVerificationEmail');
    }
);

/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {

        Route::post('/sales-tasks/csv', 'Admin\SalesTasksController@createCsv');
        Route::get('/sales-tasks', 'Admin\SalesTasksController@index');
        Route::get('/sales-tasks/create', 'Admin\SalesTasksController@create');
        Route::post('/sales-tasks', 'Admin\SalesTasksController@store');
        Route::get('/sales-tasks/{salesTask}/edit', 'Admin\SalesTasksController@edit')->name('admin/sales-tasks/edit');
        Route::post('/sales-tasks/{salesTask}', 'Admin\SalesTasksController@update')->name('admin/sales-tasks/update');
        Route::post('/sales-tasks/{salesTask}/forcedelivered', 'Admin\SalesTasksController@forceDelivered');
        Route::delete('/sales-tasks/{salesTask}', 'Admin\SalesTasksController@destroy')->name(
            'admin/sales-tasks/destroy'
        );
        Route::get('/sales-tasks/{salesTask}', 'Admin\SalesTasksController@show');
    }
);


/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/invest-tasks', 'Admin\InvestTasksController@index');
        Route::get('/invest-tasks/create', 'Admin\InvestTasksController@create');
        Route::post('/invest-tasks', 'Admin\InvestTasksController@store');
        Route::get('/invest-tasks/{investTask}/edit', 'Admin\InvestTasksController@edit')->name(
            'admin/invest-tasks/edit'
        );
        Route::post('/invest-tasks/{investTask}', 'Admin\InvestTasksController@update')->name(
            'admin/invest-tasks/update'
        );
        Route::delete('/invest-tasks/{investTask}', 'Admin\InvestTasksController@destroy')->name(
            'admin/invest-tasks/destroy'
        );
        Route::get('/invest-tasks/{investTask}', 'Admin\InvestTasksController@show');
        Route::post('/invest-tasks/{investTask}/restore', 'Admin\InvestTasksController@restore');
        Route::post('/invest-tasks/{investTask}/change-status', 'Admin\InvestTasksController@changeStatus');
    }
);


/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/dkim-keys', 'Admin\DkimKeysController@index');
        Route::get('/dkim-keys/create', 'Admin\DkimKeysController@create');
        Route::post('/dkim-keys', 'Admin\DkimKeysController@store');
        Route::get('/dkim-keys/{dkimKey}/edit', 'Admin\DkimKeysController@edit')->name('admin/dkim-keys/edit');
        Route::post('/dkim-keys/{dkimKey}', 'Admin\DkimKeysController@update')->name('admin/dkim-keys/update');
        Route::delete('/dkim-keys/{dkimKey}', 'Admin\DkimKeysController@destroy')->name('admin/dkim-keys/destroy');
    }
);


/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/investor-lists', 'Admin\InvestorListsController@index');
        Route::get('/investor-lists/create', 'Admin\InvestorListsController@create');
        Route::post('/investor-lists', 'Admin\InvestorListsController@store');
        Route::get('/investor-lists/{investorList}/edit', 'Admin\InvestorListsController@edit')->name(
            'admin/investor-lists/edit'
        );
        Route::post('/investor-lists/{investorList}', 'Admin\InvestorListsController@update')->name(
            'admin/investor-lists/update'
        );
        Route::delete('/investor-lists/{investorList}', 'Admin\InvestorListsController@destroy')->name(
            'admin/investor-lists/destroy'
        );
    }
);


/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/contragents', 'Admin\ContragentsController@index')->name('admin.contragents');
        Route::get('/contragents/create', 'Admin\ContragentsController@create');
        Route::post('/contragents', 'Admin\ContragentsController@store');
        Route::get('/contragents/{contragent}/edit', 'Admin\ContragentsController@edit')->name(
            'admin/contragents/edit'
        );
        Route::post('/contragents/{contragent}', 'Admin\ContragentsController@update')->name(
            'admin/contragents/update'
        );
        Route::delete('/contragents/{contragent}', 'Admin\ContragentsController@destroy')->name(
            'admin/contragents/destroy'
        );
    }
);


/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/auto-answers', 'Admin\AutoAnswersController@index');
        Route::get('/auto-answers/create', 'Admin\AutoAnswersController@create');
        Route::post('/auto-answers', 'Admin\AutoAnswersController@store');
        Route::get('/auto-answers/{autoAnswer}/edit', 'Admin\AutoAnswersController@edit')->name(
            'admin/auto-answers/edit'
        );
        Route::post('/auto-answers/{autoAnswer}', 'Admin\AutoAnswersController@update')->name(
            'admin/auto-answers/update'
        );
        Route::delete('/auto-answers/{autoAnswer}', 'Admin\AutoAnswersController@destroy')->name(
            'admin/auto-answers/destroy'
        );
    }
);

/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(
    function () {
        Route::get('/contragent-lists', 'Admin\ContragentListsController@index');
        Route::get('/contragent-lists/create', 'Admin\ContragentListsController@create');
        Route::post('/contragent-lists', 'Admin\ContragentListsController@store');
        Route::get('/contragent-lists/{contragentList}/edit', 'Admin\ContragentListsController@edit')->name(
            'admin/contragent-lists/edit'
        );
        Route::post('/contragent-lists/{contragentList}', 'Admin\ContragentListsController@update')->name(
            'admin/contragent-lists/update'
        );
        Route::delete('/contragent-lists/{contragentList}', 'Admin\ContragentListsController@destroy')->name(
            'admin/contragent-lists/destroy'
        );
    }
);


/* Auto-generated admin routes */
Route::prefix('admin')->middleware(['auth:' . config('admin-auth.defaults.guard'), 'admin'])->group(function () {
    Route::get('/invest-task-templates',                        'Admin\InvestTaskTemplatesController@index');
    Route::get('/invest-task-templates/create',                 'Admin\InvestTaskTemplatesController@create');
    Route::post('/invest-task-templates',                       'Admin\InvestTaskTemplatesController@store');
    Route::get('/invest-task-templates/{investTaskTemplate}/edit','Admin\InvestTaskTemplatesController@edit')->name('admin/invest-task-templates/edit');
    Route::post('/invest-task-templates/{investTaskTemplate}',  'Admin\InvestTaskTemplatesController@update')->name('admin/invest-task-templates/update');
    Route::delete('/invest-task-templates/{investTaskTemplate}','Admin\InvestTaskTemplatesController@destroy')->name('admin/invest-task-templates/destroy');
});
