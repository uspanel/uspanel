<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\HelpVideo;
use App\Media;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HelpVideoTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test store Help Video functionality
     *
     * @return void
     */
    public function testStoreIndex()
    {
        if(!copy(storage_path('path_test_help_video.mp4'), storage_path('/uploads/path_test_help_video.mp4'))){
            return;
        }
        $user = User::Role('Administrator')->firstOrFail();

        $data = [
            'description' => 'Test description help video',
            'title' => 'Test title help video',
            'video' => [[
                'action' => 'add',
                'collection_name' => 'video',
                'meta_data'=> [
                    'file_name' => 'односекундное видео!!!.mp4',
                    'name' => 'односекундное видео!!!.mp4'
                ],
                'path' => 'path_test_help_video.mp4'
            ]]
        ];

        $response = $this->actingAs($user)->ajax('post', '/admin/help-videos', $data);

        $response->assertStatus(200);

        $data = [
            'description' => 'Test description help video',
            'title' => 'Test title help video',
        ];
        $this->assertDatabaseHas('help_videos', $data);

        $helpVideo = HelpVideo::orderBy('id', 'desc')->first();

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.create_helpVideo',['id' => $helpVideo->id]),
        ]);

        $response = $this->actingAs($user)->ajax('get', '/admin/help-videos');
        $response->assertStatus(200)
        ->assertJson([
            'data' => [
                'current_page' => true,
                'data' => [[
                    'title' => true,
                    'description' => true,
                    'processing' => true
                ]],
                'first_page_url' => true,
                'from' => true,
                'last_page' => true,
                'last_page_url' => true,
                'path' => true,
                'per_page' => true,
                'to' => true,
                'total' => true
            ]
        ])
        ->assertJsonCount(1, 'data.data');
    }

    /**
     * Test Update Help Video functionality
     *
     * @return void
     */
    public function testUpdate()
    {
        $this->seed(\HelpVideoTableSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();
        $helpVideo = HelpVideo::firstOrFail();
        $data = [
            'title' => 'Test Update Help Video',
            'description' => 'Test',

        ];
        $response = $this->actingAs($user)->ajax('post', '/admin/help-videos/' . $helpVideo->id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('help_videos', $data);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.updated_helpVideo',['id' => $helpVideo->id]),
        ]);
    }

    /**
     * Test Update Help Video functionality
     *
     * @return void
     */
    public function testDestroy()
    {
        $this->seed(\HelpVideoTableSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();
        $helpVideo = HelpVideo::firstOrFail();

        $response = $this->actingAs($user)->ajax('delete', '/admin/help-videos/' . $helpVideo->id);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('help_videos', [
            'id' => $helpVideo->id
        ]);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.deleted_helpVideo',['id' => $helpVideo->id]),
        ]);
    }
}
