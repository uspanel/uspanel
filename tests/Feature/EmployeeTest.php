<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test Employee functionality
     *
     * @return void
     */
    public function testIndex()
    {
        $this->seed(\EmployeeStaffSeed::class);
        $user = User::Role('Administrator')->firstOrFail();

        $response = $this->actingAs($user)->ajax('get', '/employees');
        $response->assertStatus(200)
        ->assertJson([
            'data' => []
        ]);
    }
}
