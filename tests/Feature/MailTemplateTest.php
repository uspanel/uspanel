<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\MailTemplate;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MailTemplateTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test Mail Template functionality
     *
     * @return void
     */
    public function testIndex()
    {
        factory(MailTemplate::class, 10)->create();
        $user = User::Role('Administrator')->firstOrFail();

        $response = $this->actingAs($user)->ajax('get', '/admin/mail-templates');
        $response->assertStatus(200)
        ->assertJson([
            'data' => [
                'current_page' => true,
                'data' => [[
                    'id' => true,
                    'title' => true,
                    'body' => true,
                    'resource_url' => true
                ]],
                'first_page_url' => true,
                'from' => true,
                'last_page' => true,
                'last_page_url' => true,
                'path' => true,
                'per_page' => true,
                'to' => true,
                'total' => true
            ]
        ])
        ->assertJsonCount(10, 'data.data');
    }

    /**
     * Test store Mail Template functionality
     *
     * @return void
     */
    public function testStore()
    {
        $user = User::Role('Administrator')->firstOrFail();

        $data = [
            'title' => 'Test MailTemplate',
            'body' => 'Body',
            'subject' => 'Subject',
        ];

        $response = $this->actingAs($user)->ajax('post', '/admin/mail-templates', $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('mail_templates', $data);

        $mailTemplate = MailTemplate::orderBy('id', 'desc')->first();

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.create_mailTemplate',['id' => $mailTemplate->id]),
        ]);
    }

    /**
     * Test Update Mail Template functionality
     *
     * @return void
     */
    public function testUpdate()
    {
        factory(MailTemplate::class, 10)->create();
        $user = User::Role('Administrator')->firstOrFail();
        $mailTemplate = MailTemplate::orderBy('id', 'desc')->first();

        $data = [
            'title' => 'Test Update Mail Template',
            'body' => '666',
        ];
        $response = $this->actingAs($user)->ajax('post', '/admin/mail-templates/' . $mailTemplate->id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('mail_templates', $data);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.updated_mailTemplate',['id' => $mailTemplate->id]),
        ]);
    }

    /**
     * Test Update Mail Template functionality
     *
     * @return void
     */
    public function testDestroy()
    {
        factory(MailTemplate::class, 10)->create();
        $user = User::Role('Administrator')->firstOrFail();
        $mailTemplate = MailTemplate::orderBy('id', 'desc')->first();

        $response = $this->actingAs($user)->ajax('delete', '/admin/mail-templates/' . $mailTemplate->id);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('mail_templates', [
            'id' => $mailTemplate->id
        ]);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.deleted_mailTemplate',['id' => $mailTemplate->id]),
        ]);
    }
}
