<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Role;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RoleTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test Role functionality
     *
     * @return void
     */
    public function testIndex()
    {
        $this->seed(\RoleTableSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();

        $response = $this->actingAs($user)->ajax('get', '/admin/roles');
        $response->assertStatus(200)
        ->assertJson([
            'data' => [
                'current_page' => true,
                'data' => [[
                    'id' => true,
                    'name' => true,
                    'created_at' => true,
                    'resource_url' => true
                ]],
                'first_page_url' => true,
                'from' => true,
                'last_page' => true,
                'last_page_url' => true,
                'path' => true,
                'per_page' => true,
                'to' => true,
                'total' => true
            ]
        ])
        ->assertJsonCount(10, 'data.data');
    }

    /**
     * Test store Role functionality
     *
     * @return void
     */
    public function testStore()
    {
        $user = User::Role('Administrator')->firstOrFail();

        $data = [
            'name' => 'Test Role',
            'guard_name' => 'web',
        ];

        $response = $this->actingAs($user)->ajax('post', '/admin/roles', $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('roles', $data);

        $role = Role::orderBy('id', 'desc')->first();

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.create_role',['id' => $role->id]),
        ]);
    }

    /**
     * Test Update Role functionality
     *
     * @return void
     */
    public function testUpdate()
    {
        $this->seed(\RoleTableSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();
        $role = Role::orderBy('id', 'desc')->first();

        $data = [
            'name' => 'Test Update Role',
            'guard_name' => 'web',
        ];
        $response = $this->actingAs($user)->ajax('post', '/admin/roles/' . $role->id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('roles', $data);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.updated_role',['id' => $role->id]),
        ]);
    }

    /**
     * Test Update Role functionality
     *
     * @return void
     */
    public function testDestroy()
    {
        $this->seed(\RoleTableSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();
        $role = Role::orderBy('id', 'desc')->first();

        $response = $this->actingAs($user)->ajax('delete', '/admin/roles/' . $role->id);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('roles', [
            'id' => $role->id
        ]);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.deleted_role',['id' => $role->id]),
        ]);
    }
}
