<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test User functionality
     *
     * @return void
     */
    public function testIndex()
    {
        for ($i=0; $i < 10; $i++) {
            factory(\App\User::class)->create();
        }

        $admin = User::Role('Administrator')->firstOrFail();

        $response = $this->actingAs($admin)->ajax('get', '/admin/users');
        $response->assertStatus(200)
        ->assertJson([
            'data' => [
                'current_page' => true,
                'data' => [[
                    'id' => true,
                    'name' => true,
                    'created_at' => true,
                    'resource_url' => true
                ]],
                'first_page_url' => true,
                'from' => true,
                'last_page' => true,
                'last_page_url' => true,
                'path' => true,
                'per_page' => true,
                'to' => true,
                'total' => true
            ]
        ])
        ->assertJsonCount(10, 'data.data');
    }

    /**
     * Test store User functionality
     *
     * @return void
     */
    public function testStore()
    {
        $admin = User::Role('Administrator')->firstOrFail();

        $data = [
            'first_name' => 'First Test',
            'last_name' => 'Last Test',
            'email' => 'q@q.gm',
            'password' => '111',
            'project_id' => factory(\App\Project::class)->create()->id,
            'role_id' => 2,
            'profile' =>[
                'address' => 'address',
                'city' => 'city',
                'state_id' => 22,
                'zip' => 11111
            ],
        ];
        $response = $this->actingAs($admin)->ajax('post', '/admin/users', $data);
        $response->assertStatus(200);
        $data = [
            'first_name' => 'First Test',
            'last_name' => 'Last Test',
            'email' => 'q@q.gm'
        ];
        $this->assertDatabaseHas('users', $data);

        $user = User::orderBy('id', 'desc')->first();

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $admin->id,
            'description' => trans('history.actions.create_user',['id' => $user->id])
        ]);
    }

    /**
     * Test Update User functionality
     *
     * @return void
     */
    public function testUpdate()
    {
        factory(\App\User::class)->create();
        $admin = User::Role('Administrator')->firstOrFail();
        $user = User::orderBy('id', 'desc')->first();

        $data = [
            'first_name' => 'First Test',
            'last_name' => 'Last Test',
            'project_id' => factory(\App\Project::class)->create()->id,
            'role_id' => 2,
        ];
        $response = $this->actingAs($admin)->ajax('post', '/admin/users/' . $user->id, $data);
        $response->assertStatus(200);
        $data = [
            'first_name' => 'First Test',
            'last_name' => 'Last Test',
        ];
        $this->assertDatabaseHas('users', $data);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $admin->id,
            'description' => trans('history.actions.updated_user',['id' => $user->id])
        ]);
    }

    /**
     * Test Update User functionality
     *
     * @return void
     */
    public function testDestroy()
    {
        $this->seed(\EmployeeStaffSeed::class);
        $admin = User::Role('Administrator')->firstOrFail();
        $user = User::role('HR Manager Staff')->first();
        if (!$user) {
            $user = User::orderBy('id', 'desc')->first();
        }
        $response = $this->actingAs($admin)->ajax('delete', '/admin/users/' . $user->id);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('users', [
            'id' => $user->id,
            'deleted_at' => null
        ]);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $admin->id,
            'description' => trans('history.actions.deleted_user',['id' => $user->id])
        ]);
    }

}
