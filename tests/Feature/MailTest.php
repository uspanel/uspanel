<?php

namespace Tests\Feature;

use App\User;
use App\Mailsetting;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MailTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test Mail functionality
     *
     * @return void
     */
    public function testIndex()
    {
        $this->seed(\MailTableSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();

        $data = [
            'mailsettings',
            'defaultMailSettings'
        ];

        $response = $this->actingAs($user)->ajax('get', '/admin/mails');
        $response->assertStatus(200)
        ->assertViewIs('admin.mail.index')
        ->assertViewHasAll($data);
    }

    /**
     * Test Mail functionality
     *
     * @return void
     */
    public function testCreate()
    {
        $this->seed(\MailTableSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();

        $data = [
            'mailsettings',
            'defaultMailSettings'
        ];

        $response = $this->actingAs($user)->ajax('get', '/admin/mails/create');
        $response->assertStatus(200)
        ->assertViewIs('admin.mail.create')
        ->assertViewHasAll($data);
    }
}
