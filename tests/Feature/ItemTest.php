<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Item;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ItemTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test Item functionality
     *
     * @return void
     */
    public function testIndex()
    {
        $this->seed(\ItemTableSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();

        $response = $this->actingAs($user)->ajax('get', '/admin/items');
        $response->assertStatus(200)
        ->assertJsonStructure([
            'data' => [
                'current_page',
                'data' => [[
                    'id',
                    'size',
                    'color',
                    'link',
                    'user_id',
                    'name',
                    'weight',
                    'resource_url',
                    'user' => [
                        'id',
                        'email',
                        'created_at',
                        'updated_at',
                        'first_name',
                        'last_name',
                        'resource_url',
                        'avatar_thumb_url',
                        'name',
                        'approved',
                        'roles' => [
                            [
                                'id',
                                'name',
                                'guard_name',
                                'created_at',
                                'updated_at',
                                'resource_url',
                                'pivot' => [
                                'model_id',
                                    'role_id',
                                    'model_type',
                                ]
                            ]
                        ]
                    ]
                ]],
                'first_page_url',
                'from',
                'last_page',
                'last_page_url',
                'path',
                'per_page',
                'to',
                'total'
            ]
        ])
        ->assertJsonCount(10, 'data.data');
    }

    /**
     * Test store Item functionality
     *
     * @return void
     */
    public function testStore()
    {
        $user = User::Role('Administrator')->firstOrFail();

        $data = [
            'name' => 'Name item',
            'color' => 'red',
            'size' => '5',
            'link' => 'https://www.google.com',
            'info' => 'Info item',
            'user_id' => User::firstOrFail(),
        ];

        $response = $this->actingAs($user)->ajax('post', '/admin/items', $data);
        $response->assertStatus(200);
        unset($data['user_id']);
        $this->assertDatabaseHas('items', $data);

        $item = Item::orderBy('id', 'desc')->first();

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.create_item',['id' => $item->id]),
        ]);
    }

    /**
     * Test Update Item functionality
     *
     * @return void
     */
    public function testUpdate()
    {
        $this->seed(\ItemTableSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();
        $item = Item::orderBy('id', 'desc')->first();

        $data = [
            'name' => 'Test Update Item',
            'color' => 'red',
            'size' => '5',
            'link' => 'https://www.google.com',
        ];
        $response = $this->actingAs($user)->ajax('post', '/admin/items/' . $item->id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('items', $data);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.updated_item',['id' => $item->id]),
        ]);
    }

    /**
     * Test Update Item functionality
     *
     * @return void
     */
    public function testDestroy()
    {
        $this->seed(\ItemTableSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();
        $item = Item::orderBy('id', 'desc')->first();

        $response = $this->actingAs($user)->ajax('delete', '/admin/items/' . $item->id);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('items', [
            'id' => $item->id
        ]);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.deleted_item',['id' => $item->id]),
        ]);
    }
}
