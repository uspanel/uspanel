<?php

namespace Tests\Feature;

use App\Project;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\PhoneVerifications;
use Illuminate\Support\Facades\Storage;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRegister()
    {
        $this->seed(\EmployeeStaffSeed::class);
        $project = Project::first();
        Storage::disk('uploads')->put('file.txt', 'Contents');
        $data = [
            'phone' => '79281111121',
//            'phone' => '16503185203',
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => 'test@test.com',
            'password' => '123123',
            'password_confirmation' => '123123',
            'project_id' => $project->id
        ];
        $response = $this->ajax('POST', route('verification.send'), $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('phone_verifications', [
            'phone' => $data['phone']
        ]);
        $data['code'] = PhoneVerifications::where('phone', $data['phone'])->first()->code;
        $response = $this->ajax('POST', route('verification.code'), $data);
        $response->assertStatus(200);
        $data['cv'] = [
            [
                'path' => 'file.txt',
                'action' => 'add',
                'collection_name' => 'cv',
                'meta_data' => [
                    'file_name' => 'file.txt',
                    'name' => 'file.txt',
                ]
            ]
        ];
        $data['country'] = 'United States';
        $data['profile'] = [
            'address' => 'test',
            'city' => 'test',
            'state_id' => 1,
            'zip' => 'test',
            'authorized_to_work' => true,
            'adult' => true,
            'higher_education' => true,
        ];
        //Uncomment to test voicent integration
        config(['voicent.enabled' => true]);

        $response = $this->ajax('POST', '/register', $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('profiles', $data['profile']);
    }
}
