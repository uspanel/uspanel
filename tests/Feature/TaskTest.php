<?php

namespace Tests\Feature;

use App\{
    Package,
    Task,
    User,
    Employee,
    Warehouse
};
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use App\Notifications\System;

class TaskTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test tasks list functionality
     *
     * @return void
     */
    public function testIndex()
    {
        $i = 0;
        do {
            $this->seed(\EmployeeStaffSeed::class);
            $i++;
        }
        while ($i <10);

        $user = User::Role('Administrator')->firstOrFail();
        $response = $this->actingAs($user)->ajax('get', '/admin/tasks');
        $response->assertStatus(200)
        ->assertJson([
            'data' => [
                'current_page' => true,
                'total' => true,
                'data' => [
                    [
                        'id' => true,
                        'delivered_at' => true,
                        'carrier_id' => true,
                        'track' => true,
                        'employee_status_id' => true,
                        'comment' => true,
                        'creator_id' => true,
                        'manager_id' => true,
                        'employee_id' => true,
                        'warehouse_id' => true,
                        'resource_url' => true,
                        'warehouse' => [
                            'id' => true,
                            'name' => true
                        ],
                        'carrier' => [
                            'id' => true,
                            'name' => true
                        ],
                        'creator' => [
                            'id' => true,
                            'name' => true
                        ],
                        'manager' => [
                            'id' => true,
                            'name' => true
                        ],
                    ]
                ],
                'first_page_url' => true,
                'from' => true,
                'last_page' => true,
                'last_page_url' => true,
                'path' => true,
                'per_page' => true,
                'to' => true,
            ]
        ])->assertJsonCount(10, 'data.data');
    }

    /**
     * Test store task functionality
     *
     * @return void
     */
    public function testStore()
    {
        $this->seed(\EmployeeStaffSeed::class);
        $user = User::Role('Administrator')->firstOrFail();
        $warehouse = Warehouse::first();
        $package = Package::first();
        $package->update(['delivered_at' => date('Y-m-d H:i:s')]);
        $employee = Employee::find($package->employee_id);
        $items = $this->actingAs($user)->ajax('get', '/admin/users/' . $employee->id . '/items/available');
        $itemsjson = $items->json();
        $data = [
            'warehouse_id' => $warehouse->id,
            'employee_id' => $employee->id,
            'employee_status_id' => 1,
            'manager_id' => 1,
            'carrier_id' => 1,
            'track' => '123123',
            'item_ids' => [
                $itemsjson['data']['data'][0]['id'] => $itemsjson['data']['data'][0]['pivot']['qty']
            ]
        ];
        $response = $this->actingAs($user)->ajax('post', '/admin/tasks', $data);
        $response->assertStatus(200);
        unset($data['item_ids']);
        $this->assertDatabaseHas('tasks', $data);

        $task = Task::orderBy('id', 'desc')->first();

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.create_task',['id' => $task->id]),
        ]);
    }

    /**
     * Test Update task functionality
     *
     * @return void
     */
    public function testUpdate()
    {
        $this->seed(\EmployeeStaffSeed::class);
        $task = Task::firstOrFail();
        $user = User::Role('Administrator')->firstOrFail();
        $data = [
            'warehouse_id' => factory(\App\Warehouse::class)->create()->id,
            'carrier_id' => '2',
            'track' => '541239',
            'employee_status_id' => 2,
            'comment' => 'Nostrum labore ullam harum dolores sit similique libero.',
            'creator_id' => 1,
            'manager_id' => 1,
            'delivery_status_id' => null,
        ];
        $response = $this->actingAs($user)->ajax('post', '/admin/tasks/' . $task->id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('tasks', $data);
        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.updated_task',['id' => $task->id]),
        ]);
    }

    /**
     * Test Update task functionality
     *
     * @return void
     */
    public function testDestroy()
    {
        $this->seed(\EmployeeStaffSeed::class);
        $task = Task::orderBy('id', 'desc')->first();
        $user = User::Role('Administrator')->firstOrFail();

        $response = $this->actingAs($user)->ajax('delete', '/admin/tasks/' . $task->id);
        $response->assertStatus(200);

        $this->assertDatabaseMissing('tasks', [
            'id' => $task->id,
            'deleted_at' => null
        ]);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.deleted_task',['id' => $task->id]),
        ]);
    }

    /**
     * Test Like task functionality
     *
     * @return void
     */
    public function testLike()
    {
        Notification::fake();
        do {$this->seed(\EmployeeStaffSeed::class);
        } while (Task::where('employee_status_id','=',1)->first() == null);
        $task = Task::where('employee_status_id', '1')->first();
        $user = User::Role('Administrator')->firstOrFail();

        $response = $this->actingAs($user)->ajax('post', '/tasks/' . $task->id . '/like');
        $response->assertStatus(200);

        $this->assertDatabaseHas('tasks', [
            'id' => $task->id,
            'employee_status_id' => '2'
        ]);

        Notification::assertSentTo(
            [$user],
            System::class
        );

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.updated_task',['id' => $task->id]),
        ]);
    }
}
