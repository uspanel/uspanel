<?php

namespace Tests\Feature;

use App\Notifications\System;
use App\{
    Package,
    Employee,
    SalesTask,
    User,
    Warehouse
};
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SalesTasksTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test Sales Tasks list functionality
     *
     * @return void
     */
    public function testIndex()
    {
        $i = 0;
        do {
            $this->seed(\EmployeeStaffSeed::class);
            $i++;
        }
        while ($i <10);

        $user = User::Role('Administrator')->firstOrFail();
        $response = $this->actingAs($user)->ajax('get', '/admin/sales-tasks');
        $response->assertStatus(200)
            ->assertJson([
                'data' => [
                    'current_page' => true,
                    'total' => true,
                    'data' => [
                        [
                            'id' => true,
                            'delivered_at' => true,
                            'carrier_id' => true,
                            'track' => true,
                            'employee_status_id' => true,
                            'comment' => true,
                            'creator_id' => true,
                            'manager_id' => true,
                            'employee_id' => true,
                            'sales_status_id' => true,
                            'warehouse_id' => true,
                            'resource_url' => true,
                            'warehouse' => [
                                'id' => true,
                                'name' => true,
                                'city' => true,
                                'address' => true,
                            ],
                            'carrier' => [
                                'id' => true,
                                'name' => true
                            ],
                            'creator' => [
                                'id' => true,
                                'name' => true
                            ],
                            'manager' => [
                                'id' => true,
                                'name' => true
                            ],
                        ]
                    ],
                    'first_page_url' => true,
                    'from' => true,
                    'last_page' => true,
                    'last_page_url' => true,
                    'path' => true,
                    'per_page' => true,
                    'to' => true,
                ]
            ])->assertJsonCount(10, 'data.data');
    }

    /**
     * Test store Sales Tasks functionality
     *
     * @return void
     */
    public function testStore()
    {
        $this->seed(\EmployeeStaffSeed::class);
        $user = User::Role('Administrator')->firstOrFail();
        $package = Package::orderBy('id', 'desc')->first();
        $employee = Employee::find($package->employee_id);
        $warehouse = Warehouse::first();
        $warehouse->update(['user_id' => $employee->user->id]);
        $package->update(['delivered_at' => date('Y-m-d H:i:s')]);
        $items = $this->actingAs($user)->ajax('get', '/admin/warehouses/' . $warehouse->id . '/items/available');
        $itemsjson = $items->json();
        $data = [
            'warehouse_id' => $itemsjson['data']['data'][0]['pivot']['warehouse_id'],
            'employee_status_id' => 1,
            'manager_id' => 1,
            'carrier_id' => 1,
            'track' => '123123',
            'item_ids' => [
                $itemsjson['data']['data'][0]['id'] => [
                    'qty' => $itemsjson['data']['data'][0]['pivot']['qty'],
                    'price' => 10
                ]
            ]
        ];
        $response = $this->actingAs($user)->ajax('post', '/admin/sales-tasks', $data);
        $response->assertStatus(200);
        unset($data['item_ids']);
        $this->assertDatabaseHas('sales_tasks', $data);

        $salesTask = SalesTask::orderBy('id', 'desc')->first();

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.create_salesTask',['id' => $salesTask->id]),
        ]);
    }

    /**
     * Test Update Sales Tasks functionality
     *
     * @return void
     */
    public function testUpdate()
    {
        $this->seed(\EmployeeStaffSeed::class);
        $salesTask = SalesTask::firstOrFail();
        $user = User::Role('Administrator')->firstOrFail();
        $data = [
            'warehouse_id' => factory(\App\Warehouse::class)->create()->id,
            'carrier_id' => '2',
            'track' => '541239',
            'employee_status_id' => 2,
            'comment' => 'Nostrum labore ullam harum dolores sit similique libero.',
            'creator_id' => 1,
            'manager_id' => 1,
        ];
        $response = $this->actingAs($user)->ajax('post', '/admin/sales-tasks/' . $salesTask->id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('sales_tasks', $data);
        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.updated_salesTask',['id' => $salesTask->id]),
        ]);
    }

    /**
     * Test Update Sales Tasks functionality
     *
     * @return void
     */
    public function testDestroy()
    {
        $this->seed(\EmployeeStaffSeed::class);
        $salesTask = SalesTask::orderBy('id', 'desc')->first();
        $user = User::Role('Administrator')->firstOrFail();

        $response = $this->actingAs($user)->ajax('delete', '/admin/sales-tasks/' . $salesTask->id);
        $response->assertStatus(200);

        $this->assertDatabaseMissing('sales_tasks', [
            'id' => $salesTask->id,
            'deleted_at' => null
        ]);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.deleted_salesTask',['id' => $salesTask->id]),
        ]);
    }
}
