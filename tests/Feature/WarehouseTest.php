<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Warehouse;

class WarehouseTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test Warehouse functionality
     *
     * @return void
     */
    public function testIndex()
    {
        $this->seed(\WarehouseTableSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();

        $response = $this->actingAs($user)->ajax('get', '/admin/warehouses');
        $response->assertStatus(200)
        ->assertJson([
            'data' => [
                'current_page' => true,
                'data' => [[
                        'id' => true,
                        'name' => true,
                        'city' => true,
                        'address' => true,
                        'resource_url' => true,
                ]],
                'first_page_url' => true,
                'from' => true,
                'last_page' => true,
                'last_page_url' => true,
                'path' => true,
                'per_page' => true,
                'to' => true,
                'total' => true,
            ]

        ])
        ->assertJsonCount(10, 'data.data');
    }

    /**
     * Test store Warehouse functionality
     *
     * @return void
     */
    public function testStore()
    {
        $user = User::Role('Administrator')->firstOrFail();

        $data = [
            'name' => 'Test Warehouse',
        ];

        $response = $this->actingAs($user)->ajax('post', '/admin/warehouses', $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('warehouses', $data);

        $warehouse = Warehouse::orderBy('id', 'desc')->first();

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.create_warehouse',['id' => $warehouse->id]),
        ]);
    }

    /**
     * Test Update Warehouse functionality
     *
     * @return void
     */
    public function testUpdate()
    {
        $this->seed(\WarehouseTableSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();
        $warehouse = Warehouse::firstOrFail();

        $data = [
            'name' => 'Test Update Warehouse',
        ];

        $response = $this->actingAs($user)->ajax('post', '/admin/warehouses/' . $warehouse->id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('warehouses', $data);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.updated_warehouse',['id' => $warehouse->id]),
        ]);
    }

    /**
     * Test Update Warehouse functionality
     *
     * @return void
     */
    public function testDestroy()
    {
        $this->seed(\WarehouseTableSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();
        $warehouse = Warehouse::orderBy('id', 'desc')->first();

        $response = $this->actingAs($user)->ajax('delete', '/admin/warehouses/' . $warehouse->id);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('warehouses', [
            'id' => $warehouse->id
        ]);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.deleted_warehouse',['id' => $warehouse->id]),
        ]);
    }
}
