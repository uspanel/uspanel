<?php

namespace Tests\Feature;

use App\{User, HistoryMailing};
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HistoryMailingTest extends TestCase
{
    //use RefreshDatabase;

    /**
     * Test tasks list functionality
     *
     * @return void
     */
    public function testIndex()
    {
        $this->seed(\MailingStructureSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();
        $t = HistoryMailing::all();
        $response = $this->actingAs($user)->ajax('get', 'admin/mailings/' . HistoryMailing::first()->mailing_id . '/history-mailings');
        $response->assertStatus(200)
        ->assertJson([
            'data' => [
                'current_page' => true,
                'total' => true,
                'data' => [
                    [                        
                        'id' => true,
                        'mailing_id' => true,
                        'email_mailing_id' => true,
                        'mailing_status_id' => true,
                        'mailing_sending_id' => true,
                        'created_at' => true,
                        'mailing' => [
                            'id' => true,
                            'name' => true,
                            'mail_template_id' => true,
                            'scheduled_at' => true,
                            'created_at' => true,
                            'updated_at' => true,
                            'resource_url' => true,
                            'stats' => [
                                'total' => true,
                                'new' => true,
                                'sent' => true,
                                'registered' => true,
                            ]
                        ],
                        'mailing_status' => [
                            'id' => true,
                            'name' => true
                        ],
                        'email_mailing' => [
                            'id' => true,
                            'name' => true,
                            'email' => true,
                            'email_group_id' => true,
                            'created_at' => true,
                            'updated_at' => true,
                            'resource_url' => true
                        ],
                        'mailing_sending' => [
                            'id' => true,
                            'scheduled_at' => true,
                            'created_at' => true,
                            'updated_at' => true
                        ]
                    ]
                ],
                'first_page_url' => true,
                'from' => true,
                'last_page' => true,
                'last_page_url' => true,
                'path' => true,
                'per_page' => true,
                'to' => true,
            ]
        ])->assertJsonCount(10, 'data.data');
    }
}
