<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Setting;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SettingTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test Setting functionality
     *
     * @return void
     */
    public function testIndex()
    {
        factory(Setting::class, 10)->create();
        $user = User::Role('Administrator')->firstOrFail();

        $response = $this->actingAs($user)->ajax('get', '/admin/settings');
        $response->assertStatus(200)
        ->assertJson([
            'data' => [
                'current_page' => true,
                'data' => [[
                    'id' => true,
                    'name' => true,
                    'value' => true,
                    'resource_url' => true
                ]],
                'first_page_url' => true,
                'from' => true,
                'last_page' => true,
                'last_page_url' => true,
                'path' => true,
                'per_page' => true,
                'to' => true,
                'total' => true
            ]
        ])
        ->assertJsonCount(10, 'data.data');
    }

    /**
     * Test store Setting functionality
     *
     * @return void
     */
    public function testStore()
    {
        $user = User::Role('Administrator')->firstOrFail();

        $data = [
            'name' => 'Test Setting',
            'value' => '666',
        ];

        $response = $this->actingAs($user)->ajax('post', '/admin/settings', $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('settings', $data);

        $setting = Setting::orderBy('id', 'desc')->first();

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.create_setting',['id' => $setting->id]),
        ]);
    }

    /**
     * Test Update Setting functionality
     *
     * @return void
     */
    public function testUpdate()
    {
        factory(Setting::class, 10)->create();
        $user = User::Role('Administrator')->firstOrFail();
        $setting = Setting::orderBy('id', 'desc')->first();

        $data = [
            'name' => 'Test Update Setting',
            'value' => '666',
        ];
        $response = $this->actingAs($user)->ajax('post', '/admin/settings/' . $setting->id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('settings', $data);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.updated_setting',['id' => $setting->id]),
        ]);
    }

    /**
     * Test Update Setting functionality
     *
     * @return void
     */
    public function testDestroy()
    {
        factory(Setting::class, 10)->create();
        $user = User::Role('Administrator')->firstOrFail();
        $setting = Setting::orderBy('id', 'desc')->first();

        $response = $this->actingAs($user)->ajax('delete', '/admin/settings/' . $setting->id);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('settings', [
            'id' => $setting->id
        ]);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.deleted_setting',['id' => $setting->id]),
        ]);
    }
}
