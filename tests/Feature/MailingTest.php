<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Mailing;

class MailingTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test Mailing functionality
     *
     * @return void
     */
    public function testIndex()
    {
        $user = User::Role('Administrator')->firstOrFail();

        $i = 1;
        while ($i <= 10){
            $this->seed(\MailingsSeeder::class);
            $i++;
        }

        $response = $this->actingAs($user)->ajax('get', '/admin/mailings');
        $response->assertStatus(200)
        ->assertJson([
            'data' => [
                'current_page' => true,
                'data' => [[
                    'id' => true,
                    'name' => true,
                    'mail_template_id' => true,
                    'scheduled_at' => true,
                    'resource_url' => true,
                    'mail_template' => true,
                ]],
                'first_page_url' => true,
                'from' => true,
                'last_page' => true,
                'last_page_url' => true,
                'path' => true,
                'per_page' => true,
                'to' => true,
                'total' => true
            ]
        ])
        ->assertJsonCount(10, 'data.data');
    }

    /**
     * Test store Mailing functionality
     *
     * @return void
     */
    public function testStore()
    {
        $user = User::Role('Administrator')->firstOrFail();
        $mailTemplate = \App\MailTemplate::first();
        if (!$mailTemplate) {
            $this->seed(\MailTemplatesSeeder::class);
            $mailTemplate = \App\MailTemplate::first();
        }
        $data = [
            'name' => 'Test mailing',
            'mail_template_id' => $mailTemplate->id,
        ];

        $response = $this->actingAs($user)->ajax('post', '/admin/mailings', $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('mailings', $data);

        $mailing = Mailing::orderBy('id', 'desc')->first();

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.create_mailing',['id' => $mailing->id]),
        ]);
    }

    /**
     * Test Update Mailing functionality
     *
     * @return void
     */
    public function testUpdate()
    {
        $user = User::Role('Administrator')->firstOrFail();
        $this->seed(\MailingsSeeder::class);
        $mailing = Mailing::orderBy('id', 'desc')->first();
        $data = [
            'name' => 'Test Update Mailing',
        ];

        $response = $this->actingAs($user)->ajax('post', '/admin/mailings/' . $mailing->id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('mailings', $data);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.updated_mailing',['id' => $mailing->id]),
        ]);
    }

    /**
     * Test Update Mailing functionality
     *
     * @return void
     */
    public function testDestroy()
    {
        $user = User::Role('Administrator')->firstOrFail();
        $this->seed(\MailingsSeeder::class);
        $mailing = Mailing::orderBy('id', 'desc')->first();

        $response = $this->actingAs($user)->ajax('delete', '/admin/mailings/' . $mailing->id);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('mailings', [
            'id' => $mailing->id,
            'deleted_at' => null
        ]);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.deleted_mailing',['id' => $mailing->id]),
        ]);
    }
}
