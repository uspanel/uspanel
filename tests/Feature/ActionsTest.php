<?php

namespace Tests\Feature;

use App\Employee;
use App\Jobs\CheckExpiredTasks;
use App\Package;
use App\Task;
use App\User;
use App\Warehouse;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Spatie\MediaLibrary\Media;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;

class ActionsTest extends TestCase
{
    use RefreshDatabase;
    /**
     * Test PackageStatusChange event
     *
     * @return void
     */
    public function testPackageStatusChange()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('employees')->truncate();
        DB::table('action_business_process_event_mail_template')->truncate();
        DB::table('action_business_process_event_user')->truncate();
        DB::table('action_business_process_events')->truncate();
        DB::table('business_process_events')->truncate();
        DB::table('business_processes')->truncate();
        DB::table('mail_templates')->truncate();
        DB::table('packages')->truncate();
        DB::table('tasks')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        Notification::fake();

        $this->seed(\MailTemplatesSeeder::class);
        $this->seed(\BusinessSeeder::class);
        do {$this->seed(\EmployeeStaffSeed::class);
        } while (Package::where('status_id','=',1)->first() == null);
        Package::where('status_id','=',1)->first()->update(['status_id' => 2]);

        Notification::assertSentTo(
            [\App\User::find(1)],
            \App\Notifications\System::class
        );

        Notification::assertSentTo(
            [\App\User::find(1)],
            \App\Notifications\TemplatizedMail::class
        );
    }

    public function testPackageCreated()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('employees')->truncate();
        DB::table('action_business_process_event_mail_template')->truncate();
        DB::table('action_business_process_event_user')->truncate();
        DB::table('action_business_process_events')->truncate();
        DB::table('business_process_events')->truncate();
        DB::table('business_processes')->truncate();
        DB::table('mail_templates')->truncate();
        DB::table('packages')->truncate();
        DB::table('tasks')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        Notification::fake();

        $this->seed(\MailTemplatesSeeder::class);
        $this->seed(\BusinessSeeder::class);
        do {$this->seed(\EmployeeStaffSeed::class);
        } while (Package::where('status_id','=',1)->first() == null);
        $pac = Package::where('status_id','=',1)->first()->toArray();
        Package::create($pac);
        Notification::assertSentTo(
            [\App\User::find(1)],
            \App\Notifications\System::class
        );

        Notification::assertSentTo(
            [\App\User::find(1)],
            \App\Notifications\TemplatizedMail::class
        );
    }

    public function testPackageEdited()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('employees')->truncate();
        DB::table('action_business_process_event_mail_template')->truncate();
        DB::table('action_business_process_event_user')->truncate();
        DB::table('action_business_process_events')->truncate();
        DB::table('business_process_events')->truncate();
        DB::table('business_processes')->truncate();
        DB::table('mail_templates')->truncate();
        DB::table('packages')->truncate();
        DB::table('tasks')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        Notification::fake();

        $this->seed(\MailTemplatesSeeder::class);
        $this->seed(\BusinessSeeder::class);
        do {$this->seed(\EmployeeStaffSeed::class);
        } while (Package::where('status_id','=',1)->first() == null);
        $pac = Package::where('status_id','=',1)->first()->update(['name' => 'some edited name']);
        Notification::assertSentTo(
            [\App\User::find(1)],
            \App\Notifications\System::class
        );

        Notification::assertSentTo(
            [\App\User::find(1)],
            \App\Notifications\TemplatizedMail::class
        );
    }

    public function testTaskStatusChanged()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('employees')->truncate();
        DB::table('action_business_process_event_mail_template')->truncate();
        DB::table('action_business_process_event_user')->truncate();
        DB::table('action_business_process_events')->truncate();
        DB::table('business_process_events')->truncate();
        DB::table('business_processes')->truncate();
        DB::table('mail_templates')->truncate();
        DB::table('packages')->truncate();
        DB::table('tasks')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        Notification::fake();

        $this->seed(\MailTemplatesSeeder::class);
        $this->seed(\BusinessSeeder::class);
        do {$this->seed(\EmployeeStaffSeed::class);
        } while (Task::where('employee_status_id','=',1)->first() == null);
        $pac = Task::where('employee_status_id','=',1)->first()->update(['employee_status_id' => 2]);
        Notification::assertSentTo(
            [\App\User::find(1)],
            \App\Notifications\System::class
        );

        Notification::assertSentTo(
            [\App\User::find(1)],
            \App\Notifications\TemplatizedMail::class
        );
    }

    public function testUserUploadDocuments()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('employees')->truncate();
        DB::table('action_business_process_event_mail_template')->truncate();
        DB::table('action_business_process_event_user')->truncate();
        DB::table('action_business_process_events')->truncate();
        DB::table('business_process_events')->truncate();
        DB::table('business_processes')->truncate();
        DB::table('mail_templates')->truncate();
        DB::table('packages')->truncate();
        DB::table('tasks')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        Notification::fake();

        $this->seed(\EmployeeStaffSeed::class);
        $this->seed(\MailTemplatesSeeder::class);
        $this->seed(\BusinessSeeder::class);
        Media::create([
            'model_type' => 'App\User',
            'model_id' => '1',
            'collection_name' => 'contract',
            'name' => 'name',
            'file_name' => 'filename',
            'disk' => 'media',
            'size' => '1',
            'manipulations' => 'no',
            'custom_properties' => '[]'
        ]);
        Notification::assertSentTo(
            [\App\User::find(1)],
            \App\Notifications\System::class
        );

        Notification::assertSentTo(
            [\App\User::find(1)],
            \App\Notifications\TemplatizedMail::class
        );
    }

    public function testUserRegistered()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('employees')->truncate();
        DB::table('action_business_process_event_mail_template')->truncate();
        DB::table('action_business_process_event_user')->truncate();
        DB::table('action_business_process_events')->truncate();
        DB::table('business_process_events')->truncate();
        DB::table('business_processes')->truncate();
        DB::table('mail_templates')->truncate();
        DB::table('packages')->truncate();
        DB::table('tasks')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        Notification::fake();

        $this->seed(\EmployeeStaffSeed::class);
        $this->seed(\MailTemplatesSeeder::class);
        $this->seed(\BusinessSeeder::class);
        User::create([
            'name' => 'Name Name',
            'email' => 'email@unique.com',
            'email_verified_at' => Carbon::now(),
            'password' => '123',
            'first_name' => 'First',
            'last_name' => 'Last',
            'address' => 'Some address',
            'phone_number' => '234234',
            'receive_external_calls' => 0
        ]);
        Notification::assertSentTo(
            [\App\User::find(1)],
            \App\Notifications\System::class
        );

        Notification::assertSentTo(
            [\App\User::find(1)],
            \App\Notifications\TemplatizedMail::class
        );
    }

    public function testTaskExpired()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('employees')->truncate();
        DB::table('action_business_process_event_mail_template')->truncate();
        DB::table('action_business_process_event_user')->truncate();
        DB::table('action_business_process_events')->truncate();
        DB::table('business_process_events')->truncate();
        DB::table('business_processes')->truncate();
        DB::table('mail_templates')->truncate();
        DB::table('packages')->truncate();
        DB::table('tasks')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        Notification::fake();

        $this->seed(\EmployeeStaffSeed::class);
        $this->seed(\MailTemplatesSeeder::class);
        $warehouse = Warehouse::first();
        $package = Package::first();
        $package->update(['delivered_at' => date('Y-m-d H:i:s')]);
        $employee = Employee::find($package->employee_id);
        $user = User::Role('Administrator')->firstOrFail();
        $items = $this->actingAs($user)->ajax('get', '/admin/users/' . $employee->id . '/items/available');
        $itemsjson = $items->json();

        $task = Task::create([
            'warehouse_id' => $warehouse->id,
            'employee_id' => $employee->id,
            'employee_status_id' => null,
            'manager_id' => User::role('HR Manager Staff')->first()->id,
            'carrier_id' => 1,
            'track' => '123123',
            'item_ids' => [
                $itemsjson['data']['data'][0]['id'] => $itemsjson['data']['data'][0]['pivot']['qty']
            ]
        ]);
        $task->created_at = Carbon::now()->subYear(1);
        $task->save();
        $this->seed(\BusinessSeeder::class);
        CheckExpiredTasks::dispatchNow();

        Notification::assertSentTo(
            [\App\User::find(1)],
            \App\Notifications\System::class
        );

        Notification::assertSentTo(
            [\App\User::find(1)],
            \App\Notifications\TemplatizedMail::class
        );
    }
}
