<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use App\Mailsetting;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\ActivityLogDeprecated;

class MailAccountTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test Mail Account functionality
     *
     * @return void
     */
    public function testIndex()
    {
        $this->seed(\MailSettingsSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();

        $response = $this->actingAs($user)->ajax('get', '/admin/mailsettings');
        $response->assertStatus(200)
        ->assertJson([
            'data' => [
                'current_page' => true,
                'data' => [[
                    'id' => true,
                    'user_id' => true,
                    'smtp_host' => true,
                    'smtp_port' => true,
                    'smtp_encryption' => true,
                    'imap_host' => true,
                    'imap_port' => true,
                    'imap_encryption' => true,
                    'imap_sent_folder' => true,
                    'imap_inbox_folder' => true,
                    'login' => true,
                    'resource_url' => true
                ]],
                'first_page_url' => true,
                'from' => true,
                'last_page' => true,
                'last_page_url' => true,
                'path' => true,
                'per_page' => true,
                'to' => true,
                'total' => true
            ]
        ])
        ->assertJsonCount(1, 'data.data');
    }

    /**
     * Test store Mail Account functionality
     *
     * @return void
     */
    public function testStore()
    {
        $user = User::Role('Administrator')->firstOrFail();

        $data = [
            'user_id' => 1,
            'smtp_host' => 'smtp.yandex.ru',
            'smtp_port' => 465,
            'smtp_encryption' => 'tls',
            'imap_host' => 'imap.yandex.ru',
            'imap_port' => 993,
            'imap_encryption' => 'tls',
            'imap_sent_folder' => 'Sent',
            'imap_inbox_folder' => 'INBOX',
            'login' => 'devcodersstudio@yandex.ru',
            'password' => 'kenny228',
            'default' => 1,
            'validate_cert' => 1
        ];

        $response = $this->actingAs($user)->ajax('post', '/admin/mailsettings', $data);
        $response->assertStatus(200);

        $data = [
            'user_id' => 1,
            'smtp_host' => 'smtp.yandex.ru',
            'smtp_port' => 465,
            'smtp_encryption' => 'tls',
            'imap_host' => 'imap.yandex.ru',
            'imap_port' => 993,
            'imap_encryption' => 'tls',
            'imap_sent_folder' => 'Sent',
            'imap_inbox_folder' => 'INBOX',
            'login' => 'devcodersstudio@yandex.ru',
            'default' => 1,
            'validate_cert' => 1
        ];

        $this->assertDatabaseHas('mailsettings', $data);

        $mailAccount = Mailsetting::orderBy('id', 'desc')->first();

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.created_mailaccount',['id' => $mailAccount->id]),
        ]);
    }

    /**
     * Test Update Mail Account functionality
     *
     * @return void
     */
    public function testUpdate()
    {
        $this->seed(\MailSettingsSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();
        $mailAccount = Mailsetting::orderBy('id', 'desc')->first();

        $data = [
            'user_id' => 1,
            'smtp_host' => 'smtp.yandex.ru',
            'smtp_port' => 100,
            'smtp_encryption' => 'tls',
            'imap_host' => 'imap.yandex.ru',
            'imap_port' => 101,
            'imap_encryption' => 'tls',
            'imap_sent_folder' => 'Sent',
            'imap_inbox_folder' => 'INBOX',
            'login' => 'devcodersstudio@yandex.ru',
            'default' => 1,
            'validate_cert' => 1
        ];
        $response = $this->actingAs($user)->ajax('post', '/admin/mailsettings/' . $mailAccount->id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('mailsettings', $data);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.updated_mailaccount',['id' => $mailAccount->id]),
        ]);
    }

    /**
     * Test Update Mail Account functionality
     *
     * @return void
     */
    public function testDestroy()
    {
        $this->seed(\MailSettingsSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();
        $mailAccount = Mailsetting::orderBy('id', 'desc')->first();

        $response = $this->actingAs($user)->ajax('delete', '/admin/mailsettings/' . $mailAccount->id);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('mailsettings', [
            'id' => $mailAccount->id
        ]);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.deleted_mailaccount',['id' => $mailAccount->id]),
        ]);
    }
}
