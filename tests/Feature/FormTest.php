<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Form;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\ActivityLogDeprecated;

class FormTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test Forms functionality
     *
     * @return void
     */
    public function testIndex()
    {
        factory(Form::class, 10)->create();

        $user = User::Role('Administrator')->firstOrFail();

        $response = $this->actingAs($user)->ajax('get', '/admin/forms');
        $response->assertStatus(200)
        ->assertJson([
            'data' => [
                'current_page' => true,
                'data' => [[
                    'id' => true,
                    'name' => true,
                    'resource_url' => true
                ]],
                'first_page_url' => true,
                'from' => true,
                'last_page' => true,
                'last_page_url' => true,
                'path' => true,
                'per_page' => true,
                'to' => true,
                'total' => true
            ]
        ])
        ->assertJsonCount(10, 'data.data');
    }

    /**
     * Test store Forms functionality
     *
     * @return void
     */
    public function testStore()
    {
        $user = User::Role('Administrator')->firstOrFail();

        $data = [
            'name' => 'Test Form',
        ];

        $response = $this->actingAs($user)->ajax('post', '/admin/forms', $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('forms', $data);

        $form = Form::orderBy('id', 'desc')->first();

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.create_form',['id' => $form->id]),
        ]);
    }

}
