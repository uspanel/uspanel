<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\EmailGroup;

class EmailGroupTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test Email Group functionality
     *
     * @return void
     */
    public function testIndex()
    {
        $this->seed(\EmailGroupTableSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();

        $response = $this->actingAs($user)->ajax('get', '/admin/email-groups');
        $response->assertStatus(200)
        ->assertJson([
            'data' => [
                'current_page' => true,
                'data' => [[
                    'id' => true,
                    'name' => true,
                    'resource_url' => true
                ]],
                'first_page_url' => true,
                'from' => true,
                'last_page' => true,
                'last_page_url' => true,
                'path' => true,
                'per_page' => true,
                'to' => true,
                'total' => true
            ]
        ])
        ->assertJsonCount(10, 'data.data');
    }

    /**
     * Test store Email Group functionality
     *
     * @return void
     */
    public function testStore()
    {
        $user = User::Role('Administrator')->firstOrFail();

        $data = [
            'name' => 'Test Email Group',
        ];

        $response = $this->actingAs($user)->ajax('post', '/admin/email-groups', $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('email_groups', $data);

        $emailGroup = EmailGroup::orderBy('id', 'desc')->first();

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.create_emailGroup',['id' => $emailGroup->id]),
        ]);
    }

    /**
     * Test Update Email Group functionality
     *
     * @return void
     */
    public function testUpdate()
    {
        $this->seed(\EmailGroupTableSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();
        $emailGroup = EmailGroup::firstOrFail();

        $data = [
            'name' => 'Test Update Email Group',
        ];
        $response = $this->actingAs($user)->ajax('post', '/admin/email-groups/' . $emailGroup->id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('email_groups', $data);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.updated_emailGroup',['id' => $emailGroup->id]),
        ]);
    }

    /**
     * Test Update Email Group functionality
     *
     * @return void
     */
    public function testDestroy()
    {
        $this->seed(\EmailGroupTableSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();
        $emailGroup = EmailGroup::firstOrFail();

        $response = $this->actingAs($user)->ajax('delete', '/admin/email-groups/' . $emailGroup->id);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('email_groups', [
            'id' => $emailGroup->id,
            'deleted_at' => null
        ]);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.deleted_emailGroup',['id' => $emailGroup->id]),
        ]);
    }
}
