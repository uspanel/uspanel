<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use CodersStudio\Chat\App\Chatroom;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ChatroomTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test Chatroom functionality
     *
     * @return void
     */
    public function testIndex()
    {
        $user = User::Role('Administrator')->firstOrFail();

        $data = [
            'chatroom' => [
                'name' => 'Chat 1',
                'description' => null
            ],
            'type' => 'private',
            'users' => [
                2
            ],
        ];

        $response = $this->actingAs($user)->ajax('post', '/chatrooms', $data);
        $response->assertStatus(201);

        $data = [
            'name' => 'Chat 1',
            'description' => null
        ];

        $this->assertDatabaseHas('chatrooms', $data);

        $chatroom = Chatroom::orderBy('id', 'desc')->first();

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.create_chatroom',['id' => $chatroom->id]),
        ]);

        $response = $this->actingAs($user)->ajax('get', '/chatrooms');
        $response->assertStatus(200)
        ->assertJson([
            'data' => [[
                'type' => true,
                'id' => true,
                'attributes' => [
                    'name' => true,
                    'type' => true,
                    'creator_id' => true,
                ]
            ]]
        ])
        ->assertJsonCount(1, 'data');

        $chatroom = Chatroom::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)->ajax('delete', '/chatrooms/' . $chatroom->id);
        $response->assertStatus(204);
        $this->assertDatabaseMissing('chatrooms', [
            'id' => $chatroom->id,
            'deleted_at' => null
        ]);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.deleted_chatroom',['id' => $chatroom->id]),
        ]);
    }
}
