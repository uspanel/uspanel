<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AdminProfileTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test Profile functionality
     *
     * @return void
     */
    public function testUpdateProfile()
    {
        $this->seed(\EmployeeStaffSeed::class);
        $user = User::orderBy('id', 'desc')->first();

        $data = [
            'first_name' => 'First name test',
            'last_name' => 'Last name test',
            'phone_number' => '11111111111',
        ];

        $response = $this->actingAs($user)->ajax('post', '/admin/profile', $data);
        $response->assertStatus(200);

        $this->assertDatabaseHas('users', $data);
    }
}
