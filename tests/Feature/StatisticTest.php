<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StatisticTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test tasks list functionality
     *
     * @return void
     */
    public function testIndex()
    {
        $user = User::Role('Administrator')->firstOrFail();
        $response = $this->actingAs($user)->ajax('get', '/admin/statistics');
        $response->assertStatus(200)
        ->assertJson([
            'data' => [
                'mailings' => [],
                'userStatuses' => [
                    [
                        'id' => true,
                        'name' => true,
                        'title' => true,
                        'created_at' => true,
                        'updated_at' => true,
                        'employees' => []
                    ],
                ],
                'userTasks' => []
            ]
        ])->assertJsonCount(13, 'data.userStatuses');
    }
}
