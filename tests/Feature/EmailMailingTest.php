<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use App\EmailMailing;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\EmailGroup;

class EmailMailingTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test Email Mailing functionality
     *
     * @return void
     */
    public function testIndex()
    {
        $i = 1;
        while ($i <= 10){
            $this->seed(\MailingsSeeder::class);
            $i++;
        }

        $user = User::Role('Administrator')->firstOrFail();

        $response = $this->actingAs($user)->ajax('get', '/admin/email-mailings');
        $response->assertStatus(200)
        ->assertJson([
            'data' => [
                'current_page' => true,
                'data' => [[
                    'id' => true,
                    'name' => true,
                    'email' => true,
                    'email_group_id' => true,
                    'resource_url' => true,
                    'email_group' => [
                        'id' => true,
                        'name' => true,
                        'created_at' => true,
                        'updated_at' => true,
                        'resource_url' => true,
                    ],
                    'resource_url' => true
                ]],
                'first_page_url' => true,
                'from' => true,
                'last_page' => true,
                'last_page_url' => true,
                'path' => true,
                'per_page' => true,
                'to' => true,
                'total' => true
            ]
        ])
        ->assertJsonCount(10, 'data.data');
    }

    /**
     * Test store Email Mailing functionality
     *
     * @return void
     */
    public function testStore()
    {
        $this->seed(\EmailGroupTableSeeder::class);
        $email_group = EmailGroup::firstOrFail();
        $user = User::Role('Administrator')->firstOrFail();
        $data = [
            'name' => 'Test Name',
            'email' => 'test@test.gm',
            'email_group_id' => $email_group->id,
        ];

        $response = $this->actingAs($user)->ajax('post', '/admin/email-mailings', $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('email_mailings', $data);

        $emailMailing = EmailMailing::orderBy('id', 'desc')->first();

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.create_emailMailing',['id' => $emailMailing->id]),
        ]);
    }

    /**
     * Test Update Email Mailing functionality
     *
     * @return void
     */
    public function testUpdate()
    {
        $this->seed(\MailingsSeeder::class);

        $user = User::Role('Administrator')->firstOrFail();
        $emailMailing = EmailMailing::orderBy('id', 'desc')->first();

        $data = [
            'name' => 'Test Update Email Mailing',
        ];
        $response = $this->actingAs($user)->ajax('post', '/admin/email-mailings/' . $emailMailing->id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('email_mailings', $data);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.updated_emailMailing',['id' => $emailMailing->id]),
        ]);
    }

    /**
     * Test Update Email Mailing functionality
     *
     * @return void
     */
    public function testDestroy()
    {
        $this->seed(\MailingsSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();
        $emailMailing = EmailMailing::orderBy('id', 'desc')->first();

        $response = $this->actingAs($user)->ajax('delete', '/admin/email-mailings/' . $emailMailing->id);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('email_mailings', [
            'id' => $emailMailing->id
        ]);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.deleted_emailMailing',['id' => $emailMailing->id]),
        ]);
    }
}
