<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use App\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProfileTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test Profile functionality
     *
     * @return void
     */
    public function testIndex()
    {
        $this->seed(\EmployeeStaffSeed::class);
        $user = User::orderBy('id', 'desc')->first();

        $response = $this->actingAs($user)->ajax('get', '/profile');
        $response->assertStatus(302);
    }

    /**
     * Test Profile functionality
     *
     * @return void
     */
    public function testDocuments()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('users')->truncate();
        DB::table('employees')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        $this->seed(\EmployeeStaffSeed::class);
        $user = User::Role('Employee Staff')->first();

        $response = $this->actingAs($user)->ajax('get', '/employee/documents');
        $response->assertStatus(200)
        ->assertViewIs('admin.employee.index');
    }

    /**
     * Test Profile functionality
     *
     * @return void
     */
    public function testuploadMedia()
    {
        $user = User::orderBy('id', 'desc')->first();

        $file = new UploadedFile(
            'storage/test.txt',
            'test.txt',
            null,
            null,
            null,
            true
        );

        $data = ['file' => $file];

        $response = $this->actingAs($user)->json('post', 'upload', $data);

        $data = [
            'action' => 'add',
            'collection_name' => 'contract',
            'meta_data' => [
                'file_name' => "test.txt",
                'name' => "test.txt",
            ],
            'path' => $response->baseResponse->original['path']
        ];

        $response = $this->actingAs($user)->ajax('post', '/profile/media/contract', [$data]);
        $response->assertStatus(200);
    }
}
