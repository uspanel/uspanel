<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Project;

class ProjectTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testStoreUpdate()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('mailsettings')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        $this->seed(\EmployeeStaffSeed::class);
        $user = User::Role('Administrator')->firstOrFail();
        $hr = User::Role('HR Manager Staff')->firstOrFail();
        $support = User::Role('Support Staff')->firstOrFail();
        $data = [
            'company' => [
                'name' => 'company',
                'company_type_id' => 1,
                'city' => 'New York',
                'state_id' => '1',
                'address' => 'test',
                'zip' => '11111',
                'phone' => '+1 (111) 111-1111',
                'fax' => '+1 (111) 111-1111',
                'site' => 'test.com',
                'info_email' => 'test@test.com',
                'application' => 'test',
                'position_name' => 'test',
            ],
            'hr_percent' => [
                $hr->id => 100
            ],
            'support_percent'=> [
                $support->id => 100
            ],
            'manager_ids' => [$hr->id],
            'support_ids' => [$support->id],
            'name' => 'test',
            'salary' => 100,
            'form_ids' => [1],
            'mail_domain' => 'dummy.cst'
        ];
        $response = $this->actingAs($user)->ajax('POST', '/admin/projects', $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('projects', [
            'name' => 'test',
            'salary' => 100,
        ]);
        $this->assertDatabaseHas('companies', $data['company']);

        $project = Project::orderBy('id', 'desc')->first();
        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.create_project',['id' => $project->id]),
        ]);

        $data['name'] = 'tset';
        $data['salary'] = 200;
        $data['company']['name'] = 'ynapmoc';
        $project = Project::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)->ajax('POST', '/admin/projects/' . $project->id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('projects', [
            'name' => 'tset',
            'salary' => 200,
        ]);
        $this->assertDatabaseHas('companies', $data['company']);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.updated_project',['id' => $project->id]),
        ]);

        $response = $this->actingAs($user)->ajax('delete', '/admin/projects/' . $project->id);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('email_groups', [
            'id' => $project->id
        ]);
        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.deleted_project',['id' => $project->id]),
        ]);
    }

}
