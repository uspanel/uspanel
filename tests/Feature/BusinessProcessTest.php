<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Event;
use App\Action;
use App\User;
use App\BusinessProcess;
use App\BusinessProcessEvent;
use App\ActionBusinessProcessEvent;

class BusinessProcessTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testStore()
    {
        $user = User::Role('Administrator')->firstOrFail();
        $event = Event::firstOrFail();
        $action = Action::firstOrFail();
        $data = [
            'business_process_events' => [
                [
                    'action_business_process_events' => [
                        [
                            'action' => $action->toArray(),
                            'users' => [$user->toArray()],
                            'order' => 0,
                            'mail_template' => null,
                            'roles' => [
                                [
                                    'id' => 1,
                                    'name' => "Administrator"
                                ]
                            ]
                        ],
                    ],
                    'event' => $event->toArray()
                ],
            ],
            'title' => 'title',
            'active' => 1
        ];
        $response = $this->actingAs($user)->ajax('POST', '/admin/businessprocesses', $data);
        $response->assertStatus(200);
        $businessProcess = BusinessProcess::where('title', $data['title'])->firstOrFail();
        $this->assertDatabaseHas('business_processes', [
            'title' => $data['title']
        ]);
        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.create_businessProcess',['id' => $businessProcess->id]),
        ]);
        $businessProcessEvent = BusinessProcessEvent::where('business_process_id', $businessProcess->id)->where('event_id', $event->id)->firstOrFail();
        $this->assertDatabaseHas('business_process_events', [
            'business_process_id' => $businessProcess->id,
            'event_id' => $event->id,
        ]);
        $actionBusinessProcessEvent = ActionBusinessProcessEvent::where('action_id', $action->id)->where('business_process_event_id', $businessProcessEvent->id)->firstOrFail();
        $this->assertDatabaseHas('action_business_process_events', [
            'action_id' => $action->id,
            'business_process_event_id' => $businessProcessEvent->id,
        ]);
        $this->assertDatabaseHas('action_business_process_event_user', [
            'user_id' => $user->id,
            'action_business_process_event_id' => $actionBusinessProcessEvent->id,
        ]);
    }

    /**
     * Test Update Business Process functionality
     *
     * @return void
     */
    public function testUpdate()
    {
        $this->seed(\BusinessSeeder::class);
        $businessProcess = BusinessProcess::orderBy('id', 'desc')->first();

        $user = User::Role('Administrator')->firstOrFail();
        $event = Event::firstOrFail();
        $action = Action::firstOrFail();
        $data = [
            'business_process_events' => [
                [
                    'action_business_process_events' => [
                        [
                            'users' => [$user->toArray()],
                            'order' => 0,
                            'action' => $action->toArray(),
                            'mail_template' => null,
                            'roles' => [
                                [
                                    'id' => 1,
                                    'name' => "Administrator"
                                ]
                            ]
                        ],
                    ],
                    'event' => $event->toArray()
                ],
            ],
            'title' => 'test',
            'active' => 1
        ];

        $response = $this->actingAs($user)->ajax('post', '/admin/businessprocesses/' . $businessProcess->id, $data);
        $response->assertStatus(200);
        $data = [
            'user_id' => $user->id,
            'title' => 'test'
        ];
        $this->assertDatabaseHas('business_processes', $data);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.updated_businessProcess',['id' => $businessProcess->id])
        ]);
    }

    /**
     * Test Destroy Business Process functionality
     *
     * @return void
     */
    public function testDestroy()
    {
        $this->seed(\BusinessSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();
        $businessProcess = BusinessProcess::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)->ajax('delete', '/admin/businessprocesses/' . $businessProcess->id);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('business_processes', [
            'id' => $businessProcess->id,
            'deleted_at' => null
        ]);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.deleted_businessProcess',['id' => $businessProcess->id]),
        ]);
    }
}
