<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FileUploadTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUpload()
    {
        $user = User::Role('Administrator')->firstOrFail();

        $file = new UploadedFile(
            storage_path('path_test_help_video.mp4'),
            'path_test_help_video.mp4',
            null,
            null,
            null,
            true
        );

        $data = ['file' => $file];

        $response = $this->actingAs($user)->json('post', 'upload', $data);
        $response->assertStatus(200)
        ->assertJson([
            'path' => true,
        ])
        ->assertJsonCount(1);
    }
}
