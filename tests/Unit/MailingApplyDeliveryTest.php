<?php

namespace Tests\Unit;

use App\Mailing;
use App\MailingStatus;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MailingApplyDeliveryTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testApplyMailing()
    {
        Notification::fake();

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('mail_templates')->truncate();
        DB::table('mailings')->truncate();
        DB::table('mailing_sendings')->truncate();
        DB::table('email_groups')->truncate();
        DB::table('email_group_mailing')->truncate();
        DB::table('email_mailings')->truncate();
        DB::table('history_mailings')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        $this->seed(\MailTemplatesSeeder::class);
        $this->seed(\MailingsSeeder::class);

        $admin = User::find(1);
        $mailing = Mailing::first();

        $response = $this->actingAs($admin)->ajax('POST', '/admin/mailings/'.$mailing->id.'/apply');
        $response->assertStatus(204);

        $this->assertDatabaseHas('history_mailings', [
            'mailing_id' => $mailing->id,
            'mailing_status_id' => MailingStatus::where('name','=','new')->first()->id
        ]);
    }
}
