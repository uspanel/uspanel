<?php

namespace Tests\Unit;

use App\Carrier;
use App\Jobs\TrackingDhl;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Employee;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CarrierDhlTest extends TestCase
{

    use RefreshDatabase;

    /**
     * test dhl tracking
     *
     * @return void
     */
    public function testTrack()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('tasks')->truncate();
        DB::table('sales_tasks')->truncate();
        DB::table('packages')->truncate();
        DB::table('users')->truncate();
        DB::table('employees')->truncate();
        DB::table('packages')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        $this->seed(\EmployeeStaffSeed::class);
        $track = '4572276881';
        $carrier = Carrier::whereName('Dhl')->firstOrFail();
        $task = $carrier->tasks()->create([
            'track' => $track,
            'employee_status_id' => 1,
            'creator_id' => User::role('Administrator')->first()->id,
            'manager_id' => User::role('HR Manager Staff')->first()->id,
            'employee_id' => User::role('Employee Staff')->first()->employee->id,
        ]);
        $package = $carrier->packages()->create([
            'track' => $track,
            'name' => 1,
            'employee_id' => User::role('Employee Staff')->first()->employee->id,
            'creator_id' => User::role('Administrator')->first()->id,
            'manager_id' => User::role('HR Manager Staff')->first()->id,
        ]);
        $salestask = $carrier->salesTasks()->create([
            'track' => $track,
            'sales_status_id' => 1,
            'creator_id' => User::role('Administrator')->first()->id,
            'manager_id' => User::role('HR Manager Staff')->first()->id,
            'employee_id' => User::role('Employee Staff')->first()->employee->id,
        ]);
        TrackingDhl::dispatch();
        $this->assertDatabaseHas('activities', [
            'task_id' => $task->id,
            "title" => "Shipment picked up",
            "city" => "NAPLES-ITA",
            "date" => "2019-04-29 10:56:41",
        ]);
        $this->assertDatabaseHas('activities', [
            'package_id' => $package->id,
            "title" => "Shipment picked up",
            "city" => "NAPLES-ITA",
            "date" => "2019-04-29 10:56:41",
        ]);
        $this->assertDatabaseHas('activities', [
            'sales_task_id' => $salestask->id,
            "title" => "Shipment picked up",
            "city" => "NAPLES-ITA",
            "date" => "2019-04-29 10:56:41",
        ]);
        $this->assertDatabaseHas('tasks', [
            'id' => $task->id,
            'delivered_at' => '2019-04-30 11:50:36',
        ]);
        $this->assertDatabaseHas('sales_tasks', [
            'id' => $salestask->id,
            'delivered_at' => '2019-04-30 11:50:36',
        ]);
        $this->assertDatabaseHas('packages', [
            'id' => $package->id,
            'delivered_at' => '2019-04-30 11:50:36',
        ]);
    }
}
