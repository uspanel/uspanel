<?php

namespace Tests\Unit;

use App\Activity;
use App\Carrier;
use App\Jobs\TrackingFedex;
use App\User;
use App\Task;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CarrierFedexTest extends TestCase
{

    use RefreshDatabase;

    /**
     * test dhl tracking
     *
     * @return void
     */
    public function testTrack()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('tasks')->truncate();
        DB::table('sales_tasks')->truncate();
        DB::table('packages')->truncate();
        DB::table('users')->truncate();
        DB::table('employees')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        $this->seed(\EmployeeStaffSeed::class);
        $track = '122816215025810';
        $carrier = Carrier::whereName('Fedex')->firstOrFail();
        $task = $carrier->tasks()->create([
            'track' => $track,
            'employee_status_id' => 1,
            'creator_id' => User::role('Administrator')->first()->id,
            'manager_id' => User::role('HR Manager Staff')->first()->id,
            'employee_id' => User::role('Employee Staff')->first()->employee->id,
        ]);
        $package = $carrier->packages()->create([
            'track' => $track,
            'name' => 1,
            'employee_id' => User::role('Employee Staff')->first()->employee->id,
            'creator_id' => User::role('Administrator')->first()->id,
            'manager_id' => User::role('HR Manager Staff')->first()->id,
        ]);
        $salestask = $carrier->salesTasks()->create([
            'track' => $track,
            'sales_status_id' => 1,
            'creator_id' => User::role('Administrator')->first()->id,
            'manager_id' => User::role('HR Manager Staff')->first()->id,
            'employee_id' => User::role('Employee Staff')->first()->employee->id,
        ]);
        TrackingFedex::dispatch();
        $this->assertDatabaseHas('activities', [
            'task_id' => $task->id,
            "title" => "Delivered",
            "city" => "Norton",
            "date" => "2014-01-09 13:31:00",
        ]);
        $this->assertDatabaseHas('activities', [
            'sales_task_id' => $salestask->id,
            "title" => "Delivered",
            "city" => "Norton",
            "date" => "2014-01-09 13:31:00",
        ]);
        $this->assertDatabaseHas('activities', [
            'package_id' => $package->id,
            "title" => "Delivered",
            "city" => "Norton",
            "date" => "2014-01-09 13:31:00",
        ]);
        $this->assertDatabaseHas('tasks', [
            'id' => $task->id,
            'delivered_at' => '2014-01-09 13:31:00',
        ]);
        $this->assertDatabaseHas('sales_tasks', [
            'id' => $salestask->id,
            'delivered_at' => '2014-01-09 13:31:00',
        ]);
        $this->assertDatabaseHas('packages', [
            'id' => $package->id,
            'delivered_at' => '2014-01-09 13:31:00',
        ]);
    }

}
