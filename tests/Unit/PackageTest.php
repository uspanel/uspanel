<?php

namespace Tests\Unit;

use App\Package;
use App\User;
use App\Warehouse;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Employee;

class PackageTest extends TestCase
{

    use RefreshDatabase;

    /**
     * Test packages list functionality
     *
     * @return void
     */
    public function testIndex()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('packages')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        $this->seed(\EmployeeStaffSeed::class);
        $user = User::Role('Administrator')->firstOrFail();
        $response = $this->actingAs($user)->ajax('get', '/admin/packages');
        $response->assertStatus(200)
        ->assertJson([
            'data' => [
                'current_page' => true,
                'total' => true,
                'data' => [
                    [
                        'id' => true,
                        'creator_id' => true,
                        'manager_id' => true,
                        'status_id' => true,
                        'description' => true,
                        'creator' => [
                            'name' => true
                        ],
                        'manager' => [
                            'name' => true
                        ],
                        'status' => [
                            'title' => true
                        ],
                        'weight' => true,
                        'name' => true,
                        'comment' => true
                    ]
                ]
            ]
        ])->assertJsonCount(1, 'data.data');
    }

    /**
     * Test store package functionality
     *
     * @return void
     */
    public function testStore()
    {
        $this->seed(\EmployeeStaffSeed::class);
        $user = User::Role('Administrator')->firstOrFail();
        $data = [
            'carrier_id' => 1,
            'weight' => '12.5',
            'name' => 'Test package',
            'comment' => 'comment',
            'description' => 'description',
            'price' => '15.5',
            'color' => 'black',
            'link' => 'хттпс://гугол.ком/',
            'track' => '111',
            'employee_id' => Employee::firstOrFail()->id,
            'status_id' => 1,
            'item_ids' => [
                [
                    'name' => 'Name item',
                    'price' => '666',
                    'color' => 'red',
                    'size' => '5',
                    'link' => 'https://www.google.com',
                    'info' => 'Info item',
                    'user_id' => User::firstOrFail(),
                    'qty' => '1'
                ]
            ]
        ];
        $response = $this->actingAs($user)->ajax('post', '/admin/packages', $data);
        $response->assertStatus(200);
        $data = [
            'weight' => '12.5',
            'name' => 'Test package',
            'comment' => 'comment',
            'description' => 'description',
        ];
        $this->assertDatabaseHas('packages', $data);
        $package = Package::orderBy('id', 'desc')->first();
        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.create_package',['id' => $package->id]),
        ]);
    }

    /**
     * Test Update package functionality
     *
     * @return void
     */
    public function testUpdate()
    {
        $this->seed(\EmployeeStaffSeed::class);
        $package = Package::firstOrFail();
        $user = User::Role('Administrator')->firstOrFail();
        $data = [
            'carrier_id' => 2,
            'weight' => '12.5',
            'name' => 'Test package',
            'comment' => 'comment',
            'description' => 'description',
            'price' => '15.5',
            'color' => 'black',
            'status_id' => 1,
            'track' => 111,
            'link' => 'хттпс://гугол.ком/',
            'new_items' => [],
            'item_ids' => [
                [
                    'id' => 1,
                    'name' => 'Name item',
                    'price' => '666',
                    'color' => 'red',
                    'size' => '5',
                    'link' => 'https://www.google.com',
                    'info' => 'Info item',
                    'user_id' => User::firstOrFail(),
                    'qty' => '1'
                ]
            ]
        ];
        $response = $this->actingAs($user)->ajax('post', '/admin/packages/' . $package->id, $data);
        $response->assertStatus(200);
        $data = [
            'weight' => '12.5',
            'name' => 'Test package',
            'comment' => 'comment',
            'description' => 'description',
        ];
        $this->assertDatabaseHas('packages', $data);
        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.updated_package',['id' => $package->id]),
        ]);
    }

    /**
     * Test force delivery package functionality
     *
     * @return void
     */
    public function testForceDelivery()
    {
        $this->seed(\EmployeeStaffSeed::class);
        $package = Package::firstOrFail();
        $user = User::Role('Administrator')->firstOrFail();
        $employee = Employee::firstOrFail();
        $warehouse = Warehouse::firstOrFail();
        $warehouse->update(['user_id' => $employee->user->id]);
        $package->update(['employee_id' => $employee->id]);
        $response = $this->actingAs($user)->ajax('post', '/admin/packages/' . $package->id.'/forcedelivered');
        $response->assertStatus(204);
        $this->assertDatabaseHas('warehousables', [
            'warehousable_type' => 'App\Package',
            'warehousable_id' => $package->id,
            'warehouse_id' => $warehouse->id
        ]);
        $this->assertDatabaseHas('activities',[
            'title' => trans('admin.package.actions.manual_delivery')
        ]);
    }

    /**
     * Test Update package functionality
     *
     * @return void
     */
    public function testDestroy()
    {
        $this->seed(\EmployeeStaffSeed::class);
        $package = Package::firstOrFail();
        $user = User::Role('Administrator')->firstOrFail();
        $response = $this->actingAs($user)->ajax('delete', '/admin/packages/' . $package->id);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('packages', [
            'id' => $package->id,
            'deleted_at' => null
        ]);
        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.deleted_package',['id' => $package->id]),
        ]);
    }

}
