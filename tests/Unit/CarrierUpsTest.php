<?php

namespace Tests\Unit;

use App\Carrier;
use App\Jobs\TrackingUps;
use Illuminate\Support\Facades\DB;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CarrierUpsTest extends TestCase
{

    use RefreshDatabase;

    /**
     * test dhl tracking
     *
     * @return void
     */
    public function testTrack()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('tasks')->truncate();
        DB::table('sales_tasks')->truncate();
        DB::table('packages')->truncate();
        DB::table('users')->truncate();
        DB::table('employees')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        $this->seed(\EmployeeStaffSeed::class);
        $track = '1Z10W91X1235436628';
        $carrier = Carrier::whereName('Ups')->firstOrFail();
        $task = $carrier->tasks()->create([
            'track' => $track,
            'employee_status_id' => 1,
            'creator_id' => User::role('Administrator')->first()->id,
            'manager_id' => User::role('HR Manager Staff')->first()->id,
            'employee_id' => User::role('Employee Staff')->first()->employee->id,
        ]);
        $salesTask = $carrier->salesTasks()->create([
            'track' => $track,
            'employee_status_id' => 1,
            'creator_id' => User::role('Administrator')->first()->id,
            'manager_id' => User::role('HR Manager Staff')->first()->id,
            'employee_id' => User::role('Employee Staff')->first()->employee->id,
        ]);
        $package = $carrier->packages()->create([
            'track' => $track,
            'name' => 1,
            'employee_id' => User::role('Employee Staff')->first()->employee->id,
            'creator_id' => User::role('Administrator')->first()->id,
            'manager_id' => User::role('HR Manager Staff')->first()->id,
        ]);
        TrackingUps::dispatch();
        $this->assertDatabaseHas('activities', [
            'task_id' => $task->id,
            "title" => "Delivered",
            "city" => "WARSZAWA",
            "date" => "2019-03-06 09:53:06",
        ]);
        $this->assertDatabaseHas('activities', [
            'sales_task_id' => $salesTask->id,
            "title" => "Delivered",
            "city" => "WARSZAWA",
            "date" => "2019-03-06 09:53:06",
        ]);
        $this->assertDatabaseHas('activities', [
            'task_id' => $package->id,
            "title" => "Delivered",
            "city" => "WARSZAWA",
            "date" => "2019-03-06 09:53:06",
        ]);
        $this->assertDatabaseHas('tasks', [
            'id' => $task->id,
            'delivered_at' => '2019-03-06 00:00:00',
        ]);
        $this->assertDatabaseHas('sales_tasks', [
            'id' => $salesTask->id,
            'delivered_at' => '2019-03-06 00:00:00',
        ]);
        $this->assertDatabaseHas('packages', [
            'id' => $package->id,
            'delivered_at' => '2019-03-06 00:00:00',
        ]);
    }
}
