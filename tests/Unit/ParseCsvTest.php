<?php

namespace Tests\Unit;

use App\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ParseCsvTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testParseCsv()
    {
        $admin = User::find(1);
        $stub = storage_path('1.csv');
        $name = '1.csv';
        $path = sys_get_temp_dir().'/'.$name;

        copy($stub, $path);
        $file = new UploadedFile($path, $name, 'text/csv', null, true);

        $response = $this->actingAs($admin)->ajax('POST', '/admin/email-groups/csv',['list' => $file, 'group_name' => 'New Group']);

        $response->assertStatus(302);

        $this->assertDatabaseHas('email_groups',[
            'name' => 'New Group'
        ]);

        $this->assertDatabaseHas('email_mailings',[
            'name' => 'user1',
            'email' => 'email1@email.com'
        ]);
    }
}
