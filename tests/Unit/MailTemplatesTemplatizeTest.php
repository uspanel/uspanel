<?php

namespace Tests\Unit;

use App\MailTemplate;
use App\User;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MailTemplatesTemplatizeTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testTemplatize()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('mail_templates')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        $this->seed('MailTemplatesSeeder');
        $template = MailTemplate::first();

        $user = User::role(['Administrator'])->first();

        $response = $this->actingAs($user)->ajax('get', '/admin/mail-templates/'.$template->id.'/test');
        $response->assertStatus(200);
        $response->assertJson([
            'data'=>[
                'attributes' => [
                    'templatized' => '<h1>Hello, '.$user->first_name.' '.$user->last_name.'! Your name is [user_name] and email is '.$user->email.'</h1><p>Static value is <a href="sometning" >something</a></p>'
                ]
            ]
        ]);
    }

    /**
     * Test Mail Template functionality
     *
     * @return void
     */
    public function testIndex()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('mail_templates')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        $this->seed(\MailTemplatesSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();

        $response = $this->actingAs($user)->ajax('get', '/admin/mail-templates');
        $response->assertStatus(200)
        ->assertJson([
            'data' => [
                'current_page' => true,
                'data' => [[
                    'id' => true,
                    'title' => true,
                    'body' => true,
                    'deletable' => true,
                    'resource_url' => true
                ]],
                'first_page_url' => true,
                'from' => true,
                'last_page' => true,
                'last_page_url' => true,
                'path' => true,
                'per_page' => true,
                'to' => true,
                'total' => true
            ]
        ])
        ->assertJsonCount(1, 'data.data');
    }

    /**
     * Test store Mail Template functionality
     *
     * @return void
     */
    public function testStore()
    {
        $user = User::Role('Administrator')->firstOrFail();

        $data = [
            'title' => 'Test Mail Template',
            'body' => 'Test',
            'subject' => 'Theme',
        ];

        $response = $this->actingAs($user)->ajax('post', '/admin/mail-templates', $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('mail_templates', $data);

        $mailTemplate = MailTemplate::orderBy('id', 'desc')->first();

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.create_mailTemplate',['id' => $mailTemplate->id]),
        ]);
    }

    /**
     * Test Update Mail Template functionality
     *
     * @return void
     */
    public function testUpdate()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('mail_templates')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        $this->seed(\MailTemplatesSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();
        $mailTemplate = MailTemplate::firstOrFail();

        $data = [
            'title' => 'Test Update Mail Templates',
            'body' => 'Test',
            'subject' => 'Theme',
        ];

        $response = $this->actingAs($user)->ajax('post', '/admin/mail-templates/' . $mailTemplate->id, $data);
        $response->assertStatus(200);
        $this->assertDatabaseHas('mail_templates', $data);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.updated_mailTemplate',['id' => $mailTemplate->id]),
        ]);
    }

    /**
     * Test Update Mail Template functionality
     *
     * @return void
     */
    public function testDestroy()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('mail_templates')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
        $this->seed(\MailTemplatesSeeder::class);
        $user = User::Role('Administrator')->firstOrFail();
        $mailTemplate = MailTemplate::orderBy('id', 'desc')->first();

        $response = $this->actingAs($user)->ajax('delete', '/admin/mail-templates/' . $mailTemplate->id);
        $response->assertStatus(200);
        $this->assertDatabaseMissing('mail_templates', [
            'id' => $mailTemplate->id
        ]);

        $this->assertDatabaseHas('activity_log',[
            'causer_id' => $user->id,
            'description' => trans('history.actions.deleted_mailTemplate',['id' => $mailTemplate->id]),
        ]);
    }
}
