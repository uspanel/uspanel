<?php

namespace Tests\Unit;

use App\HistoryMailing;
use App\Jobs\MailingsDelivery;
use App\Mailing;
use App\MailingSending;
use App\MailingStatus;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MailingRedeliveryTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testMailingRedelivery()
    {
        Notification::fake();

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('mail_templates')->truncate();
        DB::table('mailings')->truncate();
        DB::table('mailing_sendings')->truncate();
        DB::table('email_groups')->truncate();
        DB::table('email_group_mailing')->truncate();
        DB::table('email_mailings')->truncate();
        DB::table('history_mailings')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        $this->seed(\MailTemplatesSeeder::class);
        $this->seed(\HistoryMailingSeeder::class);

        MailingsDelivery::dispatchNow();

        $hm =  HistoryMailing::find(1);
        $hm2 =  HistoryMailing::find(2);

        $hm->update(['mailing_status_id' => 3]);

        $user = User::find(1);
        $mailing = Mailing::first();
        $response = $this->actingAs($user)->ajax('post', '/admin/mailings/'.$mailing->id.'/resend',['scheduled_at' => Carbon::now()->toDateString()]);
        $response->assertStatus(204);

        $lastMs = MailingSending::orderBy('id','desc')->first();

        $this->assertDatabaseMissing('history_mailings', ['email_mailing_id' => $hm->email_mailing_id, 'mailing_sending_id' => $lastMs->id]);
        $this->assertDatabaseHas('history_mailings', ['email_mailing_id' => $hm2->email_mailing_id, 'mailing_sending_id' => $lastMs->id]);

    }
}
