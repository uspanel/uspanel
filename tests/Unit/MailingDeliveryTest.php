<?php

namespace Tests\Unit;

use App\Jobs\MailingsDelivery;
use App\MailingStatus;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MailingDeliveryTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testMailingDelivery()
    {
        Notification::fake();

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('mail_templates')->truncate();
        DB::table('mailings')->truncate();
        DB::table('mailing_sendings')->truncate();
        DB::table('email_groups')->truncate();
        DB::table('email_group_mailing')->truncate();
        DB::table('email_mailings')->truncate();
        DB::table('history_mailings')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        $this->seed(\MailTemplatesSeeder::class);
        $this->seed(\HistoryMailingSeeder::class);

        MailingsDelivery::dispatchNow();

        Notification::assertSentTo(
            [\App\EmailMailing::find(1)],
            \App\Notifications\TemplatizedMail::class
        );

        $this->assertDatabaseHas('history_mailings', [
            'mailing_status_id' => MailingStatus::where('name','=','sent')->first()->id
        ]);
    }
}
