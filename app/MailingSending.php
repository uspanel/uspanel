<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailingSending extends Model
{
    protected $guarded = ['id'];

    public function historyMailings()
    {
        return $this->hasMany(HistoryMailing::class);
    }
}
