<?php namespace App;

use App\{DeliveryStatus, EmployeeStatus, User};
use Illuminate\Database\Eloquent\{Model, SoftDeletes};
use Brackets\Media\HasMedia\{HasMediaCollections, HasMediaCollectionsTrait, HasMediaThumbsTrait};
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use App\Notifications\System;

class Task extends Model implements HasMediaCollections
{

    use HasMediaCollectionsTrait, HasMediaThumbsTrait, SoftDeletes;

    protected $fillable = [
        'warehouse_id',
        'date',
        'carrier_id',
        'track',
        'sku',
        'carrier_tracking_status',
        'employee_status_id',
        'comment',
        'creator_id',
        'manager_id',
        'employee_id',
        'delivered_at'
    ];

    protected $hidden = [

    ];

    protected $dates = [
        'created_at',
        'delivered_at',
    ];

    protected $casts = [
        'created_at' => 'datetime:m/d/y',
        'delivered_at' => 'datetime:m/d/y',
    ];

    protected $with = ['media'];


    public $timestamps = false;

    protected $appends = [
        'resource_url',
        'delivery_time',
        'track_url'
    ];

    /**
     * @return array|void
     * @throws \Brackets\Media\Exceptions\Collections\MediaCollectionAlreadyDefined
     */
    public function registerMediaCollections()
    {
        $this->addMediaCollection('label');
        $this->addMediaCollection('barcode');
        $this->addMediaCollection('receipt');
        $this->addMediaCollection('invoice');
        $this->addMediaCollection('content');
    }

    public function getDeliveryTimeAttribute()
    {
        return Carbon::parse($this->delivered_at)->diffInDays($this->created_at);
    }

    public function getItemIdsAttribute() {
        return $this->items->pluck('id');
    }

    public function getItemIdsWithPivotAttribute() {
        $ids = $this->items()->withPivot('qty')->get()->pluck('pivot.qty','id');
        return $ids;
    }


    /**
     * @param Media|null $media
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->autoRegisterThumb200();
    }

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/tasks/' . $this->getKey());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function carrier()
    {
        return $this->belongsTo('App\Carrier');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activities()
    {
        return $this->hasMany('App\Activity');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lastActivity()
    {
        return $this->hasOne('App\Activity')->latest('date');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Track package and save activities
     *
     * @return void
     */
    public function tracking()
    {
        $carrierClass = $this->carrier->getCarrierClass();
        $result = $carrierClass->tracking($this->track);
        if ($result['activities']) {
            $this->activities()->delete();
            foreach ($result['activities'] as $activity) {
                $this->activities()->updateOrCreate($activity);
            }
        }
        if ($result['delivered_at']) {
            $this->delivered_at = $result['delivered_at'];
            $this->save();
        }
    }

    /**
     * only undelivered tracks
     *
     * @param $query
     * @return mixed
     */
    public function scopeUndelivered($query)
    {
        return $query->whereNull('delivered_at');
    }

    /**
     * only undelivered tracks
     *
     * @param $query
     * @return mixed
     */
    public function scopeWithTrack($query)
    {
        return $query->whereNotNull('track')
            ->where('track', '!=', '');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function warehouse()
    {
        return $this->belongsTo('App\Warehouse');
    }

    public function employeeStatus()
    {
        return $this->hasOne(EmployeeStatus::class, 'id', 'employee_status_id');
    }

    public function deliveryStatus()
    {
        return $this->hasOne(DeliveryStatus::class, 'id', 'delivery_status_id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class)->withTrashed();
    }

    public function setDeliveryStatus($name)
    {
        return $this->update([
            'delivery_status_id' => DeliveryStatus::ofName($name)->first()->id
        ]);
    }

    public function setEmployeeStatus($name)
    {
        return $this->update([
            'employee_status_id' => EmployeeStatus::ofName($name)->first()->id
        ]);
    }


    /**
     * Set status to task and employee depending delivery conditions
     *
     */
    public function setConditionalStatus()
    {
        $oldTask = Task::find($this->id);
        $statusName = null;
        $difference = Carbon::parse($oldTask->created_at)->diffInDays(Carbon::now());
        if ($this->employeeStatus && $this->employeeStatus->name == 'got') {
            if ($difference > 2) {
                $statusName = 'bad';
            }
        } else if ($this->employeeStatus && $this->employeeStatus->name == 'sent' && $this->activities()->count()) {
            if ($difference > 2) {
                $statusName = 'slow';
            }
            if ($difference <= 2) {
                $statusName = 'medium';
            }
            if ($difference <= 1) {
                $statusName = 'fast';
            }
            if (Carbon::parse($oldTask->created_at)->diffInHours(Carbon::now()) <= 5) {
                $statusName = 'super_fast';
            }
        }
        if ($statusName) {
            $this->delivery_status_id = DeliveryStatus::ofName($statusName)->first()->id;
            if ($this->employee) {
                if ($avgStatusId = round($this->employee->tasks()->whereNotNull('delivery_status_id')->avg('delivery_status_id'))) {
                    $avgStatus = DeliveryStatus::find($avgStatusId);
                    if ($userStatus = UserStatus::where('name', '=', $avgStatus->name)->first()) {
                        $empl = $this->employee;
                        $empl->user_status_id = $userStatus->id;
                        $empl->save();
                    }
                }
            }
        }
    }

    public function getUsers() {
        $result = null;
        if ($this->employee) {
            $result[] = $this->employee->user;
        }
        if ($this->manager) {
            $result[] = $this->manager;
        }
        if ($this->creator) {
            $result[] = $this->creator;
        }
        if ($result) {
            $result = collect($result);
        }
        if ($this->employee) {
            $result = $result->merge($this->employee->project->users);
        }
        $result = $result->merge(User::role('Administrator')->get());
        return $result->unique('id');
    }

    public function notifyUsers($text = null) {
        if ($users = $this->getUsers()) {
            Notification::send($users, new System(
                trans('notifications.task_updated', [
                    'id' => $this->id
                ]),
                route('admin/tasks/edit', [
                    'task' => $this->id,
                ]),
                [
                    'task' => $this
                ]
            ));
        }
    }

    public function items() {
        return $this->BelongsToMany(Item::class)->withPivot(['qty']);
    }

    /**
     * Get direct tracking url
     *
     * @return string|string[]
     */
    public function getTrackUrlAttribute()
    {
        $carrier = $this->carrier;
        $url = '';
        if ($carrier && $carrier->track_url) {
            $url = str_replace('{tracknum}',$this->track,$carrier->track_url);
        }
        return $url;
    }
}
