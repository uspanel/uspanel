<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\State;

class Company extends Model
{


    protected $fillable = [
        "name",
        "company_type_id",
        "city",
        "state_id",
        "address",
        "zip",
        "phone",
        "second_phone",
        "fax",
        "site",
        "info_email",
        "control_panel",
        "position_name",
        "application",

    ];

    protected $hidden = [

    ];

    protected $dates = [
        "created_at",
        "updated_at",

    ];



    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute() {
        return url('/admin/companies/'.$this->getKey());
    }

    public function state() {
        return $this->hasOne(State::class, 'id', 'state_id');
    }

    /**
     * Company type relation. E.x. support or invest type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function companyType()
    {
        return $this->belongsTo('App\CompanyType');
    }

}
