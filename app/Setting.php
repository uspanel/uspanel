<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    
    
    protected $fillable = [
        "name",
        "value",
    
    ];
    
    protected $hidden = [
    
    ];
    
    protected $dates = [
        "created_at",
        "updated_at",
    
    ];
    
    
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute() {
        return url('/admin/settings/'.$this->getKey());
    }

    static function getSetting($name) {
        return self::where('name', $name)->first()->value ?? null;
    }
    
}
