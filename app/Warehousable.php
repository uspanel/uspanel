<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehousable extends Model
{
    
    
    protected $fillable = [
        "warehousable_type",
        "warehousable_id",
        "qty",
        "warehouse_id",
        "item_id",
    
    ];
    
    protected $hidden = [
    
    ];
    
    protected $dates = [
        "created_at",
        "updated_at",
    
    ];

    protected $casts = [
        'created_at' => 'datetime:m.d.Y H:i',
    ];
    
    
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute() {
        return url('/admin/warehousables/'.$this->getKey());
    }

    
}
