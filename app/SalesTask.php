<?php namespace App;

use Brackets\Media\HasMedia\{HasMediaCollections, HasMediaCollectionsTrait, HasMediaThumbsTrait};
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesTask extends Model implements HasMediaCollections
{
    use HasMediaCollectionsTrait, HasMediaThumbsTrait, SoftDeletes;


    protected $fillable = [
        "delivered_at",
        "warehouse_id",
        "carrier_id",
        "track",
        "track",
        "sku",
        "employee_status_id",
        "comment",
        "creator_id",
        "manager_id",
        "employee_id",
        "sales_status_id",

    ];

    protected $hidden = [

    ];

    protected $dates = [
        "delivered_at",
        "deleted_at",
        "created_at",
        "updated_at",

    ];



    protected $appends = ['resource_url','track_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute() {
        return url('/admin/sales-tasks/'.$this->getKey());
    }

    /**
     * @return array|void
     * @throws \Brackets\Media\Exceptions\Collections\MediaCollectionAlreadyDefined
     */
    public function registerMediaCollections()
    {
        $this->addMediaCollection('label');
        $this->addMediaCollection('barcode');
        $this->addMediaCollection('receipt');
        $this->addMediaCollection('invoice');
        $this->addMediaCollection('content');
    }

    /**
     * @param Media|null $media
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->autoRegisterThumb200();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function carrier()
    {
        return $this->belongsTo('App\Carrier');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function warehouse()
    {
        return $this->belongsTo('App\Warehouse');
    }

     /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo('App\Employee')->withTrashed();
    }

    public function items()
    {
        return $this->belongsToMany(Item::class)->withPivot(['price','qty']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo('App\User');
    }

    public function salesStatus()
    {
        return $this->belongsTo(SalesStatus::class);
    }

    /**
     * only undelivered tracks
     *
     * @param $query
     * @return mixed
     */
    public function scopeUndelivered($query)
    {
        return $query->whereNull('delivered_at');
    }

    /**
     * only undelivered tracks
     *
     * @param $query
     * @return mixed
     */
    public function scopeWithTrack($query)
    {
        return $query->whereNotNull('track')
            ->where('track', '!=', '');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activities()
    {
        return $this->hasMany('App\Activity');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lastActivity()
    {
        return $this->hasOne('App\Activity')->latest('date');
    }

    /**
     * Get direct tracking url
     *
     * @return string|string[]
     */
    public function getTrackUrlAttribute()
    {
        $carrier = $this->carrier;
        $url = '';
        if ($carrier && $carrier->track_url) {
            $url = str_replace('{tracknum}',$this->track,$carrier->track_url);
        }
        return $url;
    }
}
