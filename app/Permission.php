<?php

namespace App;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use \Spatie\Permission\Models\Permission as SpatiePermission;

class Permission extends SpatiePermission
{

    /**
     * @param Collection $permissions
     * @return array
     */
    public static function convertPermissionsToArray(Collection $permissions): array
    {
        $data = [];
        foreach($permissions->pluck('name', 'id') as $id => $permission){
            Arr::set($data, $permission, $id);
        }
        return $data;
    }

}
