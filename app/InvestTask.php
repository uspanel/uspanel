<?php

namespace App;

use App\Notifications\System;
use Brackets\Media\HasMedia\{
    HasMediaCollections,
    HasMediaCollectionsTrait,
    HasMediaThumbsTrait,
    AutoProcessMediaTrait
};
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\{
    SoftDeletes,
    Model
};
use Illuminate\Support\Facades\Notification;

/**
 * Class InvestTask
 * @package App
 */
class InvestTask extends Model implements HasMediaCollections
{
    use HasMediaCollectionsTrait, HasMediaThumbsTrait, SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = [
        "name",
        "theme",
        "text",
        "deadline",
        "manager_id",
        "investor_id",
        "creator_id",
        "contragent_id",
        "employee_id",
        "creator_id",
        "invest_task_status_id",
        "investorList",
        "contragentList",
        "mail_template_id",
        "deleted_at"
    ];

    /**
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * @var array
     */
    protected $dates = [
        "deleted_at",
        "created_at",
        "updated_at",

    ];

    /**
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime:m-d-y H:i:s',
        'delivered_at' => 'datetime:m/d/y',
    ];

    /**
     * @var array
     */
    protected $appends = ['resource_url', 'templatized_text', 'time_left'];

    /**
     * @return array|void
     * @throws \Brackets\Media\Exceptions\Collections\MediaCollectionAlreadyDefined
     */
    public function registerMediaCollections()
    {
        $this->addMediaCollection('files')
            ->disk('public')
            ->maxNumberOfFiles(10);
    }

    /**
     * @param Media|null $media
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->autoRegisterThumb200();
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = parent::toArray();

        if ($this->media) {
            $data['media'] = $this->media->map(function ($item) {
                $item->url = $item->getUrl();
                return $item;
            });
        }
        return $data;
    }

    /**
     * Employee relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo('App\Employee')->withTrashed();
    }

    /**
     * Manager relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo('App\User');
    }


    /**
     * Creator relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo('App\User');
    }


    /**
     * Investor relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investors()
    {
        return $this->belongsToMany('App\User', 'invest_task_investor', 'invest_task_id', 'investor_id');
    }


    /**
     * Contragent relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contragents()
    {
        return $this->belongsToMany('App\Contragent');
    }


    /**
     * Investor List relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investorLists()
    {
        return $this->belongsToMany('App\InvestorList');
    }


    /**
     * Contragent List relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contragentLists()
    {
        return $this->belongsToMany('App\ContragentList');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investTaskStatus()
    {
        return $this->belongsTo(InvestTaskStatus::class);
    }


    /**
     * @return \Illuminate\Support\Collection
     */
    public function getUsers() {
        $result = null;
        if ($this->employee || $this->employee) {
            $result[] = $this->employee->user;
        }
        if ($this->manager) {
            $result[] = $this->manager;
        }
        if ($this->creator) {
            $result[] = $this->creator;
        }
        if ($result) {
            $result = collect($result);
        } else {
            $result = collect();
        }
        if ($this->employee || $this->employee) {
            $result = $result->merge($this->employee->project->users);
        }
        $result = $result->merge(User::role('Administrator')->get());
        return $result->unique('id');
    }


    /**
     * @param null $text
     */
    public function notifyUsers($text = null) {
        if ($users = $this->getUsers()) {
            Notification::send($users, new System(
                trans('notifications.invest_task_updated', [
                    'id' => $this->id
                ]),
                route('admin/invest-tasks/edit', [
                    'task' => $this->id,
                ]),
                [
                    'task' => $this
                ]
            ));
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mailTemplate()
    {
        return $this->belongsTo(MailTemplate::class);
    }
    /* ************************ ACCESSOR ************************* */

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getResourceUrlAttribute() {
        return url('/admin/invest-tasks/'.$this->getKey());
    }

    /**
     * Accessor to get Time left
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getTimeLeftAttribute() {
        $result = null;
        if ($this->deadline) {
            return $result = \Carbon\Carbon::parse(\Carbon\Carbon::parse($this->deadline))->diffForHumans(\Carbon\Carbon::now());
        }
    }

    /**
     * Accessor to get templatized text if auth user is recipient
     * @return mixed|string
     */
    public function getTemplatizedTextAttribute() : string
    {
        if (Auth::check() && Auth::user()->employee &&  Auth::user()->employee->id == $this->employee_id) {
            return $this->getTemplatizedText(Auth::user());
        } else {
            return $this->text ?: '';
        }
    }

    /**
     * Get templatized task text
     * @param User $recipient
     * @return string
     */
    public function getTemplatizedText(User $recipient): string
    {
        $template = $this->mailTemplate;
        $template->body = $this->text ?: '';
        $tmplData = ['recipient' => $recipient];
        if ($recipient->employee) {
            $tmplData['project'] = $recipient->employee->project;
        }
        return $template->getTemplatized($tmplData);
    }

    /**
     * @return mixed
     */
    public function getUniqueContragents()
    {
        $this->load(['contragents', 'contragentLists', 'contragentLists.contragents']);
        $contragents = $this->contragents;
        foreach ($this->contragentLists as $list) {
            $contragents->merge($list->contragents);
        }

        return $contragents->unique('id');
    }

}
