<?php namespace App;

use Brackets\Media\HasMedia\HasMediaCollections;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Brackets\Media\HasMedia\AutoProcessMediaTrait;

class Contragent extends Model implements HasMediaCollections
{
    use HasMediaCollectionsTrait, HasMediaThumbsTrait;

    protected $fillable = [
        "name",
        "investing_amount",
        "user_id",
        "country",
        "roi",

    ];

    protected $hidden = [

    ];

    protected $dates = [
        "created_at",
        "updated_at",

    ];

    protected $appends = ['resource_url'];

    public function registerMediaCollections() {
        $this->addMediaCollection('file_collection')
            ->disk('public')
            ->maxNumberOfFiles(10);
    }

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute() {
        return url('/admin/contragents/'.$this->getKey());
    }

    public function toArray()
    {
        $data = parent::toArray();

        if ($this->media) {
            $data['media'] = $this->media->map(function ($item) {
                $item->url = $item->getUrl();
                return $item;
            });
        }
        return $data;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function businessProcesses() {
        return $this->belongsToMany(BusinessProcess::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function employees() {
        return $this->belongsToMany(Employee::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function investTasks() {
        return $this->belongsToMany(InvestTask::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function contragentLists() {
        return $this->belongsToMany(ContragentList::class);
    }
}
