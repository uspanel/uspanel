<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class ActivityLog extends \Spatie\Activitylog\Models\Activity
{
    public $guarded = [];

    /**
     * Polymorphic relation which references to stuff or invest project
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contragent()
    {
        return $this->belongsTo(Contragent::class);
    }

    public function causer(): MorphTo
    {
        return $this->morphTo();
    }
}
