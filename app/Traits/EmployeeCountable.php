<?php

namespace App\Traits;

use App\Employee;
use App\User;
use Illuminate\Database\Eloquent\Collection;

trait EmployeeCountable
{

    /**
     * Calculate id of HR to assign to new user
     *
     * @param Employee $employee
     * @return mixed
     */
    protected function getCountedHrId($employee)
    {
        $managers = $employee->project->managers;
        return $this->getId($managers, $employee, 'hr_id');
    }

    /**
     * Calculate id of Support to assign to new user
     *
     * @param Employee $employee
     * @return mixed
     */
    protected function getCountedSupportId($employee)
    {
        $supports = $employee->project->supports;
        return $this->getId($supports, $employee, 'support_id');
    }

    /**
     * @param Collection $collection Supports or HR's collection to choose one
     * @param Employee $employee
     * @param string $column Column to count ('support_id' || 'hr_id')
     * @return mixed
     */
    protected function getId($collection, $employee, $column)
    {
        $result = null;
        $count = Employee::where('project_id', '=', $employee->project_id)->count();
        foreach ($collection as &$item) {
            $item->empl_pct = (Employee::where('project_id', '=', $employee->project_id)
                        ->where($column, '=', $item->id)->count() / $count) * 100;
            $item->empl_pct_diff = $item->pivot->responsible_percent - $item->empl_pct;
        }
        if ($user = $collection->sortByDesc('empl_pct_diff')->first()) {
            $result = $user->id;
        }
        return $result;
    }

    /**
     * Reassign Employees in case HR or Support was deleted
     *
     * @param User $user Deleted HR or Support
     * @param string $column Column to count ('support_id' || 'hr_id')
     */
    protected function recountEmployees($user, $column)
    {
        Employee::where($column, '=', $user->id)->update([$column => null]);
        $employees = Employee::whereNull($column)->get();
        foreach ($employees as $employee) {
            if ($column == 'hr_id') {
                $employee->update([$column => $this->getCountedHrId($employee)]);
            }
            if ($column == 'support_id') {
                $employee->update([$column => $this->getCountedSupportId($employee)]);
            }
        }
    }
}
