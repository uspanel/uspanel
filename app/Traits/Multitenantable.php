<?php
namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Auth;

trait Multitenantable
{
    protected static function bootMultitenantable()
    {
        static::creating(function ($model) {
            if (Auth::check()) { 
                $model->created_by = Auth::id();
            }
        });

        static::addGlobalScope('created_by', function (Builder $builder) {
            if (Auth::check() && !Auth::user()->hasRole('Administrator')) {
                $builder->where('created_by', Auth::id());
            }
        });
    }
}
