<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Event;
use App\Action;
use App\BusinessProcessEvent;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessProcess extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $appends = ['resource_url'];

    public function getResourceUrlAttribute() {
        return url('/admin/businessprocesses/'.$this->getKey());
    }

    public function events() {
    	return $this->hasManyThrough(Event::class, BusinessProcessEvent::class, 'business_process_id', 'id');
    }

    public function businessProcessEvents() {
        return $this->hasMany(BusinessProcessEvent::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function contragents() {
        return $this->belongsToMany(Contragent::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function businessProcessType()
    {
        return $this->belongsTo(BusinessProcessType::class);
    }
}
