<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailingStatus extends Model
{
    public function scopeOfName($query, $name)
    {
        return $query->where('name', $name);
    }
}
