<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryStatus extends Model
{
    protected $guarded = ['id'];

    public $timestamps = false;
    
    public function scopeOfName($query, $name)
    {
        return $query->where('name', $name);
    }
}
