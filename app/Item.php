<?php

namespace App;

use Illuminate\Database\Eloquent\{Model, Builder};
use Auth;

class Item extends Model
{
    protected $fillable = [
        'size',
        'color',
        'info',
        'link',
        'user_id',
        'name',
        'weight',
    ];

    protected $appends = [
        'resource_url',
    ];

    public function user() {
    	return $this->belongsTo(User::class)->withTrashed();
    }

    public function getResourceUrlAttribute() {
        return url('/admin/items/'.$this->getKey());
    }

    public function packages() {
        return $this->belongsToMany(Package::class)->withPivot(['qty','price']);
    }

    public function tasks() {
        return $this->belongsToMany(Task::class)->withPivot(['qty','price']);
    }

    public function nomenclature()
    {
        return $this->belongsTo(Nomenclature::class);
    }

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('created_by', function (Builder $builder) {
            if (Auth::check() && (Auth::user()->hasRole('Employee Staff') || Auth::user()->hasRole('Employee Invest'))) {
                //$builder->where('user_id', '=', Auth::id());
            }
        });
    }

    public function warehousables()
    {
        return $this->hasMany('\App\Warehousable');
    }
}
