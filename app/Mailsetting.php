<?php

namespace App;

use Brackets\Media\HasMedia\HasMediaCollections;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;
use Illuminate\Database\Eloquent\Model;
use CodersStudio\ImapMailClient\Mailsetting as CodersStudioMailsetting;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\Media;


/**
 * Class Mailsetting
 */
class Mailsetting extends CodersStudioMailsetting  implements HasMediaCollections
{

    use HasMediaCollectionsTrait, HasMediaThumbsTrait;

    protected $appends = ['resource_url'];


    public function registerMediaCollections()
    {
        $this->addMediaCollection('csv')
//        ->accepts('text/csv')
        ->maxNumberOfFiles(20);
    }

    /**
     * @param Media|null $media
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->autoRegisterThumb200();
    }

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getResourceUrlAttribute()
    {
        return url('/admin/mailsettings/' . $this->getKey());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function autoAnswers()
    {
        return $this->hasMany(AutoAnswer::class);
    }

    /**
     * @param $query
     * @param string $login
     * @return mixed
     */
    public function scopeOfLogin($query, string $login)
    {
        return $query->where('login','=',$login);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function  user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class,'owner','id');
    }

    public function meilsettingNames()
    {
        return $this->hasMany(MeilsettingAdditionalName::class,'set_id','id');
    }

    public function meilsettingDomains()
    {
        return $this->hasMany(MeilsettingAdditionalDomain::class,'set_id','id');
    }
}
