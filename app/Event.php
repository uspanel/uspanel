<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Action;
use App\BusinessProcessEvent;
use Illuminate\Support\Collection;

class Event extends Model
{
    protected $guarded = ['id'];

    public function businessProcessEvents()
    {
        return $this->hasMany(BusinessProcessEvent::class);
    }

    /**
     * Get event by name and run specified actions
     *
     * @param string $name Event's name
     * @param array $data Some additional data if needed
     */
    static function doActions($name, $data = [])
    {
        $event = self::where('name', '=', $name)->firstOrFail();
        $event->doRelatedActions($data);
    }

    /**
     * Get event related actions and execute it
     *
     * @param array $data Some additional data
     */
    public function doRelatedActions($data)
    {
        $businessProcessType = $data['business_process_type'] ?? null; // common, invest, support
        $businessProcessType = $businessProcessType ? BusinessProcessType::where('name', $businessProcessType)->first() : null;

        $excludedBpEvents = $this
            ->businessProcessEvents()
            ->select('id')
            ->whereHas(
                'businessProcess.businessProcessType',
                function ($query) {
                    $query->where('name', 'invest');
                }
            )
            ->whereHas(
                'businessProcess',
                function ($query) {
                    $query->has('contragents', '=', 0);
                }
            )
            ->get();
        $excludedBpEvents = ($excludedBpEvents && ($excludedBpEvents instanceof Collection)) ? $excludedBpEvents : collect();
        $excludedBpEvents = $excludedBpEvents->pluck('id');

        $bpEvents = $this
            ->businessProcessEvents()
            ->whereHas(
                'businessProcess',
                function ($query) use ($businessProcessType) {
                    $query->where('active', '=', 1);
                    if ($businessProcessType) {
                        $query->where('business_process_type_id', $businessProcessType->id);
                    }
                }
            )
            ->get();

        $bpEvents = $bpEvents->filter(function ($bpEvent) use ($excludedBpEvents) {
            return $excludedBpEvents->search($bpEvent->id) === false;
        });

        foreach ($bpEvents as $bpEvent) {
            $abpes = $bpEvent->actionBusinessProcessEvents()->orderBy('position', 'asc')->get();
            foreach ($abpes as $abpe) {
                if ($abpe->action) {
                    $method = $abpe->action->name;
                    $method::run($abpe, $data);
                }
            }
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function businessProcessTypes()
    {
        return $this->belongsToMany(BusinessProcessType::class);
    }
}
