<?php


namespace App\Libs\Voicent;


use App\User;
use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;

class VoicentClient
{
    /**
     * Configuration array. See config/voicent.php
     * @var array|\Illuminate\Config\Repository|mixed
     */
    protected $config = [];

    /**
     * Guzzle client instance
     * @var Client|null
     */
    protected $client = null;

    /**
     * VoicentClient constructor.
     * @param string $useCase one of use cases: 'new_contact'
     */
    public function __construct(?string $useCase = null)
    {
        $this->config = config('voicent');
        if (!empty($useCase)) {
            $this->client = new Client(config('voicent.http_config.'.$useCase,[]));
        }
    }

    /**
     * Add new contact to voicent CRM to make phone survey
     * @param User $user
     * @return mixed|null
     */
    public function addContact(User $user)
    {
        $formParams = $this->config['auth'];
        $formParams['MOD'] = 'CUS';
        $formParams['action'] = 'add';
        $formParams['LEAD_SOURCE'] = 'api';
        $formParams['CONTACT_STATUS'] = 0;
        $formParams['OTHER_PHONE'] = $user->phone_number;
        $formParams['FIRST_NAME'] = $user->first_name;
        $formParams['LAST_NAME'] = $user->last_name;
        $query = [];
        $this->client->request('post','/crmwebform.php',
            [
                'allow_redirects'=>true,
                'on_stats'=>function (TransferStats $stats) use (&$query){
                    $query[] = $stats->getEffectiveUri()->getQuery();
                },
                'form_params' => $formParams
            ]);
        $voicentId = null;
        foreach ($query as $q) {
            $qa = explode('=',$q);
            if (!empty($qa[0]) && !empty($qa[1]) && $qa[0] == 'crmcusid') {
                $voicentId = $qa[1];
            }
        }
        return $voicentId;
    }

    /**
     * Get survey results data for co
     * @return mixed
     */
    public function getCampaignResult()
    {
        $weblogin = $this->config['web_login'];
        $client = new Client([
            'verify' => false,
            'cookies' => true,
            'allow_redirects'=>false,
            'headers' => [
                'Accept' => '*/*',
                'Content-Type' => 'application/x-www-form-urlencoded',
            ]
        ]);
        $client->request('get','https://voicent.com/hosted_crm_cloud_call_center.php');
        $client->request('post','https://www.voicentlive.com/signin',['form_params' => $weblogin]);
        $client->request('get','https://www.voicentlive.com/customer/dashboard.php');
        $client->request('get','http://74.208.161.5:8155/login');
        $client->request('post','http://74.208.161.5:8155/login', ['form_params' => [
            'admin_email' => $weblogin['email'],
            'agent_username' => 'admin',
            'agent_password' => $weblogin['uipass'],
            'f' => '/crm'
            ]
        ]);

        $client->request('get','http://74.208.161.5:8155/cdetail?campid='.$this->config['survey_campaign_id']);
        $response = $client->request('post','http://74.208.161.5:8155/cdetail?action=geten&view=-1&ctype=0',
            [
                'form_params' => [
                    'start' => 0,
                    'length' => 100000,
                    'enpid' => $this->config['survey_campaign_id']
                ]
            ]);

        $content = json_decode($response->getBody()->getContents(),true);
        return $content;
    }



}
