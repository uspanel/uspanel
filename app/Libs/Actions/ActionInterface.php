<?php


namespace App\Libs\Actions;


use App\ActionBusinessProcessEvent;

interface ActionInterface
{
    static function run(ActionBusinessProcessEvent $abpe, $data);
}
