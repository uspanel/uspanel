<?php


namespace App\Libs\Actions;


use App\ActionBusinessProcessEvent;
use App\Employee;
use App\Notifications\System;
use App\Traits\Actionable;
use App\User;

class NotifyByNotificationCenter implements ActionInterface
{
    use Actionable;

    static function run(ActionBusinessProcessEvent $abpe , $data)
    {
        if (!empty($data['message']) && !empty($data['link'])) {
            $users = $abpe->users;

            if(in_array($abpe->bpe->event_id, [9,14])){
                $employee = $data['user'];
                $users = User::whereIn('id', [$employee->hr_id, $employee->support_id])->get();
            }

            if($abpe->bpe->event_id == 5){
                $media = $data['media'];
                $media_model = $media->model;
                $employee_id = $media_model->employee_id;
                $employee = Employee::find($employee_id);
                $users = User::whereIn('id', [$employee->hr_id, $employee->support_id])->get();
            }
            if($abpe->bpe->event_id == 12){
                $employee = $data['user'];
                $users = User::whereIn('id', [$employee->hr_id, $employee->support_id])->get();
            }


            foreach ($users as $user) {
                $user->notify(new System($data['message'], $data['link']));
            }
        }
    }
}
