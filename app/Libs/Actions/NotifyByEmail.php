<?php


namespace App\Libs\Actions;


use App\ActionBusinessProcessEvent;
use App\Employee;
use App\Mailsetting;
use App\MailsettingForProject;
use App\MailTemplate;
use App\Notifications\TemplatizedMail;
use App\Project;
use App\Traits\Actionable;
use App\User;
use CodersStudio\ImapMailClient\Http\Requests\MailClientSendRequest;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

class NotifyByEmail implements ActionInterface
{
    use Actionable;

    static function run(ActionBusinessProcessEvent $abpe, $data)
    {
        $template = $abpe->mailTemplates()->first();
        $users = $abpe->users;


        if(in_array($abpe->bpe->event_id, [1,2,3])){
            $package = $data['package'];
            $employee = Employee::find($package->employee_id);
            $users = User::where('id',$employee->user_id)->get();
        }

        if(in_array($abpe->bpe->event_id, [6,8,11,13])){
            $user_emploee = $data['user'];
            $users = User::where('id', $user_emploee->id)->get();
        }
        if(in_array($abpe->bpe->event_id, [14,12])){
            $user_emploee = $data['user'];
            $users = User::where('id', $user_emploee->user_id)->get();
        }
        if($abpe->bpe->event_id == 5){
            $media = $data['media'];
            $media_model = $media->model;
            $employee_id = $media_model->employee_id;
            $employee = Employee::find($employee_id);
            $users = User::where('id', $employee->user_id)->get();
        }



        foreach ($users as $user) {

            if ($template) {
                $tmplArray = ['recipient' => $user];
                if ($user->employee) {
                    $tmplArray['project'] = $user->employee->project;
                    $tmplArray['manager'] = $user->employee->hr;

                    if(in_array($abpe->bpe->event_id, [1,2,3])) {
                        $package = $data['package'];
                        $tmplArray['package'] = $package;
                    }

                }
                $content = $template->getTemplatized($tmplArray);

                if ($user->employee) {

                    $project_id = $user->employee->project_id;
                }else{
                    $row = DB::table('project_user')->where('user_id', $user->id)->first();
                    if($row){
                        $project_id = $row->project_id;
                    }else{
                        $project_id = false;
                    }
                }


                if($project_id) {

                    $mailsetting_for_project = MailsettingForProject::where('project_id', $project_id)->first();
                    $mail = new PHPMailer(true);


                    try {
                        $mail->SMTPDebug = SMTP::DEBUG_OFF;
                        $mail->isSMTP();
                        $mail->Host       = $mailsetting_for_project->smtp_host;
                        $mail->SMTPAuth   = true;
                        $mail->Username   = $mailsetting_for_project->login;
                        $mail->Password   = 'demo1234';
                        $mail->SMTPAutoTLS = false;
                        $mail->Port       = 587;
                        $mail->setFrom($mailsetting_for_project->login);
                        $mail->addAddress($user->email);
                        $mail->Subject = 'support';
                        $mail->isHTML(true);
                        $mail->Body = $content;
                        $mail->AltBody = '';
                        $mail->send();
                    } catch (\Exception $e) {
                        return Response::json(['errors' => [$e->getMessage()]], 400);
                    }
//                    $setting = MailsettingForProject::where('project_id',$project_id)->first();
//
//                    $config = [
//                        'driver' => 'smtp',
//                        'host' => $setting->smtp_host,
//                        'port' => $setting->smtp_port,
//                        'from' => ['address' => $setting->login, 'name' => 'support'],
//                        'encryption' => $setting->smtp_encryption,
//                        'username' => $setting->login,
//                        'password' => $setting->plain_password,
//                        'sendmail' => '/usr/sbin/sendmail -bs',
//                        'pretend' => false,
//                    ];
//                    Config::set('mail', $config);
//
//                    Artisan::call("optimize1    as:clear");
//
//                    try {
//                        Mail::send([], [], function ($message) use ($user, $content) {
//                            $message->to($user->email)
//                                ->subject('info')
//                                ->setBody($content, 'text/html');
//                        });
//                    } catch (\Exception $e) {
//                        Log::error('Notification error: '.$e->getMessage());
//                    }
                }

                else{
                    $user->notify(new TemplatizedMail($template,$content));
                }
            }
        }
    }
}
