<?php

namespace App;

use App\Event;
use Illuminate\Database\Eloquent\Model;
use App\Action;
use App\User;
use App\ActionBusinessProcessEvent;

class BusinessProcessEvent extends Model
{
    protected $guarded = ['id'];

    public $timestamps = false;

    public function event() {
    	return $this->belongsTo(Event::class);
    }

    public function actions() {
    	return $this->hasManyThrough(Action::class, ActionBusinessProcessEvent::class, 'business_process_event_id', 'id');
    }

    public function actionBusinessProcessEvents() {
    	return $this->hasMany(ActionBusinessProcessEvent::class);
    }

    public function businessProcess()
    {
        return $this->belongsTo(BusinessProcess::class);
    }
}
