<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Employee;

class UserStatus extends Model
{
    protected $guarded = ['id'];

    public function scopeOfName($query, $name)
    {
        return $query->where('name', $name);
    }

    public function employees() {
    	return $this->hasMany(Employee::class)->withTrashed();
    }
}
