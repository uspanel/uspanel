<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Brackets\Media\HasMedia\HasMediaCollections;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Notification;
use App\Notifications\System;
use App\User;
use App\Item;

class Package extends Model implements HasMediaCollections
{

    use HasMediaCollectionsTrait, HasMediaThumbsTrait, SoftDeletes;

    protected $fillable = [
        'weight',
        'name',
        'manager_id',
        'comment',
        'status_id',
        'description',
        'price',
        'color',
        'link',
        'creator_id',
        'track',
        'carrier_id',
        'employee_id',
        'delivered_at'
    ];

    protected $with = ['media'];

    protected $hidden = [

    ];

    protected $dates = [

    ];


    public $timestamps = false;

    protected $appends = ['resource_url', 'invoice_url', 'track_url'];

    /**
     * @return array|void
     * @throws \Brackets\Media\Exceptions\Collections\MediaCollectionAlreadyDefined
     */
    public function registerMediaCollections()
    {
        $this->addMediaCollection('invoice')
            ->maxNumberOfFiles(4);
    }

    /**
     * @param Media|null $media
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->autoRegisterThumb200();
    }

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/packages/' . $this->getKey());
    }

    public function getItemIdsAttribute()
    {
        return $this->items->pluck('id');
    }

    /**
     * return invoice url
     *
     * @return string
     */
    public function getInvoiceUrlAttribute(): string
    {
        return $this->getFirstMediaUrl('invoice');
    }

    public function activities()
    {
        return $this->hasMany('App\Activity');
    }

    public function lastActivity()
    {
        return $this->hasOne('App\Activity')->latest('date');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo('App\Status');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo('App\User');
    }

    public function employee()
    {
        return $this->belongsTo('App\Employee')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo('App\User');
    }

    public function getUsers()
    {
        if ($this->manager) {
            $result[] = $this->manager;
        }
        $result[] = $this->creator;
        if ($this->employee) {
            $result[] = $this->employee->user;
        }
        $result = collect($result);
        $result = $result->merge(User::role('Administrator')->get());
        return $result;
    }

    public function notifyUsers($text = null)
    {

        Notification::send($this->getUsers(), new System(
            $text,
            url('admin/packages')
        ));
    }

    /**
     * only undelivered tracks
     *
     * @param $query
     * @return mixed
     */
    public function scopeUndelivered($query)
    {
        return $query->whereNull('delivered_at');
    }

    /**
     * only undelivered tracks
     *
     * @param $query
     * @return mixed
     */
    public function scopeWithTrack($query)
    {
        return $query->whereNotNull('track')
            ->where('track', '!=', '');
    }

    public function items()
    {
        return $this->belongsToMany(Item::class)->withPivot(['qty','price']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function carrier()
    {
        return $this->belongsTo(Carrier::class);
    }

    /**
     * Get direct tracking url
     *
     * @return string|string[]
     */
    public function getTrackUrlAttribute()
    {
        $carrier = $this->carrier;
        $url = '';
        if ($carrier && $carrier->track_url) {
            $url = str_replace('{tracknum}',$this->track,$carrier->track_url);
        }
        return $url;
    }
}
