<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class InvestorProfile
 * @package App
 */
class InvestorProfile extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'activity_years',
        'activity_time',
        'country',
        'comment',
        'manager_id'
    ];

    /**
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',

    ];

    /**
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * Get the User record associated with the Investor Profile.
     */
    public function users()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }
}
