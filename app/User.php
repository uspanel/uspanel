<?php

namespace App;

use App\Employee;
use App\Traits\Utilitable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use CodersStudio\Chat\App\Traits\Chatable;
use Illuminate\Support\Facades\Hash;
use CodersStudio\ImapMailClient\Traits\Mailclientable;
use Brackets\AdminAuth\Models\AdminUser;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Profile;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Brackets\Media\HasMedia\HasMediaCollections;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;
use App\Project;
use App\Task;
use App\Package;
use App\Salary;
use App\BusinessProcess;
use App\EmailGroup;
use App\Item;
use CodersStudio\FormCreator\Traits\ModelFormCreatable;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use App\Notifications\System;
use App\EmployeeStatus;

// class User extends AdminUser implements MustVerifyEmail, HasMediaCollections, HasMediaConversions
class User extends AdminUser implements MustVerifyEmail, HasMediaCollections, HasMediaConversions
{
    const FORM_ID = 1;

    use Notifiable;
    use Chatable;
    use Mailclientable;
    use HasRoles;
    use HasMediaCollectionsTrait;
    use HasMediaThumbsTrait;
    use ModelFormCreatable;
    use Utilitable;



    protected $fillable = [
        "email",
        "email_verified_at",
        "password",
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "receive_external_calls",
        "mailing_id",
    ];

    protected $hidden = [
        "password",
        "remember_token",
        'pivot'
    ];

    protected $dates = [
        "email_verified_at",
        "created_at",
        "updated_at",

    ];


    protected $appends = [
        'resource_url',
        'avatar_thumb_url',
        'tasks_count',
        'invest_tasks_count',
        'packages_count',
        'chat_id',
        'name',
        'approved',
        'restore_pass_link'
    ];

    protected $casts = [
        'created_at' => 'date:m.d.Y',
        'email_verified_at' => 'datetime',
    ];


    /* ************************ ACCESSOR ************************* */

    public function getRestorePassLinkAttribute()
    {
        return url('admin/password-reset');
    }

    public function setPhoneNumberAttribute($value)
    {
        $this->attributes['phone_number'] = $this->clearPhone($value);
    }

    public function getPhoneAttribute()
    {
        return $this->attributes['phone_number'];
    }

    public function getResourceUrlAttribute()
    {
        return url('/admin/users/' . $this->getKey());
    }

    public function getDeliveryTimeAvgAttribute()
    {
        return $this->tasks->avg('delivery_time');
    }

    public function getApprovedAttribute()
    {
        $result = true;
        $statuses = [
            'approved',
            'test',
            'first_pack',
            'bad',
            'slow',
            'medium',
            'fast',
            'super_fast',
        ];
        if ($this->hasRole('Employee Staff')) {
            $result = false;
            if ($this->employee) {
                $result = in_array($this->employee->userStatus->name, $statuses);
            }
        } elseif ($this->hasRole('Employee Invest')) {
            $result = false;
            if ($this->employee) {
                $result = in_array($this->employee->userStatus->name, $statuses);
            }
        }
        return $result;
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Return available fields
     *
     * @param string $name
     * @param string $type
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    public function getAvailableFields(string $name, string $type, $exclude = [])
    {


        return $this->getAllPermissions()
            ->map(
                function ($item) use ($name, $type, $exclude) {
                    preg_match('/crud.' . $name . '.columns.([^.]*).' . $type . '/', $item, $matches);
                    return !empty($matches[1]) && !in_array($matches[1], $exclude) ? $matches[1] : null;
                }
            )
            ->filter();
    }

    /**
     * Last user activity datetime accessor
     *
     * @return datetime|null
     */
    public function getLastActivityAttribute()
    {
        $result = null;
        $activities = $this->activityLogs()->orderBy('created_at', 'desc')->first();
        if ($activities) {
            $result = $activities->created_at;
        }
        return $result;
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('cv');
        $this->addMediaCollection('contract');
        $this->addMediaCollection('agreement');
        $this->addMediaCollection('avatar');
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class);
    }

    public function form()
    {
        if ($this->employee && $this->employee->project) {
            return $this->employee->project->forms()
                ->with(
                    [
                        'fields' => function ($query) {
                            $query->enabled();
                        },
                        'fields.fieldType'
                    ]
                );
        }
        return null;
    }

    // Something OLD and may be useless
    public function scopePermissions($query)
    {
        if (auth()->user()->hasAnyRole('HR Manager Staff', 'HR Manager Staff', 'Support Staff', 'Call support')) {
            return $query; //->role('Employee Staff')->whereIn('project_id', auth()->user()->projects->pluck('id'));
        }
        if (auth()->user()->hasAnyRole('Administrator')) {
            return $query;
        }
        return null;
    }

    public function getAvatarThumbUrlAttribute()
    {
        return $this->getFirstMediaUrl('avatar', 'thumb_150') ?: '/images/user.png';
    }

    /**
     * Employees for investor user role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function employees() {
        return $this->belongsToMany(Employee::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function employee()
    {
        return $this->hasOne(Employee::class, 'user_id', 'id')->withTrashed();
    }

    public function getTasksCountAttribute()
    {
        if ($this->employee) {
            return $this->employee->tasks()->count();
        }
        return 0;
    }

    public function getInvestTasksCountAttribute()
    {
        if ($this->employee) {
            return $this->employee->investTasks()->count();
        }
        return 0;
    }

    public function getProjectIdAttribute()
    {
        if ($this->employee) {
            return $this->employee->project_id;
        }
        if ($this->investorProfile) {
            return count($this->projects) ? $this->projects->pluck('id')[0] : null;
        }
        return $this->projects->pluck('id');
    }

    public function getPackagesCountAttribute()
    {
        if ($this->employee) {
            return $this->employee->packages()->count();
        }
        return 0;
    }

    public function getChatIdAttribute()
    {
        return $this->chatrooms()->whereHas(
                'users',
                function ($query) {
                    $query->where('user_id', auth()->id());
                }
            )->private()->first()->id ?? null;
    }

    public function salaries()
    {
        return $this->hasMany(Salary::class);
    }

    public function getRatingPosition()
    {
        DB::statement(DB::raw('SET @position = 0'));
        $sub = User::role('Employee Staff')
            ->leftJoin('tasks', 'tasks.employee_id', '=', 'users.id')
            ->selectRaw(
                '
            users.id,
            COUNT(tasks.id) as tasks_count,
            AVG(DATEDIFF(tasks.delivered_at, tasks.created_at)) as delivery_time_avg
        '
            )
            ->whereNotNull('tasks.delivered_at')
            ->where('tasks.employee_status_id', EmployeeStatus::ofName('sent')->first()->id ?? null)
            ->groupBy('users.id')
            ->orderBy('delivery_time_avg', 'tasks_count');
        return DB::table(DB::raw("({$sub->toSql()}) as tbl"))
                ->selectRaw(
                    '
            *,
            @position := @position + 1 AS position
        '
                )
                ->mergeBindings($sub->getQuery())
                ->get()
                ->where('id', $this->id)
                ->first()
                ->position ?? null;
    }

    /**
     * [setSalary description]
     * @param Task $task [description]
     * @return false|\Illuminate\Database\Eloquent\Model|void
     */
    public function setSalary(Task $task)
    {
        if (!$task->employee || !$this->employee) {
            return;
        }
        // find salary for this task
        $salary = $task->employee->user->salaries()->withTrashed()->where('task_id', $task->id)->first();
        // if salary exists and was deleted
        if ($salary) {
            // restore
            if ($salary->deleted_at) {
                return $salary->update(
                    [
                        'deleted_at' => null
                    ]
                );
            }
            return;
        }
        // create salary
        return $this->salaries()->save(
            new Salary(
                [
                    'project_id' => $this->employee->project_id,
                    'amount' => $this->employee->project->salary,
                    'task_id' => $task->id,
                ]
            )
        );
    }

    public function currentWeekSalary()
    {
        $carbon = new Carbon;
        return @ money_format(
            '$%i',
            $this->salaries()
                ->whereDate('created_at', '>=', $carbon->startOfWeek(Carbon::MONDAY)->toDateString())
                ->whereDate('created_at', '<=', $carbon->endOfWeek(Carbon::SUNDAY)->toDateString())
                ->sum('amount')
        );
    }

    public function businessProcesses()
    {
        return $this->hasMany(BusinessProcess::class);
    }

    public function emailGroups()
    {
        return $this->hasMany(EmailGroup::class, 'created_by', 'id');
    }

    public function notifyUsers($text)
    {
        Notification::send(
            self::role(['Administrator', 'HR Manager Staff', 'Support Staff'])->get(),
            new System(
                $text,
                route(
                    'admin/users/edit',
                    [
                        'user' => $this->id
                    ]
                )
            )
        );
    }

    public function localmailsettings()
    {
        return $this->hasMany('App\Mailsetting');
    }

    public function activityLogs()
    {
        return $this->morphMany(ActivityLog::class, 'causer');
    }

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function packages()
    {
        return $this->hasMany(Package::class, 'manager_id', 'id');
    }

    public function mailing()
    {
        return $this->belongsTo(\App\Mailing::class);
    }

    public function stafferGroup()
    {
        return $this->belongsToMany(Employee::class, 'employee_staffer', 'staffer_id', 'employee_id');
    }

    public function getEmployeesAttribute()
    {
        return $this->stafferGroup->pluck('id');
    }

    public function getRoleIdAttribute()
    {
        return $this->roles()->first()->id;
    }

    public function tasks()
    {
        return $this->hasManyThrough(\App\Task::class, \App\Employee::class);
    }

    public function warehouses()
    {
        return $this->hasMany(\App\Warehouse::class);
    }

    /**
     * Get the Investor Lists record associated with the User.
     */
    public function investorLists()
    {
        return $this->belongsToMany('App\InvestorList');
    }

    /**
     * Get the Investor Profile record associated with the User.
     */
    public function investorProfile()
    {
        return $this->hasOne('App\InvestorProfile');
    }

    /**
     * [getStaffersForSelect description]
     * @return [type] [description]
     */
    static function getRoleForSelect($role)
    {
        return self::role($role)->select(['id', 'first_name', 'last_name'])->get()->makeHidden(
            [
                'resource_url',
                'avatar_thumb_url',
                'tasks_count',
                'packages_count',
                'chat_id',
                'approved',
                'media',
                'employee',
                'roles',
                'first_name',
                'last_name'
            ]
        );
    }

    /**
     * [getEmployeesForSelect description]
     * @return [type] [description]
     */
    public function getEmployeesForSelect()
    {
        return Employee::
        leftJoin('users', 'users.id', '=', 'employees.user_id')
            ->select(['employees.id'])
            ->selectRaw('CONCAT(users.first_name, \' \', users.last_name) as name')
            ->whereIn('project_id', $this->projects->pluck('id'))
            ->get()
            ->makeHidden('sign_link');
    }

    /**
     * [getEmployeeStaffersForSelect description]
     * @return [type] [description]
     */
    public function getEmployeeRelationForSelect($relation)
    {
        $result = null;
        if ($this->employee) {
            $result = $this->employee->$relation()->select(['id', 'first_name', 'last_name'])->get()->makeHidden(
                [
                    'resource_url',
                    'avatar_thumb_url',
                    'tasks_count',
                    'packages_count',
                    'chat_id',
                    'approved',
                    'media',
                    'employee',
                    'roles',
                    'first_name',
                    'last_name'
                ]
            );
        }
        return $result;
    }

    /**
     * [getStafferEmployeesForSelect description]
     * @return [type] [description]
     */
    public function getStafferEmployeesForSelect()
    {
        return $this->stafferGroup()->
        leftJoin('users', 'users.id', '=', 'employees.user_id')
            ->select(['employees.id'])
            ->selectRaw('CONCAT(users.first_name, \' \', users.last_name) as name')
            ->whereIn('project_id', $this->projects->pluck('id'))
            ->get()
            ->makeHidden('sign_link');
    }

}
