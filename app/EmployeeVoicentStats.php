<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeVoicentStats extends Model
{
    protected $fillable = [
        'employee_id',
        'status',
        'start_time',
        'duration',
        'notes',
        'agent_recording',
        'credit',
        'transfer_credit',
        'tts_credit'
        ];
}
