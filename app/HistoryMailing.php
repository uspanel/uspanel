<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryMailing extends Model
{
    protected $guarded = ['id'];

    protected $casts = [
        'created_at' => 'datetime:m.d.Y',
    ];

    public function mailingStatus()
    {
        return $this->belongsTo(MailingStatus::class);
    }

    public function mailingSending()
    {
        return $this->belongsTo(MailingSending::class);
    }

    public function emailMailing()
    {
        return $this->belongsTo(EmailMailing::class);
    }

    public function mailing()
    {
        return $this->belongsTo(Mailing::class);
    }

    /**
     * Set mailing status by name
     * @param string $status Name of status
     */
    public function setStatus($status)
    {
        $status = MailingStatus::where('name', '=', $status)->firstOrFail();
        $this->update(['mailing_status_id' => $status->id]);
    }

    /**
     * Set 'registered' status to last sent mailing by email
     * @param string $email Email of registered user
     */
    static function register($email)
    {
        $historyMailing = self::whereHas('emailMailing', function ($query) use ($email) {
            $query->where('email', '=', $email);
        })->orderBy('mailing_sending_id', 'desc')->first();
        if ($historyMailing) {
            $historyMailing->setStatus('registered');
        }
        return $historyMailing;
    }
}
