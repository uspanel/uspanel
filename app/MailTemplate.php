<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MailTemplate extends Model
{


    protected $fillable = [
        "title",
        "body",
        "subject"
    ];

    protected $hidden = [

    ];

    protected $dates = [
        "created_at",
        "updated_at",

    ];

    /**
     * Array for templatizing variables. Keys are variables. Values are model properties,
     * accessors or relations (can be dot imploded ex. profile->state is profile.state)
     *
     * @var array
     */
    protected $variables = [
        'recipient' => [
            '[first_name]' => 'first_name',
            '[last_name]' => 'last_name',
            '[state]' => 'profile.state',
            '[address]' => 'profile.address',
            '[home_phone]' => 'profile.home_phone',
            '[work_phone]' => 'profile.work_phone',
            '[docs]' => 'docs_download_link', //Have to add manually to model
            '[zip]' => 'profile.zip',
//            '[sign_name]' => '',
//            '[signed_date]' => '',
//            '[signed]' => '',
            '[email]' => 'email',
            '[FullName]' => 'name',
//            '[investors_list]' => '',
            '[last_activity]' => 'last_activity',
//            '[inv_prj]' => '',
            '[sign_link]' => 'employee.sign_link',
            '[restore_pass_link]' => 'restore_pass_link', //Have to add manually to model
        ],
        'package' =>[
            '[status]' => 'lastActivity.title'
        ],
        'project' => [
            '[position]' => 'company.position_name',
            '[application]' => 'company.application',
            '[project_name]' => 'name',
//            '[project_link]' => '',
            '[control_panel_link]' => 'company.control_panel',
            '[company_name]' => 'company.name',
            '[site_link]' => 'company.site',
            '[sup_email]' => 'company.email',
            '[sup_name]' => 'company.name',
            '[company_address]' => 'company.address',
            '[company_city]' => 'company.city',
            '[company_state]' => 'company.state',
            '[company_zip]' => 'company.zip',
            '[company_phone1]' => 'company.phone',
            '[company_phone2]' => 'company.second_phone',
            '[company_fax]' => 'company.fax',
            '[company_type]' => 'company.companyType.title',
//            '[company_country]' => '', USA always??
//
        ],
        'manager' => [
            '[manager]' => 'name',
        ]
    ];



    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute() {
        return url('/admin/mail-templates/'.$this->getKey());
    }
    /***************************************************************/

    /**
     * Get template body with replacements
     *
     * @param $models array Array of models for replacement search with keys corresponding to $variables array
     *
     * @return string
     */
    public function getTemplatized($models){


        $body = $this->body;

        foreach ($this->variables as $model => $rules) {
            if (!empty($models[$model])) {
                foreach ($rules as $key => $val) {
                    $repl = $key;
                    $propArray = explode('.',$val);
                    if (array_key_exists($propArray[0], $models[$model]->attributes) || $models[$model]->hasGetMutator($propArray[0])) {
                        $repl = $models[$model]->{$propArray[0]};
                        if (is_array($repl)) {
                            $repl = implode(', ',$repl);
                        }
                    } else if(method_exists($models[$model],$propArray[0])) {
                        $repl = $models[$model]->{$propArray[0]}()->first();
                    }
                    foreach ($propArray as $levelkey => $level) {
                        if ($levelkey > 0 && $levelkey < count($propArray)-1 && count($propArray) > 1) {
                            if ($repl && is_object($repl)) {
                                $repl = $repl->{$level}()->first();
                            }
                        } else if ($levelkey == count($propArray)-1 && count($propArray) > 1) {
                            if ($repl && is_object($repl)) {
                                $repl = $repl->$level;
                                if (is_array($repl)) {
                                    $repl = implode(', ',$repl);
                                }
                            }
                        }
                    }
                    $body = str_replace($key,$repl,$body);
                }
            }
        }

        return $body;
    }

    /**
     * Get list of variable explanations
     *
     * @return array
     */
    public function getVariables()
    {
        $result = [];
        foreach ($this->variables as $key=>$rules) {
            foreach ($rules as $var => $repl) {
                $result[$var] = trans('admin.mail-template.vars.'.preg_replace("/[^a-zA-Z]/", "", $var));
            }
        }
        return $result;
    }


}
