<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\{
    User,
    Action,
    BusinessProcessEvent,
    MailTemplate
};

class ActionBusinessProcessEvent extends Model
{
    protected $guarded = ['id'];

    protected $appends = ['mail_template'];

    public $timestamps = false;

    public function users() {
    	return $this->belongsToMany(User::class)->withTrashed();
    }

    public function roles() {
        return $this->belongsToMany(\App\Role::class);
    }

    public function action() {
    	return $this->belongsTo(Action::class);
    }

    public function mailTemplates()
    {
        return $this->belongsToMany(MailTemplate::class);
    }

    public function bpe(){
        return $this->belongsTo(BusinessProcessEvent::class,'business_process_event_id', 'id');
    }


    public function getMailTemplateAttribute()
    {
        $result = $this->mailTemplates()->select('id', 'title')->first();
        if ($result) {
            $result = $result->makeHidden('pivot');
        }
        return $result;
    }
}
