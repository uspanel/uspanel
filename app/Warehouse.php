<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Auth;
use App\Employee;
use Illuminate\Database\Eloquent\SoftDeletes;


class Warehouse extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'city',
        'address',
        'user_id'
    ];

    protected $hidden = [

    ];

    protected $dates = [

    ];


    public $timestamps = false;

    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute() {
        return url('/admin/warehouses/'.$this->getKey());
    }

    /**
     *
     *
     * @return HasOne
     */
    public function tasks(){
        return $this->hasOne('App\Task');
    }

    public function morphTasks()
    {
        return $this->morphedByMany(Task::class, 'warehousable')->withPivot('qty');
    }

    public function morphPackages()
    {
        return $this->morphedByMany(Package::class, 'warehousable')->withPivot('qty');
    }

    public function morphSalesTasks()
    {
        return $this->morphedByMany(SalesTask::class, 'warehousable')->withPivot('qty');
    }

    public function user(){
        return $this->belongsTo('App\User')->withTrashed();
    }

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('created_by', function (Builder $builder) {
            if (Auth::check() && (Auth::user()->hasRole('Employee Staff') || Auth::user()->hasRole('Employee Invest'))) {
                $builder->where('user_id', '=', Auth::id());
            }
        });
    }

    public function items()
    {
        return $this->belongsToMany(Item::class,'warehousables')->withPivot(['qty','created_at','id','warehousable_type','warehousable_id']);
    }

    /**
     * Get available quantity of item for warehouse
     *
     * @param $id
     * @return mixed
     */
    public function getItemQty($id)
    {
        return $this->items()->where('item_id','=',$id)->sum('qty');
    }

    /**
     * Get warehouse overall items balance
     *
     * @return int
     */
    public function allItemsBalance() : int
    {
        return $this->items()->sum('qty');
    }

}
