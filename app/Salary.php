<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Project;
use Illuminate\Database\Eloquent\SoftDeletes;

class Salary extends Model
{
	use SoftDeletes;
	
    protected $guarded = ['id'];

    public function project() {
    	return $this->belongsTo(Project::class);
    }
}
