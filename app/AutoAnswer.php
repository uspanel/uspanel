<?php namespace App;

use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AutoAnswer
 */
class AutoAnswer extends Model
{
    use Multitenantable;

    protected $fillable = [
        "user_id",
        "mailsetting_id",

    ];

    protected $hidden = [

    ];

    protected $dates = [
        "created_at",
        "updated_at",

    ];


    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/auto-answers/' . $this->getKey());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class,'created_by','id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function mailTemplates()
    {
        return $this->belongsToMany(MailTemplate::class)->withPivot(['message_count','delay']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mailsetting()
    {
        return $this->belongsTo(Mailsetting::class);
    }
}
