<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityLogDeprecated extends Model
{
    protected $table = 'activity_log_deprecated';

    protected $fillable = [
        "user_id",
        "text",
        "ip_address",

    ];

    protected $hidden = [

    ];

    protected $dates = [
        "created_at",
        "updated_at",

    ];



    protected $appends = [
        'resource_url',
        'user_name',
    ];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute() {
        return url('/admin/activity-logs/'.$this->getKey());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getUserNameAttribute() {
        if(isset($this->user)){
            return $this->user->name;
        }
        return trans('history.actions.user_no_name');
    }

    public function user() {
        return $this->belongsTo(User::class)->withTrashed();
    }


}
