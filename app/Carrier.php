<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Exception;

class Carrier extends Model
{

    protected $fillable = ['name', 'title', 'track_url'];

    /**
     * return carrier class
     *
     * @return mixed
     * @throws Exception
     */
    public function getCarrierClass()
    {
        $className = 'App\Carriers\\' . $this->name;
        if(!class_exists($className)){
            throw new Exception('Carrier class not found');
        }
        return new $className();
    }

    public function tasks()
    {
        return $this->hasMany('App\Task');
    }

    public function salesTasks()
    {
        return $this->hasMany(SalesTask::class);
    }

    public function packages()
    {
        return $this->hasMany('App\Package');
    }

}
