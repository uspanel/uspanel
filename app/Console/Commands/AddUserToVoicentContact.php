<?php

namespace App\Console\Commands;

use App\Jobs\CreateVoicentContact;
use App\Setting;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class AddUserToVoicentContact extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voicent:addcontact {--user=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add user with given id to voicent contacts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::findOrFail($this->option('user'));
        if ($user->employee()->exists() && $user->phone_number){
            dispatch(new CreateVoicentContact($user))->delay(Carbon::now()->addMinutes(Setting::getSetting('	voicent_survey_interval_min') ?: 0));
        }
    }
}
