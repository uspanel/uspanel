<?php

namespace App\Console\Commands;

use App\ActivityLog;
use App\ActivityLogDeprecated;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Contracts\Activity;

class ImportActivityLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import activity log';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::transaction(function () {
            $oldLogs = ActivityLogDeprecated::all();

            foreach ($oldLogs as $index=>$oldLog) {
                $activity = activity('import')
                    ->withProperty('ip_address', $oldLog['ip_address'])
                    ->by($oldLog['user_id'])
                    ->log($oldLog['text']);
                DB::table('activity_log')->where('id', $activity->id)->update([
                    'created_at' => $oldLog['created_at'],
                    'updated_at' => $oldLog['updated_at'],
                ]);
            }
            DB::table('activity_log_deprecated')->update([
                'imported' => 1,
            ]);
        });
    }
}
