<?php

namespace App\Console\Commands;

use App\Jobs\SendAutoReply;
use App\Mailsetting;
use App\MailTemplate;
use CodersStudio\ImapMailClient\Mailmessage;
use Illuminate\Console\Command;

class MailAutoreply extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:autoreply {--mailsetting_id=} {--rcpt=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send auto reply email message if any configured for given mailsetting_id and recipient';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (
            Mailsetting::findOrFail($this->option('mailsetting_id'))->autoAnswers()->count() &&
            Mailsetting::ofLogin($this->option('rcpt'))->exists() &&
            Mailsetting::findOrFail($this->option('mailsetting_id'))->login != $this->option('rcpt')
        ) {
            $recipient = Mailsetting::ofLogin($this->option('rcpt'))->first();
            $mailsetting = Mailsetting::findOrFail($this->option('mailsetting_id'));
            $projects = $mailsetting->user->projects->pluck('id')->toArray();
            if (
                $recipient->user->employee &&
                in_array($recipient->user->employee->project_id, $projects)
            ) {
                $autoanswers = $mailsetting->autoAnswers()->whereHas('user', function ($query) use ($mailsetting) {
                    return $query->where('created_by','=',$mailsetting->user->id);
                })->get();
                $mCount = Mailmessage::where('mailsetting_id','=',$mailsetting->id)->where('sender_email','=',$recipient->login)->count();
                foreach ($autoanswers as $autoanswer) {
                    $template = $autoanswer->mailTemplates()->wherePivot('message_count','=',$mCount)->first();
                    if ($template) {
                        SendAutoReply::dispatch($recipient->login,$template->id, $mailsetting->id, $recipient->id)
                            ->delay(now()->addMinutes($template->pivot->delay));
                    }
                }
            }

        }
    }
}
