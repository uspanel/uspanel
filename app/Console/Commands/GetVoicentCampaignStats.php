<?php

namespace App\Console\Commands;

use App\Employee;
use App\Libs\Voicent\VoicentClient;
use Illuminate\Console\Command;

class GetVoicentCampaignStats extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'voicent:campstats';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get voicent campaign stats';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new VoicentClient();
        $content = $client->getCampaignResult();
        if (!is_array($content) || empty($content['data'])) {
            return null;
        }
        $stats = collect($content['data']);
        $employees = Employee::whereDoesntHave('voicentStats')->orWhereHas('voicentStats', function ($query) {
            $query->where('status','=','Not Contacted');
        })->get();
        foreach ($employees as $employee) {
            $stat = $stats->first(function ($item,$key) use($employee){
                return $item[2] == $employee->user->phone_number;
            });
            if ($stat) {
                $employee->voicentStats()->updateOrCreate([
                    'status' => $stat[3],
                    'start_time' => $stat[4],
                    'duration' => $stat[5],
                    'notes' => $stat[6],
                    'agent_recording' => $stat[7],
                    'credit' => $stat[8],
                    'transfer_credit' => $stat[9],
                    'tts_credit' => $stat[10]
                ]);
            }
        }
    }
}
