<?php

namespace App\Console\Commands;

use App\Notifications\NewEmailAccount;
use App\Project;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class CreateMailAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail_account:create {--user=} {--project=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create email account for selected user by id and project id';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$this->option('project')) {
            return null;
        }
        $user = User::findOrFail($this->option('user'));
        if ($user->hasRole('Investor')) {
            return null;
        }
        $project = Project::findOrFail($this->option('project'));
        if ($project->mail_domain && $user->roles()->first()) {
            dispatch(new \App\Jobs\CreateMailAccount($user, $project));
        } else {
            Log::error('Unable to register email for user ' . $user->id . ' in project ' . $project->id . '! Mail domain for project or user role is not set.');
        }

    }
}
