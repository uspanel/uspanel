<?php

namespace App\Console;

use App\Jobs\MailingsDelivery;
use App\Jobs\TrackingDhl;
use App\Jobs\TrackingFedex;
use App\Jobs\TrackingUps;
use App\Jobs\TrackingUsps;
use App\Jobs\ContractReminder;
use App\Jobs\TaskDocumentsNotUploaded;
use App\Jobs\CheckEmployeeDocuments;
use App\Jobs\CheckExpiredTasks;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\FlushRedis'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function(){
            TrackingUps::dispatch();
            TrackingFedex::dispatch();
            TrackingUsps::dispatch();
            TrackingDhl::dispatch();
        })->everyFifteenMinutes()->name('tracking')->withoutOverlapping();
        $schedule->call(function(){
            ContractReminder::dispatch();
            // TODO notifications 5
            TaskDocumentsNotUploaded::dispatch();
            // TODO notifications 7!
            CheckEmployeeDocuments::dispatch();
            // TODO notifications 6!
            CheckExpiredTasks::dispatch();
        })->dailyAt('13:00');
        $schedule->call(function(){
            MailingsDelivery::dispatch();
        })->cron(config('mailing.schedule_cron_string'));
        $schedule->command('voicent:campstats')->dailyAt('00:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
