<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Multitenantable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Mailing extends Model
{
    use Multitenantable, SoftDeletes;


    protected $fillable = [
        "name",
        "mail_template_id",
        "created_by",
        "scheduled_at",
        "max_per_min"

    ];

    protected $hidden = [
    ];

    protected $dates = [
        "scheduled_at",
        "created_at",
        "updated_at",

    ];


    protected $appends = ['resource_url', 'stats', 'active_sendings'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/mailings/' . $this->getKey());
    }


    /**
     * List of email groups
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function emailGroups()
    {
        return $this->belongsToMany('App\EmailGroup');
    }

    /**
     * Mail templates
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mailTemplate()
    {
        return $this->belongsTo(MailTemplate::class);
    }

    public function historyMailings()
    {
        return $this->hasMany(HistoryMailing::class);
    }

    public function getStatsAttribute()
    {
        $result['total'] = HistoryMailing::where('mailing_id', '=', $this->id)->distinct('email_mailing_id')->count();
        foreach (MailingStatus::all() as $status) {
            $result[$status->name] = HistoryMailing::where('mailing_id', '=', $this->id)
                ->where('mailing_status_id', '=', $status->id)
                ->distinct('email_mailing_id')->count();
        }
        return $result;
    }

    public function getActiveSendingsAttribute()
    {
        return $this->historyMailings()->whereHas('mailingSending', function($query) {
           $query->where('running','=',1);
        })->count();
    }


}
