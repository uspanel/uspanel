<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use CodersStudio\Chat\App\Http\Resources\UsersCollection;

class ContactController extends Controller
{
    public function index(Request $request) {
    	$users = User::where('id', '!=', auth()->id());
        if ($request->get('project_id')) {
            $users = $users
            ->where(function ($query) use ($request) {
                $query
                ->whereHas('employee', function ($subQuery) use ($request) {
                    $subQuery->where('project_id', $request->get('project_id'));
                })
                ->orWhere(function ($subQuery) use ($request) {
                    $subQuery->doesntHave('employee')
                    ->whereHas('projects', function ($subSubQuery) use ($request) {
                        $subSubQuery->where('id', $request->get('project_id'));
                    });
                });
            });
        }
        if ($request->get('role_id')) {
            $users = $users
            ->whereHas('roles', function ($query) use ($request) {
                $query->where('id', $request->get('role_id'));
            });
        }
        $users = $users->get();
    	if (auth()->user()->hasRole('Employee Staff')) {
    		$users = collect([
    			auth()->user()->employee->support,
    			auth()->user()->employee->hr
    		]);
        }
        return new UsersCollection($users);
    }
}
