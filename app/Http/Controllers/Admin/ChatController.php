<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\IndexChatroom;
use App\{
	Project,
	Role
};

class ChatController extends Controller
{
    public function index(IndexChatroom $request, $chatroom = null) {
    	return view('admin.chat.index', [
    		'chatroom' => $chatroom
    	]);
    }

    public function getFiltersOptions()
    {
    	return response([
    		'projects' => Project::get(),
    		'roles' => Role::get(),
    	]);
    }
}
