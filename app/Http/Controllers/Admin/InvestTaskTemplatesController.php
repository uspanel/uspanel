<?php namespace App\Http\Controllers\Admin;

use App\Contragent;
use App\ContragentList;
use App\Employee;
use App\Http\Controllers\Controller;
use App\InvestorList;
use App\InvestTask;
use App\InvestTaskStatus;
use App\MailTemplate;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\InvestTaskTemplate\IndexInvestTaskTemplate;
use App\Http\Requests\Admin\InvestTaskTemplate\StoreInvestTaskTemplate;
use App\Http\Requests\Admin\InvestTaskTemplate\UpdateInvestTaskTemplate;
use App\Http\Requests\Admin\InvestTaskTemplate\DestroyInvestTaskTemplate;
use Brackets\AdminListing\Facades\AdminListing;
use App\InvestTaskTemplate;
use Illuminate\Support\Facades\Auth;

class InvestTaskTemplatesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexInvestTaskTemplate $request
     * @return Response|array
     */
    public function index(IndexInvestTaskTemplate $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(InvestTaskTemplate::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            [
                'id',
                'name',
                'theme',
                'text',
                'manager_id',
                'created_at',
                'mail_template_id'
            ],

            // set columns to searchIn
            ['id', 'name', 'theme', 'text', 'description'],
            function ($query) use ($request) {
                $query->with('manager', 'investors', 'contragents', 'investorLists',
                    'contragentLists');
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.invest-task-template.index', ['data' => $data]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('crud.invest-task-template.create');

        return view('admin.invest-task-template.create',[
            'viewName' => 'invest-task-template',
            'investors' => User::role('Investor')->get(),
            'contragents' => Contragent::all(),
            'managers' => User::role('HR Manager Invest')->get(),
            'investor_lists' => InvestorList::all(),
            'contragent_lists' => ContragentList::all(),
            'mailTemplates' => MailTemplate::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreInvestTaskTemplate $request
     * @return Response|array
     */
    public function store(StoreInvestTaskTemplate $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        $sanitized['manager_id'] = $sanitized['manager']['id'] ?: null;
        unset($sanitized['manager']);

        // Store the InvestTask
        $investTaskTemplate = InvestTaskTemplate::create($sanitized);

        if ($sanitized['investors']) {
            foreach ($sanitized['investors'] as $investor) {
                $investTaskTemplate->investors()->attach([
                    $investor['id']
                ]);
            }
        }

        if ($sanitized['investor_lists']) {
            foreach ($sanitized['investor_lists'] as $investor_list) {
                $investTaskTemplate->investorLists()->attach([
                    $investor_list['id']
                ]);
            }
        }

        if ($sanitized['contragents']) {
            foreach ($sanitized['contragents'] as $contragent) {
                $investTaskTemplate->contragents()->attach([
                    $contragent['id']
                ]);
            }
        }

        if ($sanitized['contragent_lists']) {
            foreach ($sanitized['contragent_lists'] as $contragent_list) {
                $investTaskTemplate->contragentLists()->attach([
                    $contragent_list['id']
                ]);
            }
        }

        if ($request->ajax()) {
            return ['redirect' => url('/admin/invest-task-templates'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/invest-task-templates');
    }

    /**
     * Display the specified resource.
     *
     * @param  InvestTaskTemplate $investTaskTemplate
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(InvestTaskTemplate $investTaskTemplate)
    {
        $this->authorize('admin.invest-task-template.show', $investTaskTemplate);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  InvestTaskTemplate $investTaskTemplate
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(InvestTaskTemplate $investTaskTemplate)
    {
        $this->authorize('crud.invest-task-template.edit', $investTaskTemplate);

        $investTaskTemplate->load('manager', 'investors', 'contragents', 'investorLists',
            'contragentLists');

        return view('admin.invest-task-template.edit', [
            'investTaskTemplate' => $investTaskTemplate,
            'viewName' => 'invest-task-template',
            'investors' => User::role('Investor')->get(),
            'contragents' => Contragent::all(),
            'managers' => User::role('HR Manager Invest')->get(),
            'investor_lists' => InvestorList::all(),
            'contragent_lists' => ContragentList::all(),
            'mailTemplates' => MailTemplate::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateInvestTaskTemplate $request
     * @param  InvestTaskTemplate $investTaskTemplate
     * @return Response|array
     */
    public function update(UpdateInvestTaskTemplate $request, InvestTaskTemplate $investTaskTemplate)
    {
        // Sanitize input
        $sanitized = $request->validated();

        $sanitized['manager_id'] = $sanitized['manager']['id'] ?: null;
        unset($sanitized['manager']);

        if ($sanitized['investors']) {
            $investTaskTemplate->investors()->detach();
            foreach ($sanitized['investors'] as $investor) {
                $investTaskTemplate->investors()->attach([
                    $investor['id']
                ]);
            }
        }

        if ($sanitized['investor_lists']) {
            $investTaskTemplate->investorLists()->detach();
            foreach ($sanitized['investor_lists'] as $investor_list) {
                $investTaskTemplate->investorLists()->attach([
                    $investor_list['id']
                ]);
            }
        }

        if ($sanitized['contragents']) {
            $investTaskTemplate->contragents()->detach();
            foreach ($sanitized['contragents'] as $contragent) {
                $investTaskTemplate->contragents()->attach([
                    $contragent['id']
                ]);
            }
        }

        if ($sanitized['contragent_lists']) {
            $investTaskTemplate->contragentLists()->detach();
            foreach ($sanitized['contragent_lists'] as $contragent_list) {
                $investTaskTemplate->contragentLists()->attach([
                    $contragent_list['id']
                ]);
            }
        }

        // Update changed values InvestTask
        $investTaskTemplate->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/invest-task-templates'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/invest-task-templates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyInvestTaskTemplate $request
     * @param  InvestTaskTemplate $investTaskTemplate
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyInvestTaskTemplate $request, InvestTaskTemplate $investTaskTemplate)
    {
        $investTaskTemplate->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    }
