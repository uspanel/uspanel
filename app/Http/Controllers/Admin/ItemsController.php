<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Item\{
    DestroyItem,
    IndexItem,
    StoreItem,
    UpdateItem
};
use App\{
    Item,
    Package,
    Task
};
use Brackets\AdminListing\Facades\AdminListing;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ItemsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexItem $request
     * @return Response|array
     */
    public function index(IndexItem $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Item::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id','size','color','link','user_id','name','weight'],

            // set columns to searchIn
            ['id', 'size', 'color', 'info', 'link','name'],
            function ($query) use ($request){
                $query->with([
                    'user',
                ]);
                if (!(auth()->user()->hasRole('Administrator') || auth()->user()->hasRole('Employee Staff'))) {
                    $query->where('user_id', auth()->id());
                }
                if (auth()->user()->hasRole('Employee Staff')) {
                    $query->whereHas('tasks', function($sq) use($request){
                        $sq->whereHas('warehouse', function ($ssq) use($request) {
                            $ssq->whereIn('id', auth()->user()->warehouses()->pluck('id'));
                        });
                    });
                }
                if ($request->get('warehouse_id')) {
                    $query->whereHas('tasks', function($sq) use($request){
                        $sq->where('warehouse_id','=',$request->get('warehouse_id'))
                            ->whereNotNull('delivered_at');
                    });
                    $query->with(['tasks']);
                }
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.item.index', ['data' => $data]);

    }

    public function getUnusedItems(IndexItem $request) {
        $data = AdminListing::create(Item::class)->processRequestAndGet(
            $request,
            ['id','size','color','link','user_id'],
            [],
            function ($query) {
                $query->whereNull('package_id')
                ->with([
                    'user',
                ]);
                if (!auth()->user()->hasRole('Administrator')) {
                    $query->where('user_id', auth()->id());
                }
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }
    }

    public function getDeliveredItems(IndexItem $request) {
        $data = AdminListing::create(Item::class)->processRequestAndGet(
            $request,
            ['id', 'name','size','color','link','user_id'],
            [],
            function ($query) {
                $query->where('free_qty','>',0)
                ->with([
                    'user',
                    'package.employee.user',
                ])
                ->whereHas('package', function ($subQuery) {
                    $subQuery->whereNotNull('delivered_at');
                });
                if (Auth::check() && !Auth::user()->hasRole('Administrator')) {
                    $query->whereHas('package', function($ssq) {
                        $ssq->whereHas('employee', function ($sssq) {
                            $sssq->whereHas('staffers', function ($ssssq) {
                                $ssssq->where('id', Auth::user()->id);
                            });
                        });
                    });
                }
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }
    }


//TODO DELETE IF NOTHING BRAKES

//    public function getPackageItems(IndexItem $request, Package $package) {
//        $data = AdminListing::create(Item::class)->processRequestAndGet(
//            $request,
//            ['id','size','color','price','link','user_id', 'name'],
//            [],
//            function ($query) use ($package) {
//                $query->whereHas('packages', $packFilter = function($sq) use ($package) {
//                    $sq->where('id','=',$package->id);
//                })
//                ->with([
//                    'user',
//                    'packages' => $packFilter
//                ]);
//            }
//        );
//
//        if ($request->ajax()) {
//            return ['data' => $data];
//        }
//    }

    public function getTaskItems(IndexItem $request, Task $task) {
        $data = AdminListing::create(Item::class)->processRequestAndGet(
            $request,
            ['id','size','color','qty','link','package_id','user_id','free_qty'],
            [],
            function ($query) use ($task) {
                $query->whereHas('tasks', function($sq) use ($task) {
                    $sq->where('id',$task->id);
                })
                ->with([
                    'user',
                    'package.employee.user',
                ]);
                $query->orWhere(function ($subQuery) {
                    $subQuery->doesntHave('tasks')
                    ->whereHas('package', function ($subSubQuery) {
                        $subSubQuery->whereNotNull('delivered_at');
                    });
                });
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $packages = auth()->user()->packages();
        if (auth()->user()->hasRole('Administrator')) {
            $packages = Package::all();
        }
        $this->authorize('crud.item.create');

        return view('admin.item.create', [
            'packages' => $packages
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreItem $request
     * @return Response|array
     */
    public function store(StoreItem $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the Item
        $item = Item::where($sanitized)->first();
        if ($item) {
            $item->replicate();
            $item->push();
        } else {
            $item = auth()->user()->items()->create($sanitized);
        }

        if ($request->ajax()) {
            return ['redirect' => url('/admin/items'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/items');
    }

    /**
     * Display the specified resource.
     *
     * @param  Item $item
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Item $item)
    {
        $this->authorize('admin.item.show', $item);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Item $item
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Item $item)
    {
        $packages = auth()->user()->packages;
        if (auth()->user()->hasRole('Administrator')) {
            $packages = Package::all();
        }
        $this->authorize('crud.item.edit', $item);
        return view('admin.item.edit', [
            'item' => $item,
            'packages' => $packages
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateItem $request
     * @param  Item $item
     * @return Response|array
     */
    public function update(UpdateItem $request, Item $item)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values Item
        
        $item->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/items'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/items');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyItem $request
     * @param  Item $item
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyItem $request, Item $item)
    {
        $item->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    }
