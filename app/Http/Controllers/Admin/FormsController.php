<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\Form\IndexForm;
use App\Http\Requests\Admin\Form\StoreForm;
use App\Http\Requests\Admin\Form\UpdateForm;
use App\Http\Requests\Admin\Form\DestroyForm;
use Brackets\AdminListing\Facades\AdminListing;
use App\Form;
use App\Project;

class FormsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexForm $request
     * @return Response|array
     */
    public function index(IndexForm $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Form::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name'],

            // set columns to searchIn
            ['name'],
            function($query) {
                $query->with('projects');
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.form.index', ['data' => $data]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('crud.form.create');

        return view('admin.form.create', [
            'projects' => Project::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreForm $request
     * @return Response|array
     */
    public function store(StoreForm $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the Form
        $form = Form::create($sanitized);
        $form->projects()->sync($request->project_ids);
        if ($request->ajax()) {
            return ['redirect' => url('/admin/forms'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/forms');
    }

    /**
     * Display the specified resource.
     *
     * @param  Form $form
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Form $form)
    {
        $this->authorize('crud.form.show', $form);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Form $form
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Form $form)
    {
        $this->authorize('crud.form.edit', $form);
        $form->load('projects');
        return view('admin.form.edit', [
            'form' => $form,
            'projects' => Project::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateForm $request
     * @param  Form $form
     * @return Response|array
     */
    public function update(UpdateForm $request, Form $form)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values Form
        $form->update($sanitized);
        $form->projects()->sync($request->project_ids);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/forms'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/forms');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyForm $request
     * @param  Form $form
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyForm $request, Form $form)
    {
        $form->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    }
