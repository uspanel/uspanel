<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\MailTemplate\IndexMailTemplate;
use App\Http\Requests\Admin\MailTemplate\StoreMailTemplate;
use App\Http\Requests\Admin\MailTemplate\UpdateMailTemplate;
use App\Http\Requests\Admin\MailTemplate\DestroyMailTemplate;
use Brackets\AdminListing\Facades\AdminListing;
use App\MailTemplate;
use Illuminate\Support\Facades\Auth;

class MailTemplatesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexMailTemplate $request
     * @return Response|array
     */
    public function index(IndexMailTemplate $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(MailTemplate::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id','title', 'body', 'created_at', 'deletable'],

            // set columns to searchIn
            ['title']
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.mail-template.index', ['data' => $data]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('crud.mail_template.modify');

        return view('admin.mail-template.create',['vars' => (new MailTemplate)->getVariables()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreMailTemplate $request
     * @return Response|array
     */
    public function store(StoreMailTemplate $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the MailTemplate
        $mailTemplate = MailTemplate::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/mail-templates'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/mail-templates');
    }

    /**
     * Display the specified resource.
     *
     * @param  MailTemplate $mailTemplate
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(MailTemplate $mailTemplate)
    {
        $this->authorize('crud.mail-template.show', $mailTemplate);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  MailTemplate $mailTemplate
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(MailTemplate $mailTemplate)
    {
        $this->authorize('crud.mail_template.modify');

        return view('admin.mail-template.edit', [
            'mailTemplate' => $mailTemplate,
            'vars' => (new MailTemplate)->getVariables()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateMailTemplate $request
     * @param  MailTemplate $mailTemplate
     * @return Response|array
     */
    public function update(UpdateMailTemplate $request, MailTemplate $mailTemplate)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values MailTemplate
        $mailTemplate->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/mail-templates'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/mail-templates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyMailTemplate $request
     * @param  MailTemplate $mailTemplate
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyMailTemplate $request, MailTemplate $mailTemplate)
    {
        $mailTemplate->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Get example of templatized message body according current user
     *
     * @param Request $request
     * @param $id integer Template id
     * @return mixed
     */
    public function testTemplatization(Request $request, $id)
    {
        $template = MailTemplate::findOrFail($id);
        $recipient = Auth::user();
        $recipient->restore_pass_link = '<a href="sometning" >something</a>';
        $templatized = $template->getTemplatized(['recipient' => $recipient]);
        return response()->json([
            'data'=>[
                'type' => 'template',
                'id' => $id,
                'attributes' => [
                    'body' => $template->body,
                    'templatized' => $templatized
                ]
            ]
        ]);
    }

    }
