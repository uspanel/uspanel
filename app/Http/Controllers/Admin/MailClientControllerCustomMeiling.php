<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Mailsetting;
use App\Traits\Mailconnectable;
use App\User;
use CodersStudio\ImapMailClient\Http\Controllers\MailClientController;
use CodersStudio\ImapMailClient\Http\Requests\MailClientSendRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

class MailClientControllerCustomMeiling extends MailClientController
{
    use Mailconnectable;

    public function send(MailClientSendRequest $request)
    {

        $this->setSettings($request->get('set_id', null), $request->get('user_id', null));

        if ($this->mailsettings->owner) {
            $name = User::findOrFail($this->mailsettings->owner)->name;
        } else {
            $name = $this->mailsettings->user->name;
        }


        if($this->mailsettings->for_meiling == 1){
            $from = $this->getFrom($this->mailsettings);
        }else{
            $from =  ['address' => $this->mailsettings->login, 'name' => $name];
        }



        $config = [
            'driver' => 'smtp',
            'host' => $this->mailsettings->smtp_host,
            'port' => $this->mailsettings->smtp_port,
            'from' => $from,
            'encryption' => $this->mailsettings->smtp_encryption,
            'username' => $this->mailsettings->login,
            'password' => $this->mailsettings->plain_password,
            'sendmail' => '/usr/sbin/sendmail -bs',
            'pretend' => false,
        ];
        Config::set('mail', $config);

        $msgId = $request->get('message_id', null);
        $folder = $request->get('folder', null);
        $locMsg = null;
        $fwdApp = null;
        if ($msgId) {
            $locMsg = $this->mailsettings->mailmessages()
                ->where('uid', '=', $msgId)
                ->where('folder', '=', $folder)
                ->firstOrFail();
            $fwdApp = preg_replace('/\[sender\]/', htmlentities($locMsg->sender), config('csmailclient.forwarding_app'));
        }


//        Log::error('before_foreach', $request->get('recipients'));
//        Log::error('before_foreach_subject: ' . $request->get('subject'));



        foreach ($request->get('recipients') as $recipient) {
            try {
                Mail::send([], [], function ($message) use ($request, $recipient, $locMsg, $fwdApp) {
                    $message->to($recipient)
                        ->subject(($locMsg ? 'Fwd: ' : '') . $request->get('subject'))
                        ->setBody(($locMsg ? $fwdApp : '') . $request->get('message'), 'text/html');
                    if ($atts = $this->extractAttachments($request)) {
                        foreach ($atts as $attachment) {
                            $message->attach($attachment['pathname'], [
                                'as' => $attachment['filename'],
                                'mime' => $attachment['mime']
                            ]);
                        }
                    }
                });
                $this->moveToSent($recipient, $request, $fwdApp);
            } catch (\Exception $e) {
                if ($request->ajax()) {
                    return Response::json(['errors' => [$e->getMessage()]], 400);
                }

                return Redirect::back()->withErrors(['error', $e->getMessage()]);
            }
        }
        if ($request->ajax()) {
            return Response::json(['data' => []]);
        }

        return Redirect::back();

    }

    protected function getFrom($settings){
        $set =  Mailsetting::find($settings->id);
        $set->load('meilsettingDomains', 'meilsettingNames');
        $array_of_names = $set->meilsettingNames->toArray();
        $array_of_domains = $set->meilsettingDomains->toArray();
        $rand_name_key = array_rand($array_of_names);
        $rand_domain_key = array_rand($array_of_domains);
        $rand_elem_of_name = $array_of_names[$rand_name_key];
        $rand_elem_of_domain = $array_of_domains[$rand_domain_key];
        $login = $rand_elem_of_name['name_domain'] . "@" . $rand_elem_of_domain['domain'];
        $name = $rand_elem_of_name['name'];
        $from = ['address' => $login, 'name' => $name];
        return $from;
    }
}
