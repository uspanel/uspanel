<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\HelpVideo\IndexHelpVideo;
use App\Http\Requests\Admin\HelpVideo\StoreHelpVideo;
use App\Http\Requests\Admin\HelpVideo\UpdateHelpVideo;
use App\Http\Requests\Admin\HelpVideo\DestroyHelpVideo;
use Brackets\AdminListing\Facades\AdminListing;
use App\HelpVideo;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class HelpVideosController extends Controller
{
    protected $guard = 'web';

    /**
     * Display a listing of the resource.
     *
     * @param  IndexHelpVideo $request
     * @return Response|array
     */
    public function index(IndexHelpVideo $request)
    {
        $roles = Auth::user()->getRoleNames()->toArray();
        // create and AdminListing instance for a specific model and
        $selectedRoles = [];
        if ($request->has('roles')) {
            $rRoles = $request->get('roles', []);
            foreach ($rRoles as $rRole) {
                $selectedRoles[] = json_decode($rRole,1)['id'];
            }
        }

        $data = AdminListing::create(HelpVideo::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'title', 'description', 'processing'],

            // set columns to searchIn
            ['id', 'title', 'description'],
            function ($query) use ($roles, $selectedRoles){
                if (!Auth::user()->hasRole('Administrator')) {
                    $query->whereHas('roles', function ($subq) use ($roles){
                        $subq->whereIn('name',$roles);
                    })
                    ->orWhereDoesntHave('roles');
                }
                if (!empty($selectedRoles)) {
                    $query->whereHas('roles', function ($subq) use ($selectedRoles){
                        $subq->whereIn('id',$selectedRoles);
                    });
                }
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.help-video.index', ['data' => $data, 'roles' => Role::where('guard_name', $this->guard)->get()]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('crud.help_video.modify');

        return view('admin.help-video.create', ['helpVideo' => new HelpVideo(), 'roles' => Role::where('guard_name', $this->guard)->get()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreHelpVideo $request
     * @return Response|array
     */
    public function store(StoreHelpVideo $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the HelpVideo
        $helpVideo = HelpVideo::create($sanitized);
        $helpVideo->roles()->sync(collect($request->input('roles', []))->map->id->toArray());
        if ($request->ajax()) {
            return ['redirect' => url('admin/help-videos'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/help-videos');
    }

    /**
     * Display the specified resource.
     *
     * @param  HelpVideo $helpVideo
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(HelpVideo $helpVideo)
    {
        $this->authorize('crud.help-video.show', $helpVideo);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  HelpVideo $helpVideo
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(HelpVideo $helpVideo)
    {
        $this->authorize('crud.help_video.modify');

        return view('admin.help-video.edit', [
            'helpVideo' => $helpVideo,
            'roles' => Role::where('guard_name', $this->guard)->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateHelpVideo $request
     * @param  HelpVideo $helpVideo
     * @return Response|array
     */
    public function update(UpdateHelpVideo $request, HelpVideo $helpVideo)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values HelpVideo
        $helpVideo->update($sanitized);
        $helpVideo->roles()->sync(collect($request->input('roles', []))->map->id->toArray());
        if ($request->ajax()) {
            return ['redirect' => url('admin/help-videos'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/help-videos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyHelpVideo $request
     * @param  HelpVideo $helpVideo
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyHelpVideo $request, HelpVideo $helpVideo)
    {
        $helpVideo->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    }
