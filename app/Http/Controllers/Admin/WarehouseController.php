<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\Warehouse\IndexWarehouse;
use App\Http\Requests\Admin\Warehouse\StoreWarehouse;
use App\Http\Requests\Admin\Warehouse\UpdateWarehouse;
use App\Http\Requests\Admin\Warehouse\DestroyWarehouse;
use Brackets\AdminListing\Facades\AdminListing;
use App\{Warehouse, User, Employee};
use Illuminate\Support\Facades\Auth;

class WarehouseController extends Controller
{

    /**
     * fields for searching
     *
     * @var array
     */
    protected $filters = [
        'name',
        'city',
        'address',
        'user',
    ];

    /**
     * Display a listing of the resource.
     *
     * @param IndexWarehouse $request
     * @return Response|array
     */
    public function index(IndexWarehouse $request)
    {
        $columns = Auth::user()
            ->getAvailableFields('warehouse', 'read')
            ->prepend('id')
            ->toArray();
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Warehouse::class)->processRequestAndGet(
        // pass the request with params
            $request,

            // set columns to query
            $columns,

            // set columns to searchIn
            [''],
            function ($query) use ($request) {
                foreach ($request->only($this->filters) as $key => $value) {
                    $query->where($key, 'like', '%' . $value . '%');
                }
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.warehouse.index', [
            'data' => $data,
            'viewName' => 'warehouse',
            'user' => Employee::get()->pluck('user_name', 'user_id')
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('crud.warehouse.view');

        return view('admin.warehouse.create', [
            'user' => Employee::get()->pluck('user_name', 'user_id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreWarehouse $request
     * @return Response|array
     */
    public function store(StoreWarehouse $request)
    {
        $columns = Auth::user()->getAvailableFields('warehouse', 'write')->toArray();

        Warehouse::create($request->only($columns));

        if ($request->ajax()) {
            return ['redirect' => url('admin/warehouses'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/warehouses');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Warehouse $warehouse
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Warehouse $warehouse)
    {
        $this->authorize('crud.warehouse.view');

        return view('admin.warehouse.edit', [
            'warehouse' => $warehouse,
            'user' => Employee::get()->pluck('user_name', 'user_id')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateWarehouse $request
     * @param Warehouse $warehouse
     * @return Response|array
     */
    public function update(UpdateWarehouse $request, Warehouse $warehouse)
    {
        $columns = Auth::user()->getAvailableFields('warehouse', 'write')->toArray();
        $warehouse->update($request->only($columns));

        if ($request->ajax()) {
            return ['redirect' => url('admin/warehouses'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/warehouses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyWarehouse $request
     * @param Warehouse $warehouse
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyWarehouse $request, Warehouse $warehouse)
    {
        $warehouse->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Get Items with quantity available for given warehouse
     *
     * @param Request $request
     * @param Warehouse $warehouse
     * @return mixed
     */
    public function getAvailableItems(Request $request, Warehouse $warehouse)
    {
        $items = $warehouse->items()->groupBy('items.id')->paginate($request->get('per_page',10));
        foreach ($items as &$item) {
            $item->pivot->qty = $warehouse->getItemQty($item->id);
        }
        return response([
            'data' => $items
        ]);

    }

}
