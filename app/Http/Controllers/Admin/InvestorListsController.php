<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Database\Schema\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\InvestorList\IndexInvestorList;
use App\Http\Requests\Admin\InvestorList\StoreInvestorList;
use App\Http\Requests\Admin\InvestorList\UpdateInvestorList;
use App\Http\Requests\Admin\InvestorList\DestroyInvestorList;
use Brackets\AdminListing\Facades\AdminListing;
use App\{
    InvestorList,
    User
};
use Illuminate\Support\Facades\Auth;

class InvestorListsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexInvestorList $request
     * @return Response|array
     */
    public function index(IndexInvestorList $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(InvestorList::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name'],

            // set columns to searchIn
            ['id', 'name'],
            function ($query) use ($request) {
                $query->with('users');
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.investor-list.index', ['data' => $data]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('crud.investor-list.create');

        $investors = User::getRoleForSelect('Investor');

        return view('admin.investor-list.create', ['investors' => $investors]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreInvestorList $request
     * @return Response|array
     */
    public function store(StoreInvestorList $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the InvestorList
        $investorList = InvestorList::create($sanitized);

        foreach ($sanitized['users'] as $investor) {
            $investorList->users()->attach([
                $investor['id']
            ]);
        }

        if ($request->ajax()) {
            return ['redirect' => url('/admin/investor-lists'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/investor-lists');
    }

    /**
     * Display the specified resource.
     *
     * @param  InvestorList $investorList
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(InvestorList $investorList)
    {
        $this->authorize('crud.investor-list.show', $investorList);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  InvestorList $investorList
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(InvestorList $investorList)
    {
        $this->authorize('crud.investor-list.edit', $investorList);

        $investors = User::getRoleForSelect('Investor');

        $investorList = InvestorList::with('users')->find($investorList->id);

        return view('admin.investor-list.edit', [
            'investorList' => $investorList,
            'investors' => $investors
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateInvestorList $request
     * @param  InvestorList $investorList
     * @return Response|array
     */
    public function update(UpdateInvestorList $request, InvestorList $investorList)
    {
        // Sanitize input
        $sanitized = $request->validated();

        $investorList->users()->detach();

        foreach ($sanitized['users'] as $investor) {
            $investorList->users()->attach([
                $investor['id']
            ]);
        }

        // Update changed values InvestorList
        $investorList->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/investor-lists'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/investor-lists');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyInvestorList $request
     * @param  InvestorList $investorList
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyInvestorList $request, InvestorList $investorList)
    {
        $investorList->users()->detach();

        $investorList->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    }
