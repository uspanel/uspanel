<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\{
    Request,
    Response
};
use App\Http\Requests\Admin\InvestTask\{
    IndexInvestTask,
    StoreInvestTask,
    UpdateInvestTask,
    DestroyInvestTask,
    RestoreInvestTask,
    ChangeStatusInvestTask
};
use Brackets\AdminListing\Facades\AdminListing;
use App\{Employee,
    InvestTask,
    InvestTaskTemplate,
    MailTemplate,
    User,
    Contragent,
    InvestorList,
    ContragentList,
    InvestTaskStatus};

class InvestTasksController extends Controller
{
    /**
     * @var array
     */
    protected $filters = [
        'id',
        'name',
        'theme',
        'text',
        'deadline',
        'employee_id',
        'manager_id',
        'creator_id',
        'invest_task_status_id',
        'created_at'
    ];

    /**
     * Display a listing of the resource.
     *
     * @param IndexInvestTask $request
     * @return Response|array
     */
    public function index(IndexInvestTask $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(InvestTask::class)->processRequestAndGet(
        // pass the request with params
            $request,

            // set columns to query
            [
                'id',
                'name',
                'theme',
                'text',
                'deadline',
                'employee_id',
                'manager_id',
                'creator_id',
                'invest_task_status_id',
                'created_at',
                'deleted_at',
                'mail_template_id'
            ],

            // set columns to searchIn
            ['id', 'name', 'theme', 'text', 'deadline', 'invest_task_status_id'],

            function ($query) use ($request) {
                $query->with('manager', 'creator', 'employee.user', 'investors', 'contragents', 'investorLists.users',
                    'contragentLists.contragents', 'investTaskStatus', 'media');
                $this->filter($query, $request);
                if (auth()->user()->hasRole(['Employee Invest'])) {
                    $query->where('employee_id', auth()->user()->employee->id);
                }
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.invest-task.index', [
            'data' => $data,
            'viewName' => 'invest-task',
            'invest_task_statuses' => InvestTaskStatus::all()->pluck('title', 'id')
        ]);
    }

    /**
     * @param $query
     * @param IndexInvestTask $request
     * @return mixed
     */
    public function filter($query, IndexInvestTask $request)
    {
        foreach ($request->only($this->filters) as $key => $value) {
            if (!$value) {
                continue;
            }
            if (preg_match('/_at$/', $key)) {
                $query->whereDate($key, $value);
            } elseif (preg_match('/_id$/', $key)) {
                if ($key == 'warehouse_id') {
                    $query->whereHas('warehouse', function ($query) use ($value) {
                        $query->where('id', $value);
                    });
                }
                elseif ($key === 'invest_task_status_id' || $value === -1) {
                    return $query->onlyTrashed();
                }
                $query->where($key, $value);
            } elseif ($key == 'id') {
                $query->where($key, $value);
            } else {
                $query->where($key, 'like', '%' . $value . '%');
            }
        }
        return $query;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|Response|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('crud.invest-task.create');
        $request->validate([
            'empl.*' => 'sometimes|exists:users,id',
            'invest_task_template_id' => 'sometimes|exists:invest_task_templates,id',
        ]);

        return view('admin.invest-task.create', [
            'viewName' => 'invest-task',
            'investors' => User::role('Investor')->get(),
            'contragents' => Contragent::all(),
            'employee' => Employee::with('user')->whereHas('user', function($query) use ($request){
                $query->whereIn('id',$request->get('empl', []));
            })->get(),
            'invest_task' => InvestTask::find($request->get('invest_task_id')),
            'employees' => Employee::with('user')->whereHas('user', function ($query) {
                $query->role('Employee Invest');
            })->get(),
            'managers' => User::role('HR Manager Invest')->get(),
            'investor_lists' => InvestorList::all(),
            'contragent_lists' => ContragentList::all(),
            'invest_task_statuses' => InvestTaskStatus::all(),
            'mailTemplates' => MailTemplate::all(),
            'taskTemplate' => $request->get('invest_task_template_id') ? InvestTaskTemplate::findOrFail($request->get('invest_task_template_id'))
                ->load('manager', 'investors', 'contragents', 'investorLists','contragentLists')
                : new InvestTask()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreInvestTask $request
     * @return Response|array
     */
    public function store(StoreInvestTask $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        $sanitized['manager_id'] = $sanitized['manager']['id'];
        $sanitized['creator_id'] = Auth::user()->id;
        $employee = $sanitized['employee'];
        unset($sanitized['employee']);
        unset($sanitized['manager']);

        // Store the InvestTask
        foreach ($employee as $empl) {
            $dispatcher = InvestTask::getEventDispatcher();
            InvestTask::unsetEventDispatcher();

            $sanitized['employee_id'] = $empl['id'];
            $investTask = InvestTask::create($sanitized);
            if ($sanitized['investors']) {
                foreach ($sanitized['investors'] as $investor) {
                    $investTask->investors()->attach([
                        $investor['id']
                    ]);
                }
            }

            if ($sanitized['investor_lists']) {
                foreach ($sanitized['investor_lists'] as $investor_list) {
                    $investTask->investorLists()->attach([
                        $investor_list['id']
                    ]);
                }
            }

            if ($sanitized['contragents']) {
                foreach ($sanitized['contragents'] as $contragent) {
                    $investTask->contragents()->attach([
                        $contragent['id']
                    ]);
                }
            }

            if ($sanitized['contragent_lists']) {
                foreach ($sanitized['contragent_lists'] as $contragent_list) {
                    $investTask->contragentLists()->attach([
                        $contragent_list['id']
                    ]);
                }
            }

            InvestTask::setEventDispatcher($dispatcher);
            event('eloquent.created: App\InvestTask', $investTask);
        }


        if ($request->ajax()) {
            return [
                'redirect' => url('/admin/invest-tasks'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded')
            ];
        }

        return redirect('/admin/invest-tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param InvestTask $investTask
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(InvestTask $investTask)
    {
        $this->authorize('crud.invest-task.show', $investTask);

        $investTask->load('investTaskStatus', 'media');

        return view('admin.invest-task.show', [
            'invest_task' => $investTask,
            'invest_task_status' => $investTask->investTaskStatus()->get(),
            'invest_task_statuses' => InvestTaskStatus::all(),
            'files' => $investTask->media()->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param InvestTask $investTask
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(InvestTask $investTask)
    {
        $this->authorize('crud.invest-task.edit', $investTask);

        $investTask->load('manager', 'creator', 'employee.user', 'investors', 'contragents', 'investorLists',
            'contragentLists', 'investTaskStatus');

        return view('admin.invest-task.edit', [
            'viewName' => 'invest-task',
            'investTask' => $investTask,
            'investors' => User::role('Investor')->get(),
            'contragents' => Contragent::all(),
            'employees' => User::role('Employee Invest')->get(),
            'managers' => User::role('HR Manager Invest')->get(),
            'investor_lists' => InvestorList::all(),
            'contragent_lists' => ContragentList::all(),
            'invest_task_statuses' => InvestTaskStatus::all(),
            'mailTemplates' => MailTemplate::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateInvestTask $request
     * @param InvestTask $investTask
     * @return Response|array
     */
    public function update(UpdateInvestTask $request, InvestTask $investTask)
    {
        // Sanitize input
        $sanitized = $request->validated();

        $sanitized['employee_id'] = $sanitized['employee']['id'];
        $sanitized['manager_id'] = $sanitized['manager']['id'];
        $sanitized['creator_id'] = $sanitized['creator']['id'];
        $sanitized['invest_task_status_id'] = $sanitized['invest_task_status']['id'];
        unset($sanitized['employee']);
        unset($sanitized['manager']);
        unset($sanitized['creator']);
        unset($sanitized['invest_task_status']);

        if ($sanitized['investors']) {
            $investTask->investors()->detach();
            foreach ($sanitized['investors'] as $investor) {
                $investTask->investors()->attach([
                    $investor['id']
                ]);
            }
        }

        if ($sanitized['investor_lists']) {
            $investTask->investorLists()->detach();
            foreach ($sanitized['investor_lists'] as $investor_list) {
                $investTask->investorLists()->attach([
                    $investor_list['id']
                ]);
            }
        }

        if ($sanitized['contragents']) {
            $investTask->contragents()->detach();
            foreach ($sanitized['contragents'] as $contragent) {
                $investTask->contragents()->attach([
                    $contragent['id']
                ]);
            }
        }

        if ($sanitized['contragent_lists']) {
            $investTask->contragentLists()->detach();
            foreach ($sanitized['contragent_lists'] as $contragent_list) {
                $investTask->contragentLists()->attach([
                    $contragent_list['id']
                ]);
            }
        }

        // Update changed values InvestTask
        $investTask->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('/admin/invest-tasks'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded')
            ];
        }

        return redirect('/admin/invest-tasks');
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param RestoreInvestTask $request
     * @return array
     */
    public function restore(RestoreInvestTask $request)
    {
        $investTask = InvestTask::onlyTrashed()->find($request->input('id'));
        $investTask->restore();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyInvestTask $request
     * @param InvestTask $investTask
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyInvestTask $request, InvestTask $investTask)
    {
        $investTask->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }


    /**
     * Change status the specified resource in storage.
     *
     * @param ChangeStatusInvestTask $request
     * @param InvestTask $investTask
     * @return Response|bool
     */
    public function changeStatus(ChangeStatusInvestTask $request, InvestTask $investTask)
    {
        $sanitized = $request->validated();
        $sanitized['invest_task_status_id'] = $sanitized['invest_task_status']['id'];

        // Update changed values InvestTask
        $investTask->update($sanitized);

        if ($request->ajax()) {
            return [
                'redirect' => url('/admin/invest-tasks'),
                'message' => trans('brackets/admin-ui::admin.operation.succeeded')
            ];
        }

        return redirect('/admin/invest-tasks');
    }
}
