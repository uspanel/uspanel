<?php namespace App\Http\Controllers\Admin;

use App\EmailMailing;
use App\HistoryMailing;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\EmailGroup\{IndexEmailGroup,
    ParseCsv,
    StoreEmailGroup,
    UpdateEmailGroup,
    DestroyEmailGroup};
use Brackets\AdminListing\Facades\AdminListing;
use App\EmailGroup;
use ParseCsv\Csv;

class EmailGroupsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexEmailGroup $request
     * @return Response|array
     */
    public function index(IndexEmailGroup $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(EmailGroup::class)->processRequestAndGet(
        // pass the request with params
            $request,

            // set columns to query
            ['id', 'name'],

            // set columns to searchIn
            ['id', 'name']
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.email-group.index', [
            'data' => $data,
            'viewName' => 'email-group'
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('crud.email-group.create');

        return view('admin.email-group.create', [
            'viewName' => 'email-group'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreEmailGroup $request
     * @return Response|array
     */
    public function store(StoreEmailGroup $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the EmailGroup
        $emailGroup = EmailGroup::create($sanitized);

        $files = request('list', null);
        if($files){
            foreach($files AS $file){
                if ($file) {
                    $this->parseFile(storage_path('uploads/'.$file['path']), $emailGroup);
                }
            }
        }
        if ($request->ajax()) {
            return ['redirect' => url('/admin/email-groups'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/email-groups');
    }

    /**
     * Display the specified resource.
     *
     * @param EmailGroup $emailGroup
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(EmailGroup $emailGroup)
    {
        $this->authorize('crud.email-group.show', $emailGroup);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param EmailGroup $emailGroup
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(EmailGroup $emailGroup)
    {
        $this->authorize('crud.email-group.edit', $emailGroup);

        return view('admin.email-group.edit', [
            'emailGroup' => $emailGroup,
            'viewName' => 'email-group'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateEmailGroup $request
     * @param EmailGroup $emailGroup
     * @return Response|array
     */
    public function update(UpdateEmailGroup $request, EmailGroup $emailGroup)
    {
        // Sanitize input
        $sanitized = $request->validated();
        // Update changed values EmailGroup
        $emailGroup->update($sanitized);
        if ($request->ajax()) {
            return ['redirect' => url('/admin/email-groups'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/email-groups');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyEmailGroup $request
     * @param EmailGroup $emailGroup
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyEmailGroup $request, EmailGroup $emailGroup)
    {
        $emailGroup->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Parse uploaded CSV file and create email_mailings
     *
     * @param ParseCsv $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function parseCsv(ParseCsv $request)
    {
        $file = $request->file('list')->path();
        if ($id = $request->get('group_id')) {
            $emailGroup = EmailGroup::find($id);
        } elseif ($name = $request->get('group_name')) {
            $emailGroup = EmailGroup::create([
                'name' => $name
            ]);
        }
        $this->parseFile($file,$emailGroup);

        return redirect('/admin/email-groups');
    }

    /**
     * Parse CSV file and store result to DB
     *
     * @param string $file Path to CSV file
     * @param EmailGroup $emailGroup
     */
    protected function parseFile($file, $emailGroup):void
    {
        if(!file_exists($file)) return; // если файл не найден

        $csv = new Csv();
        $csv->heading = false;
        $csv->parse($file);
       
        foreach ($csv->data as $item) {
            if (!empty($item[0]) && !empty($item[1])) {
                EmailMailing::create([
                    'name' => $item[0],
                    'email' => $item[1],
                    'email_group_id' => $emailGroup->id
                ]);
            }
        }
        unlink($file);
    }

}
