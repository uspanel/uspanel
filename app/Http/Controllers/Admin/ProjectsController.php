<?php namespace App\Http\Controllers\Admin;

use App\CompanyType;
use App\Employee;
use App\Http\Controllers\Controller;
use App\Jobs\RegisterNewEmail;
use App\Mailsetting;
use App\MailsettingForProject;
use App\ProjectMailAccaunt;
use App\VestaMailDomain;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\Project\IndexProject;
use App\Http\Requests\Admin\Project\StoreProject;
use App\Http\Requests\Admin\Project\UpdateProject;
use App\Http\Requests\Admin\Project\DestroyProject;
use Brackets\AdminListing\Facades\AdminListing;
use App\Company;
use App\UserMailAccaunt;
use App\User;
use App\Project;
use App\State;
use App\Form;
use Illuminate\Support\Facades\Auth;

class ProjectsController extends Controller
{

    protected $filters = [
        'company' => [
            'name',
            'address',
            'city',
            'state_id',
            'zip',
            'phone',
            'fax',
            'site',
            'info_email',
            'manager_id',
            'support_id',
        ]
    ];
    /**
     * Display a listing of the resource.
     *
     * @param  IndexProject $request
     * @return Response|array
     */
    public function index(IndexProject $request)
    {
        // create and AdminListing instance for a specific model and
        $data = new Project;
        if (auth()->user()->hasRole('Employee Staff')) {
            $data = auth()->user()->project();
        }
        $data = $data->with('managers', 'supports', 'company.state')
        ->select('projects.*', 'companies.'.$request->get('orderBy', 'id'))
        ->leftJoin('companies', 'companies.id', '=', 'projects.company_id')
        ->whereHas('company', function ($query) use($request) {
            foreach ($request->only($this->filters['company']) as $key => $value) {
                $query->where($key, 'like', '%'.$value.'%');
            }
        })
        ->orderBy('companies.'.$request->get('orderBy', 'id'), $request->get('orderDirection', 'desc'))
        ->paginate();
        $hr = User::Role(['HR Manager Staff'])->get();
        $support = User::Role(['Support Staff'])->get();
        $result = [
            'data' => $data,
            'hr' => $hr,
            'support' => $support,
        ];
        if ($request->ajax()) {
            return $result;
        }
        return view('admin.project.index', $result);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('crud.project.create');

        $hrStaff = User::Role(['HR Manager Staff'])->get();
        $supportStaff = User::Role(['Support Staff'])->get();
        $hrInvest = User::Role(['HR Manager Invest'])->get();
        $supportInvest = User::Role(['Support Invest'])->get();

        return view('admin.project.create', [
            'hrStaff' => $hrStaff,
            'supportStaff' => $supportStaff,
            'hrInvest' => $hrInvest,
            'supportInvest' => $supportInvest,
            'companyTypes' => CompanyType::pluck('title', 'id'),
            'states' => State::all(),
            'forms' => Form::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreProject $request
     * @return Response|array
     */
    public function store(StoreProject $request)
    {
        // Sanitize input

        $sanitized = $request->validated();
        $company = Company::create($request->company);



        $project = Project::create([
            'name' => $request->name,
            'project_id' => $request->project_id,
            'salary' => $request->salary,
            'company_id' => $company->id,
            'mail_domain' => $request->get('mail_domain')
        ]);

        $check_if_domain_exist = VestaMailDomain::where('domain', $project->mail_domain)->first();
        if(!$check_if_domain_exist){
            $credentials = ['domain' => $project->mail_domain, 'project_id' => $project->id];

            $this->createMailDomenForProgect($credentials);
            $check_mail_accaunt_exist = ProjectMailAccaunt::where('project_id', $project->id)->first();
            if(!$check_mail_accaunt_exist){
                $mail_credentials = [
                    'name' => 'no_reply',
                    'password' => 'demo1234',
                    'domain' => $project->mail_domain,
                    'project_id' => $project->id ,
                ];
                $this->createMailProjectAccaunt($mail_credentials);

            }

        }

        $project->forms()->sync($request->form_ids);
        $this->syncUsers($request, $project);
        if ($request->ajax()) {
            return ['redirect' => url('admin/projects'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/projects');
    }
    public function createMailProjectAccaunt($mail_credentials){
        // Server credentials
        $vst_hostname = env('VESTA_HOSTNAME');
        $vst_username = env('VESTA_USERNAME');
        $vst_password = env('VESTA_PASSWORD');
        $vst_returncode = 'yes';
        $vst_command = 'v-add-mail-account';

// New Account
        $username = 'admin';
        $domain = $mail_credentials['domain'];
        $acount = $mail_credentials['name'];
        $password = $mail_credentials['password'];

// Prepare POST query
        $postvars = array(
            'user' => $vst_username,
            'password' => $vst_password,
            'returncode' => $vst_returncode,
            'cmd' => $vst_command,
            'arg1' => $username,
            'arg2' => $domain,
            'arg3' => $acount,
            'arg4' => $password
        );

        $postdata = http_build_query($postvars);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://' . $vst_hostname . ':8083/api/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
        $answer = curl_exec($curl);

// Check result
        if($answer != 0) {
            echo "Query returned error code: " .$answer. "\n";
        }else{
            ProjectMailAccaunt::create([
                'project_id' => $mail_credentials['project_id'],
                'project_domain' => $mail_credentials['domain'],
                'password' => $mail_credentials['password'],
                'user_accaunt' => $mail_credentials['name'] . '@' . $mail_credentials['domain']
            ]);

            MailsettingForProject::create([
                'smtp_host' => env('VESTA_HOSTNAME'),
                'smtp_port' => env('VESTA_SMTP_PORT'),
                'imap_host' => env('VESTA_HOSTNAME'),
                'imap_port' => env('VESTA_IMAP_PORT'),
                'imap_encryption' => env('VESTA_IMAP_ENCRYPTION'),
                'imap_sent_folder' => env('VESTA_IMAP_SENT_FOLDER'),
                'imap_inbox_folder' => env('VESTA_IMAP_INBOX_FOLDER'),
                'login' => $mail_credentials['name'] . '@' . $mail_credentials['domain'],
                'password' => $mail_credentials['password'],
                'default' => 1,
                'validate_cert' => 0,
                'project_id' => $mail_credentials['project_id'],
            ]);

        }
    }
    /**
     * Display the specified resource.
     *
     * @param  Project $project
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Project $project)
    {
        $this->authorize('crud.project.show', $project);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Project $project
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Project $project)
    {
        $this->authorize('crud.project.edit', $project);
        $project->append('form_ids', 'support_ids', 'manager_ids', 'hr_percent', 'support_percent');
        $hrStaff = User::Role(['HR Manager Staff'])->get();
        $supportStaff = User::Role(['Support Staff'])->get();
        $hrInvest = User::Role(['HR Manager Invest'])->get();
        $supportInvest = User::Role(['Support Invest'])->get();

        return view('admin.project.edit', [
            'project' => $project->load('company'),
            'hrStaff' => $hrStaff,
            'supportStaff' => $supportStaff,
            'hrInvest' => $hrInvest,
            'supportInvest' => $supportInvest,
            'companyTypes' => CompanyType::pluck('title', 'id'),
            'states' => State::all(),
            'forms' => Form::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateProject $request
     * @param  Project $project
     * @return Response|array
     */
    public function update(UpdateProject $request, Project $project)
    {
        // Sanitize input
        $sanitized = $request->validated();
        // Update changed values Project

        $project->update([
            'name' => $sanitized['name'],
            'salary' => $sanitized['salary'],
            'project_id' => $sanitized['project_id'],
            'mail_domain' => $sanitized['mail_domain']
        ]);
        $project->company()->update($sanitized['company']);
        $project->forms()->sync($request->form_ids);
        $this->syncUsers($request, $project);
        if ($request->ajax()) {
            return ['redirect' => url('admin/projects'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/projects');
    }

    /**
     * Sync users attached to project with employee percentage
     *
     * @param $request
     * @param Project $model
     */
    protected function syncUsers($request, $model)
    {
        $oldUsers = $model->users()->pluck('id')->toArray();
        $newUsers = [];
        $users = array_merge($request->get('manager_ids'),$request->get('support_ids'));
        foreach ($users  as $item) {
            if (!in_array($item,$oldUsers)) {
                $newUsers[] = $item;
            }
        }
        $model->users()->sync([]);
        foreach ($request->get('manager_ids') as $id) {
            $model->users()->attach([$id => ['responsible_percent' => $request->get('hr_percent')[$id]]]);
        }
        foreach ($request->get('support_ids') as $id) {
            $model->users()->attach([$id => ['responsible_percent' => $request->get('support_percent')[$id]]]);
        }
        foreach ($newUsers as $user) {
            dispatch(new RegisterNewEmail($user, $model->id));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyProject $request
     * @param  Project $project
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyProject $request, Project $project)
    {
        $project->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    public function getEmployees(Project $project) {
        if (!auth()->user()->hasRole('Administrator')) {
            $project = auth()->user()->projects()->findOrFail($project->id);
        }
        $employees = $project->employees()->with('user')->get()->pluck('user.name', 'user.id');
        return response([
            'employees' => $employees
        ]);
    }

    public function createMailDomenForProgect($credentials){
        // Server credentials
        $vst_hostname = env('VESTA_HOSTNAME');
        $vst_username = env('VESTA_USERNAME');
        $vst_password = env('VESTA_PASSWORD');
        $vst_returncode = 'yes';
        $vst_command = 'v-add-domain';

// New Domain
        $username = 'admin';
        $domain = $credentials['domain'];

// Prepare POST query
        $postvars = array(
            'user' => $vst_username,
            'password' => $vst_password,
            'returncode' => $vst_returncode,
            'cmd' => $vst_command,
            'arg1' => $username,
            'arg2' => $domain
        );


        $postdata = http_build_query($postvars);

// Send POST query via cURL
        $postdata = http_build_query($postvars);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://' . $vst_hostname . ':8083/api/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
        $answer = curl_exec($curl);

// Check result
        if(!$answer == 0) {
            echo "Query returned error code: " .$answer. "\n";
        }else{
            VestaMailDomain::create(['project_id' => $credentials['project_id'], 'domain' => $credentials['domain']]);
        }
    }



}
