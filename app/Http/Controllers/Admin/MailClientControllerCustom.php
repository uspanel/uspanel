<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Traits\Mailconnectable;
use App\User;
use CodersStudio\ImapMailClient\Http\Controllers\MailClientController;
use CodersStudio\ImapMailClient\Http\Requests\MailClientSendRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

class MailClientControllerCustom extends MailClientController
{
    use Mailconnectable;

    public function send(MailClientSendRequest $request)
    {

        $this->setSettings($request->get('set_id', null));

        if ($this->mailsettings->owner) {
            $name = User::findOrFail($this->mailsettings->owner)->name;
        } else {
            $name = $this->mailsettings->user->name;
        }


        if($request->get('type_mail') == 'text/plain'){
            $custom_message = $request->get('text');
        } elseif($request->get('type_mail') == 'text/html'){
            $custom_message = $request->get('message');
        }


        $config = array(
            'driver' => 'smtp',
            'host' => $this->mailsettings->smtp_host,
            'port' => $this->mailsettings->smtp_port,
            'from' => array('address' => $this->mailsettings->login, 'name' => $name),
            'encryption' => $this->mailsettings->smtp_encryption,
            'username' => $this->mailsettings->login,
            'password' => $this->mailsettings->plain_password,
            'sendmail' => '/usr/sbin/sendmail -bs',
            'pretend' => false,
        );
        Config::set('mail', $config);

        $msgId = $request->get('message_id', null);
        $folder = $request->get('folder', null);
        $locMsg = null;
        $fwdApp = null;
        if ($msgId) {
            $locMsg = $this->mailsettings->mailmessages()
                ->where('uid', '=', $msgId)
                ->where('folder', '=', $folder)
                ->firstOrFail();
            $fwdApp = preg_replace('/\[sender\]/', htmlentities($locMsg->sender), config('csmailclient.forwarding_app'));
        }

        foreach ($request->get('recipients') as $recipient) {
            try {
                Mail::send([], [], function ($message) use ($request, $recipient, $locMsg, $fwdApp, $custom_message) {
                    $message->to($recipient)
                        ->subject(($locMsg ? 'Fwd: ' : '') . $request->get('subject'))
                        ->setBody(($locMsg ? $fwdApp : '') . $custom_message, $request->get('type_mail'));
                    if ($atts = $this->extractAttachments($request)) {
                        foreach ($atts as $attachment) {
                            $message->attach($attachment['pathname'], [
                                'as' => $attachment['filename'],
                                'mime' => $attachment['mime']
                            ]);
                        }
                    }
                });
                $this->moveToSent($recipient, $request, $fwdApp);
            } catch (\Exception $e) {
                if ($request->ajax()) {
                    return Response::json(['errors' => [$e->getMessage()]], 400);
                }
                return Redirect::back()->withErrors(['error', $e->getMessage()]);
            }
        }
        if ($request->ajax()) {
            return Response::json(['data' => []]);
        }

        return Redirect::back();

    }
}
