<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\{
	Warehousable,
	Warehouse,
	Item
};

class WarehousablesController extends Controller
{
    public function index(Request $request, Warehouse $warehouse)
    {
    	$data = $warehouse->items();
    	$data = $data->orderBy($request->get('orderBy', 'id'), $request->get('orderDirection', 'desc'))->groupBy('items.id')->paginate($request->get('per_page', 10));
        foreach ($data as &$item) {
            $item->pivot->qty = $warehouse->getItemQty($item->id);
        }
    	if ($request->ajax()) {
    		return response([
    			'data' => $data
    		]);
        }
    	return view('admin.warehousable.index', [
    		'data' => $data,
    		'warehouse' => $warehouse,
    	]);
    }

    public function itemInfo(Request $request, Warehouse $warehouse, Item $item)
    {
        $data = $warehouse->items();
            if (!empty($request->get('pivot_qty'))) {
                $data = $data->wherePivot('qty','=',$request->get('pivot_qty'));
            }
            if (!empty($request->get('pivot_warehousable_type'))) {
                $data = $data->wherePivot('warehousable_type','=',$request->get('pivot_warehousable_type'));
            }
            if (!empty($request->get('pivot_created_at'))) {
                $data = $data->wherePivot('created_at','like','%'.$request->get('pivot_created_at').'%');
            }
        $data = $data->orderBy($request->get('orderBy', 'id'), $request->get('orderDirection', 'asc'))
        ->paginate($request->get('per_page', 10));
        if ($request->ajax()) {
            return response([
                'data' => $data
            ]);
        }
        return view('admin.warehousable.item', [
            'data' => $data,
            'item' => $item,
            'warehouse' => $warehouse,
        ]);
    }
}
