<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\{GotoChat,
    IndexUser,
    StoreUser,
    UpdateUser,
    DestroyUser,
    BlacklistUser,
    NeedCallsIndex,
    UpdateStatus};
use App\{ActivityLog,
    Contragent,
    ContragentList,
    Event,
    InvestorList,
    InvestTask,
    InvestTaskTemplate,
    Jobs\RegisterNewEmail,
    Mailsetting,
    Package,
    User,
    Employee,
    Profile,
    Project,
    State,
    UserMailAccaunt,
    UserStatus,
    Blacklist,
    Form};
use Illuminate\Support\Facades\{
    Auth,
    Schema,
    DB,
    Notification
};
use Illuminate\Http\{
    Request,
    Response
};
use App\Notifications\{
    UserBlacklisted,
    UserApproved
};
use App\Http\Requests\Admin\Employee\IndexEmployee;
use CodersStudio\FormCreator\Traits\FormCreatable;
use Brackets\AdminListing\Facades\AdminListing;
use Spatie\Permission\Models\Role;
use App\Http\Requests\Interview;
use Spatie\MediaLibrary\Media;
use Carbon\Carbon;

class UsersController extends Controller
{
    use FormCreatable;

    protected $guard = 'web';

    protected $role = null;

    protected $filters = [
        'last_name',
        'address',
        'email',
        'roles',
        'created_at',
        'phone_number',
        'id'
    ];

    /**
     * Display a listing of the resource.
     *
     * @param IndexUser $request
     * @return Response|array
     */
    public function index(IndexUser $request)
    {

        return $this->returnData($request);
    }

    public function getUsersByRole(Role $role, IndexEmployee $request)
    {
        $request->merge(
            [
                'roles' => $role->id,
            ]
        );
        $this->role = $role;
        return $this->returnData($request, $role);
    }

    public function returnData(Request $request, $role = false)
    {


        $result = [
            'data' => $this->prepareData($request),
            'roles' => Role::all(),
            'role' => $role,
            'investors' => User::getRoleForSelect('Investor') ?? collect(),
            'investor_lists' => InvestorList::all(),
            'contragents' => Contragent::all(),
            'contragent_lists' => ContragentList::all(),
            'invest_tasks' => InvestTaskTemplate::all(),
        ];
        if ($request->ajax()) {
            return $result;
        }
        return view('admin.user.index', $result);
    }

    /**
     * Prepare data for user listing
     *
     * @param $request
     * @param null|array $needCalls Array of status names included to "need calls" page
     * @return mixed
     */
    protected function prepareData(Request $request, $needCalls = null)
    {
        $excludedFromAutoprocessing = [
            'roles',
            'voicent_notes',
            'voicent_duration',
            'voicent_time',
            'voicent_status',
            'approved',
            'tasks',
            'employee_user_status_id',
            'name',
            'phone_number',
            'last_activity',
            'last_template',
            'project',
            'hr',
            'support',
            'cs',
        ];
        $columns = ['id', 'email', 'first_name', 'last_name', 'address', 'phone_number', 'created_at'];
        $orderByRel = $request->get('orderBy', '');
        if (in_array($orderByRel, $excludedFromAutoprocessing)) {
            $request = new Request($request->except('orderBy'));
        }

        $data = AdminListing::create(User::class)->processRequestAndGet(
        // pass the request with params
            $request,

            // set columns to query
            $columns,

            // set columns to searchIn
            ['id', 'name', 'email', 'first_name', 'last_name', 'address', 'phone_number'],

            function ($query) use ($request, $needCalls, $orderByRel, $columns) {
                $this->orderByRelations($request, $query, $orderByRel, $columns);

                $query->permissions()
                    ->with([
                        'employee.investors',
                        'employee.investorLists',
                        'employee.contragents',
                        'employee.contragentLists',
                        'employee.hr',
                        'employee.callSupport',
                        'employee.support',
                        'employee.project',
                        'roles',
                        'profile.state',
                        'employee.callSupport',
                        'employee.userStatus',
                        'projects',
                        'employee.investTasks' => function ($query) {
                            $query->orderBy('created_at', 'desc');
                        },
                        'activityLogs' => function ($query) {
                            $query->orderBy('created_at', 'desc');
                        },
                        'employee.voicentStats'

                    ])
                    ->whereRaw(
                        'concat(users.first_name, \' \', users.last_name) like \'%' . $request->get(
                            'first_name'
                        ) . '%\''
                    );

                foreach ($request->only($this->filters) as $key => $value) {
                    if (!$value) {
                        continue;
                    }
                    if ($key == 'id') {
                        $query->where('id', $value);
                    } else {
                        if (Schema::hasColumn('users', $key)) {
                            $query->where($key, 'like', '%' . $value . '%');
                        } else {
                            $query->whereHas(
                                $key,
                                function ($query) use ($value) {
                                    $query->where('id', $value);
                                }
                            );
                        }
                    }
                }
                if ($needCalls) {
                    $query->whereHas(
                        'employee',
                        function ($sq) use ($needCalls) {
                            $sq->whereHas(
                                'userStatus',
                                function ($ssq) use ($needCalls) {
                                    $ssq->whereIn('name', $needCalls);
                                }
                            );
                        }
                    );

                    if (Auth::user()->hasRole('Call support')) {
                        $query->whereHas(
                            'employee',
                            function ($subq) {
                                $subq->whereHas(
                                    'callSupport',
                                    function ($sq) {
                                        $sq->where('id', Auth::user()->id);
                                    }
                                )->orWhereDoesntHave('callSupport');
                            }
                        );
                    }
                }
                $projects = Auth::user()->projects->pluck('id')->toArray();
                if (!Auth::user()->hasRole([
                    'Administrator',
                    'Staffer',
                    'HR Manager Staff',
                    'HR Manager Invest',
                    'Support Staff',
                    'Employee Invest'
                ])) {
                    $query->where(
                        function ($subQuery) use ($projects) {
                            $subQuery->whereHas(
                                'projects',
                                function ($subq) use ($projects) {
                                    $subq->whereIn('id', $projects);
                                }
                            )
                                ->orWhereHas(
                                    'employee',
                                    function ($subq) use ($projects) {
                                        $subq->whereIn('project_id', $projects);
                                    }
                                );
                        }
                    );
                }
                if (auth()->user()->hasRole('Staffer')) {
                    $query->whereHas(
                        'employee',
                        function ($subQuery) {
                            $subQuery->whereHas(
                                'staffers',
                                function ($q) {
                                    $q->where('id', auth()->id());
                                }
                            );
                        }
                    );
                }
                if (auth()->user()->hasRole('HR Manager Staff')) {
                    $query->whereHas(
                        'employee',
                        function ($subQuery) {
                            $subQuery->whereHas(
                                'hr',
                                function ($q) {
                                    $q->where('id', auth()->id());
                                }
                            );
                        }
                    );
                }
                if (auth()->user()->hasRole('Support Staff')) {
                    $query->whereHas(
                        'employee',
                        function ($subQuery) {
                            $subQuery->whereHas(
                                'support',
                                function ($q) {
                                    $q->where('id', auth()->id());
                                }
                            );
                        }
                    );
                }
                if (auth()->user()->hasRole('Employee Invest')) {
                    $query->whereIn(
                        'id',
                        Auth::user()->employee()->first()->getUniqueInvestors()
                    );
                }
            }
        );

        foreach ($data as $user){
            if($user->hasRole('Investor')){
                $user->load('investorProfile');
            }
        }


        return $data;
    }

    protected function orderByRelations(Request $request, $query, $orderBy, $columns)
    {
        if ($orderBy === 'last_activity') {
            $query
                ->leftJoin(
                    DB::raw('(SELECT sal.causer_id, max(sal.created_at) mca from activity_log sal group by sal.causer_id) as al1'),
                    function ($join) {
                        $join->on('users.id', '=', 'al1.causer_id');
                    }
                )
                ->leftJoin(
                    'activity_log as al2',
                    function ($join) {
                        $join->on('users.id', '=', 'al2.causer_id');
                        $join->on('al1.mca', '=', 'al2.created_at');
                    }
                )
                ->groupBy('users.id')
                ->orderBy('al2.description', $request->get('orderDirection', 'asc'));
        }

        if ($orderBy === 'last_template') {
            $query
                ->join('employees as e', 'users.id', '=', 'e.user_id')
                ->join(
                    DB::raw('(SELECT st.employee_id, max(st.created_at) mca from invest_tasks st group by st.employee_id) as it1'),
                    function ($join) {
                        $join->on('e.id', '=', 'it1.employee_id');
                    }
                )
                ->join(
                    'invest_tasks as it2',
                    function ($join) {
                        $join->on('e.id', '=', 'it2.employee_id');
                        $join->on('it1.mca', '=', 'it2.created_at');
                    }
                )
                ->groupBy('users.id')
                ->orderBy('it2.name', $request->get('orderDirection', 'asc'));
        }

        if ($orderBy === 'project') {
            $query
                ->join('employees as e', 'users.id', '=', 'e.user_id')
                ->leftJoin('projects as p', 'p.id', '=', 'e.project_id')
                ->orderBy('p.name', $request->get('orderDirection', 'asc'));
        }

        if ($orderBy === 'hr') {
            $query
                ->join('employees as e', 'users.id', '=', 'e.user_id')
                ->leftJoin('users as u', 'u.id', '=', 'e.hr_id')
                ->orderBy('u.first_name', $request->get('orderDirection', 'asc'))
                ->orderBy('u.last_name', $request->get('orderDirection', 'asc'));
        }

        if ($orderBy === 'support') {
            $query
                ->join('employees as e', 'users.id', '=', 'e.user_id')
                ->leftJoin('users as u', 'u.id', '=', 'e.support_id')
                ->orderBy('u.first_name', $request->get('orderDirection', 'asc'))
                ->orderBy('u.last_name', $request->get('orderDirection', 'asc'));
        }

        if ($orderBy === 'cs') {
            $query
                ->join('employees as e', 'users.id', '=', 'e.user_id')
                ->leftJoin('users as u', 'u.id', '=', 'e.cs_id')
                ->orderBy('u.first_name', $request->get('orderDirection', 'asc'))
                ->orderBy('u.last_name', $request->get('orderDirection', 'asc'));
        }

        if ($orderBy === 'name') {
            $query
                ->orderBy('users.first_name', $request->get('orderDirection', 'asc'))
                ->orderBy('users.last_name', $request->get('orderDirection', 'asc'));
        }

        if ($orderBy === 'phone_number') {
            $query
                ->leftJoin('profiles as p', 'users.id', '=', 'p.user_id')
                ->orderBy('p.work_phone', $request->get('orderDirection', 'asc'))
                ->orderBy('p.home_phone', $request->get('orderDirection', 'asc'));
        }

        if ($orderBy === 'employee_user_status_id') {
            $query
                ->leftJoin('employees as e', 'users.id', '=', 'e.user_id')
                ->orderBy('e.user_status_id', $request->get('orderDirection', 'asc'));
        }

        if ($orderBy === 'tasks') {
            $cols = collect($columns)->map(function($column) {
                return "`users`.`$column`";
            });
            $cols->push('count(t.id) as t_count');
            $cols = $cols->toArray();

            if ($this->role->name === 'Employee Staff') {
                $query
                    ->selectRaw(implode(',', $cols))
                    ->leftJoin('employees as e', 'users.id', '=', 'e.user_id')
                    ->leftJoin('tasks as t', 'e.id', '=', 't.employee_id')
                    ->groupBy('users.id')
                    ->orderBy('t_count', $request->get('orderDirection', 'asc'));
            } else if ($this->role->name === 'Employee Invest') {
                $query
                    ->selectRaw(implode(',', $cols))
                    ->leftJoin('employees as e', 'users.id', '=', 'e.user_id')
                    ->leftJoin('invest_tasks as t', 'e.id', '=', 't.employee_id')
                    ->groupBy('users.id')
                    ->orderBy('t_count', $request->get('orderDirection', 'asc'));
            }
        }

        if ($orderBy === 'approved') {
            $query
                ->leftJoin('employees as e', 'users.id', '=', 'e.user_id')
                ->orderBy('e.user_status_id', $request->get('orderDirection', 'asc'));
        }

        if ($orderBy === 'voicent_notes') {
            $query
                ->leftJoin('employees as e', 'users.id', '=', 'e.user_id')
                ->leftJoin('employee_voicent_stats as evs', 'e.id', '=', 'evs.employee_id')
                ->orderBy('evs.notes', $request->get('orderDirection', 'asc'));
        }

        if ($orderBy === 'voicent_duration') {
            $query
                ->leftJoin('employees as e', 'users.id', '=', 'e.user_id')
                ->leftJoin('employee_voicent_stats as evs', 'e.id', '=', 'evs.employee_id')
                ->orderBy('evs.duration', $request->get('orderDirection', 'asc'));
        }

        if ($orderBy === 'voicent_time') {
            $query
                ->leftJoin('employees as e', 'users.id', '=', 'e.user_id')
                ->leftJoin('employee_voicent_stats as evs', 'e.id', '=', 'evs.employee_id')
                ->orderBy('evs.start_time', $request->get('orderDirection', 'asc'));
        }

        if ($orderBy === 'voicent_status') {
            $query
                ->leftJoin('employees as e', 'users.id', '=', 'e.user_id')
                ->leftJoin('employee_voicent_stats as evs', 'e.id', '=', 'evs.employee_id')
                ->orderBy('evs.status', $request->get('orderDirection', 'asc'));
        }

        if ($orderBy === 'roles') {
            $query
                ->join(
                    'model_has_roles as mhr',
                    function ($join) {
                        $join->on('users.id', '=', 'mhr.model_id');
                    }
                )
                ->groupBy('users.id')
                ->orderBy('mhr.role_id', $request->get('orderDirection', 'asc'));
        }
    }

    public function getNeedCallsList(NeedCallsIndex $request)
    {
        $data = $this->prepareData($request, ['applicant', 'voicemail', 'approved', 'rejected']);

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view(
            'admin.user.index',
            [
                'data' => $data,
                'roles' => Role::all(),
                'needCalls' => 1,
                'role' => false,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('crud.user.create');



        return view(
            'admin.user.create',
            [
                'roles' => Role::where('guard_name', $this->guard)->get(),
                'projects' => Project::all(),
                'states' => State::all(),
                'userStatuses' => UserStatus::all(),
                'employees' => User::getRoleForSelect('Employee Staff'),
                'employees_staff' => User::with('employee')->whereHas('roles', function ($query) {
                    $query->where('name', 'Employee Staff');
                })->get(),
                'employees_invest' => User::with('employee')->whereHas('roles', function ($query) {
                    $query->where('name', 'Employee Invest');
                })->get(),
                'staffers' => User::getRoleForSelect('Staffer'),
                'investors' => User::role('Investor')->get(),
                'contragents' => Contragent::all(),
                'investor_lists' => InvestorList::all(),
                'contragent_lists' => ContragentList::all(),
                'hr_manager_staff_list' => User::getRoleForSelect('HR Manager Staff'),
                'hr_manager_invest_list' => User::getRoleForSelect('HR Manager Invest'),
                'support_staff_list' => User::getRoleForSelect('Support Staff'),
                'support_invest_list' => User::getRoleForSelect('Support Invest'),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUser $request
     * @return Response|array
     */
    public function store(StoreUser $request)
    {

//        dd($request->all());


        // Sanitize input
        $sanitized = $request->validated();

        // Store the User
        $user = User::create($sanitized);
        $user->profile()->create($request->profile);

        // $user->roles()->sync(collect($request->input('roles', []))->map->id->toArray());
        $user->roles()->sync($request->get('role_id', []));

        if ($user->hasRole('Employee Staff')) {
            $user->employee()->create(
                [
                    'project_id' => $request->get('project_id'),
                ]
            );
        } elseif ($user->hasRole('Employee Invest')) {
            $user->employee()->create(
                [
                    'project_id' => $request->get('project_id'),
                ]
            );
            $this->syncInvestorsAndContragents($user, $sanitized);
        } elseif ($user->hasRole('Investor')) {
            $user->investorProfile()->create(
                $sanitized['investor_profile']
            );
            $mailsetting = $sanitized['investor_mail'];
            $mailsetting['default'] = 1;
            $user->mailsettings()->create($mailsetting);
            $mailsetting['default'] = User::findOrFail($sanitized['investor_profile']['manager_id'])->mailsettings()->count() ? 0 : 1;
            $mailsetting['owner'] = $user->id;
            User::findOrFail($sanitized['investor_profile']['manager_id'])->mailsettings()->create($mailsetting);
        } else {
            $projects = $request->get('project_id');
            if (!is_array($projects)) {
                $projects = [$projects];
            }
            $this->createNewEmails($user, $projects);
            $user->projects()->sync($projects);
        }

        $this->employeesToStaffer($user);

        $redirectUrl = url('/admin/users');
        if ($user->hasRole('Employee Staff')) {
            $redirectUrl = route('admin.role.user', ['role' => 5]);
        } elseif ($user->hasRole('Employee Invest')) {
            $redirectUrl = route('admin.role.user', ['role' => 6]);
        } elseif ($user->hasRole('Staffer')) {
            $redirectUrl = route('admin.role.user', ['role' => 4]);
        }

        if ($request->ajax()) {
            return [
                'redirect' => $redirectUrl,
                'message' => trans('brackets/admin-ui::admin.operation.succeeded')
            ];
        }

        return redirect($redirectUrl);
    }

    /**
     * Create email accounts for all newly attached projects
     * @param User $user
     * @param array $newProjects
     */
    protected function createNewEmails(User $user, array $newProjects)
    {
        $projects = $user->projects()->pluck('id')->toArray();
        foreach ($newProjects as $id) {
            if (!in_array($id, $projects)) {
                dispatch(new RegisterNewEmail($user->id, $id));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(User $user)
    {
        $this->authorize('crud.user.show', $user);
        return view('admin.user.show')
            ->with('user', $user)
            ->with('tasks', $user->employee->tasks()->paginate(15))
            ->with('viewName', 'task')
            ->with('rating', $user->getRatingPosition());
    }

    public function statistics()
    {
        return view('admin.user.show')
            ->with('user', auth()->user())
            ->with('tasks', auth()->user()->tasks()->paginate(15))
            ->with('viewName', 'task')
            ->with('rating', auth()->user()->getRatingPosition());
    }

    public function salary(User $user)
    {
        $salaries = $user->salaries()->with('project');
        $carbon = new Carbon;
        if (!request()->has('from')) {
            request()->merge(
                [
                    'from' => $carbon->startOfWeek(Carbon::MONDAY)->toDateString(),
                ]
            );
        }
        if (!request()->has('to')) {
            request()->merge(
                [
                    'to' => $carbon->endOfWeek(Carbon::SUNDAY)->toDateString(),
                ]
            );
        }
        $salaries = $salaries
            ->whereDate('created_at', '<=', request()->get('to'))
            ->whereDate('created_at', '>=', request()->get('from'))->get();
        $chartData['categories'] = $salaries->pluck('created_at');
        $chartData['categories'] = $chartData['categories']->map(
            function ($item, $key) {
                return Carbon::parse($item)->format('m.d.Y');
            }
        );
        $chartData['data'] = $salaries->pluck('amount');
        return response(
            [
                'salaries' => $salaries,
                'params' => request()->toArray(),
                'chartData' => $chartData
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(User $user)
    {
        $this->authorize('crud.user.edit', $user);
        $fields = $user->fields;
        if ($fields->isEmpty() && $user->hasRole('Employee Staff')) {
            $fields = $user->form()->first()->fields;
        }
        $user = User::
        with(
            [
                'roles',
                'profile',
                'investorProfile',
                'employee',
                'employee.investors',
                'employee.contragents',
                'employee.investorLists',
                'employee.contragentLists',
            ]
        )
            ->findOrFail($user->id)
            ->append('project_id', 'employees', 'role_id');
        $hr = null;
        if ($hr != $user->getEmployeeRelationForSelect('hr')) {
            $hr = $hr[0];
        }
        $support = null;
        if ($support = $user->getEmployeeRelationForSelect('support')) {
            $support = $support[0];
        }
        if ($user->hasRole('Investor')) {
            $user->investor_mail =  $user->mailSettings()->first() ?: [];
        }

        $allowedRoleSwitches = [
            'Employee Staff' => [
                'Employee Invest'
            ],
            'Employee Invest' => [
                'Employee Staff'
            ],
            'HR Manager Staff' => [
                'HR Manager Invest'
            ],
            'HR Manager Invest' => [
                'HR Manager Staff'
            ],
            'Support Staff' => [
                'Support Invest'
            ],
            'Support Invest' => [
                'Support Staff'
            ],
        ];
        $roles = Role::where('guard_name', $this->guard)->get()->filter(function($role) use ($allowedRoleSwitches, $user) {
            return $user->hasRole($role->name) || in_array($role->name, $allowedRoleSwitches[$user->roles()->first()->name] ?? []);
        });

        return view(
            'admin.user.edit',
            [
                'extra' => [
                    'form' => [
                        'employee' => [
                            'staffers' => $user->getEmployeeRelationForSelect('staffers'),
                            'hr_id' => $hr,
                            'support_id' => $support,
                        ],
                        'employees' => $user->getStafferEmployeesForSelect(),
                    ]
                ],
                'user' => $user,
                'roles' => $roles,
                'projects' => Project::all(),
                'employees' => $user->getEmployeesForSelect(),
                'employees_staff' => User::with('employee')->whereHas('roles', function ($query) {
                    $query->where('name', 'Employee Staff');
                })->get(),
                'employees_invest' => User::with('employee')->whereHas('roles', function ($query) {
                    $query->where('name', 'Employee Invest');
                })->get(),
                'states' => State::all(),
                'fields' => $fields,
                'userStatuses' => UserStatus::all(),
                'staffers' => User::getRoleForSelect('Staffer'),
                'investors' => User::role('Investor')->get(),
                'contragents' => Contragent::all(),
                'investor_lists' => InvestorList::all(),
                'contragent_lists' => ContragentList::all(),
                'hr_manager_staff_list' => User::getRoleForSelect('HR Manager Staff'),
                'hr_manager_invest_list' => User::getRoleForSelect('HR Manager Invest'),
                'support_staff_list' => User::getRoleForSelect('Support Staff'),
                'support_invest_list' => User::getRoleForSelect('Support Invest'),
            ]
        );
    }

    public function createMailUserAccaunt($mail_credentials){
        // Server credentials
        $vst_hostname = env('VESTA_HOSTNAME');
        $vst_username = env('VESTA_USERNAME');
        $vst_password = env('VESTA_PASSWORD');
        $vst_returncode = 'yes';
        $vst_command = 'v-add-mail-account';

// New Account
        $username = 'admin';
        $domain = $mail_credentials['domain'];
        $acount = $mail_credentials['name'];
        $password = $mail_credentials['password'];

// Prepare POST query
        $postvars = array(
            'user' => $vst_username,
            'password' => $vst_password,
            'returncode' => $vst_returncode,
            'cmd' => $vst_command,
            'arg1' => $username,
            'arg2' => $domain,
            'arg3' => $acount,
            'arg4' => $password
        );

        $postdata = http_build_query($postvars);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://' . $vst_hostname . ':8083/api/');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
        $answer = curl_exec($curl);

// Check result
        if($answer != 0) {
            echo "Query returned error code: " .$answer. "\n";
        }else{
            UserMailAccaunt::create([
                'user_id' => $mail_credentials['user_id'],
                'project_id' => $mail_credentials['project_id'],
                'project_domain' => $mail_credentials['domain'],
                'password' => $mail_credentials['password'],
                'user_accaunt' => $mail_credentials['name'] . '@' . $mail_credentials['domain']
            ]);

            Mailsetting::create([
                'user_id' => $mail_credentials['user_id'],
                'smtp_host' => env('VESTA_HOSTNAME'),
                'smtp_port' => env('VESTA_SMTP_PORT'),
                'imap_host' => env('VESTA_HOSTNAME'),
                'imap_port' => env('VESTA_IMAP_PORT'),
                'imap_encryption' => env('VESTA_IMAP_ENCRYPTION'),
                'imap_sent_folder' => env('VESTA_IMAP_SENT_FOLDER'),
                'imap_inbox_folder' => env('VESTA_IMAP_INBOX_FOLDER'),
                'login' => $mail_credentials['name'] . '@' . $mail_credentials['domain'],
                'password' => $mail_credentials['password'],
                'default' => 1,
                'validate_cert' => 0,
            ]);

        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUser $request
     * @param User $user
     * @return Response|array
     */
    public function update(UpdateUser $request, User $user)
    {


        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values User
        if ($user->approved != $request->approved) {
            Notification::send(User::role(['Administrator'])->get(), new UserApproved($user));
        }
        $user->update($sanitized);
        if ($user->profile) {
            $user->profile()->update($request->profile);
        }
        if ($request->get('role_id')) {
            // Если несколько ролей
            // $user->roles()->sync(collect($request->input('roles', []))->map->id->toArray());
            $user->roles()->sync($request->get('role_id', []));
        }
        if ($e = $user->employee) {

            if($request->input('employee.user_status_id') == 4){
                $check_mail_accaunt_exist = UserMailAccaunt::where('user_id', $user->id)->first();
                if(!$check_mail_accaunt_exist){
                    $project = Project::find($request->project_id);
                    $name = str_replace(' ', '_', strtolower($user['first_name'])) . '_' . str_replace(' ', '_', strtolower($user['last_name']));
                    $mail_credentials = [
                        'name' => $name,
                        'password' => 'demo1234',
                        'domain' => $project->mail_domain,
                        'project_id' => $project->id ,
                        'user_id' => $user->id
                    ];
                    $this->createMailUserAccaunt($mail_credentials);
                }
            }

            $e->update(
                [
                    'user_status_id' => $request->input('employee.user_status_id'),
                    'hr_id' => $request->input('employee.hr_id'),
                    'support_id' => $request->input('employee.support_id'),
                    'project_id' => $request->input('project_id'),
                ]
            );
        } elseif ($user->investorProfile) {
            $user->investorProfile->update(
                $sanitized['investor_profile']
            );
            $projects = $request->get('project_id');
            if (!is_array($projects)) {
                $projects = [$projects];
            }
            $user->projects()->sync($projects);
            $mailsetting = $sanitized['investor_mail'];
            $mailsetting['default'] = 1;
            $user->mailsettings()->first()->update($mailsetting);
            $mailsetting['default'] = User::findOrFail($sanitized['investor_profile']['manager_id'])->mailsettings()->count() ? 0 : 1;
            $mailsetting['owner'] = $user->id;
            User::findOrFail($sanitized['investor_profile']['manager_id'])->mailsettings()->updateOrCreate(['login' => $mailsetting['login']],$mailsetting);
        } else {
            $projects = $request->get('project_id');
            if (!is_array($projects)) {
                $projects = [$projects];
            }
            $this->createNewEmails($user, $projects);
            $user->projects()->sync($request->get('project_id'));
        }
        $this->employeesToStaffer($user);

        if ($user->hasRole('Employee Invest')) {
            $this->syncInvestorsAndContragents($user, $sanitized);
        }


        $redirectUrl = url('admin/users');

        if ($user->hasRole('Employee Staff')) {
            $redirectUrl = route('admin.role.user', ['role' => 5]);
        } elseif ($user->hasRole('Employee Invest')) {
            $redirectUrl = route('admin.role.user', ['role' => 6]);
        } elseif ($user->hasRole('Staffer')) {
            $redirectUrl = route('admin.role.user', ['role' => 4]);
        }



        if ($request->ajax()) {
            return [
                'redirect' => $redirectUrl,
                'message' => trans('brackets/admin-ui::admin.operation.succeeded')
            ];
        }

        return redirect($redirectUrl);
    }

    /**
     * sync investors, contragents and do related actions
     *
     * @param User $user
     * @param array $sanitized
     */
    protected function syncInvestorsAndContragents(User $user, array $sanitized)
    {
        $investors = $sanitized['employee']['investors'] ?? false;
        $investorLists = $sanitized['employee']['investor_lists'] ?? false;
        $contragents = $sanitized['employee']['contragents'] ?? false;
        $contragentLists = $sanitized['employee']['contragent_lists'] ?? false;
        $employee = $user->employee;

        if (is_array($investors)) {
            $changes = $employee->investors()->sync(collect($investors)->pluck('id'));
            if (!empty($changes['attached'])) {
                foreach ($changes['attached'] as $a) {
                    $investor = User::find($a);
                    Event::doActions(
                        'UserGotNewInvestor',
                        [
                            'message' => trans(
                                'notifications.user_got_new_investor',
                                [
                                    'user' => $user->name,
                                    'investor' => $investor->name
                                ]
                            ),
                            'link' => '/admin/users?id=' . $user->id,
                            'business_process_type' => 'invest'
                        ]
                    );

                }
            }
        }
        if (is_array($contragents)) {
            $changes = $employee->contragents()->sync(collect($contragents)->pluck('id'));
            if (!empty($changes['attached'])) {
                foreach ($changes['attached'] as $a) {
                    $contragent = Contragent::find($a);
                    activity()
                        ->withContragent($contragent)
                        ->on($user)
                        ->log(trans(
                            'notifications.user_got_new_contragent',
                            [
                                'user' => $user->name,
                                'contragent' => $contragent->name
                            ]
                        ));
                    Event::doActions(
                        'UserGotNewContragent',
                        [
                            'message' => trans(
                                'notifications.user_got_new_contragent',
                                [
                                    'user' => $user->name,
                                    'contragent' => $contragent->name
                                ]
                            ),
                            'link' => '/admin/users?id=' . $user->id,
                            'business_process_type' => 'invest'
                        ]
                    );
                }
            }
        }
        if (is_array($investorLists)) {
            $employee->investorLists()->sync(collect($investorLists)->pluck('id'));
        }
        if (is_array($contragentLists)) {
            $employee->contragentLists()->sync(collect($contragentLists)->pluck('id'));
        }
    }

    public function employeesToStaffer($user)
    {
        if ($user->hasRole('Staffer')) {
            $user->stafferGroup()->sync(request()->get('employees'));
        } else {
            if ($user->hasRole('Employee Staff')) {
                $user->employee->staffers()->sync(request()->input('employee.staffers'));
            }
        }
    }

    /**
     * Take applicant call
     *
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function takeEmployee(User $user)
    {
        $employee = $user->employee;
        if (!$employee) {
            abort(404);
        }
        if ($employee->cs_id != null) {
            return response()->json(['message' => trans('admin.needcalls.errors.not_yours')], 400);
        }
        $employee->update(
            [
                'cs_id' => Auth::user()->id
            ]
        );
        return response()->json([], 204);
    }

    /**
     * Change user status and comment while employment process
     *
     * @param UpdateStatus $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus(UpdateStatus $request, User $user)
    {
        $employee = $user->employee;
        if (!$employee) {
            abort(404);
        }
        if ($employee->cs_id != Auth::user()->id) {
            return response()->json(['message' => trans('admin.needcalls.errors.not_yours')], 400);
        }
        $empl = $request->get('employee');
        $employee->update(
            [
                'user_status_id' => $empl['user_status_id'],
                'call_support_comment' => $empl['call_support_comment'] ?: null,
            ]
        );
        return response()->json([], 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyUser $request
     * @param User $user
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyUser $request, User $user)
    {
        foreach ($user->warehouses as $warehouse) {
            if ($warehouse->allItemsBalance() > 0) {
                if ($request->ajax()) {
                    return response()->json(
                        ['message' => trans('admin.user.errors.user_has_not_empty_warehouse')],
                        400
                    );
                }
                return redirect()->back();
            }
        }
        $user->delete();
        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Get Application Media
     * @param string $mediaCollection
     * @return MediaCollection
     */
    public function getMedia(User $user, string $mediaCollection)
    {
        return response(
            [
                'media' => $user->getMedia($mediaCollection)
            ]
        );
    }

    /**
     * Download Application Media
     * @param string $mediaCollection
     * @param Media $media
     * @return
     */
    public function downloadMedia(User $user, string $mediaCollection, Media $media)
    {
        $media = $user->getMedia($mediaCollection)->where('id', $media->id)->first();
        return response()->download($media->getPath(), $media->file_name);
    }

    public function toBlacklist(BlacklistUser $request, User $user)
    {
        Blacklist::create(
            [
                'email' => $user->email,
                'phone' => $user->phone_number,
            ]
        );
        Notification::send(User::role(['Administrator'])->get(), new UserBlacklisted($user));
        $user->delete();
        return response([]);
    }

    public function updateApproved(User $user, Request $request)
    {
        $this->authorize('crud.user.columns.employee_user_status_id.write', $user);
        $request->validate(
            [
                'approved' => 'required|boolean',
            ]
        );
        $employee = $user->employee;
        $employee->user_status_id = $request->get('approved', false) ? UserStatus::ofName('approved')->first(
        )->id : UserStatus::ofName('rejected')->first()->id;
        $employee->save();


//        $mailsetting = Auth::user()->localmailsettings()->create($sanitized);





        return response(
            [
                'message' => trans('admin.user.сhanges_saved')
            ]
        );
    }

    public function saveInterview(Interview $request, User $user)
    {
        $form = $user->form()->first();
        $this->validate($request, $form->getRules());
        if ($user->employee->interviewed) {
            $this->updateCustomFields($request, $user, $form);
        } else {
            $this->storeCustomFields($request, $user, $form);
            $user->employee->update(
                [
                    'interviewed' => 1
                ]
            );
        }
        return response(
            [
                'fields' => $user->fields
            ]
        );
    }

    public function getForm(User $user, Form $form)
    {
        $fields = $user->fields;
        if ($fields->isEmpty()) {
            $fields = $form->fields;
        }
        $form = $fields->mapWithKeys(
            function ($item) {
                return [$item['name'] => $item['value']];
            }
        );
        return response(
            [
                'form' => $form,
                'fields' => $fields,
            ]
        );
    }

    public function saveForm(Request $request, User $user, Form $form)
    {
        $this->validate($request, $form->getRules());
        if ($user->employee->interviewed) {
            $this->updateCustomFields($request, $user, $form);
        } else {
            $this->storeCustomFields($request, $user, $form);
            $user->employee->update(
                [
                    'interviewed' => 1
                ]
            );
        }
        return response(
            [
                'fields' => $user->fields
            ]
        );
    }

    /**
     * Get Items with quantity available for given employee
     *
     * @param Request $request
     * @param Employee $employee
     * @return mixed
     */
    public function getAvailableItems(Request $request, Employee $employee)
    {
        $items = $employee->items()->groupBy('items.id')->paginate($request->get('per_page', 10));
        foreach ($items as &$item) {
            $item->pivot->qty = $employee->getItemQty($item->id);
        }
        return response(
            [
                'data' => $items
            ]
        );
    }

    public function getPackageItems(Request $request, Employee $employee, Package $package)
    {
        $items = $package->items()->groupBy('items.id')->paginate($request->get('per_page', 10));

        foreach ($items as &$item) {
            $item->pivot->qty = $employee->getItemQty($item->id);
        }
        return response(
            [
                'data' => $items
            ]
        );
    }

    /**
     * Create new chat with given user
     * @param GotoChat $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function gotoChat(GotoChat $request)
    {
        $newChat = Auth::user()->createChatroom(['chatroom' => [],'type' => 'private', 'users' => [$request->get('id')]]);
        return response()->json(['data' => ['id' => $newChat->id]]);
    }

    public function painRow(Request $request){

        $employee = Employee::withTrashed()->where('user_id', $request->user_id)->first();
        $employee->color = $request->color;
        $employee->save();
    }

}
