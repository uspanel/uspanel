<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\MailTemplate;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\AutoAnswer\IndexAutoAnswer;
use App\Http\Requests\Admin\AutoAnswer\StoreAutoAnswer;
use App\Http\Requests\Admin\AutoAnswer\UpdateAutoAnswer;
use App\Http\Requests\Admin\AutoAnswer\DestroyAutoAnswer;
use Brackets\AdminListing\Facades\AdminListing;
use App\AutoAnswer;
use Illuminate\Support\Facades\Auth;

class AutoAnswersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexAutoAnswer $request
     * @return Response|array
     */
    public function index(IndexAutoAnswer $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(AutoAnswer::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'created_by', 'mailsetting_id'],

            // set columns to searchIn
            ['id'],
            function ($query) use ($request){
                $query->with(['user','mailsetting.owner', 'mailTemplates']);
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.auto-answer.index', ['data' => $data]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('crud.auto-answer.create');
        $tmplCollection = MailTemplate::all();
        foreach ($tmplCollection as &$tmpl) {
            $tmpl->pivot = ['message_count' => null, 'delay' => null];
        }
        return view('admin.auto-answer.create',[
            'ownedMailSettings' => Auth::user()->mailsettings()->whereNotNull('owner')->get(),
            'mailtemplates' => $tmplCollection
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreAutoAnswer $request
     * @return Response|array
     */
    public function store(StoreAutoAnswer $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the AutoAnswer
        $autoAnswer = AutoAnswer::create(['mailsetting_id' => $sanitized['mailsetting_id']]);
        $this->attachTemplates($autoAnswer, $sanitized['mail_templates']);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/auto-answers'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/auto-answers');
    }

    /**
     * Attach answer templates
     * @param AutoAnswer $autoAnswer
     * @param array $templates
     */
    protected function attachTemplates(AutoAnswer $autoAnswer, array $templates)
    {
        foreach ($templates as $template) {
            $autoAnswer->mailTemplates()->attach([
                $template['id'] => [
                    'message_count' => $template['pivot']['message_count'],
                    'delay' => $template['pivot']['delay'],
                ],
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  AutoAnswer $autoAnswer
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(AutoAnswer $autoAnswer)
    {
        $this->authorize('admin.auto-answer.show', $autoAnswer);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  AutoAnswer $autoAnswer
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(AutoAnswer $autoAnswer)
    {
        $this->authorize('crud.auto-answer.edit', $autoAnswer);
        $tmplCollection = MailTemplate::all();
        foreach ($tmplCollection as &$tmpl) {
            $tmpl->pivot = ['message_count' => null, 'delay' => null];
        }

        return view('admin.auto-answer.edit', [
            'autoAnswer' => $autoAnswer->load(['user','mailsetting.owner', 'mailTemplates']),
            'ownedMailSettings' => $autoAnswer->user->mailsettings()->whereNotNull('owner')->get(),
            'mailtemplates' => $tmplCollection
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateAutoAnswer $request
     * @param  AutoAnswer $autoAnswer
     * @return Response|array
     */
    public function update(UpdateAutoAnswer $request, AutoAnswer $autoAnswer)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values AutoAnswer
        $autoAnswer->update(['mailsetting_id' => $sanitized['mailsetting_id']]);
        $autoAnswer->mailTemplates()->sync([]);
        $this->attachTemplates($autoAnswer, $sanitized['mail_templates']);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/auto-answers'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/auto-answers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyAutoAnswer $request
     * @param  AutoAnswer $autoAnswer
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyAutoAnswer $request, AutoAnswer $autoAnswer)
    {
        $autoAnswer->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    }
