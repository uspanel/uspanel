<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\{
    Request,
    Response
};
use App\Http\Requests\Admin\ContragentList\{
    IndexContragentList,
    StoreContragentList,
    UpdateContragentList,
    DestroyContragentList
};
use Brackets\AdminListing\Facades\AdminListing;
use App\{
    ContragentList,
    Contragent
};

class ContragentListsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexContragentList $request
     * @return Response|array
     */
    public function index(IndexContragentList $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(ContragentList::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name'],

            // set columns to searchIn
            ['id', 'name'],
            function ($query) use ($request) {
                $query->with('contragents');
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.contragent-list.index', ['data' => $data]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('crud.contragent-list.create');

        $contragents = Contragent::all();

        return view('admin.contragent-list.create', ['contragents' => $contragents]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreContragentList $request
     * @return Response|array
     */
    public function store(StoreContragentList $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the ContragentList
        $contragentList = ContragentList::create($sanitized);

        foreach ($sanitized['contragents'] as $contragent) {
            $contragentList->contragents()->attach([
                $contragent['id']
            ]);
        }

        if ($request->ajax()) {
            return ['redirect' => url('/admin/contragent-lists'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/contragent-lists');
    }

    /**
     * Display the specified resource.
     *
     * @param  ContragentList $contragentList
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(ContragentList $contragentList)
    {
        $this->authorize('crud.contragent-list.show', $contragentList);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  ContragentList $contragentList
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(ContragentList $contragentList)
    {
        $this->authorize('crud.contragent-list.edit', $contragentList);

        $contragents = Contragent::all();

        $contragentList = ContragentList::with('contragents')->find($contragentList->id);

        return view('admin.contragent-list.edit', [
            'contragentList' => $contragentList,
            'contragents' => $contragents
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateContragentList $request
     * @param  ContragentList $contragentList
     * @return Response|array
     */
    public function update(UpdateContragentList $request, ContragentList $contragentList)
    {
        // Sanitize input
        $sanitized = $request->validated();

        $contragentList->contragents()->detach();

        foreach ($sanitized['contragents'] as $contragent) {
            $contragentList->contragents()->attach([
                $contragent['id']
            ]);
        }

        // Update changed values ContragentList
        $contragentList->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/contragent-lists'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/contragent-lists');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyContragentList $request
     * @param  ContragentList $contragentList
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyContragentList $request, ContragentList $contragentList)
    {
        $contragentList->contragents()->detach();

        $contragentList->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    }
