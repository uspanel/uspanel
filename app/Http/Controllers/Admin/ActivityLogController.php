<?php namespace App\Http\Controllers\Admin;

use App\ActivityLog;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\ActivityLog\IndexActivityLog;
use App\Http\Requests\Admin\ActivityLog\StoreActivityLog;
use App\Http\Requests\Admin\ActivityLog\UpdateActivityLog;
use App\Http\Requests\Admin\ActivityLog\DestroyActivityLog;
use Brackets\AdminListing\Facades\AdminListing;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use App\User;

class ActivityLogController extends Controller
{
    protected $filters = [
        'causer_id',
        'date_start',
        'date_end',
        'action',
    ];

    /**
     * Display a listing of the resource.
     *
     * @param  IndexActivityLog $request
     * @return Response|array
     */
    public function index(IndexActivityLog $request)
    {
        return $this->returnData($request);
    }

    /**
     * Undocumented function
     *
     * @param User $user
     * @param IndexActivityLog $request
     * @return mixed
     */
    public function getHistorysByUser(User $user, IndexActivityLog $request) {
        $request->merge([
            'users' => $user->id,
        ]);
        return $this->returnData($request, true);
    }

    /**
     * Undocumented function
     *
     * @param IndexActivityLog $request
     * @param boolean $byUser
     * @return mixed
     */
    public function returnData(IndexActivityLog $request, $byUser = false) {
        $result = [
            'data' => $this->prepareData($request),
            'users' => User::all(),
            'byUser' => $byUser
        ];
        if ($request->ajax()) {
            return $result;
        }

        return view('admin.activity-log.index', $result);
    }

    /**
     * Prepare data for user listing
     *
     * @param $request
     * @return mixed
     */
    protected function prepareData($request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(ActivityLog::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            [
                'id',
                'description',
                'ip_address',
                'created_at',
                'causer_id',
                'causer_type',
                'contragent_id',
            ],

            // set columns to searchIn
            ['id', 'description'],

            function ($query) use ($request) {
                $query->with(['contragent', 'causer']);

                if ($request->has('causer_id') && $request->get('causer_id')) {
                    $query->where('causer_id', '=', (int)$request->get('causer_id'));
                }
                if ($request->has('user_id') && $request->get('user_id')) {
                    $query->where('causer_id', '=', (int)$request->get('user_id'));
                }
                if ($request->has('date_start') && $request->get('date_start', false)) {
                    $query->where('created_at', '>=', $request->get('date_start'));
                }
                if ($request->has('date_end') && $request->get('date_end', false)) {
                    $query->where('created_at', '<=', $request->get('date_end'));
                }
                if ($request->has('contragent')) {
                    $query->whereHas('contragent', function ($query) use ($request) {
                        $query->where('name', 'like', "%{$request->get('contragent')}%");
                    });
                }
                if ($request->has('ip_address')) {
                    $query->where('ip_address', 'like', '%' . $request->get('ip_address') . '%');
                }
                if (!$request->has('orderBy')){
                    $query->orderBy('id', 'desc');
                }
            }
        );

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('crud.activity-log.create');

        return view('admin.activity-log.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreActivityLog $request
     * @return Response|array
     */
    public function store(StoreActivityLog $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the ActivityLog
        $activityLog = ActivityLog::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/activity-logs'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/activity-logs');
    }

    /**
     * Display the specified resource.
     *
     * @param  ActivityLog $activityLog
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(ActivityLog $activityLog)
    {
        $this->authorize('crud.activity-log.show', $activityLog);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  ActivityLog $activityLog
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(ActivityLog $activityLog)
    {
        $this->authorize('crud.activity-log.edit', $activityLog);

        return view('admin.activity-log.edit', [
            'activityLog' => $activityLog,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateActivityLog $request
     * @param  ActivityLog $activityLog
     * @return Response|array
     */
    public function update(UpdateActivityLog $request, ActivityLog $activityLog)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values ActivityLog
        $activityLog->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/activity-logs'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/activity-logs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyActivityLog $request
     * @param  ActivityLog $activityLog
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyActivityLog $request, ActivityLog $activityLog)
    {
        $activityLog->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    }
