<?php namespace App\Http\Controllers\Admin;

use App\BusinessProcess;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\Contragent\IndexContragent;
use App\Http\Requests\Admin\Contragent\StoreContragent;
use App\Http\Requests\Admin\Contragent\UpdateContragent;
use App\Http\Requests\Admin\Contragent\DestroyContragent;
use Brackets\AdminListing\Facades\AdminListing;
use App\Contragent;
use Illuminate\Support\Facades\Auth;

class ContragentsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexContragent $request
     * @return Response|array
     */
    public function index(IndexContragent $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Contragent::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name', 'investing_amount', 'user_id', 'country', 'roi'],

            // set columns to searchIn
            ['id', 'name', 'country'],

            function ($query) {
                if (Auth::user()->hasRole(['Employee Invest'])) {
                    $query->whereIn('id', Auth::user()->employee()->first()->getUniqueContragents());
                }
                $query->with(['media']);
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.contragent.index', ['data' => $data]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('crud.contragent.create');

        $businessProcesses = BusinessProcess::whereHas('businessProcessType', function ($query) {
            $query->where('name', 'invest');
        })->get();

        return view('admin.contragent.create', [
            'businessProcesses' => $businessProcesses
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreContragent $request
     * @return Response|array
     */
    public function store(StoreContragent $request)
    {
        // Sanitize input
        $sanitized = $request->validated();
        $sanitized['user_id'] = auth()->user()->id;

        // Store the Contragent
        $contragent = Contragent::create($sanitized);
        $bps = $request->get('business_processes', []);
        if ($bps) {
            $contragent->businessProcesses()->sync($bps);
        }

        if ($request->ajax()) {
            return ['redirect' => url('/admin/contragents'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/contragents');
    }

    /**
     * Display the specified resource.
     *
     * @param  Contragent $contragent
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Contragent $contragent)
    {
        $this->authorize('crud.contragent.show', $contragent);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Contragent $contragent
     * @return \Illuminate\Contracts\View\Factory|Response|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Contragent $contragent)
    {
        $contragent->load('businessProcesses');
        $this->authorize('crud.contragent.edit', $contragent);

        $businessProcesses = BusinessProcess::whereHas('businessProcessType', function ($query) {
            $query->where('name', 'invest');
        })->get();

        return view('admin.contragent.edit', [
            'contragent' => $contragent,
            'businessProcesses' => $businessProcesses
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateContragent $request
     * @param  Contragent $contragent
     * @return Response|array
     */
    public function update(UpdateContragent $request, Contragent $contragent)
    {
        // Sanitize input
        $sanitized = $request->validated();
        unset($sanitized['user_id']);
        // Update changed values Contragent
        $contragent->update($sanitized);

        $bps = $request->get('business_processes', []);
        $contragent->businessProcesses()->sync($bps);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/contragents'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/contragents');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyContragent $request
     * @param  Contragent $contragent
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyContragent $request, Contragent $contragent)
    {
        $contragent->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    }
