<?php namespace App\Http\Controllers\Admin;

use App\{Carrier,
    Employee,
    EmployeeStatus,
    Http\Requests\Admin\SalesTask\ForceDelivered,
    Http\Requests\Admin\Task\IndexTask,
    Project,
    SalesStatus,
    Task,
    User,
    Warehouse,
    Item};
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\SalesTask\IndexSalesTask;
use App\Http\Requests\Admin\SalesTask\StoreSalesTask;
use App\Http\Requests\Admin\SalesTask\UpdateSalesTask;
use App\Http\Requests\Admin\SalesTask\DestroySalesTask;
use Brackets\AdminListing\Facades\AdminListing;
use App\SalesTask;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class SalesTasksController extends Controller
{

    protected $filters = [
        'warehouse_id',
        'delivered_at',
        'carrier_id',
        'track',
        'carrier_tracking_status',
        'employee_status_id',
        'comment',
        'creator_id',
        'manager_id',
        'employee_id',
        'id'
    ];

    /**
     * Display a listing of the resource.
     *
     * @param IndexSalesTask $request
     * @return Response|array
     */
    public function index(IndexSalesTask $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(SalesTask::class)->processRequestAndGet(
        // pass the request with params
            $request,

            // set columns to query
            [
                'id',
                'delivered_at',
                'warehouse_id',
                'carrier_id',
                'track',
                'sku',
                'employee_status_id',
                'comment',
                'creator_id',
                'manager_id',
                'employee_id',
                'sales_status_id'
            ],

            // set columns to searchIn
            [
                'id',
                'carrier_id',
                'track',
                'sku',
                'comment',
                'employee_id'
            ],
            function ($query) use ($request) {
                $query->with('warehouse','manager','carrier','employee.user','creator','salesStatus','media');
                $this->filter($query, $request);
                if (Auth::user()->employee) {
                    $query->whereHas('employee', function ($sq) {
                        $sq->where('id', Auth::user()->employee->id);
                    });
                }
                if (Auth::user()->hasRole(['Staffer'])) {
                    $query->whereHas('employee', function ($sq) {
                        $sq->whereHas('staffers', function ($ssq) {
                            $ssq->where('id',Auth::user()->id);
                        });
                    });
                }
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.sales-task.index', [
            'data' => $data,
            'viewName' => 'sales-task',
            'users' => User::all()->pluck('name', 'id'),
            'managers' => User::Role('HR Manager Staff')->get()->pluck('name', 'id'),
            'carriers' => Carrier::pluck('title', 'id'),
            'employees' => Employee::whereHas('user')->with('user')->get()->pluck('user.name', 'id'),
            'warehouses' => Warehouse::pluck('name', 'id')
        ]);

    }

    public function filter($query, IndexSalesTask $request) {
        foreach ($request->only($this->filters) as $key => $value) {
            if(!$value){
                continue;
            }
            if(preg_match('/_at$/', $key)){
                $query->whereDate($key, $value);
            }elseif(preg_match('/_id$/', $key)){
                if ($key == 'warehouse_id') {
                    $query->whereHas('warehouse', function ($query) use ($value) {
                        $query->where('id', $value);
                    });
                }
                $query->where($key, $value);
            }elseif($key == 'id'){
                $query->where($key, $value);
            }else{
                $query->where($key, 'like', '%'.$value.'%');
            }
        }
        return $query;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('crud.task.view');
        $request->validate([
            'empl' => 'sometimes|exists:employees,id'
        ]);
        $projects = auth()->user()->projects;
        if (auth()->user()->hasRole('Staffer')) {
            $employees = Employee::whereHas('staffers', function ($query) {
                $query->where('id', Auth::user()->id);
            })->with('user')->get()->pluck('user.name', 'user.id');
        } else {
            $employees = Employee::with('user')->get()->pluck('user.name', 'user.id');
        }

        if (auth()->user()->hasRole('Administrator')) {
            $projects = Project::all();
            $employees = Employee::with('user')->get()->pluck('user.name', 'user.id');
        }
        $warehouse = null;
        $attachedItems = collect([]);
        if ($request->get('item_id') && $request->get('warehouse_id')) {
            $warehouse = Warehouse::findOrFail($request->get('warehouse_id'));
            $attachedItems = $warehouse
            ->items()
            ->where('items.id', $request->get('item_id'))
            ->groupBy('items.id')
            ->get();
            foreach ($attachedItems as &$item) {
                $item->pivot->qty = $warehouse->getItemQty($item->id);
            }
        }
        return view('admin.sales-task.create', [
            'users' => User::all()->pluck('name', 'id'),
            'managers' => User::Role('HR Manager Staff')->get()->pluck('name', 'id'),
            'carriers' => Carrier::pluck('title', 'id'),
            'employees' => $employees,
            'courierStatuses' => EmployeeStatus::get()->pluck('name', 'id'),
            'projects' => $projects,
            'warehouses' => Warehouse::with('user.employee')->get(),
            'employee' => $request->get('empl'),
            'attachedItems' => $attachedItems,
            'warehouse' => $warehouse,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreSalesTask $request
     * @return Response|array
     */
    public function store(StoreSalesTask $request)
    {
        // Sanitize input

        $sanitized = $request->validated();

        // Store the SalesTask
        $salesTask = SalesTask::create(array_merge($sanitized, ['creator_id' => Auth::user()->id, 'sales_status_id' => SalesStatus::where('name', '=', 'reserved')->first()->id]));
        $warehouse = Warehouse::findOrFail($request->get('warehouse_id'));
        foreach ($request->get('item_ids') as $key => $val) {
            $warehouse->morphSalesTasks()->attach([$salesTask->id => [
                'qty' => -1 * $val['qty'],
                'item_id' => $key,
                'created_at' => Carbon::now()
            ]]);
            $salesTask->items()->attach([$key => ['qty' => $val['qty'], 'price' => $val['price']]]);
        }
        if ($request->ajax()) {
            return ['redirect' => url('/admin/sales-tasks'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/sales-tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param SalesTask $salesTask
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(SalesTask $salesTask)
    {
        $this->authorize('crud.sales-task.show', $salesTask);

        return view('admin.sales-task.show', [
            'salesTask' => $salesTask
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param SalesTask $salesTask
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(SalesTask $salesTask)
    {
        $this->authorize('crud.sales-task.edit', $salesTask);
        $projects = auth()->user()->projects;
        if (auth()->user()->hasRole('Staffer')) {
            $employees = Employee::whereHas('staffers', function ($query) {
                $query->where('id', Auth::user()->id);
            })->with('user')->get()->pluck('user.name', 'user.id');
        } else {
            $employees = Employee::with('user')->get()->pluck('user.name', 'user.id');
        }
        // dd($salesTask->getThumbs200ForCollection('label'));
        return view('admin.sales-task.edit', [
            'salesTask' => $salesTask,
            'users' => User::all()->pluck('name', 'id'),
            'managers' => User::Role('HR Manager Staff')->get()->pluck('name', 'id'),
            'carriers' => Carrier::pluck('title', 'id'),
            'employees' => $employees,
            'courierStatuses' => EmployeeStatus::get()->pluck('name', 'id'),
            'projects' => $projects,
            'warehouses' => Warehouse::with('user.employee')->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSalesTask $request
     * @param SalesTask $salesTask
     * @return Response|array
     */
    public function update(UpdateSalesTask $request, SalesTask $salesTask)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values SalesTask
        $salesTask->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/sales-tasks'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/sales-tasks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroySalesTask $request
     * @param SalesTask $salesTask
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroySalesTask $request, SalesTask $salesTask)
    {
        $salesTask->warehouse->morphSalesTasks()->detach($salesTask->id);
        $salesTask->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Force salestask delivered without tracking
     *
     * @param ForceDelivered $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forceDelivered(ForceDelivered $request, $id)
    {
        $task = SalesTask::findOrFail($id);
        if ($task) {
            $task->update([
                'delivered_at' => Carbon::now()
            ]);
        }
        return response()->json([],204);
    }

    public function createCsv(Request $request)
    {
        $tasks = SalesTask::whereIn('id', $request->tasks_ids)->with('items')->get()->toArray();

        $csv_array = [];
        foreach ($tasks as $task){
            $csv_array[] = $this->getTaskRowForCsv($task);
        }
        $date = date('Y_d_m');
        $filename = 'tasks_'.$date.'.csv';
        $columns = ['track', 'items', 'sku'];
        $csv = implode(',', $columns) . "\n";//Column headers
        foreach ($csv_array as $record){
            $csv .= implode(',', $record) . "\n";//Append data to csv
        }

        if(!Storage::disk('public_uploads')->put($filename, $csv)) {
            return false;
        }

        return response()->json(['filename' => env('APP_URL'). '/csv/' . $filename],200);
    }

    private function getTaskRowForCsv($task){
//        $task_csv['label'] = '';
//        $task_csv['barcode'] = '';
//        $task_csv['reciept'] = '';
//        $task_csv['invoice'] = '';
//        $task_csv['carrier'] = $task['carrier']['name'];

        $task_csv['track'] = $task['track'];
        $task_csv['items'] = '';
        $task_csv['sku'] = $task['sku'];
//        foreach ($task['media'] as $media) {
//            $path = env('APP_URL') . '/' . $media['disk'] . '/' . $media['id'] . '/' . $media['file_name'];
//            $task_csv[$media['collection_name']] = $path;
//        }

        foreach ($task['items'] as $key => $item) {
            if($key == (count($task['items']) - 1)){
                $task_csv['items'] .= $item['name'];
            }else{
                $task_csv['items'] .= $item['name'] . '/';
            }
        }

        return $task_csv;
    }
}
