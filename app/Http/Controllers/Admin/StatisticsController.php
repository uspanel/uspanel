<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\{
    Request,
    Response
};
use App\Http\Requests\Admin\Statistic\{
    IndexStatistic,
    StoreStatistic,
    UpdateStatistic,
    DestroyStatistic
};
use Brackets\AdminListing\Facades\AdminListing;
use App\{
    Statistic,
    UserStatus,
    User,
    EmployeeStatus,
    DeliveryStatus,
    Mailing,
    MailingStatus,
    Profile
};

class StatisticsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexStatistic $request
     * @return Response|array
     */
    public function index(IndexStatistic $request)
    {
        $data['mailings'] = Mailing::with([
            'historyMailings.emailMailing.approvedUser'
        ])
        ->withCount([
            'historyMailings as approved' => function($query) {
                $query->whereHas('emailMailing.approvedUser');
            },
            'historyMailings as rejected' => function($query) {
                $query->whereHas('emailMailing.rejectedUser');
            },
            'historyMailings',
            'historyMailings as registered' => function ($query) {
                $query->where('mailing_status_id', MailingStatus::ofName('registered')->first()->id);
            }
        ])
        ->get();
        $data['userStatuses'] = UserStatus::withCount('employees')
        ->with('employees')
        ->get();
        $data['userStatuses'] = $data['userStatuses']->sortByDesc('employees_count');
        $data['userTasks'] = User::role('Employee Staff')
        ->select('id')
        ->whereHas('employee.tasks.activities')
        ->withCount([
            'tasks' => function ($query) {
                $query->where('employee_status_id', EmployeeStatus::ofName('sent')->first()->id);
            }
        ]);
        foreach (DeliveryStatus::all() as $value) {
            $data['userTasks'] = $data['userTasks']
            ->withCount([
                'tasks as delivery_status_' . $value->name => function ($query) use ($value) {
                    $query->where('delivery_status_id', $value->id);
                }
            ]);
        }
        $data['userTasks'] = $data['userTasks']->get()->sortByDesc('tasks_count');
        if ($request->ajax()) {
            return ['data' => $data];
        }
        $data['userReferrers'] = Profile::get()->groupBy('referrer');
        $data['userReferrers'] = $data['userReferrers']->each(function ($item, $index) {
            return $item->put('count', $item->count())->put('title', $index);
        });
        return view('admin.statistic.index', ['data' => $data]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('crud.statistic.create');

        return view('admin.statistic.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreStatistic $request
     * @return Response|array
     */
    public function store(StoreStatistic $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the Statistic
        $statistic = Statistic::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/statistics'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/statistics');
    }

    /**
     * Display the specified resource.
     *
     * @param  Statistic $statistic
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Statistic $statistic)
    {
        $this->authorize('crud.statistic.show', $statistic);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Statistic $statistic
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Statistic $statistic)
    {
        $this->authorize('crud.statistic.edit', $statistic);

        return view('admin.statistic.edit', [
            'statistic' => $statistic,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateStatistic $request
     * @param  Statistic $statistic
     * @return Response|array
     */
    public function update(UpdateStatistic $request, Statistic $statistic)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values Statistic
        $statistic->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/statistics'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/statistics');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyStatistic $request
     * @param  Statistic $statistic
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyStatistic $request, Statistic $statistic)
    {
        $statistic->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    }
