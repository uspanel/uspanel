<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\DkimKey\IndexDkimKey;
use App\Http\Requests\Admin\DkimKey\StoreDkimKey;
use App\Http\Requests\Admin\DkimKey\UpdateDkimKey;
use App\Http\Requests\Admin\DkimKey\DestroyDkimKey;
use Brackets\AdminListing\Facades\AdminListing;
use App\DkimKey;

class DkimKeysController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexDkimKey $request
     * @return Response|array
     */
    public function index(IndexDkimKey $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(DkimKey::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'project_id', 'domain', 'dkim_record'],

            // set columns to searchIn
            ['id', 'domain', 'dkim_record']
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.dkim-key.index', ['data' => $data]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('admin.dkim-key.create');

        return view('admin.dkim-key.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreDkimKey $request
     * @return Response|array
     */
    public function store(StoreDkimKey $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the DkimKey
        $dkimKey = DkimKey::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/dkim-keys'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/dkim-keys');
    }

    /**
     * Display the specified resource.
     *
     * @param  DkimKey $dkimKey
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(DkimKey $dkimKey)
    {
        $this->authorize('admin.dkim-key.show', $dkimKey);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  DkimKey $dkimKey
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(DkimKey $dkimKey)
    {
        $this->authorize('admin.dkim-key.edit', $dkimKey);

        return view('admin.dkim-key.edit', [
            'dkimKey' => $dkimKey,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateDkimKey $request
     * @param  DkimKey $dkimKey
     * @return Response|array
     */
    public function update(UpdateDkimKey $request, DkimKey $dkimKey)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values DkimKey
        $dkimKey->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/dkim-keys'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/dkim-keys');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyDkimKey $request
     * @param  DkimKey $dkimKey
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyDkimKey $request, DkimKey $dkimKey)
    {
        $dkimKey->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    }
