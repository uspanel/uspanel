<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use CodersStudio\Notifications\Notifications\System;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NewMailMessage;

class NotificationController extends Controller
{
    public function index() {
    	// Notification::send(auth()->user(), new NewMailMessage());
    	return view('admin.notification.index');
    }
}
