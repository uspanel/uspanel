<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Package\ForceDelivered;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\Package\{
    IndexPackage,
    StorePackage,
    UpdatePackage,
    DestroyPackage
};
use Brackets\AdminListing\Facades\AdminListing;
use App\{
    Package,
    Carrier,
    Status,
    User,
    Item,
    Nomenclature,
    Employee
};
use Illuminate\Support\Facades\Auth;

class PackageController extends Controller
{

    /**
     * fields for searching
     *
     * @var array
     */
    protected $filters = [
        'delivered_at',
        'track',
        'carrier_id',
        'weight',
        'name',
        'creator_id',
        'manager_id',
        'status_id',
        'employee_id',
        'id'
    ];

    /**
     * Display a listing of the resource.
     *
     * @param  IndexPackage $request
     * @return Response|array
     */
    public function index(IndexPackage $request)
    {
        $columns = Auth::user()
            ->getAvailableFields('package', 'read')
            ->prepend('id')
            ->filter(function ($item) {
                return !in_array($item, ['invoice', 'delivered_at', 'track', 'carrier_id']);
            })
            ->toArray();
        $filters = array_merge(session('filters.package', []), $request->only($this->filters));
        session()->put('filters.package', $filters);
        $request->merge($filters);

        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Package::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            $columns,

            // set columns to searchIn
            ['employee_id'],
            function ($query) use($request) {
                $query->orderBy('id', 'desc');
                $query->with(['status', 'creator', 'manager', 'items', 'employee.user'])
                    ->leftJoin('carriers', 'carrier_id', '=', 'carriers.id')
                    ->select('packages.*','carriers.name as carrier_name')
                    ->with(['lastActivity']);
                if (Auth::user()->employee) {
                    $query->whereHas('employee', function ($sq) {
                        $sq->where('id', Auth::user()->employee->id);
                    });
                }
                if (Auth::user()->hasRole(['Staffer'])) {
                    $query->whereHas('employee', function ($sq) {
                        $sq->whereHas('staffers', function ($ssq) {
                            $ssq->where('id',Auth::user()->id);
                        });
                    });
                }
                foreach ($this->filters as $filterFieldName) {
                    if(!$request->filled($filterFieldName)){
                        continue;
                    }
                    $value = $request->get($filterFieldName);
                    if($filterFieldName === 'name'){
                        $query->where('packages.name', 'like', '%'.$value.'%');
                    }elseif($filterFieldName === 'creator_id'){
                        $query->where('packages.creator_id', $value);
                    }elseif(preg_match('/_at$/', $filterFieldName)){
                        $query->whereDate($filterFieldName, $value);
                    }elseif(preg_match('/_id$/', $filterFieldName)){
                        $query->where($filterFieldName, $request->get($filterFieldName));
                    }elseif($filterFieldName === 'id'){
                        $query->where('packages.id', $request->get($filterFieldName));
                    }else{
                        $query->where($filterFieldName, 'like', '%'.$value.'%');
                    }
                }
            }
        );
        if ($request->ajax()) {
            return ['data' => $data];
        }
        $some = Employee::whereHas('user')->with('user')->get()->pluck('user.name', 'id');

        return view('admin.package.index', [
            'data' => $data,
            'viewName' => 'package',
            'statuses' => Status::pluck('title', 'id'),
            'managers' => User::Role('HR Manager Staff')->get()->pluck('name', 'id'),
            'carriers' => Carrier::pluck('title', 'id'), //пока оставляем
            'employees' => Employee::whereHas('user')->with('user')->get()->pluck('user.name', 'id'),
            'users' => User::all()->pluck('name', 'id'),
            'filters' => $filters
            ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('crud.package.view');
        $request->validate([
            'empl' => 'sometimes|exists:employees,id'
        ]);

        return view('admin.package.create', [
            'statuses' => Status::pluck('title', 'id'),
            'managers' => User::Role('HR Manager Staff')->get()->pluck('name', 'id'),
            'carriers' => Carrier::pluck('title', 'id'),
            'employee' => $request->get('empl')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StorePackage $request
     * @return Response|array
     */
    public function store(StorePackage $request)
    {
        $columns = Auth::user()->getAvailableFields('package', 'write')->toArray();



        $package = Package::create($request->only($columns));
        foreach ($request->get('item_ids',[]) as $value) {
            $item = Item::firstOrCreate(['name' => $value['name']],
                [
                    'size' => !empty($value['size']) ? $value['size'] : null,
                    'color' => !empty($value['color']) ? $value['color'] : null,
                    'info' => !empty($value['info']) ? $value['info'] : null,
                    'link' => !empty($value['link']) ? $value['link'] : null,
                    'weight' => !empty($value['weight']) ? $value['weight'] : null,
                    'user_id' => Auth::user()->id
                ]);
            $package->items()->attach([$item->id => ['qty' => $value['qty'], 'price' => $value['price']]]);
        }
        $redirect = $request->get('redirect');
        if (!$redirect) {
            $redirect = 'admin/packages';
        }
        if ($request->ajax()) {
            return ['redirect' => url($redirect), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect($redirect);
    }

    /**
     * Display the specified resource.
     *
     * @param  Package $package
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Package $package)
    {
        $this->authorize('crud.package.view', $package);

        return view('admin.package.show', [
            'package' => $package->load('items.user')
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Package $package
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Package $package)
    {
        $this->authorize('crud.package.view', $package);
        return view('admin.package.edit', [
            'package' => $package->append('item_ids')->load('items.user'),
            'statuses' => Status::pluck('title', 'id'),
            'managers' => User::Role('HR Manager Staff')->get()->pluck('name', 'id'),
            'carriers' => Carrier::pluck('title', 'id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdatePackage $request
     * @param  Package $package
     * @return Response|array
     */
    public function update(UpdatePackage $request, Package $package)
    {
        $columns = Auth::user()->getAvailableFields('package', 'write')->toArray();
        $package->update($request->only($columns));
        if (empty($package->delivered_at)) {
            $package->items()->sync([]);
            foreach ($request->get('item_ids',[]) as $value) {
                $item = Item::firstOrCreate(['name' => $value['name']],
                    [
                        'size' => !empty($value['size']) ? $value['size'] : null,
                        'color' => !empty($value['color']) ? $value['color'] : null,
                        'info' => !empty($value['info']) ? $value['info'] : null,
                        'link' => !empty($value['link']) ? $value['link'] : null,
                        'user_id' => Auth::user()->id
                    ]);
                $package->items()->attach([$item->id => ['qty' => $value['qty'], 'price' => $value['price']]]);
            }
        }
        if ($request->ajax()) {
            return ['redirect' => url('admin/packages'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/packages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyPackage $request
     * @param  Package $package
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyPackage $request, Package $package)
    {
        $package->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Force package delivered without tracking
     *
     * @param ForceDelivered $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forceDelivered(ForceDelivered $request, $id)
    {
        $package = Package::findOrFail($id);
        if ($package) {
            $package->update([
                'delivered_at' => Carbon::now()
            ]);
            $package->activities()->create([
                'title' => trans('admin.package.actions.manual_delivery'),
                'description' => trans('admin.package.actions.manual_delivery'),
                'city' => trans('admin.package.actions.manual_delivery'),
                'date' => Carbon::now()
            ]);
        }
        return response()->json([],204);
    }

}
