<?php

namespace App\Http\Controllers\Admin;

use App\EmailMailing;
use App\HistoryMailing;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Mailing\ApplyDelivery;
use App\Http\Requests\Admin\Mailing\ResendMailing;
use App\Http\Requests\Admin\Mailing\StopMailing;
use App\Http\Requests\Admin\Mailing\TestMailingTemplate;
use App\Jobs\MailingsDelivery;
use App\MailingSending;
use App\MailingStatus;
use App\Mailsetting;
use App\Notifications\TemplatizedMail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\Mailing\IndexMailing;
use App\Http\Requests\Admin\Mailing\StoreMailing;
use App\Http\Requests\Admin\Mailing\UpdateMailing;
use App\Http\Requests\Admin\Mailing\DestroyMailing;
use Brackets\AdminListing\Facades\AdminListing;
use App\Mailing;
use App\EmailGroup;
use App\MailTemplate;
use Illuminate\Support\Facades\Auth;

class MailingsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param IndexMailing $request
     * @return Response|array
     */
    public function index(IndexMailing $request)
    {

        $searchable = [
            'mail_template_id',
        ];
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Mailing::class)->processRequestAndGet(
        // pass the request with params
            $request,

            // set columns to query
            ['id', 'name', 'mail_template_id', 'scheduled_at'],

            // set columns to searchIn
            ['id', 'name'],
            function ($query) use ($searchable, $request) {
                $query->with('emailGroups', 'historyMailings')
                      ->with('mailTemplate');
                foreach ($searchable as $value) {
                    if (request()->get($value)) {
                        $query->where($value, request()->get($value));
                    }
                }
                if (request()->get('email_group_id')) {
                    $query->whereHas('emailGroups', function ($subQuery) {
                        $subQuery->where('id', request()->get('email_group_id'));
                    });
                }
                if (request()->get('scheduled_at_from')) {
                    $query->where('scheduled_at', '=>', request()->get('scheduled_at_from'));
                }
                if (request()->get('scheduled_at_to')) {
                    $query->where('scheduled_at', '<=', request()->get('scheduled_at_to'));
                }
            }
        );
        $mailTemplates = MailTemplate::all();
        $emailGroups = EmailGroup::all();
        $mail_settings = Mailsetting::where('for_meiling', 1)->get();

       if($data->count() == 0){

           $mail_setting = Mailsetting::where('for_meiling', 1)->first();
       }
        foreach ($data as $item){
            $history_mailing = HistoryMailing::where('mailing_id','=', $item->id)->first();
            if($history_mailing){
                $mail_setting = Mailsetting::find($history_mailing->set_id);
            }
        }


        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.mailing.index', [
            'data' => $data,
            'viewName' => 'mailing',
            'mailTemplates' => $mailTemplates,
            'emailGroups' => $emailGroups,
            'mail_settings' => $mail_settings,
            'mail_setting' => $mail_setting,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('crud.mailing.create');

        return view('admin.mailing.create', [
            'viewName' => 'mailing',
            'emailGroups' => EmailGroup::all(),
            'mailTemplates' => MailTemplate::pluck('name', 'id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreMailing $request
     * @return Response|array
     */
    public function store(StoreMailing $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the Mailing
        $mailing = Mailing::create($sanitized);
        $email_groups = collect(request('email_groups', []))->pluck('id');
        $mailing->emailGroups()->sync($email_groups);
        if ($request->ajax()) {
            return ['redirect' => url('/admin/mailings'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/mailings');
    }

    /**
     * Display the specified resource.
     *
     * @param Mailing $mailing
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Mailing $mailing)
    {
        $this->authorize('crud.mailing.show', $mailing);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Mailing $mailing
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Mailing $mailing)
    {
        $this->authorize('crud.mailing.edit', $mailing);

        return view('admin.mailing.edit', [
            'mailing' => $mailing->load(['emailGroups']),
            'viewName' => 'mailing',
            'emailGroups' => EmailGroup::all(),
            'mailTemplates' => MailTemplate::pluck('name', 'id')

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateMailing $request
     * @param Mailing $mailing
     * @return Response|array
     */
    public function update(UpdateMailing $request, Mailing $mailing)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values Mailing
        $mailing->update($sanitized);
        $email_groups = collect(request('email_groups', []))->pluck('id');
        $mailing->emailGroups()->sync($email_groups);
        if ($request->ajax()) {
            return ['redirect' => url('/admin/mailings'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/mailings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyMailing $request
     * @param Mailing $mailing
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyMailing $request, Mailing $mailing)
    {
        $mailing->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    /**
     * Apply mailing for delivery and create history_mailing records
     *
     * @param ApplyDelivery $request
     * @param Mailing $mailing
     * @return \Illuminate\Http\JsonResponse
     */
    public function applyDelivery(ApplyDelivery $request, Mailing $mailing)
    {
        $this->applyFunc($request->set_id, $mailing);
        return response()->json([],204);
    }

    /**
     * Apply mailing functionality
     *
     * @param Mailing $mailing
     * @param int $now
     */
    protected function applyFunc($mail_setting, $mailing, $now = 0)
    {
        $emailGroups = $mailing->emailGroups()->with('emailMailings')->get();

        $mailingSending = MailingSending::create([
                'scheduled_at' => $now ? Carbon::now()->subHour(1) : $mailing->scheduled_at,
                'max_per_min' => $mailing->max_per_min
            ]
        );

        foreach ($emailGroups as $emailGroup) {
            foreach ($emailGroup->emailMailings as $emailMailing) {
                HistoryMailing::create([
                    'mailing_id' => $mailing->id,
                    'email_mailing_id' => $emailMailing->id,
                    'mailing_status_id' => MailingStatus::where('name','=','new')->first()->id,
                    'mailing_sending_id' => $mailingSending->id,
                    'set_id' => $mail_setting['id']
                ]);
            }
        }
    }

    /**
     * Apply mailing for delivery NOW and create history_mailing records
     *
     * @param ApplyDelivery $request
     * @param Mailing $mailing
     * @return \Illuminate\Http\JsonResponse
     */
    public function applyDeliveryNow(ApplyDelivery $request, Mailing $mailing)
    {

        $this->applyFunc($request->set_id, $mailing,1);
        return response()->json([],204);
    }

    /**
     * Resend messages to all non-reacted recipients
     *
     * @param ResendMailing $request
     * @param Mailing $mailing
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendMailing(ResendMailing $request, Mailing $mailing)
    {
        $emailGroups = $mailing->emailGroups()->with('emailMailings.historyMailings.mailingStatus')->get();
        $mailingSending = MailingSending::create([
                'scheduled_at' => $request->get('scheduled_at'),
                'max_per_min' => $mailing->max_per_min
            ]
        );
        $mail_setting = $request->set_id;
        foreach ($emailGroups as $emailGroup) {
            foreach ($emailGroup->emailMailings()->whereDoesntHave('historyMailings.mailingStatus', function($query){
                $query->whereIn('name',['registered','failed','canceled']);
            })->get() as $emailMailing) {
                HistoryMailing::create([
                    'mailing_id' => $mailing->id,
                    'email_mailing_id' => $emailMailing->id,
                    'mailing_status_id' => MailingStatus::where('name','=','new')->first()->id,
                    'mailing_sending_id' => $mailingSending->id,
                    'set_id' => $mail_setting->id
                ]);
            }
        }

        return response()->json([],204);
    }

    /**
     * Send templatized mailing to given comma separated recipients
     *
     * @param TestMailingTemplate $request
     * @param Mailing $mailing
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function testMailingTemplate(TestMailingTemplate $request, Mailing $mailing)
    {
        $emails = explode(',',$request->get('emails'));
        $failed = [];
        foreach ($emails as $email) {
            try {
                $emailMailing = new EmailMailing();
                $emailMailing->email = $email;
                $emailMailing->name = 'Some Testing Name';
                $template = $mailing->mailTemplate;
                $content = $template->getTemplatized(['recipient' => $emailMailing]);
                $emailMailing->notify(new TemplatizedMail($template,$content));
            } catch (\Exception $e) {
                $failed[] = $email;
            }
        }
        if (empty($failed)) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        } else {
            return response(['message' => trans('notifications.email_test_failed_for',['emails' => implode(',',$failed)])]);
        }
    }

    /**
     * Stop running sendings of given mailing
     *
     * @param StopMailing $request
     * @param Mailing $mailing
     * @return \Illuminate\Http\JsonResponse
     */
    public function stopSending(StopMailing $request, Mailing $mailing)
    {
        MailingSending::whereHas('historyMailings', function ($query) use ($mailing){
            $query->whereHas('mailing', function ($sq) use ($mailing) {
                $sq->where('id','=',$mailing->id);
            });
        })->where('running','=',1)->update(['command' => 'stop']);
        return response()->json([],204);
    }

}
