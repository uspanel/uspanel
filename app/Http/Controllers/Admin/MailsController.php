<?php namespace App\Http\Controllers\Admin;

use App\HistoryMailing;
use App\Http\Controllers\Controller;
use App\MailingSending;
use App\Mailsetting;
use App\Notifications\TemplatizedMail;
use App\User;
use Carbon\Carbon;
use CodersStudio\ImapMailClient\Http\Requests\MailClientSendRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\Mail\IndexMail;
use App\Http\Requests\Admin\Mail\StoreMail;
use App\Http\Requests\Admin\Mail\UpdateMail;
use App\Http\Requests\Admin\Mail\DestroyMail;
use Brackets\AdminListing\Facades\AdminListing;
use Auth;
use CodersStudio\ImapMailClient\Mailmessage;
use Illuminate\Support\Facades\Hash;

class MailsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexMail $request
     * @return Response|array
     */


    public function test()
    {

        $start = Carbon::now();

        $count = 0;
        $toDelivery = HistoryMailing::whereHas('mailingStatus', function ($query) {
            $query->where('name', '=', 'new');
        })->whereHas('mailingSending', function ($query) {
            $query->where('scheduled_at','<=',Carbon::now());
        })->with(['emailMailing','mailing'])->get();

        $toDelivery = $toDelivery->groupBy('mailing_sending_id');



        foreach ($toDelivery as $sending) {
            MailingSending::where('id','=',$sending[0]->mailing_sending_id)->update(['running' => 1]);

            foreach ($sending as $item) {
                $ms = MailingSending::find($item->mailing_sending_id);

                if ($ms->command == 'stop') {
                    MailingSending::where('id','=',$item->mailing_sending_id)->update(['running' => 0, 'command' => '']);
                    return;
                }
                if ($ms->max_per_min > 0 && $count == $ms->max_per_min && $start->diffInSeconds(Carbon::now()) < 60) {
                    sleep(60 - $start->diffInSeconds(Carbon::now()));
                    $count = 0;
                    $start = Carbon::now();
                }

                $template = $item->mailing->mailTemplate;
                $recipient = $item->emailMailing;

                $user = User::where('email', $recipient->email)->first();
                if(!$user){
                    if ($template) {
                        $content = $template->getTemplatized(['recipient' => $recipient]);



                        $sendRequest = new MailClientSendRequest([
                            'recipients' => [
                                $recipient->email
                            ],
                            'set_id' => $item->set_id,
                            'user_id' => Mailsetting::find($item->set_id)->user_id,
                            'message' => $content,
                            'subject' => 'test',
                        ]);

                        app('App\Http\Controllers\Admin\MailClientControllerCustomMeiling')->send($sendRequest);
                    }
                }


            }
            MailingSending::where('id','=',$sending[0]->mailing_sending_id)->update(['running' => 0]);
        }

    }



    public function index(IndexMail $request)
    {



        $this->test();



        // create and AdminListing instance for a specific model and
        $mailsettings = Auth::user()->mailsettings;

        $defaultMailSettings = $mailsettings->where('default', true)->first();

        $addressees = [];
        $set = [];
        foreach ($mailsettings as $mailsetting) {
            $messages = $mailsetting->mailmessages;
            foreach ($messages as $message) {
                if ($message->sender && !in_array($message->sender,$addressees)) {
                    $addressees[] = $message->sender;
                    $set[] = ['addressee' => $message->sender];
                }
                if ($message->recipient && !in_array($message->recipient,$addressees)) {
                    $addressees[] = $message->recipient;
                    $set[] = ['addressee' => $message->recipient];
                }
            }
        }
        return view('admin.mail.index', [
            'mailsettings' => $mailsettings,
            'defaultMailSettings' => $defaultMailSettings,
            'addressees' => collect($set)
        ]);

    }

    public function show($mailsettingsId, $folder, $id) {
        $mailsettings = Auth::user()->mailsettings;
        $defaultMailSettings = $mailsettings->where('default', true)->first();
        $currentMailSettings = Auth::user()->mailsettings()->findOrFail($mailsettingsId);
        return view('admin.mail.show', [
            'id' => $id,
            'mailsettings' => $mailsettings,
            'defaultMailSettings' => $defaultMailSettings,
            'mailsettingsId' => $mailsettingsId,
            'folder' => $folder,
            'currentMailSettings' => $currentMailSettings
        ]);
    }

    public function create(Request $request) {
        $mailsettings = Auth::user()->mailsettings;
        $defaultMailSettings = $mailsettings->where('default', true)->first();
        $addressees = [];
        $set = [];
        foreach ($mailsettings as $mailsetting) {
            $messages = $mailsetting->mailmessages;
            foreach ($messages as $message) {
                if ($message->sender && !in_array($message->sender,$addressees)) {
                    $addressees[] = $message->sender;
                    $set[] = ['full' => $message->sender, 'mail' => $message->sender_email];
                }
            }
        }
        return view('admin.mail.create', [
            'mailsettings' => $mailsettings,
            'defaultMailSettings' => $defaultMailSettings,
            'usersList' => collect($set),
            'rcpt' => $request->get('rcpt')
        ]);
    }
}
