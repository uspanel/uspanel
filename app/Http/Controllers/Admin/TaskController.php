<?php namespace App\Http\Controllers\Admin;

use App\Carrier;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Package\ForceDelivered;
use App\Item;
use App\Mail\ContractReminder as MailContractReminder;
use App\Package;
use App\Project;
use App\User;
use App\Employee;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\Task\IndexTask;
use App\Http\Requests\Admin\Task\StoreTask;
use App\Http\Requests\Admin\Task\UpdateTask;
use App\Http\Requests\Admin\Task\DestroyTask;
use Brackets\AdminListing\Facades\AdminListing;
use App\Task;
use Illuminate\Support\Facades\Auth;
use App\EmployeeStatus;
use App\Warehouse;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;


class TaskController extends Controller
{

    /**
     * fields for searching
     *
     * @var array
     */
    protected $filters = [
        //TODO поправить фильтр для селектов и пикеров
        'warehouse_id',
        'delivered_at',
        'carrier_id',
        'track',
        'carrier_tracking_status',
        'employee_status_id',
        'comment',
        'creator_id',
        'manager_id',
        'employee_id',
        'id'
    ];

    protected $relationships = [
        'warehouse',
        'carrier',
        'creator',
        'manager',
        'lastActivity',
        'employeeStatus',
        'employee.user'
    ];

    public function testCreateImapGmail(){
                $mbox = imap_open("{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX", "taras.v.rudenko@gmail.com", "taras1803", OP_HALFOPEN)
        or die("не получилось подключиться: " . imap_last_error());

        $name1 = "phpnewbox";
        $name2 = imap_utf7_encode("phpnewböx"); // phpnewb&w7Y-x

        $newname = $name1;

        echo "Новым именем будет '$name1'<br />\n";

// теперь создадим новый ящик "phptestbox" в вашем входящем каталоге,
// проверим его статус и удалим, чтобы вернуть ваш каталог к первоначальному
// состоянию

        if (@imap_createmailbox($mbox, imap_utf7_encode("{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX.$newname"))) {
            $status = @imap_status($mbox, "{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX.$newname", SA_ALL);
            if ($status) {
                echo "ваш новый почтовый ящик называется '$name1' и имеет следующий статус:<br />\n";
                echo "Сообщений:           " . $status->messages    . "<br />\n";
                echo "Новых:                 " . $status->recent      . "<br />\n";
                echo "Непрочитанных:     " . $status->unseen      . "<br />\n";
                echo "Следующий UID:    " . $status->uidnext     . "<br />\n";
                echo "Корректность UID:" . $status->uidvalidity . "<br />\n";

                if (imap_renamemailbox($mbox, "{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX.$newname", "{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX.$name2")) {
                    echo "переименуем новый ящик из '$name1' в '$name2'<br />\n";
                    $newname = $name2;
                } else {
                    echo "вызов imap_renamemailbox для нового ящика завершился ошибкой: " . imap_last_error() . "<br />\n";
                }
            } else {
                echo "вызов imap_status для нового ящика завершился ошибкой: " . imap_last_error() . "<br />\n";
            }

            if (@imap_deletemailbox($mbox, "{imap.gmail.com:993/imap/ssl/novalidate-cert}INBOX.$newname")) {
                echo "новый почтовый ящик удален для восстановления первоначального состояния<br />\n";
            } else {
                echo "вызов imap_deletemailbox на новом почтовом ящике завершился ошибкой: " . implode("<br />\n", imap_errors()) . "<br />\n";
            }

        } else {
            echo "невозможно создать новый почтовый ящик: " . implode("<br />\n", imap_errors()) . "<br />\n";
        }

        imap_close($mbox);

        die;
    }


    /**
     * Display a listing of the resource.
     *
     * @param  IndexTask $request
     * @return Response|array
     */
    public function index(IndexTask $request)
    {

//        $this->testCreateImapGmail();

        $columns = Auth::user()
            ->getAvailableFields('task', 'read')
            ->prepend('id')
            ->filter(function ($item) {
                return !in_array($item, [
                    'label',
                    'barcode',
                    'receipt',
                    'carrier_tracking_status',
                    'invoice',
                    'content'
                ]);
            })
            ->toArray();
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Task::class)->processRequestAndGet(
        // pass the request with params
            $request,

            // set columns to query
            $columns,

            // set columns to searchIn
            [''],
            function ($query) use($request) {
                if (auth()->user()->hasAnyRole(['HR Manager Staff', 'HR Manager Invest', 'Support Staff', 'Support Invest'])) {
                    $query->where('manager_id', auth()->id())->orWhere('creator_id', auth()->id());
                }
                $query->with($this->relationships);
                $this->filter($query, $request);
                if (Auth::user()->employee) {
                    $query->whereHas('employee', function ($sq) {
                        $sq->where('id', Auth::user()->employee->id);
                    });
                }
                if (Auth::user()->hasRole(['Staffer'])) {
                    $query->whereHas('employee', function ($sq) {
                        $sq->whereHas('staffers', function ($ssq) {
                            $ssq->where('id',Auth::user()->id);
                        });
                    });
                }
            }
        );
        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.task.index', [
            'data' => $data,
            'viewName' => 'task',
            'users' => User::all()->pluck('name', 'id'),
            'managers' => User::Role('HR Manager Staff')->get()->pluck('name', 'id'),
            'carriers' => Carrier::pluck('title', 'id'),
            'employees' => Employee::whereHas('user')->with('user')->get()->pluck('user.name', 'id'),
            'warehouses' => Warehouse::pluck('name', 'id')
        ]);

    }

    public function filter($query, IndexTask $request) {
        foreach ($request->only($this->filters) as $key => $value) {
            if(!$value){
                continue;
            }
            if(preg_match('/_at$/', $key)){
                $query->whereDate($key, $value);
            }elseif(preg_match('/_id$/', $key)){
                if ($key == 'warehouse_id') {
                    $query->whereHas('warehouse', function ($query) use ($value) {
                        $query->where('id', $value);
                    });
                }
                $query->where($key, $value);
            }elseif($key == 'id'){
                $query->where($key, $value);
            }else{
                $query->where($key, 'like', '%'.$value.'%');
            }
        }
        return $query;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('crud.task.view');
        $request->validate([
            'empl' => 'sometimes|exists:employees,id'
        ]);
        $projects = auth()->user()->projects;
        if (auth()->user()->hasRole('Staffer')) {
            $employees = Employee::whereHas('staffers', function ($query) {
                $query->where('id',Auth::user()->id);
            })->with('user')->get()->pluck('user.name', 'user.id');
        } else {
            $employees = Employee::with('user')->get()->pluck('user.name', 'user.id');
        }

        if (auth()->user()->hasRole('Administrator')) {
            $projects = Project::all();
            $employees = Employee::with('user')->get()->pluck('user.name', 'user.id');
        }


        return view('admin.task.create', [
            'employee_id' => null,
            'package_id' => null,
            'users' => User::all()->pluck('name', 'id'),
            'managers' => User::Role('HR Manager Staff')->get()->pluck('name', 'id'),
            'carriers' => Carrier::pluck('title', 'id'),
            'employees' => $employees,
            'courierStatuses' => EmployeeStatus::get()->pluck('name', 'id'),
            'projects' => $projects,
            'warehouses' => Warehouse::pluck('name', 'id'),
            'employee' => $request->get('empl')
        ]);
    }

    public function createFromPackage(Request $request, Package $package)
    {
        $this->authorize('crud.task.view');
        $request->validate([
            'empl' => 'sometimes|exists:employees,id'
        ]);
        $projects = auth()->user()->projects;
        if (auth()->user()->hasRole('Staffer')) {
            $employees = Employee::whereHas('staffers', function ($query) {
                $query->where('id',Auth::user()->id);
            })->with('user')->get()->pluck('user.name', 'user.id');
        } else {
            $employees = Employee::with('user')->get()->pluck('user.name', 'user.id');
        }

        if (auth()->user()->hasRole('Administrator')) {
            $projects = Project::all();
            $employees = Employee::with('user')->get()->pluck('user.name', 'user.id');
        }


        return view('admin.task.create', [
            'employee_id' => !empty($package->employee_id) ? $package->employee_id : null,
            'package_id' => !empty($package->id) ? $package->id : null,
            'users' => User::all()->pluck('name', 'id'),
            'managers' => User::Role('HR Manager Staff')->get()->pluck('name', 'id'),
            'carriers' => Carrier::pluck('title', 'id'),
            'employees' => $employees,
            'courierStatuses' => EmployeeStatus::get()->pluck('name', 'id'),
            'projects' => $projects,
            'warehouses' => Warehouse::pluck('name', 'id'),
            'employee' => $request->get('empl')
        ]);
    }


    public function createPackageFRomTask($request){
        $warehouse = Warehouse::find( $request->warehouse_id);
        $employee = Employee::where('user_id', $warehouse->user_id)->first();

        $package = Package::create([
            'track' => $request->track,
            'carrier_id' => $request->carrier_id,
            'name' =>  $request->package_name,
            'employee_id' => $employee->id,
            'manager_id' => $request->manager_id,
            'status_id' => 1
            ]);
        $weight = 0;

        foreach ($request->item_ids as $item_id => $item_qty) {
            $item = Item::find($item_id);
            if($item->weight){
                $weight += $item->weight * $item_qty;
            }
            $package->items()->attach([$item->id => ['qty' => $item_qty, 'price' => 00.00]]);
        }
        $package->weight = $weight;
        $package->save();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreTask $request
     * @return Response|array
     */
    public function store(StoreTask $request)
    {

        $this->createPackageFRomTask($request);

        $columns = Auth::user()->getAvailableFields('task', 'write',['label','barcode','receipt','invoice','content'])->toArray();
        $columns[] = 'sku';

        $task = Task::create($request->only($columns) );
        $employee = Employee::findOrFail($request->get('employee_id'));
        foreach ($request->get('item_ids') as $key=>$val) {
            $employee->morphTasks()->attach([$task->id => [
                'qty' => -1*$val,
                'item_id' => $key,
                'created_at' => Carbon::now()
            ]]);
            $task->items()->attach([$key => ['qty' => $val]]);
        }
        $redirect = $request->get('redirect');
        if (!$redirect) {
            $redirect = 'admin/tasks';
        }
        if ($request->ajax()) {
            return ['redirect' => url($redirect), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect($redirect);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Task $task
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Task $task)
    {
        $this->authorize('crud.task.view');
        if ($task->employee) {
            $task->load(['employee.project']);
        }
        $task->append('item_ids');

        return view('admin.task.edit', [
            'task' => $task->load('items.user'),
            'managers' => User::Role('HR Manager Staff')->get()->pluck('name', 'id'),
            'users' => User::all()->pluck('name', 'id'),
            'carriers' => Carrier::pluck('title', 'id'),
            'project' => $task->employee && $task->employee->project_id ?: 0,
            'courierStatuses' => EmployeeStatus::get()->pluck('name', 'id'),
            'warehouses' => Warehouse::pluck('name', 'id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateTask $request
     * @param  Task $task
     * @return Response|array
     */
    public function update(UpdateTask $request, Task $task)
    {
        $mediaCollections = [
            'receipt',
        ];
        // TODO notifications 4!
        foreach ($mediaCollections as $value) {
            if (request()->has($value) && !empty(request()->get($value))) {
                $task->notifyUsers(trans('admin.task.employee_uploaded_file', [
                    'mediaCollection' => $value
                ]));
            }
        }
        $columns = Auth::user()->getAvailableFields('task', 'write',['label','barcode','receipt','invoice','content'])->toArray();
        $task->update($request->only($columns));
        if ($request->ajax()) {
            return ['redirect' => url('admin/tasks'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/tasks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyTask $request
     * @param  Task $task
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyTask $request, Task $task)
    {
        $task->delete();
        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    public function userTasks(User $user, IndexTask $request) {
        $tasks = $user->tasks()->with($this->relationships);
        $this->filter($tasks, $request);
        return $this->returnData($tasks, $request);
    }

    public function authUserTasks(IndexTask $request) {
        $tasks = auth()->user()->tasks()->with($this->relationships);
        $this->filter($tasks, $request);
        return $this->returnData($tasks, $request);
    }

    public function returnData($tasks, IndexTask $request) {
        if ($request->ajax()) {
            return ['data' => $tasks->paginate()];
        }

        return view('admin.task.index', [
            'data' => $tasks->paginate(),
            'viewName' => 'task',
            'users' => User::all()->pluck('name', 'id'),
            'managers' => User::Role('HR Manager Staff')->get()->pluck('name', 'id'),
            'carriers' => Carrier::pluck('title', 'id'),
            'warehouses' => Warehouse::pluck('name', 'id'),
            'employees' => collect([]),
        ]);
    }

    public function like(Task $task) {
        $task->setEmployeeStatus('sent');
        auth()->user()->notifications()->update([
            'data' => json_encode([
                'params' => Task::findOrFail($task->id),
                'text' => request()->get('text')
            ])
        ]);
        return response([]);
    }

    public function show(Task $task) {
        return view('admin.task.show', [
            'task' => $task
        ]);
    }

    /**
     * Force package delivered without tracking
     *
     * @param ForceDelivered $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forceDelivered(ForceDelivered $request, $id)
    {
        $task = Task::findOrFail($id);
        if ($task) {
            $task->update([
                'delivered_at' => Carbon::now()
            ]);
        }
        return response()->json([],204);
    }

    public function createCsv(Request $request)
    {

        $tasks = Task::whereIn('id', $request->tasks_ids)->with('items')->get()->toArray();
            $csv_array = [];
        foreach ($tasks as $task){
            $csv_array[] = $this->getTaskRowForCsv($task);
        }
        $date = date('Y_d_m');
        $filename = 'tasks_'.$date.'.csv';
        $columns = ['track', 'items', 'sku'];
        $csv = implode(',', $columns) . "\n";//Column headers
        foreach ($csv_array as $record){
            $csv .= implode(',', $record) . "\n";//Append data to csv
        }

        if(!Storage::disk('public_uploads')->put($filename, $csv)) {
            return false;
        }

        return response()->json(['filename' => env('APP_URL'). '/csv/' . $filename],200);
    }

    private function getTaskRowForCsv($task){
//        $task_csv['label'] = '';
//        $task_csv['barcode'] = '';
//        $task_csv['reciept'] = '';
//        $task_csv['invoice'] = '';
//        $task_csv['carrier'] = $task['carrier']['name'];

        $task_csv['track'] = $task['track'];
        $task_csv['items'] = '';
        $task_csv['sku'] = $task['sku'];
//        foreach ($task['media'] as $media) {
//            $path = env('APP_URL') . '/' . $media['disk'] . '/' . $media['id'] . '/' . $media['file_name'];
//            $task_csv[$media['collection_name']] = $path;
//        }

        foreach ($task['items'] as $key => $item) {
            if($key == (count($task['items']) - 1)){
                $task_csv['items'] .= $item['name'];
            }else{
                $task_csv['items'] .= $item['name'] . '/';
            }
        }

        return $task_csv;
    }

}
