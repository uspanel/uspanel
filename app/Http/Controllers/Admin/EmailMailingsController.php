<?php namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\EmailMailing\{IndexEmailMailing, StoreEmailMailing, UpdateEmailMailing, DestroyEmailMailing};
use Brackets\AdminListing\Facades\AdminListing;
use App\{EmailMailing, EmailGroup};

class EmailMailingsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexEmailMailing $request
     * @return Response|array
     */
    public function index(IndexEmailMailing $request)
    {
        // create and AdminListing instance for a specific model and

        $eq = ['email_group_id'];
        $data = AdminListing::create(EmailMailing::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'name', 'email', 'email_group_id'],

            // set columns to searchIn
            ['id', 'name', 'email'],
            function ($query) use ($request, $eq) {
                $query->with('emailGroup');
                foreach ($request->all() as $parameter => $value) {
                    if (in_array($parameter, $eq)) {
                        $query->where($parameter,'=',$value);
                    }
                }
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.email-mailing.index', [
            'data' => $data,
            'viewName' => 'email-mailing',
            'email_group_id' => $request->get('email_group_id')
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('crud.email-mailing.create');
        return view('admin.email-mailing.create', [
            'viewName' => 'email-mailing',
            'emailGroups' => EmailGroup::pluck('name', 'id'),
            'chosenGroup' => $request->get('email_group_id')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreEmailMailing $request
     * @return Response|array
     */
    public function store(StoreEmailMailing $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the EmailMailing
        $emailMailing = EmailMailing::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/email-mailings'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/email-mailings');
    }

    /**
     * Display the specified resource.
     *
     * @param  EmailMailing $emailMailing
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(EmailMailing $emailMailing)
    {
        $this->authorize('crud.email-mailing.show', $emailMailing);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  EmailMailing $emailMailing
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(EmailMailing $emailMailing)
    {
        $this->authorize('crud.email-mailing.edit', $emailMailing);

        return view('admin.email-mailing.edit', [
            'emailMailing' => $emailMailing,
            'viewName' => 'email-mailing',
            'emailGroups' => EmailGroup::pluck('name', 'id')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateEmailMailing $request
     * @param  EmailMailing $emailMailing
     * @return Response|array
     */
    public function update(UpdateEmailMailing $request, EmailMailing $emailMailing)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values EmailMailing
        $emailMailing->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/email-mailings'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/email-mailings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyEmailMailing $request
     * @param  EmailMailing $emailMailing
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyEmailMailing $request, EmailMailing $emailMailing)
    {
        $emailMailing->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    }
