<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\MeilsettingAdditionalDomain;
use App\MeilsettingAdditionalName;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\Mailsetting\IndexMailsetting;
use App\Http\Requests\Admin\Mailsetting\StoreMailsetting;
use App\Http\Requests\Admin\Mailsetting\UpdateMailsetting;
use App\Http\Requests\Admin\Mailsetting\DestroyMailsetting;
use Brackets\AdminListing\Facades\AdminListing;
use App\Mailsetting;
use Auth;
use App\User;
use Activity;
use Illuminate\Support\Facades\Storage;


class MailsettingsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexMailsetting $request
     * @return Response|array
     */
    public function index(IndexMailsetting $request)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Mailsetting::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'user_id', 'smtp_host', 'smtp_port', 'smtp_encryption', 'imap_host', 'imap_port', 'imap_encryption', 'imap_sent_folder', 'imap_inbox_folder', 'login'],

            // set columns to searchIn
            ['id', 'smtp_host', 'smtp_encryption', 'imap_host', 'imap_encryption', 'imap_sent_folder', 'imap_inbox_folder', 'login'],
            function($query) {
                $query->where('user_id', auth()->id());
            }
        );
        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.mailsetting.index', ['data' => $data]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {


        $this->authorize('crud.mailsetting.create');

        return view('admin.mailsetting.create');
    }

    public function names(Request $request)
    {
        $contents = Storage::disk('uploads')->get($request->path);
        $contents = explode("\n", $contents);
        $csv_array = [];
        $new_array = array_diff($contents, array(''));
       foreach ($new_array as $key => $line){
           $temp = explode(";", $line);
           $csv_array[$key]['name'] = $temp[0];
           $csv_array[$key]['name_domain'] = $temp[1];
       }

        return response(['names' => $csv_array]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreMailsetting $request
     * @return Response|array
     */
    public function store(StoreMailsetting $request)
    {
        // Sanitize input
        $sanitized_all = $request->validated();
        $sanitized = $request->getModifiedData();

        // Store the Mailsetting
        // Delete
        $sanitized['default'] = $this->checkDefault($sanitized['default']);
        if($sanitized_all['meiling']){
            $sanitized['for_meiling'] = 1;
        }

        $mailsetting = Auth::user()->localmailsettings()->create($sanitized);

        if($sanitized_all['meiling']){
            $sanitizedAdditionalMeilsettingNames = $request->getModifiedDataForAdditionalMeilsettingNames($mailsetting->id);
            foreach ($sanitizedAdditionalMeilsettingNames as $sanitizedAdditionalMeilsettingName){
                MeilsettingAdditionalName::create($sanitizedAdditionalMeilsettingName);
            }
            $sanitizedAdditionalMeilsettingDomains = $request->getModifiedDataForAdditionalMeilsettingDomains($mailsetting->id);
            foreach ($sanitizedAdditionalMeilsettingDomains as $sanitizedAdditionalMeilsettingDomain){
                MeilsettingAdditionalDomain::create($sanitizedAdditionalMeilsettingDomain);
            }
        }



//        $test = Mailsetting::with('meilsettingNames')->find(11);
        // Auth::user()->mailsettings()->create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('admin/mailsettings'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/mailsettings');
    }

    /**
     * Display the specified resource.
     *
     * @param  Mailsetting $mailsetting
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Mailsetting $mailsetting)
    {
        $this->authorize('crud.mailsetting.show', $mailsetting);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Mailsetting $mailsetting
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Mailsetting $mailsetting)
    {
        $this->authorize('crud.mailsetting.edit', $mailsetting);
       if($mailsetting->for_meiling){
           $mailsetting->names = $mailsetting->meilsettingNames;
           $mailsetting->domains = $mailsetting->meilsettingDomains()->get()->pluck('domain')->implode(', ');
           return view('admin.mailsetting.edit-meiling', [
               'mailsetting' => $mailsetting->load('meilsettingNames', 'meilsettingDomains'),
           ]);
       }
        return view('admin.mailsetting.edit', [
            'mailsetting' => $mailsetting->load(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateMailsetting $request
     * @param  Mailsetting $mailsetting
     * @return Response|array
     */
    public function update(UpdateMailsetting $request, Mailsetting $mailsetting)
    {
        // Sanitize input
        $sanitized_all = $request->validated();

        $sanitized = $request->getModifiedData();

        // Update changed values Mailsetting
        $sanitized['default'] = $this->checkDefault($sanitized['default']);
        $mailsetting->update($sanitized);

        if($sanitized_all['meiling']){
            $sanitizedAdditionalMeilsettingNames = $request->getModifiedDataForAdditionalMeilsettingNames($mailsetting->id);

            MeilsettingAdditionalName::where('set_id', $mailsetting->id)->delete();

            foreach ($sanitizedAdditionalMeilsettingNames as $sanitizedAdditionalMeilsettingName){
                MeilsettingAdditionalName::create($sanitizedAdditionalMeilsettingName);
            }
            MeilsettingAdditionalDomain::where('set_id', $mailsetting->id)->delete();
            $sanitizedAdditionalMeilsettingDomains = $request->getModifiedDataForAdditionalMeilsettingDomains($mailsetting->id);
            foreach ($sanitizedAdditionalMeilsettingDomains as $sanitizedAdditionalMeilsettingDomain){
                MeilsettingAdditionalDomain::create($sanitizedAdditionalMeilsettingDomain);
            }
        }

        if ($request->ajax()) {
            return ['redirect' => url('admin/mailsettings'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/mailsettings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyMailsetting $request
     * @param  Mailsetting $mailsetting
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyMailsetting $request, Mailsetting $mailsetting)
    {

        $mailsetting = Mailsetting::find($mailsetting->id);
        MeilsettingAdditionalDomain::where('set_id', $mailsetting->id)->delete();
        MeilsettingAdditionalName::where('set_id', $mailsetting->id)->delete();
        $mailsetting->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    protected function checkDefault($default) {
        if ($default) {
            Auth::user()->mailsettings()->update([
                'default' => false
            ]);
        }
        if (!Auth::user()->mailsettings()->where('default', true)->count()) {
            $default = true;
        }
        return $default;
    }

}
