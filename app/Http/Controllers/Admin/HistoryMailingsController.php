<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\HistoryMailing\IndexHistoryMailing;
use App\Http\Requests\Admin\HistoryMailing\StoreHistoryMailing;
use App\Http\Requests\Admin\HistoryMailing\UpdateHistoryMailing;
use App\Http\Requests\Admin\HistoryMailing\DestroyHistoryMailing;
use Brackets\AdminListing\Facades\AdminListing;
use App\HistoryMailing;
use App\MailingStatus;
use App\Mailing;
use App\MailingSending;

class HistoryMailingsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexHistoryMailing $request
     * @return Response|array
     */
    public function index(Mailing $mailing, IndexHistoryMailing $request)
    {
        // create and AdminListing instance for a specific model and
        $searchable = [
            'mailing_status_id',
            'created_at',
        ];
        $data = AdminListing::create(HistoryMailing::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            [
                'history_mailings.id',
                'history_mailings.mailing_id',
                'history_mailings.email_mailing_id',
                'history_mailings.mailing_status_id',
                'history_mailings.mailing_sending_id',
                'history_mailings.created_at',
            ],

            // set columns to searchIn
            [],
            function ($query) use ($searchable, $mailing) {
                $query
                ->where('mailing_id', $mailing->id)
                ->with('mailing', 'mailingStatus', 'emailMailing.user', 'mailingSending');
                foreach ($searchable as $value) {
                    if (request()->get($value)) {
                        $query->where($value, request()->get($value));
                    }
                }
                if (request()->get('email_mailing_email')) {
                    $query->whereHas('emailMailing', function ($subQuery) {
                        $subQuery->where('email', 'like', '%' . request()->get('email_mailing_email') . '%');
                    });
                }
                if (request()->get('mailing_sending_scheduled_at')) {
                    $query->whereHas('mailingSending', function ($subQuery) {
                        $subQuery->where('scheduled_at', request()->get('mailing_sending_scheduled_at'));
                    });
                }
            }
        );
        $mailingStatuses = MailingStatus::all();
        $mailingSendings = MailingSending::orderBy('scheduled_at', 'desc')->distinct('scheduled_at')->get();
        // $mailings = Mailing::all();
        if ($request->ajax()) {
            return [
                'data' => $data
            ];
        }

        return view('admin.history-mailing.index', [
            'data' => $data,
            'mailingStatuses' => $mailingStatuses,
            // 'mailings' => $mailings,
            'mailing' => $mailing,
            'mailingSendings' => $mailingSendings,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('admin.history-mailing.create');

        return view('admin.history-mailing.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreHistoryMailing $request
     * @return Response|array
     */
    public function store(StoreHistoryMailing $request)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Store the HistoryMailing
        $historyMailing = HistoryMailing::create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/history-mailings'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/history-mailings');
    }

    /**
     * Display the specified resource.
     *
     * @param  HistoryMailing $historyMailing
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(HistoryMailing $historyMailing)
    {
        $this->authorize('admin.history-mailing.show', $historyMailing);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  HistoryMailing $historyMailing
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(HistoryMailing $historyMailing)
    {
        $this->authorize('admin.history-mailing.edit', $historyMailing);

        return view('admin.history-mailing.edit', [
            'historyMailing' => $historyMailing,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateHistoryMailing $request
     * @param  HistoryMailing $historyMailing
     * @return Response|array
     */
    public function update(UpdateHistoryMailing $request, HistoryMailing $historyMailing)
    {
        // Sanitize input
        $sanitized = $request->validated();

        // Update changed values HistoryMailing
        $historyMailing->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/admin/history-mailings'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/admin/history-mailings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyHistoryMailing $request
     * @param  HistoryMailing $historyMailing
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyHistoryMailing $request, HistoryMailing $historyMailing)
    {
        $historyMailing->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    }
