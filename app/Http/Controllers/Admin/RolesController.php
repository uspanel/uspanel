<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\Role\IndexRole;
use App\Http\Requests\Admin\Role\StoreRole;
use App\Http\Requests\Admin\Role\UpdateRole;
use App\Http\Requests\Admin\Role\DestroyRole;
use Brackets\AdminListing\Facades\AdminListing;
use App\Role;
use App\Permission;
use Illuminate\Support\Facades\Auth;

class RolesController extends Controller
{

    /**
     * fields for searching
     *
     * @var array
     */
    protected $filters = [
        'name'
    ];

    /**
     * Display a listing of the resource.
     *
     * @param  IndexRole $request
     * @return Response|array
     */
    public function index(IndexRole $request)
    {
        $columns = Auth::user()
            ->getAvailableFields('role', 'read')
            ->prepend('id')
            ->push('created_at')
            ->toArray();
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Role::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            $columns,

            // set columns to searchIn
            [],
            function ($query) use($request) {
                foreach ($request->only($this->filters) as $key => $value) {
                    $query->where($key, 'like', '%'.$value.'%');
                }
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.role.index', ['data' => $data]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('crud.role.view');
        return view('admin.role.create', [
            'permissions' => Permission::convertPermissionsToArray(Permission::all())['crud'] ?? []
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRole $request
     * @return Response|array
     */
    public function store(StoreRole $request)
    {
        // Store the Role
        $columns = Auth::user()->getAvailableFields('role', 'write')->toArray();
        $role = Role::create($request->only($columns));
        $role->syncPermissions($request->get('permission_ids', []));
        $role->givePermissionTo('admin');
        app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();
        if ($request->ajax()) {
            return ['redirect' => url('admin/roles'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/roles');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Role $role
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Role $role)
    {
        $this->authorize('crud.role.view');
        $role->append('permission_ids');
        return view('admin.role.edit', [
            'role' => $role,
            'permissions' => Permission::convertPermissionsToArray(Permission::all())['crud'] ?? []
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRole $request
     * @param  Role $role
     * @return Response|array
     */
    public function update(UpdateRole $request, Role $role)
    {
        // Update changed values Role
        $columns = Auth::user()->getAvailableFields('role', 'write')->toArray();
        $role->update($request->only($columns));
        $role->syncPermissions($request->get('permission_ids', []));
        app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();
        if ($request->ajax()) {
            return ['redirect' => url('admin/roles'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('admin/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyRole $request
     * @param  Role $role
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyRole $request, Role $role)
    {
        $role->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

}
