<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\Admin\Field\IndexField;
use App\Http\Requests\Admin\Field\StoreField;
use App\Http\Requests\Admin\Field\UpdateField;
use App\Http\Requests\Admin\Field\DestroyField;
use Brackets\AdminListing\Facades\AdminListing;
use CodersStudio\FormCreator\Field;
use App\Form;
use CodersStudio\FormCreator\FieldType;
use App\Project;

class FieldsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param  IndexField $request
     * @return Response|array
     */
    public function index(IndexField $request, Form $form)
    {
        // create and AdminListing instance for a specific model and
        $data = AdminListing::create(Field::class)->processRequestAndGet(
            // pass the request with params
            $request,

            // set columns to query
            ['id', 'form_id', 'field_type_id', 'name', 'title', 'description', 'options', 'enabled', 'ordering', 'sortable', 'searchable', 'visible', 'exportable'],

            // set columns to searchIn
            ['name', 'title', 'description'],
            function($query) use($form){
                $query->where('form_id', $form->id);
                $query->with('fieldType');
                $query->orderBy('ordering');
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view('admin.field.index', [
            'data' => $data,
            'form' => $form
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(Form $form)
    {
        $this->authorize('crud.field.create');
        $projects = Project::all();
        return view('admin.field.create', [
            'fieldTypes' => FieldType::all(),
            'form' => $form,
            'projects' => $projects
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreField $request
     * @return Response|array
     */
    public function store(StoreField $request, Form $form)
    {
        // Sanitize input
        $sanitized = $this->prepareData($request->validated());

        // Store the Field
        $field = $form->fields()->create($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/forms/'. $form->id .'/fields'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/forms/'. $form->id .'/fields');
    }

    /**
     * Display the specified resource.
     *
     * @param  Field $field
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Field $field)
    {
        $this->authorize('crud.field.show', $field);

        // TODO your code goes here
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Field $field
     * @return Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Form $form, Field $field)
    {
        $this->authorize('crud.field.edit', $field);
        $projects = Project::all();
        return view('admin.field.edit', [
            'field' => $field,
            'fieldTypes' => FieldType::all(),
            'form' => $form,
            'projects' => $projects
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateField $request
     * @param  Field $field
     * @return Response|array
     */
    public function update(UpdateField $request, Form $form, Field $field)
    {
        // Sanitize input
        $sanitized = $this->prepareData($request->validated());
        // Update changed values Field
        $field->update($sanitized);

        if ($request->ajax()) {
            return ['redirect' => url('/forms/'. $form->id .'/fields'), 'message' => trans('brackets/admin-ui::admin.operation.succeeded')];
        }

        return redirect('/forms/'. $form->id .'/fields');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  DestroyField $request
     * @param  Form $form
     * @param  Field $field
     * @return Response|bool
     * @throws \Exception
     */
    public function destroy(DestroyField $request, Form $form, Field $field)
    {
        $field->delete();

        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }

        return redirect()->back();
    }

    private function prepareData(array $sanitized)
    {
        // $sanitized['rules'] = implode('|', $sanitized['rules'] ?? []);
        $options = [];
        if(isset($sanitized['options'])) {
            foreach ($sanitized['options'] as $option) {
                $options[$option['value']] = $option['text'];
            }
        }
        $sanitized['options'] = json_encode($options);
        return $sanitized;
    }

}
