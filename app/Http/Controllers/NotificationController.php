<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use CodersStudio\Notifications\Http\Resources\NotificationsResource;
use Auth;
use App\User;

class NotificationController extends Controller
{
    public function index(Request $request, $id)
    {
    	$request->validate([
    		'date_from' => 'nullable|date',
    		'date_to' => 'nullable|date',
    		'event' => 'nullable|string',
    	]);
        if($id!=Auth::id()) abort(403, 'Access denied');
        $messages = User::findOrFail($id)
            ->unreadNotifications()
            ->where('type', config('notifications.system_notification'));
        if ($request->get('date_from')) {
        	$messages->whereDate('created_at', '>=', $request->get('date_from'));
        }
        if ($request->get('date_to')) {
        	$messages->whereDate('created_at', '<=', $request->get('date_to'));
        }
        if ($request->get('event')) {
            $messages
            ->whereRaw('JSON_EXTRACT(data, "$.text") like "%' . $request->get('event') . '%"');
        }
        return new NotificationsResource($messages->get()); 
    }
}
