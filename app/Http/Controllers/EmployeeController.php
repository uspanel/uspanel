<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests\Employee\SyncRequest;
use Illuminate\Http\Request;
use App\Project;

class EmployeeController extends Controller
{
    public function index(Request $request)
    {
    	$projects = auth()->user()->projects()->with('employees');
    	if (auth()->user()->hasRole('Administrator')) {
    		$projects = Project::with('employees');
    	}
    	if (auth()->user()->hasRole('Staffer')) {
            $projects->whereHas('employees', function ($query) {
                $query->whereHas('staffers', function ($sq) {
                    $sq->where('id', auth()->id());
                });
            });
    	}
        $projects = $projects->get();
        $projects->each(function ($project) {
            $project->employees->each(function ($employee) {
                return $employee->append('user_name');
            });
        });
    	return response([
    		'data' => $projects
    	]);
    }

    /**
     * @param SyncRequest $request
     * @param Employee $employee
     */
    public function syncRelations(SyncRequest $request, Employee $employee)
    {
        if ($employee) {
            if ($request->has('investor_lists')) {
                $employee->investorLists()->sync($request->get('investor_lists', []));
            }
            if ($request->has('investors')) {
                $employee->investors()->sync($request->get('investors', []));
            }
            if ($request->has('contragent_lists')) {
                $employee->contragentLists()->sync($request->get('contragent_lists', []));
            }
            if ($request->has('contragents')) {
                $employee->contragents()->sync($request->get('contragents', []));
            }
        }
    }
}
