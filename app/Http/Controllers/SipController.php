<?php

namespace App\Http\Controllers;

use App\User;
use CodersStudio\Notifications\Notifications\System;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\URL;
use Twilio\Jwt\ClientToken;
use Twilio\Rest\Client;
use Twilio\Twiml;

class SipController extends Controller
{
    /**
     * Get token to use with twilio
     *
     * @param Request $request
     * @param ClientToken $clientToken
     * @return \Illuminate\Http\JsonResponse
     */
    public function newToken(Request $request, ClientToken $clientToken)
    {
        $applicationSid = env('TWILIO_APPLICATION_SID', '');
        $clientToken->allowClientOutgoing($applicationSid);
        $clientToken->allowClientIncoming('user' . Auth::user()->id);
        $token = $clientToken->generateToken();
        return response()->json(['token' => $token]);
    }

    /**
     * New call callback for twilio service
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Twilio\Exceptions\TwimlException
     */
    public function newCall(Request $request)
    {
        $response = new Twiml();
        $callerIdNumber = config('services.twilio')['number'];

        $dial = $response->dial([
            'callerId' => $callerIdNumber,
            'record' => 'record-from-answer',
            'recordingStatusCallback' => url('/api/twilio/cr'),
            'recordingStatusCallbackMethod' => 'POST',
            'timeout' => 600
        ]);

        $phoneNumberToDial = $request->input('phoneNumber');
        $called = $request->input('Called');
        if (isset($phoneNumberToDial)) {
            $dial->number($phoneNumberToDial);
        } else if (isset($called) && strpos($called, 'user') !== false) {
            $client = $dial->client();
            $client->identity($called);
            $client->parameter(['name' => 'Caller','value' => $request->get('From')]);
        } else {
            $callSupport = User::where('receive_external_calls','=',1)->get();
            if ($callSupport->count() > 10) {
                $callSupport = $callSupport->random(10);
            }
            foreach ($callSupport as $cs) {
                $client = $dial->client();
                $client->identity('user'.$cs->id);
                $client->parameter(['name' => 'Caller','value' => $request->get('From')]);
            }
        }
        return response($response)->header('Content-Type','text/xml');
    }

    /**
     * Try to get user info based on twilio provided call participants id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function callerInfo(Request $request)
    {
        $caller = $this->getUser($request->get('caller'));
        if (!$caller) {
            $caller = new User();
            $caller->name = $request->get('caller');
        }
        return response()->json($caller);
    }

    /**
     * Try to identify user in system
     *
     * @param $twilioId
     * @return |null
     */
    protected function getUser($twilioId)
    {
        $caller = null;
        $matches = [];
        if (preg_match('/(client:user)([0-9]+)/', $twilioId, $matches)) {
            $caller = User::where('id', '=', $matches[2])->first();
        } else {
            $caller = User::where('phone_number', '=', $twilioId)->first();
        }
        return $caller;
    }

    /**
     * Callback for getting info about recorded call
     *
     * @param Request $request
     * @return Twiml
     * @throws \Twilio\Exceptions\ConfigurationException
     * @throws \Twilio\Exceptions\TwilioException
     * @throws \Twilio\Exceptions\TwimlException
     */
    public function callRecorded(Request $request)
    {
        $client = new Client(env('TWILIO_ACCOUNT_SID', ''), env('TWILIO_AUTH_TOKEN', ''));

        $fromCall = $client->calls($request->get('CallSid'))->fetch();
        $toCall = $client->calls->read(array("parentCallSid" => $request->get('CallSid')), 1);
        $fromUser = $this->getUser($fromCall->from);
        if ($fromUser) {
            activity()
                ->by($fromUser)
                ->log(trans('history.actions.call_recorded', ['link' => $request->get('RecordingUrl')]));
        }
        $toUser = $this->getUser($toCall[0]->to);
        if ($toUser) {
            activity()
                ->by($toUser)
                ->log(trans('history.actions.call_recorded', ['link' => $request->get('RecordingUrl')]));
        }
        $response = new Twiml();
        $response->hangup();
        return $response;
    }

    /**
     * Send missed call notification to center
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function notifyCenter(Request $request)
    {
        Notification::send(Auth::user(), new System(trans('notifications.missed_call',['caller' => $request->get('caller')]), "#"));
        return response()->json([],204);
    }
}
