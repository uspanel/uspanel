<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{BusinessProcessType, Event, Action, User, BusinessProcess, MailTemplate, BusinessProcessEvent, Role};
use App\Http\Requests\Admin\BusinessProcess\{
    IndexBusinessProcessRequest,
    DestroyBusinessProcessRequest,
    StoreBusinessProcessRequest,
    UpdateBusinessProcessRequest
};
use Brackets\AdminListing\Facades\AdminListing;

class BusinessProcessController extends Controller
{
    public function index(IndexBusinessProcessRequest $request)
    {
        $data = AdminListing::create(BusinessProcess::class)->processRequestAndGet(
            $request,
            ['id', 'title', 'created_at', 'active'],
            ['title'],
            function ($query) use ($request) {
            }
        );

        if ($request->ajax()) {
            return ['data' => $data];
        }

        return view(
            'admin.businessprocess.index',
            [
                'data' => $data,
            ]
        );
    }

    /**
     * Get events
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getEvents(Request $request)
    {
        $request->validate(
            [
                'business_process_type_id' => 'sometimes|int'
            ]
        );
        $typeId = $request->get('business_process_type_id', 0);
        $bpt = BusinessProcessType::find($typeId);

        return response(
            [
                'events' => !$bpt ? Event::all() : $bpt->events()->get()
            ]
        );
    }

    /**
     * Get actions
     * @return Illuminate\Http\Response
     */
    public function getActions()
    {
        return response(
            [
                'actions' => Action::all()
            ]
        );
    }

    /**
     * Get Mail Templates
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function getMailTemplates(Request $request)
    {
        $templates = MailTemplate::select('id', 'title');
        if ($request->has('searchFor')) {
            $templates->where('title', 'like', '%' . $request->searchFor . '%');
        }
        return response(
            [
                'templates' => $templates->paginate(10)
            ]
        );
    }

    /**
     * Get users
     * @param Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function getUsers(Request $request)
    {
        $users = User::with('roles');
        if ($request->has('searchFor')) {
            $users->where('first_name', 'like', '%' . $request->searchFor . '%')
                ->orWhere('last_name', 'like', '%' . $request->searchFor . '%');
        }
        return response(
            [
                'users' => new \App\Http\Resources\Users($users->paginate(10))
            ]
        );
    }

    /**
     * Store new Business Process
     * @param App\Http\Requests\Admin\BusinessProcess\StoreBusinessProcessRequest $request data of new Business Process
     * @return Illuminate\Http\Response
     */
    public function store(StoreBusinessProcessRequest $request)
    {
        $businessProcess = new BusinessProcess();
        $businessProcess->title = $request->title;
        $businessProcess->active = $request->active;
        $businessProcess->business_process_type_id = $request->business_process_type_id;
        auth()->user()->businessProcesses()->save($businessProcess);
        $this->saveProcess($request, $businessProcess);
        return response([]);
    }

    /**
     * Store Business Process
     * @param App\Http\Requests\Admin\BusinessProcess\UpdateBusinessProcessRequest $request data of updating Business Process
     * @param App\BusinessProcess $businessProcess updating Business Process
     * @return Illuminate\Http\Response
     */
    public function update(UpdateBusinessProcessRequest $request, BusinessProcess $businessProcess)
    {
        $businessProcess->title = $request->title;
        $businessProcess->active = $request->active;
        $businessProcess->business_process_type_id = $request->business_process_type_id;
        $businessProcess->save();
        $businessProcess->businessProcessEvents()->delete();
        $this->saveProcess($request, $businessProcess);
        return response([]);
    }

    /**
     * Prepare request data and save or update Business Process
     * @param Illuminate\Http\Request $request data of Business Process
     * @param App\BusinessProcess $businessProcess updating Business Process
     * @return void
     */
    public function saveProcess($request, $businessProcess): void
    {
        foreach ($request->business_process_events as $business_process_event) {
            $businessProcessEvent = Event::find($business_process_event['event']['id'])->businessProcessEvents(
            )->create(
                [
                    'business_process_id' => $businessProcess->id
                ]
            );
            foreach ($business_process_event['action_business_process_events'] as $key => $action_business_process_event) {
                $actionBusinessProcessEvent = $businessProcessEvent->actionBusinessProcessEvents()->create(
                    [
                        'action_id' => $action_business_process_event['action']['id'],
                        'position' => $key
                    ]
                );
                if ($action_business_process_event['mail_template']) {
                    $actionBusinessProcessEvent->mailTemplates()->attach(
                        $action_business_process_event['mail_template']['id']
                    );
                }
                foreach ($action_business_process_event['users'] as $user) {
                    $actionBusinessProcessEvent->users()->attach($user['id']);
                }
                if (!empty($action_business_process_event['roles'])) {
                    foreach ($action_business_process_event['roles'] as $role) {
                        $actionBusinessProcessEvent->users()->syncWithoutDetaching(
                            User::role($role['name'])->get()->pluck('id')
                        );
                        $actionBusinessProcessEvent->roles()->attach($role['id']);
                    }
                }

            }
        }
    }

    public function create()
    {
        $bpts = BusinessProcessType::all();
        return view('admin.businessprocess.create', [
            'business_process_types' => $bpts
        ]);
    }

    public function edit(BusinessProcess $businessProcess)
    {
        $bpts = BusinessProcessType::all();
        return view(
            'admin.businessprocess.edit',
            [
                'business_process_types' => $bpts,
                'businessProcess' => $businessProcess,
                'businessProcessResource' => (new \App\Http\Resources\BusinessProcess($businessProcess))->jsonSerialize(
                )
            ]
        );
    }

    public function destroy(DestroyBusinessProcessRequest $request, BusinessProcess $businessProcess)
    {
        $businessProcess->delete();
        if ($request->ajax()) {
            return response(['message' => trans('brackets/admin-ui::admin.operation.succeeded')]);
        }
        return redirect()->back();
    }

    /**
     * Set active status of Business Process
     * @param Illuminate\Http\Request $request
     * @param BusinessProcess $businessProcess updating Business Process
     * @return Illuminate\Http\Response
     */
    public function active(Request $request, BusinessProcess $businessProcess)
    {
        $request->validate(
            [
                'active' => 'required|boolean',
            ]
        );
        $businessProcess->update(
            [
                'active' => $request->get('active', $businessProcess->active)
            ]
        );
        return response([]);
    }

    public function getRoles()
    {
        return response(
            [
                'roles' => Role::select(['name', 'id'])->get()->makeHidden('resource_url')
            ]
        );
    }

}
