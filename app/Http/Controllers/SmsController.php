<?php

namespace App\Http\Controllers;

use App\Notifications\CustomSms;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\CustomSms as CSRequest;

class SmsController extends Controller
{
    public function sendCustom(CSRequest $request)
    {
        $recipient = User::where('id','=',$request->get('recipient'))->first();
        $recipient->notify(new CustomSms($request->get('message')));
        activity()
            ->log(trans('history.actions.sms_sent', ['phone' => $recipient->phone, 'message' => $request->get('message')]));

        return response()->json(['message' => trans('notifications.sms_sent')]);
    }
}
