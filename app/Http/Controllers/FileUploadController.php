<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Brackets\Media\Http\Controllers\FileUploadController as BracketsFileUploadController;

class FileUploadController extends BracketsFileUploadController
{
    public function upload( Request $request ) {
		if ( $request->hasFile( 'file' ) ) {
			$path = $request->file( 'file' )->store( '', [ 'disk' => 'uploads' ] );
			return response()->json( [ 'path' => $path ], 200 );
		}

		return response()->json( trans( 'brackets/media::media.file.not_provided' ), 422 );
	}
}
