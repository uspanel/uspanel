<?php

namespace App\Http\Controllers\Auth;

use App\Traits\Utilitable;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notifications\VerifyPhone;
use App\Http\Requests\SendVerificationCode;
use App\Http\Requests\Register;
use App\PhoneVerifications;
use Carbon\Carbon;
use App\Http\Requests\StoreProfile;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Blacklist;
use App\Project;
use App\State;
use Illuminate\Support\Facades\Notification;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm($project_id)
    {
        $project = Project::where('project_id', $project_id)->first();
        if(!empty($project)){
            if ($project->id) {
                return view('auth.register', [
                    'project' => $project,
                    'states' => State::all()
                ]);
            }
        }

        abort(404);
    }

    use RegistersUsers, Utilitable;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'email' => $data['email'],
            'phone_number' => $data['phone'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'password' => $data['password'],
        ]);
        $project = Project::findOrFail($data['project_id']);
        Config::set('app.name', $project->company->name);
        Config::set('app.url', $project->company->site);
        $user->employee()->create(['project_id' => $project->id]);
        $user->profile()->create($data['profile']);
        if (
            ($project->company->companyType ?? false) &&
            ($project->company->companyType->title === 'Invest')
        ) {
            $user->assignRole('Employee Invest');
        } else {
            $user->assignRole('Employee Staff');
        }

        $user->employee->setStatus('new');
        // TODO notifications 11!
        Notification::send(User::role(['Administrator', 'HR Manager Staff', 'Support Staff'])->get(), new \App\Notifications\System( trans('notifications.new_user_has_registered', [
                'name' => $user->name
            ]), route('admin.users.index')));
        return $user;
    }

    /**
     * Send Verification Code
     * @param  SendVerificationCode $request [description]
     * @return [type]                        [description]
     */
    public function sendVerificationCode(SendVerificationCode $request) {
        $code = mt_rand(000000, 999999);
        while (strlen($code) != 6) {
            $code = mt_rand(000000, 999999);
        }
        if (env('APP_ENV') == 'production') {
            $user = new User;
            $user->phone_number = $this->clearPhone($request->phone);
            $user->notify((new VerifyPhone($code)));
        }
        $verification = PhoneVerifications::create([
            'phone' => $request->phone,
            'code' => $code,
            'deadline_at' => Carbon::now()->addMinutes(5),
        ]);
        if (env('APP_ENV') == 'production') {
            return response([]);
        }
        return response($verification);
    }

    /**
     * Check Code From SMS
     * @param  VerifyPhone $request [description]
     * @return [type]               [description]
     */
    public function verifyPhone(\App\Http\Requests\VerifyPhone $request) {
        PhoneVerifications::where('phone', $request->phone)->where('code', $request->get('code'))->firstOrFail()->delete();
        return response([], 200);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Register $request)
    {
        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return response([]);
    }
}
