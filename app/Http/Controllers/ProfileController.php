<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use App\Http\Requests\StoreProfile;
use App\Http\Requests\Profile\{
    ResendVerificationEmail,
    InterviewForm,
    InterviewByPhone
};
use Illuminate\Support\Facades\{
    Auth,
    Notification
};
use Spatie\MediaLibrary\Media;
use App\Notifications\UserMediaUpdated;
use App\User;

class ProfileController extends Controller
{

    public function index()
    {
        if (auth()->user()->hasRole('Employee Staff') && !auth()->user()->approved) {
            return redirect(route('employee.documents'));
        }
        if (auth()->user()->hasRole('Call support')) {
            return redirect(url('admin/users/needcalls'));
        }
        return redirect('/admin/profile');
    }

    public function documents()
    {
        $fields = auth()->user()->fields;
        if ($fields->isEmpty()) {
            $fields = auth()->user()->form()->first()->fields;
        }
        $form = $fields->mapWithKeys(
            function ($item) {
                return [$item['name'] => $item['value']];
            }
        );
        return view(
            'admin.employee.index',
            [
                'user' => auth()->user()->load('profile', 'employee'),
                'fields' => $fields,
                'form' => $form,
            ]
        );
    }

    /**
     * Upload Application Media
     * @param Request $request
     * @param string $mediaCollection
     * @return MediaCollection
     */
    public function uploadMedia(Request $request, string $mediaCollection)
    {
        auth()->user()->clearMediaCollection($mediaCollection);
        foreach ($request->all() as $key => $value) {
            auth()->user()->addMedia(storage_path('uploads/' . $value['path']))
                ->usingFileName($value['meta_data']['name'])
                ->toMediaCollection($mediaCollection);
        }
        Notification::send(
            User::role(['Administrator'])->get(),
            new UserMediaUpdated(auth()->user(), auth()->user()->getMediaCollection($mediaCollection))
        );
        if (Auth::user()->employee &&
            Auth::user()->employee->userStatus &&
            Auth::user()->employee->userStatus->name == 'wait_docs' &&
            count(auth()->user()->getMedia('contract')) &&
            count(auth()->user()->getMedia('agreement'))
        ) {
            // TODO notifications 8!
            auth()->user()->notifyUsers(
                trans(
                    'admin.user.uploaded_documents',
                    [
                        'name' => auth()->user()->name
                    ]
                )
            );
            Auth::user()->employee->setStatus('applicant');
        }

        $user = auth()->user();
        if (
            !$user->employee->is_contract_uploaded &&
            $user->getMedia('contract')->count()
        ) {
            Event::doActions(
                'UserStartWork',
                [
                    'message' => trans(
                        'notifications.user_start_work',
                        [
                            'name' => $user->name
                        ]
                    ),
                    'link' => '/admin/users?id=' . $user->id,
                    'user' => $user
                ]
            );

            $user->employee->update(
                [
                    'is_contract_uploaded' => 1
                ]
            );
        }
        return $user->getMedia($mediaCollection);
    }

    /**
     * Get Application Media
     * @param string $mediaCollection
     * @return MediaCollection
     */
    public function getMedia(string $mediaCollection)
    {
        return response(
            [
                'media' => auth()->user()->getMedia($mediaCollection)
            ]
        );
    }

    /**
     * Download Application Media
     * @param string $mediaCollection
     * @param Media $media
     * @return
     */
    public function downloadMedia(string $mediaCollection, Media $media, User $user)
    {
        $currentUser = auth()->user();
        if ($user && $currentUser->hasRole([
            'Administrator',
            'HR Manager Invest',
            'HR Manager Staff',
            'Support Staff',
            'Support Invest',
        ])) {
            $currentUser = $user;
        }
        $media = $currentUser->getMedia($mediaCollection)->where('id', $media->id)->first();
        return response()->download($media->getPath(), $media->file_name);
    }

    public function resendVerificationEmail(resendVerificationEmail $request)
    {
        auth()->user()->update(
            [
                'email' => $request->get('email', auth()->user()->email)
            ]
        );
        auth()->user()->sendEmailVerificationNotification();
        return response([]);
    }

    public function interview(InterviewForm $request)
    {
        $fields = auth()->user()->fields;
        if ($fields->isEmpty()) {
            $fields = auth()->user()->form()->first()->fields;
        }
        return view(
            'admin.employee.interview',
            [
                'user' => auth()->user()->load('profile', 'employee'),
                'fields' => $fields,
            ]
        );
    }

    public function interviewByPhone(InterviewByPhone $request)
    {
        auth()->user()->employee->setStatus('applicant');
        return response([]);
    }

}
