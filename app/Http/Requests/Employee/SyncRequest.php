<?php

namespace App\Http\Requests\Employee;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class SyncRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('crud.user.create') || Gate::allows('crud.user.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'investor_lists' => 'sometimes|array',
            'investors' => 'sometimes|array',
            'contragent_lists' => 'sometimes|array',
            'contragents' => 'sometimes|array',
        ];
    }
}
