<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Blacklist;

class SendVerificationCode extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !auth()->check() && !$this->checkInBlacklist();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|string',
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ];
    }

    protected function checkInBlacklist() {
        return Blacklist::ofEmail($this->email)->ofPhone($this->phone)->first();
    }
}
