<?php namespace App\Http\Requests\Admin\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use App\Role;

class UpdateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('crud.user.edit', $this->user);
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        $role = Role::findByName('Employee Staff');
        $investor = Role::findByName('Investor');
        $rules = [
            'name' => ['sometimes', 'string'],
            'email' => ['sometimes', 'email', Rule::unique('users', 'email')->ignore($this->user->getKey(), $this->user->getKeyName()), 'string'],
            'email_verified_at' => ['nullable', 'date'],
            'password' => ['sometimes', 'string'],
            'first_name' => ['nullable', 'string'],
            'last_name' => ['nullable', 'string'],
            'phone_number' => ['nullable', 'string'],
            'approved' => ['boolean'],
            'receive_external_calls' => 'boolean|nullable',
            'employee.user_status_id' => 'nullable|integer',
            'employee.project_id' => 'nullable|integer',
            'project_id' => 'required|exists:projects,id',
            'employees.*' => [
                'required_if:role_id,' . Role::findByName('Staffer')->id,
                'exists:employees,id'
            ],
            'employees' => [
                'required_if:role_id,' . Role::findByName('Staffer')->id,
            ],
            'role_id' => 'required|integer|exists:roles,id',
            'profile.address' => [
                'required_if:role_id,' . $role->id
            ],
            'profile.city' => [
                'required_if:role_id,' . $role->id
            ],
            'profile.state_id' => [
                'required_if:role_id,' . $role->id,
                'nullable',
                'exists:states,id'
            ],
            'profile.zip' => [
                'required_if:role_id,' . $role->id
            ],
            'profile.authorized_to_work' => 'sometimes|boolean',
            'profile.adult' => 'sometimes|boolean',
            'profile.higher_education' => 'sometimes|boolean',
            'employee.staffers.*' => [
                'required_if:role_id,' . $role->id,
                'exists:users,id'
            ],
            'employee.staffers' => [
                'required_if:role_id,' . $role->id,
            ],
            'employee.hr_id' => [
                'required_if:role_id,' . $role->id,
            ],
            'employee.support_id' => [
                'required_if:role_id,' . $role->id,
            ],
            'employee.contragents' =>[
                'sometimes',
                'array',
            ],
            'employee.contragent_lists' =>[
                'sometimes',
                'array',
            ],
            'employee.investors' =>[
                'sometimes',
                'array',
            ],
            'employee.investor_lists' =>[
                'sometimes',
                'array',
            ],
            'investor_profile.activity_years' => [
                'required_if:role_id,' . $investor->id
            ],
            'investor_profile.activity_time' => [
                'required_if:role_id,' . $investor->id
            ],
            'investor_profile.country' => [
                'required_if:role_id,' . $investor->id
            ],
            'investor_profile.comment' => [
                'required_if:role_id,' . $investor->id
            ],
            'investor_profile.manager_id' => [
                'required_if:role_id,' . $investor->id,
                'exists:users,id'
            ],
            'investor_mail.smtp_host' => [
                'required_if:role_id,' . $investor->id,
            ],
            'investor_mail.smtp_port' => [
                'required_if:role_id,' . $investor->id,
            ],
            'investor_mail.smtp_encryption' => [
                'required_if:role_id,' . $investor->id,
            ],
            'investor_mail.imap_host' => [
                'required_if:role_id,' . $investor->id,
            ],
            'investor_mail.imap_port' => [
                'required_if:role_id,' . $investor->id,
            ],
            'investor_mail.imap_encryption' => [
                'required_if:role_id,' . $investor->id,
            ],
            'investor_mail.imap_sent_folder' => [
                'required_if:role_id,' . $investor->id,
            ],
            'investor_mail.imap_inbox_folder' => [
                'required_if:role_id,' . $investor->id,
            ],
            'investor_mail.login' => [
                'required_if:role_id,' . $investor->id,
            ],
            'investor_mail.password' => [
                'sometimes',
            ],
            'investor_mail.validate_cert' => [
                'sometimes',
            ],
        ];

        $roles = [$this->get('role_id')];
        $excludedRoles = [1,2,3,7,8];
        foreach ($excludedRoles as $er) {
            if (in_array($er,$roles)) {
                $rules['project_id'] = 'nullable';
            }
        }

        return $rules;
    }
}
