<?php namespace App\Http\Requests\Admin\AutoAnswer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreAutoAnswer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('crud.auto-answer.create');
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'mailsetting_id' => 'required|exists:mailsettings,id',
            'mail_templates' => 'required|array',
            'mail_templates.*.pivot.delay' => 'required|numeric|min:0',
            'mail_templates.*.pivot.message_count' => 'required|numeric|min:1',

        ];
    }
}
