<?php namespace App\Http\Requests\Admin\Mailsetting;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreMailsetting extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('crud.mailsetting.create');
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {


        $rules = [
            'smtp_host' => ['required', 'string'],
            'smtp_port' => ['required', 'integer'],
            'smtp_encryption' => ['nullable', 'string'],
            'imap_host' => ['required', 'string'],
            'imap_port' => ['required', 'integer'],
            'imap_encryption' => ['nullable', 'string'],
            'login' => ['required', 'string'],
            'password' => ['required', 'string'],
            'default' => ['boolean', 'nullable'],
            'validate_cert' => ['boolean', 'nullable'],
            'meiling' => ['nullable'],

        ];

        if ($this['meiling']) {

            $rules['names'] = [
                function ($attribute, $value, $fail) {
                        if(!$value){
                            $fail(trans('admin.mailsetting.errors.names') );
                        }
                },
            ];
            $rules['domains'] = ['required'];
        }else{
            $rules['imap_sent_folder'] = ['required', 'string'];
            $rules['imap_inbox_folder'] = ['required', 'string'];
            $rules['imap_host'] = ['required', 'string'];
            $rules['imap_port'] = ['required', 'string'];
            $rules['imap_encryption'] = ['nullable', 'string'];
        }

        return $rules;
    }


    public function getModifiedDataForAdditionalMeilsettingNames($meilsettings_id){
        $data = [];
        foreach ($this['names'] as $name){
            $data[] = [
                'set_id' => $meilsettings_id,
                'name' => $name['name'],
                'name_domain' => $name['name_domain'],
            ];
        }

        return $data;
    }

    public function getModifiedDataForAdditionalMeilsettingDomains($meilsettings_id){

        $domains = explode(',', $this['domains']);
        $data = [];
        foreach ($domains as $domain){
            $data[] = [
                'set_id' => $meilsettings_id,
                'domain' => trim($domain)
            ];
        }

        return $data;
    }

    public function getModifiedData(): array
    {

        $data = $this->only(collect($this->rules())->except(['names', 'domains', 'meiling'])->keys()->all());

        return $data;
    }
}
