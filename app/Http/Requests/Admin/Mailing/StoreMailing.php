<?php namespace App\Http\Requests\Admin\Mailing;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreMailing extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('crud.mailing.create');
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'mail_template_id' => ['required', 'integer'],
            'scheduled_at' => ['nullable', 'date'],
            'email_groups' => ['array'],
            'max_per_min' => 'integer|min:0'
            
        ];
    }
}
