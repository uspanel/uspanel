<?php namespace App\Http\Requests\Admin\Mailing;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateMailing extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('crud.mailing.edit', $this->mailing);
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'name' => ['sometimes', 'string'],
            'mail_template_id' => ['sometimes', 'integer'],
            'scheduled_at' => ['nullable', 'date'],
            'email_groups' => ['array'],
            'max_per_min' => 'integer|min:0'
        ];
    }
}
