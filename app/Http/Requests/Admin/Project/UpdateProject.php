<?php namespace App\Http\Requests\Admin\Project;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateProject extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {


        $hrSum = 0;
        foreach ($this->get('hr_percent') as $percent) {
            $hrSum += $percent;
        }
        $supSum = 0;
        foreach ($this->get('support_percent') as $percent) {
            $supSum += (int)$percent;
        }
        $this->merge(['hr_sum' => $hrSum, 'support_sum' => $supSum]);

        return Gate::allows('crud.project.edit', $this->project);
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'company.name' => ['sometimes', 'string'],
            'company.company_type_id' => ['nullable', 'integer', 'min:1'],
            'company.city' => ['nullable', 'string'],
            'company.state_id' => ['nullable', 'integer'],
            'company.address' => ['nullable', 'string'],
            'company.zip' => ['nullable', 'string'],
            'company.phone' => ['nullable', 'string'],
            'company.fax' => ['nullable', 'string'],
            'company.site' => ['nullable', 'string'],
            'company.info_email' => ['sometimes', 'string'],
            'manager_ids' => [
                function ($attribute, $value, $fail) {
                    if(empty($value)){
                        $fail(trans('admin.project.columns.manager'));
                    }
                },
            ],
            'support_ids' => [
                function ($attribute, $value, $fail) {
                    if(empty($value)){
                        $fail(trans('admin.project.columns.support'));
                    }
                },
            ],
            'company.control_panel' => ['nullable', 'string'],
            'company.position_name' => ['nullable', 'string'],
            'company.application' => ['nullable', 'string'],
            'form_ids' => ['required', 'array'],
            'name' => ['required', 'string', 'max:255'],
            'salary' => ['required', 'max:255', 'regex:/^\$?[\d,]+(\.\d*)?$/'],
            'hr_percent.*' => 'required|integer|min:0|max:100',
            'hr_sum' => 'in:99,100',
            'support_percent.*' => 'required|integer|min:0|max:100',
            'support_sum' => 'in:99,100',
            'mail_domain' => 'nullable|string',
            'project_id' => 'required'
        ];
    }
}
