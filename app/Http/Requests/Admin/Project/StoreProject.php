<?php namespace App\Http\Requests\Admin\Project;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreProject extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {


        $hrSum = 0;
        foreach ($this->get('hr_percent') as $percent) {
            $hrSum += $percent;
        }
        $supSum = 0;
        foreach ($this->get('support_percent') as $percent) {
            $supSum += $percent;
        }
        $this->merge(['hr_sum' => $hrSum, 'support_sum' => $supSum]);

        return Gate::allows('crud.project.create');
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {

        $rules = [
            'name' => ['required', 'string', 'max:255'],
            'company.name' => ['required', 'max:255'],
            'company.city' => ['required', 'max:255'],
//            'company.company_type_id' => ['required', 'integer', 'min:1'],
            'company.state_id' => ['required', 'integer'],
            'company.address' => ['required', 'max:255'],
            'company.zip' => ['required', 'max:255'],
            'company.phone' => ['required', 'max:255'],
            'company.fax' => ['nullable', 'max:255'],
            'company.site' => ['nullable', 'max:255'],
            'company.info_email' => ['required', 'email', 'max:255'],
            'manager_ids' => [
                function ($attribute, $value, $fail) {
                    if(empty($value)){
                        $fail(trans('admin.project.columns.manager'));
                    }
                },
            ],
            'support_ids' => [
                function ($attribute, $value, $fail) {
                    if(empty($value)){
                        $fail(trans('admin.project.columns.support'));
                    }
                },
            ],
            'company.control_panel' => ['nullable', 'max:255'],
            'company.position_name' => ['nullable', 'max:255'],
            'company.application' => ['nullable', 'max:255'],
            'salary' => ['required', 'max:255', 'regex:/^\$?[\d,]+(\.\d*)?$/'],
            'form_ids' => ['required', 'array'],
            'mail_domain' => 'nullable|string'
        ];


            $rules['hr_percent.*'] = ['required', 'integer', 'min:0', 'max:100'];
            $rules['support_percent.*'] = ['required','integer','min:0','max:100'];
            $rules['hr_sum'] = ['in:99,100'];
            $rules['support_sum'] = ['in:99,100'];


        return $rules;
    }
}
