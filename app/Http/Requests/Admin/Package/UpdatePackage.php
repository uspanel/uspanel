<?php namespace App\Http\Requests\Admin\Package;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdatePackage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('crud.package.view', $this->package);
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'track' => 'sometimes|max:255',
            'carrier_id' => 'sometimes|integer|nullable',
            'weight' => 'sometimes|numeric|nullable',
            'name' => 'sometimes|required|max:255',
            'manager_id' => 'sometimes|integer|nullable',
            'comment' => 'sometimes|max:1000',
            'status_id' => 'sometimes|required|integer',
            'description' => 'sometimes|max:1000',
            'price' => 'sometimes|numeric|nullable',
            'color' => 'sometimes|max:255',
            'link' => 'sometimes|max:255',
            'track' => 'sometimes|required',
            'carrier_id' => 'sometimes|exists:carriers,id|required',
            'employee_id' => 'sometimes|required|integer|exists:employees,id',
            'item_ids' => [
                function ($attribute, $value, $fail) {
                    if (empty($value) && empty($this->item_ids)) {
                        $fail(trans('admin.package.empty_items'));
                    }
                },
            ],
            'item_ids.*.id' => 'required|exists:items,id',
        ];
    }
}
