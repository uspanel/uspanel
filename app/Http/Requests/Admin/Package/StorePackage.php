<?php namespace App\Http\Requests\Admin\Package;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StorePackage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('crud.package.view');
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'track' => 'string|required',
            'carrier_id' => 'integer|required',
            'weight' => 'numeric|nullable',
            'name' => 'required|max:255',
            'manager_id' => 'integer|nullable',
            'comment' => 'max:1000',
            'status_id' => 'required|integer',
            'description' => 'max:1000',
            'item_ids' => [
                function ($attribute, $value, $fail) {
                    if (empty($value) && empty($this->item_ids)) {
                        $fail(trans('admin.package.empty_items'));
                    }
                },
            ],
            'item_ids.*.id' => 'sometimes|nullable|exists:items,id',
            'employee_id' => 'required|integer|exists:employees,id',
        ];
    }
}
