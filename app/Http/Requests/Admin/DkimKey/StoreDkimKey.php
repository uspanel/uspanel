<?php namespace App\Http\Requests\Admin\DkimKey;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreDkimKey extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('admin.dkim-key.create');
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'project_id' => ['required', 'integer'],
            'domain' => ['required', 'string'],
            'dkim_record' => ['required', 'string'],
            
        ];
    }
}
