<?php namespace App\Http\Requests\Admin\DkimKey;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateDkimKey extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('admin.dkim-key.edit', $this->dkimKey);
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'project_id' => ['sometimes', 'integer'],
            'domain' => ['sometimes', 'string'],
            'dkim_record' => ['sometimes', 'string'],
            
        ];
    }
}
