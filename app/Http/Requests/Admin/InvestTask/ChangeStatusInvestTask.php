<?php namespace App\Http\Requests\Admin\InvestTask;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class ChangeStatusInvestTask extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Auth::user()->hasRole(['Administrator', 'Employee Invest']);
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'invest_task_status' => ['nullable', 'array']
        ];
    }
}
