<?php namespace App\Http\Requests\Admin\InvestTask;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateInvestTask extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('crud.invest-task.edit', $this->investTask);
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'name' => ['nullable', 'string'],
            'theme' => ['nullable', 'string'],
            'text' => ['nullable', 'string'],
            'deadline' => ['nullable', 'date'],
            'investors' => ['nullable', 'array'],
            'contragents' => ['nullable', 'array'],
            'employee' => ['nullable', 'array'],
            'manager' => ['nullable', 'array'],
            'creator' => ['nullable', 'array'],
            'investor_lists' => ['nullable', 'array'],
            'contragent_lists' => ['nullable', 'array'],
            'invest_task_status' => ['nullable', 'array'],
            'deleted_at' => ['nullable']
        ];
    }
}
