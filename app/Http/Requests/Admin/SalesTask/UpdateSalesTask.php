<?php namespace App\Http\Requests\Admin\SalesTask;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateSalesTask extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('crud.sales-task.edit', $this->salesTask);
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'delivered_at' => ['nullable','date'],
            'warehouse_id' => [
                'sometimes',
                'integer',
                'exists:warehouses,id'
            ],
            'carrier_id' => [
                'sometimes',
                'integer',
                'exists:carriers,id'
            ],
            'track' => ['sometimes','string'],
            'sku'   => ['sometimes','string'],
            'employee_status_id' => [
                'sometimes',
                'nullable',
                'exists:employee_statuses,id'
            ],
            'comment' => ['nullable','string'],
            'manager_id' => [
                'sometimes',
                'integer',
                'exists:users,id'
            ],
            'employee_id' => [
                'sometimes',
                'integer',
                'exists:users,id'
            ],
            'sales_status_id' => [
                'sometimes',
                'integer',
                'exists:sales_statuses,id'
            ],
        ];
    }
}
