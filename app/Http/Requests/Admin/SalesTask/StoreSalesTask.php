<?php namespace App\Http\Requests\Admin\SalesTask;

use App\Warehouse;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreSalesTask extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('crud.sales-task.create');
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'delivered_at' => ['nullable','date'],
            'warehouse_id' => [
                'sometimes',
                'integer',
                'exists:warehouses,id'
            ],
            'carrier_id' => [
                'required',
                'integer',
                'exists:carriers,id'
            ],
            'track' => ['required','string'],
            'sku'   => ['required','string'],
            'employee_status_id' => [
                'sometimes',
                'nullable',
                'exists:employee_statuses,id'
            ],
            'comment' => ['nullable','string'],
            'manager_id' => [
                'required',
                'integer',
                'exists:users,id'
            ],
            'employee_id' => [
                'sometimes',
                'integer',
                'exists:users,id'
            ],
            'sales_status_id' => [
                'sometimes',
                'integer',
                'sales_statuses:exists,id'
            ],
            'item_ids' => [
                function ($attribute, $value, $fail) {
                    if (empty($value)) {
                        $fail(trans('admin.task.empty_items'));
                    }
                },
            ],
            'item_ids.*' => [
                function ($attribute, $value, $fail) {
                    $warehouse = Warehouse::findOrFail($this->get('warehouse_id'));
                    $id = explode('.',$attribute)[1];
                    if (empty($value['qty'])) {
                        $fail(trans('admin.task.empty_item'));
                    }
                    if (empty($value['price'])) {
                        $fail(trans('admin.sales-task.empty_price'));
                    }
                    if ($warehouse->getItemQty($id) < $value['qty']) {
                        $fail(trans('admin.task.quantity_overlap'));
                    }
                },
            ],
        ];
    }
}
