<?php

namespace App\Http\Requests\Admin\BusinessProcess;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class UpdateBusinessProcessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('crud.business-process.edit');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'regex:/(^[A-Za-z0-9 ]+$)+/|required',
            'active' => 'required|boolean',
            'business_process_type_id' => 'numeric|required',
            'business_process_events' => 'array|required',
            'business_process_events.*.event.id' => 'numeric|required',
            'business_process_events.*.action_business_process_events' => 'array|required',
            'business_process_events.*.action_business_process_events.*.action.id' => 'numeric|required',
            'business_process_events.*.action_business_process_events.*.users.*.id' => 'numeric|required',
        ];
    }
}
