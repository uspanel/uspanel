<?php namespace App\Http\Requests\Admin\Task;

use App\Employee;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreTask extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('crud.task.create');
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {

        return [
            'warehouse_id' => 'required|exists:warehouses,id',
            'date' => 'date|nullable',
            'carrier_id' => 'integer|min:1|required',
            'track' => 'string|required',
            'sku' => 'string|required',
            'employee_status_id' => 'integer|nullable',
            'comment' => 'string|nullable',
            'manager_id' => 'integer|min:1|nullable',
            'employee_id' => 'required|integer|exists:employees,id',
            'item_ids' => [
                function ($attribute, $value, $fail) {
                    if (empty($value)) {
                        $fail(trans('admin.task.empty_items'));
                    }
                },
            ],
            'item_ids.*' => [
                function ($attribute, $value, $fail) {
                    $employee = Employee::findOrFail($this->get('employee_id'));
                    $id = explode('.',$attribute)[1];
                    if (empty($value)) {
                        $fail(trans('admin.task.empty_item'));
                    }
                    if ($employee->getItemQty($id) < $value) {
                        $fail(trans('admin.task.quantity_overlap'));
                    }
                },
            ],
        ];
    }
}
