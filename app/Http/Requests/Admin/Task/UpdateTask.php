<?php namespace App\Http\Requests\Admin\Task;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use App\EmployeeStatus;

class UpdateTask extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('crud.task.view') && auth()->user()->approved;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        $rules = [
            'warehouse_id' => 'sometimes|required|exists:warehouses,id',
            'date' => 'sometimes|date|nullable',
            'carrier_id' => 'sometimes|integer|min:1|required',
            'track' => 'sometimes|string|required',
            'employee_status_id' => [
                'integer',
                'sometimes',
                'nullable',
                function ($attribute, $value, $fail) {
                    if (auth()->user()->hasRole('Employee Staff') && EmployeeStatus::findOrFail($value)->name == 'sent') {
                        if ($this->task->activities->isEmpty()) {
                            $fail(trans('admin.task.unable_to_track_packages_make_sure_you_send_packages'));
                        }
                    }
                },
            ],
            'comment' => 'sometimes|string|nullable',
            'manager_id' => 'sometimes|integer|min:1|nullable',
            'employee_id' => 'sometimes|required|integer|exists:employees,id',
            'item_ids' => [
                'sometimes',
                function ($attribute, $value, $fail) {
                    if (empty($value)) {
                        $fail(trans('admin.task.empty_items'));
                    }
                },
            ],
            'item_ids.*' => 'sometimes|required|integer'
        ];
        if ($this->task) {
            $rules['employee_id'] = 'integer';
        }
        return $rules;
    }
}
