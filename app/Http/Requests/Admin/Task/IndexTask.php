<?php namespace App\Http\Requests\Admin\Task;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class IndexTask extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('crud.task.view') && auth()->user()->approved;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'orderBy' => 'in:id, warehouse_id,delivered_at,carrier_id,track,carrier_tracking_status,employee_status,comment,creator_id,manager_id|nullable',
            'orderDirection' => 'in:asc,desc|nullable',
            'date' => 'date|nullable',
            'carrier_id' => 'integer|min:1|nullable',
            'track' => 'string|nullable',
            'carrier_tracking_status' => 'string|nullable',
            'employee_status_id' => 'integer|nullable',
            'comment' => 'string|nullable',
            'creator_id' => 'integer|min:1|nullable',
            'manager_id' => 'integer|min:1|nullable',
            'page' => 'integer|nullable',
            'per_page' => 'integer|nullable',

        ];
    }
}
