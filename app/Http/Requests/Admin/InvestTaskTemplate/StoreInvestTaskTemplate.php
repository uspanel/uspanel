<?php namespace App\Http\Requests\Admin\InvestTaskTemplate;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreInvestTaskTemplate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('crud.invest-task-template.create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'name' => ['nullable', 'string'],
            'theme' => ['nullable', 'string'],
            'text' => ['nullable', 'string'],
            'investors' => ['nullable', 'array'],
            'contragents' => ['nullable', 'array'],
            'manager' => ['nullable', 'array'],
            'investor_lists' => ['nullable', 'array'],
            'contragent_lists' => ['nullable', 'array'],
            'mail_template_id' => ['sometimes', 'exists:mail_templates,id']
        ];
    }
}
