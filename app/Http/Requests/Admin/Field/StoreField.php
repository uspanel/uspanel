<?php namespace App\Http\Requests\Admin\Field;

use Brackets\Translatable\TranslatableFormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreField extends TranslatableFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('crud.field.create');
    }

/**
     * Get the validation rules that apply to the requests untranslatable fields.
     *
     * @return    array
     */
    public function untranslatableRules() {
        return [
            'name' => 'required|max:255',
            'title' => 'required|max:255',
            'description' => 'max:255',
            'field_type_id' => 'required|integer',
            'options.*.value' => 'max:255',
            'options.*.text' => 'max:255',
            'rules.*' => 'max:255',
            'ordering' => 'integer',
            'enabled' => 'boolean',
            'sortable' => 'boolean',
            'searchable' => 'boolean',
            'visible' => 'boolean',
            'exportable' => 'boolean',
        ];
    }

    /**
     * Get the validation rules that apply to the requests translatable fields.
     *
     * @return    array
     */
    public function translatableRules($locale) {
        return [
            'options' => ['nullable', 'string'],
            
        ];
    }
}
