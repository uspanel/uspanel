<?php namespace App\Http\Requests\Admin\EmailGroup;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class ParseCsv extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('crud.email-group.edit', $this->emailGroup);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'group_id' => 'nullable|required_without:group_name|exists:email_groups,id',
            'group_name' => 'nullable|required_without:group_id|string|unique:email_groups,name',
            'list' => 'required|file|mimes:txt,csv'
        ];
    }
}
