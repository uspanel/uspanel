<?php namespace App\Http\Requests\Admin\Contragent;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateContragent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return Gate::allows('crud.contragent.edit', $this->contragent);
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'business_processes' => ['sometimes', 'array'],
            'name' => ['sometimes', 'string'],
            'investing_amount' => ['sometimes', 'numeric'],
            'country' => ['nullable', 'string'],
            'roi' => ['nullable', 'numeric'],

        ];
    }
}
