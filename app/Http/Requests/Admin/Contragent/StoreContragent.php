<?php namespace App\Http\Requests\Admin\Contragent;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreContragent extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return auth()->check() && Gate::allows('crud.contragent.create');
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'investing_amount' => ['required', 'numeric'],
            'business_processes' => ['sometimes', 'array'],
            'country' => ['nullable', 'string'],
            'roi' => ['nullable', 'numeric'],

        ];
    }
}
