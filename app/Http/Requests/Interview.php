<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Interview extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (auth()->user()->hasRole(['Employee Staff', 'Employee Invest']) && auth()->id() == $this->user->id) {
            return true;
        }
        return auth()->user()->hasAnyRole('Administrator', 'HR Manager Staff');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
