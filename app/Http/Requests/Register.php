<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;
use App\State;

class Register extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return !auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $client = \GeoIP::getLocation(\GeoIP::getClientIP());
        return [
            'cv' => [
                'required',
                function ($attribute, $value, $fail) {
                    foreach ($value as $item) {
                        if (!Storage::disk('uploads')->exists($item['path'])) {
                            $fail($attribute.' is invalid.');
                        }
                    }
                },
            ],
            'country' => [
                function ($attribute, $value, $fail) use ($client) {
                    if ($client->country != 'United States') {
                        if (env('CHECK_GEO_IP') == true) {
                            $fail(trans('admin.does_not_match_your_location_determined_by_ip', [
                                'attribute' => 'Country'
                            ]));
                        }
                    }
                },
            ],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'profile.address' => 'required',
            'profile.city' => [
                'required',
                function ($attribute, $value, $fail) use ($client) {
                    if (strpos(strtolower($client->city), strtolower($value)) === false) {
                        if (env('CHECK_GEO_IP') == true) {
                            $fail(trans('admin.does_not_match_your_location_determined_by_ip', [
                                'attribute' => 'City'
                            ]));
                        }
                    }
                },
            ],
            'profile.state_id' => [
                'required',
                function ($attribute, $value, $fail) use ($client) {
                    $state = State::find($value)->id ?? null;
                    if (strpos(strtolower($client->state_name), strtolower($state)) === false) {
                        if (env('CHECK_GEO_IP') == true) {
                            $fail(trans('admin.does_not_match_your_location_determined_by_ip', [
                                'attribute' => 'State'
                            ]));
                        }
                    }
                },
            ],
            'profile.zip' => 'required',
            'profile.authorized_to_work' => 'boolean',
            'profile.adult' => 'boolean',
            'profile.higher_education' => 'boolean',
            'profile.referrer' => 'sometimes|nullable|string',
        ];
    }
}
