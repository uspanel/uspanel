<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ActionBusinessProcessEvent extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'users' => \App\Http\Resources\User::collection($this->users)->jsonSerialize(),
            'roles' => \App\Http\Resources\Role::collection($this->roles)->jsonSerialize(),
            'action' => (new \App\Http\Resources\Action($this->action))->jsonSerialize(),
        ];
    }
}
