<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BusinessProcessEvent extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'event' => (new \App\Http\Resources\Event($this->event))->jsonSerialize(),
            'action_business_process_events' => \App\Http\Resources\ActionBusinessProcessEvent::collection($this->actionBusinessProcessEvents)->jsonSerialize(),
        ];
    }
}
