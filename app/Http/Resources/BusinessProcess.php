<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BusinessProcess extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'active' => $this->active,
            'business_process_type' => $this->businessProcessType,
            'business_process_events' => \App\Http\Resources\BusinessProcessEvent::collection($this->businessProcessEvents)->jsonSerialize()
        ];
    }
}
