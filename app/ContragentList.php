<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ContragentList
 * @package App
 */
class ContragentList extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        "name",

    ];

    /**
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * @var array
     */
    protected $dates = [
        "created_at",
        "updated_at",

    ];

    /**
     * @var array
     */
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getResourceUrlAttribute() {
        return url('/admin/contragent-lists/'.$this->getKey());
    }


    /**
     * Get the Contragents record associated with the Contragent List.
     */
    public function contragents()
    {
        return $this->belongsToMany('App\Contragent');
    }

}
