<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Artisan;

class RegisterNewEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    protected $userId = 0;

    /**
     * @var int
     */
    protected $projectId = 0;

    /**
     * Create a new job instance.
     *
     * @param int $userId
     * @param int|null $projectId
     */
    public function __construct(int $userId, ?int $projectId)
    {
        $this->userId = $userId;
        $this->projectId = $projectId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Artisan::call('mail_account:create',['--user' => $this->userId, '--project' => $this->projectId]);
    }
}
