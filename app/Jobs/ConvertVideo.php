<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Pbmedia\LaravelFFMpeg\FFMpegFacade;

class ConvertVideo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $media;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($media)
    {
        $this->media = $media;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $formatMp4 = new \FFMpeg\Format\Video\X264;
        $formatMp4->setAudioCodec("libvo_aacenc");
        $formatMp4->setKiloBitrate(500);

        $formatWebm = new \FFMpeg\Format\Video\WebM;
        $formatWebm->setKiloBitrate(500);

        if ($this->media->extension != 'webm') {
            FFMpegFacade::fromDisk($this->media->disk)
                ->open($this->media->id . '/' . $this->media->file_name)
                ->export()
                ->toDisk($this->media->disk)
                ->inFormat($formatWebm)
                ->save('/' . $this->media->id . '/conversions/conv.webm');
        }
        if ($this->media->extension != 'mp4') {
            FFMpegFacade::fromDisk($this->media->disk)
                ->open($this->media->id . '/' . $this->media->file_name)
                ->export()
                ->toDisk($this->media->disk)
                ->inFormat($formatMp4)
                ->save('/' . $this->media->id . '/conversions/conv.mp4');
        }
        FFMpegFacade::cleanupTemporaryFiles();
        if (env('QUEUE_CONNECTION') == 'database') {
            $this->media->model->update(['processing' => 0]);
        }
    }
}
