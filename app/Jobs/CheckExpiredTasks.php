<?php

namespace App\Jobs;

use App\Event;
use App\InvestTask;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Task;
use App\EmployeeStatus;
use Carbon\Carbon;


class CheckExpiredTasks implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        // TODO сколько времени на принятие задачи
        $tasks = Task::whereRaw(
            'NOW() >= DATE_ADD(created_at, INTERVAL ' . \App\Setting::getSetting('did_not_complete_task') . ' HOUR)'
        )->whereNull('employee_status_id')->get();
        foreach ($tasks as $key => $value) {
            if ($value->employee) {
                $value->notifyUsers(
                    trans(
                        'admin.task.employee_delayed_task',
                        [
                            'name' => $value->employee->name
                        ]
                    )
                );
            }
            Event::doActions(
                'TaskExpired',
                [
                    'message' => trans(
                        'notifications.task_expired',
                        [
                            'id' => $value->id,
                        ]
                    ),
                    'link' => '/admin/tasks?id=' . $value->id,
                    'task' => $value
                ]
            );
        }

        $tasks = InvestTask::whereRaw(
            'NOW() >= DATE_ADD(created_at, INTERVAL ' . \App\Setting::getSetting('did_not_complete_task') . ' HOUR)'
        )->get();
        foreach ($tasks as $key => $value) {
            $contragents = $value->getUniqueContragents();
            activity()
                ->withContragent($contragents)
                ->on($value)
                ->log(trans(
                    'notifications.invest_task_expired',
                    [
                        'id' => $value->id,
                    ]
                ));

            if ($value->employee) {
                $value->notifyUsers(
                    trans(
                        'admin.task.employee_delayed_task',
                        [
                            'name' => $value->employee->name
                        ]
                    )
                );
            }

            Event::doActions(
                'TaskExpired',
                [
                    'message' => trans(
                        'notifications.invest_task_expired',
                        [
                            'id' => $value->id,
                        ]
                    ),
                    'link' => '/admin/invest-tasks?id=' . $value->id,
                    'task' => $value,
                    'business_process_type' => 'invest'
                ]
            );
        }
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }
}
