<?php

namespace App\Jobs;

use App\Carrier;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Ups\Tracking;

class TrackingUps implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $carrier = Carrier::whereName('Ups')->firstOrFail();
        $tasks = $carrier->tasks()->undelivered()->withTrack()->get()->keyBy('track');
        foreach($tasks as $task){
            $this->track($task);
        }
        $packages = $carrier->packages()->undelivered()->withTrack()->get()->keyBy('track');
        foreach($packages as $package){
            $this->track($package);
        }
        $salesTasks = $carrier->salesTasks()->undelivered()->withTrack()->get()->keyBy('track');
        foreach($salesTasks as $salesTask){
            $this->track($salesTask);
        }
    }

    /**
     * @param $task
     * @return bool
     */
    private function track($model)
    {
        sleep(1);
        $tracking = new Tracking(
            config('services.ups.auth.key'),
            config('services.ups.auth.account'),
            config('services.ups.auth.secure'),
            app()->environment() === 'production' ? false : true
        );
        $model->activities()->delete();

        try {
            $shipment = $tracking->track($model->track);
            $package = $shipment->Package;
            if($package->DeliveryIndicator === "Y"){
                $model->delivered_at = Carbon::createFromFormat('Ymd', $package->DeliveryDate)->startOfDay();
                $model->save();
            }
        }catch(\Throwable $exception){
            if (env('APP_ENV') == 'production') {
                $model->activities()->create([
                    'title' => $exception->getMessage(),
                ]);
            }
            return false;
        }

        $responseActivities = is_array($shipment->Package->Activity)
            ? $shipment->Package->Activity
            : [$shipment->Package->Activity];

        foreach ($responseActivities as $cp) {
            $model->activities()->create([
                'title' => $cp->Status->StatusType->Description ?? null,
                'city' => $cp->ActivityLocation->Address->City ?? null,
                'date' => isset($cp->Date) ? Carbon::createFromFormat('YmdHis',$cp->Date . $cp->Time) : null,
            ]);
        }
        return true;
    }
}
