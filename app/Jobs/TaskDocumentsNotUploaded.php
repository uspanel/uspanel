<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Task;
use App\EmployeeStatus;
use Carbon\Carbon;

class TaskDocumentsNotUploaded implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // TODO сколько времени на принятие задачи
        $status = EmployeeStatus::ofName('sent')->first();
        $mediaCollections = [
            'receipt',
        ];
        if ($status) {
            $tasks = Task::where('employee_status_id', $status->id)
            ->whereHas('activities')
            ->get();        
            foreach ($tasks as $key => $task) {
                foreach ($mediaCollections as $mediaCollection) {
                    if ($task->getMedia($mediaCollection)) {
                        $task->notifyUsers(trans('admin.task.employee_did_not_upload_task_files', [
                            'mediaCollection' => $mediaCollection,
                            'name' => $task->employee->name
                        ]));
                    }
                }
            }
        }
    }
}
