<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\User;
use App\UserStatus;

class CheckEmployeeDocuments implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $users = User::whereRaw('NOW() >= DATE_ADD(users.created_at, INTERVAL ' . \App\Setting::getSetting('did_not_upload_documents') . ' HOUR)')
        ->whereHas('employee', function ($query) {
            $query->where('user_status_id', UserStatus::ofName('wait_docs')->first()->id ?? null);
        })
        ->get();
        foreach ($users as $key => $user) {
            $user->notifyUsers(trans('admin.user.did_not_upload_documents', [
                'name' => $user->name
            ]));
        }
    }
}
