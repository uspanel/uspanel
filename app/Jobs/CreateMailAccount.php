<?php

namespace App\Jobs;

use App\Notifications\NewEmailAccount;
use App\Project;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class CreateMailAccount implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Project
     */
    private $project;
    /**
     * @var User
     */
    private $user;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param Project $project
     */
    public function __construct(User $user, Project $project)
    {
        $this->user = $user;
        $this->project = $project;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        $project = $this->project;
        $restartSleep = 60;
        $localAccPath = base_path('docker/mail/config/postfix-accounts.cf');
        $newEmail = str_replace(' ', '', strtolower($user->roles()->first()->name)) . $user->id . '@' . $project->mail_domain;
        $password = str_random(8);
        $accountsFile = file_exists($localAccPath) ?
            file_get_contents($localAccPath) :
            '';
        if (!strpos($accountsFile,$newEmail)) {
            if (file_exists($localAccPath)) {
                $this->runRemote('echo "'.$newEmail.'|$(doveadm pw -s SHA512-CRYPT -u '.$newEmail.' -p '.$password.')" >> /tmp/docker-mailserver/postfix-accounts.cf');
                sleep(10);
            } else {
                $this->runRemote('echo "'.$newEmail.'|$(doveadm pw -s SHA512-CRYPT -u '.$newEmail.' -p '.$password.')" >> /tmp/docker-mailserver/postfix-accounts.cf && /etc/init.d/supervisor restart');
                sleep($restartSleep);
            }
            if (!file_exists(base_path('docker/config/opendkim/keys/'.$project->mail_domain.'/mail.txt'))) {
                $this->runRemote('generate-dkim-config 2048');
                sleep($restartSleep);
                $stdOutArray = $this->runRemote('cat /tmp/docker-mailserver/opendkim/keys/'.$project->mail_domain.'/mail.txt');
                $stdOutString = implode('',$stdOutArray);
                $matches = [];
                preg_match('/\(\s(.*)\s\)/',$stdOutString,$matches);
                if (!empty($matches[1])) {
                    $project->dkimKey()->updateOrCreate([
                        'domain' => 'mail._domainkey.'.$project->mail_domain,
                        'dkim_record' => $matches[1]
                    ]);
                } else {
                    Log::error('Can not write dkim keys for domain '.$project->mail_domain.' in BD. Please check manually.');
                }
                sleep(10);
                $this->runRemote('/etc/init.d/supervisor restart');
                sleep($restartSleep);
            }
            $user->mailsettings()->create([
                'smtp_host' => config('mail-accounts.mail_server_fqdn'),
                'smtp_port' => config('mail-accounts.smtp_port'),
                'smtp_encryption' => config('mail-accounts.smtp_encryption'),
                'imap_host' => config('mail-accounts.mail_server_fqdn'),
                'imap_port' => config('mail-accounts.imap_port'),
                'imap_encryption' => config('mail-accounts.imap_encryption'),
                'imap_sent_folder' => config('mail-accounts.sent_folder'),
                'imap_inbox_folder' => config('mail-accounts.inbox_folder'),
                'default' => $user->mailsettings()->count() ? 0 : 1,
                'login' => $newEmail,
                'password' => $password
            ]);
            $user->email = $newEmail;
            try {
                $user->notify( new NewEmailAccount($newEmail,$password));
            } catch(\Exception $e){
                Log::error('Email account with login '.$newEmail.' and password '.$password.' was created but sending email was failed with error: '.$e->getMessage());
            }
        } else {
            Log::error('Unable to register email for user ' . $user->id . ' in project ' . $project->id . '! Email already registered.');
        }
    }

    /**
     * Run command remotely and get output array
     * @param string $command
     * @return array
     */
    private function runRemote(string $command) : array
    {
        \SSH::run($command, function($line) use (&$elements) {
            $elements .= $line;
        });
        $result = array_filter(explode(PHP_EOL,$elements));
        return $result;
    }
}
