<?php

namespace App\Jobs;

use App\Carrier;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use DHL\Entity\EA\KnownTrackingRequest as Tracking;
use DHL\Entity\EA\TrackingResponse;
use DHL\Client\Web as WebserviceClient;

class TrackingDhl implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $carrier = Carrier::whereName('Dhl')->firstOrFail();
        $tasks = $carrier->tasks()->undelivered()->withTrack()->get()->keyBy('track');
        foreach($tasks as $task){
            $this->track($task);
        }
        $packages = $carrier->packages()->undelivered()->withTrack()->get()->keyBy('track');
        foreach($packages as $package){
            $this->track($package);
        }
        $salesTasks = $carrier->salesTasks()->undelivered()->withTrack()->get()->keyBy('track');
        foreach($salesTasks as $salesTask){
            $this->track($salesTask);
        }

    }

    /**
     * @param $task
     * @return bool
     */
    private function track($model)
    {
        sleep(1);

        $request = new Tracking();
        $request->SiteID = config('services.dhl.' . (app()->environment() === 'production' ? 'production' : 'local') . '.auth.site_id');
        $request->Password = config('services.dhl.' . (app()->environment() === 'production' ? 'production' : 'local') . '.auth.password');
        $request->MessageReference = '1234567890123456789012345678';
        $request->MessageTime = '2002-06-25T11:28:55-08:00';
        $request->LanguageCode = 'en';
        $request->AWBNumber = $model->track;
        $request->LevelOfDetails = 'ALL_CHECK_POINTS';
        $request->PiecesEnabled = 'S';
        $client = new WebserviceClient();
        $model->activities()->delete();
        try {
            $xml = $client->call($request);
            $xml = simplexml_load_string(str_replace('req:', '', $xml));
        }catch(\Throwable $exception){
            if (env('APP_ENV') != 'production') {
                $model->activities()->create([
                    'title' => $exception->getMessage(),
                ]);
            }
            return false;
        }
        if((string)$xml->AWBInfo->Status->ActionStatus !== 'success'){
            $model->activities()->create([
                'title' => $xml->AWBInfo->Status->Condition->ConditionData ?? 'error',
            ]);
            return false;
        }
        foreach($xml->AWBInfo->ShipmentInfo->ShipmentEvent as $event){
            $activity = $model->activities()->create([
                'title' => $event->ServiceEvent->Description ?? null,
                'city' => $event->ServiceArea->Description ?? null,
                'date' => isset($event->Date) ? Carbon::createFromFormat('Y-m-dH:i:s', $event->Date . $event->Time) : null,
            ]);
            if((string)$event->ServiceEvent->EventCode === "OK"){
                $model->delivered_at = $activity->date;
                $model->save();
            }
        }
        /**Не работает
        $result = new TrackingResponse();
        $result->initFromXML($xml);
        */
        return true;
    }
}
