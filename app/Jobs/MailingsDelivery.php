<?php

namespace App\Jobs;

use App\HistoryMailing;
use App\MailingSending;
use App\Mailsetting;
use App\Notifications\TemplatizedMail;
use App\User;
use Carbon\Carbon;
use CodersStudio\ImapMailClient\Http\Requests\MailClientSendRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class MailingsDelivery implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $start = Carbon::now();

        $count = 0;
        $toDelivery = HistoryMailing::whereHas('mailingStatus', function ($query) {
            $query->where('name', '=', 'new');
        })->whereHas('mailingSending', function ($query) {
            $query->where('scheduled_at','<=',Carbon::now());
        })->with(['emailMailing','mailing'])->get();

        $toDelivery = $toDelivery->groupBy('mailing_sending_id');

        foreach ($toDelivery as $sending) {
            MailingSending::where('id','=',$sending[0]->mailing_sending_id)->update(['running' => 1]);
            foreach ($sending as $item) {
                $ms = MailingSending::find($item->mailing_sending_id);
                if ($ms->command == 'stop') {
                    MailingSending::where('id','=',$item->mailing_sending_id)->update(['running' => 0, 'command' => '']);
                    return;
                }
                if ($ms->max_per_min > 0 && $count == $ms->max_per_min && $start->diffInSeconds(Carbon::now()) < 60) {
                    sleep(60 - $start->diffInSeconds(Carbon::now()));
                    $count = 0;
                    $start = Carbon::now();
                }
                $template = $item->mailing->mailTemplate;
                $recipient = $item->emailMailing;
                $user = User::where('email', $recipient->email)->first();
                if(!$user) {
                    if ($template) {
                        $content = $template->getTemplatized(['recipient' => $recipient]);
//                    try {
//
//                        $recipient->notify(new TemplatizedMail($template,$content));
//
//                        $item->setStatus('sent');
//                        $count++;
//                    } catch (\Exception $e) {
//                        $item->setStatus('failed');
//                    }

                        ////////////////////

                        $sendRequest = new MailClientSendRequest([
                            'recipients' => [
                                $recipient->email
                            ],
                            'set_id' => $item->set_id,
                            'user_id' => Mailsetting::find($item->set_id)->user_id,
                            'message' => $content,
                            'subject' => 'mailing',
                        ]);
                        try {
                            app('App\Http\Controllers\Admin\MailClientControllerCustomMeiling')->send($sendRequest);
                        } catch (\Exception $e) {
                            $item->setStatus('failed');
                        }
                        /////////////////////
                    }
                }
            }
            MailingSending::where('id','=',$sending[0]->mailing_sending_id)->update(['running' => 0]);
        }

    }
}
