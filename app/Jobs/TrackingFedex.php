<?php

namespace App\Jobs;

use App\Carrier;
use Carbon\Carbon;
use FedEx\TrackService\ComplexType\TrackRequest;
use FedEx\TrackService\ComplexType\TrackSelectionDetail;
use FedEx\TrackService\Request;
use FedEx\TrackService\SimpleType\TrackIdentifierType;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class TrackingFedex implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $carrier = Carrier::whereName('Fedex')->firstOrFail();


        $tasks = $carrier->tasks()->undelivered()->withTrack()->get()->keyBy('track');
        if($tasks->count()){
            $this->track($tasks);
        }
        $packages = $carrier->packages()->undelivered()->withTrack()->get()->keyBy('track');
        if($packages->count()){
            $this->track($packages);
        }
        $salesTasks = $carrier->salesTasks()->undelivered()->withTrack()->get()->keyBy('track');
        if($salesTasks->count()){
            $this->track($salesTasks);
        }

    }

    private function track($tasks)
    {
        $trackRequest = new TrackRequest();
        $trackRequest->WebAuthenticationDetail->UserCredential->Key = config('services.fedex.' . (app()->environment() === 'production' ? 'production' : 'local') . '.auth.key');
        $trackRequest->WebAuthenticationDetail->UserCredential->Password = config('services.fedex.' . (app()->environment() === 'production' ? 'production' : 'local') . '.auth.password');
        $trackRequest->ClientDetail->AccountNumber = config('services.fedex.' . (app()->environment() === 'production' ? 'production' : 'local') . '.auth.account_number');
        $trackRequest->ClientDetail->MeterNumber = config('services.fedex.' . (app()->environment() === 'production' ? 'production' : 'local') . '.auth.meter_number');
        $trackRequest->Version->ServiceId = 'trck';
        $trackRequest->Version->Major = 16;
        $trackRequest->Version->Intermediate = 0;
        $trackRequest->Version->Minor = 0;
        $details = [];
        foreach ($tasks as $key => $task){
            $trackSelectionDetail = new TrackSelectionDetail();
            $trackSelectionDetail->PackageIdentifier->Value = $task->track;
            $trackSelectionDetail->PackageIdentifier->Type = TrackIdentifierType::_TRACKING_NUMBER_OR_DOORTAG;
            $details[] = $trackSelectionDetail;
        }
        $trackRequest->SelectionDetails = $details;
        $request = new Request();
        $trackReply = $request->getTrackReply($trackRequest)->toArray();
        if($trackReply['HighestSeverity'] !== 'SUCCESS'){
            $task->activities()->create([
                'title' => $trackReply['Notifications'][0]['Message'] ?? 'error',
            ]);
            return false;
        }elseif($trackReply['CompletedTrackDetails'][0]['HighestSeverity'] !== 'SUCCESS'){
            $task->activities()->create([
                'title' => $trackReply['CompletedTrackDetails'][0]['Notifications'][0]['Message'] ?? 'error',
            ]);
            return false;
        }
        foreach($trackReply['CompletedTrackDetails'] as $completedTrackDetail){
            $trackDetail = $completedTrackDetail['TrackDetails'][0];
            $currentTask = $tasks[$trackDetail['TrackingNumber']];
            $currentTask->activities()->delete();
            if($trackDetail['Notification']['Severity'] !== 'SUCCESS'){
                if (env('APP_ENV') != 'production') {
                    $currentTask->activities()->create([
                        'title' => $trackDetail['Notification']['Message']
                    ]);
                }
                continue;
            }
            foreach($trackDetail['Events'] ?? [] as $event){
                $activity = $currentTask->activities()->create([
                    'title' => $event['EventDescription'] ?? null,
                    'description' => $event['StatusExceptionDescription'] ?? null,
                    'city' => $event['Address']['City'] ?? null,
                    'date' => isset($event['Timestamp']) ? (new Carbon($event['Timestamp']))->toDateTimeString() : null,
                ]);
                if($event['EventType'] === "DL"){
                    $task->delivered_at = $activity->date;
                    $task->save();
                }
            }
        }
        return true;
    }
}
