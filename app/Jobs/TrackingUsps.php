<?php

namespace App\Jobs;

use App\Carrier;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Johnpaulmedina\Usps\Usps;

class TrackingUsps implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $carrier = Carrier::whereName('Usps')->firstOrFail();
        $tasks = $carrier->tasks()->undelivered()->withTrack()->get()->keyBy('track');
        foreach($tasks as $task){
            $this->track($task);
        }
        $salesTasks = $carrier->salesTasks()->undelivered()->withTrack()->get()->keyBy('track');
        foreach($salesTasks as $salesTask){
            $this->track($salesTask);
        }
        $packages = $carrier->packages()->undelivered()->withTrack()->get()->keyBy('track');
        foreach($packages as $package){
            $this->track($package);
        }
    }

    /**
     * @param $task
     * @return bool
     */
    private function track($model)
    {
        $model->activities()->delete();
        try {
            $data = (new Usps(config('services.usps')))->trackConfirm(
                $model->track
            );
        }catch (\Throwable $exception){
            if (env('APP_ENV') == 'production') {
                $model->activities()->create([
                    'title' => substr($exception->getMessage(), 0, 255),
                ]);
            }
            return false;
        }
        $trackInfo = $data['TrackResponse']['TrackInfo'];
        if(preg_match('/Delivered/', $trackInfo['TrackSummary']['Event'])){
            $model->delivered_at = Carbon::parse($trackInfo['TrackSummary']['EventDate'] . $trackInfo['TrackSummary']['EventTime']);
            $model->save();
        }
        foreach($trackInfo['TrackDetail'] ?? [] as $event){
            $model->activities()->create([
                'title' => $event['Event'] ?? null,
                'city' => $event['EventCity'] ?? null,
                'date' => Carbon::parse($event['EventDate'] . $event['EventTime']),
            ]);
        }
        return true;
    }
}
