<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContractReminder as MailContractReminder;
use App\User;
use Illuminate\Support\Facades\Notification;
use CodersStudio\Notifications\Notifications\System;

class ContractReminder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach (User::role('Employee Staff')->whereHas('employee.userStatus', function ($query) {
            $query->where('name', 'wait_docs');
        })->whereRaw('users.updated_at <= DATE_ADD(users.updated_at, INTERVAL 3 DAY)')->get() as $key => $value) {
            $value->notifyUsers(trans('admin.user.employee_did_not_upload_documents', [
                'name' => $value->name,
            ]));
            Mail::to($value->email)->send(new MailContractReminder());
        }
    }
}
