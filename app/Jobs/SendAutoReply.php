<?php

namespace App\Jobs;

use App\MailTemplate;
use App\User;
use CodersStudio\ImapMailClient\Http\Requests\MailClientSendRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class SendAutoReply implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var string
     */
    protected $recipient = '';

    /**
     * @var int
     */
    protected $template = 0;

    /**
     * @var int
     */
    protected $mailsetting = 0;

    /**
     * @var int
     */
    protected $rcptUserId = 0;

    /**
     * Create a new job instance.
     *
     * @param string $recipient
     * @param int $template
     * @param int $mailsetting
     * @param int $rcptUserId
     */
    public function __construct(string $recipient, int $template, int $mailsetting, int $rcptUserId)
    {
        $this->recipient = $recipient;
        $this->template = $template;
        $this->mailsetting = $mailsetting;
        $this->rcptUserId = $rcptUserId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tmpl = MailTemplate::findOrFail($this->template);
        $user = User::findOrFail($this->rcptUserId);
        $project = $user->employee ? $user->employee->project : null;
        $tmplArray = [
            'recipient' => $user
        ];
        if ($project) {
            $tmplArray['project'] = $project;
        }
        $sendRequest = new MailClientSendRequest([
            'recipients' => [
                $this->recipient
            ],
            'set_id' => $this->mailsetting,
            'message' => $tmpl->getTemplatized($tmplArray),
            'subject' => $tmpl->subject,
        ]);
        try {
            app('CodersStudio\ImapMailClient\Http\Controllers\MailClientController')->send($sendRequest);
        } catch(\Exception $e) {
            Log::error('Unable to send autoreply to '.$this->recipient.' from setting '.$this->mailsetting.' error: '.$e->getMessage());
        }

    }
}
