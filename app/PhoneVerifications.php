<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneVerifications extends Model
{
    protected $fillable = [
    	'phone',
    	'code',
    	'deadline_at',
    ];
}
