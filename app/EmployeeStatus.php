<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeStatus extends Model
{
    protected $fillable = [
        'name',
    ];

    public $timestamps = false;

    public function scopeOfName($query, $name)
    {
        return $query->where('name', $name);
    }
}
