<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use \Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{

    protected $appends = [
        'resource_url',
    ];

    /**
     * return array of permission ids
     *
     * @return Array
     */
    public function getPermissionIdsAttribute(): Collection
    {
        return $this->permissions->pluck('id');
    }

    public function getCreatedAtAttribute($value)
    {
        return (new Carbon($value))->format('m.d.Y');
    }

    public function getResourceUrlAttribute() {
        return url('/admin/roles/'.$this->getKey());
    }

    public function syncPermissions(...$permissions)
    {
        //TODO somehow disallow Admin to disallow to himself to edit roles
        $this->permissions()->detach();
        return $this->givePermissionTo($permissions);
    }
}
