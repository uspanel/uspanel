<?php namespace App;

use App\Libs\Media\ConvertToWebm;
use Brackets\Media\HasMedia\HasMediaCollections;
use Brackets\Media\HasMedia\HasMediaCollectionsTrait;
use Brackets\Media\HasMedia\HasMediaThumbsTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Conversion\ConversionCollection;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\Media;

class HelpVideo extends Model implements HasMediaCollections, HasMediaConversions
{
    use HasMediaCollectionsTrait;
    use HasMediaThumbsTrait;


    protected $fillable = [
        "title",
        "description",
        'processing'

    ];

    protected $hidden = [

    ];

    protected $dates = [
        "created_at",
        "updated_at",

    ];


    protected $with = ['roles'];
    protected $appends = ['resource_url', 'video', 'thumb', 'webm_url', 'mp4_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/help-videos/' . $this->getKey());
    }

    public function getVideoAttribute()
    {
        return $this->getFirstMediaUrl('video');
    }

    public function getThumbAttribute()
    {
        return $this->getFirstMedia('video')->getUrl('thumb_300');
    }

    public function getWebmUrlAttribute()
    {
        if ($this->getFirstMedia('video')->extension == 'webm') {
            return $this->video;
        } else {
            return '/media/'.$this->getFirstMedia('video')->id.'/conversions/conv.webm';
        }
    }

    public function getMp4UrlAttribute()
    {
        if ($this->getFirstMedia('video')->extension == 'mp4') {
            return $this->video;
        } else {
            return '/media/' . $this->getFirstMedia('video')->id . '/conversions/conv.mp4';
        }
    }

    /***************************************************************/

    public function registerMediaCollections()
    {
        $this->addMediaCollection('video')
            ->accepts('video/*')
            ->maxFilesize(1024 * 1024 * 500);
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb_300')
            ->width(300)
            ->height(300)
            ->fit('crop', 300, 300)
            ->optimize()
            ->performOnCollections('video')
            ->nonQueued();
    }

    public function getThumbs300ForCollection(string $mediaCollectionName)
    {
        $mediaCollection = $this->getMediaCollection($mediaCollectionName);

        return $this->getMedia($mediaCollectionName)->filter(function ($medium) use ($mediaCollectionName, $mediaCollection) {

            return $conversions = ConversionCollection::createForMedia($medium)->filter(function ($conversion) use ($mediaCollectionName) {
                    return $conversion->shouldBePerformedOn($mediaCollectionName);
                })->filter(function ($conversion) {
                    return $conversion->getName() == 'thumb_300';
                })->count() > 0;
        })->map(function ($medium) use ($mediaCollection) {
            return [
                'id' => $medium->id,
                'url' => $medium->getUrl(),
                'thumb_url' => $medium->getUrl('thumb_300'),
                'type' => 'image/jpeg',
                'mediaCollection' => $mediaCollection->getName(),
                'name' => $medium->hasCustomProperty('name') ? $medium->getCustomProperty('name') : $medium->file_name,
                'size' => $medium->size
            ];
        });
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }


}
