<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class MeilsettingAdditionalName extends Model
{

    protected $table = 'additional_mailsetting_names';

    protected $fillable = [
        'name',
        'name_domain',
        'set_id',
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];
}
