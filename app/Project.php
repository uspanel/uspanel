<?php namespace App;

use App\Employee;
use Illuminate\Database\Eloquent\Model;
use App\Form;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Project extends Model
{
    use SoftDeletes;


    protected $fillable = [
        "name",
        "description",
        "project_id",
        "company_id",
        "salary",
        "mail_domain"

    ];

    protected $hidden = [

    ];

    protected $dates = [
        "created_at",
        "updated_at",

    ];



    protected $appends = [
        'resource_url',
    ];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute() {
        return url('/admin/projects/'.$this->getKey());
    }

    public function company() {
        return $this->hasOne('App\Company', 'id', 'company_id');
    }

    public function forms() {
        return $this->belongsToMany(Form::class);
    }

    public function users() {
        return $this->belongsToMany(User::class)->withPivot('responsible_percent')->withTrashed();
    }

    public function managers() {
        return $this->users()->role(['HR Manager Staff','HR Manager Invest']);
    }

    public function supports() {
        return $this->users()->role(['Support Staff','Support Invest']);
    }

    public function getFormIdsAttribute()
    {
        return $this->forms->pluck('pivot.form_id');
    }

    public function getSupportIdsAttribute()
    {
        return $this->supports->pluck('pivot.user_id');
    }

    public function getManagerIdsAttribute()
    {
        return $this->managers->pluck('pivot.user_id');
    }

    public function getHrPercentAttribute()
    {
        return $this->managers->pluck('pivot.responsible_percent','pivot.user_id');
    }

    public function getSupportPercentAttribute()
    {
        return $this->supports->pluck('pivot.responsible_percent','pivot.user_id');
    }

    public function employees() {
        $rel = $this->hasMany(Employee::class)->whereHas('user')->withTrashed();
        if (Auth::check() && Auth::user()->hasRole(['Staffer'])) {
            $rel = $rel->whereHas('staffers', function($sq) {
                $sq->where('id',Auth::user()->id);
            });
        }
        return $rel;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function dkimKey()
    {
        return $this->hasOne(DkimKey::class);
    }

}
