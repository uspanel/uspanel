<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class VestaMailDomain extends Model
{
    protected $fillable = [
        "project_id",
        "domain",
    ];

    protected $dates = [
        "created_at",
        "updated_at",

    ];
}
