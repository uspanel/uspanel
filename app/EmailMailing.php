<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\User;
use App\UserStatus;

class EmailMailing extends Model
{
    use Notifiable;


    protected $fillable = [
        "name",
        "email",
        "email_group_id",

    ];

    protected $hidden = [

    ];

    protected $dates = [
        "created_at",
        "updated_at",

    ];


    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute()
    {
        return url('/admin/email-mailings/' . $this->getKey());
    }


    /**
     * Group of email
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function emailGroup()
    {
        return $this->belongsTo(EmailGroup::class);
    }

    public function user() {
        return $this->belongsTo(User::class, 'email', 'email')->role('Employee Staff')->withTrashed();
    }

    public function approvedUser() {
        return $this->user()->whereHas('employee', function ($query) {
            $query->where('user_status_id', UserStatus::ofName('approved')->first()->id);
        });
    }

    public function rejectedUser() {
        return $this->user()->whereHas('employee', function ($query) {
            $query->where('user_status_id', UserStatus::ofName('rejected')->first()->id);
        });
    }

    public function historyMailings()
    {
        return $this->hasMany(HistoryMailing::class);
    }
}
