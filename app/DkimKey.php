<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class DkimKey extends Model
{
    
    
    protected $fillable = [
        "project_id",
        "domain",
        "dkim_record",
    
    ];
    
    protected $hidden = [
    
    ];
    
    protected $dates = [
        "created_at",
        "updated_at",
    
    ];
    
    
    
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute() {
        return url('/admin/dkim-keys/'.$this->getKey());
    }

    
}
