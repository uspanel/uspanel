<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvestTaskTemplate extends Model
{
    use SoftDeletes;


    protected $fillable = [
        "name",
        "theme",
        "text",
        "description",
        "manager_id",
        "mail_template_id",

    ];

    protected $hidden = [

    ];

    protected $dates = [
        "deleted_at",
        "created_at",
        "updated_at",

    ];



    protected $appends = ['resource_url'];

    /**
     * Manager relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Investor relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investors()
    {
        return $this->belongsToMany('App\User', 'invest_task_template_investor', 'invest_task_template_id', 'investor_id');
    }

    /**
     * Contragent relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contragents()
    {
        return $this->belongsToMany('App\Contragent');
    }

    /**
     * Investor List relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investorLists()
    {
        return $this->belongsToMany('App\InvestorList');
    }


    /**
     * Contragent List relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contragentLists()
    {
        return $this->belongsToMany('App\ContragentList');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mailTemplate()
    {
        return $this->belongsTo(MailTemplate::class);
    }
    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute() {
        return url('/admin/invest-task-templates/'.$this->getKey());
    }


}
