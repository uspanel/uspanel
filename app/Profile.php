<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $guarded = ['id'];

    public function state() {
        return $this->hasOne(State::class, 'id', 'state_id');
    }
}
