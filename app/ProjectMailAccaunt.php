<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class ProjectMailAccaunt extends Model
{

    protected $table = 'project_mail_accaunts';

    protected $fillable = [
        "project_id",
        "project_domain",
        "user_accaunt"
    ];

    protected $dates = [
        "created_at",
        "updated_at",

    ];

}
