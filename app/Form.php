<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CodersStudio\FormCreator\Form as CodersStudioForm;
use App\Project;

class Form extends CodersStudioForm
{

    protected $appends = [
    	'resource_url',
    	'project_ids'
    ];

    public function projects() {
    	return $this->belongsToMany(Project::class);
    }

    public function getResourceUrlAttribute() {
        return url('/admin/forms/'.$this->getKey());
    }

    public function getProjectIdsAttribute()
    {
        return $this->projects->pluck('pivot.project_id');
    }

    /**
     * Scope a query to only include forms of a given name.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  mixed  $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfName($query, $type)
    {
        return $query->where('name', $type);
    }
}
