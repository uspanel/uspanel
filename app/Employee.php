<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Project;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Employee extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $appends = ['sign_link', 'user_name'];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function userStatus()
    {
        return $this->belongsTo(UserStatus::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function contragents()
    {
        return $this->belongsToMany(Contragent::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function investors()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function investorLists()
    {
        return $this->belongsToMany(InvestorList::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function contragentLists()
    {
        return $this->belongsToMany(ContragentList::class);
    }

    /**
     * Set user status by name
     *
     * @param string $statusName
     */
    public function setStatus($statusName)
    {
        $status = UserStatus::where('name', '=', $statusName)->firstOrFail();
        $this->update(['user_status_id' => $status->id]);
    }

    public function callSupport()
    {
        return $this->belongsTo(User::class, 'cs_id', 'id');
    }

    public function hr()
    {
        return $this->belongsTo(User::class, 'hr_id', 'id');
    }

    public function support()
    {
        return $this->belongsTo(User::class, 'support_id', 'id');
    }

    public function getSignLinkAttribute()
    {
        return 'https://www.irs.gov/pub/irs-pdf/fw4.pdf';
    }

    public function getUserNameAttribute()
    {
        return $this->user ? ($this->user->makeHidden('employee') ? $this->user->makeHidden('employee')->name : '-') : '';
    }

    public function salesTasks()
    {
        return $this->hasMany(SalesTask::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function investTasks()
    {
        return $this->hasMany(InvestTask::class);
    }

    public function morphTasks()
    {
        return $this->morphedByMany(Task::class, 'employeeable')->withPivot('qty');
    }

    public function packages()
    {
        return $this->hasMany(Package::class);
    }

    public function morphPackages()
    {
        return $this->morphedByMany(Package::class, 'employeeable')->withPivot('qty');
    }

    public function warehouses()
    {
        return $this->hasMany(Warehouse::class);
    }

    public function staffers()
    {
        return $this->belongsToMany(\App\User::class, 'employee_staffer', 'employee_id', 'staffer_id');
    }

    /**
     * [getEmployeesForSelect description]
     * @return [type] [description]
     */
    static function getEmployeesForSelect()
    {
        return self::
        leftJoin('users', 'users.id', '=', 'employees.user_id')
            ->select(['employees.id'])
            ->selectRaw('CONCAT(users.first_name, \' \', users.last_name) as name')
            ->get()
            ->makeHidden('sign_link');
    }

    public function items()
    {
        return $this->belongsToMany(Item::class,'employeeables')->withPivot(['qty']);
    }

    /**
     * Get available quantity of item for employee
     *
     * @param $id
     * @return mixed
     */
    public function getItemQty($id)
    {
        return $this->items()->where('item_id','=',$id)->sum('qty');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function voicentStats()
    {
        return $this->hasOne(EmployeeVoicentStats::class);
    }

    /**
     * @return mixed
     */
    public function getUniqueContragents()
    {
        $this->load([
            'contragents',
            'contragentLists.contragents',
            'investTasks.contragents',
            'investTasks.contragentLists.contragents'
        ])
            ->get();
        $contragents = null;
        if($this->contragents) {
            $contragents = $this->contragents;
        }
        foreach ($this->contragentLists as $list) {
            $contragents = $contragents->merge($list->contragents);
        }
        foreach ($this->investTasks as $investTask) {
            if($investTask->contragents) {
                $contragents = $contragents->merge($investTask->contragents);
            }
            foreach ($investTask->contragentLists as $list) {
                if($list->contragents) {
                    $contragents = $contragents->merge($list->contragents);
                }
            }
        }

        return $contragents->unique('id')->pluck('id');
    }

    /**
     * @return mixed
     */
    public function getUniqueInvestors()
    {
        $this->load([
            'investors',
            'investorLists.users',
            'investTasks.investors',
            'investTasks.investorLists.users'
        ])
            ->get();
        $investors = null;
        if($this->investors) {
            $investors = $this->investors;
        }
        foreach ($this->investorLists as $list) {
            $investors = $investors->merge($list->users);
        }
        foreach ($this->investTasks as $investTask) {
            if($investTask->investors) {
                $investors = $investors->merge($investTask->investors);
            }
            foreach ($investTask->investorLists as $list) {
                if($list->users) {
                    $investors = $investors->merge($list->users);
                }
            }
        }

        return $investors->unique('id')->pluck('id');
    }

}

