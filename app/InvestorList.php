<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class InvestorList
 * @package App
 */
class InvestorList extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        "name",

    ];

    /**
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * @var array
     */
    protected $dates = [
        "created_at",
        "updated_at",

    ];

    /**
     * @var array
     */
    protected $appends = ['resource_url'];

    /* ************************ ACCESSOR ************************* */


    /**
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getResourceUrlAttribute() {
        return url('/admin/investor-lists/'.$this->getKey());
    }


    /**
     * Get the Users record associated with the Investor List.
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTrashed();
    }


    /**
     * Invest Task relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function investTask()
    {
        return $this->belongsToMany('App\InvestTask');
    }


}
