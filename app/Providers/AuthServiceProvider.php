<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('admin.admin-user.index', function ($user) {
            return $user->hasAnyRole(['Administrator']);
        });

        //Roles
        Gate::define('crud.role.view', function ($user) {
            return $user->hasPermissionTo('crud.role.view');
        });
        Gate::define('crud.role.delete', function ($user) {
            return $user->hasPermissionTo('crud.role.delete');
        });

        //Warehouse
        Gate::define('crud.warehouse.view', function ($user) {
            return $user->hasPermissionTo('crud.warehouse.view');
        });
        Gate::define('crud.warehouse.delete', function ($user) {
            return $user->hasPermissionTo('crud.warehouse.delete');
        });

        //Task
        Gate::define('crud.task.view', function ($user) {
            return $user->hasPermissionTo('crud.task.view');
        });
        Gate::define('crud.task.delete', function ($user) {
            return $user->hasPermissionTo('crud.task.delete');
        });

        // Invest Task
        Gate::define('crud.invest-task.view', function ($user) {
            return $user->hasPermissionTo('crud.task.view');
        });
        Gate::define('crud.invest-task.delete', function ($user) {
            return $user->hasPermissionTo('crud.task.delete');
        });

        //Packages
        Gate::define('crud.package.view', function ($user) {
            return $user->hasPermissionTo('crud.package.view');
        });
        Gate::define('crud.package.delete', function ($user) {
            return $user->hasPermissionTo('crud.package.delete');
        });

        // mailsetting

        Gate::define('admin.mailsetting.index', function ($user) {
            return $user->hasAnyRole(['Administrator']);
        });
        Gate::define('admin.mailsetting.edit', function ($user) {
            return $user->hasAnyRole(['Administrator']);
        });
        Gate::define('admin.mailsetting.create', function ($user) {
            return $user->hasAnyRole(['Administrator']);
        });
        Gate::define('admin.mailsetting.delete', function ($user) {
            return $user->hasAnyRole(['Administrator']);
        });
        Gate::define('user.statistics', function ($user) {
            return $user->hasRole('Employee Staff');
        });
        Gate::define('user.salary', function ($user, \App\User $currentUser) {
            return $user->hasRole('Employee Staff') ? $user->id == $currentUser->id : $user->hasAnyRole(['Administrator', 'Support Staff', 'Staffer', 'HR Manager Staff']);
        });

        // mail

        Gate::define('admin.mail.index', function ($user) {
            return $user->hasAnyRole(['Administrator']);
        });

        Gate::define('sidebar', function ($user) {
            if ($user->hasRole('Employee Staff')) {
                return $user->approved;
            }
            return auth()->check();
        });

        Gate::define('task.create', function ($user) {
            return $user->hasAnyRole(['Administrator', 'Support Staff', 'Staffer', 'HR Manager Staff']);
        });

        Gate::define('admin.statistics', function ($user) {
            return $user->hasAnyRole(['Administrator']);
        });

        Gate::define('task.like', function ($user, \App\Task $task) {
            return $user->hasAnyRole(['Administrator', 'HR Manager Staff']) || ($user->hasAnyRole(['Employee Staff']) && $task->employee_id == $user->id);
        });

    }
}
