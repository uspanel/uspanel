<?php

namespace App\Providers;

use App\Activity;
use App\ActivityLog;
use App\InvestTask;
use App\Observers\ActivityLogObserver;
use App\Observers\ActivityObserver;
use App\Observers\InvestTaskObserver;
use App\Observers\MediaObserver;
use App\Observers\SalesTasksObserver;
use App\SalesTask;
use Illuminate\Support\ServiceProvider;

use App\Observers\ProjectObserver;
use App\Observers\MailmessageObserver;
use App\Observers\WarehouseObserver;
use App\Observers\TaskObserver;
use App\Observers\PackageObserver;
use App\Observers\HelpVideoObserver;
use App\Observers\ChatroomObserver;
use App\Observers\BusinessProcessObserver;
use App\Observers\MailingObserver;
use App\Observers\EmailGroupObserver;
use App\Observers\EmailMailingObserver;
use App\Observers\ItemObserver;
use App\Observers\UserObserver;
use App\Observers\RoleObserver;
use App\Observers\MailTemplateObserver;
use App\Observers\MailsettingObserver;
use App\Observers\FormObserver;
use App\Observers\EmployeeObserver;
use App\Observers\SettingObserver;
use App\Project;
use CodersStudio\ImapMailClient\Mailmessage;
use App\Warehouse;
use App\Task;
use App\Package;
use App\HelpVideo;
use CodersStudio\Chat\App\Chatroom;
use App\BusinessProcess;
use App\Mailing;
use App\EmailGroup;
use App\EmailMailing;
use App\Item;
use App\User;
use App\Role;
use App\MailTemplate;
use App\Employee;
use App\Mailsetting;
use App\Form;
use App\Setting;
use Spatie\MediaLibrary\Media;
//use Spatie\Activitylog\Models\Activity as ActivityLog;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $mainPath = database_path('migrations');
        $directories = glob($mainPath . '/*' , GLOB_ONLYDIR);
        $paths = array_merge([$mainPath], $directories);
        $this->loadMigrationsFrom($paths);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Project::observe(ProjectObserver::class);
        Mailmessage::observe(MailmessageObserver::class);
        Warehouse::observe(WarehouseObserver::class);
        Task::observe(TaskObserver::class);
        InvestTask::observe(InvestTaskObserver::class);
        SalesTask::observe(SalesTasksObserver::class);
        Package::observe(PackageObserver::class);
        HelpVideo::observe(HelpVideoObserver::class);
        Chatroom::observe(ChatroomObserver::class);
        BusinessProcess::observe(BusinessProcessObserver::class);
        Mailing::observe(MailingObserver::class);
        EmailGroup::observe(EmailGroupObserver::class);
        EmailMailing::observe(EmailMailingObserver::class);
        Item::observe(ItemObserver::class);
        User::observe(UserObserver::class);
        Role::observe(RoleObserver::class);
        MailTemplate::observe(MailTemplateObserver::class);
        Mailsetting::observe(MailsettingObserver::class);
        Form::observe(FormObserver::class);
        Activity::observe(ActivityObserver::class);
        Employee::observe(EmployeeObserver::class);
        Media::observe(MediaObserver::class);
        Setting::observe(SettingObserver::class);
        ActivityLog::observe(ActivityLogObserver::class);
    }
}
