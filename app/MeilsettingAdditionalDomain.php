<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class MeilsettingAdditionalDomain extends Model
{

    protected $table = 'additional_mailsetting_domains';

    protected $fillable = [
        'domain',
        'set_id',
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];
}
