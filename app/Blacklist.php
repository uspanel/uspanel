<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blacklist extends Model
{
    protected $guarded = ['id'];

    public function scopeOfEmail($query, $email)
    {
        return $query->where('email', $email);
    }

    public function scopeOfPhone($query, $phone)
    {
        return $query->where('phone', $phone);
    }
}
