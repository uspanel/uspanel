<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesStatus extends Model
{
    protected $guarded = ['id'];
}
