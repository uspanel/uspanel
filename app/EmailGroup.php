<?php namespace App;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};
use App\Traits\Multitenantable;
use Brackets\Media\HasMedia\{HasMediaCollections, HasMediaCollectionsTrait, HasMediaThumbsTrait};

class EmailGroup extends Model implements HasMediaCollections
{ 
    use Multitenantable, 
        HasMediaThumbsTrait, 
        SoftDeletes;
    use  HasMediaCollectionsTrait{
        shouldAutoProcessMedia as protected traitShouldAutoProcessMedia;
    }
    
    protected $fillable = [
        "name",
        "created_by",
    ];
    
    protected $hidden = [
    
    ];
    
    protected $dates = [
        "created_at",
        "updated_at",
    
    ];
    
    protected $appends = ['resource_url'];

     /**
     * @return array|void
     * @throws \Brackets\Media\Exceptions\Collections\MediaCollectionAlreadyDefined
     */
    public function registerMediaCollections()
    {
        $this->addMediaCollection('list')->accepts("text/*");
    }

    /**
     * @param Media|null $media
     */
    public function registerMediaConversions(Media $media = null)
    {
        $this->autoRegisterThumb200();
    }

    protected function shouldAutoProcessMedia() {
		return false;
	}


    /* ************************ ACCESSOR ************************* */

    public function getResourceUrlAttribute() {
        return url('/admin/email-groups/'.$this->getKey());
    }

    public function mailings()
    {
        return $this->belongsToMany(Mailing::class);
    }

    public function emailMailings()
    {
        return $this->hasMany(EmailMailing::class);
    }
    
}
