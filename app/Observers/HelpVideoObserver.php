<?php

namespace App\Observers;

use App\HelpVideo;
use Activity;


class HelpVideoObserver
{
    /**
     * Handle the help video "created" event.
     *
     * @param  \App\HelpVideo  $helpVideo
     * @return void
     */
    public function created(HelpVideo $helpVideo)
    {
        activity()->log(trans('history.actions.create_helpVideo',['id' => $helpVideo->id])); 
    }

    /**
     * Handle the help video "updated" event.
     *
     * @param  \App\HelpVideo  $helpVideo
     * @return void
     */
    public function updated(HelpVideo $helpVideo)
    {
        activity()->log(trans('history.actions.updated_helpVideo',['id' => $helpVideo->id])); 
    }

    /**
     * Handle the help video "deleted" event.
     *
     * @param  \App\HelpVideo  $helpVideo
     * @return void
     */
    public function deleted(HelpVideo $helpVideo)
    {
        activity()->log(trans('history.actions.deleted_helpVideo',['id' => $helpVideo->id])); 
    }

    /**
     * Handle the help video "restored" event.
     *
     * @param  \App\HelpVideo  $helpVideo
     * @return void
     */
    public function restored(HelpVideo $helpVideo)
    {
        //
    }

    /**
     * Handle the help video "force deleted" event.
     *
     * @param  \App\HelpVideo  $helpVideo
     * @return void
     */
    public function forceDeleted(HelpVideo $helpVideo)
    {
        //
    }
}
