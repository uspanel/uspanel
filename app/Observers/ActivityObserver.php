<?php

namespace App\Observers;


use App\Activity;
use Carbon\Carbon;

class ActivityObserver
{
    /**
     * @param Activity $activity
     */
    public function created(Activity $activity)
    {
        $task = $activity->task;
        if ($task && $task->delivery_status_id == null) {
            $task->setConditionalStatus();
        }
        $package = $activity->package;
        if ($package && $package->employee->user->warehouses()->first()){
            foreach ($package->items as $item) {
                $package->employee->user->warehouses()->first()
                    ->morphPackages()
                    ->attach([$package->id => [
                        'qty' => $item->pivot->qty,
                        'item_id' => $item->id,
                        'created_at' => Carbon::now()
                    ]]);
            }
        }
    }
}
