<?php

namespace App\Observers;

use CodersStudio\Chat\App\Message;

class ChatMessageObserver
{
    /**
     * Handle the message "created" event.
     *
     * @param  CodersStudio\Chat\App\Message  $message
     * @return void
     */
    public function created(Message $message)
    {
    }

    /**
     * Handle the message "updated" event.
     *
     * @param  CodersStudio\Chat\App\Message  $message
     * @return void
     */
    public function updated(Message $message)
    {
        //
    }

    /**
     * Handle the message "deleted" event.
     *
     * @param  CodersStudio\Chat\App\Message  $message
     * @return void
     */
    public function deleted(Message $message)
    {
        //
    }

    /**
     * Handle the message "restored" event.
     *
     * @param  CodersStudio\Chat\App\Message  $message
     * @return void
     */
    public function restored(Message $message)
    {
        //
    }

    /**
     * Handle the message "force deleted" event.
     *
     * @param  CodersStudio\Chat\App\Message  $message
     * @return void
     */
    public function forceDeleted(Message $message)
    {
        //
    }
}
