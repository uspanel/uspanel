<?php

namespace App\Observers;

use App\Item;
use Activity;

class ItemObserver
{
    /**
     * Handle the email group "created" event.
     *
     * @param  \App\Item  $item
     * @return void
     */
    public function created(Item $item)
    {
        activity()->log(trans('history.actions.create_item',['id' => $item->id]));
    }

    public function creating(Item $item)
    {

    }

    /**
     * Handle the email group "updated" event.
     *
     * @param  \App\Item  $item
     * @return void
     */
    public function updated(Item $item)
    {
        activity()->log(trans('history.actions.updated_item',['id' => $item->id]));
    }

    public function updating(Item $item)
    {

    }
    /**
     * Handle the email group "deleted" event.
     *
     * @param  \App\Item  $item
     * @return void
     */
    public function deleted(Item $item)
    {
        activity()->log(trans('history.actions.deleted_item',['id' => $item->id]));
    }
}
