<?php

namespace App\Observers;

use App\Event;
use App\MailsettingForProject;
use App\MailTemplate;
use App\Package;
use App\Project;
use App\User;
use App\UserStatus;
use Auth;
use Activity;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

class PackageObserver
{
    /**
     * Handle the Package "creating" event.
     *
     * @param  \App\Package  $package
     * @return void
     */
    public function creating(Package $package)
    {
        if(Auth::check()) {
            $package->creator_id = Auth::id();
        }
    }

    /**
     * Handle the Package "created" event.
     *
     * @param  \App\Package  $package
     * @return void
     */
    public function created(Package $package)
    {
        // TODO notifications 13!
        $package->notifyUsers(trans('admin.package.package_created', [
            'name' => $package->name,
            'userName' => auth()->user()->name ?? null
        ]));
        activity()->log(trans('history.actions.create_package',['id' => $package->id]));


        Event::doActions('PackageCreated',[
            'message' => trans('notifications.pack_created',
                [
                    'id' => $package->id,
                ]),
            'link' => '/admin/packages?id='.$package->id,
            'package' => $package
        ]);

    }

    /**
     * Handle the Package "updated" event.
     *
     * @param  \App\Package  $package
     * @return void
     */
    public function updated(Package $package)
    {
        activity()->log(trans('history.actions.updated_package',['id' => $package->id]));
    }

    /**
     * Handle the Package "deleted" event.
     *
     * @param  \App\Package  $package
     * @return void
     */
    public function deleted(Package $package)
    {
        // TODO notifications 13!
        $package->notifyUsers(trans('admin.package.package_deleted', [
            'name' => $package->name,
            'userName' => auth()->user()->name ?? null
        ]));
        activity()->log(trans('history.actions.deleted_package',['id' => $package->id]));
    }

    public function updating(Package $package)
    {
        $oldPackage = Package::find($package->id);
        if ($oldPackage->status_id != $package->status_id || $oldPackage->delivered_at != $package->delivered_at) {
            if (!empty($package->delivered_at)) {
                $package->status_id = 2;
                if ($userStatus = UserStatus::where('name', '=', 'first_pack')->first()) {
                    $empl = $package->employee;
                    $empl->user_status_id = $userStatus->id;
                    $empl->save();
                }
                if (!$package->employee->user->warehouses()->count()) {
                    foreach ($package->items as $item) {
                        $package->employee->morphPackages()
                            ->attach([$package->id => [
                                'qty' => $item->pivot->qty,
                                'item_id' => $item->id,
                                'created_at' => Carbon::now()
                            ]]);
                    }
                }
            }

            Event::doActions('PackageStatusChanged',[
                'message' => trans('notifications.pack_status_changed',
                    [
                        'id' => $package->id,
                        'status' => $package->status->title
                    ]),
                'link' => '/admin/packages?id='.$package->id,
                'package' => $package
            ]);
        } else {
            Event::doActions('PackageEdited',[
                'message' => trans('notifications.pack_edited',
                    [
                        'id' => $package->id,
                    ]),
                'link' => '/admin/packages?id='.$package->id,
                'package' => $package
            ]);
        }
    }

}
