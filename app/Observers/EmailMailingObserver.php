<?php

namespace App\Observers;

use App\EmailMailing;
use Activity;

class EmailMailingObserver
{
    /**
     * Handle the email mailing "created" event.
     *
     * @param  \App\EmailMailing  $emailMailing
     * @return void
     */
    public function created(EmailMailing $emailMailing)
    {
        activity()->log(trans('history.actions.create_emailMailing',['id' => $emailMailing->id]));
    }

    /**
     * Handle the email mailing "updated" event.
     *
     * @param  \App\EmailMailing  $emailMailing
     * @return void
     */
    public function updated(EmailMailing $emailMailing)
    {
        activity()->log(trans('history.actions.updated_emailMailing',['id' => $emailMailing->id]));
    }

    /**
     * Handle the email mailing "deleted" event.
     *
     * @param  \App\EmailMailing  $emailMailing
     * @return void
     */
    public function deleted(EmailMailing $emailMailing)
    {
        activity()->log(trans('history.actions.deleted_emailMailing',['id' => $emailMailing->id]));
    }

    /**
     * Handle the email mailing "restored" event.
     *
     * @param  \App\EmailMailing  $emailMailing
     * @return void
     */
    public function restored(EmailMailing $emailMailing)
    {
        //
    }

    /**
     * Handle the email mailing "force deleted" event.
     *
     * @param  \App\EmailMailing  $emailMailing
     * @return void
     */
    public function forceDeleted(EmailMailing $emailMailing)
    {
        //
    }
}
