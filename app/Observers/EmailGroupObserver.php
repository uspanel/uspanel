<?php

namespace App\Observers;

use App\EmailGroup;
use Activity;

class EmailGroupObserver 
{
    /**
     * Handle the email group "created" event.
     *
     * @param  \App\EmailGroup  $emailGroup
     * @return void
     */
    public function created(EmailGroup $emailGroup)
    {
        activity()->log(trans('history.actions.create_emailGroup',['id' => $emailGroup->id]));
    }

    /**
     * Handle the email group "updated" event.
     *
     * @param  \App\EmailGroup  $emailGroup
     * @return void
     */
    public function updated(EmailGroup $emailGroup)
    {
        activity()->log(trans('history.actions.updated_emailGroup',['id' => $emailGroup->id]));
    }

    /**
     * Handle the email group "deleted" event.
     *
     * @param  \App\EmailGroup  $emailGroup
     * @return void
     */
    public function deleted(EmailGroup $emailGroup)
    {
        activity()->log(trans('history.actions.deleted_emailGroup',['id' => $emailGroup->id]));
    }

    /**
     * Handle the email group "restored" event.
     *
     * @param  \App\EmailGroup  $emailGroup
     * @return void
     */
    public function restored(EmailGroup $emailGroup)
    {
        //
    }

    /**
     * Handle the email group "force deleted" event.
     *
     * @param  \App\EmailGroup  $emailGroup
     * @return void
     */
    public function forceDeleted(EmailGroup $emailGroup)
    {
        //
    }
}
