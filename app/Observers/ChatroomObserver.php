<?php

namespace App\Observers;

use App\ActivityLog;
use CodersStudio\Chat\App\Chatroom;
use Activity;

class ChatroomObserver
{
    /**
     * Handle the chatroom "created" event.
     *
     * @param  \App\Chatroom  $chatroom
     * @return void
     */
    public function created(Chatroom $chatroom)
    {
        activity()->log(trans('history.actions.create_chatroom',['id' => $chatroom->id]));
    }

    /**
     * Handle the chatroom "updated" event.
     *
     * @param  \App\Chatroom  $chatroom
     * @return void
     */
    public function updated(Chatroom $chatroom)
    {
        activity()->log(trans('history.actions.updated_chatroom',['id' => $chatroom->id]));
    }

    /**
     * Handle the chatroom "deleted" event.
     *
     * @param  \App\Chatroom  $chatroom
     * @return void
     */
    public function deleted(Chatroom $chatroom)
    {
        if(ActivityLog::get()->last()->description !== trans('history.actions.deleted_chatroom',['id' => $chatroom->id])){
            activity()->log(trans('history.actions.deleted_chatroom',['id' => $chatroom->id]));
        }
    }

    /**
     * Handle the chatroom "restored" event.
     *
     * @param  \App\Chatroom  $chatroom
     * @return void
     */
    public function restored(Chatroom $chatroom)
    {
        //
    }

    /**
     * Handle the chatroom "force deleted" event.
     *
     * @param  \App\Chatroom  $chatroom
     * @return void
     */
    public function forceDeleted(Chatroom $chatroom)
    {
        //
    }
}
