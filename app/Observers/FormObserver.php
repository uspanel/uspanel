<?php

namespace App\Observers;

use App\Form;
use Activity;

class FormObserver
{
    /**
     * Handle the form "created" event.
     *
     * @param  \App\Form  $form
     * @return void
     */
    public function created(Form $form)
    {
        activity()->log(trans('history.actions.create_form',['id' => $form->id]));
    }

    /**
     * Handle the form "updated" event.
     *
     * @param  \App\Form  $form
     * @return void
     */
    public function updated(Form $form)
    {
        activity()->log(trans('history.actions.updated_form',['id' => $form->id]));
    }

    /**
     * Handle the form "deleted" event.
     *
     * @param  \App\Form  $form
     * @return void
     */
    public function deleted(Form $form)
    {
        activity()->log(trans('history.actions.deleted_form',['id' => $form->id]));
    }

    /**
     * Handle the form "restored" event.
     *
     * @param  \App\Form  $form
     * @return void
     */
    public function restored(Form $form)
    {
        //
    }

    /**
     * Handle the form "force deleted" event.
     *
     * @param  \App\Form  $form
     * @return void
     */
    public function forceDeleted(Form $form)
    {
        //
    }
}
