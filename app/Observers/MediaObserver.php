<?php

namespace App\Observers;

use App\Event;
use Spatie\MediaLibrary\Media;

class MediaObserver
{
    public function created(Media $media)
    {
        if ($media->collection_name == 'contract') {
            Event::doActions('UserUploadedDocument',[
                'message' => trans('notifications.user_uploaded_document',
                    [
                        'document' => 'Contract',
                        'model' => $media->model->name,
                    ]),
                'link' => '/admin/users?id='.$media->model->id,
                'media' => $media
            ]);
        } else if ($media->collection_name == 'label') {
            Event::doActions('UserUploadedDocument',[
                'message' => trans('notifications.user_uploaded_document',
                    [
                        'document' => 'Label',
                        'model' => $media->model->name,
                    ]),
                'link' => '/admin/tasks?id='.$media->model->id,
                'media' => $media
            ]);
        }else if ($media->collection_name == 'barcode') {
            Event::doActions('UserUploadedDocument',[
                'message' => trans('notifications.user_uploaded_document',
                    [
                        'document' => 'Barcode',
                        'model' => $media->model->name,
                    ]),
                'link' => '/admin/tasks?id='.$media->model->id,
                'media' => $media
            ]);
        }else if ($media->collection_name == 'receipt') {
            Event::doActions('UserUploadedDocument',[
                'message' => trans('notifications.user_uploaded_document',
                    [
                        'document' => 'Receipt',
                        'model' => $media->model->name,
                    ]),
                'link' => '/admin/tasks?id='.$media->model->id,
                'media' => $media
            ]);
        }


    }
}
