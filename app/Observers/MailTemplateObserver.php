<?php

namespace App\Observers;

use App\MailTemplate;
use Activity;
use App\ActivityLogDeprecated;

class MailTemplateObserver
{
    /**
     * Handle the mail template "creating" event.
     *
     * @param  \App\MailTemplate  $mailTemplate
     * @return void
     */
    public function creating(MailTemplate $mailTemplate)
    {
        $mailTemplate->name = str_replace(' ','_',strtoupper(trim($mailTemplate->title)));
    }

    /**
     * Handle the mail template "created" event.
     *
     * @param  \App\MailTemplate  $mailTemplate
     * @return void
     */
    public function created(MailTemplate $mailTemplate)
    {
        activity()->log(trans('history.actions.create_mailTemplate',['id' => $mailTemplate->id]));
    }

    /**
     * Handle the mail template "updating" event.
     *
     * @param  \App\MailTemplate  $mailTemplate
     * @return void
     */
    public function updating(MailTemplate $mailTemplate)
    {
        $mailTemplate->name = str_replace(' ','_',strtoupper(trim($mailTemplate->title)));
    }

    /**
     * Handle the mail template "updated" event.
     *
     * @param  \App\MailTemplate  $mailTemplate
     * @return void
     */
    public function updated(MailTemplate $mailTemplate)
    {
        activity()->log(trans('history.actions.updated_mailTemplate',['id' => $mailTemplate->id]));
    }

    /**
     * Handle the mail template "deleted" event.
     *
     * @param  \App\MailTemplate  $mailTemplate
     * @return void
     */
    public function deleting(MailTemplate $mailTemplate)
    {
        if (!$mailTemplate->deletable) {
            return abort(403);
        }
    }

    /**
     * Handle the mail template "deleted" event.
     *
     * @param  \App\MailTemplate  $mailTemplate
     * @return void
     */
    public function deleted(MailTemplate $mailTemplate)
    {
        activity()->log(trans('history.actions.deleted_mailTemplate',['id' => $mailTemplate->id]));
    }

    /**
     * Handle the mail template "restored" event.
     *
     * @param  \App\MailTemplate  $mailTemplate
     * @return void
     */
    public function restored(MailTemplate $mailTemplate)
    {
        //
    }

    /**
     * Handle the mail template "force deleted" event.
     *
     * @param  \App\MailTemplate  $mailTemplate
     * @return void
     */
    public function forceDeleted(MailTemplate $mailTemplate)
    {
        //
    }
}
