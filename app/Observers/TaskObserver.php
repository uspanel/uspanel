<?php

namespace App\Observers;

use App\Event;
use App\MailsettingForProject;
use App\MailTemplate;
use App\Project;
use App\Status;
use App\Task;
use App\User;
use Auth;
use Activity;
use Carbon\Carbon;
use Illuminate\Support\Facades\Notification;
use CodersStudio\Notifications\Notifications\System;
use Illuminate\Support\Facades\Response;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

class TaskObserver
{
    /**
     * Handle the task "creating" event.
     *
     * @param \App\Task $task
     * @return void
     */
    public function creating(Task $task)
    {
        if (Auth::check()) {
            $task->creator_id = Auth::id();
        }
    }

    /**
     * Handle the task "created" event.
     *
     * @param \App\Task $task
     * @return void
     */
    public function created(Task $task)
    {
        // TODO notifications 1!
        // TODO что-то пошло не так
        $task->notifyUsers(trans('admin.task.task_created'));
        activity()->log(trans('history.actions.create_task', ['id' => $task->id]));


//        Event::doActions(
//            'TaskCreated',
//            [
//                'message' => trans(
//                    'notifications.task_created',
//                    [
//                        'id' => $task->id,
//                    ]
//                ),
//                'link' => '/admin/tasks?id=' . $task->id,
//                'task' => $task
//            ]
//        );

        if ($task->employee) {
            if ($task->employee->tasks()->count() === 1) {
                Event::doActions(
                    'UserGotFirstTask',
                    [
                        'message' => trans(
                            'notifications.user_got_first_task',
                            [
                                'user' => $task->employee->user->name,
                                'task' => $task->id,
                            ]
                        ),
                        'link' => '/admin/tasks?id=' . $task->id,
                        'user' => $task->employee->user
                    ]
                );
            }elseif($task->employee->tasks()->count() > 1){
                Event::doActions(
                    'UserGotTask',
                    [
                        'message' => trans(
                            'notifications.user_got_task',
                            [
                                'user' => $task->employee->user->name,
                                'task' => $task->id,
                            ]
                        ),
                        'link' => '/admin/tasks?id=' . $task->id,
                        'user' => $task->employee->user
                    ]
                );
            }
        }
    }

    /**
     * Handle the Task "updated" event.
     *
     * @param \App\Task $task
     * @return void
     */
    public function updated(Task $task)
    {
        activity()->log(trans('history.actions.updated_task', ['id' => $task->id]));
        if ($task->employee) {
            $salary = $task->employee->user->salaries()->withTrashed()->where('task_id', $task->id)->first();
            if ($task->delivered_at) {
                $task->employee->user->setSalary($task);
                $done = Status::where('title', '=', 'Done')->first();
                // TODO что здесь происходит
                // if ($done) {
                //     $task->packages()->update([
                //         'status_id' => $done->id
                //     ]);
                // }
            } else {
                !$salary ?: $salary->delete();
            }
        }
    }

    public function updating(Task $task)
    {
        // TODO notifications 2!
        // TODO notifications 3
        $oldTask = Task::find($task->id);
        if ($oldTask->delivered_at != $task->delivered_at) {
            if (!empty($task->delivered_at)) {
                foreach ($task->items as $item) {
                    if ($task->warehouse) {
                        $task->warehouse->morphTasks()
                            ->attach(
                                [
                                    $task->id => [
                                        'qty' => $item->pivot->qty,
                                        'item_id' => $item->id,
                                        'created_at' => Carbon::now()
                                    ]
                                ]
                            );
                    }
                }
            }
        }
        if ($oldTask->employee_status_id != $task->employee_status_id) {
            $task->notifyUsers(
                trans(
                    'admin.task.status_changed',
                    [
                        'status' => $task->employeeStatus->name,
                    ]
                )
            );
            Event::doActions(
                'TaskStatusChanged',
                [
                    'message' => trans(
                        'notifications.task_status_changed',
                        [
                            'id' => $task->id,
                            'status' => $task->employeeStatus->name
                        ]
                    ),
                    'link' => '/admin/tasks?id=' . $task->id,
                    'task' => $task
                ]
            );

            if (
                $task->employeeStatus->name === 'sent' &&
                $task->employee->tasks()->count() === 1
            ) {
                Event::doActions(
                    'UserDoneFirstTask',
                    [
                        'message' => trans(
                            'notifications.user_done_first_task',
                            [
                                'user' => $task->employee->user->name,
                                'task' => $task->id
                            ]
                        ),
                        'link' => '/admin/tasks?id=' . $task->id,
                        'user' => $task->employee
                    ]
                );
            }

            $firstTask = $task->employee->tasks()->orderBy('created_at', 'asc')->first();
            if (
                $task->id !== $firstTask->id &&
                $firstTask->taskStatus->name !== 'sent'
            ) {
                Event::doActions(
                    'UserHasNotChangedStatusOnFirstTask',
                    [
                        'message' => trans(
                            'notifications.user_has_not_changed_status_on_first_task',
                            [
                                'user' => $task->employee->user->name,
                            ]
                        ),
                        'link' => '/admin/users?id=' . $task->employee->user->id,
                        'user' => $task->employee
                    ]
                );
            }

        }
        if ($oldTask->employee_status_id != $task->employee_status_id && $task->delivery_status_id == null) {
            $task->setConditionalStatus();
        }

        if (
            $task->isDirty('employee_id') &&
            $task->employee
        ) {
            Event::doActions(
                'UserGotTask',
                [
                    'message' => trans(
                        'notifications.user_got_task',
                        [
                            'user' => $task->employee->user->name,
                            'task' => $task->id,
                        ]
                    ),
                    'link' => '/admin/tasks?id=' . $task->id,
                ]
            );

            if ($task->employee->tasks()->count() === 1) {
                Event::doActions(
                    'UserGotFirstTask',
                    [
                        'message' => trans(
                            'notifications.user_got_first_task',
                            [
                                'user' => $task->employee->user->name,
                                'task' => $task->id,
                            ]
                        ),
                        'link' => '/admin/tasks?id=' . $task->id,
                    ]
                );
            }
        }
    }

    /**
     * Handle the task "deleted" event.
     *
     * @param \App\Task $task
     * @return void
     */
    public function deleted(Task $task)
    {
        activity()->log(trans('history.actions.deleted_task', ['id' => $task->id]));
    }


}
