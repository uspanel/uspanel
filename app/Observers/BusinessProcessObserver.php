<?php

namespace App\Observers;

use App\BusinessProcess;
use Activity;

class BusinessProcessObserver
{
    /**
     * Handle the business process "created" event.
     *
     * @param  \App\BusinessProcess  $businessProcess
     * @return void
     */
    public function created(BusinessProcess $businessProcess)
    {
        activity()->log(trans('history.actions.create_businessProcess',['id' => $businessProcess->id]));
    }

    /**
     * Handle the business process "updated" event.
     *
     * @param  \App\BusinessProcess  $businessProcess
     * @return void
     */
    public function updated(BusinessProcess $businessProcess)
    {
        activity()->log(trans('history.actions.updated_businessProcess',['id' => $businessProcess->id]));
    }

    /**
     * Handle the business process "deleted" event.
     *
     * @param  \App\BusinessProcess  $businessProcess
     * @return void
     */
    public function deleted(BusinessProcess $businessProcess)
    {
        activity()->log(trans('history.actions.deleted_businessProcess',['id' => $businessProcess->id]));
    }
}
