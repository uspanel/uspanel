<?php

namespace App\Observers;

use App\SalesStatus;
use App\SalesTask;
use Auth;
use Activity;

class SalesTasksObserver
{
    /**
     * Handle the sales task "created" event.
     *
     * @param  \App\SalesTask  $salesTask
     * @return void
     */
    public function created(SalesTask $salesTask)
    {
        activity()->log(trans('history.actions.create_salesTask', ['id' => $salesTask->id]));//
    }

    /**
     * Handle the sales task "updating" event.
     *
     * @param  \App\SalesTask  $salesTask
     * @return void
     */
    public function updating(SalesTask $salesTask)
    {
        $oldTask = SalesTask::find($salesTask->id);
        if ($oldTask->delivered_at != $salesTask->delivered_at) {
            if (!empty($salesTask->delivered_at)) {
                $salesTask->sales_status_id = SalesStatus::where('name','=','sold')->first()->id;
            }
        }
    }

    /**
     * Handle the sales task "updated" event.
     *
     * @param  \App\SalesTask  $salesTask
     * @return void
     */
    public function updated(SalesTask $salesTask)
    {
        activity()->log(trans('history.actions.updated_salesTask', ['id' => $salesTask->id]));//
    }

    /**
     * Handle the sales task "deleted" event.
     *
     * @param  \App\SalesTask  $salesTask
     * @return void
     */
    public function deleted(SalesTask $salesTask)
    {
        activity()->log(trans('history.actions.deleted_salesTask', ['id' => $salesTask->id]));//
    }

    /**
     * Handle the sales task "restored" event.
     *
     * @param  \App\SalesTask  $salesTask
     * @return void
     */
    public function restored(SalesTask $salesTask)
    {
        //
    }

    /**
     * Handle the sales task "force deleted" event.
     *
     * @param  \App\SalesTask  $salesTask
     * @return void
     */
    public function forceDeleted(SalesTask $salesTask)
    {
        //
    }
}
