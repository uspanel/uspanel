<?php

namespace App\Observers;

use App\Event;
use App\User;
use CodersStudio\ImapMailClient\Mailmessage;
use Activity;
use Illuminate\Support\Facades\Artisan;

class MailmessageObserver
{
    /**
     * Handle the mailmessage "created" event.
     *
     * @param  \App\Mailmessage  $mailmessage
     * @return void
     */
    public function created(Mailmessage $mailmessage)
    {
        activity()->log(trans('history.actions.create_mailmessage',['folder' => $mailmessage->folder, 'id' => $mailmessage->id]));
        Artisan::call('mail:autoreply',['--mailsetting_id' => $mailmessage->mailsetting_id, '--rcpt' => $mailmessage->sender_email]);


        $mailSettingOwner = User::find($mailmessage->mailsetting->owner);
        $sender = User::find($mailmessage->mailsetting->user_id);
        $recipient = User::where('email', $mailmessage->recipient)->first();

        if (
            !empty($mailSettingOwner) &&
            !empty($sender) &&
            !empty($recipient) &&
            $mailSettingOwner->hasRole('Investor') &&
            $sender->hasRole(['Support Invest', 'HR Manager Invest'])
        ) {
            Event::doActions(
                'UserGotAnswerFromInvestor',
                [
                    'message' => trans(
                        'notifications.user_got_answer_from_investor',
                        [
                            'user' => $recipient->name,
                            'investor' => $mailSettingOwner->id,
                        ]
                    ),
                    'link' => '/admin/roles/5/users?id=' . $recipient->id,
                    'business_process_type' => 'invest'
                ]
            );
        }
    }

    /**
     * Handle the mailmessage "updated" event.
     *
     * @param  \App\Mailmessage  $mailmessage
     * @return void
     */
    public function updated(Mailmessage $mailmessage)
    {
        activity()->log(trans('history.actions.updated_mailmessage',['folder' => $mailmessage->folder, 'id' => $mailmessage->id]));
    }

    /**
     * Handle the mailmessage "deleted" event.
     *
     * @param  \App\Mailmessage  $mailmessage
     * @return void
     */
    public function deleted(Mailmessage $mailmessage)
    {
        activity()->log(trans('history.actions.deleted_mailmessage',['folder' => $mailmessage->folder, 'id' => $mailmessage->id]));
    }

    /**
     * Handle the mailmessage "restored" event.
     *
     * @param  \App\Mailmessage  $mailmessage
     * @return void
     */
    public function restored(Mailmessage $mailmessage)
    {
        //
    }

    /**
     * Handle the mailmessage "force deleted" event.
     *
     * @param  \App\Mailmessage  $mailmessage
     * @return void
     */
    public function forceDeleted(Mailmessage $mailmessage)
    {
        //
    }
}
