<?php

namespace App\Observers;

use App\Event;
use App\InvestTask;
use Activity;
use App\Notifications\TemplatizedMail;
use App\Project;
use App\Task;
use Illuminate\Support\Facades\Log;

class InvestTaskObserver
{
    /**
     * Handle the invest task "created" event.
     *
     * @param \App\InvestTask $investTask
     * @return void
     */
    public function created(InvestTask $investTask)
    {
        $contragents = $investTask->getUniqueContragents();
        $investTask->notifyUsers(trans('admin.invest-task.task_created'));
        activity()
            ->withContragent($contragents)
            ->on($investTask)
            ->log(trans('history.actions.create_invest_task', ['id' => $investTask->id]));

        Event::doActions(
            'TaskCreated',
            [
                'message' => trans(
                    'notifications.invest_task_created',
                    [
                        'id' => $investTask->id,
                    ]
                ),
                'link' => '/admin/invest-tasks?id=' . $investTask->id,
                'business_process_type' => 'invest'
            ]
        );

        if ($investTask->employee) {
            Event::doActions(
                'UserGotTask',
                [
                    'message' => trans(
                        'notifications.user_got_task',
                        [
                            'user' => $investTask->employee->user->name,
                            'task' => $investTask->id,
                        ]
                    ),
                    'link' => '/admin/invest-tasks?id=' . $investTask->id,
                ]
            );

            if ($investTask->employee->investTasks()->count() === 1) {
                Event::doActions(
                    'UserGotFirstTask',
                    [
                        'message' => trans(
                            'notifications.user_got_first_task',
                            [
                                'user' => $investTask->employee->user->name,
                                'task' => $investTask->id,
                            ]
                        ),
                        'link' => '/admin/invest-tasks?id=' . $investTask->id,
                    ]
                );
            }
        }
        try {
            if ($investTask->employee) {
                $user = $investTask->employee->user;
                if ($user->mailsettings()->count()) {
                    $user->email = $user->mailsettings()->first()->login;
                }
                $user->notify(new TemplatizedMail($investTask->mailTemplate, $investTask->getTemplatizedText($user)));
            }
        } catch (\Exception $e) {
            Log::error('Unable to send task mail to '.$user->email.'. Error: '.$e->getMessage());
        }

    }

    /**
     * @param InvestTask $investTask
     */
    public function updating(InvestTask $investTask)
    {
        if ($investTask->isDirty()) {
            $contragents = $investTask->getUniqueContragents();

            if ($investTask->isDirty('invest_task_status_id')) {
                $oldStatusId = $investTask->getOriginal('invest_task_status_id');
                $newStatusId = $investTask->invest_task_status_id;

                activity()
                    ->withContragent($contragents)
                    ->on($investTask)
                    ->log(trans(
                        'notifications.invest_task_status_changed',
                        [
                            'id' => $investTask->id,
                            'status' => $investTask->investTaskStatus->title
                        ]
                    ));
                $investTask->notifyUsers(
                    trans(
                        'admin.invest-task.status_changed',
                        [
                            'status' => $investTask->investTaskStatus->title,
                        ]
                    )
                );
                Event::doActions(
                    'TaskStatusChanged',
                    [
                        'message' => trans(
                            'notifications.invest_task_status_changed',
                            [
                                'id' => $investTask->id,
                                'status' => $investTask->investTaskStatus->title
                            ]
                        ),
                        'link' => '/admin/invest-tasks?id=' . $investTask->id,
                        'business_process_type' => 'invest'
                    ]
                );

                if (
                    $investTask->investTaskStatus->name === 'done' &&
                    $investTask->employee->investTasks()->count() === 1
                ) {
                    activity()
                        ->withContragent($contragents)
                        ->on($investTask)
                        ->log(trans(
                            'notifications.user_done_first_task',
                            [
                                'user' => $investTask->employee->user->name,
                                'task' => $investTask->id
                            ]
                        ));
                    Event::doActions(
                        'UserDoneFirstTask',
                        [
                            'message' => trans(
                                'notifications.user_done_first_task',
                                [
                                    'user' => $investTask->employee->user->name,
                                    'task' => $investTask->id
                                ]
                            ),
                            'link' => '/admin/invest-tasks?id=' . $investTask->id,
                        ]
                    );
                }

                $firstTask = $investTask->employee->investTasks()->orderBy('created_at', 'asc')->first();
                if (
                    $investTask->id !== $firstTask->id &&
                    $firstTask->investTaskStatus->name !== 'done'
                ) {
                    activity()
                        ->withContragent($contragents)
                        ->on($investTask)
                        ->log(trans(
                            'notifications.user_has_not_changed_status_on_first_task',
                            [
                                'user' => $investTask->employee->user->name,
                            ]
                        ));
                    Event::doActions(
                        'UserHasNotChangedStatusOnFirstTask',
                        [
                            'message' => trans(
                                'notifications.user_has_not_changed_status_on_first_task',
                                [
                                    'user' => $investTask->employee->user->name,
                                ]
                            ),
                            'link' => '/admin/users?id=' . $investTask->employee->user->id,
                        ]
                    );
                }
            }

            if (
                $investTask->isDirty('employee_id') &&
                $investTask->employee
            ) {
                try {
                    $user = $investTask->employee->user;
                    if ($user->mailsettings()->count()) {
                        $user->email = $user->mailsettings()->first()->login;
                    }
                    $user->notify(new TemplatizedMail($investTask->mailTemplate,$investTask->getTemplatizedText($user)));
                } catch (\Exception $e) {
                    Log::error('Unable to send task mail to '.$user->email.'. Error: '.$e->getMessage());
                }

                activity()
                    ->withContragent($contragents)
                    ->on($investTask->employee->user)
                    ->log(trans(
                        'notifications.user_got_task',
                        [
                            'user' => $investTask->employee->user->name,
                            'task' => $investTask->id,
                        ]
                    ));
                Event::doActions(
                    'UserGotTask',
                    [
                        'message' => trans(
                            'notifications.user_got_task',
                            [
                                'user' => $investTask->employee->user->name,
                                'task' => $investTask->id,
                            ]
                        ),
                        'link' => '/admin/invest-tasks?id=' . $investTask->id,
                    ]
                );

                if ($investTask->employee->investTasks()->count() === 1) {
                    activity()
                        ->withContragent($contragents)
                        ->on($investTask->employee->user)
                        ->log(trans(
                            'notifications.user_got_first_task',
                            [
                                'user' => $investTask->employee->user->name,
                                'task' => $investTask->id,
                            ]
                        ));
                    Event::doActions(
                        'UserGotFirstTask',
                        [
                            'message' => trans(
                                'notifications.user_got_first_task',
                                [
                                    'user' => $investTask->employee->user->name,
                                    'task' => $investTask->id,
                                ]
                            ),
                            'link' => '/admin/invest-tasks?id=' . $investTask->id,
                        ]
                    );
                }
            }
        }
    }

    /**
     * Handle the invest task "updated" event.
     *
     * @param \App\InvestTask $investTask
     * @return void
     */
    public function updated(InvestTask $investTask)
    {
        $contragents = $investTask->getUniqueContragents();
        activity()
            ->withContragent($contragents)
            ->on($investTask)
            ->log(trans('history.actions.updated_invest_task', ['id' => $investTask->id]));
    }

    /**
     * Handle the invest task "deleted" event.
     *
     * @param \App\InvestTask $investTask
     * @return void
     */
    public function deleted(InvestTask $investTask)
    {
        $contragents = $investTask->getUniqueContragents();
        activity()
            ->withContragent($contragents)
            ->on($investTask)
            ->log(trans('history.actions.deleted_invest_task', ['id' => $investTask->id]));
    }

    /**
     * Handle the invest task "restored" event.
     *
     * @param \App\InvestTask $investTask
     * @return void
     */
    public function restored(InvestTask $investTask)
    {
        //
    }

    /**
     * Handle the invest task "force deleted" event.
     *
     * @param \App\InvestTask $investTask
     * @return void
     */
    public function forceDeleted(InvestTask $investTask)
    {
        //
    }
}
