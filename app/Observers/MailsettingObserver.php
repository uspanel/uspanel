<?php

namespace App\Observers;

use App\Mailsetting;
use Activity;

class MailsettingObserver
{
    /**
     * Handle the mailsetting "created" event.
     *
     * @param  \App\Mailsetting  $mailsetting
     * @return void
     */
    public function created(Mailsetting $mailsetting)
    {
        activity()->log(trans('history.actions.created_mailaccount',['id' => $mailsetting->id]));
    }

    /**
     * Handle the mailsetting "updated" event.
     *
     * @param  \App\Mailsetting  $mailsetting
     * @return void
     */
    public function updated(Mailsetting $mailsetting)
    {
        activity()->log(trans('history.actions.updated_mailaccount',['id' => $mailsetting->id]));
    }

    /**
     * Handle the mailsetting "deleted" event.
     *
     * @param  \App\Mailsetting  $mailsetting
     * @return void
     */
    public function deleted(Mailsetting $mailsetting)
    {
        activity()->log(trans('history.actions.deleted_mailaccount',['id' => $mailsetting->id]));
    }

    /**
     * Handle the mailsetting "restored" event.
     *
     * @param  \App\Mailsetting  $mailsetting
     * @return void
     */
    public function restored(Mailsetting $mailsetting)
    {
        //
    }

    /**
     * Handle the mailsetting "force deleted" event.
     *
     * @param  \App\Mailsetting  $mailsetting
     * @return void
     */
    public function forceDeleted(Mailsetting $mailsetting)
    {
        //
    }
}
