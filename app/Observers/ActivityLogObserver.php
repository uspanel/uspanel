<?php

namespace App\Observers;

use App\ActivityLog;

class ActivityLogObserver
{
    /**
     * Handle the activity log "creating" event.
     *
     * @param  \App\ActivityLog  $activityLog
     * @return void
     */
    public function creating(ActivityLog $activityLog)
    {
        //
    }

    /**
     * Handle the activity log "updated" event.
     *
     * @param  \App\ActivityLog  $activityLog
     * @return void
     */
    public function updated(ActivityLog $activityLog)
    {
        //
    }

    /**
     * Handle the activity log "deleted" event.
     *
     * @param  \App\ActivityLog  $activityLog
     * @return void
     */
    public function deleted(ActivityLog $activityLog)
    {
        //
    }

    /**
     * Handle the activity log "restored" event.
     *
     * @param  \App\ActivityLog  $activityLog
     * @return void
     */
    public function restored(ActivityLog $activityLog)
    {
        //
    }

    /**
     * Handle the activity log "force deleted" event.
     *
     * @param  \App\ActivityLog  $activityLog
     * @return void
     */
    public function forceDeleted(ActivityLog $activityLog)
    {
        //
    }
}
