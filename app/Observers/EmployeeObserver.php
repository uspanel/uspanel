<?php

namespace App\Observers;

use App\Traits\EmployeeCountable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\{Event,
    Jobs\CreateVoicentContact,
    Jobs\RegisterNewEmail,
    MailsettingForProject,
    MailTemplate,
    Project,
    Setting,
    User,
    UserStatus,
    Employee};
use Illuminate\Support\Facades\Notification;
use App\Notifications\UserIsApproved;
use Illuminate\Support\Facades\Response;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

class EmployeeObserver
{
    use EmployeeCountable;

    public function updating(Employee $employee)
    {
        $oldEmployee = Employee::find($employee->id);

        // TODO notifications 12!
        if ($employee->isDirty('user_status_id')) {
            $newStatus = $employee->user_status_id;
            $oldStatus = $employee->getOriginal('user_status_id');

            if ($newStatus != $oldStatus) {
                if (UserStatus::find($newStatus)->name == 'wait_docs') {
                    try {
                        Notification::send($employee->user, new UserIsApproved($employee->user));
                    } catch(\Exception $e) {
                        Log::error('Mailing error: '.$e->getMessage());
                    }

                } elseif (UserStatus::find($newStatus)->name == 'approved') {
                    dispatch(new RegisterNewEmail($employee->user->id, $employee->project_id));

                    Event::doActions(
                        'UserStatusApproved',
                        [
                            'message' => trans(
                                'notifications.user_status_approved',
                                [
                                    'name' => $employee->user->name,
                                ]
                            ),
                            'link' => '/admin/roles/5/users',
                            'user' => $employee->user
                        ]
                    );
                }
                $employee->user->notifyUsers(trans('admin.user.user_status_updated', [
                    'name' => $employee->user->name,
                    'status' => UserStatus::find($newStatus)->name
                ]));
            }
        }

         if ($employee->isDirty('interviewed') && $employee->interviewed) {
             Event::doActions(
                 'UserPassedInterview',
                 [
                     'message' => trans(
                         'notifications.user_passed_interview',
                         [
                             'name' => $employee->user->name,
                         ]
                     ),
                     'link' => '/admin/roles/5/users',
                     'user' => $employee

                 ]
             );
         }

//         if (
//            $oldEmployee->getMedia('contract')->count() &&
//            $employee->getMedia('contract')->count() &&
//         ) {
//
//         }
    }

    public function created(Employee $employee)
    {
        if ($employee->hr_id == null) {
            $employee->hr_id = $this->getCountedHrId($employee);
        }
        if ($employee->support_id == null) {
            $employee->support_id = $this->getCountedSupportId($employee);
        }
        if ($employee->isDirty()) {
            $employee->save();
        }

        $user = User::find($employee->user_id);


        Event::doActions('UserRegistered', [
            'message' => trans('notifications.new_user_has_registered',
                [
                    'name' => $user->name
                ]),
            'link' => '/admin/users?id=' . $user->id,
            'user' => $user
        ]);


        if (
            config('voicent.enabled') &&
            !empty($employee->user->phone_number)
        ) {
            dispatch(new CreateVoicentContact($employee->user))->delay(Carbon::now()->addMinutes(Setting::getSetting('	voicent_survey_interval_min') ?: 0));
        }
    }

}
