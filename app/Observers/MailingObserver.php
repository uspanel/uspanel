<?php

namespace App\Observers;

use App\Mailing;
use Activity;

class MailingObserver
{
    /**
     * Handle the mailing "created" event.
     *
     * @param  \App\Mailing  $mailing
     * @return void
     */
    public function created(Mailing $mailing)
    {
        activity()->log(trans('history.actions.create_mailing',['id' => $mailing->id]));
    }

    /**
     * Handle the mailing "updated" event.
     *
     * @param  \App\Mailing  $mailing
     * @return void
     */
    public function updated(Mailing $mailing)
    {
        activity()->log(trans('history.actions.updated_mailing',['id' => $mailing->id]));
    }

    /**
     * Handle the mailing "deleted" event.
     *
     * @param  \App\Mailing  $mailing
     * @return void
     */
    public function deleted(Mailing $mailing)
    {
        activity()->log(trans('history.actions.deleted_mailing',['id' => $mailing->id]));
    }

    /**
     * Handle the mailing "restored" event.
     *
     * @param  \App\Mailing  $mailing
     * @return void
     */
    public function restored(Mailing $mailing)
    {
        //
    }

    /**
     * Handle the mailing "force deleted" event.
     *
     * @param  \App\Mailing  $mailing
     * @return void
     */
    public function forceDeleted(Mailing $mailing)
    {
        //
    }
}
