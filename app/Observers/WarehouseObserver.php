<?php

namespace App\Observers;

use App\Warehouse;
use Activity;

class WarehouseObserver
{
    /**
     * Handle the warehouse "created" event.
     *
     * @param  \App\Warehouse  $warehouse
     * @return void
     */
    public function created(Warehouse $warehouse)
    {
        activity()->log(trans('history.actions.create_warehouse',['id' => $warehouse->id])); 
    }

    /**
     * Handle the warehouse "updated" event.
     *
     * @param  \App\Warehouse  $warehouse
     * @return void
     */
    public function updated(Warehouse $warehouse)
    {
        activity()->log(trans('history.actions.updated_warehouse',['id' => $warehouse->id]));
    }

    /**
     * Handle the warehouse "deleted" event.
     *
     * @param  \App\Warehouse  $warehouse
     * @return void
     */
    public function deleted(Warehouse $warehouse)
    {
        activity()->log(trans('history.actions.deleted_warehouse',['id' => $warehouse->id])); 
    }

    /**
     * Handle the warehouse "restored" event.
     *
     * @param  \App\Warehouse  $warehouse
     * @return void
     */
    public function restored(Warehouse $warehouse)
    {
        //
    }

    /**
     * Handle the warehouse "force deleted" event.
     *
     * @param  \App\Warehouse  $warehouse
     * @return void
     */
    public function forceDeleted(Warehouse $warehouse)
    {
        //
    }
}
