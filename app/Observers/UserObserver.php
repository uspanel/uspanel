<?php

namespace App\Observers;

use App\Employee;
use App\Event;
use App\HistoryMailing;
use App\Traits\EmployeeCountable;
use App\User;

class UserObserver
{
    use EmployeeCountable;

    /**
     * Handle the user "created" event.
     *
     * @param \App\User $user
     * @return void
     */
    public function created(User $user)
    {
        activity()->log(trans('history.actions.create_user', ['id' => $user->id]));

        Event::doActions('UserRegistered', [
            'message' => trans('notifications.new_user_has_registered',
                [
                    'name' => $user->name
                ]),
            'link' => '/admin/users?id=' . $user->id,
            'user' => $user
        ]);
        $historyMailing = HistoryMailing::register($user->email);
        if ($historyMailing) {
            $user->mailing_id = $historyMailing->mailing_id;
            $user->save();
        }
    }

    /**
     * Handle the user "updated" event.
     *
     * @param \App\User $user
     * @return void
     */
    public function updated(User $user)
    {
        if (isset($user->getDirty()['remember_token'])) {
            return;
        }

        activity()->log(trans('history.actions.updated_user', ['id' => $user->id]));
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param \App\User $user
     * @return void
     */
    public function deleted(User $user)
    {
        activity()->log(trans('history.actions.deleted_user', ['id' => $user->id]));

        if ($user->hasRole('HR Manager Staff')) {
            $this->recountEmployees($user, 'hr_id');
        }
        if ($user->hasRole('Support Staff')) {
            $this->recountEmployees($user, 'support_id');
        }

        if ($user->hasRole(['Employee Staff', 'Employee Invest']) && $user->employee) {
            $user->employee->delete();
        }
    }

    /**
     * Handle the user "restored" event.
     *
     * @param \App\User $user
     * @return void
     */
    public function restored(User $user)
    {
        if ($user->hasRole(['Employee Staff', 'Employee Invest'])) {
            $employee = Employee::onlyTrashed()->where('user_id', $user->id)->first();
            if ($employee) {
                $employee->restore();
            }
        }
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param \App\User $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        if ($user->hasRole(['Employee Staff', 'Employee Invest']) && $user->employee) {
            $user->employee->forceDelete();
        }
    }


}
