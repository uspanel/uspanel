<?php

namespace App\Observers;

use App\Setting;
use Activity;

class SettingObserver
{
    /**
     * Handle the setting "created" event.
     *
     * @param  \App\Setting  $setting
     * @return void
     */
    public function created(Setting $setting)
    {
        activity()->log(trans('history.actions.create_setting',['id' => $setting->id])); 
    }

    /**
     * Handle the setting "updated" event.
     *
     * @param  \App\Setting  $setting
     * @return void
     */
    public function updated(Setting $setting)
    {
        activity()->log(trans('history.actions.updated_setting',['id' => $setting->id]));
    }

    /**
     * Handle the setting "deleted" event.
     *
     * @param  \App\Setting  $setting
     * @return void
     */
    public function deleted(Setting $setting)
    {
        activity()->log(trans('history.actions.deleted_setting',['id' => $setting->id])); 
    }
}
