<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessProcessType extends Model
{
    protected $fillable = ['name', 'title'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function events()
    {
        return $this->belongsToMany(Event::class);
    }
}
