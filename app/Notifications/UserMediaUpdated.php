<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\User;
use Brackets\Media\HasMedia\MediaCollection;

class UserMediaUpdated extends Notification
{
    use Queueable;

    protected $user;
    protected $mediaCollection;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, MediaCollection $mediaCollection)
    {
        $this->user = $user;
        $this->mediaCollection = $mediaCollection;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray ($notifiable)
    {
        return [
            'text' => trans('notifications.user_media_updated', [
                'name' => $this->user->name,
                'mediaCollectionName' => $this->mediaCollection->getName()
            ]),
            'link' => route('admin.users.index'),
            'params' => [],
        ];
    }
}
