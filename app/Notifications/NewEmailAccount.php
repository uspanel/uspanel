<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewEmailAccount extends Notification
{
    use Queueable;

    /**
     * New email login
     * @var string
     */
    protected $login = '';

    /**
     * New email password
     * @var string
     */
    protected $password = '';

    /**
     * Create a new notification instance.
     *
     * @param string $login
     * @param string $password
     */
    public function __construct(string $login, string $password)
    {
        $this->login = $login;
        $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {   $from =  config('mail.from.address');
        if ($notifiable->employee && $notifiable->employee->project && $notifiable->employee->project->mail_domain) {
            $from = 'info@'.$notifiable->employee->project->mail_domain;
        }
        return (new MailMessage)
            ->from($from,'Info')
            ->subject(trans('notifications.new_email_account_subject'))
            ->line(trans('notifications.new_email_account_body',['login' => $this->login, 'password' => $this->password]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
