<?php

namespace App\Notifications;

use App\MailsettingForProject;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class TemplatizedMail extends Notification
{
    use Queueable;

    private $template = '';
    private $content = '';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($template, $content)
    {
        $this->template = $template;
        $this->content = $content;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        if ($notifiable->employee) {
            $project_id = $notifiable->employee->project->id;
        }else{
            $row = DB::table('project_user')->where('user_id', $notifiable->id)->first();
            if($row){
                $project_id = $row->project_id;
            }else{
                $project_id = false;
            }
        }

        if($project_id) {
            $setting = MailsettingForProject::where('project_id',$project_id)->first();
            $from =  $setting->login;
        }else{
            $from =  config('mail.from.address');
        }



//        $from =  config('mail.from.address');
//
//        if ($notifiable->employee && $notifiable->employee->project && $notifiable->employee->project->mail_domain) {
//            $from = 'info@'.$notifiable->employee->project->mail_domain;
//        }
        $mm = (new MailMessage)
            ->from($from,'support')
            ->view('emails.templatized_mail', ['content' => $this->content])
            ->subject($this->template->subject);


        return $mm;
    }
}
