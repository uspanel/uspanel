<?php

namespace App\Notifications;

use App\MailsettingForProject;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\User;
use Illuminate\Support\Facades\DB;

class UserIsApproved extends Notification
{
    use Queueable;

    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        if ($notifiable->employee) {
            $project_id = $notifiable->employee->project->id;
        }else{
            $row = DB::table('project_user')->where('user_id', $notifiable->id)->first();
            if($row){
                $project_id = $row->project_id;
            }else{
                $project_id = false;
            }
        }

        if($project_id) {
            $setting = MailsettingForProject::where('project_id',$project_id)->first();
            $from =  $setting->login;
        }else{
            $from =  config('mail.from.address');
        }


//        if ($notifiable->employee && $notifiable->employee->project && $notifiable->employee->project->mail_domain) {
//            $from = 'no_reply@'.$notifiable->employee->project->mail_domain;
//        }

        return (new MailMessage)
            ->from($from,'support')
        ->subject(trans('admin.employee.notifications.user_id_approved.greeting'))
        ->view('emails.user_id_approved')
        // ->greeting(trans('admin.employee.notifications.user_id_approved.greeting'))
        // ->line(trans('admin.employee.notifications.user_id_approved.body'))
        // ->action(trans('admin.employee.notifications.user_id_approved.download_contract'), 'https://www.irs.gov/pub/irs-pdf/fw4.pdf')
        // ->action(trans('admin.employee.notifications.user_id_approved.upload_contract'), route('employee.documents'))
        ;

    }

}
