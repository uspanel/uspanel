<?php

namespace App\Listeners;

use App\Jobs\ConvertVideo;
use Pbmedia\LaravelFFMpeg\FFMpegFacade;
use Spatie\MediaLibrary\Events\MediaHasBeenAdded;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class MediaVideoConverterListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param MediaHasBeenAdded $event
     * @return void
     */
    public function handle(MediaHasBeenAdded $event)
    {
        $media = $event->media;
        if ($media->collection_name == 'video') {
            //prevent any events from media model
            $media->flushEventListeners();
            ConvertVideo::dispatch($media);
        }
    }
}
