<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

class UserMailAccaunt extends Model
{
    protected $fillable = [
        "user_id",
        "project_id",
        "project_domain",
        "user_accaunt"
    ];

    protected $dates = [
        "created_at",
        "updated_at",

    ];

}
