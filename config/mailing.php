<?php

return [
    /**
     * How often we need to run mailing delivery job. Cron format.
     */
    'schedule_cron_string' => env('MAILING_SCHEDULE','*/5 * * * *')
];
