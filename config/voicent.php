<?php
return [
    // Parameters from web form html source in voicent crm interface
    'auth' => [
        'vgformid' => env('VOICENT_VGFORMID'),
        'vgformid2' => env('VOICENT_VGFORMID2'),
        'vgformtoken' => env('VOICENT_VGFORMTOKEN'),
        'OWNER_AGENT_ID' => env('VOICENT_OWNER_AGENT_ID',15065),
        'CUSTOMER_CATID' => env('VOICENT_CUSTOMER_CATID',17086)
    ],
    'web_login' => [
        'email' => env('VOICENT_WEB_LOGIN'),
        'uipass' =>env('VOICENT_WEB_PASSWORD'),
        'check' => 'check1'
    ],
    'http_config' => [
        'new_contact' => [
            'base_uri' => 'http://www.voicentlive.com',
            'headers' => [
                'Accept' => '*/*',
                'Content-Type' => 'application/x-www-form-urlencoded',
            ]
        ]
    ],
    'survey_campaign_id' => env('VOICENT_SURVEY_CAMPAIGN_ID'),
    // Enable/Disable automatic adding of contact to voicent
    'enabled' => env('VOICENT_ENABLED',false)
];
