<?php

return [
    /**
     * Account auto-registration section
     */

    /**
     * Full domain name of mail server
     */
    'mail_server_fqdn' => env('MAIL_SERVER_FQDN'),
    /**
     * Default imap port
     */
    'imap_port' => env('DEFAULT_IMAP_PORT',143),
    /**
     * Default smtp port
     */
    'smtp_port' => env('DEFAULT_SMTP_PORT',25),
    /**
     * Default imap encryption
     */
    'imap_encryption' => env('DEFAULT_IMAP_ENCRYPTION','tls'),
    /**
     * Default smtp encryption
     */
    'smtp_encryption' => env('DEFAULT_SMTP_ENCRYPTION','tls'),
    /**
     * Default Inbox folder
     */
    'inbox_folder' => env('DEFAULT_INBOX_FOLDER','INBOX'),
    /**
     * Default Sent folder
     */
    'sent_folder' => env('DEFAULT_SENT_FOLDER','Sent')
];
