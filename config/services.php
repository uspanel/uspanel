<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],

    'nexmo' => [
        'key' => env('NEXMO_KEY'),
        'secret' => env('NEXMO_SECRET'),
        'sms_from' => env('NEXMO_FROM'),
    ],

    'ups' => [
        'auth' => [
            'account' => env('CARRIERS_UPS_ACCOUNT'),
            'secure' => env('CARRIERS_UPS_SECURE'),
            'key' => env('CARRIERS_UPS_KEY'),
        ]
    ],
    'fedex' => [
        'local' => [
            'auth' => [
                'key' => env('CARRIERS_FEDEX_LOCAL_KEY'),
                'password' => env('CARRIERS_FEDEX_LOCAL_PASSWORD'),
                'account_number' => env('CARRIERS_FEDEX_LOCAL_ACCOUNT_NUMBER'),
                'meter_number' => env('CARRIERS_FEDEX_LOCAL_METER_NUMBER'),
            ]
        ],
        'production' => [
            'auth' => [
                'key' => env('CARRIERS_FEDEX_PRODUCTION_KEY'),
                'password' => env('CARRIERS_FEDEX_PRODUCTION_PASSWORD'),
                'account_number' => env('CARRIERS_FEDEX_PRODUCTION_ACCOUNT_NUMBER'),
                'meter_number' => env('CARRIERS_FEDEX_PRODUCTION_METER_NUMBER'),
            ]
        ],

    ],
    'dhl' => [
        'local' => [
            'auth' => [
                'site_id' => env('CARRIERS_DHL_LOCAL_SITE_ID'),
                'password' => env('CARRIERS_DHL_LOCAL_PASSWORD'),
            ]
        ],
        'production' => [
            'auth' => [
                'site_id' => env('CARRIERS_DHL_PRODUCTION_SITE_ID'),
                'password' => env('CARRIERS_DHL_PRODUCTION_PASSWORD'),
            ]
        ],

    ],
    'usps' => [
        'username' => env('CARRIERS_USPS_PRODUCTION_USERNAME'),
        'testmode' => false,
    ],
    'twilio' => [
        'accountSid' => env('TWILIO_ACCOUNT_SID'),
        'authToken' => env('TWILIO_AUTH_TOKEN'),
        'applicationSid' => env('TWILIO_APPLICATION_SID'),
        'number' => env('TWILIO_NUMBER'),
    ],

];
