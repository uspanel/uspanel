<?php

/**
 * https://coders.studio
 * User: alex
 * Date: 3/15/19
 * Time: 9:41 AM
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Messages types
    |--------------------------------------------------------------------------
    |
    |
    | Supported by default: "text"
    |
    */
    'message_types' => [
        'text' => [
            'rules' => [
            ],
            'model' => \CodersStudio\Chat\App\MessageText::class
        ],
        'file' => [
            'rules' => [
                'data.body' => 'required|max:500',
                'data.media_id' => 'required|max:500',
            ],
            'model' => \CodersStudio\Chat\App\MessageFile::class
        ],
        'MessageCall' => [
            'rules' =>[],
            'model' => \CodersStudio\Chat\App\MessageCall::class
        ],
        'MessageSms' => [
            'rules' =>[],
            'model' => \CodersStudio\Chat\App\MessageSms::class
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Chatroom types
    |--------------------------------------------------------------------------
    |
    |
    | Supported by default: "private"
    |
    */
    'chatroom_types' => [
        'private' => [
            'rules' => [],
            'model' => \CodersStudio\Chat\App\ChatroomPrivate::class
        ],
        'group' => [
            'rules' => [],
            'model' => \CodersStudio\Chat\App\ChatroomGroup::class
        ]
    ],
    'notification_class' => App\Notifications\System::class,
];
