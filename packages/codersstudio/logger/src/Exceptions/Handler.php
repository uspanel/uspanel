<?php
/**
 * CodersStudio 2019
 *  https://coders.studio
 *  info@coders.studio
 */

namespace CodersStudio\Logger\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use GuzzleHttp\Client;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Validation\ValidationException::class,
        AuthenticationException::class,
        \Illuminate\Foundation\Http\Middleware\VerifyCsrfToken::class,
    ];


    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if($this->shouldReport($exception) && env('APP_ENV') == 'live'){
            $this->sendMessageToCRM($exception->getFile().':'.$exception->getLine().' | '.$exception->getMessage());
        }
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        return parent::render($request, $exception);
    }

    /**
     * Send message to CRM log
     * @param $message
     * @param null $request
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function sendMessageToCRM($message, $request = null) {
        $client = new Client;
        try {
            $error = request()->ip()."\n";
            $error .= request()->header('User-Agent')."\n";
            if($request){
                $error .= var_dump($request->all(), 1)."\n";
            }
            $client->request('POST', config("logger.crmUrl").'api/projects/'.config("logger.projectId").'/error', [
                'json' => ['text' => $error.$message]
            ]);
        } catch (GuzzleHttp\Exception\BadResponseException $e) {

        }
    }

}
