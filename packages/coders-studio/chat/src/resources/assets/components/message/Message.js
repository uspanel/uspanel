import ChatMessageMedia from './Media.vue'

export default {
	props: {
		message: {
			default: {}
		},
		chatroom: {
			default: () => {
				id: null
			}
		}
	},
	components: {
		ChatMessageMedia
	},
	methods: {
		deleteMessage() {
			var vm = this;
			this.$modal.show('dialog', {
  				title: 'Delete Message',
  				text: 'Are you sure?',
  				buttons: [
				    {
				      	title: 'Close',
				      	class: 'btn btn-light rounded-0'
				    },
				    {
				      	title: 'Delete',
				      	class: 'btn btn-danger rounded-0',
				      	handler: () => {
				      		axios.delete('/chatrooms/'+this.chatroom.id+'/messages/'+this.message.id)
				      		.then(response => {
		                        this.$notify({
									group: 'foo',
									type: 'success',
									title: 'Result',
									text: 'Success'
								});
				      		})
				      		.catch(error => {
				      			this.$notify({
									group: 'foo',
									type: 'error',
									title: 'Result',
									text: 'Error'
								});
				      		})
				      		this.$modal.hide('dialog');
				      	}
				    },
 				]
			})
		},
		
	}
}