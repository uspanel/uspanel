import ChatFileUpload from './ChatFileUpload.vue'
import SearchingIndex from './searching/Index.vue'
import ChatroomList from './chatroom/List.vue'
import ChatroomForm from './chatroom/Form.vue'
import MessageIndex from './message/Index.vue'
import CallModal from './call/Modal.vue'
import Search from './Search.vue'
import ChatSendMessage from './mixins/ChatSendMessage.js'

export default {
    data() {
        return {
            chatContainerHeight: 0,
            search: false,
            chatroomForm: {
                users: [],
                name: null,
            },
            chatroom: {
                id: null,
            },
            messages: [],
            params: {
                page: 1,
                search: null
            },
            user: window.app.user,
            typing: [],
            scroll: {
                complete: false
            },
            scrollLoading: false,
            showChatrooms: this.modal ? false : true,
        }
    },
    props: {
        chatroomId: {
            default: null
        },
        modal: {
            default: false
        }
    },
    mixins: [ChatSendMessage],
    mounted() {
        this.chatContainerHeight = document.documentElement.scrollHeight - Number(this.$refs.chat__container.getBoundingClientRect().top) + 'px';
        if (this.chatroomId) {
            this.toChatroom({id: this.chatroomId})
        }
    },
    watch: {
        'form.data.body': function () {
            Echo.private('chatroom.' + this.chatroom.id)
                .whisper('typing', {
                    user: this.user
                });
        },
    },
    methods: {
        sendSms() {
            this.sendCustomSms(this.opponent.data.id,this.form.data.body);
            var smsdata = {
                body: this.form.data.body,
                type: 'MessageSms',
            };
            axios.post('/chatrooms/' + this.chatroom.id + '/messages', smsdata)
                .then(response => {
                    this.form.data.body = '';
                })

        },
        phoneCall() {
            this.$root.$refs.sipclient.makeCall(this.opponent.data.attributes.phone, this.opponent.data.attributes.name, this.opponent.data.attributes.avatar, {callbacks: {answerCallback : this.callMessage}, device: 'phone'})
        },
        browserCall() {
            this.$root.$refs.sipclient.makeCall('user' + this.opponent.data.id, this.opponent.data.attributes.name, this.opponent.data.attributes.avatar, {callbacks: {answerCallback : this.callMessage}, device: 'browser'});
        },
        callMessage(method, timer, type) {
            var calldata = {
                body: 'Voice call from ' + this.user.name + ' to ' + this.opponent.data.attributes.name + ' via ' + method,
                type: 'MessageCall',
                type_params: {
                    'type':type,
                    'duration':timer
                }
            };
            axios.post('/chatrooms/' + this.chatroom.id + '/messages', calldata)
                .then(response => {
                    this.onSuccessSend();
                })

        },
        toChatroom(chatroom) {
            if (this.chatroom.id == chatroom.id && this.chatroom.attributes) {
                return
            }
            Echo.leave('chatroom.' + this.chatroom.id);
            this.chatroom = chatroom;
            this.$refs.chatroomList.reset(this.chatroom.id);
            this.scroll.complete = false;
            this.messages = [];
            this.params.page = 1;
            if (!this.modal) {
                window.history.replaceState(null, null, '/chat/' + this.chatroom.id);
            }
            this.infiniteHandler(true);
            this.initOnReadWhisper();
            this.initListener();
        },
        searchMessages() {
            this.messages = [];
            this.infiniteHandler();
        },
        initListener() {
            Echo.private('chatroom.' + this.chatroom.id)
                .listenForWhisper('typing', (e) => {
                    var vm = this;
                    if (!_.find(this.typing, {id: e.user.id})) {
                        this.typing.push(e.user);
                    }
                    setTimeout(function () {
                        vm.typing = [];
                    }, 2000);
                })
                .listenForWhisper('messages_read', (e) => {
                    _.forEach(this.messages, (item, index) => {
                        this.$set(this.messages[index].relationships.read.data, 'read_at', true)
                    })
                })
                .notification((notification) => {
                    if (notification.type == 'CodersStudio\\Chat\\App\\Notifications\\MessageDeleted') {
                        var index = _.findIndex(this.messages, function (o) {
                            return o.id == notification.data.id;
                        });
                        if (notification.meta.user_id == this.chatroom.attributes.creator_id) {
                            this.$delete(this.messages, index);
                        }
                    } else if (notification.type == 'CodersStudio\\Chat\\App\\Notifications\\MessageCreated') {
                        this.messages.push(notification.data)
                        this.read();
                        this.toChatBottom();
                    } else if (notification.type == 'CodersStudio\\Chat\\App\\Notifications\\MessagesCleared') {
                        // this.messages = [];
                    }
                });
        },
        onDeleteMessage(id) {
            var index = _.findIndex(this.messages, function (o) {
                return o.id == id;
            });
            this.$delete(this.messages, index);
        },
        read() {
            axios.post('/chatrooms/' + this.chatroom.id + '/read')
                .then(response => {
                    this.initOnReadWhisper();
                })
        },
        initOnReadWhisper() {
            Echo.private('chatroom.' + this.chatroom.id)
                .whisper('messages_read', {
                    chatroom: this.chatroom
                });
        },
        clearChat() {
            var onSuccess = () => {
                this.messages = []
            }
            this.requestStatus({
                method: 'delete',
                url: '/chatrooms/' + this.chatroom.id + '/clear'
            }, onSuccess, {
                group: 'foo',
                type: 'success',
                title: this.trans('chat.result'),
                text: this.trans('chat.chat_cleared')
            })
        },
        deleteChatroom() {
            var onSuccess = () => {
                this.chatroom.id = null
            }
            this.requestStatus({
                method: 'delete',
                url: '/chatrooms/' + this.chatroom.id
            }, onSuccess)
        },
        requestStatus(params = {}, onSuccess = () => {
        }, notifyData = {}) {
            notifyData = Object.assign({
                group: 'foo',
                type: 'success',
                title: this.trans('coders-studio/chat::chat.result'),
                text: this.trans('coders-studio/chat::chat.success')
            }, notifyData)
            var onСonsent = () => {
                axios(params)
                    .then(response => {
                        status = true;
                        this.$notify(notifyData);
                        onSuccess()
                    })
                    .catch(error => {
                        this.$notify({
                            group: 'foo',
                            type: 'error',
                            title: this.trans('chat.result'),
                            text: this.trans('chat.something_went_wrong')
                        });
                    })
                this.$modal.hide('dialog');
            }
            this.deleteDialog(onСonsent);
        },
        deleteDialog(onСonsent = () => {
        }) {
            this.$modal.show('dialog', {
                title: this.trans('chat.delete_message'),
                text: this.trans('chat.are_you_sure'),
                buttons: [
                    {
                        title: this.trans('chat.close'),
                        class: 'btn btn-light rounded-0'
                    },
                    {
                        title: this.trans('chat.delete'),
                        class: 'btn btn-danger rounded-0',
                        handler: () => {
                            onСonsent()
                        }
                    },
                ]
            })
        },
        validate() {
            if (_.isNull(_.trim(this.form.data.body)) || _.isEmpty(_.trim(this.form.data.body))) {
                return false;
            }
            return true
        },
        infiniteHandler(toChatBottom = false) {
            if (this.scrollLoading) {
                return
            }
            this.scrollLoading = true;
            return new Promise((resolve, reject) => {
                axios.get('/chatrooms/' + this.chatroom.id + '/messages', {
                    params: this.params
                })
                    .then(response => {
                        this.scrollLoading = false;
                        var merged = _.concat(_.reverse(response.data.data), this.messages)
                        this.messages = [];
                        this.messages = merged;
                        if (response.data.meta.current_page == response.data.meta.last_page) {
                            this.scroll.complete = true;
                        } else {
                            ++this.params.page
                        }
                        if (toChatBottom) {
                            this.toChatBottom()
                        }
                        resolve(response);
                    })
            });
        },
        handleScroll: function (evt, el) {
            if (el.scrollTop == 0 && this.scroll.complete == false) {
                this.infiniteHandler();
            }
        },
        hasRole(name) {
            var result = _.find(this.user.roles, function (o) {
                return o.name == name
            })
            return result ? true : false;
        },
        isGroup() {
            if (this.chatroom.attributes) {
                return this.chatroom.attributes.type == 'CodersStudio\\Chat\\App\\ChatroomGroup';
            }
        },

    },
    directives: {
        scroll: {
            inserted: function (el, binding) {
                let f = function (evt) {
                    if (binding.value(evt, el)) {
                        window.removeEventListener('scroll', f)
                    }
                }
                el.addEventListener('scroll', f)
            }
        }
    },
    computed: {
        navClass() {
            return [
                'h-100',
                'flex-column',
                'col-md-3',
                this.showChatrooms ? 'd-flex' : 'd-none',
            ]
        },
        chatClass() {
            return [
                this.showChatrooms ? 'col-md-9' : 'col-md-12',
            ]
        },
        callAbility() {
            return this.chatroom && this.chatroom.attributes && (this.chatroom.attributes.type == "CodersStudio\\Chat\\App\\ChatroomPrivate");
        },
        opponent() {
            var vm = this;
            if (this.callAbility) {
                return _.filter(this.chatroom.relationships.users.data, function (o) {
                    return o.data.id != vm.user.id
                })[0];
            } else {
                return null;
            }
        },
        canCallPhone() {
            var op = this.opponent;
            return op && op.data.attributes.phone.length;
        }
    },
    components: {
        ChatroomList,
        MessageIndex,
        CallModal,
        Search,
        ChatroomForm,
        SearchingIndex,
        ChatFileUpload,
    }
}
