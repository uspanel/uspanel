export default {
	name: 'ChatSendMessage',
	data() {
		return {
			form: {
		        data: {
		            body: null,
		            media: [],
                    type: 'text'
		        },
		    },
		}
	},
	methods: {
		toChatBottom() {
            this.$nextTick(() => {
                this.$refs.messageContainer.scrollTop = this.$refs.messageContainer.scrollHeight;
            })
        },
        send() {
            if (this.validate()) {
                axios.post('/chatrooms/'+this.chatroom.id+'/messages', this.form.data)
                .then(response => {
                    this.onSuccessSend();
                })
            }
        },
        onSuccessSend() {
            this.form = {
                data: {
                    body: null,
                    files: [],
                    type: 'text'
                },
            },
            this.toChatBottom();
        },
        validate() {
            return true;
        }
	}
}