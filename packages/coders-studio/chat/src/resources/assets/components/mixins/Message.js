import ChatMessageMedia from '../message/Media.vue'

export default {
	props: {
		message: {
			default: {}
		},
		chatroom: {
			default: () => {
				id: null
			}
		}
	},
	components: {
		ChatMessageMedia
	},
	methods: {
		deleteMessage() {
			var vm = this;
			this.$modal.show('dialog', {
  				title: this.trans('coders-studio/chat::chat.delete_message'),
  				text: this.trans('coders-studio/chat::chat.are_you_sure'),
  				buttons: [
				    {
				      	title: this.trans('coders-studio/chat::chat.close'),
				      	class: 'btn btn-light rounded-0'
				    },
				    {
				      	title: this.trans('coders-studio/chat::chat.delete'),
				      	class: 'btn btn-danger rounded-0',
				      	handler: () => {
				      		axios.delete('/chatrooms/'+this.chatroom.id+'/messages/'+this.message.id)
				      		.then(response => {
		                        this.$notify({
									type: 'success',
									title: this.trans('coders-studio/chat::chat.result'),
									text: this.trans('coders-studio/chat::chat.success')
								});
				      		})
				      		.catch(error => {
				      			this.$notify({
									type: 'error',
									title: this.trans('coders-studio/chat::chat.result'),
									text: this.trans('coders-studio/chat::chat.error')
								});
				      		})
				      		this.$modal.hide('dialog');
				      	}
				    },
 				]
			})
		},
		
	}
}