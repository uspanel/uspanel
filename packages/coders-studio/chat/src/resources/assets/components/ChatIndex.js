export default {
	name: 'ChatIndex',
	data() {
		return {
			form: {
		        data: {
		            body: null,
		            media: [],
		        },
		        type: 'text'
		    },
		}
	},
	methods: {
		toChatBottom() {
            this.$nextTick(() => {
                if (this.$refs.messageContainer) {
                    this.$refs.messageContainer.scrollTop = this.$refs.messageContainer.scrollHeight;
                }
            })
        },
        send() {
            if (this.validate()) {
                axios.post('/chatrooms/'+this.chatroom.id+'/messages', this.form)
                .then(response => {
                    this.onSuccessSend();
                })
            }
        },
        onSuccessSend() {
            this.form = {
                data: {
                    body: null,
                    files: [],
                },
                type: 'text'
            },
            this.toChatBottom();
        },
        validate() {
            return true;
        }
	}
}