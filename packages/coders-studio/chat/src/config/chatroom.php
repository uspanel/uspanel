<?php

/**
 * https://coders.studio
 * User: alex
 * Date: 3/15/19
 * Time: 9:41 AM
 */

return [
    /*
    |--------------------------------------------------------------------------
    | Messages types
    |--------------------------------------------------------------------------
    |
    |
    | Supported by default: "text", "file"
    |
    */
    'message_types' => [
        'text' => [
            'rules' => [
            ],
            'model' => \CodersStudio\Chat\App\MessageText::class
        ],
        'file' => [
            'rules' => [
                'data.body' => 'required|max:500',
                'data.media_id' => 'required|max:500',
            ],
            'model' => \CodersStudio\Chat\App\MessageFile::class
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Chatroom types
    |--------------------------------------------------------------------------
    |
    |
    | Supported by default: "private"
    |
    */
    'chatroom_types' => [
        'private' => [
            'rules' => [],
            'model' => \CodersStudio\Chat\App\ChatroomPrivate::class
        ]
    ],
];