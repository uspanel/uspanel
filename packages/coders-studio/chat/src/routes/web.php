<?php

Route::namespace('\CodersStudio\Chat\App\Http\Controllers')
    ->middleware(['web', 'auth'])
    ->group(function(){
        Route::resource('/users/{user}/contacts', 'UserController')->only(['index']);
        Route::resource('/chatrooms', 'ChatroomController')
            ->only(['index', 'store', 'update', 'destroy']);
        Route::middleware('can:message,chatroom')->group(function(){
            Route::resource('/chatrooms/{chatroom}/messages', 'MessageController')
                ->only(['index', 'store', 'destroy'])
                ->middleware('can:message,chatroom');
            Route::delete('/chatrooms/{chatroom}/clear', 'MessageController@clear')
                ->middleware('can:message,chatroom');
            Route::post('/chatrooms/{chatroom}/upload', 'MessageController@upload')
                ->middleware('can:message,chatroom');
            Route::post('/chatrooms/{chatroom}/read', 'ChatroomController@read')
                ->middleware('can:message,chatroom');
        });
});
