<?php

namespace CodersStudio\Chat\App;

use CodersStudio\Chat\Events\ChatroomSaved;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Auth;

class ChatroomGroup extends Model
{

    protected $fillable = [''];
    
    /**
     * @var array
     */
    protected $hidden = ['id'];

    /**
     * @var bool 
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function chatroom()
    {
        return $this->morphMany('CodersStudio\Chat\App\Chatroom', 'chatroomable');
    }

}
