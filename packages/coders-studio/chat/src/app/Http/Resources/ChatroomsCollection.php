<?php

namespace CodersStudio\Chat\App\Http\Resources;

use CodersStudio\Chat\App\Http\Resources\ChatroomResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ChatroomsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => ChatroomResource::collection($this->collection)
        ];
    }
}
