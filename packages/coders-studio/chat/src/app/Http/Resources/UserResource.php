<?php

namespace CodersStudio\Chat\App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserResource extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => [
                'type'          => 'user',
                'id'            => $this->id,
                'attributes'    => [
                    'name' => $this->name,
                    'avatar' => $this->avatar_thumb_url ? $this->avatar_thumb_url : '/images/avatar.jpg',
                    'phone' => $this->phone ?: ''
                ],
            ]
        ];
    }
}
