<?php

namespace CodersStudio\Chat\App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use CodersStudio\Chat\App\Http\Resources\MediaCollection;

class MessageResource extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'          => 'messages',
            'id'            => $this->id,
            'attributes'    => [
                'body' => $this->body,
                'type' => class_basename($this->messagable),
                'is_read' => $this->is_read
            ],
            'date'          => Carbon::parse($this->created_at)->format('d.m.Y H:i'),
            'relationships' => [
                'user' => UserResource::make($this->user),
                'media' => new MediaCollection($this->getMedia('attachments')),
                'messagable' => $this->messagable,
                'read' => [
                    'data' => $this->read
                ]
            ]
        ];
    }
}
