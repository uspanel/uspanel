<?php

namespace CodersStudio\Chat\App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class FileResource extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'          => 'file',
            'id'            => $this->id,
            'attributes'    => [
                'link' => $this->getFullUrl()
            ],
        ];
    }
}
