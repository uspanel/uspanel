<?php

namespace CodersStudio\Chat\App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use CodersStudio\Chat\App\Http\Resources\UsersCollection;
use Auth;
use CodersStudio\Chat\App\Http\Resources\MessageResource;

class ChatroomResource extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $this->users()->where('id', '!=', auth()->id())->first();
        return [
            'type'          => 'chatrooms',
            'id'            => $this->id,
            'attributes'    => [
                'name' => $this->name ?? $user->name,
                'description' => $this->description,
                'type' => $this->chatroomable_type,
                'creator_id' => $this->creator_id,
                $this->mergeWhen(Auth::check(), [
                    'unread_messages_count' => $this->unread_messages_count,
                    'last_message_date' => $this->last_message_date,
                    'last_message' => $this->last_message,
                    'last_message' => new MessageResource($this->last_message)
                ]),
            ],
            'relationships' => [
                'users' => UsersCollection::make($this->users),
            ]
        ];
    }
}
