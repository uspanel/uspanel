<?php

namespace CodersStudio\Chat\App\Http\Resources;

use CodersStudio\Chat\App\Http\Resources\MessageResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class MessagesCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => MessageResource::collection($this->collection),
        ];
    }
}
