<?php

namespace CodersStudio\Chat\App\Http\Resources;

use CodersStudio\Chat\App\Http\Resources\MediaResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class MediaCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => MediaResource::collection($this->collection),
        ];
    }
}
