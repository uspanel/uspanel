<?php

namespace CodersStudio\Chat\App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class MediaResource extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'          => 'media',
            'id'            => $this->id,
            'attributes'    => [
                'link' => $this->getFullUrl(),
                'name' => $this->name,
                'type' => $this->mime_type
            ],
        ];
    }
}
