<?php

namespace CodersStudio\Chat\App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\User;
use CodersStudio\Chat\App\Http\Resources\UsersCollection;

class UserController extends Controller
{
    public function index() {
        $users = User::where('id', '!=', Auth::id())->get();
        return new UsersCollection($users);
    }
}
