<?php

namespace CodersStudio\Chat\App\Http\Controllers;

use Carbon\Carbon;
use CodersStudio\Chat\App\Chatroom;
use CodersStudio\Chat\App\Http\Requests\StoreChatroom;
use CodersStudio\Chat\App\Http\Requests\UpdateChatroom;
use CodersStudio\Chat\App\Http\Resources\ChatroomResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use CodersStudio\Chat\App\Http\Resources\ChatroomsCollection;

class ChatroomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return new ChatroomsCollection(Auth::user()->chatrooms);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreChatroom $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreChatroom $request)
    {
        $chatroom = Auth::user()->createChatroom($request);
        return new ChatroomResource($chatroom);
    }

    /**
     * Update resource in storage.
     *
     * @param StoreChatroom $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateChatroom $request, Chatroom $chatroom)
    {
        Auth::user()->updateChatroom($request, $chatroom);
        return new ChatroomResource($chatroom);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Chatroom $chatroom
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Chatroom $chatroom)
    {
        if ($chatroom->creator_id == auth()->id()) {
            foreach ($chatroom->users as $user) {
                $user->leaveFromChatroom($chatroom);
            }
        } else {
            Auth::user()->leaveFromChatroom($chatroom);
        }
        return Response::json(null, 204);
    }

    /**
     * Read all messages in this chat
     *
     * @param Chatroom $chatroom
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Chatroom $chatroom)
    {
        Auth::user()->readMessagesInChatroom($chatroom);
        return Response::json(null, 204);
    }

}
