<?php

namespace CodersStudio\Chat\App\Http\Controllers;

use CodersStudio\Chat\App\Chatroom;
use CodersStudio\Chat\App\Http\Requests\IndexMessage;
use CodersStudio\Chat\App\Http\Requests\StoreMessage;
use CodersStudio\Chat\App\Http\Requests\UploadFileMessage;
use CodersStudio\Chat\App\Http\Resources\FileResource;
use CodersStudio\Chat\App\Http\Resources\MessageResource;
use CodersStudio\Chat\App\Message;
use CodersStudio\Chat\App\Notifications\MessageDeleted;
use CodersStudio\Chat\App\Notifications\MessagesCleared;
use CodersStudio\Chat\App\Read;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use CodersStudio\Chat\App\Http\Resources\MessagesCollection;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Str;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Chatroom $chatroom
     * @return MessagesCollection
     */
    public function index(Request $request, Chatroom $chatroom)
    {
        $messages = Auth::user()->getMessagesFrom($chatroom, $request);
        return new MessagesCollection($messages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Chatroom $chatroom
     * @param StoreMessage $request
     * @return MessageResource
     */
    public function store(Chatroom $chatroom, StoreMessage $request)
    {
        $message = Auth::user()->sendMessageTo($chatroom, $request->all(), config('chatroom.message_types.' . $request->get('type') . '.model'));
        return new MessageResource($message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Chatroom $chatroom
     * @param Message $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Chatroom $chatroom, Message $message)
    {
        if($message->user_id === Auth::id()){
            Read::where('message_id', $message->id)->delete();
        }else{
            Auth::user()->reads()->where('message_id', $message->id)->delete();
        }
        Notification::send($chatroom->users, new MessageDeleted($message));
        return Response::json(null, 204);
    }

    /**
     * @param UploadFileMessage $request
     * @param Chatroom $chatroom
     * @return FileResource
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function upload(UploadFileMessage $request, Chatroom $chatroom) {
        $name = $request->file->getClientOriginalName();
        $tempName = Str::random(40);
        $path = Storage::putFileAs(
            'temp', $request->file('file'), $tempName
        );
        return response([
            'tempName' => $tempName,
            'name' => $name
        ]);
        // $media = $chatroom->addMediaFromRequest('file')->toMediaCollection('chatroom-files')->setCustomProperty('user_id', Auth::id());
        // return new FileResource($media);
    }

    /**
     * Remove all messages for current user
     *
     * @param Chatroom $chatroom
     * @return \Illuminate\Http\JsonResponse
     */
    public function clear(Chatroom $chatroom) {
        Auth::user()->reads()->where('chatroom_id', $chatroom->id)->delete();
        Notification::send($chatroom->users, new MessagesCleared($chatroom));
        return Response::json(null, 204);
    }
}
