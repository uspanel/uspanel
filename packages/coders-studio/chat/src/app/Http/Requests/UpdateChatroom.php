<?php

namespace CodersStudio\Chat\App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateChatroom extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        $rules = config('chatroom.chatroom_types.' . $this->request->get('type') . '.rules', []);
        return array_merge([
            'chatroom.name' => 'max:255',
            'chatroom.description' => 'nullable|max:500',
            'users.*' => 'integer',
            'type' => ['required', 'in:' . implode(',', array_keys( config('chatroom.chatroom_types')))],
        ], $rules);
    }
}
