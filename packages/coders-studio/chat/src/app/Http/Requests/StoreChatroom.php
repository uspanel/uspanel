<?php namespace CodersStudio\Chat\App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreChatroom extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        $rules = config('chatroom.chatroom_types.' . $this->request->get('type') . '.rules', []);
        return array_merge([
            'chatroom.name' => 'max:255',
            'chatroom.description' => 'nullable|max:500',
            'users.*' => [
                'integer',
                'not_in:' . Auth::id(),
                function ($attribute, $value, $fail) {
                    if ($this->type == 'private') {
                        $chatroom = auth()->user()->chatrooms()->private()
                        ->whereHas('users', function($query) use($value) {
                            $query->where('user_id', $value);
                        })->get();
                        if ($chatroom->isNotEmpty()) {
                            $fail(trans('coders-studio/chat::chat.chatroom_exists'));
                        }
                    }
                },
            ],
            'type' => ['required', 'in:' . implode(',', array_keys( config('chatroom.chatroom_types')))],
        ], $rules);
    }
}
