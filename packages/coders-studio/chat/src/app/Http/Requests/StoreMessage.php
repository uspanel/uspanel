<?php namespace CodersStudio\Chat\App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class StoreMessage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return  bool
     */
    public function authorize()
    {
        return true;
    }

/**
     * Get the validation rules that apply to the request.
     *
     * @return  array
     */
    public function rules()
    {
        $rules = config('chatroom.message_types.' . $this->request->get('type') . '.rules', []);
        return array_merge([
            'type' => ['required', 'in:' . implode(',', array_keys( config('chatroom.message_types')))],
            'body' => 'max:500'
        ], $rules);
    }
}
