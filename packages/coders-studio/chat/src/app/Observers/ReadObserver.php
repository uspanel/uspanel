<?php

namespace CodersStudio\Chat\App\Observers;

use CodersStudio\Chat\App\Notifications\ReadCreated;
use CodersStudio\Chat\App\Read;
use Illuminate\Support\Facades\Notification;
use Auth;

class ReadObserver
{
    public function created(Read $read)
    {
        $read->user->notify(new ReadCreated($read->chatroom, $read));
    }
}
