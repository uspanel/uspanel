<?php

namespace CodersStudio\Chat\App\Observers;

use Carbon\Carbon;
use CodersStudio\Chat\App\Chatroom;
use CodersStudio\Chat\App\Notifications\MessageCreated;
use CodersStudio\Chat\App\Read;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use CodersStudio\Chat\App\Notifications\ReadCreated;

class ChatroomObserver
{
    /**
     * Handle the message "created" event.
     *
     * @param  \CodersStudio\Chat\App\Message  $message
     * @return void
     */
    public function creating(Chatroom $chatroom)
    {
        if (auth()->check()) {
            $chatroom->creator_id = auth()->id();
        }
    }
}
