<?php

namespace CodersStudio\Chat\App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use CodersStudio\Chat\App\Notifications\ReadCreated;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class Read extends Model
{

    use SoftDeletes;

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'chatroom_id', 'message_id'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;

    public function scopeUnread($query)
    {
        return $query->whereNull('reads.read_at');
    }

    public function scopeAuthUser($query)
    {
        return $query->where('reads.user_id', Auth::id());
    }

    public function scopeOfUser($query, User $user)
    {
        return $query->where('reads.user_id', $user->id);
    }

    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function chatroom()
    {
        return $this->hasOne('CodersStudio\Chat\App\Chatroom', 'id', 'chatroom_id');
    }

}
