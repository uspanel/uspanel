<?php

namespace CodersStudio\Chat\App;

use Illuminate\Database\Eloquent\Model;

class MessageCall extends Model
{

    protected $guarded = ['id'];

    protected $hidden = ['id'];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function message()
    {
        return $this->morphMany('CodersStudio\Chat\App\Message', 'messagable');
    }
}
