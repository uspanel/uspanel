<?php

namespace CodersStudio\Chat\App;

use Illuminate\Database\Eloquent\Model;

class MessageFile extends Model
{

    protected $fillable = ['media_id'];

    protected $hidden = ['id', 'media', 'media_id'];

    public $timestamps = false;

    protected $appends = ['url'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function message()
    {
        return $this->morphMany('CodersStudio\Chat\App\Message', 'messagable');
    }


    public function media(){
        return $this->belongsTo('Spatie\MediaLibrary\Media');
    }

    public function getUrlAttribute($value)
    {
        return $this->media->getFullUrl();
    }

}
