<?php

namespace CodersStudio\Chat\App\Traits;

use App\User;
use Carbon\Carbon;
use CodersStudio\Chat\App\Chatroom;
use CodersStudio\Chat\App\Message;
use CodersStudio\Chat\App\MessageSystem;
use CodersStudio\Chat\App\Notifications\ChatroomCreated;
use CodersStudio\Chat\App\Notifications\ChatroomDeleted;
use CodersStudio\Chat\App\Notifications\ChatroomRead;
use CodersStudio\Chat\App\Notifications\ChatroomUpdated;
use CodersStudio\Chat\App\Notifications\MessageCreated;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Notification;
use Auth;
use Illuminate\Support\Facades\Storage;
use CodersStudio\Chat\App\Notifications\NewMessage;

trait Chatable
{
    use SoftDeletes;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function chatrooms()
    {
        return $this->belongsToMany('CodersStudio\Chat\App\Chatroom')->wherePivot('deleted_at', null);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reads()
    {
        return $this->hasMany('CodersStudio\Chat\App\Read');
    }

    /**
     * Create Chatroom
     *
     * @param array $data
     * @return Chatroom
     */
    public function createChatroom($data) : Chatroom
    {
        $chatroom = new Chatroom($data['chatroom']);
        $chatroomType = config('chatroom.chatroom_types.' . $data['type'] . '.model')::create($data['data'] ?? []);
        $chatroomType->chatroom()->save($chatroom);
        $chatroom->users()->attach([$this->id]);
        if($data['users']) {
            $users = User::whereIn('id', $data['users'])->get();
            $chatroom->users()
                ->syncWithoutDetaching($data['users']);
            Notification::send($users, new ChatroomCreated($chatroom));
            $this->notify(new ChatroomCreated($chatroom));
        }
        return $chatroom;
    }

    /**
     * Create Chatroom
     *
     * @param array $data
     * @return Chatroom
     */
    public function updateChatroom($data, Chatroom $chatroom) : Chatroom
    {
        $chatroom->update($data['chatroom']);
        $chatroom->chatroomable->update($data['data'] ?? []);
        if($data['users']) {
            $userIds = array_merge([$this->id], $data['users']);
            $syncedUsers = $chatroom->users()->sync($userIds);
            $users = User::whereIn('id', $userIds)->get();
            $detachedUsers = User::whereIn('id', $syncedUsers['detached'])->get();
            $detachedUsersNames = $detachedUsers->implode('full_name', ', ');
            $attachedUsersNames = $users->whereIn('id', $syncedUsers['attached'])->implode('full_name', ', ');
            if($detachedUsersNames || $attachedUsersNames) {
                $this->sendMessageTo($chatroom, [
                    'body' => ($detachedUsersNames ? trans('coders-studio/chat::chat.deleted_users') . $detachedUsersNames : '') . ($attachedUsersNames ? trans('coders-studio/chat::chat.new_users') . $attachedUsersNames : '')
                ], MessageSystem::class);
                $notification = config('chatroom.notification_class');
                if ($notification) {
                    foreach($detachedUsers as $detachedUser){
                        Notification::send($detachedUser, new $notification(trans('coders-studio/chat::chat.you_was_removed', [
                            'user' => auth()->user()->name
                        ]), route('chat')));
                    }
                }
            }
            Notification::send($users, new ChatroomUpdated($chatroom));
        }
        return $chatroom;
    }

    /**
     * Send message to chatroom
     *
     * @param Chatroom $chatroom
     * @param array $data
     * @param string $type
     * @return Message
     */
    public function sendMessageTo(Chatroom $chatroom, array $data, $typeModel) : Message
    {
        $message = $chatroom
        ->messages()->make([
            'user_id' => $this->id,
            'body' => $data['body'],
        ]);
        if (isset($data['media'])) {
            foreach ($data['media'] as $media) {
                $message->addMedia(storage_path('app/temp/'.$media['tempName']))->usingName($media['name'])->toMediaCollection('attachments')->setCustomProperty('user_id', Auth::id());
                Storage::delete('app/temp/'.$media['tempName']);
            }
        }
        $params = [];
        if (isset($data['type_params'])) {
            $params = $data['type_params'];
        }
        $messageType = $typeModel::create($params);
        $messageType->message()->save($message);
        $users = $message->chatroom->users;
        foreach ($users as $user){
            $message->read()->create([
                'user_id' => $user->id,
                'chatroom_id' => $message->chatroom->id,
                'read_at' => $user->id === Auth::id() ? Carbon::now() : null
            ]);
            if ($user->id != auth()->id()) {
                $notification = config('chatroom.notification_class');
                if ($notification) {
                    Notification::send($user, new $notification(trans('coders-studio/chat::chat.new_message_from', [
                        'user' => auth()->user()->name
                    ]), route('chat', ['chat' => $message->chatroom->id])));
                }
            }
        }
        Notification::send($message->chatroom, new MessageCreated($message));
        return $message;
    }

    /**
     * Get messages from chatroom
     *
     * @param $chatroom
     * @return Collection
     */
    public function getMessagesFrom($chatroom, $request) : LengthAwarePaginator
    {
        $this->readMessagesInChatroom($chatroom);
        return $chatroom
            ->messages()
            ->with(['read','messagable'])
            ->whereHas('read', function ($query) {
                $query->where('user_id', auth()->id());
            })
            ->when($request->filled('search'), function($query) use ($request) {
                return $query->where('body', 'LIKE', '%' . $request->get('search') . '%');
            })
            ->orderBy('created_at', 'desc')
            ->paginate(40);
    }

    /**
     * Read messages in chatroom for current user
     *
     * @param $chatroom
     * @return void
     */
    public function readMessagesInChatroom($chatroom) : void
    {
        $this->reads()
            ->join('messages', 'reads.message_id', '=', 'messages.id')
            ->where("messages.user_id","!=", Auth::id())
            ->where('reads.chatroom_id', $chatroom->id)
            ->where("reads.user_id","=", Auth::id())
            ->whereNull('read_at')
            ->update(['read_at' => Carbon::now()]);
        Notification::send($chatroom->users, new ChatroomRead($chatroom));
    }

    /**
     * Leave from chatroom
     *
     * @param $chatroom
     * @return bool
     */
    public function leaveFromChatroom($chatroom) : bool
    {
        Notification::send($chatroom->users, new ChatroomDeleted($chatroom));
        if ($chatroom->isPrivate()) {
            $chatroom->delete();
        }
        return $this->chatrooms()->updateExistingPivot($chatroom->id, ['deleted_at' => Carbon::now()]);
    }

}
