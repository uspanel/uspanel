<?php

namespace CodersStudio\Chat\App\Notifications;

use CodersStudio\Chat\App\Chatroom;
use CodersStudio\Chat\App\Http\Resources\ChatroomResource;
use CodersStudio\Chat\App\Http\Resources\MessageResource;
use CodersStudio\Chat\App\Message;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Broadcasting\PrivateChannel;

class MessageCreated extends Notification
{
    use Queueable;

    private $message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'data' => (array) (new MessageResource($this->message))->resolve()
        ];
    }

    public function broadcastOn()
    {
        return new PrivateChannel('chatroom.'.$this->message->chatroom_id);
    }
}
