<?php

namespace CodersStudio\Chat\App\Notifications;

use CodersStudio\Chat\App\Read;
use CodersStudio\Chat\App\Chatroom;
use CodersStudio\Chat\App\Http\Resources\ChatroomResource;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Broadcasting\PrivateChannel;
use Auth;

class ReadCreated extends Notification
{
    use Queueable;

    private $chatroom;
    private $read;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Chatroom $chatroom, Read $read)
    {
        $this->chatroom = $chatroom;
        $this->read = $read;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'data' => (array)(new ChatroomResource($this->chatroom))->resolve(),
            'meta' => [
                'unread_messages_count' => $notifiable->reads()->where('chatroom_id', $this->chatroom->id)->unread()->count(),
            ]
        ];
    }
    
}
