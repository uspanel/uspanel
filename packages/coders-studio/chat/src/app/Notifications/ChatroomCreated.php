<?php

namespace CodersStudio\Chat\App\Notifications;

use CodersStudio\Chat\App\Chatroom;
use CodersStudio\Chat\App\Http\Resources\ChatroomResource;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Broadcasting\PrivateChannel;
use Auth;

class ChatroomCreated extends Notification
{
    use Queueable;

    private $chatroom;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Chatroom $chatroom)
    {
        $this->chatroom = $chatroom;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'data' => (array) (new ChatroomResource($this->chatroom))->resolve()
        ];
    }
    
}
