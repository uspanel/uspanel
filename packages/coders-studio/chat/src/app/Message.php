<?php

namespace CodersStudio\Chat\App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class Message extends Model implements HasMedia
{

    use SoftDeletes, HasMediaTrait;

    protected $fillable = ['user_id', 'body'];

    protected $casts = [
        'created_at' => 'datetime:H:i',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function messagable()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function chatroom()
    {
        return $this->belongsTo('CodersStudio\Chat\App\Chatroom');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function read()
    {
        return $this->hasOne('CodersStudio\Chat\App\Read');
    }

    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function reads()
    {
        return $this->hasMany('CodersStudio\Chat\App\Read');
    }

    /**
     * @return int
     */
    public function getIsReadAttribute():int
    {
        return $this->reads()->where('reads.user_id', "!=", $this->user_id)->whereNotNull('read_at')->count();
    }

}
