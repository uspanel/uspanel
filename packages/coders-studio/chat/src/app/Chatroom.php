<?php

namespace CodersStudio\Chat\App;

use CodersStudio\Chat\Events\ChatroomSaved;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use \App\User;

class Chatroom extends Model implements HasMedia
{

    use HasMediaTrait, SoftDeletes;

    protected $fillable = ['name', 'description', 'creator_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function messages()
    {
        return $this->hasMany('CodersStudio\Chat\App\Message');
    }

    /**
     * Use username as chatroom name if chatroom name is empty
     *
     * @param $value
     * @return mixed
     */
    public function getNameAttribute($value)
    {
        if (is_null($value)) {
            $value = $this->users()->where('id', '!=', Auth::id())->firstOrFail()->name;
        }
        return $value;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function chatroomable()
    {
        return $this->morphTo();
    }

    public function reads() {
        return $this->hasMany( 'CodersStudio\Chat\App\Read');
    }

    public function unreads()
    {
        return $this->reads()->unread()->authUser();
    }

    public function getLastMessageDateAttribute() {
        return $this->messages()->orderBy('created_at', 'desc')->first()->created_at ?? null;
    }

    /**
     * @return mixed
     */
    public function getUnreadMessagesCountAttribute() {
        return $this->reads()
            ->join('messages', 'reads.message_id', '=', 'messages.id')
            ->where("messages.user_id","!=", Auth::id())
            ->where("reads.user_id","=", Auth::id())
            ->whereNull('read_at')
            ->count();
    }

    public function getLastMessageAttribute() {
        return $this->messages()->orderBy('created_at', 'desc')->first();
    }

    public function creator() {
        return $this->hasOne(User::class, 'id', 'creator_id');
    }

    public function scopePrivate($query)
    {
        return $query->where('chatroomable_type', 'CodersStudio\\Chat\\App\\ChatroomPrivate');
    }

    public function isPrivate()
    {
        return $this->chatroomable_type == 'CodersStudio\\Chat\\App\\ChatroomPrivate';
    }

}
