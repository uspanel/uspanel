<?php

namespace CodersStudio\Chat;

use CodersStudio\Chat\App\Chatroom;
use CodersStudio\Chat\App\Message;
use CodersStudio\Chat\App\Read;
use CodersStudio\Chat\App\Observers\MessageObserver;
use CodersStudio\Chat\App\Observers\ReadObserver;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use CodersStudio\Chat\App\Observers\ChatroomObserver;
use Illuminate\Translation\Translator;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/chatroom.php', 'chatroom'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Gate::define('message', function ($user, Chatroom $chatroom) {
            return $user->hasRole('Administrator') || $chatroom->users->contains('id', $user->id);
        });

        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'coders-studio/chat');
        $this->publishes([
            __DIR__.'/resources/lang' => resource_path('lang/vendor/coders-studio/chat'),
        ]);

        $this->publishes([
            __DIR__.'/config/chatroom.php' => config_path('chatroom.php'),
        ]);

        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadRoutesFrom(__DIR__.'/routes/channels.php');

        $this->publishes([
            __DIR__.'/resources/assets' => resource_path('assets/vendor/CodersStudioChat'),
        ], 'resources');

        $this->loadMigrationsFrom(__DIR__.'/database/migrations');

        Message::observe(MessageObserver::class);
        Read::observe(ReadObserver::class);
        Chatroom::observe(ChatroomObserver::class);

        Route::model('chatroom', Chatroom::class);
    }
}
