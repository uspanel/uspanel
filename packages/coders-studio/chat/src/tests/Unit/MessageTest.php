<?php

namespace CodersStudio\Chat\Tests\Unit;

use CodersStudio\Chat\App\Notifications\MessageCreated;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MessageTest extends TestCase
{

    use RefreshDatabase;

    /**
     * Test for index method
     *
     * @return void
     */
    public function testIndex()
    {
        $user = factory(\App\User::class)->create();
        $users = factory(\App\User::class, 3)->create();
        $chatroom = $user->createChatroom([
            'chatroom' => [
                'name' => 'chat1',
                'description' => 'description1',
            ],
            'type' => 'private',
            'users' => $users->pluck('id')
        ]);
        $message = $chatroom
            ->messages()->make([
                'user_id' => $user->id,
            ]);
        $messageType = config('chatroom.message_types.text.model')::create([
            'body' => 'text1'
        ]);
        $messageType->message()->save($message);
        $response = $this->actingAs($user)->json('GET', '/chatroom/' . $chatroom->id . '/message');
        $response->assertStatus(200)
            ->assertJson([
                'data' => [
                    [
                        'type' => 'messages',
                        'attributes' => [
                            'body' => 'text1'
                        ]
                    ]
                ]
            ]);
    }

    /**
     * Test for store chatroom functionality
     *
     * @return void
     */
    public function testStore()
    {
        Notification::fake();
        $user = factory(\App\User::class)->create();
        $users = factory(\App\User::class, 3)->create();
        $chatroom = $user->createChatroom([
            'chatroom' => [
                'name' => 'chat1',
                'description' => 'description1',
            ],
            'type' => 'private',
            'users' => $users->pluck('id')
        ]);
        $response = $this->actingAs($user)->json('POST', '/chatroom/' . $chatroom->id . '/message', [
            'data' => [
                'body' => 'text1'
            ],
            'type' => 'text'
        ]);
        $response->assertStatus(201)
            ->assertJson([
                'data' => [
                    'type' => 'messages',
                    'attributes' => [
                        'body' => 'text1'
                    ]
                ]
            ]);
        Notification::assertSentTo(
            $chatroom,
            MessageCreated::class
        );
    }

    /**
     * Test for upload files functionality
     *
     * @return void
     */
    public function testUpload()
    {
        $user = factory(\App\User::class)->create();
        $users = factory(\App\User::class, 3)->create();
        $chatroom = $user->createChatroom([
            'chatroom' => [
                'name' => 'chat1',
                'description' => 'description1',
            ],
            'type' => 'private',
            'users' => $users->pluck('id')
        ]);
        $response = $this->actingAs($user)->json('POST', '/chatroom/' . $chatroom->id . '/upload', [
            'file' => UploadedFile::fake()->image('file.jpg')->size(100)
        ]);
        $response->assertStatus(201);
    }

    /**
     * Test for upload files functionality
     *
     * @return void
     */
    public function testFileMessage()
    {
        $user = factory(\App\User::class)->create();
        $users = factory(\App\User::class, 3)->create();
        $chatroom = $user->createChatroom([
            'chatroom' => [
                'name' => 'chat1',
                'description' => 'description1',
            ],
            'type' => 'private',
            'users' => $users->pluck('id')
        ]);
        $response = $this->actingAs($user)->json('POST', '/chatroom/' . $chatroom->id . '/upload', [
            'file' => UploadedFile::fake()->image('file.jpg')
        ]);
        $data = $response->json();
        $response = $this->actingAs($user)->json('POST', '/chatroom/' . $chatroom->id . '/message', [
            'data' => [
                'body' => 'text1',
                'media_id' => $data['data']['id']
            ],
            'type' => 'file'
        ]);
        $response->assertStatus(201)
            ->assertJson([
                'data' => [
                    'type' => 'messages',
                    'attributes' => [
                        'body' => 'text1',
                        'url' => true

                    ]
                ]
            ]);
    }

}
