<?php

namespace CodersStudio\Chat\Tests\Unit;

use CodersStudio\Chat\App\Notifications\ChatroomCreated;
use CodersStudio\Chat\App\Notifications\ChatroomUpdated;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ChatroomTest extends TestCase
{

    use RefreshDatabase;

    private $user;
    private $users;
    private $chatroom;

    protected function setUp()
    {
        parent::setUp();
        $this->user = factory(\App\User::class)->create();
        $this->users = factory(\App\User::class, 3)->create();
        $this->chatroom = $this->user->createChatroom([
            'chatroom' => [
                'name' => 'chat1',
                'description' => 'description1',
            ],
            'type' => 'private',
            'users' => $this->users->pluck('id')
        ]);
    }

    /**
     * Test for index method
     *
     * @return void
     */
    public function testIndex()
    {

        $response = $this->actingAs($this->user)->json('GET', '/chatroom');
        $response->assertJson([
            'data' => [
                [
                    'type' => 'chatrooms',
                    'id' => $this->chatroom->id,
                    'attributes' => [
                        'name' => 'chat1',
                        'description' => 'description1',
                    ]
                ]
            ]
        ]);
    }

    /**
     *Test for store chatroom functionality
     *
     * @return void
     */
    public function testStore()
    {
        Notification::fake();
        $response = $this->actingAs($this->user)->json('POST', '/chatroom', [
            'chatroom' => [
                'name' => 'chat_store',
                'description' => 'description_store',
            ],
            'type' => 'private',
            'users' => $this->users->pluck('id')
        ]);
        $response->assertJson([
            'data' => [
                'type' => 'chatrooms',
                'attributes' => [
                    'name' => 'chat_store',
                    'description' => 'description_store',
                ]
            ]
        ]);
        $this->assertDatabaseHas('chatrooms', [
            'name' => 'chat_store',
            'description' => 'description_store',
        ]);
        $this->users->prepend($this->user);
        foreach($this->users as $u){
            $this->assertDatabaseHas('chatroom_user', [
                'chatroom_id' => $response->json('data.id'),
                'user_id' => $u->id
            ]);
        }
        Notification::assertSentTo(
            $this->users,
            ChatroomCreated::class
        );
    }

    /**
     *Test for store chatroom functionality
     *
     * @return void
     */
    public function testUpdate()
    {
        Notification::fake();
        $response = $this->actingAs($this->user)->json('PUT', '/chatroom/' . $this->chatroom->id, [
            'chatroom' => [
                'name' => 'chat_2',
                'description' => 'description_2',
            ],
            'type' => 'private',
            'users' => $this->users->pluck('id')
        ]);
        $response->assertJson([
            'data' => [
                'type' => 'chatrooms',
                'attributes' => [
                    'name' => 'chat_2',
                    'description' => 'description_2',
                ]
            ]
        ]);
        $this->assertDatabaseMissing('chatrooms', [
            'name' => 'chat1',
            'description' => 'description1',
        ]);
        $this->assertDatabaseHas('chatrooms', [
            'name' => 'chat_2',
            'description' => 'description_2',
        ]);
        $this->users->prepend($this->user);
        foreach($this->users as $u){
            $this->assertDatabaseHas('chatroom_user', [
                'chatroom_id' => $response->json('data.id'),
                'user_id' => $u->id
            ]);
        }
        Notification::assertSentTo(
            $this->users,
            ChatroomUpdated::class
        );
    }

    /**
     *Test remove chatroom functionality
     *
     * @return void
     */
    public function testDestroy()
    {
        $response = $this->actingAs($this->user)->json('delete', '/chatroom/' . $this->chatroom->id);
        $response->assertStatus(204);
        $this->assertDatabaseMissing('chatroom_user', [
            'chatroom_id' => $this->chatroom->id,
            'user_id' => $this->user->id,
            'deleted_at' => null
        ]);
    }
}
