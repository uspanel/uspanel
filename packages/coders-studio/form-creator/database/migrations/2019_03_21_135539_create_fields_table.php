<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('form_id');
            $table->unsignedInteger('field_type_id');
            $table->string('name');
            $table->string('title');
            $table->string('description')->nullable();
            $table->json('options')->nullable();
            $table->text('rules')->nullable();
            $table->boolean('enabled')->default(1);
            $table->unsignedTinyInteger('ordering')->default(0);
            $table->foreign('form_id')->references('id')-> on('forms');
            $table->foreign('field_type_id')->references('id')-> on('field_types');
            $table->boolean('sortable')->default(false);
            $table->boolean('searchable')->default(false);
            $table->boolean('visible')->default(false);
            $table->boolean('exportable')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fields');
    }
}
