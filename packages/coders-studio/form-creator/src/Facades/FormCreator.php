<?php

namespace CodersStudio\FormCreator\Facades;

use Illuminate\Support\Facades\Facade;

class FormCreator extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'CodersStudio\FormCreator\FormCreator';
    }
}
