<?php

namespace CodersStudio\FormCreator;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function fields()
    {
       return $this->hasMany('CodersStudio\FormCreator\Field');
    }

    public function getRules()
    {
        return $this->fields->pluck('rules', 'name')->toArray();
    }

}
