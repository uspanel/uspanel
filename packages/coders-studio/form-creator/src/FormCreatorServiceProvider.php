<?php
namespace CodersStudio\FormCreator;

use Illuminate\Support\ServiceProvider;


class FormCreatorServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'codersstudio/form-creator');
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'codersstudio/form-creator');
        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
            $this->publishes([
                __DIR__.'/../resources/views' => $this->app->basePath('resources/views/vendor/codersstudio/form-creator'),
            ], 'form-creator-views');
            $this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/vendor/codersstudio/form-creator')
            ], 'form-creator-lang');
        }
    }

}