<?php

namespace CodersStudio\FormCreator;

use Illuminate\Database\Eloquent\Model;
use CodersStudio\FormCreator\FieldType;
use Illuminate\Database\Eloquent\SoftDeletes;

class Field extends Model
{

    use SoftDeletes;

    /**
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    protected $guarded = ['id', 'created_at', 'updated_at'];
    
    protected $appends = ['value', 'formatted_value'];

    /**
     * only enabled fields
     *
     * @param $query
     * @return mixed
     */
    public function scopeEnabled($query)
    {
        return $query->where('enabled', 1);
    }

    public function scopeVisible($query)
    {
        return $query->where('visible', 1);
    }

    /**
     * @param $value
     * @return array|mixed
     */
    public function getOptionsAttribute($value)
    {
        return $value ? json_decode($value) : [];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fieldType() {
        return $this->belongsTo(FieldType::class);
    }

    

    /* ************************ ACCESSOR ************************* */

    /**
     * Value from pivot
     * 
     * @return mixed
     */

    public function getValueAttribute() {
        $value = null;
        if($this->pivot) {
            $value = $this->pivot->value;
            if($this->fieldType->name == "checkbox"){
                $value = ($value=="true")? true: false;
            } 
        }        
        return $value;
    }

    /**
     * Formatted value from pivot
     *
     * @return mixed
     */
    public function getFormattedValueAttribute() {
        $value = $this->value;
        if($this->fieldType->name == "checkbox"){
            $value = ($value)? trans("codersstudio/form-creator::form.yes"): trans("codersstudio/form-creator::form.no");
        }     
        return $value;
    }

}
