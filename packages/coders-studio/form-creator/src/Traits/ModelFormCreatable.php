<?php

namespace CodersStudio\FormCreator\Traits;

use CodersStudio\FormCreator\Field;
use CodersStudio\FormCreator\Form;

trait ModelFormCreatable
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function fields()
    {
        return $this->morphToMany(Field::class, 'fieldable')->with('fieldType')->withPivot('value')->orderBy('ordering');
    }

    /**
     * Return list of fields with values
     *
     * @return Collection
     */
    public function getFields(){
        $formFields = self::getForm()->fields()->enabled()->orderBy('ordering')->get();
        $fields = $this->fields()->enabled()->orderBy('ordering')->get();
        $result = $formFields;
        if($fields){
            $result = $formFields->merge($fields);//->sortBy('ordering');
        }
        return $result;
    }

    /**
     * Get value of specified column
     *
     * @param $name string Column name
     * @return mixed|null
     */
    public function getField($name) {
        $item = $this->fields()->where('name','=',$name)->first();
        return $item ? $item->pivot->value : null;
    }

     /**
     * Get type of specified column
     *
     * @param $name string Column name
     * @return mixed|null
     */
    public function getFieldType($name) {
        $item = $this->fields()->where('name','=',$name)->first();
        return $item ? $item->fieldType->name : null;
    }

    /**
     * Get all fields for export
     *
     * @return array
     */
    public function exportableFields() {
        $result = [];
        $items = $this->fields()->where('exportable','=', 1)->get();
        foreach ($items as $field) {
            $selected['title'] = $field->title;
            if($field->fieldType->name == "select"){
                $selected['value'] = ($field->value && $field->options && isset($field->options->{$field->value}))?$field->options->{$field->value}:"-";
            }else if($field->fieldType->name == "checkbox"){
                $selected['value'] =  ($field->value=='true') ? trans('admin.yes_bool') : trans('admin.no_bool');
            }else {
                $selected['value'] = $field->pivot->value;
            }
            $result[] = $selected;
        }

        return $result;
    }

    /**
     * return form for this model
     *
     * @param boolean $checkVisible check field visibility
     * @return Form|Form[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public static function getForm($checkVisible = false)
    {
        if(!defined('self::FORM_ID')){
            return null;
        }
        //TODO Нужно сделать сохранение в кеш
        return Form::with([
            'fields' => function($query) use ($checkVisible) {
                $query->enabled();
                if ($checkVisible) {
                    $query->visible();
                }
                $query->orderBy('ordering');
            },
            'fields.fieldType'
        ])->find(self::FORM_ID);
    }

}
