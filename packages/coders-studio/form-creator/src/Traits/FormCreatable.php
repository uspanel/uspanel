<?php

namespace CodersStudio\FormCreator\Traits;

use CodersStudio\FormCreator\Form;

trait FormCreatable
{

    /**
     * Store custom fields to database
     *
     * @param $request
     * @param $model
     * @param Form $form
     */
    private function storeCustomFields($request, $model, Form $form)
    {
        foreach($form->fields as $field){
            if($request->has($field->name) && $field->fieldType->name != "file"){
                $value = $request->get($field->name);
                if($field->fieldType->name == "checkbox") {
                    if($value) $value = "true";
                    else 
                        $value = "false"; //TODO аксессоры и мутаторы 
                }
                $model->fields()->attach([$field->id => [
                    'value' => $value
                ]]);
            }else if($field->fieldType->name == "file"){
                $model->fields()->attach([$field->id => [
                    'value' => $this->getFileCollectionJson($model, $field->name)
                ]]);    
            } else {
                $model->fields()->attach([$field->id => [
                    'value' => null
                ]]);
            }
            
        }
    }

    /**
     * Update custom fields to database
     *
     * @param $request
     * @param $model
     * @param Form $form
     */
    private function updateCustomFields($request, $model, Form $form)
    {
        foreach($form->fields as $field){
            if($request->has($field->name) && $field->fieldType->name != "file"){
                $value = $request->get($field->name);
                if($field->fieldType->name == "checkbox") {
                    if($value) $value = "true";
                    else 
                        $value = "false"; 
                }
                $model->fields()->updateExistingPivot($field->id, [
                    'value' => $value
                ]);
            }else if($field->fieldType->name == "file"){
                $model->fields()->updateExistingPivot($field->id, [
                    'value' => $this->getFileCollectionJson($model, $field->name)
                ]);
            } else {
                $model->fields()->updateExistingPivot($field->id, [
                    'value' => null
                ]);
            }
        }
    }

    /**
     * Return file collection from media library 
     *
     * @param $model
     * @param $collectionName
     * @return json
     */
    protected function getFileCollectionJson($model, $collectionName){
        $files = $model->getMedia($collectionName);
        $value = [];
        if($files){
            foreach($files AS $file) $value[$file->id] = $file->only('id', 'file_name', 'custom_properties');
        }
        return json_encode($value);
    }

}