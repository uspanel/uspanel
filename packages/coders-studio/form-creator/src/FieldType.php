<?php

namespace CodersStudio\FormCreator;

use Illuminate\Database\Eloquent\Model;

class FieldType extends Model
{
    protected $fillable = ['name'];
}
