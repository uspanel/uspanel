<?php

namespace CodersStudio\FormCreator;

use Illuminate\Support\Collection;
use PhpParser\Node\Expr\Array_;

class FormCreator
{
    /**
     * Create form HTML
     *
     * @param Illuminate\Support\Collection $fields
     * @param Array $params
     * @param String $view
     * @return string
     */
    public function build(Collection $fields, Array $params = [], $view = "codersstudio/form-creator::form-elements"){
        return view($view, array_merge(['fields' => $fields], $params))->render();
    }
}
