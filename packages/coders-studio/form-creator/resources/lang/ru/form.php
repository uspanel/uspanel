<?php

return [
    'name' => 'Имя поля',
    'title' => 'Название поля',
    'description' => 'Описание',
    'type' => 'Тип поля',
    'hint' => 'Единица измерения',
    'ordering' => 'Порядок',
    'enabled' => 'Включен',
    'sortable' => 'Сортируемое',
    'searchable' => 'Фильтруемое',
    'visible' => 'Показывать в таблице',
    'exportable' => 'Показывать в КП',
    'rules' => 'Правила',
    'rule_list' => [
        'required' => 'Обязательное поле',
        'string' => 'Строка',
    ],
    'options' => 'Опции',
    'option' => 'Опция',
    'field' => 'Поле',
    'types' => [
        'text' => 'Текст',
        'select' => 'Список',
        'textarea' => 'Текстовое поле',
        'date' => 'Дата',
        'checkbox' => 'Переключатель',
        'file' => 'Файл',
        'number' => 'Число',

    ],
    'no' => 'Нет',
    'yes' => 'Да'

];
