<?php

return [
    'name' => 'Name',
    'title' => 'Title',
    'description' => 'Description',
    'type' => 'Type',
    'ordering' => 'Ordering',
    'enabled' => 'Enabled',
    'sortable' => 'Sortable',
    'searchable' => 'Searchable',
    'visible' => 'Visible',
    'exportable' => 'Exportable',
    'rules' => 'Rules',
    'rule_list' => [
        'required' => 'Required',
        'string' => 'String',
    ],
    'options' => 'Options',
    'option' => 'Option',
    'field' => 'Field',
    'types' => [
        'text' => 'Text',
        'select' => 'Select',
        'textarea' => 'Textarea',
        'date' => 'Date',
        'checkbox' => 'Checkbox',
        'file' => 'File',
        'number' => 'Number',
    ]
];
