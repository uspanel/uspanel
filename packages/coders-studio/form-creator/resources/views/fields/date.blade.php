<div class="form-group">
	<label
	class="@if(!empty($changed)) text-warning @endif"
	for="{{ $field->name }}">
		{{ $field->title }}
		@if ($field->description)
			<span class="text-muted"> ( {{ $field->description }} ) </span>
		@endif 
		@if (stristr($field->rules, 'required'))
			<span class="text-danger">*</span>
		@endif 
	</label>
	{{-- @if ($field->description)
		<i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="{{ $field->description }}"></i>
	@endif --}}
	<input
	@change="saveDraft()"
	name="{{ $field->name }}"
	type="date"
	ref="{{ $field->name }}"
	class="form-control
	@if ($errors->has($field->name))
	 is-invalid
 	@endif"
 	id="{{ $field->name }}"
 	value="{{ old($field->name, $field->pivot->value ?? null) }}"
 	v-model="form.{{ $field->name }}">

	@include('codersstudio/form-creator::validation', [
		'name' => $field->name
	])
</div>
