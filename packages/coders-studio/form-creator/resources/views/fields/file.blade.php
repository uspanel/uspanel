<div class="form-group">
	<label
	class="@if(!empty($changed)) text-warning @endif"
	for="{{ $field->name }}">
		{{ $field->title }}
		@if ($field->description)
			<span class="text-muted"> ( {{ $field->description }} ) </span>
		@endif 
		@if (stristr($field->rules, 'required'))
			<span class="text-danger">*</span>
		@endif	
	</label>
	
	{{-- @if ($field->description)
		<i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="{{ $field->description }}"></i>
	@endif --}}
	@php
		$params = [
			'mediaCollection' => app(App\Application::class)->getMediaCollection('app_file'),
            'label' => ''
		];
		if(isset($application)) $params['media'] = $application->getThumbs200ForCollection('app_file');
	@endphp
	@include('brackets/admin-ui::admin.includes.media-uploader', [
		'mediaCollection' => $params['mediaCollection']
	])
	
	@include('codersstudio/form-creator::validation', [
		'name' => $field->name
	])
</div>
