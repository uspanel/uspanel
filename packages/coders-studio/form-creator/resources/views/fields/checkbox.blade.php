<div class="form-group">
	<label
	class="@if(!empty($changed)) text-warning @endif"
	for="{{ $field->name }}">
		{{ $field->title }}
		@if ($field->description)
			<span class="text-muted"> ( {{ $field->description }} ) </span>
		@endif  
		@if (stristr($field->rules, 'required'))
			<span class="text-danger">*</span>
		@endif	
	</label>
	{{-- @if ($field->description)
		<i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="{{ $field->description }}"></i>
	@endif --}}  
	<div class="input-container">
		<div class="switcher switcher-primary">
			<input @change="saveDraft()" ref="{{ $field->name }}" name="{{ $field->name }}" value="1" type="checkbox" id="{{ $field->name }}"  @if (old($field->name, $field->value ?? false) == true) checked @endif v-model="form.{{ $field->name }}">
			<label for="{{ $field->name }}"></label>
		</div>
	</div>
	@include('codersstudio/form-creator::validation', [
		'name' => $field->name
	])
</div>
