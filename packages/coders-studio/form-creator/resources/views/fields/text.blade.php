<div class="
form-group
@if (isset($search))
	mb-2
@endif"
>
	<label
	class="
	@if (isset($search))
		small mb-0
	@endif
	@if (!empty($changed))
		text-warning
	@endif"
	for="{{ $field->name }}">
		{{ $field->title }}
		@if (stristr($field->rules, 'required') && !isset($search))
			<span class="text-danger">*</span>
		@endif
	</label>
	{{-- @if ($field->description)
		<i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="{{ $field->description }}"></i>
	@endif --}}
	<input
	@if (!isset($search))
		@change="saveDraft()"
	@endif
	name="{{ $field->name }}"
	type="text"
	ref="{{ $field->name }}"
	class="form-control
	@if (isset($search))
		form-control-sm
	@endif
	@if ($errors->has($field->name))
		is-invalid
	@endif"
	id="{{ $field->name }}"
	placeholder="{{ $field->description }}"
	value="{{ old($field->name, $field->pivot->value ?? null) }}"
	v-model="form.{{ $field->name }}">
	@include('codersstudio/form-creator::validation', [
		'name' => $field->name
	])
</div>
