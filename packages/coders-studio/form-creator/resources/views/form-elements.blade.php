@foreach($fields as $field)
    @include('codersstudio/form-creator::fields.'.$field->fieldType->name, [
        'field' => $field,
        'changed' => isset($applicationLogFields[$field->name]) && $applicationLogFields[$field->name]->changed,
    ])
@endforeach