@if ($errors->has($name))
	<div class="invalid-feedback">
		@foreach ($errors->get($name) as $message)
			{{ $message }}
		@endforeach
	</div>
@endif
<div v-if="errors.has('{{ $name }}')" class="form-control-feedback form-text text-danger" v-cloak>@{{ errors.first('{!! $name !!}') }}</div>