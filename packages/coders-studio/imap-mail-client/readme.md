Coders Studio Web mail client

## About

This is Laravel 5.7 package for interact with external servers via IMAP/SMTP
## Install

You need to use CodersStudio\ImapMailClient\Traits\Mailclientable in your User model.

This package uses https://github.com/Webklex/laravel-imap package for interact with imap server so IMAP related configuration is described in this package documentation.

Also package includes config you can publish by php artisan vendor:publish and tune some parameters. For example if your Users model is non-standard you can tune it.

## Usage

For fetching new mail in background package includes artisan command you can run by cron or Laravel schedule or whatever. Users for background fetching are conditionaly selected by config parameters (default all).
