<?php

Route::prefix('csmailclient')->middleware(['web','auth'])->group(function () {
    Route::post('/send', 'CodersStudio\ImapMailClient\Http\Controllers\MailClientController@send');
    Route::get('/messages', 'CodersStudio\ImapMailClient\Http\Controllers\MailClientController@getMessages');
    Route::get('/message/{id}', 'CodersStudio\ImapMailClient\Http\Controllers\MailClientController@getMessage');
    Route::get('/folders', 'CodersStudio\ImapMailClient\Http\Controllers\MailClientController@getImapFolders');
    Route::delete('/message/{id}', 'CodersStudio\ImapMailClient\Http\Controllers\MailClientController@deleteMessage');
    Route::get('/message/{id}/attachment/{att_id}', 'CodersStudio\ImapMailClient\Http\Controllers\MailClientController@getAttachment');
    Route::post('/massdestroy', 'CodersStudio\ImapMailClient\Http\Controllers\MailClientController@massDestroy');

    Route::get('/templates', 'CodersStudio\ImapMailClient\Http\Controllers\MailClientController@getTemplates');
    Route::post('/templates', 'CodersStudio\ImapMailClient\Http\Controllers\MailClientController@storeTemplate');
    Route::get('/templates/{id}', 'CodersStudio\ImapMailClient\Http\Controllers\MailClientController@getTemplate');
    Route::put('/templates/{id}', 'CodersStudio\ImapMailClient\Http\Controllers\MailClientController@updateTemplate');
    Route::delete('/templates/{id}', 'CodersStudio\ImapMailClient\Http\Controllers\MailClientController@destroyTemplate');
});
