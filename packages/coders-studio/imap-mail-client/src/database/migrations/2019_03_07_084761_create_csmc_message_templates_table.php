<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCsmcMessageTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('csmc_message_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('mailsetting_id')->nullable();
            $table->string('title');
            $table->text('body')->nullable();
            $table->text('attachments')->nullable();
            $table->timestamps();
            $table->foreign('mailsetting_id')
                ->references('id')->on('mailsettings')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csmc_message_templates');
    }
}
