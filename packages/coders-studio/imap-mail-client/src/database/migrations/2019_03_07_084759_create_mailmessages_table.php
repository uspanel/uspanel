<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailmessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mailmessages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('uid');
            $table->unsignedInteger('mailsetting_id');
            $table->string('folder');
            $table->string('subject');
            $table->string('sender');
            $table->string('sender_email');
            $table->string('recipient');
            $table->datetime('date');
            $table->boolean('seen');
            $table->text('body')->nullable();
            $table->text('attachments')->nullable();
            $table->timestamps();
            $table->foreign('mailsetting_id')
                ->references('id')->on('mailsettings')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mailmessages');
    }
}
