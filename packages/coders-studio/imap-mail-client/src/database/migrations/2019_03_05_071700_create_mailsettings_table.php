<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailsettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mailsettings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('smtp_host');
            $table->unsignedInteger('smtp_port');
            $table->string('smtp_encryption')->nullable();
            $table->string('imap_host');
            $table->unsignedInteger('imap_port');
            $table->string('imap_encryption')->nullable();
            $table->string('imap_sent_folder');
            $table->string('imap_inbox_folder');
            $table->string('login');
            $table->string('password');
            $table->boolean('default')->default(0);
            $table->boolean('validate_cert')->default(0);
            $table->timestamps();
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mailsettings');
    }
}
