<?php

namespace CodersStudio\ImapMailClient;

use CodersStudio\ImapMailClient\Console\Commands\UpdateMailMessages;
use CodersStudio\ImapMailClient\Observers\MailmessageObserver;
use Illuminate\Support\ServiceProvider;

class MailClientServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/csmailclient.php', 'csmailclient'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
        Mailmessage::observe(MailmessageObserver::class);

        $this->publishes([
            __DIR__.'/config/csmailclient.php' => config_path('csmailclient.php'),
        ]);
        $this->commands([
            UpdateMailMessages::class,
        ]);
    }
}
