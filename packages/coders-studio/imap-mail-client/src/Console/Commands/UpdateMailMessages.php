<?php

namespace CodersStudio\ImapMailClient\Console\Commands;

use Illuminate\Console\Command;
use CodersStudio\ImapMailClient\Traits\Mailconnectable;

class UpdateMailMessages extends Command
{
    use Mailconnectable;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'csmail:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch new mail from remote server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->isCli = true;
        $userModel = config('csmailclient.user_model');
        $users = new $userModel;
        if (\config('csmailclient.check_new_mail_field')) {
            $users = $users->whereRaw(config('csmailclient.check_new_mail_field').trim(config('csmailclient.check_new_mail_condition')));
        }
        $users = $users->get();
        foreach ($users as $user) {
            $this->user = $user;
            foreach ($user->mailsettings as $mailsetting) {
                $data['set_id'] = $mailsetting->id;
                $data['folder'] =  $mailsetting->imap_inbox_folder;
                $this->updateMessages($data);
            }
        }
    }
}
