<?php

namespace CodersStudio\ImapMailClient\Traits;

trait Mailclientable
{
    protected $nameAttribute = '';
    /**
     * Relation for local Users model
     *
     * @return mixed
     */
    public function mailsettings()
    {
        return $this->hasMany('CodersStudio\ImapMailClient\Mailsetting');
    }

    public function getNameAttribute()
    {
        return (!empty($this->attributes['first_name']) || !empty($this->attributes['last_name'])) ? ($this->attributes['first_name'].' '.$this->attributes['last_name']) : ((!empty($this->attributes['name'])) ? $this->attributes['name'] : $this->nameAttribute);
    }
}
