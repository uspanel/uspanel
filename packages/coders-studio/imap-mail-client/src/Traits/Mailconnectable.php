<?php

namespace CodersStudio\ImapMailClient\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Webklex\IMAP\Client;
use Illuminate\Support\Facades\Auth;

trait Mailconnectable
{
    protected $mailsettings;
    protected $isCli = false;
    protected $user;

    /**
     * Set chosen or default mail settings set
     *
     * @param null $id integer ID of settings set
     */
    protected function setSettings($id = null)
    {
        if (!$this->isCli) {
            $settings = Auth::user()->mailsettings();
        } else {
            $settings = $this->user->mailsettings();
        }
        if ($id) {
            $settings = $settings->where('id', '=', $id)->firstOrFail();
        } else {
            $settings = $settings->where('default', '=', 1)->firstOrFail();
        }
        $this->mailsettings = $settings;
    }

    /**
     * Connect to IMAP server with given user credentials
     *
     * @return Client
     * @throws \Webklex\IMAP\Exceptions\ConnectionFailedException
     */
    protected function connectImap()
    {

        $client = new Client([
            'host' => $this->mailsettings->imap_host,
            'port' => $this->mailsettings->imap_port,
            'encryption' => $this->mailsettings->imap_encryption,
            'validate_cert' => $this->mailsettings->validate_cert,
            'username' => $this->mailsettings->login,
            'password' => $this->mailsettings->plain_password,
            'protocol' => 'imap',
        ]);

        $client->connect();

        return $client;
    }

    /**
     * Update messages from IMAP server to local DB
     *
     * @param Array $rData parameters data
     * @return bool
     * @throws \Webklex\IMAP\Exceptions\ConnectionFailedException
     * @throws \Webklex\IMAP\Exceptions\GetMessagesFailedException
     * @throws \Webklex\IMAP\Exceptions\InvalidWhereQueryCriteriaException
     */
    protected function updateMessages($rData)
    {


        $this->setSettings($rData['set_id']);
        $folder = $rData['folder'];
        $mailmessages = $this->mailsettings->mailmessages()->where('folder', '=', $folder);
        $criteria = 'ALL';
        if ($lastMessage = $mailmessages->orderBy('date', 'desc')->first()) {
            $criteria = [['SINCE', Carbon::parse($lastMessage->date)]];
        }
        $oClient = $this->connectImap();



        $folders = [];
        if (config('csmailclient.fetch_mail_on_query') == false) {
            $folders[] = $folder;
        } else {
            $aFolders = $oClient->getFolders();
            foreach ($aFolders as $aFolder) {
                $folders[] = $aFolder->name;
            }
        }
        foreach ($folders as $folder) {
            $aFolder = $oClient->getFolder($folder);
            $aMessage = $aFolder->getMessages($criteria, 1, false, false, true, null, 1);
            foreach ($aMessage as $message) {
                try {
                    if (!$this->mailsettings->mailmessages()->where('uid', '=', $message->getUid())->where('folder', '=', $folder)->exists()) {
                        $this->mailsettings->mailmessages()->create([
                            'subject' => $this->decodeContent($message->getSubject()),
                            'sender' => !empty($message->getFrom()[0]) ? $this->decodeContent($message->getFrom()[0]->full) : '',
                            'sender_email' => !empty($message->getFrom()[0]) ? $message->getFrom()[0]->mail : '',
                            'recipient' => !empty($message->getTo()[0]) ? $this->decodeContent($message->getTo()[0]->full) : '',
                            'seen' => $message->getFlags()['seen'],
                            'date' => $message->getDate(),
                            'folder' => $folder,
                            'uid' => $message->getUid()
                        ]);
                    }
                } catch (\Exception $e) {
                    Log::error('csmailclient: '.$e->getMessage());
                }

            }
        }
        return true;
    }

    /**
     * Try to decode content to proper UTF8 format
     *
     * @param $content string
     * @return bool|false|string|string[]|null
     */
    protected function decodeContent($content)
    {
        $result = !empty(imap_mime_header_decode ($content)[0]) ? imap_mime_header_decode ($content)[0]->text : (!empty($content) ? 'Unable to decode!' : '');
        $result = mb_convert_encoding($result, 'UTF-8', mb_detect_encoding($result, 'ASCII,UTF-8,ISO-8859-15'));
        return $result;
    }
}
