<?php

namespace CodersStudio\ImapMailClient\Http\Controllers;

use App\User;
use Carbon\Carbon;
use CodersStudio\ImapMailClient\CsmcMessageTemplate;
use CodersStudio\ImapMailClient\Http\Requests\MailClientSendRequest;
use CodersStudio\ImapMailClient\Http\Requests\SaveTemplateRequest;
use CodersStudio\ImapMailClient\Http\Requests\UpdateTemplateRequest;
use CodersStudio\ImapMailClient\Mailmessage;
use CodersStudio\ImapMailClient\Mailsetting;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use CodersStudio\ImapMailClient\Traits\Mailconnectable;
use phpDocumentor\Reflection\Types\Integer;
use Illuminate\Support\Facades\Log;

class MailClientController extends Controller
{
    use Mailconnectable;

    /**
     * Set mail settings from request data to perform some operations
     *
     * @param Request $request
     */
    protected function setSettingsFromRequest(Request $request)
    {
        $mailsetting = new Mailsetting();
        $mailsetting->smtp_host = $request->get('smtp_host', '');
        $mailsetting->smtp_port = $request->get('smtp_port', '');
        $mailsetting->smtp_encryption = $request->get('smtp_encryption', '');
        $mailsetting->imap_host = $request->get('imap_host', '');
        $mailsetting->imap_port = $request->get('imap_port', '');
        $mailsetting->imap_encryption = $request->get('imap_encryption', '');
        $mailsetting->login = $request->get('login', '');
        $mailsetting->password = $request->get('password', '');
        $mailsetting->validate_cert = false;
        $this->mailsettings = $mailsetting;
    }

    /**
     * Send message with authenticated user's settings
     *
     * @param MailClientSendRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function send(MailClientSendRequest $request)
    {

        $this->setSettings($request->get('set_id', null));

        if ($this->mailsettings->owner) {
            $name = User::findOrFail($this->mailsettings->owner)->name;
        } else {
            $name = $this->mailsettings->user->name;
        }

        $config = array(
            'driver' => 'smtp',
            'host' => $this->mailsettings->smtp_host,
            'port' => $this->mailsettings->smtp_port,
            'from' => array('address' => $this->mailsettings->login, 'name' => $name),
            'encryption' => $this->mailsettings->smtp_encryption,
            'username' => $this->mailsettings->login,
            'password' => $this->mailsettings->plain_password,
            'sendmail' => '/usr/sbin/sendmail -bs',
            'pretend' => false,
        );
        Config::set('mail', $config);

        $msgId = $request->get('message_id', null);
        $folder = $request->get('folder', null);
        $locMsg = null;
        $fwdApp = null;
        if ($msgId) {
            $locMsg = $this->mailsettings->mailmessages()
                ->where('uid', '=', $msgId)
                ->where('folder', '=', $folder)
                ->firstOrFail();
            $fwdApp = preg_replace('/\[sender\]/', htmlentities($locMsg->sender), config('csmailclient.forwarding_app'));
        }
        foreach ($request->get('recipients') as $recipient) {
            try {
                Mail::send([], [], function ($message) use ($request, $recipient, $locMsg, $fwdApp) {
                    $message->to($recipient)
                        ->subject(($locMsg ? 'Fwd: ' : '') . $request->get('subject'))
                        ->setBody(($locMsg ? $fwdApp : '') . $request->get('message'), 'text/html');
                    if ($atts = $this->extractAttachments($request)) {
                        foreach ($atts as $attachment) {
                            $message->attach($attachment['pathname'], [
                                'as' => $attachment['filename'],
                                'mime' => $attachment['mime']
                            ]);
                        }
                    }
                });
                $this->moveToSent($recipient, $request, $fwdApp);
            } catch (\Exception $e) {
                if ($request->ajax()) {
                    return Response::json(['errors' => [$e->getMessage()]], 400);
                }

                return Redirect::back()->withErrors(['error', $e->getMessage()]);
            }
        }
        if ($request->ajax()) {
            return Response::json(['data' => []]);
        }

        return Redirect::back();

    }

    /**
     * Extract attachments array from message or upload request
     *
     * @param Request $request
     * @param Integer $template Id of template to grab attachments
     * @return array
     * @throws \Webklex\IMAP\Exceptions\ConnectionFailedException
     * @throws \Webklex\IMAP\Exceptions\GetMessagesFailedException
     * @throws \Webklex\IMAP\Exceptions\InvalidWhereQueryCriteriaException
     * @throws \Webklex\IMAP\Exceptions\MessageSearchValidationException
     */
    protected function extractAttachments($request)
    {
        $result = [];
        $files = $request->file('attachments');
        if ($files) {
            foreach ($files as $file) {
                $item['pathname'] = $file->getPathname();
                $item['filename'] = $file->getClientOriginalName();
                $item['mime'] = $file->getMimeType();
                $result[] = $item;
            }
        }
        else if ($request->get('tmplid') && $request->get('tmplatt')) {
            $id = $request->get('tmplid');
            $template = CsmcMessageTemplate::findOrFail($id);
            $folder = config('csmailclient.file_storage','csmc_templates');
            $storage = storage_path('app/public/'.$folder.'/'.$id);
            $atts = $template->attachments ? json_decode($template->attachments, 1) : [];
            foreach ($atts as $att) {
                $item['pathname'] = $storage.'/'.$att['name'];
                $item['filename'] = $att['name'];
                $item['mime'] = mime_content_type($storage.'/'.$att['name']);
                $result[] = $item;
            }
        }
        if ($request->get('set_id') && $request->get('folder') && $request->get('message_id')) {
            $attachments = $this->fetchAttachments($request->get('set_id'), $request->get('message_id'), $request->get('folder'));
            foreach ($attachments as $attachment) {
                $pathname = '/tmp/' . uniqid();
                file_put_contents($pathname, $attachment['content']);
                $item['pathname'] = $pathname;
                $item['filename'] = $attachment['filename'];
                $item['mime'] = $attachment['content_type'];
                $result[] = $item;
            }
        }
        return $result;
    }

    /**
     * Create message copy in Sent folder via IMAP
     *
     * @param $recipient string Recipient email
     * @param $request Request
     * @param $fwdApp string Forwarping body appendix
     * @throws \Webklex\IMAP\Exceptions\ConnectionFailedException
     */
    protected function moveToSent($recipient, $request, $fwdApp = null)
    {

        $oClient = $this->connectImap();
        $date = date('d-M-Y H:i:s O');
        $boundary1 = "###" . md5(microtime()) . "###";
        $boundary2 = "###" . md5(microtime() . rand(99, 999)) . "###";

        $msg = "From: ".$this->mailsettings->user->name." <" . $this->mailsettings->login . ">\r\n"
            . "To: " . $recipient . "\r\n"
            . "Date: $date\r\n"
            . "Subject: " . ($fwdApp ? 'Fwd: ' : '') . quoted_printable_encode($request->get('subject')) . "\r\n"
            . "MIME-Version: 1.0\r\n"
            . "Content-Type: multipart/mixed; boundary=\"$boundary1\"\r\n"
            . "\r\n\r\n"
            . "--$boundary1\r\n"
            . "Content-Type: multipart/alternative; boundary=\"$boundary2\"\r\n"
            . "\r\n\r\n"
            . "--$boundary2\r\n"
            . "Content-Type: text/html; charset=\"utf-8\"\r\n"
            . "Content-Transfer-Encoding: quoted-printable \r\n"
            . "\r\n\r\n"
            . ($fwdApp ? $fwdApp : '')
            . html_entity_decode($request->get('message')) . "\r\n"
            . "\r\n\r\n"
            . "--$boundary2\r\n"
            . "\r\n\r\n";
        // ADD attachment(s)
        if ($atts = $this->extractAttachments($request)) {
            foreach ($atts as $attachment) {
                $attContent = chunk_split(base64_encode(file_get_contents($attachment['pathname'])));
                $origName = $attachment['filename'];
                $msg .=
                    "--$boundary1\r\n"
                    . "Content-Type: " . $attachment['mime'] . "; name=\"$origName\"\r\n"
                    . "Content-Transfer-Encoding: base64\r\n"
                    . "Content-Disposition: attachment; filename=\"$origName\"\r\n"
                    . "\r\n\r\n"
                    . $attContent
                    . "\r\n\r\n";
            };
        }
        $msg .= "--$boundary1--\r\n\r\n";

        $aFolder = $oClient->getFolder($this->mailsettings->imap_sent_folder);
        $aFolder->appendMessage($msg, '\Seen', $date);
    }

    /**
     * Get all messages from folder (default from INBOX)
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Webklex\IMAP\Exceptions\ConnectionFailedException
     * @throws \Webklex\IMAP\Exceptions\GetMessagesFailedException
     * @throws \Webklex\IMAP\Exceptions\InvalidWhereQueryCriteriaException
     */
    public function getMessages(Request $request)
    {

        try {
            $originalReq = $request;
            if (config('csmailclient.fetch_mail_on_query')) {
                foreach (Auth::user()->mailsettings as $mailsetting) {

                    try {
                        $this->updateMessages(['set_id' => $mailsetting->id, 'folder' => $originalReq->get('folder', $mailsetting->imap_inbox_folder)]);
//                        $this->updateMessages(['set_id' => $mailsetting->id, 'folder' => 'Sent']);
                    } catch (\Exception $e) {
                        //TODO make some exception handling
                    }
                }
            }
            $this->setSettings($originalReq->get('set_id', null));
            $folder = $originalReq->get('folder', $this->mailsettings->imap_inbox_folder);
            $addressee = $originalReq->get('addressee',null);
            $messages = $this->mailsettings
                ->mailmessages();
            if ($addressee) {
                $messages->where(function($query){
                    $query->where('folder', '=', $this->mailsettings->imap_inbox_folder)
                        ->orWhere('folder', '=', $this->mailsettings->imap_sent_folder);
                })->where(function($query) use ($addressee){
                    $query->where('recipient', 'LIKE', '%' . $addressee . '%')
                        ->orWhere('sender', 'LIKE', '%' . $addressee . '%');
                });

            } else {
                $messages->where('folder', '=', $folder);
            }
            if ($cond = $originalReq->get('search')) {
                $messages->where('recipient', 'LIKE', '%' . $cond . '%')
                    ->orWhere('sender', 'LIKE', '%' . $cond . '%')
                    ->orWhere('subject', 'LIKE', '%' . $cond . '%');
            }
            if ($ord = $originalReq->get('orderBy')) {
                $messages->orderBy($ord, $originalReq->get('orderDirection', 'asc'));
            } else {
                $messages->orderBy('date', 'desc');
            }
            $messages = $messages->paginate($originalReq->get('per_page', 10));
            return Response::json($messages);
        } catch (\Exception $e) {
            return Response::json(['errors' => $e->getMessage()], 400);
        }
    }

    /**
     * Get specific message by UID
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMessage(Request $request, $id = 0)
    {
        $this->setSettings($request->get('set_id', null));
        try {
            $folder = $request->get('folder', $this->mailsettings->imap_inbox_folder);
            $locMsg = $this->mailsettings->mailmessages()
                ->where('uid', '=', $id)
                ->where('folder', '=', $folder)
                ->first();
            $data = [];
            if (empty($locMsg->body)) {
                $oClient = $this->connectImap();
                $aFolder = $oClient->getFolder($folder);
                $aFolder->getMessages([['SUBJECT', $locMsg->subject], ['FROM', $locMsg->sender]]);
                $message = $aFolder->getMessage($id, null, null, true, true, true);
                if ($message) {
                    $message->setFlag('seen');
                    $att = [];
                    if ($message->hasAttachments()) {
                        $attachments = $message->getAttachments();
                        foreach ($attachments as $key => $attachment) {
                            $item['id'] = $key;
                            $item['name'] = $attachment->getName();
                            $att[] = $item;
                        }
                    }
                    $locMsg->update([
                        'seen' => 1,
                        'body' => $message->getHTMLBody(true) ? $message->getHTMLBody(true) : $message->getTextBody(),
                        'attachments' => json_encode($att)
                    ]);
                }
            }
            $data = [
                'type' => 'message',
                'id' => $id,
                'attributes' => [
                    'subject' => $locMsg->subject,
                    'sender' => $locMsg->sender,
                    'recipient' => $locMsg->recipient,
                    'sender_email' => $locMsg->sender_email,
                    'message' => $locMsg->body,
                    'attachments' => json_decode($locMsg->attachments)
                ]
            ];

            return Response::json([
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return Response::json(['errors' => $e->getMessage()], 400);
        }
    }


    /**
     * Get list of IMAP folders
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getImapFolders(Request $request)
    {
        try {
            if ($request->get('force_settings')) {
                $this->setSettingsFromRequest($request);
            } else {
                $this->setSettings($request->get('set_id', null));
            }
            $oClient = $this->connectImap();
            $aFolders = $oClient->getFolders();
            $data = [];
            foreach ($aFolders as $key => $folder) {
                $item['type'] = 'folder';
                $item['id'] = $key;
                $item['attributes'] = [
                    'name' => $folder->name,
                    'unseen' => $this->mailsettings->mailmessages()
                        ->where('folder', '=', $folder->name)
                        ->where('seen', '=', 0)
                        ->count()
                ];
                $data[] = $item;
            }
            return Response::json(['data' => $data]);
        } catch (\Exception $e) {
            return Response::json(['errors' => $e->getMessage()], 400);
        }
    }

    /**
     * Delete specified message
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteMessage(Request $request, $id)
    {
        try {
            $this->setSettings($request->get('set_id', null));
            $oClient = $this->connectImap();
            $aFolder = $oClient->getFolder($request->get('folder', $this->mailsettings->imap_inbox_folder));
            $message = $aFolder->getMessage($id, null, null, false, false, true);
            if ($message) {
                $message->delete();
                $this->mailsettings->mailmessages()
                    ->where('uid', '=', $id)
                    ->where('folder', '=', $request->get('folder', $this->mailsettings->imap_inbox_folder))
                    ->delete();
            }
            return Response::json(['data' => []]);
        } catch (\Exception $e) {
            return Response::json(['errors' => $e->getMessage()], 400);
        }
    }

    /**
     * Download specific attachment of specific message
     *
     * @param Request $request
     * @param $id integer Message ID
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function getAttachment(Request $request, $id, $att_id)
    {
        try {
            $attachments = $this->fetchAttachments($request->get('set_id', null), $id, $request->get('folder'));
            $requestedAtt = '';
            if (isset($attachments[$att_id])) {
                $requestedAtt = $attachments[$att_id]['content'];
                $headers = array(
                    'Content-Type' => $attachments[$att_id]['content_type'],
                    'Content-Disposition' => 'attachment; filename="' . $attachments[$att_id]['filename'] . '"',
                );
            }
            return Response::make($requestedAtt, 200, $headers);
        } catch (\Exception $e) {
            return Response::json(['errors' => $e->getMessage()], 400);
        }
    }

    /**
     * Fetch all message attachments from mail server
     *
     * @param $setId integer ID of settings set
     * @param $messageId string Message id
     * @param $folder string Message folder
     * @return array
     * @throws \Webklex\IMAP\Exceptions\ConnectionFailedException
     * @throws \Webklex\IMAP\Exceptions\GetMessagesFailedException
     * @throws \Webklex\IMAP\Exceptions\InvalidWhereQueryCriteriaException
     * @throws \Webklex\IMAP\Exceptions\MessageSearchValidationException
     */
    protected function fetchAttachments($setId, $messageId, $folder)
    {
        $result = [];
        $this->setSettings($setId);
        $oClient = $this->connectImap();
        $aFolder = $oClient->getFolder($folder ? $folder : $this->mailsettings->imap_inbox_folder);
        $locMes = $this->mailsettings->mailmessages()
            ->where('uid', '=', $messageId)
            ->where('folder', '=', $folder)
            ->firstOrFail();
        $aFolder->getMessages([['SUBJECT', $locMes->subject], ['FROM', $locMes->sender]]);
        $message = $aFolder->getMessage($messageId, null, null, true, true, true);
        if ($message) {
            if ($message->hasAttachments()) {
                $attachments = $message->getAttachments();
                foreach ($attachments as $key => $attachment) {
                    $result[$key] = [
                        'content_type' => $attachment->getContentType(),
                        'filename' => $attachment->getName(),
                        'content' => $attachment->getContent()
                    ];
                }
            }
        }
        return $result;
    }

    /**
     * Mass delete messages listed in ids parameter
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function massDestroy(Request $request)
    {
        try {
            $this->setSettings($request->get('set_id', null));
            $oClient = $this->connectImap();
            $aFolder = $oClient->getFolder($request->get('folder', $this->mailsettings->imap_inbox_folder));
            foreach ($request->get('ids', []) as $id) {
                $message = $aFolder->getMessage($id, null, null, false, false, true);
                if ($message) {
                    $message->delete();
                    $this->mailsettings->mailmessages()
                        ->where('uid', '=', $id)
                        ->where('folder', '=', $request->get('folder', $this->mailsettings->imap_inbox_folder))
                        ->delete();
                }
            }
            return Response::json(['data' => []]);
        } catch (\Exception $e) {
            return Response::json(['errors' => $e->getMessage()], 400);
        }
    }

    /**
     * Save template for message
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeTemplate(SaveTemplateRequest $request)
    {
        $data = [
            'title' => $request->get('title'),
            'body' => $request->get('body'),
        ];
        if (!$request->get('public')) {
            $this->setSettings($request->get('set_id', null));
            $template = $this->mailsettings->templates()->create($data);
        } else {
            $template = CsmcMessageTemplate::create($data);
        }
        $this->tmplAttach($template, $request);
        return Response::json(['data' => []]);
    }


    /**
     * Get template by specified id
     *
     * @param Request $request
     * @param $id integer Template Id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTemplate(Request $request, $id)
    {
        $this->setSettings($request->get('set_id', null));
        $ms = $this->mailsettings;
        $template = CsmcMessageTemplate::where(function ($query) use ($ms){
            $query->where('mailsetting_id', '=', $ms->id)
                ->orWhereNull('mailsetting_id');
        })
            ->where('id', '=', $id)
            ->firstOrFail();
        return Response::json([
            'type' => 'template',
            'id' => $template->id,
            'attributes' => [
                'title' => $template->title,
                'body' => $template->body,
                'attachments' => json_decode($template->attachments,1)
            ]
        ]);
    }

    /**
     * Update specified template
     *
     * @param Request $request
     * @param $id integer Template Id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTemplate(UpdateTemplateRequest $request, $id)
    {
        $data = [
            'title' => $request->get('title'),
            'body' => $request->get('body'),
        ];
        if (!$request->get('public')) {
            $this->setSettings($request->get('set_id', null));
            $template = $this->mailsettings->templates()->where('id', '=', $id)->firstOrFail();
            $template->update($data);
        } else {
            $template = CsmcMessageTemplate::updateOrCreate(['id' => $id], $data);
        }
        $this->tmplAttach($template, $request);
        return Response::json(['data' => []]);
    }

    /**
     * Store and attach attachments to saved/updated template
     *
     * @param $template
     * @param $request
     * @throws \Webklex\IMAP\Exceptions\ConnectionFailedException
     * @throws \Webklex\IMAP\Exceptions\GetMessagesFailedException
     * @throws \Webklex\IMAP\Exceptions\InvalidWhereQueryCriteriaException
     * @throws \Webklex\IMAP\Exceptions\MessageSearchValidationException
     */
    protected function tmplAttach($template, $request)
    {
        $folder = config('csmailclient.file_storage','csmc_templates');
        $tmplAtts = [];
        $currentAtts = $template->attachments ? json_decode($template->attachments,1) : [];
        $storage = storage_path('app/public/'.$folder.'/'.$template->id);
        if ($atts = $this->extractAttachments($request)) {
            if (!file_exists($storage)) {
                mkdir($storage, 0777, true);
            }
            foreach ($atts as $attachment) {
                rename($attachment['pathname'],$storage.'/'.$attachment['filename']);
                $tmplAtts[] = ['name' => $attachment['filename']];
                foreach ($currentAtts as $key=>$file) {
                    if ($file['name'] == $attachment['filename']) {
                        unset($currentAtts[$key]);
                    }
                }

            }
        }
        $template->update(['attachments' => json_encode($tmplAtts)]);
        if (!empty($currentAtts)) {
            foreach ($currentAtts as $file) {
                unlink($storage.'/'.$file['name']);
            }
        }
    }

    /**
     * Destroy non-public template
     *
     * @param Request $request
     * @param $id Template id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyTemplate(Request $request, $id)
    {
        $this->setSettings($request->get('set_id', null));
        $template = $this->mailsettings->templates()->where('id', '=', $id)->firstOrFail();
        $folder = config('csmailclient.file_storage','csmc_templates');
        $storage = storage_path('app/public/'.$folder.'/'.$template->id);
        $currentAtts = $template->attachments ? json_decode($template->attachments,1) : [];
        if (!empty($currentAtts)) {
            foreach ($currentAtts as $file) {
                unlink($storage.'/'.$file['name']);
            }
        }
        $template->delete();
        return Response::json(['data' => []]);
    }

    /**
     * Get all avaliable templates
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTemplates(Request $request)
    {
        $this->setSettings($request->get('set_id', null));
        $ms = $this->mailsettings;
        $templates = CsmcMessageTemplate::where(function ($query) use ($ms){
            $query->where('mailsetting_id', '=', $ms->id)
                ->orWhereNull('mailsetting_id');
        })->get();
        $data = [];
        foreach ($templates as $key => $template) {
            $item['type'] = 'template';
            $item['id'] = $template->id;
            $item['attributes'] = [
                'title' => $template->title,
                'body' => $template->body,
                'mailsetting_id' => $template->mailsetting_id,
                'attachments' => json_decode($template->attachments,1)
            ];
            $data[] = $item;
        }
        return Response::json($data);
    }

}
