<?php

namespace CodersStudio\ImapMailClient\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MailClientSendRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'recipients.*' => 'required|email',
            'subject' => 'required',
            'message' => 'required',
            'set_id' => 'nullable|numeric',
            'user_id' => 'nullable',
        ];
    }
}
