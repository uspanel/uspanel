<?php

namespace CodersStudio\ImapMailClient\tests;

use CodersStudio\ImapMailClient\Mailsetting;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class MailClientTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Create mail settings
     *
     * @return void
     */
    public function testCreateSettings()
    {

        $user = User::create([
            'name' => 'Testing name',
            'email' => 'some@email.com',
            'password' => '123',
        ]);

        $mailsetting = new Mailsetting();
        $mailsetting->fill([
            'smtp_host' => env('CS_SMTP_HOST'),
            'smtp_port' => env('CS_SMTP_PORT'),
            'smtp_encryption' => env('CS_SMTP_ENC'),
            'imap_host' => env('CS_IMAP_HOST'),
            'imap_port' => env('CS_IMAP_PORT'),
            'imap_encryption' => env('CS_IMAP_ENC'),
            'login' => env('CS_LOGIN'),
            'password' => env('CS_PASSWORD'),
            'imap_sent_folder' => env('CS_SENT_FOLDER'),
            'imap_inbox_folder' => env('CS_INBOX_FOLDER'),
            'default' => 1
        ]);
        $user->mailsettings()->save($mailsetting);

        $this->assertDatabaseHas('mailsettings',[
            'user_id' => $user->id,
            'smtp_encryption' => env('CS_SMTP_ENC'),
        ]);

//        $response = $this->actingAs($user)->ajax('POST','/csmailclient/send',['recipient' => 'recipient@mail.com','subject' => 'test message', 'message' => '<h1>Hello!</h1>']);
//        $response = $this->actingAs($user)->ajax('GET','/csmailclient/messages');
//        $response = $this->actingAs($user)->ajax('GET','/csmailclient/message/4214');
//        $response = $this->actingAs($user)->ajax('GET','/csmailclient/folders');
//        $response = $this->actingAs($user)->ajax('DELETE','/csmailclient/message/4210');
//        $response = $this->actingAs($user)->ajax('GET','/csmailclient/message/4214/attachment/f_jsx63o0i0');
    }
}
