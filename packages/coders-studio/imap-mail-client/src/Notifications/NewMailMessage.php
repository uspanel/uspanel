<?php

namespace CodersStudio\ImapMailClient\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class NewMailMessage extends Notification
{
    use Queueable;

    private $newMail;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($newMail)
    {
        $this->newMail = $newMail;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast'];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'newmail' => $this->newMail,
        ]);
    }
}
