<?php

return [
    /**
     * User model with namespace
     */
    'user_model' => 'App\User',

    /**
     * Notification class for new mail
     */
    'notification' => 'CodersStudio\ImapMailClient\Notifications\NewMailMessage',

    /**
     * Field name in users table for conditional mail update in background
     * can be null (all users) or name of existing field
     */
    'check_new_mail_field' => null,

    /**
     * Condition for field check_new_mail_field to be updated in background
     * can be null or SQL query string ex. "= 'active'" or "IS NULL" etc
     */
    'check_new_mail_condition' => null,

    /**
     * Check new mail on remote server every time user opens folder in UI.
     * Boolean. If set to false you have to tune some background mail fetching or mail never comes.
     *
     */
    'fetch_mail_on_query' => true,

    /**
     * Forwarding appendix. Will prepend forwarded message. Concat with original sender
     *
     *
     */
    'forwarding_app' => '<p> Original sender: [sender] </p>',

    /**
     * Folder name for storing files attached to mail templates (in storage folder)
     */
    'file_storage' => 'csmc_templates'
];
