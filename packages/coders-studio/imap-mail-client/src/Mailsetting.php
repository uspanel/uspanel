<?php

namespace CodersStudio\ImapMailClient;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Mailsetting extends Model
{
    protected $guarded = ['id'];

    protected $hidden = ['password'];

    /**
     * Get plain password from encrypted
     *
     * @return string
     */
    public function getPlainPasswordAttribute()
    {
        return Crypt::decrypt($this->attributes['password']);
    }

    /**
     * Store encrypted passwords
     *
     * @param $value plain text password
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Crypt::encrypt($value);
    }

    public function mailmessages()
    {
        return $this->hasMany('CodersStudio\ImapMailClient\Mailmessage');
    }

    public function templates()
    {
        return $this->hasMany('CodersStudio\ImapMailClient\CsmcMessageTemplate');
    }

    public function user()
    {
        return $this->belongsTo(config('csmailclient.user_model','App\User'));
    }
}
