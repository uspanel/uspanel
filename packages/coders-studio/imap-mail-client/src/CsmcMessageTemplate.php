<?php

namespace CodersStudio\ImapMailClient;

use Illuminate\Database\Eloquent\Model;

class CsmcMessageTemplate extends Model
{
    protected $guarded = ['id'];

    public function mailsetting()
    {
        return $this->belongsTo('CodersStudio\ImapMailClient\Mailsetting');
    }

    public function setBodyAttribute($value)
    {
        $this->attributes['body'] = preg_replace(['/<pre>/','/<\/pre>/'],'',$value);
    }
}
