<?php

namespace CodersStudio\ImapMailClient\Observers;

use CodersStudio\ImapMailClient\Mailmessage;
use CodersStudio\ImapMailClient\Notifications\NewMailMessage;

class MailmessageObserver
{
    /**
     * Handle the mailmessage "created" event.
     *
     * @param  \App\Mailmessage  $mailmessage
     * @return void
     */
    public function created(Mailmessage $mailmessage)
    {
        if (!$mailmessage->seen) {
            $mailmessage->mailsetting->user->notify(new NewMailMessage($mailmessage));
        }
    }

    /**
     * Handle the mailmessage "updated" event.
     *
     * @param  \App\Mailmessage  $mailmessage
     * @return void
     */
    public function updated(Mailmessage $mailmessage)
    {
        //
    }

    /**
     * Handle the mailmessage "deleted" event.
     *
     * @param  \App\Mailmessage  $mailmessage
     * @return void
     */
    public function deleted(Mailmessage $mailmessage)
    {
        //
    }

    /**
     * Handle the mailmessage "restored" event.
     *
     * @param  \App\Mailmessage  $mailmessage
     * @return void
     */
    public function restored(Mailmessage $mailmessage)
    {
        //
    }

    /**
     * Handle the mailmessage "force deleted" event.
     *
     * @param  \App\Mailmessage  $mailmessage
     * @return void
     */
    public function forceDeleted(Mailmessage $mailmessage)
    {
        //
    }
}
