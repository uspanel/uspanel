export default {
	data() {
        return {
            messages: [],
        }
    },
    props: [
        'id'
    ],
    methods: {
	    getData: function(){
	        axios.get("/users/"+this.id+"/notifications")
	        .then((response) => {
	            this.messages = response.data.data;
	        })
	        .catch(function (error) {
	            console.log(error);
	        });
	    },
	    setRead: function(messageId, link){
	        axios.patch("/users/"+this.id+"/notifications/"+messageId)
	        .then((response) => {
	        	if (link) {
	        		document.location.href = link;
	        	}
	        })
	        .catch(function (error) {
	            console.log(error);
	        });
		},
	    setReadAll: function(){
	        axios.patch("/users/"+this.id+"/notifications/read-all")
	        .then((response) => {
				this.getData();
	        })
	        .catch(function (error) {
	            console.log(error);
	        });
	    }


	},
}