import AppListing from '../app-components/Listing/AppListing';

Vue.component('warehousable-item-listing', {
    mixins: [AppListing],
    filters: {
	  	type: function (value) {
		    if (_.includes(_.toLower(value), 'salestask')) return 'Sales Task';
		    if (_.includes(_.toLower(value), 'task')) return 'Task';
		    if (_.includes(_.toLower(value), 'package')) return 'Package';
		},
		typeLink: function (value, id) {
			console.log(value, id)
			if (_.includes(_.toLower(value), 'salestask')) return '/admin/sales-tasks/' + id;
		    if (_.includes(_.toLower(value), 'task')) return '/admin/tasks/' + id;
		    if (_.includes(_.toLower(value), 'package')) return '/admin/packages/' + id;
		}
	}
});