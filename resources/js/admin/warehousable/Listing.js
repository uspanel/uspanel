import AppListing from '../app-components/Listing/AppListing';

Vue.component('warehousable-listing', {
    mixins: [AppListing]
});