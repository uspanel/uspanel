import AppListing from '../app-components/Listing/AppListing';
import TableUpload from '../app-components/Listing/TableUpload';

Vue.component('invest-task-listing', {
    mixins: [AppListing, TableUpload],
    props: {
        invest_task_statuses: {
            default: () => {
                return []
            }
        },
    },
    methods:{
        restoreItem(item){
            axios.post(item.resource_url + '/restore', {id: item.id})
                .then(() => {
                    this.$notify({ type: 'success', title: 'Success', text: response.data.message });
                    this.$modal.hide(type + '_upload');
                    this.loadData();
                })
                .catch(error => {
                    console.log(error);
                });
        }
    }
});
