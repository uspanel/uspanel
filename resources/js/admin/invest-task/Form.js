import AppForm from '../app-components/Form/AppForm';

Vue.component('invest-task-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                name: this.taskTemplate.name ? this.taskTemplate.name : '',
                theme: this.taskTemplate.theme ? this.taskTemplate.theme : '',
                text: this.taskTemplate.text ? this.taskTemplate.text : '',
                deadline: this.taskTemplate.deadline ? this.taskTemplate.deadline : '',
                manager: this.taskTemplate.manager ? this.taskTemplate.manager : null,
                investors: this.taskTemplate.investors ? this.taskTemplate.investors : [],
                contragents: this.taskTemplate.contragents ? this.taskTemplate.contragents : [],
                invest_task_status: this.taskTemplate.invest_task_status ? this.taskTemplate.invest_task_status : null,
                investor_lists: this.taskTemplate.investor_lists ? this.taskTemplate.investor_lists : [],
                contragent_lists: this.taskTemplate.contragent_lists ? this.taskTemplate.contragent_lists : [],
                employee: _.get(this, 'employee'),
                mail_template_id: this.taskTemplate.mail_template_id ? this.taskTemplate.mail_template_id : null,
            },
            mediaCollections: ['files'],
            mntd: false
        }
    },
    props: {
        mailtemplates: {
            default: () => {
                return []
            }
        },
        taskTemplate: {
            default: () => {
                return {}
            }
        },
        investors: {
            default: () => {
                return []
            }
        },
        contragents: {
            default: () => {
                return []
            }
        },
        employees: {
            default: () => {
                return []
            }
        },
        invest_task: {
            default: () => {
                return []
            }
        },
        employee: {
            type: Object,
            default: () => {
                return {}
            }
        },
        managers: {
            default: () => {
                return []
            }
        },
        creators: {
            default: () => {
                return []
            }
        },
        investor_lists: {
            default: () => {
                return []
            }
        },
        contragent_lists: {
            default: () => {
                return []
            }
        },
        invest_task_statuses: {
            default: () => {
                return []
            }
        }
    },
    computed: {
        template() {
            return this.form.mail_template_id;
        }
    },
    watch: {
        template (val) {
            if (!this.mntd && this.form.text !== '') {
                this.mntd = true;
            } else {
                this.mntd = true;
                let tmpl = _.filter(this.mailtemplates, function (o){
                    return o.id === parseInt(val);
                });
                if (tmpl[0]) {
                    this.form.text = tmpl[0].body;
                } else {
                    this.form.text = '';
                }
            }
        }
    }
});
