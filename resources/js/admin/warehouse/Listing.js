import AppListing from '../app-components/Listing/AppListing';

Vue.component('warehouse-listing', {
    mixins: [AppListing]
});