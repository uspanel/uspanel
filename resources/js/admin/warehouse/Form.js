import AppForm from '../app-components/Form/AppForm';

Vue.component('warehouse-form', {
    mixins: [AppForm],
    props:[
        'user'
    ],
});
