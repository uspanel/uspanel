import AppForm from '../app-components/Form/AppForm';

Vue.component('mailsetting-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                smtp_host:  '' ,
                smtp_port:  '' ,
                smtp_encryption: null,
                imap_host:  '' ,
                imap_port:  '' ,
                imap_encryption: null,
                imap_sent_folder:  '' ,
                imap_inbox_folder:  '' ,
                login:  '' ,
                password:  '' ,
                default: false,
                validate_cert: false,
                names: false,
                meiling: false,

            },
            mediaCollections: ['csv'],
            folders: [],
            loading: false,


        }
    },
    mounted() {
        this.pushCurrentFolders(this.form.imap_sent_folder);
        this.pushCurrentFolders(this.form.imap_inbox_folder);
        if(this.form.id){
            this.form.meiling = !this.form.meiling;
        }

    },
    // computed:{
    //     addMailing(){
    //
    //       this.form.meiling = !this.form.meiling;
    //       return  this.form.meiling;
    //     },
    // },
    methods: {
        getFolders() {
            this.loading = true;
            var params = this.form;
            if (_.isEmpty(this.data)) {
                params.force_settings = 1;
            }
            this.$validator.validateAll([
                'smtp_host',
                'smtp_port',
                'smtp_encryption',
                'imap_host',
                'imap_port',
                'imap_encryption',
                'login',
                'password',
            ])
            .then(response => {
                if (this.$validator.errors.count() == 0) {
                    axios.get('/csmailclient/folders', {
                        params: params
                    })
                    .then(response => {
                        this.folders = response.data.data
                        this.form.imap_inbox_folder = _.head(this.folders).attributes.name
                        this.form.imap_sent_folder = _.head(this.folders).attributes.name
                        this.loading = false;
                    })
                    .catch(error => {
                        this.loading = false;
                    })
                } else {
                    this.loading = false;
                }
            });
        },
        pushCurrentFolders(value) {
            if (value) {
                this.folders.push({
                    attributes: {
                        name: value
                    }
                });
            }
        },
        addMailing(){
            this.form.meiling = !this.form.meiling;
        },
        getParseCSV(){
            var _this3 = this;
            if (this.mediaCollections) {
                this.mediaCollections.forEach(function (collection, index, arr) {
                    if (_this3.form[collection]) {
                        console.warn("MediaUploader warning: Media input must have a unique name, '" + collection + "' is already defined in regular inputs.");
                    }

                    if (_this3.$refs[collection + '_uploader']) {
                        _this3.form[collection] = _this3.$refs[collection + '_uploader'].getFiles();
                    }
                });
            }

            this.getNames(this.form.csv[0].path);
        },
        async getNames(path){
            let response = await axios.post('/admin/mailsettings/names',{
                path: path,
            });
            this.form.names = response.data.names;
        },

    }

});
