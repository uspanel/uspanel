import AppListing from '../app-components/Listing/AppListing';

Vue.component('history-mailing-listing', {
    mixins: [AppListing],
    data() {
    	return {
    		filters: {
    			mailing_status_id: null,
    		}
    	}
    },
    methods: {
        chooseBg(type) {
            var success = ['registered'];
            var danger = ['failed'];
            var light = ['total'];
            var warning = ['new'];
            var primary = ['sent'];
            var secondary = [,'canceled'];
            if (success.includes(type)) {
                return 'badge-success'
            }
            if (danger.includes(type)) {
                return 'badge-danger'
            }
            if (warning.includes(type)) {
                return 'badge-warning'
            }
            if (light.includes(type)) {
                return 'badge-light'
            }
            if (primary.includes(type)) {
                return 'badge-primary'
            }
            if (secondary.includes(type)) {
                return 'badge-secondary'
            }

        },
    }
});
