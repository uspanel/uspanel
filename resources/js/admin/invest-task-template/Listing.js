import AppListing from '../app-components/Listing/AppListing';

Vue.component('invest-task-template-listing', {
    mixins: [AppListing]
});