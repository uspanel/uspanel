import AppForm from '../app-components/Form/AppForm';

Vue.component('invest-task-template-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                name: '',
                theme: '',
                text: '',
                manager: null,
                investors: [],
                contragents: [],
                investor_lists: [],
                contragent_lists: [],
                mail_template_id: null,
            },
            mntd: false
        }
    },
    props: {
        mailtemplates: {
            default: () => {
                return []
            }
        },
        investors: {
            default: () => {
                return []
            }
        },
        contragents: {
            default: () => {
                return []
            }
        },
        managers: {
            default: () => {
                return []
            }
        },
        investor_lists: {
            default: () => {
                return []
            }
        },
        contragent_lists: {
            default: () => {
                return []
            }
        },
    },
    computed: {
        template() {
            return this.form.mail_template_id;
        }
    },
    watch: {
        template (val) {
            if (!this.mntd && this.form.text !== '') {
                this.mntd = true;
            } else {
                this.mntd = true;
                let tmpl = _.filter(this.mailtemplates, function (o){
                    return o.id === parseInt(val);
                });
                if (tmpl[0]) {
                    this.form.text = tmpl[0].body;
                } else {
                    this.form.text = '';
                }
            }
        }
    }

});
