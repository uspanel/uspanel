import AppForm from '../app-components/Form/AppForm';

Vue.component('project-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                company: {},
                form_ids: [],
                manager_ids: [],
                support_ids: [],
                hr_percent: {},
                support_percent: {},
            },
            company_type_id: null,
        }
    },
    props: {
        hrStaff: {
            type: [Object,Array],
            required: true
        },
        hrInvest: {
            type: [Object,Array],
            required: true
        },
        supportStaff: {
            type: [Object,Array],
            required: true
        },
        supportInvest: {
            type: [Object,Array],
            required: true
        },
        states: {
            type: [Object,Array],
            required: true
        }
    },
    methods: {
        increasePct: function(id){

        },
        togleInvest(invest_id){
            let id = parseInt(invest_id);
           if(this.form.support_ids.includes(id)){
               const index = this.form.support_ids.indexOf(id);
               if (index > -1) {
                   this.form.support_ids.splice(index, 1);
               }
               if(this.form.company.id){
                   delete this.form.support_percent[id];
               }
           }else{
               this.form.support_ids.push(id);
           }
        },
        togleManager(manager_id){
            let id = parseInt(manager_id);
            if(this.form.manager_ids.includes(id)){
                const index = this.form.manager_ids.indexOf(id);
                if (index > -1) {
                    this.form.manager_ids.splice(index, 1);
                }
                if(this.form.company.id){
                    delete this.form.hr_percent[id];
                }
            }else{
                this.form.manager_ids.push(id);
            }
        },

        customsend(){
                if (this.form.company.state_id) {
                    this.form.company.state_id = this.form.company.state_id.id;
                }

        }
        // onSubmit() {
        //     if (this.form.company.state_id) {
        //         this.form.company.state_id = this.form.company.state_id.id;
        //     }
        //     console.log(this.form);
        //     axios.post(this.action, this.form)
        //         .then( response => {
        //             return this.onSuccess(response.data);
        //         })
        //         .catch( errors => {
        //             return this.onFail(errors.response.data.errors);
        //         });
        // }
    },
    mounted() {
        if (this.form.company.state_id) {
            var vm = this;
            this.form.company.state_id = _.filter(this.states, function(o){
                return o.id == vm.form.company.state_id;
            })[0];
        }
    },
    computed: {
        managers: function() {
            console.log('reassemble managers');
                return this.hrInvest;

        },
        supports: function() {
            console.log('reassemble supports');
                return this.supportInvest;

        }
    },

});
