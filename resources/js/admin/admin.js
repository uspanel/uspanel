console.log("%c##############################\n" +
    "#########===========##########\n" +
    "######=================#######\n" +
    "####============**=======#####\n" +
    "###======++====+ :=++=====+###\n" +
    "##=====*..+====-.==: .+====###\n" +
    "##===*..+=====* *====:..+===##\n" +
    "##=+..+=======.-=======: -==##\n" +
    "##===: -+====: +=====+..*===##\n" +
    "##=====: -+=+.:====+..*====+##\n" +
    "###======:*=-.=====:*======###\n" +
    "####=======+.*============####\n" +
    "######==================######\n" +
    "#########============#########\n" +
    "##############################\n", 'color: green;font-weight:bold;');
console.log("%c Site: https://coders.studio", 'color: green;font-weight:bold;');
console.log("%c Mail: info@coders.studio",'color: green;font-weight:bold;');
console.log("%c Developed with \u2665. 2019",'color: green;font-weight:bold;');

import './bootstrap';
import 'vue-multiselect/dist/vue-multiselect.min.css';
import flatPickr from 'vue-flatpickr-component';
import VueQuillEditor from 'vue-quill-editor'
import Notifications from 'vue-notification';
import Multiselect from 'vue-multiselect';
import VeeValidate from 'vee-validate';
import 'flatpickr/dist/flatpickr.css';
import VueCookie from 'vue-cookie';
import {Admin} from 'craftable';
import VModal from 'vue-js-modal'
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue'
import './app-components/bootstrap';
import './index';
import 'craftable/dist/ui';
import VueProgressBar from 'vue-progressbar'
import VueDraggableResizable from 'vue-draggable-resizable'
import 'vue-draggable-resizable/dist/VueDraggableResizable.css'
import VueUploadComponent from 'vue-upload-component'
import VueImg from 'v-img';
import moment from 'moment'
import VueTheMask from 'vue-the-mask'
import './mouldifi/functions';
import VueTimers from 'vue-timers'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueApexCharts from 'vue-apexcharts'
import Twilio from 'twilio-client';
import SipClient from './twilio/Sip.vue';
import VueCountdown from '@chenfengyuan/vue-countdown';
// Vue.config.devtools = true;
Vue.use(VueTheMask);
Vue.use(VueTimers);
Vue.use(BootstrapVue)
Vue.use(VueApexCharts)
Vue.component(VueCountdown.name, VueCountdown);

Vue.component('apexchart', VueApexCharts)

// moment.locale('ru');
Vue.component('vue-draggable-resizable', VueDraggableResizable)
Vue.component('file-upload', VueUploadComponent)
Vue.use(VueImg);
Vue.component('chat', require('../../assets/vendor/CodersStudioChat/components/Chat.vue').default);
Vue.component('user-salary', require('./user/Salary.vue').default);
Vue.component('coders-studio-notifications', require('../../assets/vendor/codersstudio/js/components/Notifications.vue').default);
Vue.component('coders-studio-notifications-index', require('../../assets/vendor/codersstudio/js/components/NotificationsIndex').default);
Vue.component('register', require('./Register.js').default);
Vue.prototype.moment = moment
Vue.component('multiselect', Multiselect);
Vue.use(VeeValidate, {strict: true});
Vue.component('datetime', flatPickr);
Vue.use(VModal, {dialog: true});
Vue.use(VueQuillEditor);
Vue.use(Notifications);
Vue.use(VueCookie);
Vue.use(BootstrapVue)
Vue.use(VueProgressBar, {
    color: '#4273FA',
    failedColor: '#fe0c37',
    thickness: '10px'
})
Object.defineProperty(Vue.prototype, '_', {value: _});

const mixin = {
    data() {
        return {
            Device: Twilio.Device,
        }
    },
    methods: {
        translate: function (key) {
            key = key.replace(/::/, '.');
            return _.get(window.trans, key, key);
        },
        deleteDialog(onСonsent = () => {}) {
            this.$modal.show('dialog', {
                title: this.translate('chat.delete_message'),
                text: this.translate('chat.are_you_sure'),
                buttons: [
                    {
                        title: this.translate('chat.close'),
                        class: 'btn btn-light rounded-0'
                    },
                    {
                        title: this.translate('chat.delete'),
                        class: 'btn btn-danger rounded-0',
                        handler: () => {
                            onСonsent()
                        }
                    },
                ]
            })
        },
        sendCustomSms: function (userId, message) {
            axios.post('/sms/custom', {'recipient': userId, 'message': message}).then(response => {
                this.$notify({ type: 'success', title: 'Success!', text: response.data.message ? response.data.message : 'Action completed' });
            });
        }
    },
    components: {
        SipClient
    },

};
Vue.mixin(mixin);
const app = new Vue({
    mixins: [Admin],
    data() {
        return {
            pending: false,
        }
    },
    created() {
    },
    mounted() {
        if (window.app.user && window.app.user.id) {
            axios.get('/twilio/token').then(response => {
                this.Device.setup(response.data.token);
            });
        }
        if (this.$root.$refs.sipclient) {
            this.$root.$refs.sipclient.callbacks.missedCallback = this.missedCall;
        }
    },
    methods: {
        missedCall: function (caller) {
            axios.post('/twilio/missedcall', {'caller': caller});
        }
    }
});
