import AppForm from '../app-components/Form/AppForm';

Vue.component('auto-answer-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                user_id:  '' ,
                mailsetting_id:  '' ,

            }
        }
    },
    props: {
        ownedMailSettings: {
            default: () => {
                return {}
            }
        },
    }

});
