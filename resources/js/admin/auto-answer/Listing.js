import AppListing from '../app-components/Listing/AppListing';

Vue.component('auto-answer-listing', {
    mixins: [AppListing]
});