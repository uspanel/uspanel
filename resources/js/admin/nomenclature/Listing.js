import AppListing from '../app-components/Listing/AppListing';

Vue.component('nomenclature-listing', {
    mixins: [AppListing]
});