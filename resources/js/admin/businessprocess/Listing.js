import AppListing from '../app-components/Listing/AppListing';

Vue.component('businessprocess-listing', {
    mixins: [AppListing],
    methods: {
    	setActive(item) {
            axios.post('/admin/businessprocesses/' + item.id + '/active', {
                active: item.active
            })
            .then(response => {
            	this.$notify({ type: 'success', title: 'Success!', text: response.data.message ? response.data.message : 'Action completed' });
            })
            .catch(error => {
                
            })
    	}
    }
});