import AppForm from '../app-components/Form/AppForm';
import draggable from 'vuedraggable'
import BusinessProcessEvent from './BusinessProcessEvent.vue'
import Users from './Users.vue'
import MailTemplates from './MailTemplates.vue'
import Multiselect from 'vue-multiselect'

Vue.component('businessprocess-form', {
    mixins: [AppForm],
    components: {
        draggable,
        BusinessProcessEvent,
        Users,
        MailTemplates,
        Multiselect
    },
    props: {
        data: {
            default: () => {
                return {
                    business_process_events: [],
                    title: null,
                    active: false,
                    id: null
                }
            }
        },
        businessProcessTypes: {
            type: Array,
            default: () => []
        }
    },
    data: function() {
        return {
            form: {},
            business_process_types: this.businessProcessTypes,
            business_process_type: {},
            events: [],
            actions: [],
            business_process_events: this.data.business_process_events,
            business_process_event_index: null,
            title: this.data.title,
            active: this.data.active,
            action_business_process_events_index: null,
            searchFor: {
            	event: null,
            	action: null,
            },
            action_business_process_events: {
                action: {},
                users: [],
                roles: [],
                mail_template: null,
                order: 0,
            },
            templates: []
        }
    },
    mounted() {
        this.business_process_type = _.get(this.data, 'business_process_type', _.get(this, 'businessProcessTypes[0]', {}));
        this.getEvents();
        this.getActions();
        this.getMailTemplates();
    },
    watch: {
        business_process_type(type) {
            this.getEvents();
            this.$set(this.form, 'business_process_type_id', type.id);
        }
    },
    methods: {
        getEvents() {
            axios.get('/admin/businessprocesses/events', {
                params: {
                    business_process_type_id: _.get(this.business_process_type, 'id', null)
                }
            })
            .then(response => {
                this.events = response.data.events
            })
        },
        getMailTemplates() {
            axios.get('/admin/businessprocesses/mail_templates')
            .then(response => {
                this.templates = response.data.templates
            })
        },
        getActions() {
            axios.get('/admin/businessprocesses/actions')
            .then(response => {
                this.actions = response.data.actions
            })
        },
    	filter(collection, searchFor) {
    		if (_.isString(searchFor)) {
    			return collection.filter((value) => {
    				if (_.toLower(value.title).includes(_.toLower(searchFor))) {
    					return value
    				}
			    })
    		}
    		return collection
    	},
        cloneEvent(event) {
            return {
                event: event,
                action_business_process_events: []
            }
        },
        addAction(action) {
            this.action_business_process_events.action = action;
        },
        saveAction() {
            this.errors.clear();
            if (_.isUndefined(this.action_business_process_events.action.id)) {
                return
            }
            if (_.isEmpty(this.$refs.users.selectedUsers)) {
                this.errors.add({
                    field: 'users',
                    msg: this.translate('admin.businessprocess.errors.select_users')
                });
                return
            }
            if (this.action_business_process_events.action.name == 'App\\Libs\\Actions\\NotifyByEmail' && _.isNull(this.$refs.templates.selectedTemplate)) {
                this.errors.add({
                    field: 'mail_template',
                    msg: this.translate('admin.businessprocess.errors.select_mail_template')
                });
                return
            }
            this.action_business_process_events.users = this.$refs.users.selectedUsers;
            this.action_business_process_events.roles = this.$refs.users.selectedRoles;
            if (this.action_business_process_events.action.name == 'App\\Libs\\Actions\\NotifyByEmail') {
                this.action_business_process_events.mail_template = this.$refs.templates.selectedTemplate
            }
            if (_.isNil(this.action_business_process_events_index)) {
                this.business_process_events[this.business_process_event_index].action_business_process_events.push(this.action_business_process_events)
            } else {
                this.business_process_events[this.business_process_event_index].action_business_process_events[this.action_business_process_events_index] = this.action_business_process_events
            }
            this.$modal.hide('addAction');
        },
        beforeOpen(event) {
            this.business_process_event_index = event.params.business_process_event_index
            if (!_.isUndefined(event.params.action_business_process_events_index)) {
                this.action_business_process_events_index = event.params.action_business_process_events_index
                this.action_business_process_events = this.business_process_events[this.business_process_event_index].action_business_process_events[event.params.action_business_process_events_index]
            }
        },
        beforeClose(event) {
            this.business_process_event_index = null;
            this.action_business_process_events_index = null;
            this.action_business_process_events = {
                action: {},
                users: [],
                mail_template: null
            }
        },
        deleteAction(business_process_event_index, action_business_process_events_index) {
            this.$delete(this.business_process_events[business_process_event_index].action_business_process_events, action_business_process_events_index)
            this.$modal.hide('dialog');
        },
        deleteEvent(business_process_event_index) {
            console.log(business_process_event_index)
            this.$delete(this.business_process_events, business_process_event_index)
            this.$modal.hide('dialog');
        },
        save() {
            this.errors.clear()
            var url = '/admin/businessprocesses'
            if (this.data.id) {
                var url = '/admin/businessprocesses/' + this.data.id
            }
            axios.post(url, {
                business_process_events: this.business_process_events,
                title: this.title,
                active: this.active,
                business_process_type_id: _.get(this.business_process_type, 'id', null)
            })
            .then(response => {
                document.location.replace('/admin/businessprocesses');
            })
            .catch(error => {
                _.forEach(error.response.data.errors, (item, key) => {
                    this.errors.add({
                        field: key,
                        msg: _.head(item)
                    });
                })
            })
        }
    },
});
