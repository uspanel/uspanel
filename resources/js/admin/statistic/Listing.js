import AppListing from '../app-components/Listing/AppListing';

Vue.component('statistic-listing', {
    mixins: [AppListing],
    data() {
    	return {
    		userStatusesOptions: {
    			chart: {
          			id: 'basic-bar'
        		},
       	 		xaxis: {
          			categories: _.map(this.data.userStatuses, 'title')
        		},
    		},
    		userStatusesSeries: [
    			{
		          	name: 'Total',
        			data: _.map(this.data.userStatuses, 'employees_count')
		        }
    		],
            userReferrersOptions: {
                chart: {
                    id: 'basic-bar'
                },
                xaxis: {
                    categories: _.map(this.data.userReferrers, 'title')
                },
            },
            userReferrersSeries: [
                {
                    name: 'Total',
                    data: _.map(this.data.userReferrers, 'count')
                }
            ],
    		userTasksOptions: {
    			chart: {
          			id: 'basic-bar'
        		},
       	 		xaxis: {
          			categories: _.map(this.data.userTasks, 'name')
        		},
    		},
    		userTasksSeries: [
    			{
		          	name: this.translate('admin.statistic.tasks_counts'),
        			data: _.map(this.data.userTasks, 'tasks_count').sort().reverse()
		        },
		        {
		          	name: this.translate('admin.statistic.delivery_status_bad'),
        			data: _.map(this.data.userTasks, 'delivery_status_bad').sort().reverse()
		        },
		        {
		          	name: this.translate('admin.statistic.delivery_status_fast'),
        			data: _.map(this.data.userTasks, 'delivery_status_fast').sort().reverse()
		        },
		        {
		          	name: this.translate('admin.statistic.delivery_status_medium'),
        			data: _.map(this.data.userTasks, 'delivery_status_medium').sort().reverse()
		        },
		        {
		          	name: this.translate('admin.statistic.delivery_status_slow'),
        			data: _.map(this.data.userTasks, 'delivery_status_slow').sort().reverse()
		        },
		        {
		          	name: this.translate('admin.statistic.delivery_status_super_fast'),
        			data: _.map(this.data.userTasks, 'delivery_status_super_fast').sort().reverse()
		        },
    		],
            mailingsOptions: {
                chart: {
                    id: 'basic-bar'
                },
                xaxis: {
                    categories: _.map(this.data.mailings, 'name')
                },
            },
            mailingsSeries: [
                {
                    name: this.translate('admin.statistic.history_mailings_count'),
                    data: _.map(this.data.mailings, 'history_mailings_count').sort().reverse()
                },
                {
                    name: this.translate('admin.statistic.registered'),
                    data: _.map(this.data.mailings, 'registered').sort().reverse()
                },
                {
                    name: this.translate('admin.statistic.approved'),
                    data: _.map(this.data.mailings, 'approved').sort().reverse()
                },
                {
                    name: this.translate('admin.statistic.rejected'),
                    data: _.map(this.data.mailings, 'rejected').sort().reverse()
                },
            ],
    	}
    }
});
