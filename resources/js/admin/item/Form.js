import AppForm from '../app-components/Form/AppForm';
import ItemNomenclature from './Nomenclature.vue';

Vue.component('item-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {},
            item: this.data,
        }
    },
    components: {
        ItemNomenclature
    },
    methods: {
        onSelectItem() {
            this.form = {};
            this.form = this.item
        }
    }
});