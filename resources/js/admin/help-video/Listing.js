import AppListing from '../app-components/Listing/AppListing';

Vue.component('help-video-listing', {
    mixins: [AppListing],
    data: function() {
        return {
            selectedItem: {
                title:  '' ,
                description:  '' ,
                video:'',
                webm_url:'',
                mp4_url:''
            },
            searchroles:[]
        }
    },
    methods : {
        showVideoModal (item) {
            this.selectedItem = item;
            this.$refs['videoModal'].show()
        }
    },
    watch: {
        searchroles: function (newval, oldval) {
            this.filter('roles', newval);
        }
    }
});
