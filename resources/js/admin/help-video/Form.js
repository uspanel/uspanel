import AppForm from '../app-components/Form/AppForm';

Vue.component('help-video-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                title:  '' ,
                description:  '' ,
                
            },
            mediaCollections: ['video']
        }
    }

});
