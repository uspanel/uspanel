import AppForm from '../app-components/Form/AppForm';

Vue.component('email-mailing-form', {
    mixins: [AppForm],
    props: {
        emailGroups: [Object, Array],
        chosenGroup: {
            required: false
        }
    },
    data: function() {
        return {
            form: {
                name:  '' ,
                email:  '' ,
                email_group_id:  this.chosenGroup ? this.chosenGroup : '',
                
            }
        }
    }

});
