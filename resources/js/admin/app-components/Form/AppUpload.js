import { BaseUpload } from 'craftable';

Vue.component('media-upload', {
    mixins: [BaseUpload],
    template: '<dropzone :timeout="300000" :id="collection" \n                       :url="url" \n                       v-bind:preview-template="template"\n                       v-on:vdropzone-success="onSuccess"\n                       v-on:vdropzone-error="onUploadError"\n                       v-on:vdropzone-removed-file="onFileDelete"\n                       v-on:vdropzone-file-added="onFileAdded"\n                       :useFontAwesome="true" \n                       :ref="collection"\n                       :maxNumberOfFiles="maxNumberOfFiles"\n                       :maxFileSizeInMB="maxFileSizeInMb"\n                       :acceptedFileTypes="acceptedFileTypes"\n                       :thumbnailWidth="thumbnailWidth"\n                       :headers="headers">\n                \n                <input type="hidden" name="collection" :value="collection">\n            </dropzone>',

});
