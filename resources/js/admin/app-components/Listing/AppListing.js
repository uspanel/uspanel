import { BaseListing } from 'craftable';

export default {
	mixins: [BaseListing],
    data() {
	    return {
            datetimePickerConfig: {
                enableTime: true,
                time_24hr: true,
                enableSeconds: true,
                dateFormat: 'Y-m-d H:i:S',
                altInput: true,
                altFormat: 'd.m.Y H:i:S',
                locale: document.documentElement.lang === 'en' ? null : require('flatpickr/dist/l10n/' + document.documentElement.lang + '.js')[document.documentElement.lang]
            },
        }
    }
};
