export default {
    data(){
        return {
            currentIndex: null,
        }
    },
    methods:{
        uploadFile(type){
            let params = {};
            params[type] = this.$refs[type + '_uploader'].getFiles();
            axios.post(this.collection[this.currentIndex].resource_url, params)
                .then(() => {
                    this.$modal.hide(type + '_upload');
                    this.loadData();
                })
                .catch(error => {
                    console.log(error);
                });
        }
    }
}
