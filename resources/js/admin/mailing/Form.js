import AppForm from '../app-components/Form/AppForm';

Vue.component('mailing-form', {
    mixins: [AppForm],
    props: {    
        mailTemplates: [Object, Array],
        emailGroups: [Object, Array]
    },
    data: function() {
        return {
            form: {
                name:  '' ,
                mail_template_id:  '' ,
                user_id:  '' ,
                email_groups: '',
                scheduled_at:  ''   
            }
        }
    }

});