import AppListing from '../app-components/Listing/AppListing';

Vue.component('mailing-listing', {
    mixins: [AppListing],
    props: ['mail_settings', 'curent_setting'],
    data() {
    	return {
    		currentMailingId: null,
    		emails: null,
            filters: {
                mail_template_id: null,
                email_group_id: null,
            },
            sent: false,
            id: null,
            mail_setting: '',
            newSchedule: null
    	}
    },
    mounted() {
            this.mail_setting = this.curent_setting;
    },
    methods: {
        stopSending(id) {
            axios.post('/admin/mailings/'+id+'/stopsending').then(response => {
                this.$notify({ type: 'success', title: 'Success', text: this.translate('admin.mailing.mailing_stopped') });
                this.loadData();
            });
        },
        sendNow(id) {
            if (this.sent) {
                return
            }
            this.sent = true


            axios.post('/admin/mailings/' + id + '/applynow', {
                set_id: this.mail_setting
            })
                .then(response => {
                    this.sent = false
                    this.$notify({ type: 'success', title: 'Success', text: this.translate('admin.mailing.mailing_added_to_queue') });
                    this.loadData();
                })
                .catch(error => {
                    this.sent = false
                    this.$notify({
                        type: 'error',
                        title: 'Error',
                        text: this.translate('admin.mailing.failed_to_add_mailing_to_queue')
                    });
                })
        },
        chooseBg(type) {
            var success = ['registered'];
            var danger = ['failed'];
            var light = ['total'];
            var warning = ['new'];
            var primary = ['sent'];
            var secondary = [,'canceled'];
            if (success.includes(type)) {
                return 'badge-success'
            }
            if (danger.includes(type)) {
                return 'badge-danger'
            }
            if (warning.includes(type)) {
                return 'badge-warning'
            }
            if (light.includes(type)) {
                return 'badge-light'
            }
            if (primary.includes(type)) {
                return 'badge-primary'
            }
            if (secondary.includes(type)) {
                return 'badge-secondary'
            }

        },
    	toQueue(id) {
            if (this.sent) {
                return
            }
            this.sent = true
    		axios.post('/admin/mailings/' + id + '/apply', {
                set_id: this.mail_setting
            })
    		.then(response => {
                this.sent = false
    			this.$notify({ type: 'success', title: 'Success', text: this.translate('admin.mailing.mailing_added_to_queue') });
    		})
    		.catch(error => {
                this.sent = false
    			this.$notify({
					type: 'error',
					title: 'Error',
					text: this.translate('admin.mailing.failed_to_add_mailing_to_queue')
				});
    		})
    	},
        resend(id) {
    	    this.id = id;
            this.$modal.show('mailingResend');
        },
        resendMailing() {
            this.$modal.hide('mailingResend');
            axios.post('/admin/mailings/'+ this.id + '/resend',
                {
                    scheduled_at: this.newSchedule,
                    set_id: this.mail_setting
                }).then(response => {
                this.sent = false
                this.$notify({ type: 'success', title: 'Success', text: this.translate('admin.mailing.mailing_added_to_queue') });
            })
                .catch(error => {
                    this.sent = false
                    this.$notify({
                        type: 'error',
                        title: 'Error',
                        text: this.translate('admin.mailing.failed_to_add_mailing_to_queue')
                    });
                });
            this.id = null;
            this.newSchedule = null;

        },
    	openMailingTestModal(id) {
    		this.currentMailingId = id
    		this.$modal.show('mailingTest');

    	},
    	testMailing() {
            this.errors.clear();
    		axios.post('/admin/mailings/' + this.currentMailingId + '/test', {
    			emails: this.emails,
                set_id: this.mail_setting
    		})
    		.then(response => {
                this.emails = null
    			this.$modal.hide('mailingTest');
    			this.$notify({ type: 'success', title: 'Success', text: this.translate('admin.mailing.check_emails') });
    		})
    		.catch(error => {
    			_.forEach(error.response.data.errors, (item, key) => {
                    this.errors.add({
                        field: key,
                        msg: _.head(item)
                    });
                })
    			this.$notify({
					type: 'error',
					title: 'Error',
					text: this.translate('admin.mailing.something_went_wrong')
				});
    		})
    	}
    }
});
