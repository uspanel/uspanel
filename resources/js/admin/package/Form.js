import AppForm from '../app-components/Form/AppForm';
import ItemSelect from '../item/Select.vue'
import UserSelectEmployee from '../user/SelectEmployee.vue'
Vue.component('package-form', {
    mixins: [AppForm],
    props:[
        'statuses',
        'carriers',
        'couriers',
        'managers',
        'employee'
    ],
    components: {
        ItemSelect,
        UserSelectEmployee
    },
    data: function() {
        return {
            form: {
                track: '',
                carrier_id: '',
                weight: '',
                name: '',
                courier_id: '',
                manager_id: '',
                comment: '',
                status_id: 1,
                description: '',
                price: '',
                color: '',
                link: '',
                item_ids: this.data.item_ids,
                new_items: [],
                employee_id: this.data.employee_id ? this.data.employee_id : (this.employee ? this.employee : null),
                redirect: (this.employee ? 'admin/roles/5/users' : null)
            },
            mediaCollections: ['invoice']
        }
    },
    methods: {
        getPostData: function getPostData() {
            var _this3 = this;

            if (this.mediaCollections) {
                this.mediaCollections.forEach(function (collection, index, arr) {
                    if (_this3.form[collection]) {
                        console.warn("MediaUploader warning: Media input must have a unique name, '" + collection + "' is already defined in regular inputs.");
                    }

                    if (_this3.$refs[collection + '_uploader']) {
                        _this3.form[collection] = _this3.$refs[collection + '_uploader'].getFiles();
                    }
                });
            }
            this.form['wysiwygMedia'] = this.wysiwygMedia;
            if (this.$refs.itemSelect) {
                this.form.item_ids = this.$refs.itemSelect.selected;
            }
            return this.form;
        },
    }

});
