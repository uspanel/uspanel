import AppListing from '../app-components/Listing/AppListing';
import TableUpload from '../app-components/Listing/TableUpload';

Vue.component('package-listing', {
    mixins: [AppListing, TableUpload],
    props: {
        statuses: [Object, Array],
        carriers: [Object, Array],
        couriers: [Object, Array],
        employees: [Object, Array],
        managers: [Object, Array],
        users: [Object, Array],
        defFilters: [Object, Array]
    },
    data(){
        return {
            filters: {
                delivered_at: '',
                track: '',
                carrier_id: '',
                weight: '',
                name: '',
                courier_id: '',
                creator_id: '',
                manager_id: '',
                status_id: '',
                id: ''
            },
            orderBy: {
                direction: 'desc'
            }
        }
    },
    mounted() {
        this.filters = _.assign(this.filters, this.defFilters);
    },
    methods: {
        forceDelivered(id) {
            axios.post('/admin/packages/'+id+'/forcedelivered').then(response => {
                this.loadData();
            });
        },
        uploadFiles(files){
            var element = document.createElement('a');
            element.setAttribute('download',"");
            element.style.display = 'none';
            document.body.appendChild(element);
            files.forEach(function(el) {
                var link = "/media/" + el.id + '/' + el.file_name;
                element.setAttribute('href', link);
                element.click();
            });
            document.body.removeChild(element);
        },
        showFiles(files){
            var element = document.createElement('a');
            element.setAttribute('target',"_blank");
            element.style.display = 'none';
            document.body.appendChild(element);
            files.forEach(function(el) {
                var link = "/media/" + el.id + '/' + el.file_name;
                element.setAttribute('href', link);
                element.click();
            });
            document.body.removeChild(element);
        }
    }
});
