import AppForm from '../app-components/Form/AppForm';

Vue.component('profile-edit-profile-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                activated:  false ,
                forbidden:  false ,
            },
            mediaCollections: ['avatar'],
            fails: null
        }
    },
    methods: {
        onSuccess(data) {
            this.$notify({ type: 'success', title: 'Success', text: this.translate('admin.profile_updated') });
        },
        onFail(errors) {
            this.fails = errors
        },
        update() {
            this.fails = null;
            this.onSubmit();
        }
    }
});
