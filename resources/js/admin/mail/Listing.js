import AppListing from '../app-components/Listing/AppListing';
import MailFolder from './Folder.vue'
Vue.component('mail-listing', {
    mixins: [AppListing],
    data() {
        return {
            selectedItems: [],
            selectedAll: [],
            params: {
                page: 1,
                per_page: 10,
                search: null,
                set_id: this.defaultMailSettings ? this.defaultMailSettings.id : null,
                folder: this.defaultMailSettings ? this.defaultMailSettings.imap_inbox_folder : null,
            },
            folders: [],
            pending: false,
            notification: {},
            filterAcc: null,
            addresseeFilter: {'addressee' : null}
        }
    },
    props: {
        mailsettings: {
            default: []
        },
        defaultMailSettings: {
            default: {}
        },
    },
    mounted() {
        Echo.private('App.User.1')
        .notification((notification) => {
            this.notification = notification
        });
        if (!_.isEmpty(this.mailsettings)) {
            this.getMessages();
        }
    },
    watch: {
        'pagination.state.set_id': function(newVal, oldVal) {
            this.getMessages();
        },
        addresseeFilter: function(newVal) {
            this.loadData(false);
        }

    },
    computed: {
        filteredMs() {
            if (this.filterAcc == null) {
                return this.mailsettings;
            } else if (this.filterAcc == 'personal') {
                return _.filter(this.mailsettings, function(item, index) {
                    return item.owner == null;
                });
            } else if (this.filterAcc == 'owned') {
                return _.filter(this.mailsettings, function(item, index) {
                    return item.owner != null;
                });
            }

        },
        hasOwned() {
            var owned = _.filter(this.mailsettings, function(item, index) {
                return item.owner != null;
            });
            return !_.isEmpty(owned);
        }
    },
    methods: {
        loadData: function loadData(resetCurrentPage) {
            var _this = this;

            var options = {
                params: {
                    per_page: this.pagination.state.per_page,
                    page: this.pagination.state.current_page,
                    orderBy: this.orderBy.column,
                    orderDirection: this.orderBy.direction,
                    folder: this.params.folder,
                    search: this.params.search,
                    set_id: this.params.set_id,
                    addressee: this.addresseeFilter.addressee ? this.addresseeFilter.addressee : null
                }
            };

            if (resetCurrentPage === true) {
                options.params.page = 1;
            }

            Object.assign(options.params, this.filters);
            this.pending = true;
            axios.get('/csmailclient/messages', options).then(response => {
                this.collection = response.data.data
                this.pagination.state.current_page = response.data.current_page
                this.pagination.state.last_page = response.data.last_page
                this.pagination.state.per_page = response.data.per_page
                this.pagination.state.total = response.data.total
            }).catch(error => {

            }).finally(result => {
                this.pending = false;
            });
        },
        openMessage(uid) {
            window.location = '/admin/mailsettings/'+this.params.set_id+'/folder/'+this.params.folder+'/mails/'+uid;
        },
        getMessages() {
            this.pending = true;
            axios.get('/csmailclient/messages', {
                params: this.params
            })
            .then(response => {
                this.collection = response.data.data
                this.pagination.state.current_page = response.data.current_page
                this.pagination.state.last_page = response.data.last_page
                this.pagination.state.per_page = response.data.per_page
                this.pagination.state.total = response.data.total
                this.pending = false
            })
            .catch(response => {
                this.$notify({ type: 'error', title: 'Error!', text: error.response.data.message ? error.response.data.message : 'An error has occured.' });
                this.pending = false
            })
        },
        deleteSelected() {
            var vm = this;
            this.$modal.show('dialog', {
                title: 'Warning!',
                text: 'Do you really want to delete this item?',
                buttons: [{ title: 'No, cancel.' }, {
                    title: '<span class="btn-dialog btn-danger">Yes, delete.<span>',
                    handler: function handler() {
                        vm.$modal.hide('dialog');
                        axios.post('/csmailclient/massdestroy', {
                            set_id: vm.defaultMailSettings.id,
                            folder: vm.params.folder,
                            ids: vm.selectedItems
                        }).then(function (response) {
                            vm.onSuccessDelete();
                        }, function (error) {
                            vm.$notify({ type: 'error', title: 'Error!', text: error.response.data.message ? error.response.data.message : 'An error has occured.' });
                        });
                    }
                }]
            });
        },
        onSuccessDelete() {
            this.getMessages();
            this.selectedItems = [];
            this.selectedAll = [];
            this.$notify({ type: 'success', title: 'Success!', text: response.data.message ? response.data.message : 'Item successfully deleted.' });
        },
        selectItem() {
            if (_.size(this.collection) == _.size(this.selectedItems)) {
                this.selectedAll = [
                    true
                ];
            } else {
                this.selectedAll = [];
            }
        },
        selectAll() {
            var vm = this;
            if (_.isEmpty(this.selectedAll)) {
                _.forEach(this.collection, function(item, index) {
                    vm.selectedItems.push(item.id);
                });
                this.selectedAll = [
                    true
                ];
            } else {
                this.selectedItems = [];
                this.selectedAll = [];
            }
        },
        getFolders() {
            axios.get('/csmailclient/folders')
            .then(response => {
                this.folders = response.data.data
            })
            .catch(error => {

            })
        },
        setParams(data) {
            var vm = this;
            _.forEach(data, function(item, index) {
                vm.$set(vm.params, index, item);
            })
            this.params.page = 1;
            this.getMessages();
        }
    },
    components: {
        MailFolder
    }
});
