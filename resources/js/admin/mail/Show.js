Vue.component('mail-show', {
    data: function() {
        return {
            message: {
                attributes: {
                    message: '',
                    sender: null,
                    subject: '',
                    attachments: []
                }
            },
            reply: false
        }
    },
    props: {
        mailsettingsId: {
            default: null
        },
        folder: {
            default: null
        },
        id: {
            default: {}
        },
        mailsettings: {
            default: []
        },
    },
    mounted() {
        this.getMessage();
    },
    methods: {
        getParams() {
            return {
                set_id: this.mailsettingsId,
                folder: this.folder,
                uid: this.id,
            }
        },
        getMessage() {
            this.$root.pending = true
            axios.get('/csmailclient/message/'+this.id, {
                params: this.getParams()
            })
            .then(response => {
                this.$root.pending = false;
                this.message = response.data.data
            })
            .catch(error => {
                this.$notify({ type: 'error', title: 'Error!', text: error.response.data.message ? error.response.data.message : 'An error has occured.' });
            })
        },
        deleteMessage() {
            var vm = this;
            this.$modal.show('dialog', {
                title: 'Warning!',
                text: 'Do you really want to delete this item?',
                buttons: [{ title: 'No, cancel.' }, {
                    title: '<span class="btn-dialog btn-danger">Yes, delete.<span>',
                    handler: function handler() {
                        vm.$modal.hide('dialog');
                        axios.delete('/csmailclient/message/'+vm.id, {
                            params: vm.getParams()
                        })
                        .then(function (response) {
                            window.location.replace('/admin/mails');
                            vm.$notify({ type: 'success', title: 'Success!', text: response.data.message ? response.data.message : 'Item successfully deleted.' });
                        }, function (error) {
                            vm.$notify({ type: 'error', title: 'Error!', text: error.response.data.message ? error.response.data.message : 'An error has occured.' });
                        });
                    }
                }]
            });
        }
    }

});
