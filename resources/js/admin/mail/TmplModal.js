export default {
    name: 'TmplModal',
    props: {
        tmpltitle: {
            default: ''
        },
        public: {
            default: 0
        },
        storeTemplate: {
            default: {}
        },
    },
    data() {
        return {
            tmpltit: '',
            pub: 0
        }
    },
    mounted() {
        this.tmpltit = this.tmpltitle;
        this.pub = this.public;
    },
    watch: {
        tmpltit: function (val) {
            this.$emit('update:tmpltitle', val);
        },
        pub: function (val) {
            this.$emit('update:public', val);
        }
    },
    methods: {}
}
