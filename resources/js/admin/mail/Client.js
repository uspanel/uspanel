export default {
    name: 'MailClient',
    data() {
        return {
            form: {
                recipients: [],
                fwd_rec:[],
                subject: '',
                message: null,
                sender: this.defaultMailSettings.id,
                message_id: '',
                folder: '',
                text: '',
                tmplatt: ''
            },
            config: {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            },
            params: new FormData(),
            rules: {
                message: 'required',
                recipients: 'required',
                subject: 'required',
            },
            users: this.usersList,
            template: '',
            typeMail: 'text/html'
        }
    },
    props: {
        message: {
            default: () => {
                return {
                    attributes: {
                        message: '',
                        sender: null,
                        subject: '',
                        attachments: []
                    },

                }
            }
        },
        usersList: {
            default: []
        },
        mailsettings: {
            default: []
        },
        currentMailSettings: {
            default: () => {
                return {}
            }
        },
        defaultMailSettings: {
            default: {
                id: null
            }
        },
        folder: {
            default: null
        },
        rcpt: {
            default: ''
        }
    },
    watch: {
        'message.attributes.subject': function() {
            this.form.subject = 'Re:'+this.message.attributes.subject
        },
        'message.attributes.sender': function() {
            this.addTag(this.message.attributes.sender_email, this.message.attributes.sender);
        },
        'message.attributes.message': function() {
            this.form.message = this.message.attributes.message
        }
    },
    mounted() {
        if (this.message.attributes.sender) {
            this.addTag(this.message.attributes.sender_email, this.message.attributes.sender, false)
        }
        if (this.rcpt) {
            this.addTag(this.rcpt);
        }
        this.setRules();
    },
    methods: {
        forward() {
            this.form.sender = this.currentMailSettings.id;
            this.form.message_id = this.message.id;
            this.form.subject = this.message.attributes.subject;
            this.form.message = this.message.attributes.message;
            this.form.folder = this.folder;
            this.sendMessage();
        },

        selectTypeMail(type){
            this.typeMail = type;
        },
        replace(){
            this.form.message = this.form.text;
        },
        send() {
            this.params = new FormData;
            this.$validator.validateAll()
            .then(response => {
                if (this.$validator.errors.count()) {
                    this.onError();
                    return
                }
                var vm = this;
                if (this.$refs.attachments) {
                    _.forEach(this.$refs.attachments.files, function(file, index) {
                        vm.params.append('attachments[]', file);
                    })
                }
                this.sendMessage();
            });

        },
        sendMessage() {
            this.$root.pending = true;
            this.prepareData();
            axios.post('/test/send_custom', this.params, this.config)
            .then(response => {

                console.log(response);
                // window.history.back();
                // window.history.back();
                // this.onSuccess();
            })
            .catch(error => {
                this.onError(error);
            })
        },
        prepareData() {
            var vm = this;
            this.params.append('subject', this.form.subject);
            this.params.append('type_mail', this.typeMail);
            this.params.append('text', this.form.text);
            this.params.append('message', this.form.message);
            this.params.append('set_id', this.form.sender);
            this.params.append('message_id', this.form.message_id);
            this.params.append('folder', this.form.folder);
            this.params.append('tmplatt', this.form.tmplatt);
            this.params.append('tmplatt', this.form.text);
            this.params.append('tmplid', this.tmplid ? this.tmplid : '');
            _.forEach(this.form.recipients, function(item, index) {
                vm.params.append('recipients[]', item.mail);
            })
        },
        onSuccess() {
            this.$root.pending = false;
            this.$notify({ type: 'success', title: 'Success!', text: response.data.message ? response.data.message : 'Message sent' });
        },
        onError(error = null) {
            this.$root.pending = false;
            var text = 'An error has occured.'
            if (error) {
                var text = error.response.data.message;
            }
            this.$notify({ type: 'error', title: 'Error!', text: text });
        },
        addTag(newTag, label = null, push = true) {
            if (_.isNull(label)) {
                label = newTag
            }
            var sender = {
                full: label,
                host: null,
                mail: newTag,
                mailbox: null,
                personal: null,
            }
            if (push) {
                this.form.recipients.push(sender);
            }
            this.users.push(sender);
        },
        setRules() {
            var vm = this
            _.forEach(this.rules, function(rules, name) {
                vm.addField(name, rules);
            })
        },
        addField(name, rules) {
            this.$validator.attach({
                continues: true,
                name: name,
                rules: rules,
                getter: () => this.form[name]
            });
        }
    }
}
