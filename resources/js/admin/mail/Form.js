import AppForm from '../app-components/Form/AppForm';
import MailClient from './Client.js';
import TmplModal from './TmplModal.js';
import _ from "lodash";

Vue.component('mail-form', {
    mixins: [
        AppForm,
        MailClient,
    ],
    components: {
        TmplModal
    },
    data: function() {
        return {
            editorOption: {
                modules: {
                    toolbar: [
                        ['bold', 'italic', 'underline', 'strike'],
                        ['blockquote', 'code-block'],
                        [{ 'header': 1 }, { 'header': 2 }],
                        [{ 'list': 'ordered' }, { 'list': 'bullet' }],
                        [{ 'script': 'sub' }, { 'script': 'super' }],
                        [{ 'indent': '-1' }, { 'indent': '+1' }],
                        [{ 'direction': 'rtl' }],
                        [{ 'size': ['small', false, 'large', 'huge'] }],
                        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
                        [{ 'font': [] }],
                        [{ 'color': [] }, { 'background': [] }],
                        [{ 'align': [] }],
                        ['clean'],
                        ['link', 'image', 'video']
                    ],
                    syntax: {
                        highlight: text => hljs.highlightAuto(text).value
                    }
                }
            },
            templates: [],
            public: 0,
            tmpltitle: '',
            tmplid: null
        }
    },
    mounted() {
        this.getTemplates();
    },
    methods: {
        removeTmplAtt() {
            this.form.tmplatt = null;
        },
        getTemplates(){
            axios.get('/csmailclient/templates', {
                params: {'set_id': this.mailsettings.id}
            }).then( response => {
                this.templates = response.data;
            })
            .catch(error => {
                this.$notify({ type: 'error', title: 'Error!', text: error.response.data.message ? error.response.data.message : 'An error has occured.' });
            })
        },
        storeTemplate() {
            this.params = new FormData;
            this.params.append('title', this.tmpltitle);
            this.params.append('body', this.form.message);
            this.params.append('public', this.public);
            this.params.append('set_id', this.currentMailSettings.length ? this.currentMailSettings.id : this.defaultMailSettings.id);
            var vm = this;
            _.forEach(this.$refs.attachments.files, function(file, index) {
                vm.params.append('attachments[]', file);
            })
            if (this.template) {
                this.params.append('_method', 'PUT');
               axios.post('/csmailclient/templates/'+this.template, this.params, this.config)
                .then( response => {
                    this.$notify({ type: 'success', title: 'Success!', text: 'Template saved' });
                    this.$modal.hide('newTemplate');
                })
                .catch(error => {
                    this.$notify({ type: 'error', title: 'Error!', text: error.response.data.message ? error.response.data.message : 'An error has occured.' });
                })
            } else {
                axios.post('/csmailclient/templates', this.params, this.config)
                .then(response => {
                    this.$notify({ type: 'success', title: 'Success!', text: 'Template saved' });
                    this.$modal.hide('newTemplate');
                    this.getTemplates();
                })
                .catch(error => {
                    this.$notify({ type: 'error', title: 'Error!', text: error.response.data.message ? error.response.data.message : 'An error has occured.' });
                })
            }
        },
        setTemplate() {
            var vm = this;
            var tmpl = this._.filter(this.templates, function(o) { return o.id == vm.template; });
            if (tmpl[0]) {
                this.form.message =  tmpl[0].attributes.body;
                this.tmplid = tmpl[0].id;
                this.tmpltitle = tmpl[0].attributes.title;
                this.public = tmpl[0].attributes.mailsetting_id ? 0 : 1;
                this.form.tmplatt = tmpl[0].attributes.attachments;
            } else {
                this.form.message =  '';
                this.tmpltitle = '';
                this.public = 1;
                this.form.tmplatt = null;
                this.tmplid = null;
            }
        },
        destroyTemplate() {
            if (this.template) {
                var _this2 = this;
                this.$modal.show('dialog', {
                    title: 'Warning!',
                    text: 'Do you really want to delete this item?',
                    buttons: [{title: 'No, cancel.'}, {
                        title: '<span class="btn-dialog btn-danger">Yes, delete.<span>',
                        handler: function handler() {
                            _this2.$modal.hide('dialog');
                            axios.delete('/csmailclient/templates/' + _this2.template+'?set_id='+_this2.defaultMailSettings.id).then(function (response) {
                                _this2.form.message =  '';
                                _this2.tmpltitle = '';
                                _this2.public = 1;
                                _this2.getTemplates();
                                _this2.$notify({
                                    type: 'success',
                                    title: 'Success!',
                                    text: response.data.message ? response.data.message : 'Item successfully deleted.'
                                });
                            }, function (error) {
                                _this2.$notify({
                                    type: 'error',
                                    title: 'Error!',
                                    text: error.response.data.message ? error.response.data.message : 'An error has occured.'
                                });
                            });
                        }
                    }]
                });
            }
        }
    }
});
