import AppForm from '../app-components/Form/AppForm';

Vue.component('field-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                field_type_id: null,
                description: null,
                enabled: 0,
                name: null,
                searchable: 0,
                sortable: 0,
                visible: 0,
                exportable: 0,
                ordering: 0,
                rules: null,
            },
        }
    },
    mounted(){
        // if(this.form.rules) {
        //     this.form.rules = this.form.rules.split('|');
        //     this.form.rules_required = this.isRuleSet('required');
            
        // }else{
        //     this.form.rules = [];
        // }
        if(this.form.options) {
            this.form.options = _.map(this.form.options, function (option, optionValue) {
                console.log(option, optionValue)
                return {
                    value: optionValue,
                    text: option
                }
            });
        }
    },
    methods: {
        addRule(){ //переписываем все правила 
            let inputRule = "string";
            this.form.rules = [];
            if(this.form.rules_required) this.form.rules.push('required');
            else 
                this.form.rules.push('nullable');
            //пока правила по типам зашиты нужно будет переделать 
            //TODO брать правила из поля в БД
            if(this.form.field_type_id == 7) inputRule = 'numeric';
            else if(this.form.field_type_id == 6) inputRule = 'array';
            else if(this.form.field_type_id == 5) inputRule = 'boolean';
            else if(this.form.field_type_id == 4) inputRule = 'date';
            this.form.rules.push(inputRule);
        },
        isRuleSet(rule){
            let found = this.form.rules.find(function(element) {
                return element === rule;
            });
            if(found) return true;
            return false;
        },
        addOption(){
            this.form.options.push({});
            this.$forceUpdate();
        },
        removeOption(key){
            this.$delete(this.form.options, key);
            this.$forceUpdate();
        },
        save() {
            this.onSubmit();
        }
    }

});