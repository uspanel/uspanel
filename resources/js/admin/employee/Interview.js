Vue.component('employee-interview', {
    data() {
        return {
            formFields: null,
            form: {
                adult: null
            }
        }
    },
    props: {
        user: {
            type: Object,
            required: true,
            default: null
        },
        redirect: {
            type: String,
            required: false,
            default: null
        }
    },
    filters: {
        format: function (value) {
            if (value == 0) {
                return 'No'
            }
            if (value == 1) {
                return 'Yes'
            }
            return value
        }
    },
    mounted() {
        this.getForm();
    },
    methods: {
        getForm() {
            axios.get('/users/' + this.user.id + '/forms/interview')
            .then(response => {
                this.form = response.data.form
                this.formFields = response.data.fields
            })
            .catch(error => {

            })
            .finally(() => {

            })
        },
        onSuccess() {
            this.$notify({
                type: 'success',
                title: 'Result',
                text: 'Success'
            });
        },
        saveInterview() {
            this.errors.clear();
            axios.post('/users/' + this.user.id + '/employee/interview', this.form)
            .then(response => {
                this.onSuccess();
                if (this.redirect) {
                    window.location.href = this.redirect;
                }
            })
            .catch(error => {
                _.forEach(error.response.data.errors, (item, key) => {
                    this.errors.add({
                        field: key,
                        msg: _.head(item)
                    });
                })
            })
        },
        saveDraft() {
            return
        }
    }
});
