Vue.component('employee-contract', {
    data() {
    	return {
    		media: [],
    	}
    },
    props: {
        mediaCollection: {
            default: null
        },
	},
	mounted() {
		this.getMedia();
	},
    methods: {
        getMedia() {
            axios.get('/profile/media/'+this.mediaCollection)
            .then(response => {
                this.media = response.data.media
            })
        },
    	upload() {
    		if (_.isEmpty(this.$refs[this.mediaCollection+'_uploader'].getFiles())) {
    			return
    		}
    		axios.post('/profile/media/'+this.mediaCollection, this.$refs[this.mediaCollection+'_uploader'].getFiles())
    		.then(response => {
                this.onSuccess();
                this.getMedia();
    			this.$refs[this.mediaCollection+'_uploader'].$refs[this.mediaCollection].removeAllFiles()
    		})
    	},
    	getDownloadLink(mediaId) {
    		return '/profile/media/'+this.mediaCollection+'/'+mediaId
    	},
        onSuccess() {
            this.$notify({
                type: 'success',
                title: 'Result',
                text: 'Success'
            });
        },
    }
});
