Vue.component('employee-application', {
    data() {
        return {
            sent: false
        }
    },
    methods: {
        interviewByPhone() {
            axios.post('/employee/interview/phone')
            .then(response => {
                this.sent = true
            })
            .catch(error => {
                this.$notify({ type: 'error', title: 'Error!', text: errors.response.data.message})
            })
            .finally(() => {
            })
        }
    }
});
