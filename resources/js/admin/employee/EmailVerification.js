Vue.component('employee-email-verification', {
    data() {
    	return {
    		form: {
                email: this.email
            },
            loading: false
    	}
    },
    props: {
        email: {
            type: String,
            required: true,
            default: null
        },
	},
    methods: {
        resend() {
            this.loading = true
            axios.post('/resend-verification-email', this.form)
            .then(response => {
                this.$notify({ type: 'success', title: this.translate('admin.check_your_email'), text: response.data.message});
                this.$modal.hide('resendVerificationEmail')
            })
            .catch(error => {
                this.$notify({ type: 'error', title: 'Error!', text: errors.response.data.message})
            })
            .finally(() => {
                this.loading = false
            })
        }
    }
});
