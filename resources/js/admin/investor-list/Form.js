import AppForm from '../app-components/Form/AppForm';

Vue.component('investor-list-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                name:  '' ,
            }
        }
    },
    props: {
        investors: {
            default: () => {
                return []
            }
        }
    },


});
