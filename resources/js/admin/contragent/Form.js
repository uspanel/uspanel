import AppForm from '../app-components/Form/AppForm';

Vue.component('contragent-form', {
    mixins: [AppForm],
    props: {
        businessProcesses: {
            type: Array,
            default: () => []
        }
    },
    data: function() {
        return {
            form: {
                name:  '' ,
                investing_amount:  '' ,
                user_id:  '' ,
                country:  '' ,
                roi:  '' ,
                business_processes: []
            },
            selected_business_processes: [],
            business_processes: this.businessProcesses,

            mediaCollections: ['file_collection']
        }
    },
    watch: {
        selected_business_processes(processes) {
            if (processes instanceof Array) {
                let bp_ids = _.map(processes, 'id');
                bp_ids = [...bp_ids];
                this.$set(this.form, 'business_processes', bp_ids);
            }
        }
    },
    mounted() {
        this.selected_business_processes = _.get(this.data, 'business_processes', this.selected_business_processes);
    }
});
