import AppListing from '../app-components/Listing/AppListing';
import TableUpload from '../app-components/Listing/TableUpload';

Vue.component('sales-task-listing', {
    mixins: [AppListing,TableUpload],
    props: {
        users: [Object, Array],
        managers: [Object, Array],
        carriers: [Object, Array],
        warehouses: [Object, Array],
        employees: [Object, Array],
    },
    data() {
        return {
            filters: {
                warehouse_id: '',
                carrier_id: '',
                creator_id: '',
                manager_id: '',
            },
        }
    },
    methods: {
        forceDelivered(id) {
            axios.post('/admin/sales-tasks/' + id + '/forcedelivered').then(response => {
                this.loadData();
            });
        },
        checkAll(){
            var elements = document.getElementsByClassName('csv');
            var  c = elements.length
            var all_checkbox = document.getElementById('CheckAll');
            for (var d = 0; d < c; d++) {
                if (all_checkbox.checked == true) {
                    elements[d].checked = true;
                } else {
                    elements[d].checked = false;
                }
            }
        },
        async createCsv(){
            var elements = document.getElementsByClassName('csv');
            var ids = [];
            elements.forEach(function(el) {
                var id = el.getAttribute('value');
                if(el.checked == true){
                    ids.push(id);
                }
            });
            if(ids.length > 0){
                let res = await axios.post('/admin/sales-tasks/csv', { tasks_ids : ids } );
                this.uploadCSV(res.data.filename);
            }else{
                alert('One or more task must be checked!');
            }

        },
        uploadCSV(file_name){
            var element = document.createElement('a');
            element.setAttribute('download',"");
            element.style.display = 'none';
            document.body.appendChild(element);
            element.setAttribute('href', file_name);
            element.click();
            document.body.removeChild(element);
        }
    }
});
