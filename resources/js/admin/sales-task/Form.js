import AppForm from '../app-components/Form/AppForm';
import ItemSelect from '../item/Select.vue'
import UserSelectEmployee from '../user/SelectEmployee.vue'

Vue.component('sales-task-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                warehouse_id: this.warehouseId,
                sku: ''
            },
            employeeWarehouse: false,
            mediaCollections: [
                'label',
                'barcode',
                'receipt',
                'invoice',
                'content'
            ]
        }
    },
    props: {
        warehouseId: {
            type: [String,Number],
            required: false,
            default: null
        },
        carriers: [Object, Array],
        employees: [Object, Array],
        users: [Object, Array],
        managers: [Object, Array],
        courierStatuses: [Object, Array],
        warehouses: [Object, Array],
        warehousesList: [Object, Array],
        projects: [Object, Array],
        project: null,
        employee: null
    },
    components: {
        ItemSelect,
        UserSelectEmployee,
    },
    watch: {
        'form.warehouse_id': function() {
            if (this.data.id) return;
            this.$refs.itemSelect.getData('/admin/warehouses/' + this.form.warehouse_id + '/items/available');
        },
        'form.employee_id': function() {
            let warehouse = _.find(this.warehouses, (warehouse) => {
                if (warehouse.user) return warehouse.user.employee.id == this.form.employee_id;
            });
            if (warehouse) this.form.warehouse_id = warehouse.id;
        },
    },
    methods: {
        getPostData: function getPostData() {
            var _this3 = this;
            if (this.mediaCollections) {
                this.mediaCollections.forEach(function (collection, index, arr) {
                    if (_this3.form[collection]) {
                        console.warn("MediaUploader warning: Media input must have a unique name, '" + collection + "' is already defined in regular inputs.");
                    }

                    if (_this3.$refs[collection + '_uploader']) {
                        _this3.form[collection] = _this3.$refs[collection + '_uploader'].getFiles();
                    }
                });
            }
            this.form['wysiwygMedia'] = this.wysiwygMedia;
            if (this.$refs.itemSelect) {
                this.form.item_ids = this.$refs.itemSelect.selected;
                this.form.item_ids = _.keyBy(this.form.item_ids, 'id');
                this.form.item_ids = _.mapValues(this.form.item_ids, (o) => {
                    return {
                        qty: o.qty,
                        price: o.price,
                    }
                });
            }
            return this.form;
        },
        changeEmployee() {
            let warehouse = _.find(this.warehouses, { id: this.form.warehouse_id });
            if (warehouse && warehouse.user) this.form.employee_id = warehouse.user.employee.id;
        },
    }


});
