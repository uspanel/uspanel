import AppForm from '../app-components/Form/AppForm';

Vue.component('dkim-key-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                project_id:  '' ,
                domain:  '' ,
                dkim_record:  '' ,
                
            }
        }
    }

});