import AppListing from '../app-components/Listing/AppListing';

Vue.component('dkim-key-listing', {
    mixins: [AppListing]
});