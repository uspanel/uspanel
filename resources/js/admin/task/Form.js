import AppForm from '../app-components/Form/AppForm';
import ItemSelect from '../item/Select.vue'
import UserSelectEmployee from '../user/SelectEmployee.vue'

Vue.component('task-form', {
    mixins: [AppForm],
    props: {
        carriers: [Object, Array],
        employees: [Object, Array],
        users: [Object, Array],
        managers: [Object, Array],
        courierStatuses: [Object, Array],
        warehouses: [Object, Array],
        projects: [Object, Array],
        project: null,
        employee: null,
        employee_id: null,
        package_id: null
    },
    components: {
        ItemSelect,
        UserSelectEmployee
    },
    data: function() {
        return {
            form: {
                item_ids : this.data.item_ids,
                employee_id: null,
                package_id: this.package_id,
                redirect: (this.employee ? 'admin/roles/5/users' : null)
            },
            employeesData: this.employees,
            project_id: this.project ? this.project : (this.projects && this.projects[0] ? this.projects[0].id : null),
            mediaCollections: [
                'label',
                'barcode',
                'receipt',
                'invoice',
                'content'
            ]
        }
    },
    watch: {
        'form.employee_id': function() {
            if (this.data.id || this.form.package_id) return;
            this.$refs.itemSelect.getData('/admin/users/' + this.form.employee_id + '/items/available');
        },
    },
    methods: {
        getEmployees() {
            if (!this.project_id) {
                return;
            }
            this.form.employee_id = null
            axios.get('/projects/' + this.project_id + '/employees')
                .then(response => {
                    this.employeesData =  response.data.employees
                })
        },
        getPackageItems(){
            this.form.employee_id = this.employee_id
            this.$refs.itemSelect.getData('/admin/users/' + this.form.employee_id +'/'+this.form.package_id+'/package-items/available');
        },
        getPostData: function getPostData() {
            var _this3 = this;

            if (this.mediaCollections) {
                this.mediaCollections.forEach(function (collection, index, arr) {
                    if (_this3.form[collection]) {
                        console.warn("MediaUploader warning: Media input must have a unique name, '" + collection + "' is already defined in regular inputs.");
                    }

                    if (_this3.$refs[collection + '_uploader']) {
                        _this3.form[collection] = _this3.$refs[collection + '_uploader'].getFiles();
                    }
                });
            }
            this.form['wysiwygMedia'] = this.wysiwygMedia;
            if (this.$refs.itemSelect) {
                this.form.item_ids = _.keyBy(this.$refs.itemSelect.selected, 'id');
                this.form.item_ids = _.mapValues(this.form.item_ids, 'qty')
            }
            return this.form;
        },
    },
    mounted() {
        if (!this.form.employee_id) {
            this.getEmployees();
        }
        if ((this.employee_id || this.form.package_id) && !this.form.id){
            this.getPackageItems();
        }
    }

});
