import AppListing from '../app-components/Listing/AppListing';

Vue.component('activity-log-listing', {
    mixins: [AppListing],
    data: function () {
        return {
            pagination: {
                state: {
                    per_page: this.$cookie.get('per_page') || 10, // required
                    current_page: 1, // required
                    last_page: 1, // required
                    from: 1,
                    to: 10 // required
                },
                options: {
                    alwaysShowPrevNext: true
                }
            },
            orderBy: {
                column: 'id',
                direction: 'desc'
            },
            filters: {},
            search: '',
            collection: null
        };
    }
});