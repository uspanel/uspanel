import AppForm from '../app-components/Form/AppForm';

Vue.component('activity-log-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                user_id:  '' ,
                text:  '' ,
                ip_address:  '' ,
                
            }
        }
    }

});