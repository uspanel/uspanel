import AppListing from '../app-components/Listing/AppListing';

Vue.component('mail-template-listing', {
    mixins: [AppListing]
});