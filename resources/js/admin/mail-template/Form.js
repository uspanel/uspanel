import AppForm from '../app-components/Form/AppForm';

Vue.component('mail-template-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                title:  '' ,
                body:  '' ,
                
            },
            readmore : 0
        }
    }

});
