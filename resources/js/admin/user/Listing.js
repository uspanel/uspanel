import AppListing from '../app-components/Listing/AppListing';

Vue.component('user-listing', {
    mixins: [AppListing],
    props: {
        investor_lists: {
            default: () => []
        },
        investors: {
            default: () => []
        },
        contragent_lists: {
            default: () => []
        },
        contragents: {
            default: () => []
        },
        invest_tasks: {
            default: () => []
        }
    },
    data() {
        return {
            selected_contragents: [],
            selected_contragent_lists: [],
            selected_investors: [],
            selected_investor_lists: [],
            invest_task: {},
            colors: ['red', 'green', 'blue', 'orange', 'invisiblee'],
            info: {
                contractCollection: [],
                agreementCollection: [],
                deadline: null
            },
            filters: {
                roles: ''
            },
            empl: {
                employee: {}
            },
            chatId: null,
            userId: null,
            smsMessage: '',
            comment: null,
            currentItem: null,
            selectedItems: {},
            selectAll: false
        }
    },
    computed:{
        itemsSanitized: function(){
            var sanitized = [];
            _.forEach(this.selectedItems, function(o,id){
                if (o) {
                    sanitized.push(id);
                }
            });
            return sanitized;
        }
    },
    watch: {
        selectAll: function (val) {
            var vm = this;
            if (val) {
                _.forEach(this.collection, function(o,id){
                    Vue.set(vm.selectedItems, o.id, true);
                })
            } else {
                vm.selectedItems = {};
            }
        }
    },
    methods: {
        startChat(item) {
            if (item.chat_id) {
                window.location.href = '/chat/'+item.chat_id;
            } else {
                axios.post('/admin/users/gotochat', {'id': item.id}).then(response => {
                    window.location.href = '/chat/' + response.data.data.id;
                });
            }
        },
        toggleAll(){

        },
        toggleInfoModal(item) {
            this.$refs['bv-modal-attached-files'].toggle();
            this.currentItem = item;

            axios.get(`/admin/users/${item.id}/media/contract`).then(response => {
                this.$set(this.info, 'contractCollection', response.data.media);
            });
            axios.get(`/admin/users/${item.id}/media/agreement`).then(response => {
                this.$set(this.info, 'agreementCollection', response.data.media);
            });
        },
        toggleAddInvestorsModal(item) {
            this.$refs['bv-modal-add-investors'].toggle();
            this.currentItem = item;

            this.selected_investors = [...item.employee.investors];
            this.selected_investor_lists = [...item.employee.investor_lists];
        },
        toggleAddContragentsModal(item) {
            this.$refs['bv-modal-add-contragents'].toggle();
            this.currentItem = item;

            this.selected_contragents = [...item.employee.contragents];
            this.selected_contragent_lists = [...item.employee.contragent_lists];
        },
        toggleAddInvestTaskModal(item = null) {
            this.$refs['bv-modal-add-invest-task'].toggle();
            if (item) {
                Vue.set(this.selectedItems,item.id,true);
            }
        },
        handleAddInvestorsModal() {
            axios.put(`/employees/${this.currentItem.employee.id}/sync`, {
                investors: _.map(this.selected_investors, 'id'),
                investor_lists: _.map(this.selected_investor_lists, 'id'),
            }).then(() => {
                this.$notify({
                    type: 'success',
                    text: 'success',
                })
            }).error(() => {
                this.$notify({
                    type: 'error',
                    text: 'error',
                })
            });
        },
        handleAddContragentsModal() {
            axios.put(`/employees/${this.currentItem.employee.id}/sync`, {
                contragents: _.map(this.selected_contragents, 'id'),
                contragent_lists: _.map(this.selected_contragent_lists, 'id'),
            }).then(() => {
                this.$notify({
                    type: 'success',
                    text: 'success',
                })
            }).error(() => {
                this.$notify({
                    type: 'error',
                    text: 'error',
                })
            });
        },
        handleAddInvestTaskModal() {
            var emplParam = '?';
            _.forEach(this.itemsSanitized, function(o){
                emplParam +='empl[]='+o+'&';
            });
            if (this.invest_task.id) {
                window.location.href = '/admin/invest-tasks/create'+emplParam+'invest_task_template_id=' + _.get(this, 'invest_task.id', '');
            } else {
                window.location.href = '/admin/invest-tasks/create'+emplParam;
            }
        },

        sendSms() {
            this.sendCustomSms(this.userId, this.smsMessage);
            this.smsMessage = '';
            this.$modal.hide('smsModal');
        },
        editEmpl(item) {
            this.empl = item;
            this.$modal.show('processEmpl')
        },
        updateEmpl() {
            this.$modal.hide('processEmpl');
            axios.put('/admin/users/' + this.empl.id, this.empl).then(response => {
                this.$notify({type: 'success', title: 'Success'});
                this.loadData();
            }).catch(error => {
                this.$notify({type: 'error', title: 'Error!', text: error.response.data.message})
                this.loadData();
            });
        },
        takeEmpl(item) {
            this.$modal.hide('processEmpl');
            axios.post('/admin/users/' + item.id + '/take').then(response => {
                this.$notify({type: 'success', title: 'Success'});
                this.loadData();
            }).catch(error => {
                this.$notify({type: 'error', title: 'Error!', text: error.response.data.message})
                this.loadData();
            });
        },
        openChat(chatId) {
            this.chatId = chatId
            this.$modal.show('chat')
        },
        smsModal(userId) {
            this.userId = userId;
            this.$modal.show('smsModal')
        },
        updateApproved(id, approved) {
            axios.post('/admin/users/' + id + '/approved', {
                approved: approved
            })
            .then(response => {

            })
        },
        showComment(comment) {
            this.comment = comment
            this.$modal.show('comment')
        },
        painRow(id, color){
            for(let item of this.collection){
                if(item.id == id){
                    item.employee.color = color;
                }
            }
            axios.post('/admin/pain_row',{
                user_id: id,
                color: color,
            }).then(response => {

            });
        }
    }
});
