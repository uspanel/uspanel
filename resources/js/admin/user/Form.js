import AppForm from '../app-components/Form/AppForm';

Vue.component('user-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                profile: {},
                investor_profile: {},
                employee: _.get(this.data, 'employee', {}),
                investor_mail: {
                    smtp_host:  '' ,
                    smtp_port:  '' ,
                    smtp_encryption: null,
                    imap_host:  '' ,
                    imap_port:  '' ,
                    imap_encryption: null,
                    imap_sent_folder:  '' ,
                    imap_inbox_folder:  '' ,
                    login:  '' ,
                    password:  '' ,
                    default: false,
                    validate_cert: false,
                },
            },
            extraForm: {
                employee: {
                    staffers: this.extra.form.employee.staffers,
                    hr_id: this.extra.form.employee.hr_id,
                    support_id: this.extra.form.employee.support_id,
                },
                employees: this.extra.form.employees
            },
            contract: [],
            contractCollection: 'contract',
            agreement: [],
            agreementCollection: 'agreement',
            folders: [],
            loading: false,
            role_id: _.get(this, 'data.role_id', null),
        }
    },
    computed: {
        hr() {
            console.log('computed hr!');

            let hr = [];
            if (this.role_id == 5) {
                hr = [...this.hr_manager_staff_list];
            } else if (this.role_id == 6) {
                hr = [...this.hr_manager_invest_list];
            }
            return hr;
        },
        support() {
            console.log('computed supp!');

            let support = [];
            if (this.role_id == 5) {
                support = [...this.support_staff_list];
            } else if (this.role_id == 6) {
                support = [...this.support_invest_list];
            }
            return support;
        },
        employees() {
            console.log('computed emp!');
            console.log('role ' + this.role_id);

            let employees = [];
            if (this.role_id == 4 || this.role_id == 5) {
                employees = this.employees_staff.map((employee_user) => {
                    return {
                        id: employee_user.employee.id,
                        name: employee_user.name
                    };
                });
            }
            return employees;
        },
    },
    mounted() {
        if (this.form.profile.state_id) {
            var vm = this;
            this.form.profile.state_id = _.filter(this.states, function(o){
                return o.id == vm.form.profile.state_id;
            })[0];
        }
        if (this.data.id) {
            this.getContract();
            this.getAgreement();
        }
        if (this.form.investor_mail) {
            this.pushCurrentFolders(this.form.investor_mail.imap_sent_folder);
            this.pushCurrentFolders(this.form.investor_mail.imap_inbox_folder);
        }
    },
    props: {
        employees_staff: {
            default: () => {
                return []
            }
        },
        employees_invest: {
            default: () => {
                return []
            }
        },
        staffers: {
            default: () => {
                return []
            }
        },
        extra: {
            type: Object,
            required:false,
            default: () => {
                return {
                    form: {
                        employee: {
                            staffers: null,
                            hr_id: null,
                            support_id: null
                        },
                        employees: []
                    }
                }
            }
        },
        investors: {
            default: () => {
                return []
            }
        },
        contragents: {
            default: () => {
                return []
            }
        },
        investor_lists: {
            default: () => {
                return []
            }
        },
        contragent_lists: {
            default: () => {
                return []
            }
        },

        hr_manager_staff_list: {
            default: () => {
                return []
            }
        },
        hr_manager_invest_list: {
            default: () => {
                return []
            }
        },
        support_staff_list: {
            default: () => {
                return []
            }
        },
        support_invest_list: {
            default: () => {
                return []
            }
        },
        states: {
            type: [Object,Array],
            required: true
        }
    },
    methods: {
        pushCurrentFolders(value) {
            if (value) {
                this.folders.push({
                    attributes: {
                        name: value
                    }
                });
            }
        },
        getFolders() {
            this.loading = true;
            var params = this.form.investor_mail;
            params.force_settings = 1;
            this.$validator.validateAll([
                'investor_mail.smtp_host',
                'investor_mail.smtp_port',
                'investor_mail.smtp_encryption',
                'investor_mail.imap_host',
                'investor_mail.imap_port',
                'investor_mail.imap_encryption',
                'investor_mail.login',
                'investor_mail.password',
            ])
                .then(response => {
                    if (this.$validator.errors.count() == 0) {
                        axios.get('/csmailclient/folders', {
                            params: params
                        })
                            .then(response => {
                                this.folders = response.data.data
                                this.form.investor_mail.imap_inbox_folder = _.head(this.folders).attributes.name
                                this.form.investor_mail.imap_sent_folder = _.head(this.folders).attributes.name
                                this.loading = false;
                            })
                            .catch(error => {
                                this.loading = false;
                            })
                    } else {
                        this.loading = false;
                    }
                });
        },
        getContract() {
            axios.get('/admin/users/'+this.data.id+'/media/'+this.contractCollection)
            .then(response => {
                this.contract = response.data.media
            })
        },
        getAgreement() {
            axios.get('/admin/users/'+this.data.id+'/media/'+this.agreementCollection)
            .then(response => {
                this.agreement = response.data.media
            })
        },
        getDownloadLink(collection,mediaId) {
            return '/admin/users/'+this.data.id+'/media/'+collection+'/'+mediaId
        },
        toBlacklist() {
            axios.post('/admin/users/'+this.data.id+'/blacklist')
            .then(response => {
                window.location = '/admin/users';
            })
        },
        onSubmit() {
            this.errors.items = [];
            var _this4 = this;
            var data = _this4.form;
            if (data.profile.state_id) {
                data.profile.state_id = data.profile.state_id.id;
            }
            if (!_this4.sendEmptyLocales) {
                data = _.omit(_this4.form, _this4.locales.filter(function (locale) {
                    return _.isEmpty(_this4.form[locale]);
                }));
            }

            _this4.submiting = true;
            console.log(_this4.getPostData());
            axios.post(_this4.action, _this4.getPostData())
            .then(function (response) {
                return _this4.onSuccess(response.data);
            })
            .catch(function (errors) {
                for (var prop in errors.response.data.errors) {
                    this.errors.items.push({ field: prop, msg:errors.response.data.errors[prop][0], scope:null, vmId:18 });
                }
                return _this4.onFail(errors.response.data.errors);
            });
        },
        getPostData() {
            var _this3 = this;

            if (this.mediaCollections) {
                this.mediaCollections.forEach(function (collection, index, arr) {
                    if (_this3.form[collection]) {
                        console.warn("MediaUploader warning: Media input must have a unique name, '" + collection + "' is already defined in regular inputs.");
                    }

                    if (_this3.$refs[collection + '_uploader']) {
                        _this3.form[collection] = _this3.$refs[collection + '_uploader'].getFiles();
                    }
                });
            }
            this.form['wysiwygMedia'] = this.wysiwygMedia;
            this.form = Object.assign(
                this.form,
                {
                    employees: _.map(this.extraForm.employees, 'id')
                },
            );
            if (this.extraForm.employee.staffers) {
                this.form.employee.staffers = _.map(this.extraForm.employee.staffers, 'id');
                this.form.employee.hr_id = this.extraForm.employee.hr_id ? this.extraForm.employee.hr_id.id : null;
                this.form.employee.support_id = this.extraForm.employee.support_id ? this.extraForm.employee.support_id.id : null;
            }
            return this.form;
        },
    },
    watch: {
        role_id(new_role_id, old_role_id) {
            console.log('new ' + new_role_id, 'old ' + old_role_id);
            this.$set(this.form, 'role_id', new_role_id);

            if (new_role_id !== old_role_id) {
                if (new_role_id == 4) {
                    this.$set(this.extraForm, 'employees', null);

                } else if (new_role_id == 5) {
                    this.$set(this.extraForm, 'hr_id', null);
                    this.$set(this.extraForm, 'support_id', null);
                } else if (new_role_id == 6) {
                    this.$set(this.extraForm, 'hr_id', null);
                    this.$set(this.extraForm, 'support_id', null);
                }
            }
        },
    }

});
