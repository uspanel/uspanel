import AppForm from '../app-components/Form/AppForm';

Vue.component('role-form', {
    mixins: [AppForm],
    props: {
        permissions: Object
    },
    data: function() {
        return {
            form: {
                name:  '' ,
                guard_name:  '',
                permission_ids: []
            }
        }
    },
    methods:{
        showBlock(id){
            return _.includes(this.form.permission_ids, id);
        },
        checkedAllCheckbox(name, type){
            let childsIds = _.map(this.permissions[name].columns, type);
            let selectedIds = _.sortBy(_.intersection(this.form.permission_ids, childsIds));
            return _.isEqual(selectedIds, childsIds);
        },
        checkAll(name, type, checked){
            let childsIds = _.map(this.permissions[name].columns, type);
            if(checked){
                this.form.permission_ids = _.union(this.form.permission_ids, childsIds);
            }else{
                this.form.permission_ids = _.difference(this.form.permission_ids, childsIds);
            }
            return false;
        }
    }
});