import AppListing from '../app-components/Listing/AppListing';

Vue.component('email-group-listing', {
    mixins: [AppListing]
});