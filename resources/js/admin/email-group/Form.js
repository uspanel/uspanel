import AppForm from '../app-components/Form/AppForm';

Vue.component('email-group-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                name:  '',
                list: ''
            },
            mediaCollections: ['list']
        }
    }

});