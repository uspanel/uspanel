import {clone} from "bootstrap-vue/esm/utils/object";

export default {
	data() {
		return {
			form: {
				first_name: null,
				last_name: null,
				phone: null,
				name: null,
				email: null,
				password: null,
				password_confirmation: null,
				project_id: this.project.id
			},
			profile: {
				higher_education: 0,
				authorized_to_work: 0,
				adult: 0,
				referrer: document.referrer
			},
			cv: null,
			showProfileForm: false,
			code: null,
			fails: {},
			countdownEnd: false,
			document: document
		}
	},
	mounted() {

	},
	props: {
		project: {
			default: () => {
				return {}
			}
		}
	},
	methods: {
		sendVerificationCode() {
			this.fails = {};
			axios.post('/register/verification/send', this.form)
			.then(response => {
				this.$modal.show('verifyCode');
				this.countdownEnd = false;
			})
			.catch(error => {
				this.fails = _.isUndefined(error.response.data.errors) ? error.response.data : error.response.data.errors
			})
		},
		verifyPhone() {
			this.fails = {};
			axios.post('/register/phone/verify', Object.assign(this.form, { code: this.code }))
			.then(response => {
				this.$modal.hide('verifyCode');
				this.showProfileForm = true;
			})
			.catch(error => {
				this.fails = error.response.data.errors
			})
		},
		register() {
		    var prof = clone(this.profile);
            if (prof.state_id) {
                prof.state_id = prof.state_id.id;
            }
			this.cv = this.$refs['cv_uploader'].getFiles();
			axios.post('/register', Object.assign(this.form, { profile: prof, cv: this.cv }))
			.then(response => {
				window.location = '/employee/documents';
			})
			.catch(error => {
			    console.log('ERR',error.response.data);
				this.fails = _.isUndefined(error.response.data.errors) ? error.response.data : error.response.data.errors
                console.log('F', this.fails['profile.zip']);
                console.log('H', _.head(this.fails['profile.zip']));
			})
		},
		end() {
			this.countdownEnd = true;
		}
	}
}
