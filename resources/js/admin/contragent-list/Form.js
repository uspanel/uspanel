import AppForm from '../app-components/Form/AppForm';

Vue.component('contragent-list-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                name:  '' ,
            }
        }
    },
    props: {
        contragents: {
            default: () => {
                return []
            }
        }
    },

});
