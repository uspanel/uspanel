
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
console.log("%c##############################\n" +
    "#########===========##########\n" +
    "######=================#######\n" +
    "####============**=======#####\n" +
    "###======++====+ :=++=====+###\n" +
    "##=====*..+====-.==: .+====###\n" +
    "##===*..+=====* *====:..+===##\n" +
    "##=+..+=======.-=======: -==##\n" +
    "##===: -+====: +=====+..*===##\n" +
    "##=====: -+=+.:====+..*====+##\n" +
    "###======:*=-.=====:*======###\n" +
    "####=======+.*============####\n" +
    "######==================######\n" +
    "#########============#########\n" +
    "##############################\n", 'color: green;font-weight:bold;');
console.log("%c Site: https://coders.studio", 'color: green;font-weight:bold;');
console.log("%c Mail: info@coders.studio",'color: green;font-weight:bold;');
console.log("%c Developed with \u2665. 2019",'color: green;font-weight:bold;');





require('./bootstrap');

window.Vue = require('vue');

import Notifications from 'vue-notification'
import VModal from 'vue-js-modal'
import lodash from 'lodash';
import VueDraggableResizable from 'vue-draggable-resizable'
import 'vue-draggable-resizable/dist/VueDraggableResizable.css'
import VueUploadComponent from 'vue-upload-component'
import VueImg from 'v-img';

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('chat', require('../assets/vendor/CodersStudioChat/components/Chat.vue').default);
Vue.component('vue-draggable-resizable', VueDraggableResizable)
Vue.component('file-upload', VueUploadComponent)
Object.defineProperty(Vue.prototype, '_', { value: lodash });
Vue.use(Notifications)
Vue.use(VModal, { dialog: true });
Vue.use(VueImg);
Vue.config.devtools = true;
const app = new Vue({

    el: '#app',
    mounted() {

    },
});
