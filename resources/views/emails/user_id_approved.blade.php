<div>
	@lang('admin.employee.notifications.user_id_approved.body')
</div>
<div>
	<a href="https://www.irs.gov/pub/irs-pdf/fw4.pdf">@lang('admin.employee.notifications.user_id_approved.download_contract')</a>
</div>
<div>
	<a href="{{ route('employee.documents') }}">@lang('admin.employee.notifications.user_id_approved.upload_contract')</a>
</div>