<div id="menu" class="side-nav gx-sidebar">
    <div class="navbar-expand-lg">
        <div class="sidebar-header">
            <a class="site-logo" href="#">
                {{ env('APP_NAME') }}
            </a>
        </div>

            <div id="main-menu" class="main-menu navbar-collapse collapse">
                <ul class="nav-menu">
                    @can('sidebar')
                        Test complete
                    @include('brackets/admin-ui::admin.layout.menu-item', [
                        'url' => route('admin.role.user', ['role' => 5]),
                        'name' => trans('admin.user.employees'),
                        'gate' => 'crud.user.view',
                        'icon' => 'zmdi zmdi-account zmdi-hc-fw'
                    ])
                    @include('brackets/admin-ui::admin.layout.menu-item', [
                        'url' => url('admin/mails'),
                        'name' => trans('admin.mail.title'),
                        'gate' => 'crud.mailsetting.view',
                        'icon' => 'zmdi zmdi-email zmdi-hc-fw'
                    ])
                    @endcan
                    @include('brackets/admin-ui::admin.layout.menu-item', [
                        'url' => url('chat'),
                        'name' => trans("chat.name"),
                        'gate' => 'crud.chatroom.view',
                        'icon' => 'zmdi zmdi-comments zmdi-hc-fw'
                    ])
                    @can('sidebar')
                    @include('brackets/admin-ui::admin.layout.menu-item', [
                        'url' => url('admin/packages'),
                        'name' => trans('admin.package.title'),
                        'gate' => 'crud.package.view',
                        'icon' => 'zmdi zmdi-truck zmdi-hc-fw'
                    ])
                    @include('brackets/admin-ui::admin.layout.menu-item', [
                        'url' => auth()->user()->hasRole('Employee Staff') ? url('tasks') : url('admin/tasks'),
                        'name' => trans('admin.task.title'),
                        'gate' => 'crud.task.view',
                        'icon' => 'zmdi zmdi-assignment-check zmdi-hc-fw'
                    ])
                    @include('brackets/admin-ui::admin.layout.menu-item', [
                        'url' => auth()->user()->hasRole('Employee Invest') ? url('invest-tasks') : url('admin/invest-tasks'),
                        'name' => trans('admin.invest-tasks.title'),
                        'gate' => 'crud.invest-tasks.view',
                        'icon' => 'zmdi zmdi-assignment-check zmdi-hc-fw'
                    ])
                    @include('brackets/admin-ui::admin.layout.menu-item', [
                        'url' => url('admin/sales-tasks'),
                        'name' => trans('admin.sales-task.title'),
                        'gate' => 'crud.task.create',
                        'icon' => 'zmdi zmdi-money zmdi-hc-fw'
                    ])
                    @if(
                        (!auth()->user()->hasRole('Employee Staff'))
                        || (auth()->user()->hasRole('Employee Staff') && (auth()->user()->warehouses()->count() > 0))
                    )
                        @include('brackets/admin-ui::admin.layout.menu-item', [
                            'url' => url('admin/warehouses'),
                            'name' => trans('admin.warehouse.title'),
                            'gate' => 'crud.warehouse.view',
                            'icon' => 'zmdi zmdi-apps zmdi-hc-fw'
                        ])
                    @endif
                    <li class="menu @if(
                        url()->current() == url('admin/users')
                        || url()->current() == url('admin/projects')
                        || url()->current() == route('admin.role.user', ['role' => 4])
                        || url()->current() == url('admin/investor-lists')
                        || url()->current() == url('admin/help-videos')
                        || url()->current() == url('admin/history')
                        || url()->current() == route('user.statistics')
                        || url()->current() == route('admin.businessprocess.index')
                        || url()->current() == url('admin/users/needcalls')
                        || url()->current() == url('admin/statistics')
                        )
                            open
                        @endif">
                        <a href="javascript:void(0)">
                            <!--<i class="zmdi zmdi-email"></i>-->
                            <span class="nav-text">@lang('admin.other')</span>
                        </a>
                        <ul class="sub-menu">
                            @include('brackets/admin-ui::admin.layout.menu-item', [
                                'url' => url('admin/projects'),
                                'name' => trans('admin.project.title'),
                                'gate' => 'crud.project.view',
                                'icon' => 'zmdi zmdi-apps zmdi-hc-fw'
                            ])
                            @include('brackets/admin-ui::admin.layout.menu-item', [
                                'url' => route('admin.role.user', ['role' => 4]),
                                'name' => trans('admin.user.staffers'),
                                'gate' => 'crud.user.columns.staffer.read',
                                'icon' => 'zmdi zmdi-account zmdi-hc-fw'
                            ])
                            @include('brackets/admin-ui::admin.layout.menu-item', [
                                'url' => url('admin/investor-lists'),
                                'name' => trans('admin.investor-list.title'),
                                'gate' => 'crud.investor-list.index',
                                'icon' => 'zmdi zmdi-view-stream zmdi-hc-fw'
                            ])
                            @include('brackets/admin-ui::admin.layout.menu-item', [
                                'url' => url('admin/help-videos'),
                                'name' => trans('admin.help-video.title'),
                                'gate' => 'crud.help_video.view',
                                'icon' => 'zmdi zmdi-play zmdi-hc-fw'
                            ])
                            @include('brackets/admin-ui::admin.layout.menu-item', [
                                'url' => url('admin/history'),
                                'name' => trans('admin.history.title'),
                                'gate' => 'crud.activity-log.view',
                                'icon' => 'zmdi zmdi-view-stream zmdi-hc-fw'
                            ])
                            @include('brackets/admin-ui::admin.layout.menu-item', [
                                'url' => route('user.statistics'),
                                'name' => trans('admin.stats_and_pay'),
                                'gate' => 'user.statistics',
                                'icon' => 'zmdi zmdi-chart zmdi-hc-fw'
                            ])
                            @include('brackets/admin-ui::admin.layout.menu-item', [
                                'url' => route('admin.businessprocess.index'),
                                'name' => trans('admin.businessprocess.short_title'),
                                'gate' => 'crud.business-process.view',
                                'icon' => 'zmdi zmdi-circle zmdi-hc-fw'
                            ])
                            @if(!auth()->user()->hasRole('Administrator'))
                            @include('brackets/admin-ui::admin.layout.menu-item', [
                                'url' => url('admin/users/needcalls'),
                                'name' => trans('admin.needcalls.title'),
                                'gate' => 'crud.user.callsupport',
                                'icon' => 'zmdi zmdi-phone zmdi-hc-fw'
                            ])
                            @endif
                            @include('brackets/admin-ui::admin.layout.menu-item', [
                                'url' => url('admin/statistics'),
                                'name' => trans('admin.statistic.title'),
                                'gate' => 'crud.statistic.view',
                                'icon' => 'zmdi zmdi-chart zmdi-hc-fw'
                            ])
                            @can('crud.mailing.view')
                            <li class="menu @if(
                            url()->current() == url('admin/users')
                            || url()->current() == url('admin/mailings')
                            || url()->current() == url('admin/email-groups')
                            || url()->current() == url('admin/email-mailings')
                            )
                                open
                            @endif">
                                <a href="javascript:void(0)">
                                    <i class="zmdi zmdi-email zmdi-hc-fw"></i>
                                    <span class="nav-text">@lang('admin.response')</span>
                                </a>
                                <ul class="sub-menu">
                                    @include('brackets/admin-ui::admin.layout.menu-item', [
                                        'url' => url('admin/mailings'),
                                        'name' => trans('admin.mailing.title'),
                                        'gate' => 'crud.mailing.view',
                                        'icon' => null
                                    ])

                                    @include('brackets/admin-ui::admin.layout.menu-item', [
                                        'url' => url('admin/email-groups'),
                                        'name' => trans('admin.email-group.title'),
                                        'gate' => 'crud.email-group.view',
                                        'icon' => null
                                    ])
                                    @include('brackets/admin-ui::admin.layout.menu-item', [
                                        'url' => url('admin/email-mailings'),
                                        'name' => trans('admin.email-mailing.title'),
                                        'gate' => 'crud.email-mailing.view',
                                        'icon' => null
                                    ])
                                </ul>
                            </li>
                            @endcan
                            @if(!auth()->user()->hasRole('Call support'))
                            <li class="menu @if(
                            url()->current() == url('admin/users')
                            || url()->current() == url('admin/users/needcalls')
                            || url()->current() == url('admin/roles')
                            || url()->current() == url('admin/mail-templates')
                            || url()->current() == url('admin/mailsettings')
                            || url()->current() == url('admin/forms')
                            || url()->current() == url('admin/settings')
                            || url()->current() == url('admin/items')
                            || url()->current() == url('admin/nomenclatures')
                            )
                            open
                            @endif">
                            <a href="javascript:void(0)">
                                <i class="zmdi zmdi-settings zmdi-hc-fw"></i>
                                <span class="nav-text">@lang('brackets/admin-ui::admin.sidebar.settings')</span>
                            </a>
                            <ul class="sub-menu">
                                @include('brackets/admin-ui::admin.layout.menu-item', [
                                    'url' => url('admin/items'),
                                    'name' => trans('admin.item.title'),
                                    'gate' => 'crud.item.view',
                                    'icon' => null,
                                    'hideFor' => ['Employee Staff','Employee Invest']
                                ])
                                @include('brackets/admin-ui::admin.layout.menu-item', [
                                    'url' => url('admin/users'),
                                    'name' => trans('admin.user.title'),
                                    'gate' => 'crud.user.view',
                                    'icon' => null
                                ])
                                @include('brackets/admin-ui::admin.layout.menu-item', [
                                    'url' => url('admin/roles'),
                                    'name' => trans('admin.role.title'),
                                    'gate' => 'crud.role.view',
                                    'icon' => null
                                ])
                                @include('brackets/admin-ui::admin.layout.menu-item', [
                                    'url' => url('admin/mail-templates'),
                                    'name' => trans('admin.mail-template.title'),
                                    'gate' => 'crud.mail_template.modify',
                                    'icon' => null
                                ])
                                @include('brackets/admin-ui::admin.layout.menu-item', [
                                    'url' => url('admin/mailsettings'),
                                    'name' => trans('admin.mailsetting.title'),
                                    'gate' => 'crud.mailsetting.view',
                                    'icon' => null
                                ])
                                @include('brackets/admin-ui::admin.layout.menu-item', [
                                    'url' => url('admin/forms'),
                                    'name' => trans('admin.form.title'),
                                    'gate' => 'crud.form.view',
                                    'icon' => null
                                ])
                                @include('brackets/admin-ui::admin.layout.menu-item', [
                                    'url' => url('admin/settings'),
                                    'name' => trans('admin.setting.title'),
                                    'gate' => 'crud.email-mailing.view',
                                    'icon' => null
                                ])
                            </ul>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endcan
                </ul>
            </div>

    </div>
</div>
