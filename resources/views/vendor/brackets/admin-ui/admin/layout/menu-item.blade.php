@can($gate)
    @if (empty($hideFor) || !Auth::user()->hasRole($hideFor))
        <li class="menu no-arrow @if(url()->current() === $url) selected active @endif">
            <a href="{{ $url }}">
                @if($icon)
                    <i class="{{ $icon }}"></i>
                @endif
                <span class="nav-text">{{ $name }}</span>
            </a>
        </li>
    @endif
@endcan
