<div id="menu" class="side-nav gx-sidebar">
    <div class="navbar-expand-lg">
        <div class="sidebar-header">
            <a class="gx-menu-icon menu-toggle">
                <span class="menu-icon icon-grey"></span>
            </a>
        </div>

        <div id="main-menu" class="main-menu navbar-collapse collapse">
            <ul class="nav-menu">
                @if(true)
                    @can('sidebar')


                        @if(!auth()->user()->hasRole('Employee Invest')
                        && (!auth()->user()->hasRole('Administrator'))
                        && (!auth()->user()->hasRole('HR Manager Invest'))
                        && (!auth()->user()->hasRole('Support Invest'))
                        && (!auth()->user()->hasRole('Support Invest'))
                        && (!auth()->user()->hasRole('Investor'))
                        )
                            @include('brackets/admin-ui::admin.layout.menu-item', [
                                'url' => route('admin.role.user', ['role' => 5]),
                                'name' => trans('admin.user.employees'),
                                'gate' => 'crud.user.view',
                                'icon' => 'zmdi zmdi-account zmdi-hc-fw'
                            ])
                        @endif

                        @if(!auth()->user()->hasRole('Employee Invest'))
                            @include('brackets/admin-ui::admin.layout.menu-item', [
                                'url' => route('admin.role.user', ['role' => 6]),
                                'name' => trans('admin.user.invest_employees'),
                                'gate' => 'crud.user.view',
                                'icon' => 'zmdi zmdi-account zmdi-hc-fw',
                                'hideFor' => ['HR Manager Staff', 'Staffer', 'Support Staff']
                            ])
                        @endif


                        @if(auth()->user()->hasRole('Administrator')
                            && (auth()->user()->hasRole('Support Staff'))
                            && (auth()->user()->hasRole('Support Invest'))
                            && (auth()->user()->hasRole('HR Manager Invest'))
                            && (auth()->user()->hasRole('HR Manager Staff'))
                            && (auth()->user()->hasRole('Employee Staff'))
                            )

                            @include('brackets/admin-ui::admin.layout.menu-item', [
                                'url' => url('admin/mails'),
                                'name' => trans('admin.mail.title'),
                                'gate' => 'crud.mailsetting.view',
                                'icon' => 'zmdi zmdi-email zmdi-hc-fw'
                            ])

                        @endif


                    @endcan
                    @include('brackets/admin-ui::admin.layout.menu-item', [
                        'url' => url('chat'),
                        'name' => trans("chat.name"),
                        'gate' => 'crud.chatroom.view',
                        'icon' => 'zmdi zmdi-comments zmdi-hc-fw'
                    ])
                    @can('sidebar')

                        @if(!auth()->user()->hasRole('Employee Invest')
                        && (!auth()->user()->hasRole('Administrator'))
                        && (!auth()->user()->hasRole('HR Manager Invest'))
                        && (!auth()->user()->hasRole('Support Invest'))
                        && (!auth()->user()->hasRole('Support Invest'))
                        && (!auth()->user()->hasRole('Investor'))
                        )

                            @include('brackets/admin-ui::admin.layout.menu-item', [
                                'url' => url('admin/packages'),
                                'name' => trans('admin.package.title'),
                                'gate' => 'crud.package.view',
                                'icon' => 'zmdi zmdi-truck zmdi-hc-fw'
                            ])
                            @include('brackets/admin-ui::admin.layout.menu-item', [
                                'url' => url('admin/tasks'),
                                'name' => trans('admin.task.title'),
                                'gate' => 'crud.task.view',
                                'icon' => 'zmdi zmdi-assignment-check zmdi-hc-fw'
                            ])

                        @endif


                        @if((!auth()->user()->hasRole('Employee Staff')))

                            @include('brackets/admin-ui::admin.layout.menu-item', [
                                'url' => url('/admin/invest-tasks'),
                                'name' => trans('admin.invest-task.title'),
                                'gate' => 'crud.invest-task.view',
                                'icon' => 'zmdi zmdi-assignment-check zmdi-hc-fw',
                                'hideFor' => ['HR Manager Staff', 'Staffer', 'Support Staff']
                            ])

                        @endif

                        @include('brackets/admin-ui::admin.layout.menu-item', [
                            'url' => url('/admin/invest-task-templates'),
                            'name' => trans('admin.invest-task-template.title_short'),
                            'gate' => 'crud.invest-task-template.view',
                            'icon' => 'zmdi zmdi-assignment-check zmdi-hc-fw',
                            'hideFor' => ['HR Manager Staff', 'Staffer', 'Support Staff']
                        ])
{{--                        @include('brackets/admin-ui::admin.layout.menu-item', [--}}
{{--                            'url' => url('admin/sales-tasks'),--}}
{{--                            'name' => trans('admin.sales-task.title'),--}}
{{--                            'gate' => 'crud.task.create',--}}
{{--                            'icon' => 'zmdi zmdi-money zmdi-hc-fw',--}}
{{--                            'hideFor' => ['Staffer']--}}
{{--                        ])--}}
                        @if((!auth()->user()->hasRole('Employee Staff'))
                         && (!auth()->user()->hasRole('Administrator'))
                         && (!auth()->user()->hasRole('HR Manager Invest'))
                         && (!auth()->user()->hasRole('Support Invest'))
                         && (!auth()->user()->hasRole('Support Invest'))
                         && (!auth()->user()->hasRole('Investor'))
                          && (auth()->user()->warehouses()->count() > 0))
                            )
                            @include('brackets/admin-ui::admin.layout.menu-item', [
                                'url' => url('admin/warehouses'),
                                'name' => trans('admin.warehouse.title'),
                                'gate' => 'crud.warehouse.view',
                                'icon' => 'zmdi zmdi-apps zmdi-hc-fw'
                            ])
                        @endif
                        @if(!auth()->user()->hasRole('Staffer'))
                            <li class="menu @if(
                        url()->current() == url('admin/users')
                        || url()->current() == url('admin/projects')
                        || url()->current() == route('admin.role.user', ['role' => 4])
                        || url()->current() == url('admin/investor-lists')
                        || url()->current() == url('admin/contragent-lists')
                        || url()->current() == url('admin/help-videos')
                        || url()->current() == url('admin/history')
                        || url()->current() == url('admin/contragents')
                        || url()->current() == route('user.statistics')
                        || url()->current() == route('admin.businessprocess.index')
                        || url()->current() == url('admin/users/needcalls')
                        || url()->current() == url('admin/statistics')
                        || url()->current() == url('admin/dkim-keys')
                        || url()->current() == url('admin/auto-answers')
                        )
                                open
@endif">


                                    @include('brackets/admin-ui::admin.layout.menu-item', [
                                        'url' => url('admin/contragents'),
                                        'name' => trans('admin.contragent.title'),
                                        'gate' => 'crud.contragent.view',
                                        'icon' => 'zmdi zmdi-apps zmdi-hc-fw'
                                    ])
                                    @include('brackets/admin-ui::admin.layout.menu-item', [
                                        'url' => url('admin/projects'),
                                        'name' => trans('admin.project.title'),
                                        'gate' => 'crud.project.view',
                                        'icon' => 'zmdi zmdi-apps zmdi-hc-fw'
                                    ])
{{--                                    @include('brackets/admin-ui::admin.layout.menu-item', [--}}
{{--                                        'url' => route('admin.role.user', ['role' => 4]),--}}
{{--                                        'name' => trans('admin.user.staffers'),--}}
{{--                                        'gate' => 'crud.user.columns.staffer.read',--}}
{{--                                        'icon' => 'zmdi zmdi-account zmdi-hc-fw'--}}
{{--                                    ])--}}
                                    @include('brackets/admin-ui::admin.layout.menu-item', [
                                        'url' => route('admin.role.user', ['role' => 11]),
                                        'name' => trans('admin.header.investors'),
                                        'gate' => 'crud.user.view',
                                        'icon' => 'zmdi zmdi-account zmdi-hc-fw',
                                        'hideFor' => ['HR Manager Staff', 'Staffer', 'Support Staff']
                                    ])
                                    @include('brackets/admin-ui::admin.layout.menu-item', [
                                        'url' => url('admin/investor-lists'),
                                        'name' => trans('admin.investor-list.title'),
                                        'gate' => 'crud.investor-list.index',
                                        'icon' => 'zmdi zmdi-view-stream zmdi-hc-fw',
                                        'hideFor' => ['HR Manager Staff', 'Staffer', 'Support Staff']
                                    ])
{{--                                    @include('brackets/admin-ui::admin.layout.menu-item', [--}}
{{--                                        'url' => url('admin/contragent-lists'),--}}
{{--                                        'name' => trans('admin.contragent-list.title'),--}}
{{--                                        'gate' => 'crud.contragent-list.index',--}}
{{--                                        'icon' => 'zmdi zmdi-view-stream zmdi-hc-fw'--}}
{{--                                    ])--}}
                                    @include('brackets/admin-ui::admin.layout.menu-item', [
                                        'url' => url('admin/help-videos'),
                                        'name' => trans('admin.help-video.title'),
                                        'gate' => 'crud.help_video.view',
                                        'icon' => 'zmdi zmdi-play zmdi-hc-fw'
                                    ])
                                    @include('brackets/admin-ui::admin.layout.menu-item', [
                                        'url' => url('admin/history'),
                                        'name' => trans('admin.history.title'),
                                        'gate' => 'crud.activity-log.view',
                                        'icon' => 'zmdi zmdi-view-stream zmdi-hc-fw'
                                    ])
                                    @include('brackets/admin-ui::admin.layout.menu-item', [
                                        'url' => route('user.statistics'),
                                        'name' => trans('admin.stats_and_pay'),
                                        'gate' => 'user.statistics',
                                        'icon' => 'zmdi zmdi-chart zmdi-hc-fw'
                                    ])
                                    @include('brackets/admin-ui::admin.layout.menu-item', [
                                        'url' => route('admin.businessprocess.index'),
                                        'name' => trans('admin.businessprocess.short_title'),
                                        'gate' => 'crud.business-process.view',
                                        'icon' => 'zmdi zmdi-circle zmdi-hc-fw'
                                    ])
                                    @if(!auth()->user()->hasRole('Administrator'))
                                        @include('brackets/admin-ui::admin.layout.menu-item', [
                                            'url' => url('admin/users/needcalls'),
                                            'name' => trans('admin.needcalls.title'),
                                            'gate' => 'crud.user.callsupport',
                                            'icon' => 'zmdi zmdi-phone zmdi-hc-fw'
                                        ])
                                    @endif
                                    @include('brackets/admin-ui::admin.layout.menu-item', [
                                        'url' => url('admin/statistics'),
                                        'name' => trans('admin.statistic.title'),
                                        'gate' => 'crud.statistic.view',
                                        'icon' => 'zmdi zmdi-chart zmdi-hc-fw'
                                    ])
                                    @can('crud.mailing.view')
                                        <li class="menu @if(
                            url()->current() == url('admin/users')
                            || url()->current() == url('admin/mailings')
                            || url()->current() == url('admin/email-groups')
                            || url()->current() == url('admin/email-mailings')
                            )
                                            open
@endif">
                                            <a href="javascript:void(0)">
                                                <i class="zmdi zmdi-email zmdi-hc-fw"></i>
                                                <span class="nav-text">@lang('admin.response')</span>
                                            </a>
                                            <ul class="sub-menu">
                                                @include('brackets/admin-ui::admin.layout.menu-item', [
                                                    'url' => url('admin/mailings'),
                                                    'name' => trans('admin.mailing.title'),
                                                    'gate' => 'crud.mailing.view',
                                                    'icon' => null
                                                ])

                                                @include('brackets/admin-ui::admin.layout.menu-item', [
                                                    'url' => url('admin/email-groups'),
                                                    'name' => trans('admin.email-group.title'),
                                                    'gate' => 'crud.email-group.view',
                                                    'icon' => null
                                                ])
                                                @include('brackets/admin-ui::admin.layout.menu-item', [
                                                    'url' => url('admin/email-mailings'),
                                                    'name' => trans('admin.email-mailing.title'),
                                                    'gate' => 'crud.email-mailing.view',
                                                    'icon' => null
                                                ])
                                            </ul>
                                        </li>
                                    @endcan
                                    @if(!auth()->user()->hasRole('Call support'))
                                        <li class="menu @if(
                            url()->current() == url('admin/users')
                            || url()->current() == url('admin/users/needcalls')
                            || url()->current() == url('admin/roles')
                            || url()->current() == url('admin/mail-templates')
                            || url()->current() == url('admin/mailsettings')
                            || url()->current() == url('admin/forms')
                            || url()->current() == url('admin/settings')
                            || url()->current() == url('admin/items')
                            || url()->current() == url('admin/nomenclatures')
                            || url()->current() == url('admin/dkim-keys')
                            || url()->current() == url('admin/auto-answers')
                            )
                                            open
@endif">
                                            <a href="javascript:void(0)">
                                                <i class="zmdi zmdi-settings zmdi-hc-fw"></i>
                                                <span class="nav-text">@lang('brackets/admin-ui::admin.sidebar.settings')</span>
                                            </a>
                                            <ul class="sub-menu">
{{--                                                @include('brackets/admin-ui::admin.layout.menu-item', [--}}
{{--                                                    'url' => url('admin/items'),--}}
{{--                                                    'name' => trans('admin.item.title'),--}}
{{--                                                    'gate' => 'crud.item.view',--}}
{{--                                                    'icon' => null,--}}
{{--                                                    'hideFor' => ['Employee Staff','Employee Invest']--}}
{{--                                                ])--}}
                                                @if(!auth()->user()->hasRole('Employee Invest'))
                                                    @include('brackets/admin-ui::admin.layout.menu-item', [
                                                        'url' => url('admin/users'),
                                                        'name' => trans('admin.user.title'),
                                                        'gate' => 'crud.user.view',
                                                        'icon' => null
                                                    ])
                                                @endif
                                                @include('brackets/admin-ui::admin.layout.menu-item', [
                                                    'url' => url('admin/roles'),
                                                    'name' => trans('admin.role.title'),
                                                    'gate' => 'crud.role.view',
                                                    'icon' => null
                                                ])
                                                @include('brackets/admin-ui::admin.layout.menu-item', [
                                                    'url' => url('admin/mail-templates'),
                                                    'name' => trans('admin.mail-template.title'),
                                                    'gate' => 'crud.mail_template.modify',
                                                    'icon' => null
                                                ])
                                                @include('brackets/admin-ui::admin.layout.menu-item', [
                                                    'url' => url('admin/mailsettings'),
                                                    'name' => trans('admin.mailsetting.title'),
                                                    'gate' => 'crud.mailsetting.view',
                                                    'icon' => null
                                                ])
                                                @include('brackets/admin-ui::admin.layout.menu-item', [
                                                    'url' => url('admin/forms'),
                                                    'name' => trans('admin.form.title'),
                                                    'gate' => 'crud.form.view',
                                                    'icon' => null
                                                ])
                                                @include('brackets/admin-ui::admin.layout.menu-item', [
                                                    'url' => url('admin/settings'),
                                                    'name' => trans('admin.setting.title'),
                                                    'gate' => 'crud.email-mailing.view',
                                                    'icon' => null
                                                ])
                                                @include('brackets/admin-ui::admin.layout.menu-item', [
                                                    'url' => url('admin/dkim-keys'),
                                                    'name' => trans('admin.dkim-key.title'),
                                                    'gate' => 'crud.dkim-key.view',
                                                    'icon' => null
                                                ])
                                                @include('brackets/admin-ui::admin.layout.menu-item', [
                                                    'url' => url('admin/auto-answers'),
                                                    'name' => trans('admin.auto-answer.title'),
                                                    'gate' => 'crud.auto-answer.view',
                                                    'icon' => null
                                                ])
                                            </ul>
                                        </li>
                                    @endif

                            </li>
                        @endif
                    @endcan
                @endif
            </ul>
        </div>

    </div>
</div>
