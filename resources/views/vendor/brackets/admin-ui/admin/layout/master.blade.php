<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- TODO translatable suffix --}}
    <title>@yield('title', 'Craftable') - {{ trans('brackets/admin-ui::admin.page_title_suffix') }}</title>
    @include('brackets/admin-ui::admin.partials.main-styles')

    @yield('styles')

</head>


<body id="body" data-theme="indigo">

    <div class="loader-backdrop">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
            </svg>
        </div>
    </div>

    <div class="gx-container" id="app" v-cloak>
        @if(View::exists('brackets/admin-ui::admin.layout.sidebar') && auth()->check())
            @include('brackets/admin-ui::admin.layout.sidebar')
            <sip-client ref="sipclient"></sip-client>
        @endif

        <div class="gx-main-container">

            @yield('header')

            <div class="gx-main-content">
                <div class="gx-wrapper">
                    @yield('content')
                </div>
            </div>
        </div>
        @yield('footer')

    </div>

    <div class="menu-backdrop fade"></div>
    @include('brackets/admin-ui::admin.partials.wysiwyg-svgs')
    @include('brackets/admin-ui::admin.partials.main-bottom-scripts')
    @yield('bottom-scripts')
</body>

</html>
