@extends('brackets/admin-ui::admin.layout.master')

@section('header')
    @include('brackets/admin-ui::admin.partials.header')
@endsection

@section('content')
    <div :class="{'loading': loading, 'pending': pending}">
        <div class="modals">
            <v-dialog/>
        </div>
        <div>
            <notifications position="bottom right" :duration="2000"/>
        </div>
        @yield('body')
    </div>
@endsection

@section('bottom-scripts')
    @parent
@endsection
<script type="text/javascript">
    window.app = {
        user: {!! json_encode(Auth::user()) !!},
    }
    window.trans = <?php
    $lang_files = File::files(resource_path() . '/lang/' . App::getLocale());
    $trans = [];
    foreach ($lang_files as $f) {
        $filename = pathinfo($f)['filename'];
        $trans[$filename] = trans($filename);
    }
    $trans['coders-studio/chat']['chat'] = \Lang::get('coders-studio/chat::chat');
    echo json_encode($trans);
    ?>;
</script>

<script type="text/javascript" src="https://codersstudio.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/ghuq87/b/20/a44af77267a987a660377e5c46e0fb64/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=ru-RU&collectorId=2e7bb0b6"></script>

