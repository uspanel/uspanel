<header class="main-header">
    <div class="gx-toolbar">
        <div class="sidebar-mobile-menu">

        </div>

        <ul class="quick-menu header-notifications text-right nav-menu nav-top d-none d-lg-block">
            <coders-studio-notifications :id="{{ auth()->id() }}"></coders-studio-notifications>
            @if(auth()->check())
                @can('user.statistics')
                    <li>
                        {{ auth()->user()->currentWeekSalary() }}
                    </li>
                @endcan
                <li class="dropdown user-nav">
                    <a class="dropdown-toggle no-arrow d-inline-block" href="#" role="button" id="userInfo"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="user-avatar size-40"
                             src="{{ auth()->user()->avatar_thumb_url ?: '/images/user.png' }}">
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userInfo">
                        <div class="user-profile">
                            <img class="user-avatar border-0 size-40"
                                 src="{{ auth()->user()->avatar_thumb_url ?: '/images/user.png' }}">
                            <div class="user-detail ml-2">
                                <h4 class="user-name mb-0">{{ auth()->user()->name }}</h4>
                                <small>{{ auth()->user()->roles()->first()->name }}</small>
                            </div>
                        </div>
                        <a class="dropdown-item" href="{{ route('profile.index') }}">
                            <i class="zmdi zmdi-face zmdi-hc-fw mr-1"></i>
                            @lang('admin.profile')
                        </a>
                        <a class="dropdown-item" href="{{ url('admin/logout') }}">
                            <i class="zmdi zmdi-sign-in zmdi-hc-fw mr-1"></i>
                            @lang('brackets/admin-auth::admin.profile_dropdown.logout')
                        </a>
                    </div>
                </li>
            @endif
        </ul>
        <ul class="quick-menu header-notifications text-center ml-auto d-block d-lg-none">
            <coders-studio-notifications :id="{{ auth()->id() }}"></coders-studio-notifications>
            @if(auth()->check())
                @can('user.statistics')
                    <li>
                        {{ auth()->user()->currentWeekSalary() }}
                    </li>
                @endcan
                <li class="dropdown user-nav">
                    <a class="dropdown-toggle no-arrow d-inline-block" href="#" role="button" id="userInfo"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="user-avatar size-40"
                             src="{{ auth()->user()->avatar_thumb_url ?: '/images/user.png' }}">
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userInfo">
                        <div class="user-profile">
                            <img class="user-avatar border-0 size-40"
                                 src="{{ auth()->user()->avatar_thumb_url ?: '/images/user.png' }}">
                            <div class="user-detail ml-2">
                                <h4 class="user-name mb-0">{{ auth()->user()->name }}</h4>
                                <small>{{ auth()->user()->roles()->first()->name }}</small>
                            </div>
                        </div>
                        <a class="dropdown-item" href="{{ route('profile.index') }}">
                            <i class="zmdi zmdi-face zmdi-hc-fw mr-1"></i>
                            @lang('admin.profile')
                        </a>
                        <a class="dropdown-item" href="{{ url('admin/logout') }}">
                            <i class="zmdi zmdi-sign-in zmdi-hc-fw mr-1"></i>
                            @lang('brackets/admin-auth::admin.profile_dropdown.logout')
                        </a>
                    </div>
                </li>
            @endif
        </ul>
    </div>
</header>

