{{'@'}}extends('brackets/admin-ui::admin.layout.default')

{{'@'}}section('title', trans('admin.{{ $modelLangFormat }}.actions.create'))

{{'@'}}section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>{{'{{'}} trans('admin.{{ $modelLangFormat }}.actions.create') }}</span></h2>
    </div>

    <{{ $modelJSName }}-form
        :action="'{{'{{'}} url('{{ config('admin-generator.admin_url_prefix') }}{{ $resource }}') }}'"
        @if($hasTranslatable):locales="@{{ json_encode($locales) }}"
        :send-empty-locales="false"@endif

        inline-template>

        <div class="card">
            <div class="card-body">
                <form method="post" {{'@'}}submit.prevent="onSubmit" :action="this.action" novalidate>
                    {{'@'}}include('admin.{{ $modelDotNotation }}.components.form-elements')
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            @{{ translate('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>


    </{{ $modelJSName }}-form>


{{'@'}}endsection
