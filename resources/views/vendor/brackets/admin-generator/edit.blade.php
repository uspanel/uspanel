{{'@'}}extends('brackets/admin-ui::admin.layout.default')

{{'@'}}section('title', trans('admin.{{ $modelLangFormat }}.actions.edit', ['name' => ${{ $modelVariableName }}->{{$modelTitle}}]))

{{'@'}}section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0">
            <span>
                {{'{{'}} trans('admin.{{ $modelLangFormat }}.actions.edit', ['name' => ${{ $modelVariableName }}->{{$modelTitle}}]) }}
            </span>
        </h2>
    </div>
    @if($hasTranslatable)<{{ $modelJSName }}-form
        :action="'{{'{{'}} ${{ $modelVariableName }}->resource_url }}'"
        :data="{{'{{'}} ${{ $modelVariableName }}->toJsonAllLocales() }}"
        :locales="@{{ json_encode($locales) }}"
        :send-empty-locales="false"
        inline-template>
    @else<{{ $modelJSName }}-form
        :action="'{{'{{'}} ${{ $modelVariableName }}->resource_url }}'"
        :data="{{'{{'}} ${{ $modelVariableName }}->toJson() }}"
        inline-template>
    @endif
        <div class="card">
            <div class="card-body">
                <form method="post" {{'@'}}submit.prevent="onSubmit" :action="this.action" novalidate>
                    {{'@'}}include('admin.{{ $modelDotNotation }}.components.form-elements')
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            @{{ translate('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>

</{{ $modelJSName }}-form>

{{'@'}}endsection
