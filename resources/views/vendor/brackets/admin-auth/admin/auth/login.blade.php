@extends('brackets/admin-ui::admin.layout.master')

@section('title', trans('brackets/admin-auth::admin.login.title'))

@section('content')
	<div class="login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3" id="app">
		<div class="login-content">
	        <auth-form
				:action="'{{ url('/admin/login') }}'"
				:data="{}"
				inline-template>
				<form role="form" method="POST" action="{{ url('/admin/login') }}" novalidate>
					{{ csrf_field() }}
					<div class="login-header">
						<h1>{{ trans('brackets/admin-auth::admin.login.title') }}</h1>
						<p>{{ trans('brackets/admin-auth::admin.login.sign_in_text') }}</p>
					</div>
					<div class="login-form">
						<div class="form-group" :class="{'has-danger': errors.has('email'), 'has-success': this.fields.email && this.fields.email.valid }">
							<input type="text" v-model="form.email" class="form-control form-control-lg" placeholder="{{ trans('brackets/admin-auth::admin.auth_global.email') }}" name="email">
							@if($errors->has('email'))
								<div class="text-danger" v-cloak>{{ $errors->first('email') }}</div>
							@endif
						</div>

						<div class="form-group" :class="{'has-danger': errors.has('password'), 'has-success': this.fields.password && this.fields.password.valid }">
							<input type="password" v-model="form.password"  class="form-control form-control-lg" placeholder="{{ trans('brackets/admin-auth::admin.auth_global.password') }}" name="password">
							@if($errors->has('password'))
								<div class="text-danger" v-cloak>{{ $errors->first('password') }}</div>
							@endif
						</div>
						<div class="form-group text-right">
							<a href="{{ url('/admin/password-reset') }}" class="auth-ghost-link">{{ trans('brackets/admin-auth::admin.login.forgot_password') }}</a>
						</div>
						<input type="hidden" name="remember" value="1">
						<button type="submit" class="gx-btn gx-btn-rounded gx-btn-primary btn-spinner"><i class="fa"></i> {{ trans('brackets/admin-auth::admin.login.button') }}</button>
						
					</div>
				</form>
			</auth-form>
		</div>
	</div>
@endsection


@section('bottom-scripts')
<script type="text/javascript">
    // fix chrome password autofill
    // https://github.com/vuejs/vue/issues/1331
    // document.getElementById('password').dispatchEvent(new Event('input'));
</script>
@endsection
