@extends('layouts.app')

@section('content')
<div class="container app-body" v-cloak>
	<div class="card w-100">
        <div class="card-header">
        	@lang('admin.profile')
        </div>
        <div class="card-body">
        	<div class="table-responsive">
				<table class="table">
					<tbody>
						<tr>
							<td class="font-weight-bold">@lang('admin.address')</td>
							<td>
								{{ $profile->address }}
							</td>
						</tr>
						<tr>
							<td class="font-weight-bold">@lang('admin.city')</td>
							<td>
								{{ $profile->city }}
							</td>
						</tr>
						<tr>
							<td class="font-weight-bold">@lang('admin.state')</td>
							<td>
								{{ $profile->state }}
							</td>
						</tr>
						<tr>
							<td class="font-weight-bold">@lang('admin.zip')</td>
							<td>
								{{ $profile->zip }}
							</td>
						</tr>
						<tr>
							<td class="font-weight-bold">@lang('admin.authorized_to_work')</td>
							<td>
								{{ $profile->authorized_to_work ? trans('admin.yes') : trans('admin.no') }}
							</td>
						</tr>
						<tr>
							<td class="font-weight-bold">@lang('admin.adult')</td>
							<td>
								{{ $profile->adult ? trans('admin.yes') : trans('admin.no') }}
							</td>
						</tr>
						<tr>
							<td class="font-weight-bold">@lang('admin.higher_education')</td>
							<td>
								{{ $profile->higher_education ? trans('admin.yes') : trans('admin.no') }}
							</td>
						</tr>
					</tbody>
				</table>
			</div>
        </div>
    </div>
</div>
@endsection
