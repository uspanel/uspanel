@extends('layouts.app')

@section('content')
<div class="container app-body" v-cloak>
	<profile inline-template>
		<div class="card w-100">
	        <div class="card-header">
	        	@lang('admin.profile')
	        </div>
	        <div class="card-body">
	            <div class="form-group row">
	                <label class="col-md-4 col-form-label text-md-right">
	                	@lang('admin.address')
	                </label>
	                <div class="col-md-6">
	                    <input type="text" class="form-control" :class="{ 'is-invalid': fails.address }" v-model="form.address" required>
	                    <span class="invalid-feedback" role="alert" v-if="fails.address">
	                        <strong>@{{ _.head(fails.address) }}</strong>
	                    </span>
	                </div>
	            </div>
	            <div class="form-group row">
	                <label class="col-md-4 col-form-label text-md-right">
	                	@lang('admin.city')
	                </label>
	                <div class="col-md-6">
	                    <input type="text" class="form-control" :class="{ 'is-invalid': fails.city }" v-model="form.city" required>
	                    <span class="invalid-feedback" role="alert" v-if="fails.city">
	                        <strong>@{{ _.head(fails.city) }}</strong>
	                    </span>
	                </div>
	            </div>
	            <div class="form-group row">
	                <label class="col-md-4 col-form-label text-md-right">
	                	@lang('admin.state')
	                </label>
	                <div class="col-md-6">
	                    <input type="text" class="form-control" :class="{ 'is-invalid': fails.state }" v-model="form.state" required>
	                    <span class="invalid-feedback" role="alert" v-if="fails.state">
	                        <strong>@{{ _.head(fails.state) }}</strong>
	                    </span>
	                </div>
	            </div>
	            <div class="form-group row">
	                <label class="col-md-4 col-form-label text-md-right">
	                	@lang('admin.zip')
	                </label>
	                <div class="col-md-6">
	                    <input type="text" class="form-control" :class="{ 'is-invalid': fails.zip }" v-model="form.zip" required>
	                    <span class="invalid-feedback" role="alert" v-if="fails.zip">
	                        <strong>@{{ _.head(fails.zip) }}</strong>
	                    </span>
	                </div>
	            </div>
	            <div class="form-group row">
	                <label class="col-md-4 col-form-label text-md-right">
	                	@lang('admin.authorized_to_work')
	                </label>
	                <div class="col-md-6">
	                	<div class="row">
	                		<div class="col-auto">
	                			<div class="form-check">
								  	<input class="form-check-input" type="radio" id="authorized_to_work_yes" value="1" v-model="form.authorized_to_work">
								  	<label class="form-check-label" for="authorized_to_work_yes">
								    	@lang('admin.yes')
								  	</label>
								</div>
	                		</div>
	                		<div class="col-auto">
	                			<div class="form-check">
								  	<input class="form-check-input" type="radio" id="authorized_to_work_no" value="0" v-model="form.authorized_to_work">
								  	<label class="form-check-label" for="authorized_to_work_no">
								    	@lang('admin.no')
								  	</label>
								</div>
	                		</div>
	                	</div>
						
	                    <span class="invalid-feedback" role="alert" v-if="fails.authorized_to_work">
	                        <strong>@{{ _.head(fails.authorized_to_work) }}</strong>
	                    </span>
	                </div>
	            </div>
	            <div class="form-group row">
	                <label class="col-md-4 col-form-label text-md-right">
	                	@lang('admin.adult')
	                </label>
	                <div class="col-md-6">
	                	<div class="row">
	                		<div class="col-auto">
	                			<div class="form-check">
								  	<input class="form-check-input" type="radio" id="adult_yes" value="1" v-model="form.adult">
								  	<label class="form-check-label" for="adult_yes">
								    	@lang('admin.yes')
								  	</label>
								</div>
	                		</div>
	                		<div class="col-auto">
	                			<div class="form-check">
								  	<input class="form-check-input" type="radio" id="adult_no" value="0" v-model="form.adult">
								  	<label class="form-check-label" for="adult_no">
								    	@lang('admin.no')
								  	</label>
								</div>
	                		</div>
	                	</div>
	                    <span class="invalid-feedback" role="alert" v-if="fails.adult">
	                        <strong>@{{ _.head(fails.adult) }}</strong>
	                    </span>
	                </div>
	            </div>
	            <div class="form-group row">
	                <label class="col-md-4 col-form-label text-md-right">
	                	@lang('admin.higher_education')
	                </label>
	                <div class="col-md-6">
	                	<div class="row">
	                		<div class="col-auto">
	                			<div class="form-check">
								  	<input class="form-check-input" type="radio" id="higher_education_yes" value="1" v-model="form.higher_education">
								  	<label class="form-check-label" for="higher_education_yes">
								    	@lang('admin.yes')
								  	</label>
								</div>
	                		</div>
	                		<div class="col-auto">
	                			<div class="form-check">
								  	<input class="form-check-input" type="radio" id="higher_education_no" value="0" v-model="form.higher_education">
								  	<label class="form-check-label" for="higher_education_no">
								    	@lang('admin.no')
								  	</label>
								</div>
	                		</div>
	                	</div>
	                    <span class="invalid-feedback" role="alert" v-if="fails.higher_education">
	                        <strong>@{{ _.head(fails.higher_education) }}</strong>
	                    </span>
	                </div>
	            </div>

	            <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button class="btn btn-primary" @click="save">
                            @lang('admin.save')
                        </button>
                    </div>
                </div>
	        </div>
	    </div>
	</profile>
</div>
@endsection
