<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    </head>
    <body>
        <div class="" id="app">
            <chat></chat>
        </div>
        <script defer src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
