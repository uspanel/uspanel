<div class="form-group mb-3">
    <input type="text" class="form-control" v-model="profile.address" placeholder="@lang('admin.address')">
    <div class="text-danger" v-if="fails['profile.address']">
        @{{ _.head(fails['profile.address']) }}
    </div>
</div>
<div class="form-group mb-3">
    <the-mask :masked="true" mask="XXXXX" type="text" class="form-control" v-model="profile.zip" placeholder="@lang('admin.zip')" ></the-mask>
    <div class="text-danger" v-if="fails['profile.zip']">
        @{{ _.head(fails['profile.zip']) }}
    </div>
</div>

<div class="form-group mb-3">
    <input type="text" class="form-control" v-model="profile.city" placeholder="@lang('admin.city')">
    <div class="text-danger" v-if="fails['profile.city']">
        @{{ _.head(fails['profile.city']) }}
    </div>
</div>
<div class="form-group mb-3">
    <multiselect
        v-model="profile.state_id"
        :options="{{$states->toJson()}}"
        :multiple="false"
        track-by="id"
        label="name"></multiselect>
    <div class="text-danger" v-if="fails['profile.state_id']">
        @{{ _.head(fails['profile.state_id']) }}
    </div>
</div>

<div class="form-group">
    <label>
        @lang('admin.authorized_to_work')
    </label>
    <div class="row">
        <div class="col-auto">
            <div class="form-check">
                <input class="form-check-input" type="radio" id="authorized_to_work_yes" value="1" v-model="profile.authorized_to_work">
                <label class="form-check-label" for="authorized_to_work_yes">
                    @lang('admin.yes')
                </label>
            </div>
        </div>
        <div class="col-auto">
            <div class="form-check">
                <input class="form-check-input" type="radio" id="authorized_to_work_no" value="0" v-model="profile.authorized_to_work">
                <label class="form-check-label" for="authorized_to_work_no">
                    @lang('admin.no')
                </label>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label>
        @lang('admin.adult')
    </label>
    <div class="row">
        <div class="col-auto">
            <div class="form-check">
                <input class="form-check-input" type="radio" id="adult_yes" value="1" v-model="profile.adult">
                <label class="form-check-label" for="adult_yes">
                    @lang('admin.yes')
                </label>
            </div>
        </div>
        <div class="col-auto">
            <div class="form-check">
                <input class="form-check-input" type="radio" id="adult_no" value="0" v-model="profile.adult">
                <label class="form-check-label" for="adult_no">
                    @lang('admin.no')
                </label>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <label>
        @lang('admin.higher_education')
    </label>
    <div class="row">
        <div class="col-auto">
            <div class="form-check">
                <input class="form-check-input" type="radio" id="higher_education_yes" value="1" v-model="profile.higher_education">
                <label class="form-check-label" for="higher_education_yes">
                    @lang('admin.yes')
                </label>
            </div>
        </div>
        <div class="col-auto">
            <div class="form-check">
                <input class="form-check-input" type="radio" id="higher_education_no" value="0" v-model="profile.higher_education">
                <label class="form-check-label" for="higher_education_no">
                    @lang('admin.no')
                </label>
            </div>
        </div>
    </div>
</div>
<div class="form-group mb-3">
    <label>
        @lang('admin.cv')
    </label>
    <media-upload
        ref="cv_uploader"
        collection="cv"
        :url="'{{ route('brackets/media::upload') }}'"
    ></media-upload>
    <div class="text-danger" v-if="fails.cv">
        @{{ _.head(fails.cv) }}
    </div>
</div>
<div class="alert-danger alert" v-if="fails.message">
    @lang('auth.fail')
</div>

<button type="submit" class="gx-btn gx-btn-rounded gx-btn-primary" @click="register">
    {{ __('Register') }}
</button>
