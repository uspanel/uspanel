@extends('brackets/admin-ui::admin.layout.master')

@section('title', trans('admin.register'))

@section('content')
    <div class="login-container d-flex justify-content-center align-items-center animated slideInUpTiny animation-duration-3" id="app">
        <register inline-template :project="{{ $project }}">
            <div class="login-content" v-cloak>
                
                
                <div class="login-header">
                    <h1 class="auth-title">@lang('admin.register')</h1>
                    <p class="auth-subtitle">
                        @lang('admin.create_new_account')
                        or <a href="{{ route('login') }}" class="auth-ghost-link">{{ trans('admin.login') }}</a>
                    </p>
                </div>
                <div class="login-form" v-if="showProfileForm">
                    @include('auth.profile')
                </div>
                <div class="login-form" v-else>
                    @include('brackets/admin-auth::admin.auth.includes.messages')
                    <div class="form-group mb-3">
                        <input type="text" class="form-control"  required v-model="form.first_name" placeholder="@lang('admin.first_name')">
                        <div class="text-danger" v-if="fails.first_name">
                            @{{ _.head(fails.first_name) }}
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <input type="text" class="form-control" required v-model="form.last_name" placeholder="@lang('admin.last_name')">
                        <div class="text-danger" v-if="fails.last_name">
                            @{{ _.head(fails.last_name) }}
                        </div>
                    </div>

                    <div class="form-group mb-3">
                        <input type="email" class="form-control" required v-model="form.email" placeholder="{{ __('E-Mail Address') }}">
                        <div class="text-danger" v-if="fails.email">
                            @{{ _.head(fails.email) }}
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <the-mask :masked="true" :mask="'+' + {{env('PHONE_PREFIX')}} + ' (###) ###-####'" type="text" class="form-control" required v-model="form.phone" placeholder="@lang('admin.phone')"></the-mask>
                        <div class="text-danger" v-if="fails.phone">
                            @{{ _.head(fails.phone) }}
                        </div>
                    </div>

                    <div class="form-group mb-3">
                        <input type="password" class="form-control" required v-model="form.password" placeholder="{{ __('Password') }}">
                        <div class="text-danger" v-if="fails.password">
                            @{{ _.head(fails.password) }}
                        </div>
                    </div>
                    <div class="form-group mb-3">
                        <input type="password" class="form-control" required v-model="form.password_confirmation" placeholder="{{ __('Confirm Password') }}">
                    </div>
                    <button class="gx-btn gx-btn-rounded gx-btn-primary" @click="sendVerificationCode">
                        @lang('admin.verify_phone')
                    </button>
                </div>
                
                <modal name="verifyCode" height="auto" :click-to-close="false" :width="document.body.clientWidth < 420 ? '90%' : '420px'">
                    <div class="modal-content">
                        <div class="modal-body p-5">
                            <div class="form-group mb-3">
                                <input type="text" class="form-control" v-model="code" :class="{ 'is-invalid': fails.code }" placeholder="@lang('admin.enter_code_from_sms')">
                                <div class="invalid-feedback" v-if="fails.code">
                                    @{{ _.head(fails.code) }}
                                </div>

                            </div>
                            <div class="text-center">
                                <button v-if="countdownEnd" class="gx-btn gx-btn-rounded gx-btn-primary" @click="sendVerificationCode">
                                    @lang('admin.resend_code')
                                </button>
                                <button v-else class="gx-btn gx-btn-rounded gx-btn-primary" @click="verifyPhone">
                                    @lang('admin.send')
                                </button>
                            </div>
                            <countdown :time="{{env('SMS_RESEND_INTERVAL')}}" @end="end" v-if="!countdownEnd">
                            {{-- <countdown :time="10000" @end="end" v-if="!countdownEnd"> --}}
                                <template slot-scope="props">
                                    <h1 class="text-center">
                                        <div>
                                            @lang('admin.resending_after')
                                        </div>
                                        @{{ props.minutes }}:@{{ props.seconds }}
                                    </h1>
                                </template>
                            </countdown>
                        </div>
                    </div>
                </modal>
            </div>
        </register>
    </div>
   
@endsection


@section('bottom-scripts')
<script type="text/javascript">

</script>
@endsection
