<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
        <div class="" id="app">
            <chat></chat>
        </div>
        <script type="text/javascript">
            window.app = {
                user: {!! json_encode(Auth::user()) !!},
            }
        </script>

        <script defer src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
