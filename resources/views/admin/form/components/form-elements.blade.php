<div class="form-group row align-items-center">
    <label class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.form.columns.name') }}</label>
   	<div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.name" class="form-control" placeholder="{{ trans('admin.form.columns.name') }}">
    </div>
</div>