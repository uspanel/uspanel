@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.dkim-key.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0">
                <span>{{ trans('admin.dkim-key.actions.index') }}</span>
            </h2>
        </div>
    </div>
    <dkim-key-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('/admin/dkim-keys') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <form @submit.prevent="">
                            <div class="row justify-content-md-between">
                                <div class="col col-lg-7 col-xl-5 form-group">
                                    <div class="input-group">
                                        <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                        <span class="input-group-append">
                                            <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                        </span>
                                    </div>
                                </div>

                                <div class="col-sm-auto form-group ">
                                    <select class="form-control" v-model="pagination.state.per_page">

                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>

                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th is='sortable' :column="'id'">{{ trans('admin.dkim-key.columns.id') }}</th>
                                        <th is='sortable' :column="'project_id'">{{ trans('admin.dkim-key.columns.project_id') }}</th>
                                        <th is='sortable' :column="'domain'">{{ trans('admin.dkim-key.columns.domain') }}</th>
                                        <th>{{ trans('admin.dkim-key.columns.dkim_record') }}</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection">
                                        <td>@{{ item.id }}</td>
                                        <td>@{{ item.project_id }}</td>
                                        <td>@{{ item.domain }}</td>
                                        <td style="max-width: 500px;">@{{ item.dkim_record }}</td>

                                        <td class="nowrap">
{{--                                            Maybe we will want to delete items--}}
{{--                                            <button--}}
{{--                                            @click="deleteItem(item.resource_url)"--}}
{{--                                            class="btn btn-sm btn-danger"--}}
{{--                                            title="Delete">--}}
{{--                                                <i class="fas fa-trash-alt"></i>--}}
{{--                                            </button>--}}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center" v-if="!collection.length > 0">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </dkim-key-listing>

@endsection
