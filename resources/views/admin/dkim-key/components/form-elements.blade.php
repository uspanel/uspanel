<div class="form-group row align-items-center" :class="{'has-danger': errors.has('project_id'), 'has-success': this.fields.project_id && this.fields.project_id.valid }">
    <label for="project_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.dkim-key.columns.project_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.project_id" v-validate="'required|integer'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('project_id'), 'form-control-success': this.fields.project_id && this.fields.project_id.valid}" id="project_id" name="project_id" placeholder="{{ trans('admin.dkim-key.columns.project_id') }}">
        <div v-if="errors.has('project_id')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('project_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('domain'), 'has-success': this.fields.domain && this.fields.domain.valid }">
    <label for="domain" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.dkim-key.columns.domain') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.domain" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('domain'), 'form-control-success': this.fields.domain && this.fields.domain.valid}" id="domain" name="domain" placeholder="{{ trans('admin.dkim-key.columns.domain') }}">
        <div v-if="errors.has('domain')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('domain') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('dkim_record'), 'has-success': this.fields.dkim_record && this.fields.dkim_record.valid }">
    <label for="dkim_record" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.dkim-key.columns.dkim_record') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <wysiwyg v-model="form.dkim_record" v-validate="'required'" id="dkim_record" name="dkim_record" :config="mediaWysiwygConfig"></wysiwyg>
        </div>
        <div v-if="errors.has('dkim_record')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('dkim_record') }}</div>
    </div>
</div>


