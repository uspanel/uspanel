<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('name'), 'has-success': this.fields.name && this.fields.name.valid }">
    <label for="name" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.invest-task.columns.name') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.name" v-validate="''" @input="validate($event)" class="form-control"
               :class="{'form-control-danger': errors.has('name'), 'form-control-success': this.fields.name && this.fields.name.valid}"
               id="name" name="name" placeholder="{{ trans('admin.invest-task.columns.name') }}">
        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('theme'), 'has-success': this.fields.theme && this.fields.theme.valid }">
    <label for="theme" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.invest-task.columns.theme') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.theme" v-validate="''" @input="validate($event)" class="form-control"
               :class="{'form-control-danger': errors.has('theme'), 'form-control-success': this.fields.theme && this.fields.theme.valid}"
               id="theme" name="theme" placeholder="{{ trans('admin.invest-task.columns.theme') }}">
        <div v-if="errors.has('theme')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('theme') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('text'), 'has-success': this.fields.text && this.fields.text.valid }">
    <label for="text" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.invest-task.columns.text') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <wysiwyg v-model="form.text" id="body" name="body"
                 :config="mediaWysiwygConfig"></wysiwyg>
        <div v-if="errors.has('text')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('text') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label class="col-form-label text-md-right col-md-2">
        {{ trans('admin.invest-task.columns.template') }}
    </label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select class="form-control" v-model="form.mail_template_id">
            <option :value="null"></option>
            <option v-for="item in mailtemplates" :value="item.id">@{{ item.title }}</option>
        </select>
    </div>
    <div class="col-md-9 col-xl-8">
        <div v-if="errors.has('template')" class="text-danger" v-cloak>@{{ errors.first('template') }}</div>
    </div>
</div>


<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('deadline'), 'has-success': this.fields.deadline && this.fields.deadline.valid }">
    <label for="deadline" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.invest-task.columns.deadline') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.deadline" :config="datetimePickerConfig"
                      v-validate="'date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr"
                      :class="{'form-control-danger': errors.has('deadline'), 'form-control-success': this.fields.deadline && this.fields.deadline.valid}"
                      id="deadline" name="deadline"
                      placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
        </div>
        <div v-if="errors.has('deadline')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('deadline')
            }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label class="col-form-label text-md-right col-md-2">
        {{ trans('admin.invest-task.columns.invest_task_status') }}
    </label>
    <div class="col-md-9 col-xl-8">
        <multiselect v-model="form.invest_task_status" :multiple="false" :options="invest_task_statuses" label="title"
                     placeholder="{{ trans('admin.invest-task.columns.invest_task_status') }}"
                     track-by="id"></multiselect>
        <div v-if="errors.has('investors')" class="text-danger" v-cloak>@{{ errors.first('invest_task_status') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label class="col-form-label text-md-right col-md-2">
        {{ trans('admin.invest-task.columns.investors') }}
    </label>
    <div class="col-md-9 col-xl-8">
        <multiselect v-model="form.investors" :multiple="true" :options="investors" label="name"
                     placeholder="{{ trans('admin.invest-task.columns.investors') }}" track-by="id"
                     :close-on-select="false"
        ></multiselect>
        <div v-if="errors.has('investors')" class="text-danger" v-cloak>@{{ errors.first('investors') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label class="col-form-label text-md-right col-md-2">
        {{ trans('admin.invest-task.columns.investor_lists') }}
    </label>
    <div class="col-md-9 col-xl-8">
        <multiselect v-model="form.investor_lists" :multiple="true" :options="investor_lists" label="name"
                     placeholder="{{ trans('admin.invest-task.columns.investor_lists') }}" track-by="id"
                     :close-on-select="false"
        ></multiselect>
        <div v-if="errors.has('investor_lists')" class="text-danger" v-cloak>@{{ errors.first('investor_lists') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label class="col-form-label text-md-right col-md-2">
        {{ trans('admin.invest-task.columns.contragents') }}
    </label>
    <div class="col-md-9 col-xl-8">
        <multiselect v-model="form.contragents" :multiple="true" :options="contragents" label="name"
                     placeholder="{{ trans('admin.invest-task.columns.contragents') }}" track-by="id"
                     :close-on-select="false"
        ></multiselect>
        <div v-if="errors.has('contragents')" class="text-danger" v-cloak>@{{ errors.first('contragents') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label class="col-form-label text-md-right col-md-2">
        {{ trans('admin.invest-task.columns.contragent_lists') }}
    </label>
    <div class="col-md-9 col-xl-8">
        <multiselect v-model="form.contragent_lists" :multiple="true" :options="contragent_lists" label="name"
                     placeholder="{{ trans('admin.invest-task.columns.contragent_lists') }}"
                     track-by="id" :close-on-select="false"
        ></multiselect>
        <div v-if="errors.has('contragent_lists')" class="text-danger" v-cloak>@{{ errors.first('contragent_lists') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label class="col-form-label text-md-right col-md-2">
        {{ trans('admin.invest-task.columns.employee_id') }}
    </label>
    <div class="col-md-9 col-xl-8">
        <multiselect v-model="form.employee" :multiple="!form.id" :options="employees" label="user_name"
                     placeholder="{{ trans('admin.invest-task.columns.employee_id') }}" track-by="id"></multiselect>
        <div v-if="errors.has('employee_id')" class="text-danger" v-cloak>@{{ errors.first('employee_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label class="col-form-label text-md-right col-md-2">
        {{ trans('admin.invest-task.columns.manager_id') }}
    </label>
    <div class="col-md-9 col-xl-8">
        <multiselect v-model="form.manager" :multiple="false" :options="managers" label="name"
                     placeholder="{{ trans('admin.invest-task.columns.manager_id') }}" track-by="id"></multiselect>
        <div v-if="errors.has('manager_id')" class="text-danger" v-cloak>@{{ errors.first('manager_id') }}</div>
    </div>
</div>

<div class="row align-items-center">
    <div class="col-md-2 text-md-right">
        <label for="">
            {{ trans('admin.invest-task.columns.files') }}
        </label>
    </div>
    <div class="col-md-9 col-xl-8">
        @include('brackets/admin-ui::admin.includes.media-uploader', [
            'mediaCollection' => ( $investTask ?? app(App\InvestTask::class))->getMediaCollection('files'),
            'label' => trans('admin.invest-task.columns.files'),
            'media' => ( $investTask ?? app(App\InvestTask::class))->getThumbs200ForCollection('files')
        ])
    </div>
</div>
