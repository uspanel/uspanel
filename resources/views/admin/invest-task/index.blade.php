@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.invest-task.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0">
                <span>{{ trans('admin.invest-task.actions.index') }}</span>
            </h2>
        </div>
        <div class="col-auto">
            @can('crud.invest-task.create')
                <a class="btn btn-primary" href="{{ url('/admin/invest-tasks/create') }}" role="button">
                    <i class="fa fa-plus"></i>
                    {{ trans('admin.invest-task.actions.create') }}
                </a>
            @endcan
        </div>
    </div>
    <invest-task-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('/admin/invest-tasks') }}'"
        :invest_task_statuses="{{ $invest_task_statuses->toJson() }}"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>

                        <div class="row align-items-end">
                            <div class="col">
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">{{ trans('brackets/admin-ui::admin.placeholder.search') }}</label>
                                            <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">{{ trans('admin.invest-task.columns.invest_task_status') }}</label>
                                            <select v-model="filters.invest_task_status_id" class="form-control" id="invest_task_status_id">
                                                <option value="">@lang('admin.invest-task.columns.invest_task_status_all')</option>
                                                <template v-for="(name, id) in {{ $invest_task_statuses }}">
                                                    <option :key="id" :value="id">@{{name}}</option>
                                                </template>
                                                <option value="-1">@lang('admin.invest-task.columns.remote')</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-auto">
                                        <div class="form-group">
                                            <label for="">{{ trans('admin.invest-task.columns.per_page') }}</label>
                                            <select class="form-control" v-model="pagination.state.per_page">
                                                <option value="10">10</option>
                                                <option value="25">25</option>
                                                <option value="100">100</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-auto align-self-end">
                                        <button type="button" class="btn btn-primary mb-3" @click="filter('search', search)">
                                            <i class="fa fa-search"></i>
                                            {{ trans('brackets/admin-ui::admin.btn.search') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th is='sortable' :column="'id'">{{ trans('admin.invest-task.columns.id') }}</th>
                                        <th is='sortable' :column="'name'">{{ trans('admin.invest-task.columns.name') }}</th>
                                        <th is='sortable' :column="'theme'">{{ trans('admin.invest-task.columns.theme') }}</th>
                                        <th is='sortable' :column="'text'">{{ trans('admin.invest-task.columns.text') }}</th>
                                        <th is='sortable' :column="'time_left'">{{ trans('admin.invest-task.columns.time_left') }}</th>
                                        <th is='sortable' :column="'deadline'">{{ trans('admin.invest-task.columns.deadline') }}</th>
                                        <th is='sortable' :column="'created_at'">{{ trans('admin.invest-task.columns.created_at') }}</th>
                                        <th is='sortable' :column="'invest_task_status_id'">{{ trans('admin.invest-task.columns.invest_task_status') }}</th>
                                        <th is='sortable' :column="'investor_id'">{{ trans('admin.invest-task.columns.investors') }}</th>
                                        <th is='sortable' :column="'contragent_id'">{{ trans('admin.invest-task.columns.contragents') }}</th>
                                        <th is='sortable' :column="'employee_id'">{{ trans('admin.invest-task.columns.employee_id') }}</th>
                                        <th is='sortable' :column="'manager_id'">{{ trans('admin.invest-task.columns.manager_id') }}</th>
                                        <th is='sortable' :column="'creator_id'">{{ trans('admin.invest-task.columns.creator_id') }}</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection">
                                        <td>@{{ item.id ? item.id : '-' }}</td>
                                        <td>@{{ item.name ? item.name : '-' }}</td>
                                        <td>@{{ item.theme ? item.theme : '-' }}</td>
                                        <td v-html="item.templatized_text"></td>
                                        <td>@{{ item.time_left ? item.time_left : '-' }}</td>
                                        <td>@{{ item.deadline ? item.deadline : '-' }}</td>
                                        <td>@{{ item.created_at }}</td>
                                        <td>@{{ item.invest_task_status.title ? item.invest_task_status.title : '-' }}</td>
                                        <td>
                                            <div v-for="investor in item.investors">
                                                @{{ investor.name + ' ' + investor.email + ',' }}<br/><br/>
                                            </div>
                                            <div v-if="item.investor_lists[0]" v-for="investor_list in item.investor_lists">
                                                <div v-if="investor_list.users" v-for="investor in investor_list.users">
                                                    @{{ investor.name + ' ' + investor.email + ',' }}<br/><br/>
                                                </div>
                                            </div>
                                            <div>@{{ !item.investors[0] && !item.investor_lists[0] ? '-' : '' }}</div>
                                        </td>
                                        <td>
                                            <div v-for="contragent in item.contragents">
                                                @{{ contragent.name + ', ' }}<br/><br/>
                                            </div>
                                            <div v-if="item.contragent_lists[0]" v-for="contragent_list in item.contragent_lists">
                                                <div v-if="contragent_list.contragents" v-for="contragent in contragent_list.contragents">
                                                    @{{ contragent.name + ', ' }}<br/><br/>
                                                </div>
                                            </div>
                                            <div>@{{ !item.contragents[0] && !item.contragent_lists[0] ? '-' : '' }}</div>
                                        </td>
                                        <td>@{{ item.employee ? item.employee.user_name : '-' }}</td>
                                        <td>@{{ item.manager ? item.manager.name : '-' }}</td>
                                        <td>@{{ item.creator ? item.creator.name : '-' }}</td>

                                        <td class="nowrap">
                                            <template v-if="_.findIndex(item.media, {collection_name:'files'}) >= 0">
                                                <media-download
                                                    :media="_.find(item.media, {collection_name:'files'})"
                                                    inline-template>
                                                    <a
                                                        class="btn btn-primary btn-sm"
                                                        :href="'/media/' + media.id + '/' + media.file_name"
                                                        target="_blank"
                                                        download>
                                                        <i class="fas fa-download"></i>
                                                    </a>
                                                </media-download>
                                            </template>
                                            @can('crud.invest-task.edit')
                                            <template v-else>
                                                <a class="btn btn-info btn-sm" href="#" @click="currentIndex = index; $modal.show('files_upload')">
                                                    <i class="fas fa-cloud-upload-alt"></i>
                                                </a>
                                            </template>
                                            @endcan
                                            <a class="btn btn-success btn-sm" :href="'/admin/invest-tasks/' + item.id">
                                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                            </a>
                                            @can('crud.invest-task.edit')
                                            <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'"
                                               title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            @endcan
                                            @can('crud.invest-task.edit')
                                            <button v-if="item.deleted_at"
                                            @click="restoreItem(item)"
                                            class="btn btn-success btn-sm"
                                            title="Restore">
                                                <i class="fas fa-trash-restore"></i>
                                            </button>
                                            <button v-else
                                            @click="deleteItem(item.resource_url)"
                                            class="btn btn-sm btn-danger"
                                            title="Delete">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                            @endcan
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center" v-if="!collection.length > 0">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
                <modal name="files_upload" height="auto">
                    <div class="modal-content">
                        <div class="modal-header">
                            @lang('admin.upload_file')
                        </div>
                        <div class="modal-body">
                            @include('brackets/admin-ui::admin.includes.media-uploader', [
                                'mediaCollection' => app(App\InvestTask::class)->getMediaCollection('files'),
                                'label' => trans('admin.invest-task.columns.files'),
                            ])
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" @click="uploadFile('files')">
                                @lang('admin.send')
                            </button>
                        </div>
                    </div>
                </modal>
            </div>
        </div>
    </invest-task-listing>

@endsection
