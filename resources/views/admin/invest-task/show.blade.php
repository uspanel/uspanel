@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.invest-task.task_id', ['id' => $invest_task->id]))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>{{ trans('admin.sales-task.task_id', ['id' => $invest_task->id]) }}</span></h2>
    </div>

    <div class="mb-3">
        <a class="btn btn-light" href="{{ url()->previous() }}">
            @lang('admin.back_to_the_list')
        </a>
    </div>

    <div class="card">
        <div class="card-body">
            <table class="table table-bordered">
                <tbody>

                    @can('crud.invest-task.columns.name.read')
                    <tr>
                        <td class="font-weight-bold">
                            {{ trans('admin.invest-task.columns.name') }}
                        </td>
                        <td>{{ $invest_task->name }}</td>
                    </tr>
                    @endcan

                    @can('crud.invest-task.columns.invest_task_status_id.read')
                    <tr>
                        <td class="font-weight-bold">
                            {{ trans('admin.invest-task.columns.invest_task_status') }}
                        </td>
                        <td>
                            {{ $invest_task_status ? $invest_task_status[0]->title : '-' }}
                            <status-change
                                :invest_task="{{ $invest_task }}"
                                :invest_task_status="{{ $invest_task_status ? $invest_task_status[0] : '-' }}"
                                :invest_task_statuses="{{ $invest_task_statuses }}"
                            >
                            </status-change>
                        </td>
                    </tr>
                    @endcan

                    @can('crud.invest-task.columns.deadline.read')
                    <tr>
                        <td class="font-weight-bold">
                            {{ trans('admin.invest-task.columns.deadline') }}
                        </td>
                        <td>{{ $invest_task->deadline }}</td>
                    </tr>
                    @endcan

                    @can('crud.invest-task.columns.files.read')
                    <tr>
                        <td class="font-weight-bold">
                            {{ trans('admin.invest-task.columns.attachments') }}
                        </td>
                        <td>
                            @foreach ($files as $file)
                                <div>
                                    <a class="btn btn-primary btn-sm"
                                        href="/media/{{ $file->id }}/{{ $file->file_name }}"
                                        target="_blank"
                                        download>
                                        <i class="fas fa-download"></i>
                                    </a>
                                    {{ $file->file_name }}
                                </div>
                            @endforeach
                        </td>
                    </tr>
                    @endcan

                    @can('crud.invest-task.columns.text.read')
                    <tr>
                        <td class="font-weight-bold">
                            {{ trans('admin.invest-task.columns.text') }}
                        </td>
                        <td>{{ $invest_task->text ? $invest_task->text : '-' }}</td>
                    </tr>
                    @endcan

                </tbody>
            </table>

        </div>
    </div>
@endsection
