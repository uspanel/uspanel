<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('mailsetting_id'), 'has-success': this.fields.mailsetting_id && this.fields.mailsetting_id.valid }">
    <label for="mailsetting_id" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.auto-answer.columns.mailsetting_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select v-model="form.mailsetting_id" v-validate="'required|integer'" @input="validate($event)"
                class="form-control"
                :class="{'form-control-danger': errors.has('mailsetting_id'), 'form-control-success': this.fields.mailsetting_id && this.fields.mailsetting_id.valid}"
                id="mailsetting_id" name="mailsetting_id" placeholder="{{ trans('admin.auto-answer.columns.owner') }}">
            <option v-for="ms in ownedMailSettings" :value="ms.id">@{{ ms.login }}</option>
        </select>
        <div v-if="errors.has('mailsetting_id')" class="form-control-feedback form-text" v-cloak>@{{
            errors.first('mailsetting_id') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center"
     :class="{'has-danger': errors.has('mail_templates'), 'has-success': this.fields.mail_templates && this.fields.mail_templates.valid }">
    <label for="mail_templates" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.auto-answer.columns.templates') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect v-model="form.mail_templates"
                     placeholder="{{ trans('brackets/admin-ui::admin.forms.select_options') }}" label="title"
                     track-by="id" :options="{{ $mailtemplates->toJson() }}" :multiple="true"
                     open-direction="bottom"></multiselect>
        <div v-if="errors.has('mail_templates')" class="form-control-feedback form-text" v-cloak>@{{
            errors.first('mail_templates') }}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-9 col-xl-8">
        <div v-for="(tmpl,index) in form.mail_templates" class="card">
            <div class="card-header">@{{ tmpl.title }}</div>
            <div class="card-body">
                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('message_count')}">
                    <label for="message_count" class="col-form-label text-md-right"
                           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.auto-answer.columns.message_count') }}</label>
                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                        <input type="text" v-model="form.mail_templates[index].pivot.message_count"
                               v-validate="'required|integer'" @input="validate($event)" class="form-control"
                               :class="{'form-control-danger': errors.has('message_count')}" id="message_count"
                               name="message_count" placeholder="{{ trans('admin.auto-answer.columns.message_count') }}">
                        <div v-if="errors.has('message_count')" class="form-control-feedback form-text" v-cloak>@{{
                            errors.first('message_count') }}
                        </div>
                    </div>
                </div>
                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('delay')}">
                    <label for="delay" class="col-form-label text-md-right"
                           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.auto-answer.columns.delay') }}</label>
                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                        <input type="text" v-model="form.mail_templates[index].pivot.delay" v-validate="'required|integer'"
                               @input="validate($event)" class="form-control"
                               :class="{'form-control-danger': errors.has('delay')}" id="project_id" name="delay"
                               placeholder="{{ trans('admin.auto-answer.columns.delay') }}">
                        <div v-if="errors.has('delay')" class="form-control-feedback form-text" v-cloak>@{{
                            errors.first('delay') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



