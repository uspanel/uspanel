@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.mail.actions.index'))

@section('body')
    <mail-listing
        :data="{ data: [] }"
        :mailsettings="{{ json_encode($mailsettings) }}"
        :default-mail-settings="{{ json_encode($defaultMailSettings) }}"
        :url="'{{ url('admin/mails') }}'"
        inline-template>

        <div class="gx-module">
            <div id="gxModuleSidebar" class="gx-module-sidenav">
                <div class="module-side">
                    <div class="module-side-header">
                        <div class="module-logo">
                            <h2>@lang('admin.mail.actions.index')</h2>
                        </div>
                    </div>
                    <div class="module-side-content">
                        <div class="module-side-scroll custom-side-scrollbar">
                            @if(auth()->user()->mailsettings->isNotEmpty())
                                <div class="module-add-task">
                                    <button class="btn gx-btn-shadow btn-danger btn-block text-uppercase" @click="deleteSelected">
                                        <i class="zmdi zmdi-delete"></i>
                                        <span>@lang('admin.mail.delete_selected')</span>
                                    </button>
                                </div>
                                <div class="module-add-task">
                                    <a class="btn gx-btn-shadow btn-primary btn-block text-uppercase" role="button" href="{{ url('admin/mails/create') }}">
                                        <i class="zmdi zmdi-edit zmdi-hc-fw"></i>
                                        <span>@lang('admin.mail.actions.create')</span>
                                    </a>
                                </div>
                            @endif
                            <select class="form-control" v-model="filterAcc" v-if="hasOwned">
                                <option :value="null">@lang('admin.mail.actions.all_accounts')</option>
                                <option value="personal">@lang('admin.mail.actions.personal')</option>
                                <option value="owned">@lang('admin.mail.actions.owned')</option>
                            </select>
                            <ul class="module-nav">
                                <template v-for="item in filteredMs">
                                    <mail-folder :get-messages="getMessages" :notification="notification" :settings="item" :params="params" :set-params="setParams" :key="item.id"></mail-folder>
                                </template>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="module-box">
                <div class="module-box-header">
                    <a id="gxModuleSideNav" href="javascript:void(0)" class="drawer-btn d-block d-lg-none">
                        <i class="zmdi zmdi-menu"></i>
                    </a>
                    <div class="module-box-header-inner">
                        <div class="search-bar right-side-icon bg-transparent">
                            <div class="form-group">
                                <input class="form-control border-0" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="params.search" @keyup.enter="getMessages" type="search" />
                                <button class="search-icon" @click="getMessages"><i class="zmdi zmdi-search zmdi-hc-lg"></i></button>
                            </div>
                        </div>
                        <div class="search-addressee-bar">
                            <div class="form-group">
                                <multiselect
                                :options="{{$addressees->toJson()}}"
                                :label="'addressee'"
                                :track-by="'addressee'"
                                v-model="addresseeFilter"
                                ></multiselect>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="module-box-content">
                    <div class="module-list mail-list">
                        <div class="custom-scrollbar module-mail-list-scroll table-responsive">
                            <div class="loader-backdrop" v-if="pending">
                                <div class="loader">
                                    <svg class="circular" viewBox="25 25 50 50">
                                        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
                                    </svg>
                                </div>
                            </div>
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th>
                                            <div class="form-checkbox">
                                                <input type="checkbox" id="selectAll" v-model="selectedAll" :value="true" @input="selectAll">
                                                <span class="check">
                                                    <i class="zmdi zmdi-check zmdi-hc-lg"></i>
                                                </span>
                                            </div>
                                        </th>
                                        <th is='sortable' :column="'sender'">@lang('admin.mail.from')</th>
                                        <th is='sortable' :column="'subject'">@lang('admin.mail.subject')</th>
                                        <th is='sortable' :column="'recipient'">@lang('admin.mail.recipient')</th>
                                        <th is='sortable' :column="'date'">@lang('admin.mail.date')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection" :class="{ 'font-weight-bold': !item.seen }">
                                        <td>
                                            <div class="form-checkbox">
                                                <input v-model="selectedItems" :value="item.uid" type="checkbox" :id="'selectItem'+item.uid" @change="selectItem">
                                                <span class="check">
                                                    <i class="zmdi zmdi-check zmdi-hc-lg"></i>
                                                </span>
                                            </div>
                                        </td>
                                        <td @click="openMessage(item.uid)" class="pointer">
                                            @{{ item.sender }}
                                        </td>
                                        <td @click="openMessage(item.uid)" class="pointer">
                                            @{{ item.subject }}
                                        </td>
                                        <td @click="openMessage(item.uid)" class="pointer">
                                            @{{ item.recipient }}
                                        </td>
                                        <td @click="openMessage(item.uid)" class="pointer content-left">
                                            @{{ item.date }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="module-list-item mail-cell" v-if="pagination.state.total > 0">
                                <b-pagination size="md" :total-rows="pagination.state.total" v-model="params.page" :per-page="pagination.state.per_page" @change="getMessages" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="col">
                <div class="card" :class="{ pending: pending }">
                    <div class="card-body" v-cloak>
                        <form @submit.prevent="">
                            <div class="row justify-content-md-between">
                                <div class="col col-lg-7 col-xl-5 form-group">
                                    <div class="input-group">
                                        <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="params.search" @keyup.enter="getMessages" />
                                        <span class="input-group-append">
                                            <button type="button" class="btn btn-primary" @click="getMessages"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                        </span>
                                    </div>
                                </div>

                                <div class="col-sm-auto form-group ">
                                    <select class="form-control" v-model="params.per_page" @change="getMessages">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>

                            </div>
                        </form>
                        <div class="row">

                            <div class="col-md-9 col-lg-10">
                                <div class="table-responsive">
                                    <table class="table table-hover table-listing">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="selectAll" v-model="selectedAll" :value="true" @input="selectAll">
                                                        <label class="custom-control-label" for="selectAll">&nbsp;</label>
                                                    </div>
                                                </th>
                                                <th is='sortable' :column="'sender'">@lang('admin.mail.from')</th>
                                                <th is='sortable' :column="'subject'">@lang('admin.mail.subject')</th>
                                                <th is='sortable' :column="'recipient'">@lang('admin.mail.recipient')</th>
                                                <th is='sortable' :column="'date'">@lang('admin.mail.date')</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="(item, index) in collection" :class="{ 'font-weight-bold': !item.seen }">
                                                <td>
                                                    <div class="form-checkbox">
                                                        <input v-model="selectedItems" :value="item.uid" type="checkbox" :id="'selectItem'+item.uid" @change="selectItem">
                                                        <span class="check">
                                                            <i class="zmdi zmdi-check zmdi-hc-lg"></i>
                                                        </span>
                                                    </div>
                                                </td>
                                                <td @click="openMessage(item.uid)" class="pointer">
                                                    @{{ item.sender }}
                                                </td>
                                                <td @click="openMessage(item.uid)" class="pointer">
                                                    @{{ item.subject }}
                                                </td>
                                                <td @click="openMessage(item.uid)" class="pointer">
                                                    @{{ item.recipient }}
                                                </td>
                                                <td @click="openMessage(item.uid)" class="pointer content-left">
                                                    @{{ item.date }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row" v-if="pagination.state.total > 0">
                            <div class="col-sm">
                                <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                            </div>
                            <div class="col-sm-auto">
                                <b-pagination size="md" :total-rows="pagination.state.total" v-model="params.page" :per-page="pagination.state.per_page" @change="getMessages" />
                            </div>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
    </mail-listing>

@endsection
