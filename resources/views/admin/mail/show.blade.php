@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.mailsetting.actions.edit', ['name' => null]))

@section('body')
    <mail-show
    :id="'{{ $id }}'"
    :mailsettings-id="'{{ $mailsettingsId }}'"
    :mailsettings="{{ $mailsettings }}"
    :folder="'{{ $folder }}'"
    inline-template>
        <div>

            <div class="module-box-content mb-4">
                <div class="module-box-topbar">
                    <h2 class="mb-0">
                        <a class="mr-3" href="{{ url()->previous() }}">
                            <i class="zmdi zmdi-arrow-back text-primary"></i>
                        </a>
                        @lang('admin.back')
                    </h2>

                    <div class="module-topbar-user-detail ml-2">
                        <div class="d-flex justify-content-between ml-xl-4 ml-2">
                            <a class="action-btn" href="#" @click.prevent="reply = !reply">
                                <i class="zmdi zmdi-mail-reply"></i>
                            </a>

                            <a class="action-btn" href="#" @click.prevent="$modal.show('forward')">
                                <i class="zmdi zmdi-forward"></i>
                            </a>

                            <a class="action-btn" href="javascript:void(0)" @click="deleteMessage">
                                <i class="zmdi zmdi-delete"></i>
                            </a>
                        </div>
                    </div>
                </div>


                <div class="form-horizontal form-edit">

                    
                    <div class="card-body">
                        <div class="form-group row align-items-center">
                            <label class="text-md-right col-md-2">
                                @lang('admin.mail.from')
                            </label>
                            <div class="col-md-9 col-xl-8">
                                @{{ message.attributes.sender }}
                            </div>
                        </div>
                        <div class="form-group row align-items-center">
                            <label class="text-md-right col-md-2">
                                @lang('admin.mail.subject')
                            </label>
                            <div class="col-md-9 col-xl-8">
                                @{{ message.attributes.subject }}
                            </div>
                        </div>
                        <hr>
                        <div class="form-group row align-items-center">
                            <div class="text-muted text-md-right col-md-2"></div>
                            <div class="col-md-9 col-xl-8" v-html="message.attributes.message">

                            </div>
                        </div>
                        <hr>
                        <div class="form-group row align-items-center">
                            <label class="text-md-right col-md-2">
                                @lang('admin.mail.attachments')
                            </label>
                            <div class="col-md-9 col-xl-8">
                                <div class="row">
                                    <div class="col-sm-2 text-center" v-for="item in message.attributes.attachments">
                                        <a :href="'/csmailclient/message/'+id+'/attachment/'+item.id+'?set_id='+mailsettingsId+'&folder='+folder">
                                            @{{ item.name }}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <mail-form
                v-show="reply"
                :message="message"
                :action="''"
                :data="{}"
                :mailsettings="{{ json_encode($mailsettings) }}"
                :default-mail-settings="{{ json_encode($defaultMailSettings) }}"
                inline-template>
                <div class="card">
                    <div class="card-body">

                        @include('admin.mail.components.form-elements')

                    </div>
                </div>
            </mail-form>
            @include('admin.mail.components.forward')
        </div>
    </mail-show>
@endsection
