<modal name="forward" height="auto">
    <mail-form
        :message="message"
        :mailsettings="mailsettings"
        :current-mail-settings="{{ json_encode($currentMailSettings) }}"
        :default-mail-settings="{{ json_encode($defaultMailSettings) }}"
        :folder="'{{ $folder }}'"
        :action="''"
        :data="{}"
        inline-template>
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-group row align-items-center" :class="{'has-danger': errors.has('recipient'), 'has-success': this.fields.recipient && this.fields.recipient.valid }">
                    <label for="recipient" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">Recipient</label>
                    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                        <multiselect
                            v-model="form.recipients"
                            :options="users"
                            :multiple="true"
                            label="full"
                            :close-on-select="false"
                            :clear-on-select="false"
                            :preserve-search="true"
                            :taggable="true"
                            label="mail"
                            track-by="mail"
                            @tag="addTag">

                        </multiselect>
                        <div v-if="errors.has('recipient')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('recipient') }}</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" @click="$modal.hide('forward')">Cancel</button>
                <button type="button" name="button" class="btn btn-primary" @click="forward">
                    @lang('admin.btn.send')
                </button>
            </div>
        </div>
    </mail-form>
</modal>
