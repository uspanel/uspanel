<vue-progress-bar></vue-progress-bar>
<div>
    <div class="form-group row align-items-center">
        <label for="sender" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">@lang('admin.mail.sender')</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <select v-model="form.sender" class="form-control">
                <option v-for="item in mailsettings" :value="item.id">
                    @{{ item.login }}
                </option>
            </select>
            <div v-if="errors.has('sender')" class="text-danger" v-cloak>@{{
                errors.first('sender') }}
            </div>
        </div>
    </div>

    <div class="form-group row align-items-center">
        <label for="recipient" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">@lang('admin.mail.recipient')</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <multiselect
                open-direction="top"
                v-model="form.recipients"
                :options="users"
                :multiple="true"
                :close-on-select="true"
                :clear-on-select="false"
                :preserve-search="true"
                :taggable="true"
                label="full"
                track-by="mail"
                @tag="addTag">

            </multiselect>
            <div v-if="errors.has('recipients')" class="text-danger" v-cloak>@{{
                errors.first('recipients') }}
            </div>
        </div>
    </div>

    <div class="form-group row align-items-center">
        <label for="subject" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">@lang('admin.mail.subject')</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.subject" class="form-control" placeholder="Subject">
            <div v-if="errors.has('subject')" class="text-danger" v-cloak>@{{
                errors.first('subject') }}
            </div>
        </div>
    </div>

    <div class="form-group row align-items-center">
        <label for="subject" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">@lang('admin.mail.typeMail')</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <span class="typeMail" :class="typeMail == 'text/plain' ? 'active' : ''" @click="selectTypeMail('text/plain')">Text</span>
            <span class="typeMail" :class="typeMail == 'text/html' ? 'active' : ''" @click="selectTypeMail('text/html')">Html</span>
        </div>
    </div>


    <div  v-show="typeMail == 'text/html'" class="form-group row align-items-center">
        <label for="template" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">@lang('admin.mail.template')</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <select v-model="template" class="form-control" @change="setTemplate">
                <option value=""></option>
                <option v-for="template in templates" :value="template.id">@{{ template.attributes.title }}</option>
            </select>
        </div>
    </div>

    <div  class="form-group row align-items-center">
        <label for="message" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'"></label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            {{-- <textarea type="text" v-model="form.message" class="form-control" rows="20"></textarea> --}}
            <quill-editor v-show="typeMail == 'text/html'"
                ref="myTextEditor"
                v-model="form.message"
                :options="editorOption">
            </quill-editor>

            <div v-show="typeMail == 'text/plain'" class="form-group row align-items-center" :class="{'has-danger': errors.has('message'), 'has-success': fields.message && fields.message.valid }">
                <div :class="isFormLocalized ? 'col-md-4' : 'col-md-12 col-xl-12'">
                    <div>
                        <textarea v-model="form.text" class="form-control" @change="replace" placeholder="@lang('admin.mail.text_mail')"></textarea>
                    </div>
                </div>
            </div>

            <div v-if="errors.has('message')" class="form-control-feedback form-text text-danger" v-cloak>@{{
                errors.first('message') }}
            </div>
            <div class="text-right mt-3">
                <button class="btn btn-primary" @click="send">
                    <i class="fa fa-paper-plane"></i>
                    {{ trans('admin.btn.send') }}
                </button>
                <button v-show="typeMail == 'text/html'" class="btn btn-danger" @click="destroyTemplate" v-if="!public && template">
                    @lang('admin.mail.delete')
                </button>
                <button v-show="typeMail == 'text/html'" class="btn btn-primary"
                        @click="$modal.show('newTemplate',{tmpltitle: 'tmpltitle', storeTemplate: 'storeTemplate'})">
                    @lang('admin.mail.save_as_new_template')
                </button>
            </div>
        </div>
    </div>



    <div  class="form-group row align-items-center" v-if="!form.tmplatt || !form.tmplatt.length">
        <label class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'"></label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="file" multiple ref="attachments">
        </div>
    </div>
    <template v-else>
        <div class="form-group row align-items-center" v-for="att in form.tmplatt">
            <label class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'"></label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <a :href="'/storage/{{config('csmailclient.file_storage')}}/'+tmplid+'/'+att.name" target="_blank">@{{
                    att.name }}</a>
            </div>
        </div>
        <div class="form-group row align-items-center">
            <label class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'"></label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <a href="#" @click.stop.prevent="removeTmplAtt"> @lang('admin.mail.actions.other_files')</a>
            </div>
        </div>
    </template>
    <modal name="newTemplate" height="auto">
        <tmpl-modal :tmpltitle.sync="tmpltitle" :store-template="storeTemplate" :public.sync="public" inline-template>
            <div class="modal-content">
                <div class="modal-header">
                    @lang('admin.mail.new_template')
                    <div slot="top-right">
                        <a class="pointer" @click="$modal.hide('newTemplate')">
                            ❌
                        </a>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">@lang('admin.mail.name')</label>
                        <input type="text" class="form-control" v-model="tmpltit">
                    </div>
                    <div class="form-check row"
                         :class="{'has-danger': errors.has('validate_cert'), 'has-success': this.fields.validate_cert && this.fields.validate_cert.valid }">
                        <div class="ml-md-auto" class="col-md-8">
                            <input class="form-check-input" id="is_public" type="checkbox" v-model="pub" v-validate="''"
                                   data-vv-name="is_public" name="default_fake_element" v-bind:true-value="1" v-bind:false-value="0">
                            <label class="form-check-label" for="is_public">
                                {{ trans('admin.mail.public_template') }}
                            </label>
                            <input type="hidden" name="validate_cert" :value="public">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" @click="storeTemplate">
                        @lang('admin.mail.save')
                    </button>
                </div>
            </div>
        </tmpl-modal>
    </modal>

</div>
