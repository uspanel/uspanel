@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.mail.actions.create'))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>@lang('admin.mail.actions.create')</span></h2>
    </div>

    <mail-form
        action=""
        :mailsettings="{{ json_encode($mailsettings) }}"
        :default-mail-settings="{{ json_encode($defaultMailSettings) }}"
        :users-list="{{$usersList->toJson()}}"
        :rcpt="'{{$rcpt}}'"
        inline-template>
        <div class="card">
            <div class="card-body">
                @include('admin.mail.components.form-elements')
            </div>
        </div>
    </mail-form>

@endsection
