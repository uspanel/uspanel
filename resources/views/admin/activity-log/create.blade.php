@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.activity-log.actions.create'))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>{{ trans('admin.activity-log.actions.create') }}</span></h2>
    </div>

    <activity-log-form
        :action="'{{ url('/admin/activity-logs') }}'"
        
        inline-template>
        
        <div class="card">
            <div class="card-body">
                <form method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>
                    @include('admin.activity-log.components.form-elements')
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        

    </activity-log-form>


@endsection