@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.history.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0">
                <span>{{ trans('admin.history.actions.index') }}</span>
            </h2>
        </div>

    </div>
    <activity-log-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('/admin/history') }}'"
        inline-template>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <div class="row align-items-end">
                            <div class="col">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">@lang('admin.history.user')</label>
                                            <select class="form-control" name="" v-model="filters.user_id">
                                                <option value="0" selected>@lang('admin.all')</option>
                                                    @foreach ($users as $user)
                                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                                    @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">@lang('admin.history.columns.ip_address')</label>
                                            <input v-model="filters.ip_address" type="text" name="ip_address" value=""
                                                   placeholder="@lang('admin.history.columns.ip_address')"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">@lang('admin.history.columns.project')</label>
                                            <input v-model="filters.contragent" type="text" name="contragent" value=""
                                                   placeholder="@lang('admin.history.columns.project')"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">@lang('admin.history.columns.date_start')</label>
                                            <input v-model="filters.date_start" type="date" name="date_start" value=""
                                                placeholder="@lang('admin.history.columns.date_start')"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">@lang('admin.history.columns.date_end')</label>
                                            <input v-model="filters.date_end" type="date" name="date_end" value=""
                                                placeholder="@lang('admin.history.columns.date_end')"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">@lang('admin.history.columns.action')</label>
                                            <input class="form-control"
                                                placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}"
                                                v-model="search"
                                                @keyup.enter="filter('search', $event.target.value)"
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row align-items-end">
                            <div class="col">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <button type="button" class="btn btn-primary mb-3" @click="filter('search', search)">
                                                <i class="fa fa-search"></i>
                                                {{ trans('brackets/admin-ui::admin.btn.search') }}
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3"></div>
                                    <div class="col-md-3">
                                        <select class="form-control" v-model="pagination.state.per_page">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th is='sortable' :column="'id'">{{ trans('admin.history.columns.id') }}</th>
                                        <th is='sortable' :column="'user_id'">{{ trans('admin.history.columns.user_id') }}</th>
                                        <th is='sortable' :column="'text'">{{ trans('admin.history.columns.action') }}</th>
                                        <th is='sortable' :column="'text'">{{ trans('admin.history.columns.project') }}</th>
                                        <th is='sortable' :column="'created_at'">{{ trans('admin.history.columns.date') }}</th>
                                        <th is='sortable' :column="'ip_address'">{{ trans('admin.history.columns.ip') }}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection">
                                        <td>@{{ item.id }}</td>
                                        <td>@{{ '[' + _.get(item, 'causer.id', '') + ']' + _.get(item, 'causer.name', '') }}</td>
                                        <td>@{{ item.description }}</td>
                                        <td>@{{ _.get(item, 'contragent.name', '') }}</td>
                                        <td>@{{ item.created_at }}</td>
                                        <td>@{{ item.ip_address }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center" v-if="!collection.length > 0">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </activity-log-listing>

@endsection
