@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.statistic.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0">
                <span>{{ trans('admin.statistic.actions.index') }}</span>
            </h2>
        </div>
    </div>
    <statistic-listing
        :data="{{ json_encode($data) }}"
        :url="'{{ url('/admin/statistics') }}'"
        inline-template>
        <div>
            <div class="card">
                <div class="card-header">
                    @lang('admin.statistic.by_user_status')
                </div>
                <div class="card-body" v-cloak>
                    <apexchart type=bar height=350 :options="userStatusesOptions" :series="userStatusesSeries" />
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    @lang('admin.statistic.by_completed_tasks')
                </div>
                <div class="card-body" v-cloak>
                    <apexchart type=bar height=350 :options="userTasksOptions" :series="userTasksSeries" />
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    @lang('admin.statistic.by_mailings')
                </div>
                <div class="card-body" v-cloak>
                    <apexchart type=bar height=350 :options="mailingsOptions" :series="mailingsSeries" />
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    @lang('admin.statistic.referrers')
                </div>
                <div class="card-body" v-cloak>
                    <apexchart type=bar height=350 :options="userReferrersOptions" :series="userReferrersSeries" />
                </div>
            </div>
        </div>
    </statistic-listing>

@endsection
