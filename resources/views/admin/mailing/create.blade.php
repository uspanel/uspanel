@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.mailing.actions.create'))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>{{ trans('admin.mailing.actions.create') }}</span></h2>
    </div>

    <mailing-form
        :action="'{{ url('/admin/mailings') }}'"
        :mail-templates="{{ $mailTemplates->toJson() }}"
        :email-groups="{{ $emailGroups->toJson() }}"
        inline-template>
        
        <div class="card">
            <div class="card-body">
                <form method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>
                    @include('admin.mailing.components.form-elements')
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        

    </mailing-form>


@endsection