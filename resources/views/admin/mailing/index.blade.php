@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.mailing.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0">
                <span>{{ trans('admin.mailing.actions.index') }}</span>
            </h2>
        </div>
        <div class="col-auto">
            @if(Auth::user()->hasPermissionTo('crud.mailing.create'))
                <a class="btn btn-primary" href="{{ url('/admin/mailings/create') }}" role="button">
                    <i class="fa fa-plus"></i>
                    {{ trans('admin.mailing.actions.create') }}
                </a>
            @endif
        </div>

    </div>
    <mailing-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('/admin/mailings') }}'"
        :mail_settings="{{ $mail_settings->toJson() }}"
        :curent_setting="{{ $mail_setting->toJson() }}"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <form @submit.prevent="">
                            <div class="row justify-content-md-between align-items-end">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.mailing.columns.mail_template_id')</label>
                                        <select class="form-control" name="" v-model="filters.mail_template_id">
                                            <option :value="null">@lang('admin.all')</option>
                                            <option value="111">111</option>
                                            @foreach ($mailTemplates as $value)
                                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.mailing.columns.email_group_id')</label>
                                        <select class="form-control" name="" v-model="filters.email_group_id">
                                            <option :value="null">@lang('admin.all')</option>
                                            <option value="111">111</option>
                                            @foreach ($emailGroups as $value)
                                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.mailing.columns.scheduled_at_from')</label>
                                        <input type="date" class="form-control" v-model="filters.scheduled_at_from">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.mailing.columns.scheduled_at_to')</label>
                                        <input type="date" class="form-control" v-model="filters.scheduled_at_to">
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <button type="button" class="btn btn-primary mb-3"
                                            @click="filter('search', search)">
                                        <i class="fa fa-search"></i>
                                        {{ trans('brackets/admin-ui::admin.btn.search') }}
                                    </button>
                                </div>
                                <div class="col-sm-auto form-group">
                                    <label for="">&nbsp;</label>
                                    <select class="form-control" v-model="pagination.state.per_page">

                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>

                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-hover table-listing">
                                <thead>
                                <tr>
                                    @include('admin.layout.cells.th', ['name' => 'id'])
                                    @include('admin.layout.cells.th', ['name' => 'name'])
                                    @include('admin.layout.cells.th', ['name' => 'mail_template_id'])
                                    @include('admin.layout.cells.th', ['name' => 'email_groups'])
                                    @include('admin.layout.cells.th', ['name' => 'scheduled_at'])
                                    <th class="nowrap">{{ trans('admin.mailing.columns.stats') }}</th>
                                    <th class="nowrap">{{ trans('admin.mailing.columns.settings') }}</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(item, index) in collection">
                                    @include('admin.layout.cells.td', ['name' => 'id'])
                                    @include('admin.layout.cells.td', ['name' => 'name'])
                                    @include('admin.layout.cells.td', ['name' => 'mail_template_id', 'field' => 'mail_template.name'])
                                    @include('admin.layout.cells.element-list', ['name' => 'email_groups', 'elements' => 'item.email_groups'])
                                    @include('admin.layout.cells.td', ['name' => 'scheduled_at'])


                                    <td style="width: 21%;">
                                       <span v-for="(count,index) in item.stats" class="badge badge-pill mb-1" :class="chooseBg(index)">@{{index}} <span class="badge badge-pill badge-light mb-0">@{{ count }}</span> </span>
                                    </td>

                                    <td style="width: 21%;">

                                        <multiselect
                                            v-model="mail_setting"
                                            placeholder="{{ trans('brackets/admin-ui::admin.forms.select_an_option') }}"
                                            :options="mail_settings"
                                            track-by="id"
                                            label="smtp_host"
                                            :multiple="false"
                                            open-direction="bottom"
                                        >
                                        </multiselect>
                                    </td>
                                    <td class="nowrap">
                                        @can('crud.history-mailing.view')
                                            <button class="gx-btn gx-btn-amber gx-btn-xs mr-0"
                                                    @click="openMailingTestModal(item.id)" data-toggle="tooltip"
                                                    data-placement="top" title=""
                                                    :data-original-title="'{{ trans('admin.mailing.run_test') }}'">
                                                <i class="zmdi zmdi-email"></i>
                                            </button>
                                            <button class="btn btn-primary btn-sm"
                                                    @click="item.history_mailings && item.history_mailings.length ? resend(item.id) : toQueue(item.id)"
                                                    data-toggle="tooltip" data-placement="top" title=""
                                                    :data-original-title="'{{ trans('admin.mailing.add_to_queue') }}'">
                                                <i v-if="item.history_mailings" class="zmdi zmdi-repeat"></i>
                                                <i v-else class="zmdi zmdi-play"></i>
                                            </button>
                                            <button class="btn btn-primary btn-sm"
                                                    @click="item.active_sendings ? stopSending(item.id) : sendNow(item.id)"
                                                    data-toggle="tooltip" data-placement="top" title=""
                                                    :data-original-title="'{{ trans('admin.mailing.start_stop_sending') }}'">
                                                <i v-if="item.active_sendings" class="zmdi zmdi-stop"></i>
                                                <i v-else class="zmdi zmdi-mail-send"></i>
                                            </button>
                                            <a :href="'/admin/mailings/' + item.id + '/history-mailings'"
                                               class="btn btn-secondary btn-sm" role="button" data-toggle="tooltip"
                                               data-placement="top" title=""
                                               :data-original-title="'{{ trans('admin.mailing.history') }}'">
                                                <i class="zmdi zmdi-format-list-numbered"></i>
                                            </a>
                                        @endcan
                                        @if(Auth::user()->hasPermissionTo('crud.mailing.edit'))
                                            <a class="btn btn-sm btn-spinner btn-info"
                                               :href="item.resource_url + '/edit'"
                                               title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i
                                                    class="fa fa-edit"></i></a>
                                        @endif
                                        @if(Auth::user()->hasPermissionTo('crud.task.delete'))
                                            <button
                                                @click="deleteItem(item.resource_url)"
                                                class="btn btn-sm btn-danger"
                                                title="Delete">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                        <div class="text-center" v-if="!collection.length > 0">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
            </div>
            <modal name="mailingTest">
                <div class="modal-content d-flex h-100 flex-column">
                    <div class="modal-header">
                        <div>
                            @lang('admin.mailing.test')
                        </div>
                        <button type="button" class="close" @click="$modal.hide('mailingTest')">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body h-100">
                        <div class="form-group">
                            <label for="">@lang('admin.mailing.emails_comma_separated_without_spaces')</label>
                            <textarea class="form-control" v-model="emails"></textarea>
                            <div v-if="errors.has('emails')" class="text-danger" v-cloak>@{{ errors.first('emails') }}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" @click="testMailing">
                            @lang('admin.mailing.run_test')
                        </button>
                    </div>
                </div>
            </modal>
            <modal name="mailingResend">
                <div class="modal-content d-flex h-100 flex-column">
                    <div class="modal-header">
                        <div>
                            @lang('admin.mailing.resend')
                        </div>
                        <button type="button" class="close" @click="$modal.hide('mailingResend')">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body h-100">
                        <div class="form-group">
                            <label for="">@lang('admin.mailing.columns.scheduled_at')</label>
                            <div class="input-group input-group--custom mb-4">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <datetime v-model="newSchedule" :config="datetimePickerConfig"
                                          v-validate="'date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr"
                                          placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
                            </div>
                            <div>
                                {{ trans('admin.mailing.resend_info') }}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" @click="resendMailing">
                            @lang('admin.mailing.resend')
                        </button>
                    </div>
                </div>
            </modal>
        </div>
    </mailing-listing>

@endsection
