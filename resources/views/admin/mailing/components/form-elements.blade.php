@include('admin.layout.inputs.text', [
    'name' => 'name'
])

@include('admin.layout.inputs.text', [
    'name' => 'max_per_min'
])

@include('admin.layout.inputs.select', [
    'name' => 'mail_template_id',
    'arrayName' => 'mailTemplates',
])

@include('admin.layout.inputs.multiselect', [
    'name' => 'email_groups',
    'options' => $emailGroups
])

@include('admin.layout.inputs.datetime', [
    'name' => 'scheduled_at'
])
