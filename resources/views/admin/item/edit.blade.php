@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.item.actions.edit', ['name' => $item->name]))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0">
            <span>
                {{ trans('admin.item.actions.edit', ['name' => $item->name]) }}
            </span>
        </h2>
    </div>
    <div class="card">
        <div class="card-body">
            <item-edit :data="{{ $item }}"></item-edit>
            
        </div>
    </div>
    
@endsection