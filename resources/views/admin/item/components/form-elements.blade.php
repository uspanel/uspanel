<item-nomenclature v-on:on-select="onSelectItem" v-model="item"></item-nomenclature>

<div class="form-group row align-items-center">
    <label for="size" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.item.columns.size') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.size" class="form-control" placeholder="{{ trans('admin.item.columns.size') }}" :readonly="item && item.id">
        <div v-if="errors.has('size')" class="text-danger" v-cloak>@{{ errors.first('size') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="color" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.item.columns.color') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.color" class="form-control" placeholder="{{ trans('admin.item.columns.color') }}" :readonly="item && item.id">
        <div v-if="errors.has('color')" class="text-danger" v-cloak>@{{ errors.first('color') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="weight" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.item.columns.weight') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.weight" class="form-control" placeholder="{{ trans('admin.item.columns.weight') }}" :readonly="item && item.id">
        <div v-if="errors.has('weight')" class="text-danger" v-cloak>@{{ errors.first('weight') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="info" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.item.columns.info') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <textarea class="form-control" v-model="form.info" :readonly="item && item.id"></textarea>
        </div>
        <div v-if="errors.has('info')" class="text-danger" v-cloak>@{{ errors.first('info') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="link" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.item.columns.link') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.link" class="form-control" placeholder="{{ trans('admin.item.columns.link') }}" :readonly="item && item.id">
        <div v-if="errors.has('link')" class="text-danger" v-cloak>@{{ errors.first('link') }}</div>
    </div>
</div>
