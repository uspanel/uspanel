@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.project.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0"><span>@lang('admin.project.actions.index')</span></h2>
        </div>
        <div class="col-auto">
            <a class="btn btn-primary" href="{{ url('admin/projects/create') }}" role="button">
                <i class="fa fa-plus"></i>
                @lang('admin.project.actions.create')
            </a>
        </div>
    </div>
    <project-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/projects') }}'"
        inline-template>

        <div class="card">
            <div class="card-body">
                <div class="row align-items-end">
                    <div class="col">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">@lang('admin.company.columns.name')</label>
                                    <input v-model="filters.name" type="text" name="" value="" placeholder="@lang('admin.company.columns.name')" class="form-control">
                                </div>
                            </div>
                            <template v-if="showFilters">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.company.columns.address')</label>
                                        <input v-model="filters.address" type="text" name="" value="" placeholder="@lang('admin.company.columns.address')" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.company.columns.city')</label>
                                        <input v-model="filters.city" type="text" name="" value="" placeholder="@lang('admin.company.columns.city')" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.company.columns.state')</label>
                                        <input v-model="filters.state" type="text" name="" value="" placeholder="@lang('admin.company.columns.state')" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.company.columns.zip')</label>
                                        <input v-model="filters.zip" type="text" name="" value="" placeholder="@lang('admin.company.columns.zip')" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.company.columns.phone')</label>
                                        <input v-model="filters.phone" type="text" name="" value="" placeholder="@lang('admin.company.columns.phone')" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.company.columns.second_phone')</label>
                                        <input v-model="filters.second_phone" type="text" name="" value="" placeholder="@lang('admin.company.columns.second_phone')" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.company.columns.fax')</label>
                                        <input v-model="filters.fax" type="text" name="" value="" placeholder="@lang('admin.company.columns.fax')" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.company.columns.site')</label>
                                        <input v-model="filters.site" type="text" name="" value="" placeholder="@lang('admin.company.columns.site')" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.company.columns.info_email')</label>
                                        <input v-model="filters.info_email" type="text" name="" value="" placeholder="@lang('admin.company.columns.info_email')" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.company.columns.manager_id')</label>
                                        <select name="" class="form-control" v-model="filters.manager_id">
                                            @foreach ($hr as $item)
                                                <option value="{{ $item->id }}">
                                                    {{ $item->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.company.columns.support_id')</label>
                                        <select name="" class="form-control" v-model="filters.support_id">
                                            @foreach ($support as $item)
                                                <option value="{{ $item->id }}">
                                                    {{ $item->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </template>
                        </div>

                    </div>
                    <div class="col-auto">
                        <button class="btn btn-warning mb-3 text-white" @click="showFilters = !showFilters">
                            &nbsp;
                            <i class="fa fa-filter" aria-hidden="true"></i>
                            &nbsp;
                        </button>
                        <button type="button" class="btn btn-primary mb-3" @click="filter('search', search)">
                            <i class="fa fa-search"></i>
                            {{ trans('brackets/admin-ui::admin.btn.search') }}
                        </button>
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-auto form-group">
                        <select class="form-control" v-model="pagination.state.per_page">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover table-listing">
                        <thead>
                            <tr>
                                <th class="nowrap" is='sortable' :column="'name'">{{ trans('admin.company.columns.name') }}</th>
                                <th class="nowrap" is='sortable' :column="'address'">{{ trans('admin.company.columns.address') }}</th>
                                <th class="nowrap" is='sortable' :column="'zip'">{{ trans('admin.company.columns.zip') }}</th>
                                <th class="nowrap" is='sortable' :column="'phone'">{{ trans('admin.company.columns.phone') }}</th>
                                <th class="nowrap" is='sortable' :column="'fax'">{{ trans('admin.company.columns.fax') }}</th>
                                <th class="nowrap" is='sortable' :column="'site'">{{ trans('admin.company.columns.site') }}</th>
                                <th class="nowrap" is='sortable' :column="'info_email'">{{ trans('admin.company.columns.info_email') }}</th>
                                <th class="nowrap" is='sortable' :column="'manager_id'">{{ trans('admin.company.columns.hr_manager_name') }}</th>
                                <th class="nowrap" is='sortable' :column="'manager_id'">{{ trans('admin.company.columns.hr_manager_email') }}</th>
                                <th class="nowrap" is='sortable' :column="'support_id'">{{ trans('admin.company.columns.support_name') }}</th>
                                <th class="nowrap" is='sortable' :column="'support_id'">{{ trans('admin.company.columns.support_email') }}</th>
{{--                                <th class="nowrap" is='sortable' :column="'control_panel'">{{ trans('admin.company.columns.control_panel') }}</th>--}}
                                <th class="nowrap" is='sortable' :column="'position_name'">{{ trans('admin.company.columns.position_name') }}</th>
{{--                                <th class="nowrap" is='sortable' :column="'application'">{{ trans('admin.company.columns.application') }}</th>--}}

                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(item, index) in collection">
                                <td>@{{ item.company.name }}</td>
                                <td>
                                    @{{ item.company.city }}
                                    @{{ item.company.state.name }}
                                    @{{ item.company.address }}
                                </td>
                                <td>@{{ item.company.zip }}</td>
                                <td>
                                    @{{ item.company.second_phone ? item.company.phone + ',' : item.company.phone }}
                                    <div v-if="item.company.second_phone">
                                        @{{ item.company.second_phone }}
                                    </div>
                                </td>
                                <td>@{{ item.company.fax }}</td>
                                <td>
                                    <a :href="item.company.site">@{{ item.company.site }}</a>
                                </td>
                                <td>@{{ item.company.info_email }}</td>
                                <td>
                                    <template v-for="user in item.managers">
                                        @{{ user.name }}
                                    </template>
                                </td>
                                <td>
                                    <template v-for="user in item.managers">
                                        @{{ user.email }}
                                    </template>
                                </td>
                                <td>
                                    <template v-for="user in item.supports">
                                        @{{ user.name }}
                                    </template>
                                </td>
                                <td>
                                    <template v-for="user in item.supports">
                                        @{{ user.email }}
                                    </template>
                                </td>
{{--                                <td>--}}
{{--                                    <a :href="item.company.control_panel">@{{ item.company.control_panel }}</a>--}}
{{--                                </td>--}}
                                <td>@{{ item.company.position_name }}</td>
{{--                                <td>@{{ item.company.application }}</td>--}}

                                <td class="nowrap">
                                    <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <button
                                    @click="deleteItem(item.resource_url)"
                                    class="btn btn-sm btn-danger"
                                    title="{{ trans('brackets/admin-ui::admin.btn.delete') }}">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <div class="text-center" v-if="_.isEmpty(collection)">
                    <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                    <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                </div>
                <div v-if="pagination.state.total > 0">
                    <pagination></pagination>
                </div>
            </div>
        </div>
    </project-listing>

@endsection
