@can('crud.project.columns.name.write')
    <div class="form-group row align-items-center">
        <label for="name" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.project.columns.name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.name" class="form-control" id="name" name="name"
                   placeholder="{{ trans('admin.project.columns.name') }}">
            <div v-if="errors.has('name')" class="text-danger" v-cloak>@{{ errors.first('name') }}</div>
        </div>
    </div>
@endcan

@can('crud.project.columns.name.write')
    <div class="form-group row align-items-center">
        <label for="project_id" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.project.columns.project_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.project_id" class="form-control" id="project_id" name="project_id"
                   placeholder="{{ trans('admin.project.columns.project_id') }}">
            <div v-if="errors.has('project_id')" class="text-danger" v-cloak>@{{ errors.first('project_id') }}</div>
        </div>
    </div>
@endcan

@can('crud.company.columns.name.write')
    <div class="form-group row align-items-center">
        <label for="name" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.company.columns.name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.company.name" class="form-control" id="name" name="name"
                   placeholder="{{ trans('admin.company.columns.name') }}">
            <div v-if="errors.has('company_name')" class="text-danger" v-cloak>@{{ errors.first('company_name') }}</div>
        </div>
    </div>
@endcan



@can('crud.company.columns.city.write')
    <div class="form-group row align-items-center">
        <label for="city" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.company.columns.city') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.company.city" class="form-control" id="city" name="city"
                   placeholder="{{ trans('admin.company.columns.city') }}">
            <div v-if="errors.has('company_city')" class="text-danger" v-cloak>@{{ errors.first('company_city') }}</div>
        </div>
    </div>
@endcan

@can('crud.company.columns.state_id.write')
    <div class="form-group row align-items-center">
        <label for="state_id" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.company.columns.state_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <multiselect
                v-model="form.company.state_id"
                :options="{{$states->toJson()}}"
                :multiple="false"
                track-by="id"
                label="name"></multiselect>
            <div v-if="errors.has('company_state_id')" class="text-danger" v-cloak>@{{ errors.first('company_state_id')
                }}
            </div>
        </div>
    </div>
@endcan

@can('crud.company.columns.address.write')
    <div class="form-group row align-items-center">
        <label for="address" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.company.columns.address') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.company.address" class="form-control" id="address" name="address"
                   placeholder="{{ trans('admin.company.columns.address') }}">
            <div v-if="errors.has('company_address')" class="text-danger" v-cloak>@{{ errors.first('company_address')
                }}
            </div>
        </div>
    </div>
@endcan

@can('crud.company.columns.zip.write')
    <div class="form-group row align-items-center">
        <label for="zip" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.company.columns.zip') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <the-mask :masked="true" mask="XXXXX" type="text" v-model="form.company.zip" class="form-control" id="zip"
                      name="zip" placeholder="{{ trans('admin.company.columns.zip') }}"/>
        </div>
        <label for="zip" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'"></label>
        <label for="zip" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'"></label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <div v-if="errors.has('company_zip')" class="text-danger" v-cloak>@{{ errors.first('company_zip') }}</div>
        </div>
    </div>
@endcan

@can('crud.company.columns.phone.write')
    <div class="form-group row align-items-center">
        <label for="phone" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.company.columns.phone') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <the-mask :masked="true" :mask="'+' + {{env('PHONE_PREFIX')}} + ' (###) ###-####'" type="text"
                      v-model="form.company.phone" class="form-control" id="phone" name="phone"
                      placeholder="{{ trans('admin.company.columns.phone') }}"/>
        </div>
        <label for="phone" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'"></label>
        <label for="phone" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'"></label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <div v-if="errors.has('company_phone')" class="text-danger" v-cloak>@{{ errors.first('company_phone') }}
            </div>
        </div>
    </div>

    <div class="form-group row align-items-center">
        <label for="second_phone" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.company.columns.second_phone') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <the-mask :masked="true" :mask="'+' + {{env('PHONE_PREFIX')}} + ' (###) ###-####'" type="text"
                      v-model="form.company.second_phone" class="form-control" id="second_phone" name="second_phone"
                      placeholder="{{ trans('admin.company.columns.second_phone') }}"/>
        </div>
        <label for="second_phone" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'"></label>
        <label for="second_phone" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'"></label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <div v-if="errors.has('company_second_phone')" class="text-danger" v-cloak>@{{ errors.first('company_second_phone') }}
            </div>
        </div>
    </div>
@endcan

@can('crud.company.columns.fax.write')
    <div class="form-group row align-items-center">
        <label for="fax" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.company.columns.fax') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <the-mask :masked="true" :mask="'+' + {{env('PHONE_PREFIX')}} + ' (###) ###-####'" type="text"
                      v-model="form.company.fax" class="form-control" id="fax" name="fax"
                      placeholder="{{ trans('admin.company.columns.fax') }}"/>
            <div v-if="errors.has('company_fax')" class="text-danger" v-cloak>@{{ errors.first('company_fax') }}</div>
        </div>
    </div>
@endcan

@can('crud.company.columns.site.write')
    <div class="form-group row align-items-center">
        <label for="site" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.company.columns.site') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.company.site" class="form-control" id="site" name="site"
                   placeholder="{{ trans('admin.company.columns.site') }}">
            <div v-if="errors.has('company_site')" class="text-danger" v-cloak>@{{ errors.first('company_site') }}</div>
        </div>
    </div>
@endcan

@can('crud.project.columns.mail_domain.write')
    <div class="form-group row align-items-center">
        <label for="mail_domain" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.project.columns.mail_domain') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.mail_domain" class="form-control" id="mail_domain" name="mail_domain"
                   placeholder="{{ trans('admin.project.columns.mail_domain') }}">
            <div v-if="errors.has('mail_domain')" class="text-danger" v-cloak>@{{ errors.first('mail_domain') }}</div>
        </div>
    </div>
@endcan

@can('crud.company.columns.info_email.write')
    <div class="form-group row align-items-center">
        <label for="info_email" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.company.columns.info_email') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.company.info_email" class="form-control" id="info_email" name="info_email"
                   placeholder="{{ trans('admin.company.columns.info_email') }}">
            <div v-if="errors.has('company_info_email')" class="text-danger" v-cloak>@{{
                errors.first('company_info_email') }}
            </div>
        </div>
    </div>
@endcan

<div class="form-group row align-items-center" style="display: none">
    <label for="manager_id" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.company.columns.manager_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select multiple v-model="form.manager_ids" class="form-control" id="manager_id" name="manager_id">
            <option v-for="item in managers" :value="item.id">
                    @{{ _.get(item, 'name', '') }}
            </option>
        </select>
        <div v-if="errors.has('manager_ids')" class="text-danger" v-cloak>@{{ errors.first('manager_ids') }}</div>
    </div>
</div>

<div class="form-check row">
    <label for="position_name" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.manager_ids') }}</label>
    <div  v-for="(item, index) in managers" class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input  class="form-check-input" v-if="form.manager_ids.includes(item.id)" checked="checked"  type="checkbox" @click="togleManager(index)">
        <input  class="form-check-input" v-else  type="checkbox" @click="togleManager(index)">
        <label class="form-check-label" for="checkbox">
            @{{ _.get(item, 'name', '') }}
        </label>
    </div>
    <div v-if="errors.has('manager_ids')" class="text-danger" v-cloak>@{{ errors.first('manager_ids') }}</div>
</div>

<div v-if="form.manager_ids.length">
    <label class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">@lang('admin.project.columns.employee_pct')</label>
    <template v-for="id in form.manager_ids">
        <div class="form-group row align-items-center">
            <label class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">
                @{{ _.get(managers[id], 'name', '') }}
            </label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <input type="number" min="0" max="100" v-model="form.hr_percent[id]">
                <div v-if="errors.has('hr_percent_'+id)" class="text-danger" v-cloak>@{{ errors.first('hr_percent_'+id) }}
                </div>
            </div>
        </div>
    </template>
    <div class="row">
        <label class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'"></label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <div v-if="errors.has('hr_sum')" class="text-danger" v-cloak>@{{ errors.first('hr_sum') }}</div>
        </div>
    </div>
</div>

<div class="form-group row align-items-center" style="display: none">
    <label for="support_id" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.company.columns.support_id') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select multiple v-model="form.support_ids" class="form-control">
            <option v-for="item in supports" :value="item.id">
                @{{ _.get(item, 'name', '') }}
            </option>
        </select>
        <div v-if="errors.has('support_ids')" class="text-danger" v-cloak>@{{ errors.first('support_ids') }}</div>
    </div>
</div>

<div class="form-check row">
    <label for="position_name" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.support_ids') }}</label>
    <div  v-for="(item, index) in supports" class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input  class="form-check-input" v-if="form.support_ids.includes(item.id)" checked="checked" type="checkbox" @click="togleInvest(index)">
        <input  class="form-check-input" v-else  type="checkbox" @click="togleInvest(index)">
        <label class="form-check-label" for="checkbox">
            @{{ _.get(item, 'name', '') }}
        </label>
    </div>
    <div v-if="errors.has('support_ids')" class="text-danger" v-cloak>@{{ errors.first('support_ids') }}</div>
</div>

<template v-if="form.support_ids.length">
    <label class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">@lang('admin.project.columns.employee_pct')</label>
    <template v-for="id in form.support_ids">
        <div class="form-group row align-items-center">
            <label class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">
                @{{ _.get(supports[id], 'name', '') }}
            </label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <input type="number" min="0" max="100" v-model="form.support_percent[id]">
                <div v-if="errors.has('support_percent_'+id)" class="text-danger" v-cloak>@{{ errors.first('support_percent_'+id) }}
                </div>
            </div>
        </div>
    </template>
    <div class="row">
        <label class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'"></label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <div v-if="errors.has('support_sum')" class="text-danger" v-cloak>@{{ errors.first('support_sum') }}</div>
        </div>
    </div>
</template>


@can('crud.company.columns.position_name.write')
    <div class="form-group row align-items-center">
        <label for="position_name" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.company.columns.position_name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.company.position_name" class="form-control" id="position_name"
                   name="position_name" placeholder="{{ trans('admin.company.columns.position_name') }}">
            <div v-if="errors.has('company_position_name')" class="text-danger" v-cloak>@{{
                errors.first('company_position_name') }}
            </div>
        </div>
    </div>
@endcan

@can('crud.project.columns.salary.write')
    <div class="form-group row align-items-center">
        <label for="salary" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.company.columns.salary') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.salary" class="form-control" id="salary" name="salary"
                   placeholder="{{ trans('admin.company.columns.salary') }}">
            <div v-if="errors.has('salary')" class="text-danger" v-cloak>@{{ errors.first('salary') }}</div>
        </div>
    </div>
@endcan

@can('crud.project.columns.interview_forms.write')
    <div class="form-group row align-items-center">
        <label for="field_type_id" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">
            {{ trans('admin.project.columns.interview_forms') }}
        </label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <select v-model="form.form_ids" class="form-control" multiple>
                @foreach($forms as $form)
                    <option value="{{ $form->id }}">
                        {{ $form->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
@endcan
