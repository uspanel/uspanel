@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.project.actions.create'))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>@lang('admin.project.actions.create')</span></h2>
    </div>
    <project-form
        :states="{{ $states->toJson() }}"
        :action="'{{ url('admin/projects') }}'"
        :hr-staff="{{ $hrStaff->keyBy('id')->toJson() }}"
        :support-staff="{{ $supportStaff->keyBy('id')->toJson() }}"
        :hr-invest="{{ $hrInvest->keyBy('id')->toJson() }}"
        :support-invest="{{ $supportInvest->keyBy('id')->toJson() }}"
        inline-template>
        <div class="card">
            <div class="card-body">
                <form method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>
                    @include('admin.project.components.form-elements')
                    <div class="text-right">
                        <button @click="customsend" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </project-form>

@endsection
