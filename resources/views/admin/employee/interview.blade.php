@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.admin-user.actions.edit_profile'))

@section('body')

    <div class="container-xl">
        <employee-interview
        :user="{{ auth()->user() }}"
        redirect="{{ route('employee.documents') }}"
        inline-template>
            <div class="card">
                <div class="card-header">
                    @lang('admin.interview')
                </div>
                <div class="card-body">
                    {!! FormCreator::build($fields) !!}
                    <div class="text-right">
                        <button class="btn btn-primary" @click="saveInterview">
                            @lang('admin.save')
                        </button>
                    </div>
                </div>
            </div>
        </employee-interview>
    </div>
    
@endsection
