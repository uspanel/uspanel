@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.admin-user.actions.edit_profile'))

@section('body')

    <div class="container-xl">
        @if(auth()->user()->email_verified_at)
            @if(!$user->employee->interviewed)
                @if(auth()->user()->employee->userStatus->name == 'applicant')
                <div class="alert alert-success text-center">
                    @lang('admin.by_phone_result')
                </div>
                @else
                <employee-application inline-template>
                    <div class="card">
                        <div class="card-body">
                            <div v-if="this.sent" class="alert alert-success text-center">
                                @lang('admin.by_phone_result')
                            </div>
                            <template v-else>
                                <div class="alert alert-success text-center">
                                    @lang('admin.email_verification_success')
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-primary" @click="interviewByPhone">
                                        @lang('admin.by_phone')
                                    </button>
                                    <a href="{{ route('employee.interview') }}" class="btn btn-success">
                                        @lang('admin.online')
                                    </a>
                                </div>
                            </template>
                        </div>
                    </div>
                </employee-application>
                @endif
            @else
                @if(auth()->user()->employee->userStatus->name == 'wait_docs')
                    <employee-contract
                    inline-template
                    media-collection="contract">
                        <div class="card">
                            <div class="card-header">
                                @lang('admin.contract')
                            </div>
                            <div class="card-body">
                                <h5 class="mb-3 font-weight-bold">
                                    @lang('admin.employee_contract.download')
                                    <a href="https://www.irs.gov/pub/irs-pdf/fw4.pdf">
                                        @lang('admin.contract')
                                    </a>
                                    <div>
                                        @lang('admin.employee_contract.upload')
                                    </div>
                                </h5>

                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr v-for="item in media">
                                                <td>@{{ item.file_name }}</td>
                                                <td class="text-right">
                                                    <a role="button" class="btn btn-primary" :href="getDownloadLink(item.id)">
                                                        <i class="fa fa-download" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                @include('vendor.brackets.admin-ui.admin.includes.media-uploader', [
                                    'mediaCollection' => $user->getMediaCollection('contract'),
                                ])
                                <div class="text-right">
                                    <button class="btn btn-primary" @click="upload">
                                        @lang('admin.send')
                                    </button>
                                </div>
                            </div>
                        </div>
                    </employee-contract>
                    <employee-contract
                    inline-template
                    media-collection="agreement">
                        <div class="card">
                            <div class="card-header">
                                @lang('admin.agreement')
                            </div>
                            <div class="card-body">
                                <h5 class="mb-3 font-weight-bold">
                                    @lang('admin.employee_contract.download')
                                    <a target="_blank" href="/docs/agreement.pdf">
                                        @lang('admin.agreement')
                                    </a>
                                    <div>
                                        @lang('admin.employee_agreement.upload')
                                    </div>
                                </h5>

                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr v-for="item in media">
                                                <td>@{{ item.file_name }}</td>
                                                <td class="text-right">
                                                    <a role="button" class="btn btn-primary" :href="getDownloadLink(item.id)">
                                                        <i class="fa fa-download" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                @include('vendor.brackets.admin-ui.admin.includes.media-uploader', [
                                    'mediaCollection' => $user->getMediaCollection('agreement'),
                                ])
                                <div class="text-right">
                                    <button class="btn btn-primary" @click="upload">
                                        @lang('admin.send')
                                    </button>
                                </div>
                            </div>
                        </div>
                    </employee-contract>
                @elseif(auth()->user()->employee->userStatus->name == 'rejected')
                <div class="alert alert-danger text-center">
                    @lang('admin.employee.rejected')
                </div>
                @endif
            @endif
        @endif
        <div class="card">
            <div class="card-header">
                {{ trans('admin.documents') }}
            </div>
            <div class="card-body">
                <employee-email-verification email="{{ $user->email }}" inline-template>
                    <div>
                        @if (!$user->email_verified_at)
                            <div class="alert alert-danger d-flex justify-content-between align-items-center">
                                <span>@lang('admin.check_your_email_to_verification', [
                                    'supportEmail' => env('SUPPORT_EMAIL')
                                ])</span>
                                <button class="btn btn-primary mb-0" @click="$modal.show('resendVerificationEmail')">
                                    @lang('admin.resend')
                                </button>
                            </div>
                        @endif
                        <modal name="resendVerificationEmail">
                            <div class="modal-content d-flex h-100 flex-column">
                                <div class="modal-header">
                                    <div>
                                        @lang('admin.resend_verifying_email')
                                    </div>
                                    <button type="button" class="close" @click="$modal.hide('resendVerificationEmail')">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body h-100">
                                    <div class="form-group">
                                        <input type="email" v-model="form.email" class="form-control">
                                    </div>
                                    <div class="alert alert-info">
                                        @lang('admin.you_can_change_your_email')
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-primary" :class="{ disabled: loading }" @click="resend">
                                        @lang('admin.send')
                                    </button>
                                </div>
                            </div>
                        </modal>
                    </div>
                </employee-email-verification>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td width="30%" class="font-weight-bold">@lang('admin.admin-user.columns.first_name')</td>
                                <td>{{ $user->first_name }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">@lang('admin.admin-user.columns.last_name')</td>
                                <td>{{ $user->last_name }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">@lang('admin.admin-user.columns.email')</td>
                                <td>{{ $user->email }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">@lang('admin.admin-user.columns.phone_number')</td>
                                <td>{{ $user->phone }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">@lang('admin.address')</td>
                                <td>{{ $user->profile->address }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">@lang('admin.city')</td>
                                <td>{{ $user->profile->city }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">@lang('admin.state')</td>
                                <td>{{ $user->profile->state->name }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">@lang('admin.zip')</td>
                                <td>{{ $user->profile->zip }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">@lang('admin.higher_education')</td>
                                <td>{{ $user->profile->higher_education ? trans('admin.yes') : trans('admin.no') }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">@lang('admin.authorized_to_work')</td>
                                <td>{{ $user->profile->authorized_to_work ? trans('admin.yes') : trans('admin.no') }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">@lang('admin.adult')</td>
                                <td>{{ $user->profile->adult ? trans('admin.yes') : trans('admin.no') }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">@lang('admin.cv')</td>
                                <td>
                                    @if($user->getMedia('cv')->first())
                                        <a href="/profile/media/cv/{{ $user->getMedia('cv')->first()->id }}">
                                            {{ $user->getMedia('cv')->first()->file_name }}
                                        </a>
                                    @endif
                                </td>
                            </tr>
                            <tr class="@if ($user->approved) table-success @else table-danger @endif">
                                <td class="font-weight-bold">@lang('admin.approved')</td>
                                <td>{{ $user->approved ? trans('admin.yes') : trans('admin.no') }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

        @if ($user->email_verified_at && $user->employee->interviewed)
            <employee-interview
            :user="{{ auth()->user() }}"
            inline-template>
                <div class="card">
                    <div class="card-header">
                        @lang('admin.interview')
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr v-for="item in formFields">
                                        <td width="30%" class="font-weight-bold">@{{ item.title }}</td>
                                        <td>@{{ item.value | format }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </employee-interview>

        @endif
    </div>

@endsection
