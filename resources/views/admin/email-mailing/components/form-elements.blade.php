@include('admin.layout.inputs.text', [
    'name' => 'name'
])

@include('admin.layout.inputs.text', [
    'name' => 'email'
])

@include('admin.layout.inputs.select', [
    'name' => 'email_group_id',
    'arrayName' => 'emailGroups',
])
