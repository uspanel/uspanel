@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.email-mailing.actions.edit', ['name' => $emailMailing->name]))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0">
            <span>
                {{ trans('admin.email-mailing.actions.edit', ['name' => $emailMailing->name]) }}
            </span>
        </h2>
    </div>
    <email-mailing-form
        :action="'{{ $emailMailing->resource_url }}'"
        :data="{{ $emailMailing->toJson() }}"
        :email-groups="{{ $emailGroups->toJson() }}"
        inline-template>
            <div class="card">
            <div class="card-body">
                <form method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>
                    @include('admin.email-mailing.components.form-elements')
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        
</email-mailing-form>

@endsection
