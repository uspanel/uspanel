@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.chat.actions.index'))

@section('body')

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body" v-cloak>
                    <chat @if ($chatroom) :chatroom-id="{{ $chatroom->id }}" @endif></chat>
                </div>
            </div>
        </div>
    </div>

@endsection