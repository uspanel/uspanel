@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.warehousable.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0">
                <span>{{ trans('admin.warehousable.actions.index') }}</span>
            </h2>
        </div>
        <div class="col-auto">
            
        </div>
    </div>
    <warehousable-listing
        :data="{{ $data->toJson() }}"
        :url="'/admin/warehouses/{{ $warehouse->id }}/items'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <form @submit.prevent="">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">{{ trans('admin.item.columns.name') }}</label>
                                        <input class="form-control" v-model="search" />
                                    </div>
                                </div>
                                <div class="col-auto align-self-end">
                                    <button type="button" class="btn btn-primary mb-3" @click="filter('search', search)">
                                        <i class="fa fa-search"></i>
                                        {{ trans('brackets/admin-ui::admin.btn.search') }}
                                    </button>
                                </div>
                            </div>
                            <div class="row justify-content-end">
                                <div class="col-auto form-group">
                                    <select class="form-control" v-model="pagination.state.per_page">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th is='sortable' :column="'id'">{{ trans('admin.item.columns.id') }}</th>
                                        <th is='sortable' :column="'name'">{{ trans('admin.item.columns.name') }}</th>
                                        <th is='sortable' :column="'size'">{{ trans('admin.item.columns.size') }}</th>
                                        <th is='sortable' :column="'color'">{{ trans('admin.item.columns.color') }}</th>
                                        <th is='sortable' :column="'link'">{{ trans('admin.item.columns.link') }}</th>
                                        <th>{{ trans('admin.item.columns.qty') }}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection">
                                        <td>@{{ item.id }}</td>
                                        <td>@{{ item.name }}</td>
                                        <td>@{{ item.size }}</td>
                                        <td>@{{ item.color }}</td>
                                        <td>@{{ item.link }}</td>
                                        <td>@{{ item.pivot.qty }}</td>
                                        <td>
                                            <a v-if="item.pivot.qty > 0" :href="'/admin/sales-tasks/create?warehouse_id={{ $warehouse->id }}&item_id=' + item.id" class="btn btn-primary btn-sm" data-toggle="tooltip" title="@lang('admin.warehousable.create_sales_task')">
                                                <i class="fas fa-plus"></i>
                                            </a>
                                            <a :href="'/admin/warehouses/{{ $warehouse->id }}/items/' + item.id" class="btn btn-info btn-sm" data-toggle="tooltip" title="@lang('admin.warehousable.item_history')">
                                                <i class="fas fa-info-circle"></i>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center" v-if="!collection.length > 0">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </warehousable-listing>

@endsection
