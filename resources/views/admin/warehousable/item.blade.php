@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.warehousable.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0">
                <span>{{ trans('admin.warehousable.actions.item') }}</span>
            </h2>
        </div>
        <div class="col-auto">
            
        </div>
    </div>
    <warehousable-item-listing
        :data="{{ $data->toJson() }}"
        :url="'/admin/warehouses/{{ $warehouse->id }}/items/{{ $item->id }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <form @submit.prevent="">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">{{ trans('admin.item.columns.qty') }}</label>
                                        <input type="text" class="form-control" v-model="filters.pivot_qty" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">{{ trans('admin.item.columns.created_at') }}</label>
                                        <input type="date" class="form-control" v-model="filters.pivot_created_at" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">{{ trans('admin.item.columns.warehousable_type') }}</label>
                                        <select class="form-control" v-model="filters.pivot_warehousable_type">
                                            <option value=""></option>
                                            <option value="App\Task">Task</option>
                                            <option value="App\SalesTask">Sales Task</option>
                                            <option value="App\Package">Package</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-auto align-self-end">
                                    <button type="button" class="btn btn-primary mb-3" @click="filter('search', search)">
                                        <i class="fa fa-search"></i>
                                        {{ trans('brackets/admin-ui::admin.btn.search') }}
                                    </button>
                                </div>
                            </div>
                            <div class="row justify-content-end">
                                <div class="col-auto form-group">
                                    <select class="form-control" v-model="pagination.state.per_page">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th>{{ trans('admin.item.columns.id') }}</th>
                                        <th>{{ trans('admin.item.columns.name') }}</th>
                                        <th is='sortable' :column="'pivot_qty'">{{ trans('admin.item.columns.qty') }}</th>
                                        <th is='sortable' :column="'pivot_warehousable_type'">{{ trans('admin.item.columns.warehousable_type') }}</th>
                                        <th is='sortable' :column="'pivot_created_at'">{{ trans('admin.item.columns.created_at') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection">
                                        <td>@{{ item.pivot.id }}</td>
                                        <td>@{{ item.name }}</td>
                                        <td>@{{ item.pivot.qty }}</td>
                                        <td>
                                            <a :href="item.pivot.warehousable_type|typeLink(item.pivot.warehousable_id)">
                                                @{{ item.pivot.warehousable_type|type }} №@{{ item.pivot.warehousable_id }}
                                            </a>
                                        </td>
                                        <td>@{{ item.pivot.created_at }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center" v-if="!collection.length > 0">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </warehousable-item-listing>

@endsection
