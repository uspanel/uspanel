@can('crud.' . $viewName . '.columns.' . $name . '.read')
    <td>
        <template v-if="_.findIndex(item.media, {collection_name:'{{$name}}'}) >= 0">
            <media-download
                :media="_.find(item.media, {collection_name:'{{$name}}'})"
                inline-template>
                <a
                    class="btn btn-primary btn-sm"
                    :href="'/media/' + media.id + '/' + media.file_name"
                    target="_blank"
                    download>
                    <i class="fas fa-download"></i>
                    {{-- @{{media.custom_properties.name}} --}}
                </a>
            </media-download>
        </template>
        <template v-else>
            @if(Auth::user()->hasPermissionTo('crud.' . $viewName . '.columns.' . $name . '.write'))
                <a class="btn btn-info btn-sm" href="#" @click="currentIndex = index; $modal.show('{{$name}}_upload')">
                    <i class="fas fa-cloud-upload-alt"></i>
                    {{-- @lang('admin.upload') @lang('admin.' . $viewName . '.columns.' . $name) --}}
                </a>
            @else
                -
            @endif
        </template>
    </td>
@endcan
