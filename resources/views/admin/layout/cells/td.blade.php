@can('crud.' . $viewName . '.columns.' . $name . '.read')
    <td>@{{ item.{!! explode('.', $field ?? $name)[0] ?? '' !!} ? item.{!! $field ?? $name !!} : '-' }}</td>
@endcan