@can('crud.' . $viewName . '.columns.' . $name . '.read')
    <th class="nowrap" @if(!isset($notSortable) || !$notSortable)is='sortable' :column="'{{$name}}'"@endif>{{ trans('admin.' . $viewName . '.columns.' . $name) }}</th>
@endcan