@can('crud.' . $viewName . '.columns.' . $name . '.read')
    <td>
        {{-- //TODO make tooltip in new theme --}}
        <span v-for="el in {{ $elements }}" class="badge badge-success mb-1">@{{el.name}}</span>
    </td>
@endcan
