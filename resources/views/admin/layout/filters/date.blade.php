@can('crud.' . $viewName . '.columns.' . $name . '.read')
<div class="col-md-3">
    <div class="form-group">
        <label for="">@lang('admin.' . $viewName . '.columns.' . $name)</label>
        <input v-model="filters.{{$name}}" type="date" placeholder="@lang('admin.' . $viewName . '.columns.' . $name)" class="form-control">
    </div>
</div>
@endcan