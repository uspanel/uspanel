@can('crud.' . $viewName . '.columns.' . $name . '.read')
<div class="col-md-3">
    <div class="form-group">
        <label for="">{{ trans('admin.' . $viewName . '.columns.' . $name) }}</label>
        <select v-model="filters.{{$name}}" class="form-control" id="{{$name}}">
            <option value="">@lang('admin.' . $viewName . '.columns.' . $name)</option>
            <template v-for="(name, id) in {{$arrayName}}">
                <option :key="id" :value="id">@{{name}}</option>
            </template>
        </select>
    </div>
</div>
@endcan