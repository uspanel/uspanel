@can('crud.' . $viewName . '.columns.' . $name . '.write')
<div class="form-group row align-items-center" :class="{'has-danger': errors.has('{{$name}}'), 'has-success': this.fields.{{$name}} && this.fields.{{$name}}.valid }">
    <label for="{{$name}}" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.' . $viewName . '.columns.' . $name) }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div class="input-group input-group--custom">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            <datetime v-model="form.{{$name}}" :config="datetimePickerConfig" v-validate="'date_format:yyyy-MM-dd HH:mm:ss'" class="flatpickr" :class="{'form-control-danger': errors.has('{{$name}}'), 'form-control-success': this.fields.{{$name}} && this.fields.{{$name}}.valid}" id="{{$name}}" name="{{$name}}" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_date_and_time') }}"></datetime>
        </div>
        <div v-if="errors.has('{{$name}}')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('{!!$name!!}') }}</div>
    </div>
</div>
@endcan