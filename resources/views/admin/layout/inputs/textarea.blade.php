@can('crud.' . $viewName . '.columns.' . $name . '.write')
    <div class="form-group row align-items-center">
        <label for="{{$name}}" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.' . $viewName . '.columns.' . $name) }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <textarea type="text" v-model="form.{{$name}}" class="form-control" id="{{$name}}" name="{{$name}}" placeholder="{{ trans('admin.' . $viewName . '.columns.' . $name) }}"></textarea>
            <div v-if="errors.has('{{$name}}')" class="text-danger" v-cloak>@{{ errors.first('{!!$name!!}') }}</div>
        </div>
    </div>
@endcan