@can('crud.' . $viewName . '.columns.' . $name . '.write')
    <div class="form-group row align-items-center">
        <label for="{{$name}}" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.' . $viewName . '.columns.' . $name) }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select v-model="form.{{$name}}" class="form-control" id="{{$name}}" name="{{$name}}">
                <option value=""></option>
                <template v-for="(name, id) in {{$arrayName}}">
                    <option :key="id" :value="id">@{{name}}</option>
                </template>
            </select>
            <div v-if="errors.has('{{$name}}')" class="text-danger" v-cloak>@{{ errors.first('{!!$name!!}') }}</div>
        </div>
    </div>
@endcan