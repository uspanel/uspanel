@can('crud.' . $viewName . '.columns.' . $name . '.write')
    <div class="form-group row align-items-center">
        <label for="{{$name}}" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.' . $viewName . '.columns.' . $name) }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <multiselect v-model="form.{{$name}}" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_options') }}" label="name" track-by="id" :options="{{ $options->toJson() }}" :multiple="true" open-direction="bottom"></multiselect>
            <div v-if="errors.has('{{$name}}')" class="text-danger" v-cloak>@{{ errors.first('{!! $name !!}') }}</div>
        </div>
    </div>
@endcan