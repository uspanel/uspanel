@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.history-mailing.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0">
                <span>{{ trans('admin.history-mailing.actions.index') }}</span>
            </h2>
        </div>
    </div>
    <history-mailing-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('/admin/mailings/' . $mailing->id . '/history-mailings') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <form @submit.prevent="">
                            <div class="row justify-content-md-between align-items-end">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.history-mailing.columns.email_mailing_id')</label>
                                        <input type="text" class="form-control" v-model="filters.email_mailing_email">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.history-mailing.columns.created_at')</label>
                                        <select class="form-control" v-model="filters.mailing_sending_scheduled_at">
                                            <option :value="null">@lang('admin.all')</option>
                                            @foreach ($mailingSendings as $value)
                                                <option value="{{ $value->scheduled_at }}">{{ $value->scheduled_at }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">@lang('admin.history-mailing.columns.mailing_status_id')</label>
                                        <select class="form-control" name="" v-model="filters.mailing_status_id">
                                            <option :value="null">@lang('admin.all')</option>
                                            @foreach ($mailingStatuses as $value)
                                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <button type="button" class="btn btn-primary mb-3" @click="filter('search', search)">
                                        <i class="fa fa-search"></i>
                                        {{ trans('brackets/admin-ui::admin.btn.search') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <span v-for="count,index in {{ json_encode($mailing->stats) }}" class="badge badge-pill mb-1" :class="chooseBg(index)">@{{index}} <span class="badge badge-pill badge-light mb-0">@{{ count }}</span> </span>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th is='sortable' :column="'mailing_id'">{{ trans('admin.history-mailing.columns.mailing_id') }}</th>
                                        <th is='sortable' :column="'email_mailing_id'">{{ trans('admin.history-mailing.columns.email_mailing_id') }}</th>
                                        <th is='sortable' :column="'mailing_status_id'">{{ trans('admin.history-mailing.columns.mailing_status_id') }}</th>
                                        <th is='sortable' :column="'created_at'">{{ trans('admin.history-mailing.columns.created_at') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection">
                                        <td>@{{ item.mailing.name }}</td>
                                        <td>
                                            <div v-if="item.email_mailing.user">
                                                <a :href="'/admin/users/' + item.email_mailing.user.id">@{{ item.email_mailing.user.name }}</a>
                                            </div>
                                            @{{ item.email_mailing.email }}
                                        </td>
                                        <td>@{{ item.mailing_status.name }}</td>
                                        <td>@{{ item.mailing_sending.scheduled_at }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center" v-if="!collection.length > 0">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </history-mailing-listing>

@endsection
