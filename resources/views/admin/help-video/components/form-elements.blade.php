<div class="row text-center">
    <div class="col-12">
        <div class="avatar-upload">
            @include('brackets/admin-ui::admin.includes.avatar-uploader', [
                'mediaCollection' => app(\App\HelpVideo::class)->getMediaCollection('video'),
                'media' => $helpVideo->getThumbs300ForCollection('video')
            ])
        </div>
        <div v-if="errors.has('video')" class="text-danger" v-cloak>@{{ errors.first('video') }}</div>
    </div>
</div>
<div class="form-group row align-items-center">
    <label for="title" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.help-video.columns.title') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.title" class="form-control" id="title" name="title" placeholder="{{ trans('admin.help-video.columns.title') }}">
        <div v-if="errors.has('title')" class="text-danger" v-cloak>@{{ errors.first('title') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="description" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.help-video.columns.description') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <wysiwyg v-model="form.description" v-validate="''" id="description" name="description" :config="mediaWysiwygConfig"></wysiwyg>
        </div>
        <div v-if="errors.has('description')" class="text-danger" v-cloak>@{{ errors.first('description') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="roles" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.admin-user.columns.roles') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect v-model="form.roles" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_options') }}" label="name" track-by="id" :options="{{ $roles->toJson() }}" :multiple="true" open-direction="bottom"></multiselect>
        <div v-if="errors.has('roles')" class="text-danger" v-cloak>@{{ errors.first('roles') }}</div>
    </div>
</div>


