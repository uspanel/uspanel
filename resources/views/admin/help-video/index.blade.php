@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.help-video.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0"><span>@lang('admin.help-video.actions.index')</span></h2>
        </div>
        <div class="col-auto">
            <a class="btn btn-primary" href="{{ url('admin/help-videos/create') }}" role="button">
                <i class="fa fa-plus"></i>
                @lang('admin.help-video.actions.create')
            </a>
        </div>
    </div>
    <help-video-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/help-videos') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <form @submit.prevent="">
                            <div class="row justify-content-md-between">
                                <div class="col-md-3">
                                    <label for="">{{ trans('brackets/admin-ui::admin.placeholder.search') }}</label>
                                    <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" />
                                    
                                </div>
                                @can('crud.help_video.modify')
                                    <div class="col-md-3">
                                        <label for="roles">{{ trans('admin.admin-user.columns.roles') }}</label>
                                        <multiselect v-model="searchroles" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_options') }}" label="name" track-by="id" :options="{{ $roles->toJson() }}" :multiple="true" open-direction="bottom"></multiselect>
                                    </div>
                                @endcan
                                <div class="col-auto align-self-end">
                                    <button type="button" class="btn btn-primary mb-3" @click="filter('search', search)">
                                        <i class="fa fa-search"></i>
                                        {{ trans('brackets/admin-ui::admin.btn.search') }}
                                    </button>
                                </div>
                                
                            </div>
                            <div class="row justify-content-end">
                                <div class="col-auto form-group">
                                    <select class="form-control" v-model="pagination.state.per_page">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </form>

                        <div class="mb-4">
                            <div class="row">
                                <div class="col-3 mb-4 d-flex align-items-stretch" v-for="(item, index) in collection">
                                    <div class="card w-100">
                                        <div class="card-body">
                                            <div class="card mb-2">
                                                <div class="card-body pointer d-flex justify-content-center align-items-center" @click="showVideoModal(item)">
                                                    <div class="embed-responsive embed-responsive-16by9">
                                                        <video class="embed-responsive-item" v-if="!item.processing">
                                                           <source :src="item.webm_url" type="video/webm">
                                                           <source :src="item.mp4_url" type="video/mp4">
                                                           <p>@lang('admin.help-video.errors.bad_browser')</p>
                                                        </video>
                                                        <p v-else >@lang('admin.help-video.errors.wait_for_processing')</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-5">
                                                <p>@{{ item.title }}</p>
                                            </div>
                                            @can('crud.help_video.modify')
                                                <a role="button" class="btn btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                                <button
                                                @click="deleteItem(item.resource_url)"
                                                class="btn btn-sm btn-danger"
                                                title="{{ trans('brackets/admin-ui::admin.btn.delete') }}">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            @endcan
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="text-center" v-if="_.isEmpty(collection)">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
            </div>
            <b-modal ref="videoModal" :title="selectedItem.title" :size="'xl'" v-cloak :busy="true">
                <div slot="modal-footer"></div>
                <div class="container-fluid">
                    <div class="text-center mb-5">
                        <div class="embed-responsive embed-responsive-16by9">
                            <video v-if="selectedItem.video" controls class="embed-responsive-item">
                                <source :src="selectedItem.webm_url" >
                                <source :src="selectedItem.mp4_url" >
                                <p>@lang('admin.help-video.errors.bad_browser')</p>
                            </video>
                        </div>
                    </div>
                    <p v-html="selectedItem.description"></p>
                </div>
            </b-modal>
        </div>
    </help-video-listing>

@endsection
