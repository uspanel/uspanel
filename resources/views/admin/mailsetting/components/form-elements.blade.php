<button type="button" name="button" class="btn btn-primary" @click="addMailing">
      {{ trans('admin.mailsetting.addMailing') }}
</button>

<div v-show="form.meiling"  class="modal-content">
   <div class="modal-header">
       @lang('admin.upload_file')
   </div>
   <div class="modal-body">
       @include('brackets/admin-ui::admin.includes.media-uploader', [
           'mediaCollection' => app(App\Mailsetting::class)->getMediaCollection('csv'),
           'label' => trans('admin.task.columns.invoice'),
       ])
   </div>
   <div class="modal-footer" >
       <button  type="button" class="btn btn-primary" @click="getParseCSV">
           @lang('admin.send_csv')
       </button>

   </div>
    <div v-if="errors.has('names')" class="text-danger" v-cloak>@{{ errors.first('names') }}</div>
</div>



<div  v-if="form.names && form.meiling"  class="form-group row align-items-center" :class="{'has-danger': errors.has('names'), 'has-success': fields.names && fields.names.valid }">
    <label for="description" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.first_domain') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
         <p  v-for="(item, index) in form.names">
             <span style="font-weight: bold">@{{ index + 1 }}. From name:</span> @{{ item.name }}
             <span style="font-weight: bold">First Part of domain:</span> @{{ item.name_domain }}
             <br/>
         </p>
    </div>
</div>

<div v-show="form.meiling" class="form-group row align-items-center">
    <label for="domains" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.second_domain') }}<i class="fa fa-question-circle" title="{{ trans('admin.mailsetting.columns.second_domain_title') }}" aria-hidden="true"></i></label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.domains"   class="form-control" id="domains" name="domains" placeholder="{{ trans('admin.mailsetting.columns.second_domain') }}">
        <div v-if="errors.has('domains')" class="text-danger" v-cloak>@{{ errors.first('domains') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="smtp_host" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.smtp_host') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.smtp_host" class="form-control" id="smtp_host" name="smtp_host" placeholder="{{ trans('admin.mailsetting.columns.smtp_host') }}">
        <div v-if="errors.has('smtp_host')" class="text-danger" v-cloak>@{{ errors.first('smtp_host') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="smtp_port" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.smtp_port') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.smtp_port" class="form-control" id="smtp_port" name="smtp_port" placeholder="{{ trans('admin.mailsetting.columns.smtp_port') }}">
        <div v-if="errors.has('smtp_port')" class="text-danger" v-cloak>@{{ errors.first('smtp_port') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="smtp_encryption" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.smtp_encryption') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select
            class="form-control"
            id="smtp_encryption"
            name="smtp_encryption"
            v-model="form.smtp_encryption">
            <option value="ssl">SSL</option>
            <option value="tls">TLS</option>
            <option :value="null">None</option>
        </select>
        <div v-if="errors.has('smtp_encryption')" class="text-danger" v-cloak>@{{ errors.first('smtp_encryption') }}</div>
    </div>
</div>

<hr class="my-5">

<div class="form-group row align-items-center" >
    <label for="imap_host" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.imap_host') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.imap_host" class="form-control" id="imap_host" name="imap_host" placeholder="{{ trans('admin.mailsetting.columns.imap_host') }}">
        <div v-if="errors.has('imap_host')" class="text-danger" v-cloak>@{{ errors.first('imap_host') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="imap_port" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.imap_port') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.imap_port" class="form-control" id="imap_port" name="imap_port" placeholder="{{ trans('admin.mailsetting.columns.imap_port') }}">
        <div v-if="errors.has('imap_port')" class="text-danger" v-cloak>@{{ errors.first('imap_port') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="imap_encryption" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.imap_encryption') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select
            class="form-control"
            id="imap_encryption"
            name="imap_encryption"
            v-model="form.imap_encryption">
            <option value="ssl">SSL</option>
            <option value="tls">TLS</option>
            <option :value="null">None</option>
        </select>
        <div v-if="errors.has('imap_encryption')" class="text-danger" v-cloak>@{{ errors.first('imap_encryption') }}</div>
    </div>
</div>

<hr class="my-5">

<div class="form-group row align-items-center">
    <label for="login" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.login') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.login" class="form-control" id="login" name="login" placeholder="{{ trans('admin.mailsetting.columns.login') }}">
        <div v-if="errors.has('login')" class="text-danger" v-cloak>@{{ errors.first('login') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="password" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.password') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="password" v-model="form.password" class="form-control" id="password" name="password" placeholder="{{ trans('admin.mailsetting.columns.password') }}" ref="password">
        <div v-if="errors.has('password')" class="text-danger" v-cloak>@{{ errors.first('password') }}</div>
    </div>
</div>

<hr class="my-5">


<div v-show="!form.meiling" class="form-group row align-items-center">
    <label class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'"></label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div class="">
            <button type="button" name="button" class="btn btn-primary" @click="getFolders">
                <i v-if="loading" class="fa fa-spinner"></i>
                {{ trans('admin.mailsetting.get_folders') }}
            </button>
        </div>
    </div>
</div>

<div v-show="!form.meiling" class="form-group row align-items-center">
    <label for="imap_inbox_folder" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.imap_inbox_folder') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select
            id="imap_inbox_folder"
            name="imap_inbox_folder"
            class="form-control"
            v-model="form.imap_inbox_folder">
            <option :value="item.attributes.name" v-for="item in folders">
                @{{ item.attributes.name }}
            </option>
        </select>
        <div v-if="errors.has('imap_inbox_folder')" class="text-danger" v-cloak>@{{ errors.first('imap_inbox_folder') }}</div>
    </div>
</div>

<div v-show="!form.meiling" class="form-group row align-items-center">
    <label for="imap_sent_folder" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.imap_sent_folder') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select
            id="imap_sent_folder"
            name="imap_sent_folder"
            class="form-control"
            v-model="form.imap_sent_folder">
            <option :value="item.attributes.name" v-for="item in folders">
                @{{ item.attributes.name }}
            </option>
        </select>
        <div v-if="errors.has('imap_sent_folder')" class="text-danger" v-cloak>@{{ errors.first('imap_sent_folder') }}</div>
    </div>
</div>

<hr class="my-5">

<div class="form-check row">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="default" type="checkbox" v-model="form.default" name="default_fake_element">
        <label class="form-check-label" for="default">
            {{ trans('admin.mailsetting.columns.default') }}
        </label>
        <input type="hidden" name="default" :value="form.default">
        <div v-if="errors.has('default')" class="text-danger" v-cloak>@{{ errors.first('default') }}</div>
    </div>
</div>

<div class="form-check row">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="validate_cert" type="checkbox" name="default_fake_element">
        <label class="form-check-label" for="validate_cert">
            {{ trans('admin.mailsetting.columns.validate_cert') }}
        </label>
        <input type="hidden" name="validate_cert" :value="form.validate_cert">
        <div v-if="errors.has('validate_cert')" class="text-danger" v-cloak>@{{ errors.first('validate_cert') }}</div>
    </div>
</div>
