@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.mailsetting.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0"><span>@lang('admin.mailsetting.actions.index')</span></h2>
        </div>
        <div class="col-auto">
            <a class="btn btn-primary" href="{{ url('admin/mailsettings/create') }}" role="button">
                <i class="fa fa-plus"></i>
                @lang('admin.mailsetting.actions.create')
            </a>
        </div>
    </div>
    <mailsetting-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/mailsettings') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <form @submit.prevent="">
                            <div class="row justify-content-md-between">
                                <div class="col-md-3 form-group">
                                    <label for="">{{ trans('brackets/admin-ui::admin.placeholder.search') }}</label>
                                    <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" />
                                </div>

                                <div class="col-auto align-self-end">
                                    <button type="button" class="btn btn-primary mb-3" @click="filter('search', search)">
                                        <i class="fa fa-search"></i>
                                        {{ trans('brackets/admin-ui::admin.btn.search') }}
                                    </button>
                                </div>

                            </div>
                            <div class="row justify-content-end">
                                <div class="col-auto form-group">
                                    <select class="form-control" v-model="pagination.state.per_page">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th is='sortable' :column="'smtp_host'" class="nowrap">{{ trans('admin.mailsetting.columns.smtp_host') }}</th>
                                        <th is='sortable' :column="'smtp_port'" class="nowrap">{{ trans('admin.mailsetting.columns.smtp_port') }}</th>
                                        <th is='sortable' :column="'smtp_encryption'" class="nowrap">{{ trans('admin.mailsetting.columns.smtp_encryption') }}</th>
                                        <th is='sortable' :column="'imap_host'" class="nowrap">{{ trans('admin.mailsetting.columns.imap_host') }}</th>
                                        <th is='sortable' :column="'imap_port'" class="nowrap">{{ trans('admin.mailsetting.columns.imap_port') }}</th>
                                        <th is='sortable' :column="'imap_encryption'" class="nowrap">{{ trans('admin.mailsetting.columns.imap_encryption') }}</th>
                                        <th is='sortable' :column="'imap_sent_folder'" class="nowrap">{{ trans('admin.mailsetting.columns.imap_sent_folder') }}</th>
                                        <th is='sortable' :column="'imap_inbox_folder'" class="nowrap">{{ trans('admin.mailsetting.columns.imap_inbox_folder') }}</th>
                                        <th is='sortable' :column="'login'" class="nowrap">{{ trans('admin.mailsetting.columns.login') }}</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection">
                                        <td>@{{ item.smtp_host }}</td>
                                        <td>@{{ item.smtp_port }}</td>
                                        <td>@{{ item.smtp_encryption }}</td>
                                        <td>@{{ item.imap_host }}</td>
                                        <td>@{{ item.imap_port }}</td>
                                        <td>@{{ item.imap_encryption }}</td>
                                        <td>@{{ item.imap_sent_folder }}</td>
                                        <td>@{{ item.imap_inbox_folder }}</td>
                                        <td>@{{ item.login }}</td>

                                        <td class="nowrap">
                                            <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                            <button
                                            @click="deleteItem(item.resource_url)"
                                            class="btn btn-sm btn-danger"
                                            title="{{ trans('brackets/admin-ui::admin.btn.delete') }}">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                        <div class="text-center" v-if="_.isEmpty(collection)">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </mailsetting-listing>

@endsection
