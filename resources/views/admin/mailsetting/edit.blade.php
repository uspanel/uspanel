@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.mailsetting.actions.edit', ['name' => $mailsetting->id]))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>@lang('admin.mailsetting.actions.edit', [
            'name' => $mailsetting->name
        ])</span></h2>
    </div>
    <mailsetting-form
        :action="'{{ $mailsetting->resource_url }}'"
        :data="{{ $mailsetting->toJson() }}"
        inline-template>
        <div class="card">
            <div class="card-body">
                <form method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>
                    @include('admin.mailsetting.components.form-elements')
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </mailsetting-form>
@endsection