@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.user.actions.edit', ['name' => $user->name]))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>@lang('admin.user.actions.edit', [
            'name' => $user->name
        ])</span></h2>
    </div>
    <user-form
        :states="{{ $states->toJson() }}"
        :action="'{{ $user->resource_url }}'"
        :data="{{ $user->toJson() }}"
        :employees_staff="{{ $employees_staff->toJson() }}"
        :employees_invest="{{ $employees_invest->toJson() }}"
        :staffers="{{ $staffers }}"
        :extra="{{ json_encode($extra) }}"
        :investors="{{ $investors->toJson() }}"
        :contragents="{{ $contragents->toJson() }}"
        :investor_lists="{{ $investor_lists->toJson() }}"
        :contragent_lists="{{ $contragent_lists->toJson() }}"

        :hr_manager_staff_list="{{ $hr_manager_staff_list->toJson() }}"
        :hr_manager_invest_list="{{ $hr_manager_invest_list->toJson() }}"
        :support_staff_list="{{ $support_staff_list->toJson() }}"
        :support_invest_list="{{ $support_invest_list->toJson() }}"

        inline-template>
        <div>
            <div class="card">
                <div class="card-body">
                    <form method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>
                        @include('admin.user.components.form-elements')
                    </form>
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary" :disabled="submiting" @click="onSubmit">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                        @if ($user->hasRole('Employee Staff'))
                            <div class="mt-3">
                                <button class="btn btn-danger" @click="toBlacklist" :disabled="submiting">
                                    <i class="fa fa-ban" aria-hidden="true"></i>
                                    @lang('admin.to_blacklist')
                                </button>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            @if($user->hasRole('Employee Staff'))
                <employee-interview
                    ref="employeeInterview"
                    :user="{{ $user }}"
                    inline-template>
                    <div class="card">
                        <div class="card-body">
                            {!! FormCreator::build($fields) !!}
                        </div>
                        <div class="card-footer text-right">
                            <button class="btn btn-primary" @click="saveInterview">
                                @lang('admin.save')
                            </button>
                        </div>
                    </div>
                </employee-interview>
            @endif
            <div class="card">
                <div class="card-header">
                    @lang('admin.contract')
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            <tr v-for="item in contract">
                                <td>@{{ item.file_name }}</td>
                                <td class="text-right">
                                    <a role="button" class="btn btn-primary"
                                       :href="getDownloadLink('contract',item.id)">
                                        <i class="fa fa-download" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    @lang('admin.agreement')
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            <tr v-for="item in agreement">
                                <td>@{{ item.file_name }}</td>
                                <td class="text-right">
                                    <a role="button" class="btn btn-primary"
                                       :href="getDownloadLink('agreement',item.id)">
                                        <i class="fa fa-download" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </user-form>
@endsection
