@extends('brackets/admin-ui::admin.layout.default')

@section('title', empty($needCalls) ? trans('admin.user.actions.index') : trans('admin.needcalls.title'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0">
                <span>{{ empty($needCalls) ? trans('admin.user.actions.index') : trans('admin.needcalls.title') }}</span>
            </h2>
        </div>
        <div class="col-auto">
            @can('crud.user.create')
                <a class="btn btn-primary" href="{{ url('admin/users/create') }}" role="button">
                    <i class="fa fa-plus"></i>
                    @lang('admin.user.actions.create')
                </a>
            @endcan
        </div>
    </div>
    <user-listing
        :data="{{ $data->toJson() }}"
        :investor_lists="{{ $investor_lists->toJson() }}"
        :investors="{{ $investors->toJson() }}"
        :contragent_lists="{{ $contragent_lists->toJson() }}"
        :contragents="{{ $contragents->toJson() }}"
        :invest_tasks="{{ $invest_tasks->toJson() }}"
        @if($role)
        url="{{ route('admin.role.user', ['role' => $role->id]) }}"
        @else
        :url="'{{ empty($needCalls) ? url('admin/users') : url('admin/users/needcalls') }}'"
        @endif
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <div class="row align-items-end">
                            <div class="col">
                                <div class="row">
                                    @include('admin.layout.filters.text', [
                                        'viewName' => 'user',
                                        'name' => 'id',
                                    ])
                                    @can('crud.user.columns.first_name.read')
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">@lang('admin.user.columns.name')</label>
                                            <input v-model="filters.first_name" type="text" placeholder="@lang('admin.user.columns.name')" class="form-control">
                                        </div>
                                    </div>
                                    @endcan
                                    @include('admin.layout.filters.text', [
                                        'viewName' => 'user',
                                        'name' => 'address',
                                    ])
                                    @include('admin.layout.filters.text', [
                                        'viewName' => 'user',
                                        'name' => 'email',
                                    ])
                                    @include('admin.layout.filters.text', [
                                        'viewName' => 'user',
                                        'name' => 'phone_number',
                                    ])
                                    @include('admin.layout.filters.date', [
                                        'viewName' => 'user',
                                        'name' => 'created_at',
                                    ])
                                    @empty($needCalls)
                                        @empty($role)
                                            @can('crud.user.columns.roles.read')
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="">@lang('admin.user.columns.roles')</label>
                                                        <select class="form-control" name="" v-model="filters.roles">
                                                            <option value="">@lang('admin.all')</option>
                                                            @foreach ($roles as $role)
                                                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            @endcan
                                        @endempty
                                    @endempty
                                    <div class="col-auto align-self-end">
                                        <button type="button" class="btn btn-primary mb-3" @click="filter('search', search)">
                                            <i class="fa fa-search"></i>
                                            {{ trans('brackets/admin-ui::admin.btn.search') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-end">
                            @if($role && $role->name === 'Employee Invest')
                                <div class="col">
                                    <button :disabled="!itemsSanitized.length" type="button" class="btn btn-primary mb-3" @click="toggleAddInvestTaskModal()">
                                        <i class="fas fa-boxes"></i>
                                        {{ trans('admin.invest-task.columns.add_task') }}
                                    </button>
                                </div>
                            @endif
                            <div class="col-auto form-group">
                                <select class="form-control" v-model="pagination.state.per_page">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="100">100</option>
                                </select>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover table-listing">
                                <thead>
                                <tr>
                                    <th class="no-left-padding">
                                        <div class="form-checkbox">
                                            <input type="checkbox" v-model="selectAll">
                                            <span class="check">
                                                    <i class="zmdi zmdi-check zmdi-hc-lg"></i>
                                                </span>
                                        </div>
                                    </th>

                                    @if(request()->is('admin/roles/6/users'))
                                        <th>color</th>
                                    @endif

                                    @include('admin.layout.cells.th', ['viewName' => 'user', 'name' => 'name'])
                                    <th>
                                        @lang('admin.user.columns.comment')
                                    </th>
                                    @include('admin.layout.cells.th', ['viewName' => 'user', 'name' => 'address'])
                                    @include('admin.layout.cells.th', ['viewName' => 'user', 'name' => 'email'])
                                    @include('admin.layout.cells.th', ['viewName' => 'user', 'name' => 'phone_number'])
                                    @can('crud.user.view')
                                        <th is="sortable" column="last_activity">
                                            @lang('admin.user.columns.last_activity')
                                        </th>
                                    @endcan


                                @if((auth()->user()->hasRole('Employee Invest')) && request()->is('admin/roles/11/users'))

                                @else
                                     @can('crud.user.view')
                                         <th is="sortable" column="last_template">
                                             @lang('admin.user.columns.last_template')
                                         </th>
                                    @endcan
                                    @can('crud.user.view')
                                        <th is="sortable" column="project">
                                            @lang('admin.user.columns.employee_project_id')
                                        </th>
                                    @endcan
                                    @can('crud.user.view')
                                        <th is="sortable" column="hr">
                                            @lang('admin.user.columns.hr_id')
                                        </th>
                                    @endcan
                                    @can('crud.user.view')
                                        <th is="sortable" column="support">
                                            @lang('admin.user.columns.support_id')
                                        </th>
                                    @endcan
                                    @can('crud.user.view')
                                        <th is="sortable" column="cs">
                                            @lang('admin.user.columns.cs_id')
                                        </th>
                                    @endcan
                                    @include('admin.layout.cells.th', ['viewName' => 'user', 'name' => 'created_at'])

                                @endif


                                @if(($role ? ($role->name == 'Employee Staff' || $role->name == 'Employee Invest') : false) || !empty($needCalls))
                                    @include('admin.layout.cells.th', ['viewName' => 'user', 'name' => 'employee_user_status_id'])

                                    @can('crud.user.columns.employee_call_support_comment.read')
                                        <th width="10%">{{ trans('admin.user.columns.employee_call_support_comment') }}</th>
                                        @endcan
                                        @include('admin.layout.cells.th', ['viewName' => 'user', 'name' => 'tasks'])
                                        @can('crud.user.columns.employee_user_status_id.write')
                                        <th is="sortable" column="approved">
                                            @lang('admin.user.columns.employee_approved')
                                        </th>
                                        @endcan
                                    @endif
                                    @if($role && ($role->name === 'Employee Invest' || $role->name === 'Employee Staff'))
{{--                                        <th is="sortable" column="voicent_status">--}}
{{--                                            @lang('admin.user.columns.voicent_status')--}}
{{--                                        </th>--}}
{{--                                        <th is="sortable" column="voicent_time">--}}
{{--                                            @lang('admin.user.columns.voicent_time')--}}
{{--                                        </th>--}}
{{--                                        <th is="sortable" column="voicent_duration">--}}
{{--                                            @lang('admin.user.columns.voicent_duration')--}}
{{--                                        </th>--}}
{{--                                        <th is="sortable" column="voicent_notes">--}}
{{--                                            @lang('admin.user.columns.voicent_notes')--}}
{{--                                        </th>--}}
                                    @endif
                                    @include('admin.layout.cells.th', ['viewName' => 'user', 'name' => 'roles'])
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(item, index) in collection"  v-if="item.employee" :class="item.employee.color">
                                    <td>
                                        <div class="form-checkbox">
                                            <input v-model="selectedItems[item.id]" type="checkbox">
                                            <span class="check">
                                                <i class="zmdi zmdi-check zmdi-hc-lg"></i>
                                            </span>
                                        </div>
                                    </td>

                                    @if(request()->is('admin/roles/6/users'))
                                        <td>
                                            <div class="form-checkbox" >
                                                <span v-for="(color, ind) in colors" @click="painRow(item.id, color)" class="colors" :class="color">x</span>
                                            </div>
                                        </td>
                                    @endif

                                    @include('admin.layout.cells.td', ['viewName' => 'user', 'name' => 'name'])

                                    <td v-if="item.investor_profile">
                                        @{{item.investor_profile.comment}}
                                    </td>
                                    <td v-else>
                                      -
                                    </td>
                                    @can('crud.user.columns.address.read')
                                        <td>
                                            <template v-if="item.profile">
                                                <template v-if="item.profile.state">
                                                    @{{ item.profile.city }}
                                                    @{{ item.profile.state.name }}
                                                    @{{ item.profile.address }}
                                                </template>
                                            </template>
                                        </td>
                                    @endcan
                                    @include('admin.layout.cells.td', ['viewName' => 'user', 'name' => 'email'])
                                    @can('crud.user.columns.phone_number.read')
                                        <td>
                                            <div class="d-flex flex-nowrap align-items-center justify-content-between w-100 ">
                                                <div class="pr-2 nowrap">
                                                    @{{ item.phone_number }}
                                                </div>
                                                <div class="nowrap">
                                                    <a
                                                    v-if="item.phone_number && item.employee && (item.employee.cs_id || '{{Auth::user()->hasRole('HR Manager Invest')}}')"
                                                    class="btn btn-success btn-sm mb-0"
                                                    @click="$root.$refs.sipclient.makeCall(item.phone_number,item.name, item.avatar_thumb_url)">
                                                        <i class="fa fa-phone"></i>
                                                    </a>
                                                    <a
                                                    v-if="item.phone_number"
                                                    class="btn btn-success btn-sm mb-0"
                                                    @click="smsModal(item.id)">
                                                        <i class="fa fa-envelope"></i>
                                                    </a>
                                                </div>

                                            </div>
                                        </td>
                                    @endcan

                                    @can('crud.user.view')
                                        <td>
                                            @{{ _.get(item, 'activity_logs[0].description', '') }}
                                        </td>
                                    @endcan

                                    @if((auth()->user()->hasRole('Employee Invest')) && request()->is('admin/roles/11/users'))

                                    @else

                                    @can('crud.user.view')
                                        <td>
                                            @{{ _.get(item, 'employee.invest_tasks[0].name', '') }}
                                        </td>
                                    @endcan
                                    @can('crud.user.view')
                                        <td>
                                            @{{ _.get(item, 'employee.project.name', '') }}
                                        </td>
                                    @endcan
                                    @can('crud.user.view')
                                        <td>
                                            @{{ _.get(item, 'employee.hr.name', '') }}
                                        </td>
                                    @endcan
                                    @can('crud.user.view')
                                        <td>
                                            @{{ _.get(item, 'employee.support.name', '') }}
                                        </td>
                                    @endcan
                                    @can('crud.user.view')
                                        <td>
                                            @{{ _.get(item, 'employee.call_support.name', '') }}
                                        </td>
                                    @endcan

                                    @include('admin.layout.cells.td', ['viewName' => 'user', 'name' => 'created_at'])

                                    @endif

                                    @if(($role ? ($role->name == 'Employee Staff' || $role->name == 'Employee Invest') : false) || !empty($needCalls))
                                        @can('crud.user.columns.employee_user_status_id.read')
                                            <td>@{{ item.employee ? item.employee.user_status.title : '-' }}</td>
                                        @endcan
                                        @can('crud.user.columns.employee_call_support_comment.read')
                                            <td>
                                                <div class="comment_limit" @click="showComment(item.employee.call_support_comment)">
                                                    @{{ item.employee ? item.employee.call_support_comment : '-' }}
                                                </div>
                                            </td>
                                        @endcan
                                        @can('crud.user.columns.tasks.read')
                                                @if($role && $role->name === 'Employee Staff')
                                                    <td class="nowrap">
                                                        <a :href="'/admin/users/' + item.id + '/tasks'" class="gx-btn gx-btn-default gx-btn-sm">
                                                            @{{ item.tasks_count }}
                                                        </a>
                                                    </td>
                                                @elseif($role && $role->name === 'Employee Invest')
                                                    <td class="nowrap">
                                                        <a :href="'/admin/invest-tasks/?employee_id=' + item.employee.id" class="gx-btn gx-btn-default gx-btn-sm">
                                                            @{{ item.invest_tasks_count }}
                                                        </a>
                                                    </td>
                                                @endif
                                        @endcan
                                        @can('crud.user.columns.employee_user_status_id.write')
                                        <td>
                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                <input type="checkbox" :id="'approved' + item.id" class="custom-control-input" v-model="item.approved" :value="item.approved" @change="updateApproved(item.id, item.approved)">
                                                <label class="custom-control-label" :for="'approved' + item.id"></label>
                                            </div>
                                        </td>
                                        @endcan
                                    @endif
                                    @if($role && ($role->name === 'Employee Invest' || $role->name === 'Employee Staff'))
{{--                                        <td>--}}
{{--                                            @{{ _.get(item, 'employee.voicent_stats.status', '') }}--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            @{{ _.get(item, 'employee.voicent_stats.start_time', '') }}--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            @{{ _.get(item, 'employee.voicent_stats.duration', '') }}--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            @{{ _.get(item, 'employee.voicent_stats.notes', '') }}--}}
{{--                                        </td>--}}
                                    @endif
                                    @can('crud.user.columns.roles.read')
                                        <td>
                                            <template v-for="role in item.roles">
                                                <span class="badge badge-primary mr-1 mb-1">@{{ role.name }}</span>
                                            </template>
                                        </td>
                                    @endcan
                                    <td class="nowrap">
                                        @if($role)
                                            <template v-if="item.employee">
                                                <a
                                                    v-if="_.first(item.roles).name == 'Employee Invest'"
                                                    class="btn btn-sm btn-info"
                                                    :href="'/admin/mails/create?rcpt='+item.email"
                                                    title="{{ trans('admin.mail.actions.create') }}" role="button"
                                                >
                                                    <i class="fas fa-at"></i>
                                                </a>
                                            @can('crud.user.view')
                                                <a
                                                    @click="startChat(item)"
                                                    class="btn btn-sm btn-info"
                                                    href="javascript: void(0)"
                                                    title="@lang('admin.chat.actions.index')" role="button"
                                                ><i class="fas fa-comments"></i></a>
                                            @endcan
                                            @can('crud.user.view')
                                                <a
                                                    v-if="_.first(item.roles).name == 'Employee Invest'"
                                                    @click="toggleInfoModal(item)"
                                                    class="btn btn-sm btn-info"
                                                    href="javascript: void(0)"
                                                    title="@lang('admin.item.columns.info')" role="button"
                                                ><i class="fas fa-folder"></i></a>
                                            @endcan
                                            @can('crud.user.edit')
                                                <a
                                                    v-if="_.first(item.roles).name == 'Employee Invest'"
                                                    @click="toggleAddInvestorsModal(item)"
                                                    class="btn btn-sm btn-info"
                                                    href="javascript: void(0)"
                                                    title="{{ trans('admin.investor.actions.create') }}" role="button"
                                                ><i class="fas fa-user-plus"></i></a>
                                            @endcan
                                            @can('crud.user.edit')
                                                <a
                                                    v-if="_.first(item.roles).name == 'Employee Invest'"
                                                    @click="toggleAddContragentsModal(item)"
                                                    class="btn btn-sm btn-info"
                                                    href="javascript: void(0)"
                                                    title="{{ trans('admin.contragent.actions.create') }}" role="button"
                                                ><i class="fas fa-plus-circle"></i></a>
                                            @endcan
                                            @can('crud.task.create')
                                                    <a
                                                        v-if="_.first(item.roles).name == 'Employee Staff'"
                                                        class="btn btn-sm btn-info"
                                                        :href="'/admin/tasks/create?empl='+item.employee.id"
                                                        title="{{ trans('admin.task.actions.create') }}" role="button"
                                                    >
                                                        <i class="fas fa-boxes"></i>
                                                    </a>
                                                @endcan
                                                @can('crud.invest-task.create')
                                                    <a
                                                        v-if="_.first(item.roles).name == 'Employee Invest'"
                                                        class="btn btn-sm btn-info"
                                                        @click="toggleAddInvestTaskModal(item)"
                                                        href="javascript: void(0)"
                                                        title="{{ trans('admin.task.actions.create') }}" role="button"
                                                    >
                                                        <i class="fas fa-boxes"></i>
                                                    </a>
                                                @endcan
                                                @can('crud.package.create')
                                                    <a
                                                        v-if="_.first(item.roles).name == 'Employee Staff'"
                                                        class="btn btn-sm btn-info"
                                                        :href="'/admin/packages/create?empl='+item.employee.id"
                                                        title="{{ trans('admin.package.actions.create') }}" role="button"
                                                    >
                                                        <i class="fas fa-box"></i>
                                                    </a>
                                                @endcan
                                            </template>
                                            @if($role->name == 'Employee Staff')
                                            @can('crud.user.show')
                                                <a :href="'/admin/users/' + item.id" role="button" class="btn-sm btn btn-secondary" data-toggle="tooltip" data-placement="top" title="User Info">
                                                    <i class="zmdi zmdi-info"></i>
                                                </a>
                                            @endcan
                                            @endif
                                            @can('crud.activity-log.view')
                                            <a :href="'/admin/history?user_id=' + item.id"
                                                role="button"
                                                class="btn-sm btn btn-first badge-primary "
                                                title="{{ trans('admin.mailing.history') }}">
                                                <i class="zmdi zmdi-format-list-numbered"></i>
                                            </a>
                                            @endcan
                                        @endif
                                        @if (empty($needCalls))
                                            <a v-if="item.chat_id" href="#" class="btn btn-sm btn-success" @click.prevent="openChat(item.chat_id)">
                                                <i class="zmdi zmdi-comments zmdi-hc-fw"></i>
                                            </a>
                                            @can('crud.user.edit')
                                                <a class="btn btn-sm btn-spinner btn-info"
                                                   :href="item.resource_url + '/edit'"
                                                   title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i
                                                        class="fa fa-edit"></i></a>
                                            @endcan
                                            @can('crud.user.delete')
                                                <button
                                                @click="deleteItem(item.resource_url)"
                                                class="btn btn-sm btn-danger"
                                                title="{{ trans('brackets/admin-ui::admin.btn.delete') }}">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            @endcan
                                        @else
                                            <template v-if="item.employee">
                                                <a class="btn btn-sm btn-info" href="#" @click="editEmpl(item)"
                                                   title="{{ trans('admin.needcalls.process_empl') }}" role="button"
                                                   v-if="item.employee.cs_id"><i
                                                        class="fa fa-check"></i></a>
                                                <a class="btn btn-sm btn-info" href="#" @click="takeEmpl(item)"
                                                   title="{{ trans('admin.needcalls.get_empl') }}" role="button"
                                                   v-if="!item.employee.cs_id"><i
                                                        class="far fa-hand-rock"></i></a>
                                            </template>
                                        @endif
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center" v-if="_.isEmpty(collection)">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
            </div>

            <b-modal ref="bv-modal-attached-files" hide-footer>
                <template v-slot:modal-title>
                    @lang('admin.item.columns.info')
                </template>
                <table class="table">
                    <tr v-for="media of info.contractCollection">
                        <td>@lang('admin.contract')</td>
                        <td>@{{ media.file_name }}</td>
                        <td>
                            <a :href="`/profile/media/${media.collection_name}/${media.id}/${media.model_id}`" class="btn btn-sm btn-primary">
                                <i class="fa fa-download"></i>
                            </a>
                        </td>
                    </tr>
                    <tr v-for="media of info.agreementCollection">
                        <td>@lang('admin.agreement')</td>
                        <td>@{{ media.file_name }}</td>
                        <td>
                            <a :href="`/profile/media/${media.collection_name}/${media.id}/${media.model_id}`" class="btn btn-sm btn-primary">
                                <i class="fa fa-download"></i>
                            </a>
                        </td>
                    </tr>
                </table>

            </b-modal>

            <b-modal ref="bv-modal-add-investors" hide-footer>
                <template v-slot:modal-title>
                    {{ trans('admin.investor.actions.create') }}
                </template>
                <div class="d-block text-center">
                    <div class="form-group row align-items-center">
                        <label class="col-form-label text-md-right col-md-2">
                            {{ trans('admin.invest-task.columns.investor_lists') }}
                        </label>
                        <div class="col-md-9 col-xl-8">
                            <multiselect v-model="selected_investor_lists" :multiple="true" :options="investor_lists" label="name"
                                         placeholder="{{ trans('admin.invest-task.columns.investor_lists') }}"
                                         track-by="id"
                                         :close-on-select="false"
                            ></multiselect>
                            <div v-if="errors.has('investor_lists')" class="text-danger" v-cloak>@{{ errors.first('investor_lists')
                                }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label class="col-form-label text-md-right col-md-2">
                            {{ trans('admin.invest-task.columns.investors') }}
                        </label>
                        <div class="col-md-9 col-xl-8">
                            <multiselect v-model="selected_investors" :multiple="true" :options="investors" label="name"
                                         placeholder="{{ trans('admin.invest-task.columns.investors') }}"
                                         track-by="id"
                                         :close-on-select="false"
                            ></multiselect>
                            <div v-if="errors.has('investors')" class="text-danger" v-cloak>@{{ errors.first('investors') }}
                            </div>
                        </div>
                    </div>
                    <button class="mt-5 btn btn-primary btn-lg w-100" @click="handleAddInvestorsModal">{{ trans('admin.investor.actions.create') }}</button>
                </div>
            </b-modal>

            <b-modal ref="bv-modal-add-contragents" hide-footer>
                <template v-slot:modal-title>
                    {{ trans('admin.contragent.actions.create') }}
                </template>
                <div class="d-block text-center">
                    <div class="form-group row align-items-center">
                        <label class="col-form-label text-md-right col-md-2">
                            {{ trans('admin.invest-task.columns.contragent_lists') }}
                        </label>
                        <div class="col-md-9 col-xl-8">
                            <multiselect v-model="selected_contragent_lists" :multiple="true" :options="contragent_lists" label="name"
                                         placeholder="{{ trans('admin.invest-task.columns.contragent_lists') }}"
                                         track-by="id"
                                         :close-on-select="false"
                            ></multiselect>
                            <div v-if="errors.has('contragent_lists')" class="text-danger" v-cloak>@{{ errors.first('contragent_lists')
                                }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label class="col-form-label text-md-right col-md-2">
                            {{ trans('admin.invest-task.columns.contragents') }}
                        </label>
                        <div class="col-md-9 col-xl-8">
                            <multiselect v-model="selected_contragents" :multiple="true" :options="contragents" label="name"
                                         placeholder="{{ trans('admin.invest-task.columns.contragents') }}"
                                         track-by="id"
                                         :close-on-select="false"
                            ></multiselect>
                            <div v-if="errors.has('contragents')" class="text-danger" v-cloak>@{{ errors.first('contragents') }}
                            </div>
                        </div>
                    </div>
                    <button class="mt-5 btn btn-primary btn-lg w-100" @click="handleAddContragentsModal">{{ trans('admin.contragent.actions.create') }}</button>
                </div>
            </b-modal>

            <b-modal ref="bv-modal-add-invest-task" hide-footer>
                {{--TODO lang--}}
                <template v-slot:modal-title>
                    {{ trans('admin.invest-task.columns.add_task') }}
                </template>
                <div class="d-block text-center">
                    <div class="form-group row align-items-center">
                        <label class="col-form-label text-md-right col-md-2">
                            {{ trans('admin.invest-task.columns.task') }}
                        </label>
                        <div class="col-md-9 col-xl-8">
                            <multiselect v-model="invest_task" :multiple="false" :options="invest_tasks" label="name"
                                         placeholder="{{ trans('admin.invest-task.columns.task') }}"
                                         track-by="id"
                            ></multiselect>
                            <div v-if="errors.has('investor_id')" class="text-danger" v-cloak>@{{ errors.first('investor_id') }}
                            </div>
                        </div>
                    </div>
                    <button @click="handleAddInvestTaskModal" class="btn btn-primary btn-lg w-100">Create new invest task</button>
                </div>
            </b-modal>

            <modal name="processEmpl">
                <div class="modal-content h-100">
                    <div class="modal-header">
                        <h5 class="modal-title">@lang('admin.needcalls.process_empl')</h5>
                        <button type="button" class="close" @click="$modal.hide('processEmpl')" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row align-items-center">
                            <label
                                class="col-form-label text-md-right col-md-4">@lang('admin.needcalls.columns.status')</label>
                            <div class="col-md-9 col-xl-8">
                                <select class="form-control" v-model="empl.employee.user_status_id">
                                    <option value="2">@lang('admin.needcalls.statuses.applicant')</option>
                                    <option value="3">@lang('admin.needcalls.statuses.approved')</option>
                                    <option value="4">@lang('admin.needcalls.statuses.rejected')</option>
                                    <option value="12">@lang('admin.needcalls.statuses.voicemail')</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row align-items-center">
                            <label
                                class="col-form-label text-md-right col-md-4">@lang('admin.needcalls.columns.cs_comment')</label>
                            <div class="col-md-9 col-xl-8">
                                <textarea class="form-control" v-model="empl.employee.call_support_comment"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"
                                @click="$modal.hide('processEmpl')">@lang('admin.cancel')</button>
                        <button type="button" name="button" class="btn btn-primary" @click="updateEmpl">
                            @lang('admin.save')
                        </button>
                    </div>
                </div>
            </modal>
            <modal name="chat" :draggable="true" :resizable="true">
                <div class="modal-content d-flex flex-column h-100">
                    <div class="modal-body p-0">
                        <chat :chatroom-id="chatId" :modal="true"></chat>
                    </div>
                </div>
            </modal>
            <modal name="smsModal" :resizable="true">
                <div class="modal-content d-flex flex-column h-100">
                    <div class="modal-header">
                        <h5 class="modal-title">@lang('admin.user.send_sms')</h5>
                        <button type="button" class="close" @click="$modal.hide('smsModal')" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="form-group align-items-center">
                            <label
                                class="col-form-label text-md-right col-md-4">@lang('admin.user.sms_text')</label>
                            <div class="col">
                                <textarea class="form-control" v-model="smsMessage"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"
                                @click="$modal.hide('smsModal')">@lang('admin.cancel')</button>
                        <button type="button" name="button" class="btn btn-primary" @click="sendSms">
                            @lang('admin.send')
                        </button>
                    </div>
                </div>
            </modal>
            <modal name="comment">
                <div class="modal-content d-flex flex-column h-100">
                    <div class="modal-header">
                        <h5 class="modal-title">@lang('admin.needcalls.columns.cs_comment')</h5>
                        <button type="button" class="close" @click="$modal.hide('comment')" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @{{ comment }}
                    </div>
                </div>
            </modal>
        </div>
    </user-listing>

@endsection
