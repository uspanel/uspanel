@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.user.actions.create'))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>@lang('admin.user.actions.create')</span></h2>
    </div>
    <user-form
        :states="{{ $states->toJson() }}"
        :action="'{{ url('admin/users') }}'"
        :employees_staff="{{ $employees_staff->toJson() }}"
        :employees_invest="{{ $employees_invest->toJson() }}"
        :staffers="{{ $staffers ? $staffers->toJson() : '[]' }}"
        :investors="{{ $investors ? $investors->toJson() : '[]' }}"
        :contragents="{{ $contragents ? $contragents->toJson() : '[]' }}"service
        :investor_lists="{{ $investor_lists ? $investor_lists->toJson() : '[]' }}"
        :contragent_lists="{{ $contragent_lists ? $contragent_lists->toJson() : '[]' }}"

        :hr_manager_staff_list="{{ $hr_manager_staff_list ? $hr_manager_staff_list->toJson() : '[]'  }}"
        :hr_manager_invest_list="{{ $hr_manager_invest_list ? $hr_manager_invest_list->toJson() : '[]' }}"
        :support_staff_list="{{ $support_staff_list ? $support_staff_list->toJson() : '[]' }}"
        :support_invest_list="{{ $support_invest_list ? $support_invest_list->toJson() : '[]' }}"

        inline-template>
        <form method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>
            <div class="card">
                <div class="card-body">
                    @include('admin.user.components.form-elements')
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </user-form>
@endsection
