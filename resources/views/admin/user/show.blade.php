@extends('brackets/admin-ui::admin.layout.default')

@section('title', $user->name)

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0">
        	<span>
	        	{{ $user->name }}
	        </span>
	    </h2>
    </div>
    <div class="row">
    	<div class="col-md-3">
    		<div class="gx-card net-chart">
			    <div class="gx-card-thumb bg-secondary">
			        <i class="zmdi zmdi-assignment-check"></i>
			    </div>
			    <div class="gx-card-body br-break">
			        <h4 class="mb-0"><strong>{{ $user->tasks_count }}</strong></h4>
			        <p class="mb-0">@lang('admin.user.tasks')</p>
			        <h4 class="mb-0"><strong>{{ $user->employee->tasks()->whereNotNull('delivered_at')->count() }}</strong></h4>
			        <p class="mb-0">@lang('admin.user.delivered')</p>
			    </div>
			</div>
    	</div>
    	<div class="col-md-3">
    		<div class="gx-card net-chart">
			    <div class="gx-card-thumb bg-teal">
			        <i class="zmdi zmdi-truck zmdi-hc-fw"></i>
			    </div>
			    <div class="gx-card-body br-break">
			        <h4 class="mb-0"><strong>{{ $user->employee->tasks()->withCount('items')->get()->sum('items_count') }}</strong></h4>
			        <p class="mb-0">@lang('admin.user.items')</p>
			    </div>
			</div>
    	</div>
    	@if($rating)
	    	<div class="col-md-3">
	    		<div class="gx-card net-chart">
				    <div class="gx-card-thumb bg-amber">
				        <i class="zmdi zmdi-star"></i>
				    </div>
				    <div class="gx-card-body br-break">
				        <h4 class="mb-0"><strong>{{ $rating }}</strong></h4>
				        <p class="mb-0">@lang('admin.user.rating')</p>
				    </div>
				</div>
	    	</div>
    	@endif
    	@if(auth()->user()->hasRole('Administrator') && $user->mailing)
    	<div class="col-md-3">
    		<div class="gx-card net-chart">
			    <div class="gx-card-thumb bg-purple">
			        <i class="zmdi zmdi-format-list-numbered"></i>
			    </div>
			    <div class="gx-card-body br-break">
			        <p class="mb-0">
			        	{{ $user->mailing->name }}
			        </p>
			    </div>
			</div>
    	</div>
    	@endif
    </div>
    <div class="gx-card">
	    <div class="gx-card-body">
	    	<table class="table">
	    		<tbody>
	    			<tr>
	    				<td class="font-weight-bold">
	    					@lang('admin.user.profile.columns.referrer')
	    				</td>
	    				<td>
	    					{{ $user->profile->referrer }}
	    				</td>
	    			</tr>
	    		</tbody>
	    	</table>
	    </div>
	</div>
    <div class="gx-card">
	    <div class="gx-card-header">
	    </div>
	    <task-listing
	        :data="{{ $tasks->toJson() }}"
	        :url="'{{ url('admin/tasks') }}'"
	        :employee-id="{{ $user->id }}"
	        inline-template>
		    <div class="gx-card-body">
		        @include('admin.task.table')
			</div>
		</task-listing>
	</div>
	<user-salary :user-id="{{ $user->id }}" />
@endsection