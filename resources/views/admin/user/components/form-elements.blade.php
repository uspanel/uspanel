{{-- @can('crud.user.columns.name.write')
<div class="form-group row align-items-center">
    <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.name" @input="validate($event)" class="form-control" placeholder="{{ trans('admin.user.columns.name') }}">
        <div v-if="errors.has('name')" class="text-danger" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>
@endcan --}}

@can('crud.user.columns.email.write')
    <div class="form-group row align-items-center">
        <label for="email" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.email') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.email" v-validate="'required|email'" @input="validate($event)"
                   class="form-control" id="email" name="email" placeholder="{{ trans('admin.user.columns.email') }}">
            <div v-if="errors.has('email')" class="text-danger" v-cloak>@{{ errors.first('email') }}</div>
        </div>
    </div>
@endcan

@can('crud.user.columns.password.write')
    <div class="form-group row align-items-center">
        <label for="password" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.password') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="password" v-model="form.password" @input="validate($event)" class="form-control" id="password"
                   name="password" placeholder="{{ trans('admin.user.columns.password') }}" ref="password">
            <div v-if="errors.has('password')" class="text-danger" v-cloak>@{{ errors.first('password') }}</div>
        </div>
    </div>
@endcan

@can('crud.user.columns.first_name.write')
    <div class="form-group row align-items-center">
        <label for="first_name" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.first_name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.first_name" v-validate="''" @input="validate($event)" class="form-control"
                   id="first_name" name="first_name" placeholder="{{ trans('admin.user.columns.first_name') }}">
            <div v-if="errors.has('first_name')" class="text-danger" v-cloak>@{{ errors.first('first_name') }}</div>
        </div>
    </div>
@endcan

@can('crud.user.columns.last_name.write')
    <div class="form-group row align-items-center">
        <label for="last_name" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.last_name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.last_name" v-validate="''" @input="validate($event)" class="form-control"
                   id="last_name" name="last_name" placeholder="{{ trans('admin.user.columns.last_name') }}">
            <div v-if="errors.has('last_name')" class="text-danger" v-cloak>@{{ errors.first('last_name') }}</div>
        </div>
    </div>
@endcan

@can('crud.user.columns.phone_number.write')
    <div class="form-group row align-items-center">
        <label for="phone_number" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.phone_number') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <the-mask :masked="true" :mask="'+' + {{env('PHONE_PREFIX')}} + ' (###) ###-####'"
                      v-model="form.phone_number" class="form-control" id="phone_number" name="phone_number"
                      placeholder="{{ trans('admin.user.columns.phone_number') }}"></the-mask>
                <div v-if="errors.has('phone_number')" class="text-danger" v-cloak>@{{ errors.first('phone_number') }}
                </div>
        </div>
    </div>
@endcan

@can('crud.user.columns.roles.write')
    <div class="form-group row align-items-center">
        <label for="roles" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.admin-user.columns.roles') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            {{-- <multiselect v-model="form.roles" placeholder="{{ trans('brackets/admin-ui::admin.forms.select_options') }}" label="name" track-by="id" :options="{{ $roles->toJson() }}" :multiple="true" open-direction="bottom"></multiselect> --}}
            <select class="form-control" v-model="role_id">
                @foreach($roles as $role)
                    <option value="{{ $role->id }}">{{ $role->name }}</option>
                @endforeach
            </select>
            <div v-if="errors.has('role_id')" class="text-danger" v-cloak>@{{ errors.first('role_id') }}</div>
        </div>
    </div>
@endcan
@can('crud.user.columns.staffer.write')
    <div class="form-group row align-items-center" v-if="form.role_id == 4">
        <label class="col-form-label text-md-right col-md-2">
            {{ trans('admin.user.columns.employees') }}
        </label>
        <div class="col-md-9 col-xl-8">
            <multiselect v-model="extraForm.employees" :multiple="true" :options="employees" label="name"
                         track-by="id" :close-on-select="false"
            ></multiselect>
            <div v-if="errors.has('employees')" class="text-danger" v-cloak>@{{ errors.first('employees') }}</div>
        </div>
    </div>

    <div class="form-group row align-items-center" v-if="form.role_id == 5 || form.role_id == 6">
        <label class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.staffer_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <multiselect v-model="extraForm.employee.staffers" :multiple="true" :options="staffers" track-by="id"
                         label="name" :close-on-select="false"
            ></multiselect>
            <div v-if="errors.has('employee.staffers')" class="text-danger" v-cloak>@{{
                errors.first('employee.staffers') }}
            </div>
        </div>
    </div>

    <div class="form-group row align-items-center" v-if="form.role_id == 5 || form.role_id == 6">
        <label class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.hr_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <multiselect v-model="extraForm.employee.hr_id" :multiple="false" :options="hr" track-by="id"
                         label="name"></multiselect>
            <div v-if="errors.has('employee.hr_id')" class="text-danger" v-cloak>@{{ errors.first('employee.hr_id') }}
            </div>
        </div>
    </div>

    <div class="form-group row align-items-center" v-if="form.role_id == 5 || form.role_id == 6">
        <label class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.support_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <multiselect v-model="extraForm.employee.support_id" :multiple="false" :options="support" track-by="id"
                         label="name"></multiselect>
            <div v-if="errors.has('employee.support_id')" class="text-danger" v-cloak>@{{
                errors.first('employee.support_id') }}
            </div>
        </div>
    </div>
@endcan

@can('crud.user.columns.employee_project_id.write')
    <div class="form-group row align-items-center">
        <label class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.project_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            @if(!isset($user) || $user->hasRole('Employee Staff') || $user->hasRole('Employee Invest') || $user->hasRole('Investor'))
                <select class="form-control" v-model="form.project_id">
                    <option :value="null"></option>
                    @foreach ($projects as $project)
                        <option value="{{ $project->id }}">
                            {{ $project->name }}
                        </option>
                    @endforeach
                </select>
            @else
                <select class="form-control" multiple v-model="form.project_id">
                    @foreach ($projects as $project)
                        <option value="{{ $project->id }}">
                            {{ $project->name }}
                        </option>
                    @endforeach
                </select>
            @endif
            <div v-if="errors.has('project_id')" class="text-danger" v-cloak>@{{ errors.first('project_id') }}</div>
        </div>
    </div>
@endcan





@can('crud.user.columns.receive_external_calls.write')
    <div class="form-group row align-items-center">
        <label for="roles" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.receive_external') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <div class="row">
                <div class="col-auto">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" id="receive_external_yes" value="1"
                               v-model="form.receive_external_calls">
                        <label class="form-check-label" for="receive_external_yes">
                            @lang('admin.yes')
                        </label>
                    </div>
                </div>
                <div class="col-auto">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" id="receive_external_no" value="0"
                               v-model="form.receive_external_calls">
                        <label class="form-check-label" for="receive_external_no">
                            @lang('admin.no')
                        </label>
                    </div>
                </div>
            </div>
            <div v-if="errors.has('receive_external_calls')" class="text-danger">@{{
                errors.first('receive_external_calls') }}
            </div>
        </div>
    </div>
@endcan



<div v-if="form.role_id == 5 || form.role_id == 6 || form.role_id == 11">
    @if ($user->profile ?? !isset($user))

        @can('crud.user.columns.profile_address.write')
            <div class="form-group row align-items-center">
                <label for="roles" class="col-form-label text-md-right"
                       :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.address') }}</label>
                <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                    <input type="text" class="form-control" v-model="form.profile.address">
                    <div v-if="errors.has('profile.address')" class="text-danger">@{{ errors.first('profile.address')
                        }}
                    </div>
                </div>
            </div>
        @endcan

        @can('crud.user.columns.profile_city.write')
            <div class="form-group row align-items-center">
                <label for="roles" class="col-form-label text-md-right"
                       :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.city') }}</label>
                <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                    <input type="text" class="form-control" v-model="form.profile.city">
                    <div v-if="errors.has('profile.city')" class="text-danger">@{{ errors.first('profile.city') }}</div>
                </div>
            </div>
        @endcan

        @can('crud.user.columns.profile_state_id.write')
            <div class="form-group row align-items-center">
                <label for="roles" class="col-form-label text-md-right"
                       :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.state') }}</label>
                <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                    <multiselect
                        v-model="form.profile.state_id"
                        :options="states"
                        :multiple="false"
                        track-by="id"
                        label="name"></multiselect>
                    <div v-if="errors.has('profile.state_id')" class="text-danger">@{{ errors.first('profile.state_id')
                        }}
                    </div>
                </div>
            </div>
        @endcan

        @can('crud.user.columns.profile_zip.write')
            <div class="form-group row align-items-center">
                <label for="roles" class="col-form-label text-md-right"
                       :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.zip') }}</label>
                <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                    <the-mask :masked="true" mask="XXXXX" class="form-control" v-model="form.profile.zip"></the-mask>
                    <div v-if="errors.has('profile.zip')" class="text-danger">@{{ errors.first('profile.zip') }}</div>
                </div>
            </div>
        @endcan
        <div v-if="form.role_id == 5 || form.role_id == 6">
        @can('crud.user.columns.profile_authorized_to_work.write')
            <div class="form-group row align-items-center">
                <label for="roles" class="col-form-label text-md-right"
                       :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.authorized_to_work') }}</label>
                <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                    <div class="row">
                        <div class="col-auto">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="authorized_to_work_yes" value="1"
                                       v-model="form.profile.authorized_to_work">
                                <label class="form-check-label" for="authorized_to_work_yes">
                                    @lang('admin.yes')
                                </label>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="authorized_to_work_no" value="0"
                                       v-model="form.profile.authorized_to_work">
                                <label class="form-check-label" for="authorized_to_work_no">
                                    @lang('admin.no')
                                </label>
                            </div>
                        </div>
                    </div>
                    <div v-if="errors.has('profile.authorized_to_work')" class="text-danger">@{{
                        errors.first('profile.authorized_to_work') }}
                    </div>
                </div>
            </div>
        @endcan
        @can('crud.user.columns.profile_adult.write')
            <div class="form-group row align-items-center">
                <label for="roles" class="col-form-label text-md-right"
                       :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.adult') }}</label>
                <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                    <div class="row">
                        <div class="col-auto">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="adult_yes" value="1"
                                       v-model="form.profile.adult">
                                <label class="form-check-label" for="adult_yes">
                                    @lang('admin.yes')
                                </label>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="adult_no" value="0"
                                       v-model="form.profile.adult">
                                <label class="form-check-label" for="adult_no">
                                    @lang('admin.no')
                                </label>
                            </div>
                        </div>
                    </div>
                    <div v-if="errors.has('profile.adult')" class="text-danger">@{{ errors.first('profile.adult') }}
                    </div>
                </div>
            </div>
        @endcan
        @can('crud.user.columns.profile_higher_education.write')
            <div class="form-group row align-items-center">
                <label for="roles" class="col-form-label text-md-right"
                       :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.higher_education') }}</label>
                <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                    <div class="row">
                        <div class="col-auto">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="higher_education_yes" value="1"
                                       v-model="form.profile.higher_education">
                                <label class="form-check-label" for="higher_education_yes">
                                    @lang('admin.yes')
                                </label>
                            </div>
                        </div>
                        <div class="col-auto">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="higher_education_no" value="0"
                                       v-model="form.profile.higher_education">
                                <label class="form-check-label" for="higher_education_no">
                                    @lang('admin.no')
                                </label>
                            </div>
                        </div>
                    </div>
                    <div v-if="errors.has('profile.higher_education')" class="text-danger">@{{
                        errors.first('profile.higher_education') }}
                    </div>
                </div>
            </div>
        @endcan
        @can('crud.user.columns.employee_user_status_id.read')
            <div class="form-group row align-items-center">
                <label for="roles" class="col-form-label text-md-right"
                       :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.employee.columns.user_status_id') }}</label>
                <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                    <select class="form-control" v-model="form.employee.user_status_id">
                        <option :value="null"></option>
                        @foreach ($userStatuses as $status)
                            <option value="{{ $status->id }}">
                                {{ $status->title }}
                            </option>
                        @endforeach
                    </select>
                    <div v-if="errors.has('employee.user_status_id')" class="text-danger">@{{
                        errors.first('employee.user_status_id') }}
                    </div>
                </div>
            </div>
        @endcan
        </div>
    @endif

</div>

<div v-if="form.role_id == 6">
    @if ($user->profile ?? !isset($user))

        {{-- Contragents --}}
        <div class="form-group row align-items-center">
            <label class="col-form-label text-md-right col-md-2">
                {{ trans('admin.invest-task.columns.contragent_lists') }}
            </label>
            <div class="col-md-9 col-xl-8">
                <multiselect
                    v-model="form.employee.contragent_lists"
                    :multiple="true"
                    :options="contragent_lists"
                    label="name"
                    placeholder="{{ trans('admin.invest-task.columns.contragent_lists') }}"
                    track-by="id"
                    :close-on-select="false"
                ></multiselect>
                <div v-if="errors.has('contragent_lists')" class="text-danger" v-cloak>@{{
                    errors.first('contragent_lists') }}
                </div>
            </div>
        </div>
        <div class="form-group row align-items-center">
            <label class="col-form-label text-md-right col-md-2">
                {{ trans('admin.invest-task.columns.contragents') }}
            </label>
            <div class="col-md-9 col-xl-8">
                <multiselect
                    v-model="form.employee.contragents"
                    :multiple="true"
                    :options="contragents"
                    label="name"
                    placeholder="{{ trans('admin.invest-task.columns.contragents') }}"
                    track-by="id"
                    :close-on-select="false"
                ></multiselect>
                <div v-if="errors.has('contragents')" class="text-danger" v-cloak>@{{ errors.first('contragents')
                    }}
                </div>
            </div>
        </div>

        {{-- Investors --}}
        <div class="form-group row align-items-center">
            <label class="col-form-label text-md-right col-md-2">
                {{ trans('admin.invest-task.columns.investor_lists') }}
            </label>
            <div class="col-md-9 col-xl-8">
                <multiselect v-model="form.employee.investor_lists" :multiple="true" :options="investor_lists" label="name"
                             placeholder="{{ trans('admin.invest-task.columns.investor_lists') }}"
                             track-by="id"
                             :close-on-select="false"
                ></multiselect>
                <div v-if="errors.has('investor_lists')" class="text-danger" v-cloak>@{{ errors.first('investor_lists')
                    }}
                </div>
            </div>
        </div>
        <div class="form-group row align-items-center">
            <label class="col-form-label text-md-right col-md-2">
                {{ trans('admin.invest-task.columns.investors') }}
            </label>
            <div class="col-md-9 col-xl-8">
                <multiselect v-model="form.employee.investors" :multiple="true" :options="investors" label="name"
                             placeholder="{{ trans('admin.invest-task.columns.investors') }}"
                             track-by="id"
                             :close-on-select="false"
                ></multiselect>
                <div v-if="errors.has('investors')" class="text-danger" v-cloak>@{{ errors.first('investors') }}
                </div>
            </div>
        </div>
    @endif
</div>




@can('crud.user.columns.investor.write')
    <div v-if="form.role_id == 11">
        <div class="form-group row align-items-center">
            <label for="activity_years" class="col-form-label text-md-right"
                   :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.activity_years') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <input type="text" v-model="form.investor_profile.activity_years" v-validate="''"
                       @input="validate($event)" class="form-control" id="activity_years" name="activity_years"
                       placeholder="{{ trans('admin.user.columns.activity_years') }}">
                <div v-if="errors.has('activity_years')" class="text-danger" v-cloak>@{{ errors.first('activity_years')
                    }}
                </div>
            </div>
        </div>
        <div class="form-group row align-items-center">
            <label for="activity_time" class="col-form-label text-md-right"
                   :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.activity_time') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <input type="text" v-model="form.investor_profile.activity_time" v-validate="''"
                       @input="validate($event)" class="form-control" id="activity_time" name="activity_time"
                       placeholder="{{ trans('admin.user.columns.activity_time') }}">
                <div v-if="errors.has('activity_time')" class="text-danger" v-cloak>@{{ errors.first('activity_time')
                    }}
                </div>
            </div>
        </div>
        <div class="form-group row align-items-center">
            <label for="country" class="col-form-label text-md-right"
                   :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.country') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <input type="text" v-model="form.investor_profile.country" v-validate="''" @input="validate($event)"
                       class="form-control" id="country" name="country"
                       placeholder="{{ trans('admin.user.columns.country') }}">
                <div v-if="errors.has('country')" class="text-danger" v-cloak>@{{ errors.first('country') }}</div>
            </div>
        </div>
        <div class="form-group row align-items-center">
            <label for="comment" class="col-form-label text-md-right"
                   :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.comment') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <input type="text" v-model="form.investor_profile.comment" v-validate="''" @input="validate($event)"
                       class="form-control" id="comment" name="comment"
                       placeholder="{{ trans('admin.user.columns.comment') }}">
                <div v-if="errors.has('comment')" class="text-danger" v-cloak>@{{ errors.first('comment') }}</div>
            </div>
        </div>
        <div class="form-group row align-items-center">
            <label for="comment" class="col-form-label text-md-right"
                   :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.user.columns.manager') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <select class="form-control" v-model="form.investor_profile.manager_id">
                    @foreach (\App\User::role(['HR Manager Invest'])->get() as $manager)
                        <option value="{{ $manager->id }}">
                            {{ $manager->name }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr class="my-5">
        <div class="form-group row align-items-center">
            <label for="investor_mail.smtp_host" class="col-form-label text-md-right"
                   :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.smtp_host') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <input type="text" v-model="form.investor_mail.smtp_host" class="form-control"
                       id="investor_mail.smtp_host" name="investor_mail.smtp_host"
                       placeholder="{{ trans('admin.mailsetting.columns.smtp_host') }}">
                <div v-if="errors.has('investor_mail.smtp_host')" class="text-danger" v-cloak>@{{
                    errors.first('investor_mail.smtp_host') }}
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center">
            <label for="investor_mail.smtp_port" class="col-form-label text-md-right"
                   :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.smtp_port') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <input type="text" v-model="form.investor_mail.smtp_port" class="form-control"
                       id="investor_mail.smtp_port" name="investor_mail.smtp_port"
                       placeholder="{{ trans('admin.mailsetting.columns.smtp_port') }}">
                <div v-if="errors.has('investor_mail.smtp_port')" class="text-danger" v-cloak>@{{
                    errors.first('investor_mail.smtp_port') }}
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center">
            <label for="investor_mail.smtp_encryption" class="col-form-label text-md-right"
                   :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.smtp_encryption') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <select
                    class="form-control"
                    id="investor_mail.smtp_encryption"
                    name="investor_mail.smtp_encryption"
                    v-model="form.investor_mail.smtp_encryption">
                    <option value="ssl">SSL</option>
                    <option value="tls">TLS</option>
                    <option :value="null">None</option>
                </select>
                <div v-if="errors.has('investor_mail.smtp_encryption')" class="text-danger" v-cloak>@{{
                    errors.first('investor_mail.smtp_encryption') }}
                </div>
            </div>
        </div>

        <hr class="my-5">

        <div class="form-group row align-items-center">
            <label for="investor_mail.imap_host" class="col-form-label text-md-right"
                   :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.imap_host') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <input type="text" v-model="form.investor_mail.imap_host" class="form-control"
                       id="investor_mail.imap_host" name="investor_mail.imap_host"
                       placeholder="{{ trans('admin.mailsetting.columns.imap_host') }}">
                <div v-if="errors.has('investor_mail.imap_host')" class="text-danger" v-cloak>@{{
                    errors.first('investor_mail.imap_host') }}
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center">
            <label for="investor_mail.imap_port" class="col-form-label text-md-right"
                   :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.imap_port') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <input type="text" v-model="form.investor_mail.imap_port" class="form-control"
                       id="investor_mail.imap_port" name="investor_mail.imap_port"
                       placeholder="{{ trans('admin.mailsetting.columns.imap_port') }}">
                <div v-if="errors.has('investor_mail.imap_port')" class="text-danger" v-cloak>@{{
                    errors.first('investor_mail.imap_port') }}
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center">
            <label for="investor_mail.imap_encryption" class="col-form-label text-md-right"
                   :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.imap_encryption') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <select
                    class="form-control"
                    id="investor_mail.imap_encryption"
                    name="investor_mail.imap_encryption"
                    v-model="form.investor_mail.imap_encryption">
                    <option value="ssl">SSL</option>
                    <option value="tls">TLS</option>
                    <option :value="null">None</option>
                </select>
                <div v-if="errors.has('investor_mail.imap_encryption')" class="text-danger" v-cloak>@{{
                    errors.first('investor_mail.imap_encryption') }}
                </div>
            </div>
        </div>

        <hr class="my-5">

        <div class="form-group row align-items-center">
            <label for="investor_mail.login" class="col-form-label text-md-right"
                   :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.login') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <input type="text" v-model="form.investor_mail.login" class="form-control" id="investor_mail.login"
                       name="investor_mail.login" placeholder="{{ trans('admin.mailsetting.columns.login') }}">
                <div v-if="errors.has('investor_mail.login')" class="text-danger" v-cloak>@{{
                    errors.first('investor_mail.login') }}
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center">
            <label for="investor_mail.password" class="col-form-label text-md-right"
                   :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.password') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <input type="password" v-model="form.investor_mail.password" class="form-control"
                       id="investor_mail.password" name="investor_mail.password"
                       placeholder="{{ trans('admin.mailsetting.columns.password') }}" ref="password">
                <div v-if="errors.has('investor_mail.password')" class="text-danger" v-cloak>@{{
                    errors.first('investor_mail.password') }}
                </div>
            </div>
        </div>

        <hr class="my-5">


        <div class="form-group row align-items-center">
            <label class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'"></label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <div class="">
                    <button type="button" name="button" class="btn btn-primary" @click="getFolders">
                        <i v-if="loading" class="fa fa-spinner"></i>
                        {{ trans('admin.mailsetting.get_folders') }}
                    </button>
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center">
            <label for="investor_mail.imap_inbox_folder" class="col-form-label text-md-right"
                   :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.imap_inbox_folder') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <select
                    id="investor_mail.imap_inbox_folder"
                    name="investor_mail.imap_inbox_folder"
                    class="form-control"
                    v-model="form.investor_mail.imap_inbox_folder">
                    <option :value="item.attributes.name" v-for="item in folders">
                        @{{ item.attributes.name }}
                    </option>
                </select>
                <div v-if="errors.has('investor_mail.imap_inbox_folder')" class="text-danger" v-cloak>@{{
                    errors.first('investor_mail.imap_inbox_folder') }}
                </div>
            </div>
        </div>

        <div class="form-group row align-items-center">
            <label for="investor_mail.imap_sent_folder" class="col-form-label text-md-right"
                   :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mailsetting.columns.imap_sent_folder') }}</label>
            <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
                <select
                    id="investor_mail.imap_sent_folder"
                    name="investor_mail.imap_sent_folder"
                    class="form-control"
                    v-model="form.investor_mail.imap_sent_folder">
                    <option :value="item.attributes.name" v-for="item in folders">
                        @{{ item.attributes.name }}
                    </option>
                </select>
                <div v-if="errors.has('investor_mail.imap_sent_folder')" class="text-danger" v-cloak>@{{
                    errors.first('investor_mail.imap_sent_folder') }}
                </div>
            </div>
        </div>

        <hr class="my-5">

        <div class="form-check row">
            <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
                <input class="form-check-input" id="investor_mail.validate_cert" type="checkbox"
                       name="default_fake_element">
                <label class="form-check-label" for="investor_mail.validate_cert">
                    {{ trans('admin.mailsetting.columns.validate_cert') }}
                </label>
                <input type="hidden" name="investor_mail.validate_cert" :value="form.investor_mail.validate_cert">
                <div v-if="errors.has('investor_mail.validate_cert')" class="text-danger" v-cloak>@{{
                    errors.first('investor_mail.validate_cert') }}
                </div>
            </div>
        </div>
        <br>
    </div>
@endcan
