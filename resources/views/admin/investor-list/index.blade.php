@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.investor-list.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0">
                <span>{{ trans('admin.investor-list.actions.index') }}</span>
            </h2>
        </div>
        @can('crud.investor-list.create')
        <div class="col-auto">
            <a class="btn btn-primary" href="{{ url('/admin/investor-lists/create') }}" role="button">
                <i class="fa fa-plus"></i>
                {{ trans('admin.investor-list.actions.create') }}
            </a>
        </div>
        @endcan
    </div>
    <investor-list-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('/admin/investor-lists') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <form @submit.prevent="">
                            <div class="row justify-content-md-between">
                                <div class="col col-lg-7 col-xl-5 form-group">
                                    <div class="input-group">
                                        <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                        <span class="input-group-append">
                                            <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                        </span>
                                    </div>
                                </div>

                                <div class="col-sm-auto form-group ">
                                    <select class="form-control" v-model="pagination.state.per_page">

                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>

                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th is='sortable' :column="'id'">{{ trans('admin.investor-list.columns.id') }}</th>
                                        <th is='sortable' :column="'name'">{{ trans('admin.investor-list.columns.name') }}</th>
                                        <th is='sortable' :column="'users'">{{ trans('admin.investor-list.columns.investors') }}</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection">
                                        <td>@{{ item.id }}</td>
                                        <td>@{{ item.name }}</td>
                                        <td>
                                            <div v-for="user in item.users">
                                                @{{ user.name }} [@{{ user.email }}]
                                            </div>
                                        </td>

                                        <td class="nowrap">
                                            @can('crud.contragent-list.edit')
                                                <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                            @endcan
                                            @can('crud.contragent-list.delete')
                                            <button
                                            @click="deleteItem(item.resource_url)"
                                            class="btn btn-sm btn-danger"
                                            title="Delete">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                            @endcan
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center" v-if="!collection.length > 0">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </investor-list-listing>

@endsection
