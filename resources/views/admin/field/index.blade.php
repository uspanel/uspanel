@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.field.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0">
                <span>{{ trans('admin.field.actions.index') }}</span>
            </h2>
        </div>
        <div class="col-auto">
            <a class="btn btn-primary" href="{{ route('admin/fields/create', [
                'form' => $form->id
            ]) }}" role="button">
                <i class="fa fa-plus"></i>
                {{ trans('admin.field.actions.create') }}
            </a>
        </div>
    </div>
    <field-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('/forms/' . $form->id . '/fields') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <form @submit.prevent="">
                            <div class="row justify-content-md-between">
                                <div class="col col-lg-7 col-xl-5 form-group">
                                    <div class="input-group">
                                        <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                        <span class="input-group-append">
                                            <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                        </span>
                                    </div>
                                </div>

                                <div class="col-sm-auto form-group ">
                                    <select class="form-control" v-model="pagination.state.per_page">

                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>

                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th is='sortable' :column="'id'">{{ trans('admin.field.columns.id') }}</th>
                                        <th is='sortable' :column="'field_type_id'">{{ trans('admin.field.columns.field_type_id') }}</th>
                                        <th is='sortable' :column="'title'">{{ trans('admin.field.columns.title') }}</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection">
                                        <td>@{{ item.id }}</td>
                                        <td>@{{ item.field_type.name }}</td>
                                        <td>@{{ item.title }}</td>
                                        <td class="nowrap">
                                            <a class="btn btn-sm btn-spinner btn-info" :href="'/forms/{{ $form->id }}/fields/' + item.id + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                            <button
                                            @click="deleteItem('/forms/{{ $form->id }}/fields/' + item.id)"
                                            class="btn btn-sm btn-danger"
                                            title="{{ trans('brackets/admin-ui::admin.btn.delete') }}">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center" v-if="!collection.length > 0">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </field-listing>

@endsection
