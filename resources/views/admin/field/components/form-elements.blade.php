<div class="form-group row align-items-center">
    <label for="field_type_id" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.field.columns.field_type_id') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <select v-model="form.field_type_id" class="form-control" id="field_type_id" name="field_type_id">
            @foreach($fieldTypes as $fieldType)
                <option value="{{$fieldType->id}}">@lang('codersstudio/form-creator::form.types.' . $fieldType->name)</option>
            @endforeach
        </select>
        <div v-if="errors.has('field_type_id')" class="text-danger" v-cloak>@{{ errors.first('field_type_id') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.field.columns.name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.name" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('name'), 'form-control-success': this.fields.name && this.fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.field.columns.name') }}">
        <div v-if="errors.has('name')" class="text-danger" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="title" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.field.columns.title') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.title" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('title'), 'form-control-success': this.fields.title && this.fields.title.valid}" id="title" name="title" placeholder="{{ trans('admin.field.columns.title') }}">
        <div v-if="errors.has('title')" class="text-danger" v-cloak>@{{ errors.first('title') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="description" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.field.columns.description') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.description" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('description'), 'form-control-success': this.fields.description && this.fields.description.valid}" id="description" name="description" placeholder="{{ trans('admin.field.columns.description') }}">
        <div v-if="errors.has('description')" class="text-danger" v-cloak>@{{ errors.first('description') }}</div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="rules" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.field.columns.rules') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <div class="custom-control custom-checkbox custom-control-inline">
                <input type="checkbox" value="required" v-model="form.rules" id="rules" name="rules" class="custom-control-input" :checked="true">
                <label class="custom-control-label" for="rules">@lang('admin.required')</label>
            </div>
        </div>
        <div v-if="errors.has('rules')" class="text-danger" v-cloak>@{{ errors.first('rules') }}</div>
    </div>
</div>
<div class="form-group row align-items-center">
    <label for="ordering" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('codersstudio/form-creator::form.ordering') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="number" v-model="form.ordering" v-validate="''" @input="validate($event)"
               class="form-control" id="ordering" name="ordering">
        <div v-if="errors.has('ordering')" class="text-danger" v-cloak>@{{ errors.first('ordering')
            }}
        </div>
    </div>
</div>

<div class="form-check row">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="enabled" type="checkbox" v-model="form.enabled" v-validate="''" data-vv-name="enabled"  name="enabled_fake_element">
        <label class="form-check-label" for="enabled">
            {{ trans('admin.field.columns.enabled') }}
        </label>
        <input type="hidden" name="enabled" :value="form.enabled">
        <div v-if="errors.has('enabled')" class="text-danger" v-cloak>@{{ errors.first('enabled') }}</div>
    </div>
</div>



<div class="form-check row">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="sortable" type="checkbox" v-model="form.sortable" v-validate="''" data-vv-name="sortable"  name="sortable_fake_element">
        <label class="form-check-label" for="sortable">
            {{ trans('admin.field.columns.sortable') }}
        </label>
        <input type="hidden" name="sortable" :value="form.sortable">
        <div v-if="errors.has('sortable')" class="text-danger" v-cloak>@{{ errors.first('sortable') }}</div>
    </div>
</div>

<div class="form-check row">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="searchable" type="checkbox" v-model="form.searchable" v-validate="''" data-vv-name="searchable"  name="searchable_fake_element">
        <label class="form-check-label" for="searchable">
            {{ trans('admin.field.columns.searchable') }}
        </label>
        <input type="hidden" name="searchable" :value="form.searchable">
        <div v-if="errors.has('searchable')" class="text-danger" v-cloak>@{{ errors.first('searchable') }}</div>
    </div>
</div>

<div class="form-check row">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="visible" type="checkbox" v-model="form.visible" v-validate="''" data-vv-name="visible"  name="visible_fake_element">
        <label class="form-check-label" for="visible">
            {{ trans('admin.field.columns.visible') }}
        </label>
        <input type="hidden" name="visible" :value="form.visible">
        <div v-if="errors.has('visible')" class="text-danger" v-cloak>@{{ errors.first('visible') }}</div>
    </div>
</div>

<div class="form-check row">
    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
        <input class="form-check-input" id="exportable" type="checkbox" v-model="form.exportable" v-validate="''" data-vv-name="exportable"  name="exportable_fake_element">
        <label class="form-check-label" for="exportable">
            {{ trans('admin.field.columns.exportable') }}
        </label>
        <input type="hidden" name="exportable" :value="form.exportable">
        <div v-if="errors.has('exportable')" class="text-danger" v-cloak>@{{ errors.first('exportable') }}</div>
    </div>
</div>

<div v-if="parseInt(form.field_type_id) === {{ $fieldTypes->where('name', 'select')->first()->id }}"
     class="form-group row align-items-center">
    <label class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('codersstudio/form-creator::form.options') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <template v-for="(option, key) in form.options">
            <div class="row mb-3">
                <div class="col-4">
                    <input type="text" v-model="option.value" :name="'value_' + key" v-validate="''" class="form-control">
                    <div v-if="errors.has('value_' + key)" class="form-control-feedback form-text text-danger" v-cloak>@{{ errors.first('value_' + key)
                        }}
                    </div>
                </div>
                <div class="col-7">
                    <input type="text" v-model="option.text" :name="'text_' + key" v-validate="''" class="form-control">
                    <div v-if="errors.has('text_' + key)" class="form-control-feedback form-text text-danger" v-cloak>@{{ errors.first('text_' + key)
                        }}
                    </div>
                </div>
                <div class="col-1 text-center">
                    <a @click="removeOption(key)" href="#" title="Удалить" class="btn btn-danger m-r-2"><i
                                class="fa fa-trash"></i></a>
                </div>
            </div>
        </template>
        <div class="row">
            <div class="col-12">
                <a href="#" @click.prevent="addOption()" class="btn btn-success mb-3"><i
                            class="fa fa-plus"></i> @lang('codersstudio/form-creator::form.option')</a>
            </div>
        </div>

    </div>
</div>


