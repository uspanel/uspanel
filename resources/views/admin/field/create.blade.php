@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.field.actions.create'))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>{{ trans('admin.field.actions.create') }}</span></h2>
    </div>

    <field-form
        :action="'{{ url('/forms/' . $form->id . '/fields') }}'"
        inline-template>
        
        <div class="card">
            <div class="card-body">
                @include('admin.field.components.form-elements')
                <div class="text-right">
                    <button class="btn btn-primary" @click="save">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                </div>
            </div>
        </div>
        

    </field-form>


@endsection