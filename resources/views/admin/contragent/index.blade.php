@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.contragent.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0">
                <span>{{ trans('admin.contragent.actions.index') }}</span>
            </h2>
        </div>
        @can('crud.contragent.create')
        <div class="col-auto">
            <a class="btn btn-primary" href="{{ url('/admin/contragents/create') }}" role="button">
                <i class="fa fa-plus"></i>
                {{ trans('admin.contragent.actions.create') }}
            </a>
        </div>
        @endcan
    </div>
    <contragent-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('/admin/contragents') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <form @submit.prevent="">
                            <div class="row justify-content-md-between">
                                <div class="col col-lg-7 col-xl-5 form-group">
                                    <div class="input-group">
                                        <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                        <span class="input-group-append">
                                            <button type="button" class="btn btn-primary" @click="filter('search', search)"><i class="fa fa-search"></i>&nbsp; {{ trans('brackets/admin-ui::admin.btn.search') }}</button>
                                        </span>
                                    </div>
                                </div>

                                <div class="col-sm-auto form-group ">
                                    <select class="form-control" v-model="pagination.state.per_page">

                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>

                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th is='sortable' :column="'id'">{{ trans('admin.contragent.columns.id') }}</th>
                                        <th is='sortable' :column="'name'">{{ trans('admin.contragent.columns.name') }}</th>
                                        <th is='sortable' :column="'investing_amount'">{{ trans('admin.contragent.columns.investing_amount') }}</th>
                                        <th is='sortable' :column="'user_id'">{{ trans('admin.contragent.columns.user_id') }}</th>
                                        <th is='sortable' :column="'country'">{{ trans('admin.contragent.columns.country') }}</th>
                                        <th is='sortable' :column="'roi'">{{ trans('admin.contragent.columns.roi') }}</th>
                                        <th is='sortable' :column="'roi'">{{ trans('admin.contragent.columns.media') }}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection">
                                        <td>@{{ item.id }}</td>
                                        <td>@{{ item.name }}</td>
                                        <td>@{{ item.investing_amount }}</td>
                                        <td>@{{ item.user_id }}</td>
                                        <td>@{{ item.country }}</td>
                                        <td>@{{ item.roi }}</td>
                                        <td>
                                            <b-button v-b-modal="'modal-' + item.id">@{{ _.get(item, 'media.length', 0) }}</b-button>
                                            <b-modal :id="'modal-' + item.id" title="{{ trans('admin.contragent.columns.media') }}">
                                                <table class="table">
                                                    <tr v-for="media of item.media">
                                                        <td>@{{ media.custom_properties.name }}</td>
                                                        <td>
                                                            <a :href="media.url" class="btn btn-sm btn-primary">
                                                                <i class="fa fa-download"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </b-modal>
                                        </td>

                                        <td class="nowrap">
                                            @can('crud.contragent.edit')
                                            <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                            @endcan
                                            @can('crud.contragent.delete')
                                            <button
                                            @click="deleteItem(item.resource_url)"
                                            class="btn btn-sm btn-danger"
                                            title="Delete">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                            @endcan
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center" v-if="!collection.length > 0">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </contragent-listing>

@endsection
