<div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': this.fields.name && this.fields.name.valid }">
    <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.contragent.columns.business_processes') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <multiselect
            v-model="selected_business_processes"
            track-by="id"
            label="title"
            placeholder="@lang('admin.contragent.columns.business_processes')"
            :options="business_processes"
            :searchable="true"
            :multiple="true"
            :close-on-select="false"
            :allow-empty="true"></multiselect>
        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': this.fields.name && this.fields.name.valid }">
    <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.contragent.columns.name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.name" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('name'), 'form-control-success': this.fields.name && this.fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.contragent.columns.name') }}">
        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('investing_amount'), 'has-success': this.fields.investing_amount && this.fields.investing_amount.valid }">
    <label for="investing_amount" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.contragent.columns.investing_amount') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="number" v-model="form.investing_amount" v-validate="'required|decimal'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('investing_amount'), 'form-control-success': this.fields.investing_amount && this.fields.investing_amount.valid}" id="investing_amount" name="investing_amount" placeholder="{{ trans('admin.contragent.columns.investing_amount') }}">
        <div v-if="errors.has('investing_amount')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('investing_amount') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('country'), 'has-success': this.fields.country && this.fields.country.valid }">
    <label for="country" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.contragent.columns.country') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.country" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('country'), 'form-control-success': this.fields.country && this.fields.country.valid}" id="country" name="country" placeholder="{{ trans('admin.contragent.columns.country') }}">
        <div v-if="errors.has('country')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('country') }}</div>
    </div>
</div>

<div class="form-group row align-items-center" :class="{'has-danger': errors.has('roi'), 'has-success': this.fields.roi && this.fields.roi.valid }">
    <label for="roi" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.contragent.columns.roi') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="number" v-model="form.roi" v-validate="''" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('roi'), 'form-control-success': this.fields.roi && this.fields.roi.valid}" id="roi" name="roi" placeholder="{{ trans('admin.contragent.columns.roi') }}">
        <div v-if="errors.has('roi')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('roi') }}</div>
    </div>
</div>

@include('brackets/admin-ui::admin.includes.media-uploader', [
    'mediaCollection' => app(App\Contragent::class)->getMediaCollection('file_collection'),
    'label' => 'Files',
    'media' => ($contragent ?? app(App\Contragent::class))->getThumbs200ForCollection('file_collection')
])


