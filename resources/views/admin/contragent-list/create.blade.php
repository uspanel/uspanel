@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.contragent-list.actions.create'))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>{{ trans('admin.contragent-list.actions.create') }}</span></h2>
    </div>

    <contragent-list-form
        :action="'{{ url('/admin/contragent-lists') }}'"
        :contragents="{{ $contragents->toJson() }}"

        inline-template>

        <div class="card">
            <div class="card-body">
                <form method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>
                    @include('admin.contragent-list.components.form-elements')
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>


    </contragent-list-form>


@endsection
