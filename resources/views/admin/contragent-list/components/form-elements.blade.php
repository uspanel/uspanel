<div class="form-group row align-items-center" :class="{'has-danger': errors.has('name'), 'has-success': this.fields.name && this.fields.name.valid }">
    <label for="name" class="col-form-label text-md-right" :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.contragent-list.columns.name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.name" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('name'), 'form-control-success': this.fields.name && this.fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.contragent-list.columns.name') }}">
        <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
    </div>
</div>
<div class="form-group row align-items-center">
    <label class="col-form-label text-md-right col-md-2">
        {{ trans('admin.contragent-list.columns.contragents') }}
    </label>
    <div class="col-md-9 col-xl-8">
        <multiselect v-model="form.contragents" :multiple="true" :close-on-select="false" :options="contragents" label="name" track-by="id"></multiselect>
        <div v-if="errors.has('contragents')" class="text-danger" v-cloak>@{{ errors.first('contragents') }}</div>
    </div>
</div>

