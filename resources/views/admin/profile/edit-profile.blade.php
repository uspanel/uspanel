@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.admin-user.actions.edit_profile'))

@section('body')
    <profile-edit-profile-form
        :action="'{{ url('admin/profile') }}'"
        :data="{{ $adminUser->toJson() }}"

        inline-template>
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col-md-4">
                        <div class="avatar-upload">
                            @include('brackets/admin-ui::admin.includes.avatar-uploader', [
                                'mediaCollection' => app(\Brackets\AdminAuth\Models\AdminUser::class)->getMediaCollection('avatar'),
                                'media' => $adminUser->getThumbs200ForCollection('avatar')
                            ])
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="first_name">@lang('admin.admin-user.columns.first_name')</label>
                            <input type="text" v-model="form.first_name" class="form-control">
                            <div v-if="fails" class="text-danger">
                                @{{ _.head(fails.first_name) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="last_name">@lang('admin.admin-user.columns.last_name')</label>
                            <input type="text" v-model="form.last_name" class="form-control">
                            <div v-if="fails" class="text-danger">
                                @{{ _.head(fails.last_name) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email">@lang('admin.admin-user.columns.email')</label>
                            <input type="email" v-model="form.email" class="form-control">
                            <div v-if="fails" class="text-danger">
                                @{{ _.head(fails.email) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone_number">@lang('admin.admin-user.columns.phone_number')</label>
                            <the-mask :masked="true" :mask="'+' + {{env('PHONE_PREFIX')}} + ' (###) ###-####'" type="text" v-model="form.phone_number" class="form-control"></the-mask>
                            <div v-if="fails" class="text-danger">
                                @{{ _.head(fails.phone_number) }}
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="text-right">
                    <button type="submit" class="btn btn-primary" @click="update">
                        <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                        {{ trans('brackets/admin-ui::admin.btn.save') }}
                    </button>
                </div>
            </div>
        </div>
    </profile-edit-profile-form>
@endsection
