@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.businessprocess.actions.edit', ['name' => $businessProcess->id]))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0">
            <span>
                {{ trans('admin.businessprocess.actions.edit', ['name' => $businessProcess->id]) }}
            </span>
        </h2>
    </div>
    <businessprocess-form
        :business-process-types="{{ $business_process_types->toJson() }}"
        :action="'{{ $businessProcess->resource_url }}'"
        :data="{{ json_encode($businessProcessResource) }}"
        inline-template>
        @include('admin.businessprocess.components.form-elements')
    </businessprocess-form>

@endsection
