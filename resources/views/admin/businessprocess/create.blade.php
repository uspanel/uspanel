@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.businessprocess.actions.create'))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>{{ trans('admin.businessprocess.actions.create') }}</span></h2>
    </div>

    <businessprocess-form
        :business-process-types="{{ $business_process_types->toJson() }}"
        :action="'{{ url('/admin/businessprocesses') }}'"
        inline-template>
        @include('admin.businessprocess.components.form-elements')
    </businessprocess-form>


@endsection
