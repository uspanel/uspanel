<div class="row">
    <div class="col-12">
        <div class="card mb-3">
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-md-2 col-sm-3 control-label">@lang('admin.businessprocess.columns.business_process_type_id')</label>
                    <div class="col-md-10 col-sm-9">
                        <multiselect
                            v-model="business_process_type"
                            track-by="id"
                            label="title"
                            placeholder="@lang('admin.businessprocess.columns.business_process_type_id')"
                            :options="business_process_types"
                            :searchable="false"
                            :allow-empty="false"></multiselect>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-2 col-sm-3 control-label">@lang('admin.businessprocess.columns.title')</label>
                    <div class="col-md-10 col-sm-9">
                        <input type="text" class="form-control" v-model="title">
                    </div>
                </div>
                <div class="form-check row">
                    <div class="ml-md-auto" :class="isFormLocalized ? 'col-md-8' : 'col-md-10'">
                        <input class="form-check-input" id="active" type="checkbox" name="default_fake_element"
                               v-model="active">
                        <label class="form-check-label" for="active">
                            {{ trans('admin.businessprocess.columns.active') }}
                        </label>
                        <input type="hidden" name="active" :value="active">
                    </div>
                </div>

                <div class="alert alert-danger" v-if="errors.any()">
                    <div v-for="item in errors.all()">
                        @{{ item }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-xl-3">
        <div class="card">
            <div class="card-body">
                <div class="input-group">
                    <input type="text" class="form-control" v-model="searchFor.event">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button">
                            <i class="zmdi zmdi-search"></i>
                        </button>
                    </div>
                </div>
                <draggable class="list-group my-3" :list="events"
                           :group="{ name: 'process', pull: 'clone', put: false }" :clone="cloneEvent">
                    <a v-for="item in filter(events, searchFor.event)" href="#"
                       class="list-group-item list-group-item-action">
                        @{{ item.title }}
                    </a>
                </draggable>
            </div>
        </div>
    </div>
    <div class="col">
        <draggable :list="business_process_events" group="process" class="row">
            <div class="col-12" v-if="_.isEmpty(business_process_events)">
                <div class="card">
                    <div class="card-body">
                        @lang('admin.businessprocess.put_event_here')
                    </div>
                </div>
            </div>
            <template v-for="(business_process_event, business_process_event_index) in business_process_events">
                <business-process-event
                    :key="business_process_event_index"
                    :business_process_event="business_process_event"
                    :business_process_event_index="business_process_event_index"
                    v-on:delete-action="deleteAction"
                    v-on:delete-event="deleteEvent"/>
            </template>
        </draggable>
        <div class="text-right">
            <button class="btn btn-primary" @click="save">
                <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                {{ trans('brackets/admin-ui::admin.btn.save') }}
            </button>
        </div>
    </div>
    <modal name="addAction" height="80%" @before-open="beforeOpen" @before-close="beforeClose">
        <div class="modal-content d-flex h-100 flex-column">
            <div class="modal-header">
                <div>
                    @lang('admin.businessprocess.add_action')
                </div>
                <button type="button" class="close" @click="$modal.hide('addAction')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body h-100">
                <div class="input-group">
                    <input type="text" class="form-control" v-model="searchFor.action">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button">
                            <i class="zmdi zmdi-search"></i>
                        </button>
                    </div>
                </div>
                <div class="list-group mt-3">
                    <a v-for="item in filter(actions, searchFor.action)" href="#"
                       class="list-group-item list-group-item-action" @click="addAction(item)"
                       :class="{ 'list-group-item-success': item.id == action_business_process_events.action.id }">
                        @{{ item.title }}
                    </a>
                </div>
                <hr>
                <mail-templates :selected="action_business_process_events.mail_template" ref="templates"
                                v-if="action_business_process_events.action.name == 'App\\Libs\\Actions\\NotifyByEmail'"></mail-templates>
                <div class="alert alert-danger mt-3" v-if="errors.has('mail_template')">
                    @{{ errors.first('mail_template') }}
                </div>
                <users
                    :selected="{ users: action_business_process_events.users, roles: action_business_process_events.roles }"
                    ref="users"></users>
                <div class="alert alert-danger mt-3" v-if="errors.has('users')">
                    @{{ errors.first('users') }}
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" @click="saveAction">
                    @lang('admin.save')
                </button>
            </div>
        </div>
    </modal>
</div>
