@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.businessprocess.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0">
                <span>{{ trans('admin.businessprocess.actions.index') }}</span>
            </h2>
        </div>
        <div class="col-auto">
            @can('crud.business-process.create')
                <a class="btn btn-primary" href="{{ url('/admin/businessprocesses/create') }}" role="button">
                    <i class="fa fa-plus"></i>
                    {{ trans('admin.businessprocess.actions.create') }}
                </a>
            @endcan
        </div>
    </div>
    <businessprocess-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('/admin/businessprocesses') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <form @submit.prevent="">
                            <div class="row justify-content-md-between">
                                <div class="col-md-3 form-group">
                                    <label for="">{{ trans('brackets/admin-ui::admin.placeholder.search') }}</label>
                                    <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" />
                                </div>
                                <div class="col-auto align-self-end">
                                    <button type="button" class="btn btn-primary mb-3" @click="filter('search', search)">
                                        <i class="fa fa-search"></i>
                                        {{ trans('brackets/admin-ui::admin.btn.search') }}
                                    </button>
                                </div>
                            </div>
                            <div class="row justify-content-end">
                                <div class="col-auto form-group">
                                    <select class="form-control" v-model="pagination.state.per_page">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th is='sortable' :column="'id'">@lang('admin.businessprocess.columns.id')</th>
                                        <th>@lang('admin.businessprocess.columns.title')</th>
                                        <th>@lang('admin.businessprocess.columns.created_at')</th>
                                        <th>@lang('admin.businessprocess.columns.active')</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection">
                                        <td>@{{ item.id }}</td>
                                        <td>@{{ item.title }}</td>
                                        <td>@{{ item.created_at }}</td>
                                        <td>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" :id="'active' + item.id" @change="setActive(item)" v-model="item.active">
                                                <label class="custom-control-label" :for="'active' + item.id"></label>
                                            </div>
                                        </td>
                                        <td class="nowrap">
                                            <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                            @can('crud.business-process.create')
                                                <button
                                                    @click="deleteItem(item.resource_url)"
                                                    class="btn btn-sm btn-danger"
                                                    title="Delete">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            @endcan
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center" v-if="!collection.length > 0">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </businessprocess-listing>

@endsection
