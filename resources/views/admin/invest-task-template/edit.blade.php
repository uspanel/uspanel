@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.invest-task-template.actions.edit', ['name' => $investTaskTemplate->name]))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0">
            <span>
                {{ trans('admin.invest-task-template.actions.edit', ['name' => $investTaskTemplate->name]) }}
            </span>
        </h2>
    </div>
    <invest-task-template-form
        :action="'{{ $investTaskTemplate->resource_url }}'"
        :data="{{ $investTaskTemplate->toJson() }}"
        :investors="{{ $investors->toJson() }}"
        :contragents="{{ $contragents->toJson() }}"
        :managers="{{ $managers->toJson() }}"
        :investor_lists="{{ $investor_lists->toJson() }}"
        :contragent_lists="{{ $contragent_lists->toJson() }}"
        :mailtemplates="{{ $mailTemplates->toJson() }}"
        inline-template>
            <div class="card">
            <div class="card-body">
                <form method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>
                    @include('admin.invest-task-template.components.form-elements')
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>

</invest-task-template-form>

@endsection
