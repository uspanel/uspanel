@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.invest-task-template.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0">
                <span>{{ trans('admin.invest-task-template.actions.index') }}</span>
            </h2>
        </div>
        <div class="col-auto">
            @can('crud.invest-task.create')
                <a class="btn btn-primary" href="{{ url('/admin/invest-task-templates/create') }}" role="button">
                    <i class="fa fa-plus"></i>
                    {{ trans('admin.invest-task-template.actions.create') }}
                </a>
            @endcan
        </div>
    </div>
    <invest-task-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('/admin/invest-task-templates') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>

                        <div class="row align-items-end">
                            <div class="col">
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">{{ trans('brackets/admin-ui::admin.placeholder.search') }}</label>
                                            <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                        </div>
                                    </div>

                                    <div class="col-sm-auto">
                                        <div class="form-group">
                                            <label for="">{{ trans('admin.invest-task.columns.per_page') }}</label>
                                            <select class="form-control" v-model="pagination.state.per_page">
                                                <option value="10">10</option>
                                                <option value="25">25</option>
                                                <option value="100">100</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-auto align-self-end">
                                        <button type="button" class="btn btn-primary mb-3" @click="filter('search', search)">
                                            <i class="fa fa-search"></i>
                                            {{ trans('brackets/admin-ui::admin.btn.search') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-hover table-listing">
                                <thead>
                                <tr>
                                    <th is='sortable' :column="'id'">{{ trans('admin.invest-task.columns.id') }}</th>
                                    <th is='sortable' :column="'name'">{{ trans('admin.invest-task.columns.name') }}</th>
                                    <th is='sortable' :column="'theme'">{{ trans('admin.invest-task.columns.theme') }}</th>
                                    <th is='sortable' :column="'text'">{{ trans('admin.invest-task.columns.text') }}</th>
                                    <th is='sortable' :column="'investor_id'">{{ trans('admin.invest-task.columns.investors') }}</th>
                                    <th is='sortable' :column="'contragent_id'">{{ trans('admin.invest-task.columns.contragents') }}</th>
                                    <th is='sortable' :column="'manager_id'">{{ trans('admin.invest-task.columns.manager_id') }}</th>

                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(item, index) in collection">
                                    <td>@{{ item.id }}</td>
                                    <td>@{{ item.name }}</td>
                                    <td>@{{ item.theme }}</td>
                                    <td v-html="item.text"></td>
                                    <td>
                                        <div v-for="investor in item.investors">
                                            @{{ investor.name }}
                                        </div>
                                        <div v-if="!item.investors">-</div>
                                    </td>
                                    <td>
                                        <div v-for="contragent in item.contragents">
                                            @{{ contragent.name }}
                                        </div>
                                        <div v-if="!item.investors">-</div>
                                    </td>
                                    <td>@{{ item.manager ? item.manager.name : '-' }}</td>

                                    <td class="nowrap">
                                         <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'"
                                           title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <button
                                            @click="deleteItem(item.resource_url)"
                                            class="btn btn-sm btn-danger"
                                            title="Delete">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center" v-if="!collection.length > 0">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </invest-task-listing>

@endsection
