@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.mail.actions.index'))

@section('body')
	<div class="card">
		<div class="card-header">
			@lang('admin.notifications')
		</div>
		<div class="card-body">
			<coders-studio-notifications-index :id="{{ auth()->id() }}"></coders-studio-notifications-index>
		</div>
	</div>
@endsection
