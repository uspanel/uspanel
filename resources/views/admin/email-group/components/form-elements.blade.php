@include('admin.layout.inputs.text', [
    'name' => 'name'
])

@can('crud.email-group.columns.name.write')
     @include('brackets/admin-ui::admin.includes.media-uploader', [
                'mediaCollection' => ( $emailGroup ?? app(App\EmailGroup::class))->getMediaCollection('list'),
                'label' => trans('admin.email-group.columns.csv'),
                'media' => ( $emailGroup ?? app(App\EmailGroup::class))->getThumbs200ForCollection('list')
    ])
@endcan