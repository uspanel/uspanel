@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.warehouse.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0"><span>@lang('admin.warehouse.actions.index')</span></h2>
        </div>
        <div class="col-auto">
            @can('crud.warehouse.create')
            <a class="btn btn-primary" href="{{ url('admin/warehouses/create') }}" role="button">
                <i class="fa fa-plus"></i>
                @lang('admin.warehouse.actions.create')
            </a>
            @endcan
        </div>
    </div>
    <warehouse-listing
            :data="{{ $data->toJson() }}"
            :url="'{{ url('admin/warehouses') }}'"
            :user="{{ $user->toJson() }}"
            inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <div class="row align-items-end">
                            <div class="col">
                                <div class="row">
                                    @include('admin.layout.filters.text', [
                                    'name' => 'name',
                                    ])
                                    @include('admin.layout.filters.text', [
                                    'name' => 'city',
                                    ])
                                    @include('admin.layout.filters.text', [
                                    'name' => 'address',
                                    ])
                                    <div class="col-auto align-self-end">
                                        <button type="button" class="btn btn-primary mb-3" @click="filter('search', search)">
                                            <i class="fa fa-search"></i>
                                            {{ trans('brackets/admin-ui::admin.btn.search') }}
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row justify-content-end">
                            <div class="col-auto form-group">
                                <select class="form-control" v-model="pagination.state.per_page">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="100">100</option>
                                </select>
                            </div>
                        </div>
                        <table class="table table-hover table-listing">
                            <thead>
                                <tr>
                                    <th is='sortable' :column="'id'">{{ trans('admin.warehouse.columns.id') }}</th>
                                    @include('admin.layout.cells.th', ['name' => 'name'])
                                    @include('admin.layout.cells.th', ['name' => 'city'])
                                    @include('admin.layout.cells.th', ['name' => 'address'])
                                    @include('admin.layout.cells.th', ['name' => 'user_id'])
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(item, index) in collection">
                                    <td>@{{ item.id }}</td>
                                    @include('admin.layout.cells.td', ['name' => 'name'])
                                    @include('admin.layout.cells.td', ['name' => 'city'])
                                    @include('admin.layout.cells.td', ['name' => 'address'])
                                    @can('crud.warehouse.columns.user_id.read')
                                    <td> @{{ $attrs['user'][item.user_id] ? $attrs['user'][item.user_id] : '-' }} </td>
                                    @endcan
                                    <td>
                                        <a class="btn btn-sm btn-spinner btn-info" :href="'/admin/warehouses/' + item.id + '/items'" title="{{ trans('admin.item.title') }}" role="button">
                                           <i class="fa fa-list-alt"></i>
                                        </a>
                                        <a class="btn btn-sm btn-spinner btn-info"
                                           :href="item.resource_url + '/edit'"
                                           title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i
                                                    class="fa fa-edit"></i></a>
                                        @can('crud.warehouse.delete')
                                            <button
                                            @click="deleteItem(item.resource_url)"
                                            class="btn btn-sm btn-danger"
                                            title="{{ trans('brackets/admin-ui::admin.btn.delete') }}">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endcan
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="row" v-if="pagination.state.total > 0">
                            <div class="col-sm">
                                <span class="pagination-caption">{{ trans('brackets/admin-ui::admin.pagination.overview') }}</span>
                            </div>
                            <div class="col-sm-auto">
                                <pagination></pagination>
                            </div>
                        </div>
                        <div class="text-center" v-if="_.isEmpty(collection)">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </warehouse-listing>

@endsection
