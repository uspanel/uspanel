@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.warehouse.actions.create'))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>@lang('admin.warehouse.actions.create')</span></h2>
    </div>
    <warehouse-form
        :action="'{{ url('admin/warehouses') }}'"
        :user="{{$user->toJson()}}"
        inline-template>
        <div class="card">
            <div class="card-body">
                <form method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>
                    @include('admin.warehouse.components.form-elements', [
                        'viewName' => 'warehouse'
                    ])
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </warehouse-form>

@endsection
