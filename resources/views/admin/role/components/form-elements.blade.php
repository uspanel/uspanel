<div class="card">
    <div class="card-body">
        @if(Auth::user()->hasPermissionTo('crud.role.columns.name.write'))
            <div class="form-group" :class="{'has-danger': errors.has('name'), 'has-success': this.fields.name && this.fields.name.valid }">
                <label for="name">{{ trans('admin.role.columns.name') }}</label>
                <input type="text" v-model="form.name" v-validate="'required'" @input="validate($event)" class="form-control" :class="{'form-control-danger': errors.has('name'), 'form-control-success': this.fields.name && this.fields.name.valid}" id="name" name="name" placeholder="{{ trans('admin.role.columns.name') }}">
                <div v-if="errors.has('name')" class="form-control-feedback form-text" v-cloak>@{{ errors.first('name') }}</div>
            </div>
        @endif
    </div>
</div>

<div class="row">
    <template v-for="(permission, name) in permissions">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    @{{ translate('admin.' + name + '.title') }}
                </div>
                <div class="card-body">
                    <div class="row" :key="name">
                        <div class="form-group col-md-3">
                            <div class="custom-control custom-checkbox w-50">
                                <input v-model="form.permission_ids" type="checkbox" class="custom-control-input" :value="permission.view" :id="'view-' + permission.view">
                                <label class="custom-control-label" :for="'view-' + permission.view">@lang('admin.role.view')</label>
                            </div>
                        </div>
                        <div v-if="showBlock(permission.view)" class="form-group col-md-3">
                            <div class="custom-control custom-checkbox w-50">
                                <input v-model="form.permission_ids" type="checkbox" class="custom-control-input" :value="permission.create" :id="'view-' + permission.create">
                                <label class="custom-control-label" :for="'view-' + permission.create">@lang('admin.role.create')</label>
                            </div>
                        </div>
                        <div v-if="showBlock(permission.view)" class="form-group col-md-3">
                            <div class="custom-control custom-checkbox w-50">
                                <input v-model="form.permission_ids" type="checkbox" class="custom-control-input" :value="permission.edit" :id="'view-' + permission.edit">
                                <label class="custom-control-label" :for="'view-' + permission.edit">@lang('admin.role.edit')</label>
                            </div>
                        </div>
                        <div v-if="showBlock(permission.view)" class="form-group col-md-3">
                            <div class="custom-control custom-checkbox w-50">
                                <input v-model="form.permission_ids" type="checkbox" class="custom-control-input" :value="permission.delete" :id="'view-' + permission.delete">
                                <label class="custom-control-label" :for="'view-' + permission.delete">@lang('admin.role.delete')</label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <table v-if="showBlock(permission.view)" class="table">
                        <thead>
                        <tr>
                            <th scope="col" class="w-50">@lang('admin.role.column')</th>
                            <th scope="col" class="w-25">
                                <div class="custom-control custom-checkbox">
                                    <input :checked="checkedAllCheckbox(name, 'read')" @change.prevent="checkAll(name, 'read', $event.target.checked)" type="checkbox" class="custom-control-input" :id="name + '-read'">
                                    <label class="custom-control-label" :for="name + '-read'">@lang('admin.role.read')</label>
                                </div>
                            </th>
                            <th scope="col" class="w-25">
                                <div class="custom-control custom-checkbox">
                                    <input :checked="checkedAllCheckbox(name, 'write')" @change.prevent="checkAll(name, 'write', $event.target.checked)" type="checkbox" class="custom-control-input" :id="name + '-write'">
                                    <label class="custom-control-label" :for="name + '-write'">@lang('admin.role.write')</label>
                                </div>
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <template v-for="(column, columnName) in permission.columns">
                            <tr :key="columnName">
                                <th scope="row">@{{ translate('admin.' + name + '.columns.' + columnName) }}</th>
                                <template v-for="(right, rightName) in column">
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input v-model="form.permission_ids" type="checkbox" class="custom-control-input" :value="right" :id="'right-' + right">
                                            <label class="custom-control-label" :for="'right-' + right">&nbsp;</label>
                                        </div>
                                    </td>
                                    <template v-if="2 - _.size(column) > 0">
                                        <td v-for="item in 2 - _.size(column)"></td>
                                    </template>
                                </template>
                            </tr>
                        </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </template>
</div>
