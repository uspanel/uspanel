@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.role.actions.create'))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>@lang('admin.role.actions.create')</span></h2>
    </div>

    <role-form
        :action="'{{ url('admin/roles') }}'"
        :permissions="{{json_encode($permissions)}}"
        inline-template>
        <div class="card">
            <div class="card-body">
                <form method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>
                    @include('admin.role.components.form-elements')
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </role-form>

@endsection