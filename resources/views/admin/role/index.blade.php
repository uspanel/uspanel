@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.role.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0"><span>@lang('admin.role.actions.index')</span></h2>
        </div>
        <div class="col-auto">
            <a class="btn btn-primary" href="{{ url('admin/roles/create') }}" role="button">
                <i class="fa fa-plus"></i>
                @lang('admin.role.actions.create')
            </a>
        </div>
    </div>
    <role-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('admin/roles') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <div class="row align-items-end">
                            <div class="col">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">@lang('admin.role.columns.name')</label>
                                            <input v-model="filters.name" type="text" placeholder="@lang('admin.role.columns.name')" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-auto align-self-end">
                                        <button type="button" class="btn btn-primary mb-3" @click="filter('search', search)">
                                            <i class="fa fa-search"></i>
                                            {{ trans('brackets/admin-ui::admin.btn.search') }}
                                        </button>
                                    </div>
                                </div>
                                <div class="row justify-content-end">
                                    <div class="col-auto form-group">
                                        <select class="form-control" v-model="pagination.state.per_page">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <table class="table table-hover table-listing">
                            <thead>
                                <tr>
                                    <th is='sortable' :column="'id'">{{ trans('admin.role.columns.id') }}</th>
                                    @if(Auth::user()->hasPermissionTo('crud.role.columns.name.read'))
                                        <th is='sortable' :column="'name'">{{ trans('admin.role.columns.name') }}</th>
                                    @endif
                                    <th>{{ trans('admin.role.columns.created_at') }}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(item, index) in collection">
                                    <td>@{{ item.id }}</td>
                                    @if(Auth::user()->hasPermissionTo('crud.role.columns.name.read'))
                                        <td>@{{ item.name }}</td>
                                    @endif
                                    <td>@{{ item.created_at }}</td>
                                    <td>
                                        <a class="btn btn-sm btn-spinner btn-info" :href="'{{url('admin/roles')}}/' + item.id + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                        @if(Auth::user()->hasPermissionTo('crud.role.delete'))
                                            <button
                                            @click="deleteItem(item.resource_url)"
                                            class="btn btn-sm btn-danger"
                                            title="{{ trans('brackets/admin-ui::admin.btn.delete') }}">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="text-center" v-if="_.isEmpty(collection)">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </role-listing>

@endsection
