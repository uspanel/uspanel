@extends('brackets/admin-ui::admin.layout.investor.default')

@section('title', trans('admin.investor.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0">
                <span>@lang('admin.investor.title')</span>
            </h2>
        </div>
    </div>

    <div>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <div class="row my-3 pl-2">
                            <button type="button" class="btn btn-info btn-min-width height-60">@lang('admin.investor.buttons.add_task')</button>
                            <button type="button" class="btn btn-info btn-min-width height-60">@lang('admin.investor.buttons.add_investors')</button>
                            <button type="button" class="btn btn-info btn-min-width height-60">@lang('admin.investor.buttons.add_invest_project')</button>
                            <button type="button" class="btn btn-info btn-min-width height-60">@lang('admin.investor.buttons.send_mail_template')</button>
                            <button type="button" class="btn btn-info btn-min-width height-60">@lang('admin.investor.buttons.remove')</button>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover table-listing">
                                <thead>
                                <tr>
                                    <th>@lang('admin.investor.columns.name')</th>
                                    <th>@lang('admin.investor.columns.email')</th>
                                    <th>@lang('admin.investor.columns.phones')</th>
                                    <th>@lang('admin.investor.columns.address')</th>
                                    <th>@lang('admin.investor.columns.last_activity')</th>
                                    <th>@lang('admin.investor.columns.last_mail_template')</th>
                                    <th>@lang('admin.investor.columns.who_manager')</th>
                                    <th>@lang('admin.investor.columns.project')</th>
                                    <th>@lang('admin.investor.columns.when_join')</th>
                                    <th>@lang('admin.investor.columns.chat')</th>
                                    <th>@lang('admin.investor.columns.sms')</th>
                                    <th>@lang('admin.investor.columns.info')</th>
                                    <th>@lang('admin.investor.columns.call_support_comment')</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td class="nowrap">
                                        <a href="" title="Edit" role="button" class="btn btn-sm btn-spinner btn-info"><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<style scoped>
    .btn-min-width {
        min-width: 128px;
    }
    .height-60 {
        height: 60px;
    }
</style>
