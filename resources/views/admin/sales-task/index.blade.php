@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.sales-task.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0">
                <span>{{ trans('admin.sales-task.actions.index') }}</span>
            </h2>
        </div>
        <div class="col-auto">
            <a class="btn btn-primary" href="{{ url('/admin/sales-tasks/create') }}" role="button">
                <i class="fa fa-plus"></i>
                {{ trans('admin.sales-task.actions.create') }}
            </a>
        </div>
    </div>
    <sales-task-listing
        :warehouses="{{$warehouses->toJson()}}"
        :data="{{ $data->toJson() }}"
        :users="{{ $users->toJson() }}"
        :managers="{{ $managers->toJson() }}"
        :carriers="{{ $carriers->toJson() }}"
        :employees="{{$employees->toJson()}}"
        :url="'{{ url('/admin/sales-tasks') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <div class="row align-items-end">
                            <div class="col">
                                <div class="row">
                                    @include('admin.layout.filters.text', [
                                    'name' => 'id',
                                    ])
                                    @include('admin.layout.filters.select', [
                                    'name' => 'warehouse_id',
                                    'arrayName' => 'warehouses',
                                    ])
                                    @include('admin.layout.filters.date', [
                                    'name' => 'date',
                                    ])
                                    @include('admin.layout.filters.select', [
                                    'name' => 'carrier_id',
                                    'arrayName' => 'carriers',
                                    ])
                                    @include('admin.layout.filters.select', [
                                    'name' => 'employee_id',
                                    'arrayName' => 'employees',
                                    ])
                                    @include('admin.layout.filters.text', [
                                    'name' => 'track',
                                    ])
                                    @include('admin.layout.filters.text', [
                                    'name' => 'employee_status',
                                    ])
                                    @include('admin.layout.filters.text', [
                                    'name' => 'comment',
                                    ])
                                    @include('admin.layout.filters.select', [
                                    'name' => 'creator_id',
                                    'arrayName' => 'users',
                                    ])
                                    @include('admin.layout.filters.select', [
                                    'name' => 'manager_id',
                                    'arrayName' => 'managers',
                                    ])
                                    <div class="col-auto align-self-end">
                                        <button type="button" class="btn btn-primary mb-3" @click="filter('search', search)">
                                            <i class="fa fa-search"></i>
                                            {{ trans('brackets/admin-ui::admin.btn.search') }}
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <a class="btn btn-primary" @click="createCsv()" role="button">
                            <i class="fa fa-archive"></i>
                            @lang('admin.sales-task.actions.csv')
                        </a>
                        <div class="table-responsive">
                            <table class="table table-hover table-listing">
                                <thead>
                                    <tr>
                                        <th class="nowrap"><input id="CheckAll" type="checkbox" @click="checkAll()"/>{{ trans('admin.task.columns.csv') }}</th>
                                        <th is='sortable' :column="'id'">{{ trans('admin.sales-task.columns.id') }}</th>
                                        <th is='sortable' :column="'delivered_at'">{{ trans('admin.sales-task.columns.delivered_at') }}</th>
                                        <th is='sortable' :column="'warehouse_id'">{{ trans('admin.sales-task.columns.warehouse_id') }}</th>
                                        <th is='sortable' :column="'carrier_id'">{{ trans('admin.sales-task.columns.carrier_id') }}</th>
                                        <th is='sortable' :column="'track'">{{ trans('admin.sales-task.columns.track') }}</th>
                                        <th is='sortable' :column="'sku'">{{ trans('admin.sales-task.columns.sku') }}</th>
{{--                                        <th is='sortable' :column="'employee_status_id'">{{ trans('admin.sales-task.columns.employee_status_id') }}</th>--}}
                                        <th is='sortable' :column="'comment'">{{ trans('admin.sales-task.columns.comment') }}</th>
                                        <th is='sortable' :column="'creator_id'">{{ trans('admin.sales-task.columns.creator_id') }}</th>
                                        <th is='sortable' :column="'manager_id'">{{ trans('admin.sales-task.columns.manager_id') }}</th>
                                        <th is='sortable' :column="'employee_id'">{{ trans('admin.sales-task.columns.employee_id') }}</th>
                                        <th is='sortable' :column="'sales_status_id'">{{ trans('admin.sales-task.columns.sales_status_id') }}</th>
                                        {{-- @include('admin.layout.cells.th', ['name' => 'label', 'notSortable' => true])
                                        @include('admin.layout.cells.th', ['name' => 'barcode', 'notSortable' => true]) --}}
                                        @include('admin.layout.cells.th', ['name' => 'receipt', 'notSortable' => true])
                                        @include('admin.layout.cells.th', ['name' => 'invoice', 'notSortable' => true])
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="(item, index) in collection">
                                        <td><input class="csv" type="checkbox" :value="item.id" value/></td>
                                        <td>@{{ item.id }}</td>
                                        <td>@{{ item.delivered_at | datetime }}</td>
                                        <td>
                                            <template v-if="item.warehouse">
                                                @{{ item.warehouse.name }}
                                            </template>
                                        </td>
                                        <td>@{{ item.carrier.name }}</td>
                                        <td>
                                            <div>
                                                @{{ item.track }}
                                            </div>
                                            <div v-if="item.track_url">
                                                <a :href="item.track_url" target="_blank">@lang('admin.track_on_site')</a>
                                            </div>
                                        </td>
                                        <td>@{{ item.sku }}</td>
{{--                                        <td>@{{ item.employee_status_id }}</td>--}}
                                        <td>@{{ item.comment }}</td>
                                        <td>@{{ item.creator.name }}</td>
                                        <td>@{{ item.manager.name }}</td>
                                        <td>
                                            <template v-if="item.employee">
                                                @{{ item.employee.user.name }}
                                            </template>
                                        </td>
                                        <td>@{{ item.sales_status.title }}</td>
                                        {{-- @include('admin.layout.cells.file', ['name' => 'label']) --}}
                                        {{-- @include('admin.layout.cells.file', ['name' => 'barcode']) --}}
                                        @include('admin.layout.cells.file', ['name' => 'receipt'])
                                        @include('admin.layout.cells.file', ['name' => 'invoice'])
                                        <td class="nowrap">
                                            <a class="btn btn-success btn-sm" :href="'/admin/sales-tasks/' + item.id">
                                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                            </a>
                                            <a v-if="!item.delivered_at" class="btn btn-sm btn-spinner btn-info" @click="forceDelivered(item.id)" href="#" title="{{ trans('admin.force_delivered') }}" role="button"><i class="fas fa-truck"></i></a>
                                            <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                            <button
                                            @click="deleteItem(item.resource_url)"
                                            class="btn btn-sm btn-danger"
                                            title="Delete">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="text-center" v-if="!collection.length > 0">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
                <modal name="label_upload" height="auto">
                    <div class="modal-content">
                        <div class="modal-header">
                            @lang('admin.upload_file')
                        </div>
                        <div class="modal-body">
                            @include('brackets/admin-ui::admin.includes.media-uploader', [
                                'mediaCollection' => app(App\Task::class)->getMediaCollection('label'),
                                'label' => trans('admin.task.columns.label'),
                            ])
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" @click="uploadFile('label')">
                                @lang('admin.send')
                            </button>
                        </div>
                    </div>
                </modal>
                <modal name="barcode_upload" height="auto">
                    <div class="modal-content">
                        <div class="modal-header">
                            @lang('admin.upload_file')
                        </div>
                        <div class="modal-body">
                            @include('brackets/admin-ui::admin.includes.media-uploader', [
                                'mediaCollection' => app(App\Task::class)->getMediaCollection('barcode'),
                                'label' => trans('admin.task.columns.barcode'),
                            ])
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" @click="uploadFile('barcode')">
                                @lang('admin.send')
                            </button>
                        </div>
                    </div>
                </modal>
                <modal name="receipt_upload" height="auto">
                    <div class="modal-content">
                        <div class="modal-header">
                            @lang('admin.upload_file')
                        </div>
                        <div class="modal-body">
                            @include('brackets/admin-ui::admin.includes.media-uploader', [
                                'mediaCollection' => app(App\Task::class)->getMediaCollection('receipt'),
                                'label' => trans('admin.task.columns.receipt'),
                            ])
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" @click="uploadFile('receipt')">
                                @lang('admin.send')
                            </button>
                        </div>
                    </div>
                </modal>
                <modal name="invoice_upload" height="auto">
                    <div class="modal-content">
                        <div class="modal-header">
                            @lang('admin.upload_file')
                        </div>
                        <div class="modal-body">
                            @include('brackets/admin-ui::admin.includes.media-uploader', [
                                'mediaCollection' => app(App\Task::class)->getMediaCollection('invoice'),
                                'label' => trans('admin.task.columns.invoice'),
                            ])
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" @click="uploadFile('invoice')">
                                @lang('admin.send')
                            </button>
                        </div>
                    </div>
                </modal>
            </div>

        </div>
    </sales-task-listing>

@endsection
