@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.sales-task.actions.edit', ['name' => $salesTask->id]))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0">
            <span>
                {{ trans('admin.sales-task.actions.edit', ['name' => $salesTask->id]) }}
            </span>
        </h2>
    </div>
    <sales-task-form
        :warehouses="{{ $warehouses }}"
        :action="'{{ $salesTask->resource_url }}'"
        :data="{{ $salesTask->toJson() }}"
        :users="{{ $users->toJson() }}"
        :managers="{{ $managers->toJson() }}"
        :employees="{{ $employees->toJson() }}"
        :carriers="{{ $carriers->toJson() }}"
        :warehouses="{{ $warehouses }}"
        :projects="{{$projects->toJson()}}"
        :courier-statuses="{{ $courierStatuses }}"
        inline-template>
            <div class="card">
            <div class="card-body">
                <form method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>
                    @include('admin.sales-task.components.form-elements',[
                    'viewName' => 'task'
                    ])
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
        
</sales-task-form>

@endsection
