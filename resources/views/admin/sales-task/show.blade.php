@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.sales-task.task_id', ['id' => $salesTask->id]))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>{{ trans('admin.sales-task.task_id', ['id' => $salesTask->id]) }}</span></h2>
    </div>
    <div class="mb-3">
        <a class="btn btn-light" href="{{ url()->previous() }}">
            @lang('admin.back_to_the_list')
        </a>   
    </div>
    <div class="card">
        <div class="card-body">
            <table class="table table-bordered">
                <tbody>
                    @can('crud.task.columns.warehouse_id.read')
                    <tr>
                        <td class="font-weight-bold">
                            @lang('admin.task.columns.warehouse_id')
                        </td>
                        <td>{{ $salesTask->warehouse->name }}</td>
                    </tr>
                    @endcan

                    @can('crud.task.columns.track.read')
                    <tr>
                        <td class="font-weight-bold">
                            @lang('admin.task.columns.track')
                        </td>
                        <td>{{ $salesTask->track }}</td>
                    </tr>
                    @endcan

                    @can('crud.task.columns.carrier_id.read')
                    <tr>
                        <td class="font-weight-bold">
                            @lang('admin.task.columns.carrier_id')
                        </td>
                        <td>{{ $salesTask->carrier->name }}</td>
                    </tr>
                    @endcan

                    @can('crud.task.columns.delivery_status_id.read')
                    <tr>
                        <td class="font-weight-bold">
                            @lang('admin.task.columns.delivery_status_id')
                        </td>
                        <td>{{ $salesTask->deliveryStatus->name ?? null }}</td>
                    </tr>
                    @endcan

                    @can('crud.task.columns.delivered_at.read')
                    <tr>
                        <td class="font-weight-bold">
                            @lang('admin.task.columns.delivered_at')
                        </td>
                        <td>{{ $salesTask->delivered_at }}</td>
                    </tr>
                    @endcan

                    @can('crud.task.columns.employee_status_id.read')
                    <tr>
                        <td class="font-weight-bold">
                            @lang('admin.task.columns.employee_status_id')
                        </td>
                        <td>{{ $salesTask->employeeStatus ? $salesTask->employeeStatus->name : '-' }}</td>
                    </tr>
                    @endcan

                    @can('crud.task.columns.comment.read')
                    <tr>
                        <td class="font-weight-bold">
                            @lang('admin.task.columns.comment')
                        </td>
                        <td>{{ $salesTask->comment }}</td>
                    </tr>
                    @endcan

                    @can('crud.task.columns.creator_id.read')
                    <tr>
                        <td class="font-weight-bold">
                            @lang('admin.task.columns.creator_id')
                        </td>
                        <td>{{ $salesTask->creator->name }}</td>
                    </tr>
                    @endcan

                    @can('crud.task.columns.manager_id.read')
                    <tr>
                        <td class="font-weight-bold">
                            @lang('admin.task.columns.manager_id')
                        </td>
                        <td>{{ $salesTask->manager ? $salesTask->manager->name : '-' }}</td>
                    </tr>
                    @endcan

                    @can('crud.task.columns.employee_id.read')
                    <tr>
                        <td class="font-weight-bold">
                            @lang('admin.task.columns.employee_id')
                        </td>
                        <td>{{ $salesTask->employee ? $salesTask->employee->user->name : '-' }}</td>
                    </tr>
                    @endcan
                </tbody>
            </table>
            
        </div>
    </div>
    <div class="card">
        <div class="card-header">Items</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                        <th>{{ trans('admin.item.columns.id') }}</th>
                        <th>{{ trans('admin.item.columns.name') }}</th>
                        <th>{{ trans('admin.item.columns.size') }}</th>
                        <th>{{ trans('admin.item.columns.color') }}</th>
                        <th>{{ trans('admin.item.columns.qty') }}</th>
                        <th>{{ trans('admin.item.columns.price') }}</th>
                        <th>{{ trans('admin.item.columns.link') }}</th>
                        <th>{{ trans('admin.item.columns.user_id') }}</th>
                    </thead>
                    <tbody>
                        @foreach($salesTask->items()->withPivot('qty')->get() as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->size }}</td>
                                <td>{{ $item->color }}</td>
                                <td>{{ $item->pivot->qty }}</td>
                                <td>{{ $item->price }}</td>
                                <td><a href="{{ $item->link }}">{{ $item->link }}</a></td>
                                <td>{{ $item->user->name }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
