@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.package.actions.index'))

@section('body')
    <package-listing
        :data="{{ $data->toJson() }}"
        :statuses="{{$statuses->toJson()}}"
        :managers="{{$managers->toJson()}}"
        :carriers="{{$carriers->toJson()}}"
        :employees="{{$employees->toJson()}}"
        :users="{{$users->toJson()}}"
        :url="'{{ url('admin/packages') }}'"
        :def-filters="{{ json_encode($filters) }}"
        inline-template>
        <div>
            <div class="row align-items-center page-heading">
                <div class="col">
                    <h2 class="title mb-0 mb-sm-0"><span>@lang('admin.package.actions.index')</span></h2>
                </div>
                <div class="col-auto">
                    @can('crud.package.create')
                        <a class="btn btn-primary" href="{{ url('admin/packages/create') }}" role="button">
                            <i class="fa fa-plus"></i>
                            @lang('admin.package.actions.create')
                        </a>
                    @endcan
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body" v-cloak>
                            <div class="row align-items-end">
                                <div class="col">
                                    <div class="row">
                                        @include('admin.layout.filters.text', [
                                        'name' => 'id',
                                        ])
                                        @include('admin.layout.filters.date', [
                                        'name' => 'delivered_at',
                                        ])
                                        @include('admin.layout.filters.text', [
                                        'name' => 'track',
                                        ])
                                        @include('admin.layout.filters.select', [
                                        'name' => 'carrier_id',
                                        'arrayName' => 'carriers',
                                        ])
                                        @include('admin.layout.filters.select', [
                                        'name' => 'employee_id',
                                        'arrayName' => 'employees',
                                        ])
                                        @include('admin.layout.filters.text', [
                                        'name' => 'weight',
                                        ])
                                        @include('admin.layout.filters.text', [
                                        'name' => 'name',
                                        ])
                                        @include('admin.layout.filters.select', [
                                        'name' => 'creator_id',
                                        'arrayName' => 'users',
                                        ])
                                        @include('admin.layout.filters.select', [
                                        'name' => 'manager_id',
                                        'arrayName' => 'managers',
                                        ])
                                        @include('admin.layout.filters.select', [
                                        'name' => 'status_id',
                                        'arrayName' => 'statuses',
                                        ])
                                        <div class="col-auto align-self-end">
                                            <button type="button" class="btn btn-primary mb-3" @click="filter('search', search)">
                                                <i class="fa fa-search"></i>
                                                {{ trans('brackets/admin-ui::admin.btn.search') }}
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row justify-content-end">
                                <div class="col-auto form-group">
                                    <select class="form-control" v-model="pagination.state.per_page">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-hover table-listing">
                                    <thead>
                                    <tr>
                                        @include('admin.layout.cells.th', ['name' => 'delivered_at'])
                                        @include('admin.layout.cells.th', ['name' => 'track'])
                                        @include('admin.layout.cells.th', ['name' => 'name'])
                                        @include('admin.layout.cells.th', ['name' => 'carrier_tracking_status', 'notSortable' => true])
                                        @include('admin.layout.cells.th', ['name' => 'weight'])
                                        @include('admin.layout.cells.th', ['name' => 'creator_id'])
                                        @include('admin.layout.cells.th', ['name' => 'manager_id'])
                                        @include('admin.layout.cells.th', ['name' => 'employee_id'])
                                        @include('admin.layout.cells.th', ['name' => 'comment'])
                                        @include('admin.layout.cells.th', ['name' => 'status_id'])
                                        @can('crud.package.columns.description.items.read')
                                            <th>@lang('admin.package.columns.items')</th>
                                        @endcan
                                        @include('admin.layout.cells.th', ['name' => 'invoice', 'notSortable' => true])
                                        <th width="7%"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr v-for="(item, index) in collection">
                                        @can('crud.package.columns.delivered_at.read')
                                            <td class="text-center">
                                                <template v-if="item.delivered_at">
                                                    @{{ item.delivered_at}}
                                                </template>
                                                <template v-else>
                                                    -
                                                </template>
                                            </td>
                                        @endcan
                                        @can('crud.package.columns.track.read')
                                            <td class="text-center">
                                                <template v-if="item.carrier_name">
                                                    @can('crud.package.columns.carrier_id.read')
                                                        <i :class="['d-block', 'fab', 'fa-3x', 'fa-' + item.carrier_name]"></i>
                                                        <div v-if="item.track_url">
                                                            <a :href="item.track_url" target="_blank">@lang('admin.track_on_site')</a>
                                                        </div>
                                                    @endcan
                                                    @{{ item.track}}
                                                </template>
                                                <template v-else>
                                                    -
                                                </template>
                                            </td>
                                        @endcan
                                        @include('admin.layout.cells.td', ['name' => 'name', 'field' => 'name'])
                                        @include('admin.layout.cells.td', ['name' => 'carrier_tracking_status', 'field' => 'last_activity.title'])
                                        @include('admin.layout.cells.td', ['name' => 'weight'])
                                        @include('admin.layout.cells.td', ['name' => 'creator_id', 'field' => 'creator.name'])
                                        @include('admin.layout.cells.td', ['name' => 'manager_id', 'field' => 'manager.name'])
                                        @include('admin.layout.cells.td', ['name' => 'employee_id', 'field' => 'employee.user.name'])
                                        @include('admin.layout.cells.td', ['name' => 'comment'])
                                        @include('admin.layout.cells.td', ['name' => 'status_id', 'field' => 'status.title'])
                                        <td>
                                            <div v-for="it in item.items">
                                                @{{ it.name }}
                                            </div>
                                        </td>
                                        @include('admin.layout.cells.td', ['name' => 'items_count', 'field' => 'items_count'])
                                        @can('crud.package.columns.invoice.read')
                                        <td>
                                                <template v-if="item.media.length > 0">

                                                    <a  class="btn btn-primary btn-sm" @click="uploadFiles(item.media)" title="Download">
                                                        <i class="fas fa-download"></i>
                                                    </a>
                                                    <a  class="btn btn-primary btn-sm" @click="showFiles(item.media)" title="Open">
                                                        <i class="fas fa-eye"></i>
                                                    </a>
                                                    <!--<a v-for="it in item.media"
                                                       class="btn btn-primary btn-sm"
                                                        :href="'/media/' + it.id + '/' + it.file_name"
                                                        target="_blank"
                                                        download>
                                                        <i class="fas fa-download"></i>
                                                    </a>-->
                                                </template>
                                                <template v-else>
                                                    @if(Auth::user()->hasPermissionTo('crud.package.columns.invoice.write'))
                                                        <a class="btn btn-info btn-sm" href="#" @click="currentIndex = index; $modal.show('invoice_upload')">
                                                            <i class="fas fa-cloud-upload-alt"></i>
                                                        </a>
                                                    @else
                                                        -
                                                    @endif
                                                </template>
                                        </td>
                                        @endcan
                                        <td class="nowrap">
                                            <a class="btn btn-success btn-sm" :href="'/admin/packages/' + item.id">
                                                <i class="fa fa-info-circle" aria-hidden="true"></i>
                                            </a>
                                            @can('crud.package.edit')
                                                <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-sm btn-spinner btn-info" :href="'/admin/tasks/create-from-package/'+item.id"><i class="fa fa-tasks"></i></a>
                                            @endcan
                                            @can('crud.package.create')
                                                <a v-if="!item.delivered_at" class="btn btn-sm btn-spinner btn-info" @click="forceDelivered(item.id)" href="#" title="{{ trans('admin.force_delivered') }}" role="button"><i class="fas fa-truck"></i></a>
                                            @endcan
                                            @can('crud.package.delete')
                                                <button
                                                    @click="deleteItem(item.resource_url)"
                                                    class="btn btn-sm btn-danger"
                                                    title="{{ trans('brackets/admin-ui::admin.btn.delete') }}">
                                                    <i class="fas fa-trash-alt"></i>
                                                </button>
                                            @endcan
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center" v-if="_.isEmpty(collection)">
                                <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                                <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                            </div>
                            <div v-if="pagination.state.total > 0">
                                <pagination></pagination>
                            </div>
                        </div>
                    </div>
                </div>
                <modal name="invoice_upload" height="auto">
                    <div class="modal-content">
                        <div class="modal-header">
                            @lang('admin.upload_file')
                        </div>
                        <div class="modal-body">
                            @include('brackets/admin-ui::admin.includes.media-uploader', [
                                'mediaCollection' => app(App\Package::class)->getMediaCollection('invoice'),
                                'label' => trans('admin.package.columns.invoice'),
                            ])
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary" @click="uploadFile('invoice')">
                                @lang('admin.send')
                            </button>
                        </div>
                    </div>
                </modal>
            </div>
        </div>
    </package-listing>

@endsection
