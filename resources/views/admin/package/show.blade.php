@extends('brackets/admin-ui::admin.layout.default')

@section('title', $package->name)

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>{{ $package->name }}</span></h2>
    </div>
    <div class="mb-3">
        <a class="btn btn-light" href="{{ url()->previous() }}">
            @lang('admin.back_to_the_list')
        </a>
    </div>
    <div class="card">
        <div class="card-body">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td class="font-weight-bold">
                            @lang('admin.package.columns.name')
                        </td>
                        <td>{{ $package->name }}</td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold">
                            @lang('admin.package.columns.weight')
                        </td>
                        <td>{{ $package->weight }}</td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold">
                            @lang('admin.package.columns.creator_id')
                        </td>
                        <td>{{ $package->creator ? $package->creator->name : '-' }}</td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold">
                            @lang('admin.package.columns.manager_id')
                        </td>
                        <td>{{ $package->manager ? $package->manager->name : '-' }}</td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold">
                            @lang('admin.package.columns.status_id')
                        </td>
                        <td>{{ $package->status->title }}</td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold">
                            @lang('admin.package.columns.comment')
                        </td>
                        <td>{{ $package->comment }}</td>
                    </tr>
                    <tr>
                        <td class="font-weight-bold">
                            @lang('admin.package.columns.description')
                        </td>
                        <td>{{ $package->description }}</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <div class="card">
        <div class="card-header">Items</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-borderless">
                    <thead>
                        <th>{{ trans('admin.item.columns.id') }}</th>
                        <th>{{ trans('admin.item.columns.name') }}</th>
                        <th>{{ trans('admin.item.columns.size') }}</th>
                        <th>{{ trans('admin.item.columns.color') }}</th>
                        <th>{{ trans('admin.item.columns.qty') }}</th>
                        <th>{{ trans('admin.item.columns.price') }}</th>
                        <th>{{ trans('admin.item.columns.link') }}</th>
                        <th>{{ trans('admin.item.columns.user_id') }}</th>
                    </thead>
                    <tbody>
                        @foreach($package->items as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->size }}</td>
                                <td>{{ $item->color }}</td>
                                <td>{{ $item->pivot->qty }}</td>
                                <td>{{ $item->pivot->price }}</td>
                                <td><a href="{{ $item->link }}">{{ $item->link }}</a></td>
                                <td>{{ $item->user ? $item->user->name : '-' }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
