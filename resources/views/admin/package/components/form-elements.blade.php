@include('admin.layout.inputs.text', [
'name' => 'track'
])
@include('admin.layout.inputs.select', [
'name' => 'carrier_id',
'arrayName' => 'carriers',
])
@include('admin.layout.inputs.text', [
'name' => 'weight'
])
@include('admin.layout.inputs.text', [
'name' => 'name'
])
@include('admin.layout.inputs.select', [
'name' => 'manager_id',
'arrayName' => 'managers',
])
@include('admin.layout.inputs.textarea', [
'name' => 'comment'
])
@include('admin.layout.inputs.select', [
'name' => 'status_id',
'arrayName' => 'statuses',
])
@include('admin.layout.inputs.textarea', [
'name' => 'description'
])
@if(Auth::user()->hasPermissionTo('crud.package.columns.invoice.write') && isset($package) && $package->id)
	<div class="row align-items-center">
        <div class="col-md-2 text-md-right">
            <label for="">@lang('admin.package.columns.invoice')</label>
        </div>
        <div class="col-md-9 col-xl-8">
            @include('brackets/admin-ui::admin.includes.media-uploader', [
		        'mediaCollection' => ( $package ?? app(App\Package::class))->getMediaCollection('invoice'),
		        'label' => trans('admin.package.columns.invoice'),
		        'media' => ( $package ?? app(App\Package::class))->getThumbs200ForCollection('invoice')
		    ])
        </div>
    </div>
@endif
@can('crud.package.columns.employee_id.write')
@if(isset($package) && $package->id)
<div class="form-group row align-items-center">
    <label class="col-form-label text-md-right col-md-2">{{ trans('admin.package.columns.employee_id') }}</label>
    <div class="col-md-9 col-xl-8">
        <input type="text" value="{{ $package->employee->user->name }}" class="form-control" readonly>
    </div>
</div>
@else
<user-select-employee ref="employeeSelect" v-model="form.employee_id"></user-select-employee>
@endif

<div class="row align-items-center">
    <div class="col-md-2">
    </div>
    <div class="col-md-9 col-xl-8">
        <div v-if="errors.has('employee_id')" class="text-danger" v-cloak>@{{ errors.first('employee_id') }}</div>
    </div>
</div>
@endcan
@if(auth()->user()->hasAnyRole(['Administrator', 'Staffer', 'HR Manager Staff', 'Support Staff']))
<item-select
ref="itemSelect"
@if(isset($package))
    @if($package->delivered_at)
    :create="false"
    @else
    :price-input="true"
    :qty-input="true"
    @endif
:attached-items="{{ $package->items }}"
@else
:price-input="true"
:qty-input="true"
@endif
:fails="errors"></item-select>
<div v-if="errors.has('item_ids')" class="text-danger" v-cloak>@{{ errors.first('item_ids') }}</div>
@endif
