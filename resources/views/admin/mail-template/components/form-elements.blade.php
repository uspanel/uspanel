<div class="form-group row align-items-center">
    <label for="title" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mail-template.columns.title') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.title" class="form-control" id="title" name="title" placeholder="{{ trans('admin.mail-template.columns.title') }}">
        <div v-if="errors.has('title')" class="text-danger" v-cloak>@{{ errors.first('title') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="subject" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mail-template.columns.subject') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <input type="text" v-model="form.subject" class="form-control" id="subject" name="subject" placeholder="{{ trans('admin.mail-template.columns.subject') }}">
        <div v-if="errors.has('subject')" class="text-danger" v-cloak>@{{ errors.first('subject') }}
        </div>
    </div>
</div>

<div class="form-group row align-items-center">
    <label for="body" class="col-form-label text-md-right"
           :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.mail-template.columns.body') }}</label>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <div>
            <wysiwyg v-model="form.body" id="body" name="body"
                     :config="mediaWysiwygConfig"></wysiwyg>
        </div>
        <div v-if="errors.has('body')" class="text-danger" v-cloak>@{{ errors.first('body') }}</div>
    </div>
</div>

<div class="row">
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-2'"></div>
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
        <strong>@lang('admin.mail-template.columns.vars_you_can_use')</strong></div>
</div>
@php $count = 0 @endphp
@foreach ($vars as $key=>$val)
    <div @if($count > 10) v-show="readmore" @endif class="row">
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-2'"></div>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">{{$key}} - {{$val}}</div>
    </div>
    @php $count++ @endphp
@endforeach
<div @if($count > 10) @endif class="row">
    <div :class="isFormLocalized ? 'col-md-4' : 'col-md-2'"></div>
    <div v-show="!readmore" :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'" @click="readmore = !readmore"><a
            href="#">@lang('admin.readmore')</a></div>
    <div v-show="readmore" :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'" @click="readmore = !readmore"><a
            href="#">@lang('admin.readless')</a></div>
</div>



