@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.mail-template.actions.create'))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>@lang('admin.mail-template.actions.create')</span></h2>
    </div>
    <mail-template-form
        :action="'{{ url('/admin/mail-templates') }}'"
        inline-template>
        <div class="card">
            <div class="card-body">
                <form class="form-horizontal form-create" method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>
                    @include('admin.mail-template.components.form-elements')
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </mail-template-form>
@endsection
