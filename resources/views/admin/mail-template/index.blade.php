@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.mail-template.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0"><span>@lang('admin.mail-template.actions.index')</span></h2>
        </div>
        <div class="col-auto">
            <a class="btn btn-primary" href="{{ url('admin/mail-templates/create') }}" role="button">
                <i class="fa fa-plus"></i>
                @lang('admin.mail-template.actions.create')
            </a>
        </div>
    </div>
    <mail-template-listing
        :data="{{ $data->toJson() }}"
        :url="'{{ url('/admin/mail-templates') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <form @submit.prevent="">
                            <div class="row justify-content-md-between">
                                <div class="col-md-3 form-group">
                                    <label for="">{{ trans('brackets/admin-ui::admin.placeholder.search') }}</label>
                                    <input class="form-control" placeholder="{{ trans('brackets/admin-ui::admin.placeholder.search') }}" v-model="search" @keyup.enter="filter('search', $event.target.value)" />
                                </div>
                                <div class="col-auto">
                                    <button type="button" class="btn btn-primary mb-3" @click="filter('search', search)">
                                        <i class="fa fa-search"></i>
                                        {{ trans('brackets/admin-ui::admin.btn.search') }}
                                    </button>
                                </div>
                            </div>
                            <div class="row justify-content-end">
                                <div class="col-auto form-group">
                                    <select class="form-control" v-model="pagination.state.per_page">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </form>

                        <table class="table table-hover table-listing">
                            <thead>
                                <tr>
                                    <th is='sortable' :column="'title'" class="w-25">{{ trans('admin.mail-template.columns.title') }}</th>
                                    <th class="w-50">{{ trans('admin.mail-template.columns.body') }}</th>
                                    <th is='sortable' :column="'created_at'" class="w-auto">{{ trans('admin.mail-template.columns.created_at') }}</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(item, index) in collection">
                                    <td>@{{ item.title }}</td>
                                    <td v-html="item.body"></td>
                                    <td>@{{ item.created_at }}</td>
                                    <td class="nowrap">
                                        <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                                        <button
                                        @click="deleteItem(item.resource_url)"
                                        class="btn btn-sm btn-danger"
                                        title="{{ trans('brackets/admin-ui::admin.btn.delete') }}">
                                            <i class="fas fa-trash-alt"></i>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="text-center" v-if="_.isEmpty(collection)">
                            <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
                            <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
                        </div>
                        <div v-if="pagination.state.total > 0">
                            <pagination></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </mail-template-listing>

@endsection
