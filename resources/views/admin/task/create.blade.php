@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.task.actions.create'))

@section('body')
    <div class="align-items-center page-heading">
        <h2 class="title mb-0 mb-sm-0"><span>@lang('admin.task.actions.create')</span></h2>
    </div>
    <task-form
        :action="'{{ url('admin/tasks') }}'"
        :users="{{ $users->toJson() }}"
        :managers="{{ $managers->toJson() }}"
        :employees="{{ $employees->toJson() }}"
        :carriers="{{ $carriers->toJson() }}"
        :warehouses="{{$warehouses->toJson()}}"
        :projects="{{$projects->toJson()}}"
        :courier-statuses="{{ $courierStatuses }}"
        :employee="{{$employee ?: 0}}"
        :employee_id= "{{$employee_id ?:0}}"
        :package_id = "{{ $package_id ?:0 }}"
        inline-template>
        <div class="card">
            <div class="card-body">
                <form method="post" @submit.prevent="onSubmit" :action="this.action" novalidate>
                    @include('admin.task.components.form-elements', [
                        'viewName' => 'task'
                    ])
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary" :disabled="submiting">
                            <i class="fa" :class="submiting ? 'fa-spinner' : 'fa-download'"></i>
                            {{ trans('brackets/admin-ui::admin.btn.save') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </task-form>
@endsection
