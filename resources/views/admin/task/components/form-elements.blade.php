@include('admin.layout.inputs.text', [
'name' => 'track'
])
@can('crud.task.columns.track.write')
    <div class="form-group row align-items-center">
        <label for="sku" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.task.columns.sku') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.sku" class="form-control" id="sku" name="sku"
                   placeholder="{{ trans('admin.task.columns.sku') }}">
            <div v-if="errors.has('sku')" class="text-danger" v-cloak>@{{ errors.first('sku') }}</div>
        </div>
    </div>
@endcan
@can('crud.task.columns.track.write')
    <div class="form-group row align-items-center">
        <label for="package_name" class="col-form-label text-md-right"
               :class="isFormLocalized ? 'col-md-4' : 'col-md-2'">{{ trans('admin.task.columns.package_name') }}</label>
        <div :class="isFormLocalized ? 'col-md-4' : 'col-md-9 col-xl-8'">
            <input type="text" v-model="form.package_name" class="form-control" id="package_name" name="package_name"
                   placeholder="{{ trans('admin.task.columns.package_name') }}">
            <div v-if="errors.has('package_name')" class="text-danger" v-cloak>@{{ errors.first('package_name') }}</div>
        </div>
    </div>
@endcan
@include('admin.layout.inputs.select', [
'name' => 'carrier_id',
'arrayName' => 'carriers',
])
@include('admin.layout.inputs.select', [
'name' => 'warehouse_id',
'arrayName' => 'warehouses',
])
@if(isset($task))
@include('admin.layout.inputs.select', [
    'name' => 'employee_status_id',
    'arrayName' => 'courierStatuses',
])
@endif
@include('admin.layout.inputs.textarea', [
'name' => 'comment'
])
@include('admin.layout.inputs.select', [
'name' => 'manager_id',
'arrayName' => 'managers',
])

@can('crud.task.columns.label.write')
    <div class="row align-items-center">
        <div class="col-md-2 text-md-right">
            <label for="">@lang('admin.task.columns.label')</label>
        </div>
        <div class="col-md-9 col-xl-8">
            @include('brackets/admin-ui::admin.includes.media-uploader', [
                'mediaCollection' => ( $task ?? app(App\Task::class))->getMediaCollection('label'),
                'label' => trans('admin.task.columns.label'),
                'media' => ( $task ?? app(App\Task::class))->getThumbs200ForCollection('label')
            ])
        </div>
    </div>
@endcan
@can('crud.task.columns.barcode.write')
    <div class="row align-items-center">
        <div class="col-md-2 text-md-right">
            <label for="">@lang('admin.task.columns.barcode')</label>
        </div>
        <div class="col-md-9 col-xl-8">
            @include('brackets/admin-ui::admin.includes.media-uploader', [
                'mediaCollection' => ( $task ?? app(App\Task::class))->getMediaCollection('barcode'),
                'label' => trans('admin.task.columns.barcode'),
                'media' => ( $task ?? app(App\Task::class))->getThumbs200ForCollection('barcode')
            ])
        </div>
    </div>
@endcan
@can('crud.task.columns.receipt.write')
    <div class="row align-items-center">
        <div class="col-md-2 text-md-right">
            <label for="">@lang('admin.task.columns.receipt')</label>
        </div>
        <div class="col-md-9 col-xl-8">
            @include('brackets/admin-ui::admin.includes.media-uploader', [
                'mediaCollection' => ( $task ?? app(App\Task::class))->getMediaCollection('receipt'),
                'label' => trans('admin.task.columns.receipt'),
                'media' => ( $task ?? app(App\Task::class))->getThumbs200ForCollection('receipt')
            ])
        </div>
    </div>
@endcan
<!--@can('crud.task.columns.invoice.write')
    <div class="row align-items-center">
        <div class="col-md-2 text-md-right">
            <label for="">@lang('admin.task.columns.invoice')</label>
        </div>
        <div class="col-md-9 col-xl-8">
            @include('brackets/admin-ui::admin.includes.media-uploader', [
                'mediaCollection' => ( $task ?? app(App\Task::class))->getMediaCollection('invoice'),
                'label' => trans('admin.task.columns.invoice'),
                'media' => ( $task ?? app(App\Task::class))->getThumbs200ForCollection('invoice')
            ])
        </div>
    </div>
@endcan
@can('crud.task.columns.content.write')
    <div class="row align-items-center">
        <div class="col-md-2 text-md-right">
            <label for="">@lang('admin.task.columns.content')</label>
        </div>
        <div class="col-md-9 col-xl-8">
            @include('brackets/admin-ui::admin.includes.media-uploader', [
                'mediaCollection' => ( $task ?? app(App\Task::class))->getMediaCollection('content'),
                'label' => trans('admin.task.columns.content'),
                'media' => ( $task ?? app(App\Task::class))->getThumbs200ForCollection('content')
            ])
        </div>
    </div>
@endcan-->
@can('crud.task.columns.employee_id.write')
<user-select-employee ref="employeeSelect" v-model="form.employee_id"></user-select-employee>
<div class="row align-items-center">
    <div class="col-md-2">
    </div>
    <div class="col-md-9 col-xl-8">
        <div v-if="errors.has('employee_id')" class="text-danger" v-cloak>@{{ errors.first('employee_id') }}</div>
    </div>
</div>
@endcan
@if(auth()->user()->hasAnyRole(['Administrator', 'Staffer', 'HR Manager Staff']))
<item-select
    ref="itemSelect"
    @if(isset($task))
    :selectable="false"
    :attached-items="{{ $task->items }}"
    @else
    :fails="errors"
    :qty-input="true"
    @endif
    :create="false"></item-select>
<div v-if="errors.has('item_ids')" class="text-danger" v-cloak>@{{ errors.first('item_ids') }}</div>
@endif
