<div class="table-responsive">
    <table class="table table-hover table-listing">
        <thead>
            <tr>
                <th class="nowrap"><input id="CheckAll" type="checkbox" @click="checkAll()"/>{{ trans('admin.task.columns.csv') }}</th>
                <th class="nowrap" is='sortable' :column="'id'">{{ trans('admin.task.columns.id') }}</th>
                @include('admin.layout.cells.th', ['name' => 'warehouse_id'])
                @include('admin.layout.cells.th', ['name' => 'delivered_at'])
                @include('admin.layout.cells.th', ['viewName' => 'package', 'name' => 'track'])
                @include('admin.layout.cells.th', ['name' => 'carrier_tracking_status', 'notSortable' => true])
                @include('admin.layout.cells.th', ['name' => 'employee_status_id'])
                @include('admin.layout.cells.th', ['name' => 'comment'])
                @include('admin.layout.cells.th', ['name' => 'employee_id'])
                @include('admin.layout.cells.th', ['name' => 'creator_id'])
                @include('admin.layout.cells.th', ['name' => 'manager_id'])
                @include('admin.layout.cells.th', ['name' => 'label', 'notSortable' => true])
                @include('admin.layout.cells.th', ['name' => 'barcode', 'notSortable' => true])
                @include('admin.layout.cells.th', ['name' => 'receipt', 'notSortable' => true])
                @include('admin.layout.cells.th', ['name' => 'invoice', 'notSortable' => true])
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="(item, index) in collection">
                <td><input class="csv" type="checkbox" :value="item.id" value/></td>
                <td>@{{item.id}}</td>
                @include('admin.layout.cells.td', ['name' => 'warehouse_id', 'field' => 'warehouse.name'])
                @include('admin.layout.cells.td', ['name' => 'delivered_at'])
                @can('crud.package.columns.track.read')
                    <td class="text-center">
                        @can('crud.package.columns.carrier_id.read')
                            <i v-if="item.carrier" :class="['d-block', 'fab', 'fa-3x', 'fa-' + item.carrier.name]"></i>
                            <div v-if="item.track_url">
                                <a :href="item.track_url" target="_blank">@lang('admin.track_on_site')</a>
                            </div>
                        @endcan
                        @{{ item.track}}
                    </td>
                @endcan
                @include('admin.layout.cells.td', ['name' => 'carrier_tracking_status', 'field' => 'last_activity.title'])
                @include('admin.layout.cells.td', ['name' => 'employee_status_id', 'field' => 'employee_status.name'])
                @include('admin.layout.cells.td', ['name' => 'comment'])
                @include('admin.layout.cells.td', ['name' => 'employee_id', 'field' => 'employee.user.name'])
                @include('admin.layout.cells.td', ['name' => 'creator_id', 'field' => 'creator.name'])
                @include('admin.layout.cells.td', ['name' => 'manager_id', 'field' => 'manager.name'])
                @include('admin.layout.cells.file', ['name' => 'label'])
                @include('admin.layout.cells.file', ['name' => 'barcode'])
                @include('admin.layout.cells.file', ['name' => 'receipt'])
                @include('admin.layout.cells.file', ['name' => 'invoice'])
                <td class="nowrap">
                    <a class="btn btn-success btn-sm" :href="'/admin/tasks/' + item.id">
                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                    </a>
                    <a v-if="!item.delivered_at" class="btn btn-sm btn-spinner btn-info" @click="forceDelivered(item.id)" href="#" title="{{ trans('admin.force_delivered') }}" role="button"><i class="fas fa-truck"></i></a>
                    <a class="btn btn-sm btn-spinner btn-info" :href="item.resource_url + '/edit'" title="{{ trans('brackets/admin-ui::admin.btn.edit') }}" role="button"><i class="fa fa-edit"></i></a>
                    @if(Auth::user()->hasPermissionTo('crud.task.delete'))
                        <button
                        @click="deleteItem(item.resource_url)"
                        class="btn btn-sm btn-danger"
                        title="{{ trans('brackets/admin-ui::admin.btn.delete') }}">
                            <i class="fas fa-trash-alt"></i>
                        </button>
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="text-center" v-if="_.isEmpty(collection)">
    <h3>{{ trans('brackets/admin-ui::admin.index.no_items') }}</h3>
    <p>{{ trans('brackets/admin-ui::admin.index.try_changing_items') }}</p>
</div>
<div v-if="pagination.state.total > 0">
    <pagination></pagination>
</div>
