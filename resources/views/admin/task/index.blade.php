@extends('brackets/admin-ui::admin.layout.default')

@section('title', trans('admin.task.actions.index'))

@section('body')
    <div class="row align-items-center page-heading">
        <div class="col">
            <h2 class="title mb-0 mb-sm-0"><span>@lang('admin.task.actions.index')</span></h2>
        </div>
        <div class="col-auto">
            @can('crud.task.create')
                <a class="btn btn-primary" href="{{ url('admin/tasks/create') }}" role="button">
                    <i class="fa fa-plus"></i>
                    @lang('admin.task.actions.create')
                </a>

            @endcan
        </div>
    </div>
    <task-listing
        :warehouses="{{$warehouses->toJson()}}"
        :data="{{ $data->toJson() }}"
        :users="{{ $users->toJson() }}"
        :managers="{{ $managers->toJson() }}"
        :carriers="{{ $carriers->toJson() }}"
        :employees="{{$employees->toJson()}}"
        :url="'{{ url('admin/tasks') }}'"
        inline-template>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body" v-cloak>
                        <div class="row align-items-end">
                            <div class="col">
                                <div class="row">
                                    @include('admin.layout.filters.text', [
                                    'name' => 'id',
                                    ])
                                    @include('admin.layout.filters.select', [
                                    'name' => 'warehouse_id',
                                    'arrayName' => 'warehouses',
                                    ])
                                    @include('admin.layout.filters.date', [
                                    'name' => 'date',
                                    ])
                                    @include('admin.layout.filters.select', [
                                    'name' => 'carrier_id',
                                    'arrayName' => 'carriers',
                                    ])
                                    @if(!auth()->user()->hasRole('Employee Staff'))
                                    @include('admin.layout.filters.select', [
                                    'name' => 'employee_id',
                                    'arrayName' => 'employees',
                                    ])
                                    @endif
                                    @include('admin.layout.filters.text', [
                                    'name' => 'track',
                                    ])
                                    @include('admin.layout.filters.text', [
                                    'name' => 'carrier_tracking_status',
                                    ])
                                    @include('admin.layout.filters.text', [
                                    'name' => 'employee_status',
                                    ])
                                    @if(!auth()->user()->hasRole('Employee Staff'))
                                    @include('admin.layout.filters.text', [
                                    'name' => 'comment',
                                    ])
                                    @include('admin.layout.filters.select', [
                                    'name' => 'creator_id',
                                    'arrayName' => 'users',
                                    ])
                                    @include('admin.layout.filters.select', [
                                    'name' => 'manager_id',
                                    'arrayName' => 'managers',
                                    ])
                                    @endif
                                    <div class="col-auto align-self-end">
                                        <button type="button" class="btn btn-primary mb-3" @click="filter('search', search)">
                                            <i class="fa fa-search"></i>
                                            {{ trans('brackets/admin-ui::admin.btn.search') }}
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row justify-content-end">
                            <div class="col-auto form-group">
                                <select class="form-control" v-model="pagination.state.per_page">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="100">100</option>
                                </select>
                            </div>
                        </div>
                        <a class="btn btn-primary" @click="createCsv()" role="button">
                            <i class="fa fa-archive"></i>
                            @lang('admin.task.actions.csv')
                        </a>
                        @include('admin.task.table')
                    </div>
                </div>
            </div>
            <modal name="label_upload" height="auto">
                <div class="modal-content">
                    <div class="modal-header">
                        @lang('admin.upload_file')
                    </div>
                    <div class="modal-body">
                        @include('brackets/admin-ui::admin.includes.media-uploader', [
                            'mediaCollection' => app(App\Task::class)->getMediaCollection('label'),
                            'label' => trans('admin.task.columns.label'),
                        ])
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" @click="uploadFile('label')">
                            @lang('admin.send')
                        </button>
                    </div>
                </div>
            </modal>
            <modal name="barcode_upload" height="auto">
                <div class="modal-content">
                    <div class="modal-header">
                        @lang('admin.upload_file')
                    </div>
                    <div class="modal-body">
                        @include('brackets/admin-ui::admin.includes.media-uploader', [
                            'mediaCollection' => app(App\Task::class)->getMediaCollection('barcode'),
                            'label' => trans('admin.task.columns.barcode'),
                        ])
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" @click="uploadFile('barcode')">
                            @lang('admin.send')
                        </button>
                    </div>
                </div>
            </modal>
            <modal name="receipt_upload" height="auto">
                <div class="modal-content">
                    <div class="modal-header">
                        @lang('admin.upload_file')
                    </div>
                    <div class="modal-body">
                        @include('brackets/admin-ui::admin.includes.media-uploader', [
                            'mediaCollection' => app(App\Task::class)->getMediaCollection('receipt'),
                            'label' => trans('admin.task.columns.receipt'),
                        ])
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" @click="uploadFile('receipt')">
                            @lang('admin.send')
                        </button>
                    </div>
                </div>
            </modal>
            <modal name="invoice_upload" height="auto">
                <div class="modal-content">
                    <div class="modal-header">
                        @lang('admin.upload_file')
                    </div>
                    <div class="modal-body">
                        @include('brackets/admin-ui::admin.includes.media-uploader', [
                            'mediaCollection' => app(App\Task::class)->getMediaCollection('invoice'),
                            'label' => trans('admin.task.columns.invoice'),
                        ])
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" @click="uploadFile('invoice')">
                            @lang('admin.send')
                        </button>
                    </div>
                </div>
            </modal>
        </div>

    </task-listing>

@endsection
