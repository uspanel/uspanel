<?php

return [
    'page_title_suffix' => 'FBA',
    'btn' => [
        'send' => 'Send',
    ],
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => "ID",
            'first_name' => "First name",
            'last_name' => "Last name",
            'email' => "Email",
            'password' => "Password",
            'password_repeat' => "Password Confirmation",
            'activated' => "Activated",
            'forbidden' => "Forbidden",
            'language' => "Language",
            'address' => "Address",
            'phone' => "Phone",

            //Belongs to many relations
            'roles' => "Roles",

        ],
    ],

    'user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'email' => "Email",
            'email_verified_at' => "Email verified at",
            'password' => "Password",
            'first_name' => "First name",
            'last_name' => "Last name",
            'address' => "Address",
            'phone' => "Phone",
            'created_at' => "Created At",
            'groups' => "Groups",
            'first_name_last_name' => "First Name/Last Name",

        ],
    ],

    'mail-setting' => [
        'title' => 'Mail Settings',

        'actions' => [
            'index' => 'Mail Settings',
            'create' => 'New Mail Setting',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",

        ],
    ],

    'mail-setting' => [
        'title' => 'Mail Settings',

        'actions' => [
            'index' => 'Mail Settings',
            'create' => 'New Mail Setting',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'email' => "Email",
            'server' => "Server",
            'login' => "Login",
            'port' => "Port",
            'password' => "Password",

        ],
    ],

    'company' => [
        'title' => 'Companies',

        'actions' => [
            'index' => 'Companies',
            'create' => 'New Company',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Company Name",
            'company_type' => "Company type",
            'city' => "City",
            'state' => "State",
            'address' => "Address",
            'zip' => "Zip",
            'phone' => "Phone",
            'fax' => "Fax",
            'site' => "Company Site",
            'info_email' => "Info Email",
            'manager_id' => "Manager",
            'hr_manager_name' => "HR Manager Name",
            'hr_manager_email' => "HR Manager Email",
            'support_name' => "Support Name",
            'support_email' => "Support Email",
            'support_id' => "Support",
            'control_panel' => "Control Panel",
            'position_name' => "Position Name",
            'application' => "Application",

        ],
    ],

    'project' => [
        'title' => 'Projects',

        'actions' => [
            'index' => 'Projects',
            'create' => 'New Project',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'description' => "Description",
            'project_id' => "Project",

        ],
    ],

    'project' => [
        'title' => 'Projects',

        'actions' => [
            'index' => 'Projects',
            'create' => 'New Project',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'description' => "Description",
            'company_id' => "Company",

        ],
    ],

    'mailsetting' => [
        'set_imap_sent_folder' => 'Set IMAP Sent Folder',
        'set_imap_inbox_folder' => 'Set IMAP Inbox Folder',
        'title' => 'Mail Settings',

        'actions' => [
            'index' => 'Mail Settings',
            'create' => 'New Setting',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'user_id' => "User",
            'smtp_host' => "SMTP Host",
            'smtp_port' => "SMTP Port",
            'smtp_encryption' => "SMTP Encryption",
            'imap_host' => "IMAP Host",
            'imap_port' => "IMAP Port",
            'imap_encryption' => "IMAP Encryption",
            'imap_sent_folder' => "IMAP Sent Folder",
            'imap_inbox_folder' => "IMAP Inbox Folder",
            'login' => "Login",
            'password' => "Password",
            'default' => "Default",

        ],
        'get_folders' => 'Get Folders'
    ],
    'mail' => [
        'from' => 'From',
        'forward' => 'Forward',
        'title' => 'Mail Client',
        'subject' => 'Subject',
        'date' => 'Date',
        'attachments' => 'Attachments',
        'delete_selected' => 'Delete Selected',
        'delete' => 'Delete',

        'actions' => [
            'index' => 'Mail Client',
            'create' => 'New Letter',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'user_id' => "User",
            'smtp_host' => "SMTP Host",
            'smtp_port' => "SMTP Port",
            'smtp_encryption' => "SMTP Encryption",
            'imap_host' => "IMAP Host",
            'imap_port' => "IMAP Port",
            'imap_encryption' => "IMAP Encryption",
            'imap_sent_folder' => "IMAP Sent Folder",
            'imap_inbox_folder' => "IMAP Inbox Folder",
            'login' => "Login",
            'password' => "Password",
            'default' => "Default",

        ],
        'get_folders' => 'Try to get Folders'
    ],

    'user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'email' => "Email",
            'email_verified_at' => "Email verified at",
            'password' => "Password",
            'password_repeat' => "Password Confirmation",
            'first_name' => "First name",
            'last_name' => "Last name",
            'address' => "Address",
            'phone' => "Phone",

            //Belongs to many relations
            'roles' => "Roles",

        ],
    ],

    'warehouse' => [
        'title' => 'Warehouses',

        'actions' => [
            'index' => 'Warehouses',
            'create' => 'New Warehouse',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            
        ],
    ],

    'warehouse' => [
        'title' => 'Warehouse',

        'actions' => [
            'index' => 'Warehouse',
            'create' => 'New Warehouse',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];
