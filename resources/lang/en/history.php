<?php

return [

    'actions' => [
        'login' => 'User login. Id=:id',
        'logout' => 'User logout. Id=:id',

        'create_project' => 'New project created. Id=:id',
        'deleted_project' => 'Project deleted. Id=:id',
        'updated_project' => 'Project updated. Id=:id',

        'create_mailmessage' => 'Mail message sent. Folder=:folder. Id=:id',
        'deleted_mailmessage' => 'Mail message deleted. Id=:id',
        'updated_mailmessage' => 'Mail message updated. Folder=:folder. Id=:id',

        'create_warehouse' => 'New warehouse created. Id=:id',
        'deleted_warehouse' => 'Warehouse deleted. Id=:id',
        'updated_warehouse' => 'Warehouse updated. Id=:id',

        'create_task' => 'New task created. Id=:id',
        'deleted_task' => 'Task deleted. Id=:id',
        'updated_task' => 'Task updated. Id=:id',

        'create_invest_task' => 'New invest task created. Id=:id',
        'deleted_invest_task' => 'Invest task deleted. Id=:id',
        'updated_invest_task' => 'Invest task updated. Id=:id',

        'create_salesTask' => 'New sales task created. Id=:id',
        'deleted_salesTask' => 'Sales task deleted. Id=:id',
        'updated_salesTask' => 'Sales task updated. Id=:id',

        'create_package' => 'New package created. Id=:id',
        'deleted_package' => 'Package deleted. Id=:id',
        'updated_package' => 'Package updated. Id=:id',

        'create_helpVideo' => 'New help video created. Id=:id',
        'deleted_helpVideo' => 'Help video deleted. Id=:id',
        'updated_helpVideo' => 'Help video updated. Id=:id',

        'create_chatroom' => 'New chatroom created. Id=:id',
        'deleted_chatroom' => 'Chatroom deleted. Id=:id',
        'updated_chatroom' => 'Chatroom updated. Id=:id',

        'create_businessProcess' => 'New business process created. Id=:id',
        'deleted_businessProcess' => 'Business process deleted. Id=:id',
        'updated_businessProcess' => 'Business process updated. Id=:id',


        'create_mailing' => 'New mailing created. Id=:id',
        'deleted_mailing' => 'Mailing deleted. Id=:id',
        'updated_mailing' => 'Mailing updated. Id=:id',

        'create_emailGroup' => 'New email group created. Id=:id',
        'deleted_emailGroup' => 'Email group deleted. Id=:id',
        'updated_emailGroup' => 'Email group updated. Id=:id',

        'create_emailMailing' => 'New email mailing created. Id=:id',
        'deleted_emailMailing' => 'Email mailing deleted. Id=:id',
        'updated_emailMailing' => 'Email mailing updated. Id=:id',

        'create_item' => 'New item created. Id=:id',
        'deleted_item' => 'Item deleted. Id=:id',
        'updated_item' => 'Item updated. Id=:id',

        'create_user' => 'New user created. Id=:id',
        'deleted_user' => 'User deleted. Id=:id',
        'updated_user' => 'User updated. Id=:id',

        'create_role' => 'New role created. Id=:id',
        'deleted_role' => 'Role deleted. Id=:id',
        'updated_role' => 'Role updated. Id=:id',

        'create_mailTemplate' => 'New mail template created. Id=:id',
        'deleted_mailTemplate' => 'Mail template deleted. Id=:id',
        'updated_mailTemplate' => 'Mail template updated. Id=:id',

        'created_mailaccount' => 'New mail account created. Id=:id',
        'deleted_mailaccount' => 'Mail account deleted. Id=:id',
        'updated_mailaccount' => 'Mail account updated. Id=:id',

        'create_form' => 'New form created. Id=:id',
        'deleted_form' => 'Form deleted. Id=:id',
        'updated_form' => 'Form updated. Id=:id',

        'create_setting' => 'New setting created. Id=:id',
        'deleted_setting' => 'Setting deleted. Id=:id',
        'updated_setting' => 'Setting updated. Id=:id',

        'call_recorded' => 'Voice call recorded. Download link: :link'.'.mp3',
        'sms_sent' => 'Sent sms ( :message ) to number :phone',

        'test' => 'Test Test. Id=:id',
        'user_no_name' => '-',
    ],
];
