<?php

return [
    'support_ids' => 'Support Invest',
    'manager_ids' => 'HR Manager Invest',
    'track_on_site' => 'Track on website',
    'agreement' => 'Agreement',
    'force_delivered' => 'Make delivered',
    'receive_external' => 'Receive calls to external phone number',
    'minimize' => 'Minimize',
    'upload_file' => 'Upload file',
    'change' => 'Change',
    'send' => 'Send',
    'send_csv' => 'Create data from CSV',
    'clear_list' => 'Create list of names',
    'required' => 'Required',
    'interview' => 'Interview',
    'show_filters' => 'Show Filters',
    'hide_filters' => 'Hide Filters',
    'back' => 'Back',
    'all' => 'All',
    'back_to_the_list' => 'Back to the list',
    'read_all' => 'Read all',
    'upload' => 'Upload',
    'documents' => 'Documents',
    'new_notification' => 'New Notification!',
    'notifications' => 'Notifications',
    'no_notifications' => 'No Notifications',
    'to_blacklist' => 'To Blacklist',
    'show_more' => 'Show More',
    'verify_phone' => 'Verify Phone',
    'by_phone_result' => 'The manager will get in touch with you by phone during the day.',
    'check_your_email_to_verification' => 'Check your E-mail to verify your Account. You can contact support :supportEmail if you have not received the letter.',
    'registration' => 'Registration',
    'email_verification_success' => 'Your Email was successfully confirmed. So now, please, select the way you’d like to do your interview:',
    'phone' => 'Phone',
    'enter_code_from_sms' => 'Enter Verification Code from SMS',
    'yes' => 'Yes',
    'stats_and_pay' => 'Stats and Pay',
    'no' => 'No',
    'online' => 'Online',
    'by_phone' => 'By Phone',
    'higher_education' => 'Do you have higher education?',
    'address' => 'Address',
    'city' => 'City',
    'state' => 'State',
    'zip' => 'ZIP',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'profile' => 'Profile',
    'profile_updated' => 'Profile Updated!',
    'save' => 'Save',
    'approved' => 'Approved',
    'cv' => 'CV',
    'authorized_to_work' => 'Are you authorized to work in the USA?',
    'adult' => 'Are you 18 years old or more?',
    'contract' => 'Contract',
    'register' => 'Register',
    'create_new_account' => 'Create New Account',
    'login' => 'Login',
    'readmore' => 'Read more...',
    'readless' => 'Hide',
    'cancel' => 'Cancel',
    'statistics' => 'Statistics',
    'response' => 'Response',
    'deleting' => 'Deleting',
    'resending_after' => 'Resending after',
    'resend_code' => 'Resend Code',
    'yes' => 'Yes',
    'check_your_email' => 'Check your Email',
    'other' => 'Other',
    'resend' => 'Resend',
    'resend_verifying_email' => 'Resend Verifying Email',
    'you_can_change_your_email' => 'You can change your Email. Verification letter will be sent to the new Email',
    'are_you_sure' => 'Are you sure?',
    'does_not_match_your_location_determined_by_ip' => ':attribute does not match your location determined by IP.',
    'sip' => [
        'missed_call' => 'Missed call...',
        'incoming_call' => 'Incoming call...',
    ],
    'btn' => [
        'send' => 'Send',
        'save' => 'Save',
    ],
    'employee_contract' => [
        'download' => 'Download the',
        'upload' => 'Fill in the Contract and Upload to the Form',
    ],
    'employee_agreement' => [
        'upload' => 'Fill in the Agreement and Upload to the Form',
    ],
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => "ID",
            'first_name' => "First name",
            'last_name' => "Last name",
            'email' => "Email",
            'password' => "Password",
            'password_repeat' => "Password Confirmation",
            'activated' => "Activated",
            'forbidden' => "Forbidden",
            'language' => "Language",
            'address' => "Address",
            'phone' => "Phone",
            'phone_number' => "Phone",

            //Belongs to many relations
            'roles' => "Roles",

        ],
    ],

    'company' => [
        'title' => 'Companies',

        'actions' => [
            'index' => 'Companies',
            'create' => 'New Company',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Company Name",
            'company_type_id' => "Company Type",
            'city' => "City",
            'state' => "State",
            'state_id' => "State",
            'address' => "Address",
            'zip' => "Zip",
            'phone' => "Phone",
            'second_phone' => "Phone 2",
            'fax' => "Fax",
            'site' => "Company Site",
            'info_email' => "Info Email",
            'manager_id' => "Manager",
            'hr_manager_name' => "HR Manager Name",
            'hr_manager_email' => "HR Manager Email",
            'support_name' => "Support Name",
            'support_email' => "Support Email",
            'support_id' => "Support",
            'control_panel' => "Control Panel",
            'position_name' => "Position Name",
            'application' => "Application",
            'salary' => "Salary",

        ],
    ],


    'project' => [
        'title' => 'Projects',

        'actions' => [
            'index' => 'Projects',
            'create' => 'New Project',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Project Name",
            'project_id' => "Project ID",
            'description' => "Description",
            'company_id' => "Company",
            'interview_forms' => "Interview Forms",
            'salary' => "Salary",
            'employee_pct' => "Employee percent",
            'manager' => "Must select at last one manager",
            'support' => "Must select at last one support",
            'mail_domain' => 'Mail domain'

        ],
    ],

    'mailsetting' => [
        'set_imap_sent_folder' => 'Set IMAP Sent Folder',
        'set_imap_inbox_folder' => 'Set IMAP Inbox Folder',
        'title' => 'Mail Account',

        'actions' => [
            'index' => 'Mail Account',
            'create' => 'New Account',
            'edit' => 'Edit :name',
        ],
        'errors' => [
            'names' => 'Names must be created',
        ],
        'columns' => [
            'id' => "ID",
            'user_id' => "User",
            'first_domain' => "Name and first part of mail",
            'second_domain' => "Second part of mail",
            'second_domain_title' => "Comma separated",
            'smtp_host' => "SMTP Host",
            'smtp_port' => "SMTP Port",
            'smtp_encryption' => "SMTP Encryption",
            'imap_host' => "IMAP Host",
            'imap_port' => "IMAP Port",
            'imap_encryption' => "IMAP Encryption",
            'imap_sent_folder' => "IMAP Sent Folder",
            'imap_inbox_folder' => "IMAP Inbox Folder",
            'login' => "Login",
            'password' => "Password",
            'default' => "Default",
            'validate_cert' => "Validate Mail server certificate"

        ],
        'get_folders' => 'Get Folders',
        'addMailing' => 'Add setting for mailing',
        'hideMailing' => 'Hide setting for mailing',
    ],
    'mail' => [
        'from' => 'From',
        'to' => 'To',
        'typeMail' => 'Type Mail',
        'text' => 'Text',
        'text_mail' => 'Text mail',
        'save' => 'Save',
        'new_template' => 'Template',
        'name' => 'Name',
        'forward' => 'Forward',
        'title' => 'Mail Client',
        'subject' => 'Subject',
        'date' => 'Date',
        'attachments' => 'Attachments',
        'delete_selected' => 'Delete Selected',
        'delete' => 'Delete',
        'sender' => 'Sender',
        'recipient' => 'Recipient',
        'template' => 'Template',
        'save_as_new_template' => 'Save as Template',
        'public_template' => 'Avaliable for all users',
        'reply' => 'Reply',

        'actions' => [
            'index' => 'Mail Client',
            'create' => 'New Letter',
            'edit' => 'Edit :name',
            'other_files' => 'Choose other files',
            'all_accounts' => 'All accounts',
            'personal' => 'Personal accounts',
            'owned' => 'Other users accounts'
        ],
    ],

    'user' => [
        'send_sms' => 'Send SMS to user',
        'sms_text' => 'Text of SMS message',
        'title' => 'Users',
        'employees' => 'Employees',
        'invest_employees' => 'Invest employees',
        'staffers' => 'Staffer',
        'сhanges_saved' => 'Changes saved!',
        'open_tasks' => 'Open Tasks',
        'tasks' => 'Tasks',
        'total' => 'Total',
        'packages' => 'Packages',
        'delivered' => 'Delivered',
        'rating' => 'Rating',
        'items' => 'Items',
        'user_status_updated' => 'User Status updated: :status (:name).',
        'employee_did_not_upload_documents' => 'Employee :name did not upload Documents',
        'uploaded_documents' => 'Employee :name uploaded Documents',
        'did_not_upload_documents' => 'Employee :name did non upload Documents',
        'employee' => [
            'columns' => [
                'user_status_id' => 'User Status'
            ],
        ],
        'salary' => [
            'from' => 'From',
            'to' => 'To',
            'created_at' => 'Date',
            'amount' => 'Amount',
            'project_id' => 'Project',
            'task_id' => 'Task',
            'total' => 'Total',
        ],

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'email' => "Email",
            'email_verified_at' => "Email verified at",
            'password' => "Password",
            'password_repeat' => "Password Confirmation",
            'first_name' => "First name",
            'last_name' => "Last name",
            'address' => "Address",
            'phone' => "Phone",
            'created_at' => "Created",
            'phone_number' => "Phone",
            'project_id' => "Project",
            'approved' => "Approved",
            'tasks' => "Tasks",
            'deleted_at' => "Deleted",
            'updated_at' => "Updated",
            'employee_project_id' => "Employee Project",
            'receive_external_calls' => "Receive External Calls",
            'profile_address' => "Address",
            'profile_city' => "City",
            'profile_state_id' => "State",
            'profile_zip' => "ZIP",
            'profile_authorized_to_work' => "Authorized to work",
            'profile_higher_education' => "Higher Education",
            'profile_adult' => "Adult",
            "staffer_id" => "Staffer",
            'employees' => 'Employees',
            'employee_approved' => "Approved",
            'activity_years' => 'Activity years',
            'activity_time' => 'Activity time',
            'country' => 'Country',
            'comment' => 'Comment',
            'imap_email' => 'IMAP email',
            'imap_login' => 'IMAP login',
            'imap_password' => 'IMAP password',
            'imap_server' => 'IMAP server',
            'support' => 'Support',
            'last_template' => 'Last template',
            'last_activity' => 'Last activity',

            //Belongs to many relations
            'roles' => "Roles",
            'employee_user_status_id' => "Status",
            'employee_call_support_comment' => "Call Support Comment",
            'hr_id' => 'HR Manager',
            'support_id' => 'Support',
            'cs_id' => 'Call support',
            'manager' => 'Manager',
            'voicent_status' => 'Auto Call Status',
            'voicent_time' => 'Auto Call Start Time',
            'voicent_duration' => 'Auto Call duration',
            'voicent_notes' => 'Auto Call Notes',

        ],
        'profile' => [
            'columns' => [
                'referrer' => 'Referrer'
            ]
        ],
        'errors' => [
            'user_has_not_empty_warehouse' => 'User with attached warehouse with items balance > 0 can not be deleted.'
        ]
    ],

    'role' => [
        'title' => 'Roles',

        'actions' => [
            'index' => 'Roles',
            'create' => 'New Role',
            'edit' => 'Edit :name',
        ],
        'view' => 'View',
        'delete' => 'Delete',
        'edit' => 'Edit',
        'create' => 'Create',
        'read' => 'Read',
        'write' => 'Write',
        'column' => 'Column',
        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'guard_name' => "Guard name",
            'created_at' => "Created At"

        ],
    ],

    'warehouse' => [
        'title' => 'Warehouses',

        'actions' => [
            'index' => 'Warehouse',
            'create' => 'New Warehouse',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => 'Name',
            'city' => 'City',
            'address' => 'Address',
            'user_id' => 'User',
        ],
    ],

    'task' => [
        'empty_item' => 'Item quantity can not be empty',
        'quantity_overlap' => 'Quantity is more than available',
        'title' => 'Task',
        'task_id' => 'Task № :id',
        'packages' => 'Packages',
        'status_changed' => 'Task Status changed: :status',
        'task_updated' => 'Task №:id. :text',
        'task_created' => 'New Task created.',
        'employee_uploaded_file' => 'Employee uploaded Document :mediaCollection',
        'employee_delayed_task' => 'Employee :name delayed Task.',
        'employee_did_not_upload_task_files' => 'Employee :name did not upload task file :mediaCollection.',
        'unable_to_track_packages_make_sure_you_send_packages' => 'Unable to track packages. Make sure you send packages.',
        'projects' => 'Projects',
        'empty_items' => 'You can not create a task without items. Add item.',
        'actions' => [
            'index' => 'Task',
            'create' => 'New Task',
            'csv' => 'Create Csv',
            'edit' => 'Edit :name',
        ],
        'columns' => [
            'id' => "ID",
            'csv' => "Create csv",
            'warehouse_id' => 'Warehouse',
            'delivered_at' => 'Delivery Date',
            'packages' => 'Packages',
            'carrier_id' => 'Carrier',
            'track' => 'Track',
            'sku' => 'Sku',
            'package_name' => 'Package name',
            'carrier_tracking_status' => 'Tracking status by carrier',
            'employee_status' => 'Status by Employee',
            'employee_status_id' => 'Status by Employee',
            'comment' => 'Comment',
            'creator_id' => 'Who created',
            'manager_id' => 'Who manage',
            'label' => 'Label',
            'barcode' => 'Barcode',
            'receipt' => 'Receipt',
            'courier_status_id' => 'Courier Status',
            'employee_id' => 'Employee',
            'delivery_status_id' => 'Delivery Status',
            'invoice' => 'Invoice',
            'content' => 'Content',
        ],
    ],
    'package' => [
        'title' => 'Package',
        'package_deleted' => 'Package :name deleted by :userName.',
        'package_created' => 'Package :name created by :userName.',
        'empty_items' => 'You can not create a task without items. Add item.',
        'actions' => [
            'index' => 'Package',
            'create' => 'New Package',
            'edit' => 'Edit :name',
            'manual_delivery' => 'Manual delivery'
        ],
        'columns' => [
            'id' => "ID",
            'delivered_at' => 'Delivery date',
            'track' => 'Track',
            'carrier_id' => 'Carrier',
            'weight' => 'Weight',
            'name' => 'Name on package',
            'creator_id' => 'Created by',
            'courier_id' => 'Courier',
            'manager_id' => 'Managed by',
            'employee_id' => 'Employee',
            'comment' => 'Comment',
            'status_id' => 'Status',
            'description' => 'Description',
            'price' => 'Price',
            'color' => 'Color',
            'link' => 'Link',
            'invoice' => 'Invoice',
            'task_id' => 'Task',
            'items' => 'Items',
            'carrier_tracking_status' => 'Tracking status',
        ]
    ],
    'help-video' => [
        'title' => 'Help Videos',

        'actions' => [
            'index' => 'Help Videos',
            'create' => 'New Help Video',
            'edit' => 'Edit :name',
        ],
        'columns' => [
            'id' => "ID",
            'title' => "Title",
            'description' => "Description",

        ],
        'errors' => [
            'bad_browser' => 'Sorry, but your browser does not support HTML 5 video in WEBM and MP4 formats',
            'wait_for_processing' => 'Video is under processing. Please reload page little bit later.'
        ]
    ],
    'needcalls' => [
        'title' => 'Need calls',
        'get_empl' => 'Take for call',
        'process_empl' => 'Process employee',
        'columns' => [
            'status' => 'Status',
            'cs_comment' => 'Call support comment'
        ],
        'statuses' => [
            'applicant' => 'Applicant',
            'approved' => 'Approved',
            'rejected' => 'Rejected',
            'voicemail' => 'Voice mail',
        ],
        'errors' => [
            'not_yours' => 'This user is already taken by other Call support'
        ]
    ],

    'mail-template' => [
        'title' => 'Mail Templates',

        'actions' => [
            'index' => 'Mail Templates',
            'create' => 'New Mail Template',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'title' => "Title",
            'subject' => "Subject",
            'body' => "Body",
            'created_at' => 'Created',
            'vars_you_can_use' => 'You can use variables listed below:'

        ],
        'vars' => [
            'firstname' => 'Recipient first name',
            'lastname' => 'Recipient last name',
            'state' => 'Recipient state',
            'address' => 'Company state',
            'homephone' => 'Recipient home phone',
            'workphone' => 'Recipient work phone',
            'position' => 'Company position',
            'docs' => 'docs',
            'zip' => 'Recipient zip',
            'signname' => 'Sign name',
            'signeddate' => 'Signed date',
            'signed' => 'Signed',
            'email' => 'Recipient email',
            'username' => 'Recipient username',
            'manager' => 'Company manager',
            'investorslist' => 'Investors list',
            'lastactivity' => 'Last activity',
            'application' => 'Application',
            'invprj' => 'Inv prj',
            'projectname' => 'Project name',
            'projectlink' => 'Project link',
            'webmailuser' => 'Webmail user',
            'controlpanellink' => 'Control panel link',
            'signlink' => 'Sign link',
            'webmaillink' => 'Webmail link',
            'restorepasslink' => 'Restore password link',
            'companyname' => 'Company name',
            'sitelink' => 'Site link',
            'supemail' => 'Support email',
            'supname' => 'Support name',
            'companytype' => 'Company type',
            'companyaddress' => 'Company address',
            'companycity' => 'Company city',
            'companycountry' => 'Company country',
            'companystate' => 'Company state',
            'companyzip' => 'Company zip',
            'companyphone' => 'Company phone',
            'companyfax' => 'Company fax',

        ]
    ],
    'chat' => [
        'actions' => [
            'index' => 'Chat'
        ]
    ],
    'field' => [
        'title' => 'Fields',

        'actions' => [
            'index' => 'Fields',
            'create' => 'New Field',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'form_id' => "Form",
            'field_type_id' => "Field type",
            'name' => "Name",
            'title' => "Title",
            'description' => "Description",
            'options' => "Options",
            'rules' => "Rules",
            'enabled' => "Enabled",
            'ordering' => "Ordering",
            'sortable' => "Sortable",
            'searchable' => "Searchable",
            'visible' => "Visible",
            'exportable' => "Exportable",
            'projects' => "Projects",

        ],
    ],
    'employee' => [
        'verify_email_to_pass_interview' => 'Verify you Email Address to pass the Interview',
        'rejected' => 'You haven’t passed the interview yet.',
        'notifications' => [
            'user_id_approved' => [
                'body' => 'I’m glad to inform you that you successfully passed the interview and your candidacy was approved for the position of Warehouse Coordinator. Before you start, you have to sign the Employment Agreement and W-4 Form in order to be able to to withhold the correct federal income tax from your pay. Please, press the link below to download the documents on your PC:',
                'download_contract' => 'Download Contract.',
                'upload_contract' => 'Upload Contract to the Form in your Profile.',
                'greeting' => 'Your Account is Approved.',
            ]
        ],
    ],

    'form' => [
        'title' => 'Forms',

        'actions' => [
            'index' => 'Forms',
            'create' => 'New Form',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'projects' => "Projects",
        ],
    ],

    'history' => [
        'title' => 'History',
        'user' => 'User',

        'actions' => [
            'index' => 'History',
            'create' => 'New Activity Log',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'user_id' => "User",
            'action' => "Action",
            'date' => "Date",
            'ip' => "Ip",
            'project' => "Contragent",
            'date_start' => "Date start",
            'date_end' => "Date end",
            'ip_address' => "Ip address",
        ],
    ],
    'notification' => [
        'date_from' => 'Date From',
        'date_to' => 'Date To',
        'event' => 'Event',
        'search' => 'Search',
    ],
    'businessprocess' => [
        'add_notification' => 'Add Notification',
        'add_action' => 'Add Action',
        'who_to_notify' => 'Who to notify',
        'users' => 'Users',
        'put_event_here' => 'Put Event Here',
        'select_mail_template' => 'Select Mail Template',
        'title' => 'Business Processes',
        'short_title' => 'Business P',
        'errors' => [
            'select_users' => 'Select Users',
            'select_mail_template' => 'Select Mail Template',
        ],
        'columns' => [
            'id' => "ID",
            'title' => "Title",
            'created_at' => "Date",
            'active' => "Active",
            'business_process_type_id' => 'Process type'
        ],
        'actions' => [
            'index' => 'Business Processes',
            'create' => 'New Business Process',
            'edit' => 'Edit :name',
        ],
    ],

    'mailing' => [
        'resend' => 'Resend',
        'start_stop_sending' => 'Immediately start/stop sending',
        'resend_info' => 'Mails will be sent to every recipient that was not registered, failed or canceled',
        'mailing_stopped' => 'Mailing sending stopped',
        'title' => 'Mailings',

        'actions' => [
            'index' => 'Mailings',
            'create' => 'New Mailing',
            'edit' => 'Edit :name',
        ],
        'mailing_added_to_queue' => 'Mailing added to queue',
        'failed_to_add_mailing_to_queue' => 'Failed to add Mailing to queue',
        'add_to_queue' => 'Add to queue',
        'history' => 'History',
        'emails_comma_separated_without_spaces' => 'Emails (comma separated without spaces)',
        'test' => 'Test',
        'run_test' => 'Run Test',
        'check_emails' => 'Check Email',
        'something_went_wrong' => 'Something went wrong',
        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'mail_template_id' => "Mail template",
            'user_id' => "User",
            'scheduled_at' => "Scheduled at",
            'scheduled_at_from' => "Scheduled at from",
            'scheduled_at_to' => "Scheduled at to",
            'email_groups' => "Mail Groups",
            'email_group_id' => 'Email Group',
            'stats' => 'Statistics',
            'settings' => 'Mail settings',
            'max_per_min' => 'Max mails per minute'
        ],
    ],

    'email-group' => [
        'title' => 'Email Groups',

        'actions' => [
            'index' => 'Email Groups',
            'create' => 'New Group',
            'edit' => 'Edit :name',
            'list' => 'Mails list'
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'csv' => "CSV file",
            'user_id' => "User",

        ],
    ],

    'email-mailing' => [
        'title' => 'Emails',

        'actions' => [
            'index' => 'Emails',
            'create' => 'New Email',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'email' => "Email",
            'email_group_id' => "Group",

        ],
    ],

    'setting' => [
        'title' => 'Settings',

        'actions' => [
            'index' => 'Settings',
            'create' => 'New Setting',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'value' => "Value",

        ],
    ],
    'history-mailing' => [
        'title' => 'History',

        'actions' => [
            'index' => 'History',
            'create' => 'New History',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'mailing_id' => 'Mailing',
            'mailing_status_id' => 'Status',
            'email_mailing_id' => 'Email',
            'created_at' => 'Date',
        ],
    ],
    'statistic' => [
        'title' => 'Statistics',

        'actions' => [
            'index' => 'Statistics',
            'create' => 'New Statistics',
            'edit' => 'Edit :name',
        ],
        'by_user_status' => 'By User Status',
        'by_completed_tasks' => 'By Completed Tasks',
        'by_mailings' => 'By Emailings',
        'referrers' => 'Referrers',
        'tasks_counts' => 'Total',
        'history_mailings_count' => 'Total',
        'registered' => 'Registered',
        'approved' => 'Approved',
        'rejected' => 'Rejected',
        'delivery_status_bad' => 'Bad',
        'delivery_status_fast' => 'Fast',
        'delivery_status_medium' => 'Medium',
        'delivery_status_slow' => 'Slow',
        'delivery_status_super_fast' => 'Super Fast',
        'columns' => [
        ],
    ],
    'chatroom' => [
        'title' => 'Chat'
    ],
    'mail_template' => [
        'title' => 'Mail Templates',
        'columns' => [
            'title' => 'Title',
            'description' => 'Description',
        ]
    ],
    'help_video' => [
        'title' => 'Help Videos',
        'columns' => [
            'title' => 'Title',
            'description' => 'Description',
        ]
    ],
    'business-process' => [
        'title' => 'Business Process'
    ],

    'item' => [
        'title' => 'Items',

        'actions' => [
            'index' => 'Items',
            'create' => 'New Item',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'size' => "Size",
            'color' => "Color",
            'price' => "Price",
            'info' => "Info",
            'link' => "Link",
            'package_id' => "Package",
            'user_id' => "User",
            'qty' => "Quantity",
            'employee_id' => "Employee",
            'created_at' => "Created",
            'warehousable_type' => "Type",
            'weight' => "Weight",
        ],
    ],

    'nomenclature' => [
        'title' => 'Nomenclatures',

        'actions' => [
            'index' => 'Nomenclatures',
            'create' => 'New Nomenclature',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",

        ],
    ],

    'sales-task' => [
        'empty_price' => 'Price can not be empty',
        'title' => 'Sales Tasks',
        'task_id' => 'Sales Task № :id',
        'actions' => [
            'index' => 'Sales Tasks',
            'create' => 'New Sales Task',
            'edit' => 'Edit :name',
            'csv' => 'Create Csv',
        ],

        'columns' => [
            'id' => "ID",
            'delivered_at' => "Delivered at",
            'warehouse_id' => "Warehouse",
            'carrier_id' => "Carrier",
            'track' => "Track",
            'sku' => "Sku",
            'employee_status_id' => "Employee status",
            'comment' => "Comment",
            'creator_id' => "Creator",
            'manager_id' => "Manager",
            'employee_id' => "Employee",
            'sales_status_id' => "Sales status",
            'label' => 'Label',
            'barcode' => 'Barcode',
            'receipt' => 'Receipt',
            'courier_status_id' => 'Courier Status',
            'delivery_status_id' => 'Delivery Status',
            'invoice' => 'Invoice',
            'content' => 'Content',

        ],
    ],
    'warehousable' => [
        'title' => 'Warehouse Items',
        'create_sales_task' => 'Create Sales Task',
        'item_history' => 'Item History',
        'actions' => [
            'index' => 'Warehouse Items',
            'item' => 'Item History',
        ],

        'columns' => [
            'id' => "ID",
        ],
    ],

    'dkim-key' => [
        'title' => 'Dkim Keys',

        'actions' => [
            'index' => 'Dkim Keys',
            'create' => 'New Dkim Key',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'project_id' => "Project",
            'domain' => "Domain",
            'dkim_record' => "Dkim record",

        ],
    ],

    'investor' => [
        'title' => 'Employees HR manager invest',

        'actions' => [
            'index' => 'Investor',
            'create' => 'New investor',
        ],
        'buttons' => [
            'add_task' => 'Add task',
            'add_investors' => 'Add investors',
            'add_invest_project' => 'Add invest Project',
            'send_mail_template' => 'Send Mail Template',
            'remove' => 'Remove'
        ],
        'columns' => [
            'name' => "Name",
            'email' => "Email",
            'address' => "Address",
            'phones' => "Phones",
            'last_activity' => "Last activity",
            'last_mail_template' => "Last mail template",
            'who_manager' => "Who manager",
            'project' => "Project",
            'when_join' => "When join",
            'chat' => "Chat",
            'sms' => "sms",
            'info' => "Info",
            'call_support_comment' => "Call support comment",
        ],
    ],

    'investor-list' => [
        'title' => 'Investor Lists',

        'actions' => [
            'index' => 'Investor Lists',
            'create' => 'New Investor List',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'investors' => "Investors"

        ],
    ],

    'invest-task' => [
        'title' => 'Invest Tasks',

        'actions' => [
            'index' => 'Invest Tasks',
            'create' => 'New Invest Task',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'theme' => "Theme",
            'text' => "Text",
            'time_left' => "Time left",
            'deadline' => "Deadline",
            'investors' => "Investors",
            'employee_id' => "Employee",
            'manager_id' => "Manager",
            'creator_id' => "Creator",
            'contragents' => "ICO projects",
            'investor_lists' => "Investor lists",
            'contragent_lists' => "Contragent lists",
            'invest_task_status' => "Status",
            'invest_task_status_all' => "All",
            'created_at' => "Created",
            'files' => "Files",
            'template' => 'Template',
            'task' => 'Task',
            'add_task' => 'Add invest task',
            'description' => "Description",
            'attachments' => "Attachments",
            'per_page' => "Per page",
            'remote' => "Remote"

        ],

        'task_id' => 'Invest Task № :id',
        'status_changed' => 'Task Status changed: :status',
        'task_updated' => 'Ivest task №:id. :text',
        'task_created' => 'New Ivest task created.',

        // TODO check if needed
        'employee_uploaded_file' => 'Employee uploaded Document :mediaCollection',
        'employee_delayed_task' => 'Employee :name delayed Task.',
        'employee_did_not_upload_task_files' => 'Employee :name did not upload task file :mediaCollection.',
        'projects' => 'Projects',
    ],

    'contragent' => [
        'title' => 'ICO projects',

        'actions' => [
            'index' => 'ICO projects',
            'create' => 'New Contragent',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'investing_amount' => "Investing amount",
            'user_id' => "User",
            'country' => "Country",
            'roi' => "Roi",
            'media' => 'Files',
            'business_processes' => 'Business processes'
        ],
    ],

    'auto-answer' => [
        'title' => 'Auto Answers',

        'actions' => [
            'index' => 'Auto Answers',
            'create' => 'New Auto Answer',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'user_id' => "User",
            'mailsetting_id' => "Mailsetting",
            'mail' => 'Mail',
            'owner' => 'Owner',
            'templates' => 'Templates',
            'message_count' => 'Order number of reply',
            'delay' => 'Delay'

        ],
    ],

    'contragent-list' => [
        'title' => 'Contragent Lists',

        'actions' => [
            'index' => 'Contragent Lists',
            'create' => 'New Contragent List',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'contragents' => "ICO projects"

        ],
    ],
    'header' => [
        'investors' => 'Investors',
        'investors_mail' => 'Investors mail',
        'contragents' => 'ICO projects',
        'task' => 'Task',
        'invest_task' => 'Invest task',
    ],

    'invest-task-template' => [
        'title' => 'Invest Task Templates',
        'title_short' => 'Invest Task Tmpl',

        'actions' => [
            'index' => 'Invest Task Templates',
            'create' => 'New Invest Task Template',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => "ID",
            'name' => "Name",
            'theme' => "Theme",
            'text' => "Text",
            'description' => "Description",
            'manager_id' => "Manager",
            'mail_template_id' => "Mail template",

        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];
