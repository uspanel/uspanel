<?php

return [
    'new_user_has_registered' => ':name has Registered',
    'user_blacklisted' => ':name Blacklisted',
    'user_media_updated' => ':name Uploaded Media To :mediaCollectionName Collection',
    'user_status_approved' => ':name Approved',
    'user_uploaded_document' => ':document was uploaded for :model',
    'user_got_new_investor' => 'User :user got new investor :investor',
    'user_got_new_contragent' => 'User :user got new contragent :contragent',
    'user_passed_interview' => 'User :name passed interview',
    'user_start_work' => 'User :name started to work',
    'user_has_not_changed_status_on_first_task' => "User :name hasn't changed status on first task",
    'user_got_task' => "User :user got new task :task",
    'user_got_first_task' => "User :user got first task :task",
    'user_got_answer_from_investor' => "User :user got answer from investor :investor",
    'user_done_first_task' => "User :user done first task :task",

    'pack_status_changed' => 'Status of package :id was changed to :status',
    'pack_created' => 'Package :id was created',
    'pack_edited' => 'Package :id was edited',

    'email_test_failed_for' => 'Emails was not sent for emails :emails',
    'new_email_account_subject' => 'New email account',
    'new_email_account_body' => 'New email account was registered. Login: :login Password: :password',

    'new_message_in_chat' => 'New message in chat',
    'sms_sent' => 'SMS sent to user',
    'missed_call' => 'You have missed call from :caller',

    'task_created' => 'Task :id has been created',
    'task_updated' => 'Task :id has been updated',
    'task_status_changed' => 'Status of task :id was changed to :status',
    'task_expired' => 'Task :id was expired',

    'invest_task_created' => 'Invest task :id has been created',
    'invest_task_updated' => 'Invest task :id has been updated',
    'invest_task_status_changed' => 'Status of invest task :id was changed to :status',
    'invest_task_expired' => 'Invest task :id was expired',
];
