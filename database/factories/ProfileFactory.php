<?php

use Faker\Generator as Faker;

$factory->define(App\Profile::class, function (Faker $faker) {
    return [
    	'user_id' => factory(\App\User::class)->create()->id,
        'city' => $faker->city,
        'state_id' => 1,
        'address' => $faker->address,
        'zip' => $faker->randomNumber(5),
        'higher_education' => true,
        'authorized_to_work' => true,
        'adult' => true,
        'date_of_birth' => now(),
        'work_phone' => $faker->phoneNumber,
        'home_phone' => $faker->phoneNumber,
    ];
});
