<?php

use Faker\Generator as Faker;
use App\User;

$factory->define(App\Company::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'city' => $faker->city,
        'state_id' => 1,
        'address' => $faker->address,
        'zip' => $faker->randomNumber(5),
        'phone' => $faker->phoneNumber,
        'second_phone' => $faker->phoneNumber,
        'fax' => $faker->phoneNumber,
        'info_email' => $faker->email,
        'company_type_id' => $faker->randomElement([1, 2])
    ];
});
