<?php

use Faker\Generator as Faker;

$factory->define(App\Project::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'description' => $faker->text,
        'salary' => $faker->randomNumber(4),
        'company_id' => factory(App\Company::class)->create()->id,
    ];
});
