<?php

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Warehouse::class, function (Faker\Generator $faker) {
    return [


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Task::class, function (Faker\Generator $faker) {
    return [


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\HelpVideo::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'description' => $faker->text(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
    ];
});

$factory->define(App\Models\Package::class, function (Faker\Generator $faker) {
    return [


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\MailTemplate::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'body' => $faker->text(),
        'subject' =>$faker->text(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Field::class, function (Faker\Generator $faker) {
    return [
        'form_id' => $faker->randomNumber(5),
        'field_type_id' => $faker->randomNumber(5),
        'name' => $faker->firstName,
        'title' => $faker->sentence,
        'description' => $faker->sentence,
        'rules' => $faker->text(),
        'enabled' => $faker->boolean(),
        'ordering' => $faker->boolean(),
        'sortable' => $faker->boolean(),
        'searchable' => $faker->boolean(),
        'visible' => $faker->boolean(),
        'exportable' => $faker->boolean(),
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,

        'options' => ['en' => $faker->sentence],

    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\ActivityLogDeprecated::class, function (Faker\Generator $faker) {
    return [
        'user_id' => $faker->randomNumber(5),
        'text' => $faker->sentence,
        'ip_address' => $faker->sentence,
    ];
});

$factory->define(App\Form::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,

    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Mailing::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'mail_template_id' => $faker->randomNumber(5),
        'created_by' => $faker->randomNumber(5),
        'scheduled_at' => $faker->dateTime,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\EmailGroup::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_by' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\EmailMailing::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'email' => $faker->email,
        'email_group_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Setting::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'value' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Role::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'guard_name' => 'web',
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(CodersStudio\Chat\App\Chatroom::class, function (Faker\Generator $faker) {
    return [


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Item::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'size' => $faker->sentence,
        'color' => $faker->sentence,
        'info' => $faker->text(),
        'link' => $faker->sentence,
        'user_id' => App\User::role('Administrator')->first()->id,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Nomenclature::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\SalesTask::class, function (Faker\Generator $faker) {
    return [
        'delivered_at' => $faker->dateTime,
        'warehouse_id' => $faker->randomNumber(5),
        'carrier_id' => $faker->sentence,
        'track' => $faker->sentence,
        'employee_status_id' => $faker->randomNumber(5),
        'comment' => $faker->sentence,
        'creator_id' => $faker->randomNumber(5),
        'manager_id' => $faker->randomNumber(5),
        'employee_id' => $faker->randomNumber(5),
        'sales_status_id' => $faker->randomNumber(5),
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\InvestTask::class, function (Faker\Generator $faker) {
    return [
        'comment' => $faker->sentence,
        'employee_id' => $faker->randomNumber(5),
        'manager_id' => $faker->randomNumber(5),
        'creator_id' => $faker->randomNumber(5),
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\DkimKey::class, function (Faker\Generator $faker) {
    return [
        'project_id' => $faker->randomNumber(5),
        'domain' => $faker->sentence,
        'dkim_record' => $faker->text(),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\InvestorList::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\InvestTask::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'theme' => $faker->sentence,
        'text' => $faker->sentence,
        'time' => $faker->time(),
        'investor_id' => $faker->randomNumber(5),
        'employee_id' => $faker->randomNumber(5),
        'manager_id' => $faker->randomNumber(5),
        'creator_id' => $faker->randomNumber(5),
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Contragent::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'investing_amount' => $faker->randomNumber(5),
        'user_id' => $faker->randomNumber(5),
        'country' => $faker->sentence,
        'roi' => $faker->sentence,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\AutoAnswer::class, function (Faker\Generator $faker) {
    return [
        'user_id' => $faker->randomNumber(5),
        'mailsetting_id' => $faker->randomNumber(5),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\ContragentList::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,


    ];
});

/** @var  \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\InvestTaskTemplate::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'theme' => $faker->sentence,
        'text' => $faker->sentence,
        'description' => $faker->sentence,
        'manager_id' => $faker->randomNumber(5),
        'mail_template_id' => $faker->randomNumber(5),
        'deleted_at' => null,
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
        
        
    ];
});

