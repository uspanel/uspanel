<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;
use App\EmailMailing;
use App\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
	$firstName = $faker->firstNameMale;
	$lastName = $faker->lastName;
	$name = $firstName . ' ' . $lastName;
    $email = $firstName . $lastName . '@coders.studio';
    $emails = User::select('email')->get()->pluck('email')->toArray();
    $email = EmailMailing::whereNotIn('email', $emails)->first()->email ?? $email;
    return [
        'first_name' => $firstName,
        'last_name' => $lastName,
        'address' => $faker->address,
        'email' => $email,
        'email_verified_at' => now(),
        'password' => '123123',
        'remember_token' => Str::random(10),
        'phone_number' => $faker->phoneNumber,
    ];
});
