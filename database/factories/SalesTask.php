<?php

use Faker\Generator as Faker;

$factory->define(App\SalesTask::class, function (Faker $faker) {
    $data = [
        'created_at' => $faker->dateTimeBetween($startDate = '-30 days', $endDate = '-10 days'),
        'delivered_at' => $faker->dateTimeBetween($startDate = '-10 days', $endDate = 'now'),
        'carrier_id' => \App\Carrier::first()->id,
        'track' => $faker->numberBetween(000000, 999999),
        'employee_status_id' => \App\EmployeeStatus::first()->id,
        'comment' => $faker->sentence(),
        'creator_id' => App\User::role('Administrator')->first()->id,
        'manager_id' => App\User::role('HR Manager Staff')->first()->id,
        'employee_id' => App\User::role('Employee Staff')->first()->id,
        'warehouse_id' => factory(\App\Warehouse::class)->create()->id,
        'delivered_at' => now(),
        'sales_status_id' => 2,
    ];
    return $data;
});
