<?php

use Faker\Generator as Faker;

$factory->define(App\MailTemplate::class, function (Faker $faker) {
    return [
        'title' => $faker->unique()->words(rand(1, 4), true),
        'name' => $faker->words(rand(1, 4), true),
        'subject' => $faker->words(rand(1, 4), true),
        'body' => $faker->text(),
        'deletable' => true,
    ];
});
