<?php

use Faker\Generator as Faker;

$factory->define(App\EmailMailing::class, function (Faker $faker) {
	$name = $faker->firstName;
    return [
        'name' => $name,
        'email' => $name . '@coders.studio.com',
    ];
});
