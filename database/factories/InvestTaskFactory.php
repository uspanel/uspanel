<?php

use Faker\Generator as Faker;

$factory->define(App\InvestTask::class, function (Faker $faker) {
    return [
        "name" => $faker->text(50),
        "theme" => $faker->text(50),
        "text" => $faker->text(50),
        "investor_id" => 1,
        "employee_id" => 1,
        "manager_id" => 1,
        "creator_id" => 1,
    ];
});
