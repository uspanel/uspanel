<?php

use Faker\Generator as Faker;

$factory->define(App\EmailGroup::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
    ];
});
