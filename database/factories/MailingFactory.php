<?php

use Faker\Generator as Faker;

$factory->define(App\Mailing::class, function (Faker $faker) {
    return [
		'name' => $faker->words(rand(1, 4), true),
		'scheduled_at' => \Carbon\Carbon::now()->addDays(10),
    ];
});
