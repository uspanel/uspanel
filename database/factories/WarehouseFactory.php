<?php

use Faker\Generator as Faker;

$factory->define(App\Warehouse::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'city' => $faker->city,
        'address' => $faker->address,
    ];
});
