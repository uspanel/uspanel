<?php

use Faker\Generator as Faker;

$factory->define(App\HistoryMailing::class, function (Faker $faker) {
    return [
        'mailing_status_id' => rand(1, 5)
    ];
});
