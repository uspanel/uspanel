<?php

use Faker\Generator as Faker;

$factory->define(App\Item::class, function (Faker $faker) {
    return [
        'color' => $faker->colorName,
        'name' => $faker->word,
        'size' => $faker->word,
        'link' => $faker->url,
        'info' => $faker->sentence(10),
        'weight' => 10,
        'user_id' => App\User::role('Administrator')->first()->id,
    ];
});
