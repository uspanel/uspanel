<?php

use Faker\Generator as Faker;

$factory->define(App\MailingSending::class, function (Faker $faker) {
    return [
        'scheduled_at' => \Carbon\Carbon::now()
    ];
});
