<?php

use Faker\Generator as Faker;

$factory->define(App\Package::class, function (Faker $faker) {
    return [
        'track' => $faker->numberBetween(1, 100),
        'carrier_id' => $faker->numberBetween(1, 4),
        'weight' => $faker->numberBetween(1, 100),
        'name' => $faker->word,
        'creator_id' => App\User::role('Administrator')->first()->id,
        'manager_id' => App\User::role('Administrator')->first()->id,
        'comment' => $faker->sentence(4),
        'status_id' => $faker->numberBetween(1, 2),
        'description' => $faker->sentence(10),
        //'delivered_at' => now(),
    ];
});
