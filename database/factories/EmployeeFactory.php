<?php

use Faker\Generator as Faker;

$factory->define(App\Employee::class, function (Faker $faker) {
    return [
        'user_id' => factory(App\Profile::class)->create()->user_id,
        'interviewed' => true,
        'user_status_id' => 4,
    ];
});
