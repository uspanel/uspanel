<?php

use Faker\Generator as Faker;

$factory->define(App\Task::class, function (Faker $faker) {
    return [
        'created_at' => $faker->dateTimeBetween($startDate = '-30 days', $endDate = '-10 days'),
        'delivered_at' => $faker->dateTimeBetween($startDate = '-10 days', $endDate = 'now'),
        'carrier_id' => $faker->numberBetween(1, 4),
        'track' => $faker->numberBetween(000000, 999999),
        'employee_status_id' => $faker->numberBetween(1, 2),
        'comment' => $faker->sentence(),
        'creator_id' => App\User::role('Administrator')->first()->id,
        'manager_id' => App\User::role('HR Manager Staff')->first()->id,
        'employee_id' => App\User::role('Employee Staff')->first()->id,
        'warehouse_id' => factory(\App\Warehouse::class)->create()->id,
        'delivered_at' => now(),
    ];
});
