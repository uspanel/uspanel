<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailGroupMailingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_group_mailing', function (Blueprint $table) {
            $table->integer('mailing_id')->unsigned();
            $table->integer('email_group_id')->unsigned();
            $table->foreign('mailing_id')->references('id')->on('mailings')->onDelete('cascade');
            $table->foreign('email_group_id')->references('id')->on('email_groups');
            $table->unique(['mailing_id', 'email_group_id']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('email_group_mailing');
        Schema::enableForeignKeyConstraints();
    }
}
