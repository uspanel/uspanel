<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\EmployeeStatus;

class FillEmployeeStatusesTable extends Migration
{
    protected $data = [
        [
            'name' => 'got'
        ],
        [
            'name' => 'sent'
        ]
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        EmployeeStatus::insert($this->data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->data as $key => $value) {
            EmployeeStatus::where('name', $value)->delete();
        }
    }
}
