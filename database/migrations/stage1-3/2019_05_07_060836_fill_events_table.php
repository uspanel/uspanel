<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Event;

class FillEventsTable extends Migration
{

    protected $data = [
        [
            'name' => 'PackageStatusChanged',
            'title' => 'Package status changed',
        ],
        [
            'name' => 'PackageCreated',
            'title' => 'Package created',
        ],
        [
            'name' => 'PackageEdited',
            'title' => 'Package edited',
        ],
        [
            'name' => 'TaskStatusChanged',
            'title' => 'Task status changed',
        ],
        [
            'name' => 'UserUploadedDocument',
            'title' => 'User uploaded document',
        ],
        [
            'name' => 'UserRegistered',
            'title' => 'User registered',
        ],
        [
            'name' => 'TaskExpired',
            'title' => 'Task expired',
        ],
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Event::insert($this->data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->data as $key => $value) {
            Event::where('name', $value['name'])->delete();
        }
    }
}
