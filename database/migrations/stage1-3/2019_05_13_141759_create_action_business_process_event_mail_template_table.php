<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionBusinessProcessEventMailTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_business_process_event_mail_template', function (Blueprint $table) {
            $table->unsignedInteger('action_business_process_event_id');
            $table->unsignedInteger('mail_template_id');
            $table->foreign('action_business_process_event_id', 'a_b_p_e_id_foreign')
            ->references('id')->on('action_business_process_events')
            ->onDelete('cascade');
            $table->unique('action_business_process_event_id', 'mail_template_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_business_process_event_mail_template');
    }
}
