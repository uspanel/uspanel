<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class FillPermissionsForUser extends Migration
{
    /**
     * @var  \Illuminate\Config\Repository|mixed
     */
    protected $guardName;
    /**
     * @var  array
     */
    protected $permissions;
    /**
     * @var  array
     */
    protected $roles;

    /**
     * FillPermissionsForUser constructor.
     */
    public function __construct()
    {
        $this->guardName = config('admin-auth.defaults.guard');

        $permissions = collect([
            'crud.user.view',
            'crud.user.create',
            'crud.user.edit',
            'crud.user.delete',
            'crud.user.show',
            'crud.user.callsupport',
            'crud.user.columns.employee_user_status_id.read',
            'crud.user.columns.employee_call_support_comment.read',
            'crud.user.columns.roles.read',
            'crud.user.columns.tasks.read',
            'crud.user.columns.id.read',
            'crud.user.columns.name.read',
            'crud.user.columns.email.read',
            'crud.user.columns.email_verified_at.read',
            'crud.user.columns.password.read',
            'crud.user.columns.deleted_at.read',
            'crud.user.columns.created_at.read',
            'crud.user.columns.updated_at.read',
            'crud.user.columns.first_name.read',
            'crud.user.columns.last_name.read',
            'crud.user.columns.address.read',
            'crud.user.columns.phone_number.read',
            'crud.user.columns.id.write',
            'crud.user.columns.name.write',
            'crud.user.columns.email.write',
            'crud.user.columns.email_verified_at.write',
            'crud.user.columns.password.write',
            'crud.user.columns.deleted_at.write',
            'crud.user.columns.created_at.write',
            'crud.user.columns.updated_at.write',
            'crud.user.columns.first_name.write',
            'crud.user.columns.last_name.write',
            'crud.user.columns.address.write',
            'crud.user.columns.phone_number.write',
            'crud.user.columns.roles.write',
            'crud.user.columns.employee_project_id.write',
            'crud.user.columns.receive_external_calls.write',
            'crud.user.columns.profile_address.write',
            'crud.user.columns.profile_city.write',
            'crud.user.columns.profile_state_id.write',
            'crud.user.columns.profile_zip.write',
            'crud.user.columns.profile_authorized_to_work.write',
            'crud.user.columns.profile_adult.write',
            'crud.user.columns.profile_higher_education.write',
            'crud.user.columns.employee_user_status_id.read',
            'crud.user.columns.employee_user_status_id.write',
            'crud.user.columns.staffer.read',
            'crud.user.columns.staffer.write',
        ]);

        //Add New permissions
        $this->permissions = $permissions->map(function ($permission) {
            return [
                'name' => $permission,
                'guard_name' => $this->guardName,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ];
        })->toArray();

        //Role should already exists
        $this->roles = [
            [
                'name' => 'Administrator',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
            [
                'name' => 'HR Manager Staff',
                'guard_name' => $this->guardName,
                'permissions' => [
                    'crud.user.view',
                    'crud.user.show',
                    'crud.user.create',
                    'crud.user.edit',
                    'crud.user.columns.employee_user_status_id.read',
                    'crud.user.columns.employee_call_support_comment.read',
                    'crud.user.columns.roles.read',
                    'crud.user.columns.tasks.read',
                    'crud.user.columns.id.read',
                    'crud.user.columns.name.read',
                    'crud.user.columns.email.read',
                    'crud.user.columns.created_at.read',
                    'crud.user.columns.first_name.read',
                    'crud.user.columns.last_name.read',
                    'crud.user.columns.address.read',
                    'crud.user.columns.phone_number.read',
                    'crud.user.columns.employee_user_status_id.write',
                    'crud.user.columns.staffer.read',
                    'crud.user.columns.staffer.write',
                    'crud.user.columns.id.write',
                    'crud.user.columns.name.write',
                    'crud.user.columns.email.write',
                    'crud.user.columns.email_verified_at.write',
                    'crud.user.columns.password.write',
                    'crud.user.columns.deleted_at.write',
                    'crud.user.columns.created_at.write',
                    'crud.user.columns.updated_at.write',
                    'crud.user.columns.first_name.write',
                    'crud.user.columns.last_name.write',
                    'crud.user.columns.address.write',
                    'crud.user.columns.phone_number.write',
                    'crud.user.columns.employee_project_id.write',
                    'crud.user.columns.receive_external_calls.write',
                    'crud.user.columns.profile_address.write',
                    'crud.user.columns.profile_city.write',
                    'crud.user.columns.profile_state_id.write',
                    'crud.user.columns.profile_zip.write',
                    'crud.user.columns.profile_authorized_to_work.write',
                    'crud.user.columns.profile_adult.write',
                    'crud.user.columns.profile_higher_education.write',
                ],
            ],
            [
                'name' => 'HR Manager Invest',
                'guard_name' => $this->guardName,
                'permissions' => [
                    'crud.user.view',
                    'crud.user.show',
                    'crud.user.edit',
                    'crud.user.columns.employee_user_status_id.read',
                    'crud.user.columns.employee_call_support_comment.read',
                    'crud.user.columns.roles.read',
                    'crud.user.columns.tasks.read',
                    'crud.user.columns.id.read',
                    'crud.user.columns.name.read',
                    'crud.user.columns.email.read',
                    'crud.user.columns.email_verified_at.read',
                    'crud.user.columns.created_at.read',
                    'crud.user.columns.updated_at.read',
                    'crud.user.columns.first_name.read',
                    'crud.user.columns.last_name.read',
                    'crud.user.columns.address.read',
                    'crud.user.columns.phone_number.read',
                    'crud.user.columns.id.write',
                    'crud.user.columns.name.write',
                    'crud.user.columns.email.write',
                    'crud.user.columns.email_verified_at.write',
                    'crud.user.columns.first_name.write',
                    'crud.user.columns.last_name.write',
                    'crud.user.columns.address.write',
                    'crud.user.columns.phone_number.write',
                    'crud.user.columns.roles.write',
                    'crud.user.columns.employee_project_id.write',
                    'crud.user.columns.receive_external_calls.write',
                    'crud.user.columns.profile_address.write',
                    'crud.user.columns.profile_city.write',
                    'crud.user.columns.profile_state_id.write',
                    'crud.user.columns.profile_zip.write',
                    'crud.user.columns.profile_authorized_to_work.write',
                    'crud.user.columns.profile_adult.write',
                    'crud.user.columns.profile_higher_education.write',
                    'crud.user.columns.employee_user_status_id.read',
                ],
            ],
            [
                'name' => 'Staffer',
                'guard_name' => $this->guardName,
                'permissions' => [
                    'crud.user.view',
                    'crud.user.show',
                    'crud.user.columns.employee_user_status_id.read',
                    'crud.user.columns.employee_call_support_comment.read',
                    'crud.user.columns.roles.read',
                    'crud.user.columns.tasks.read',
                    'crud.user.columns.id.read',
                    'crud.user.columns.name.read',
                    'crud.user.columns.email.read',
                    'crud.user.columns.created_at.read',
                    'crud.user.columns.first_name.read',
                    'crud.user.columns.last_name.read',
                    'crud.user.columns.address.read',
                    'crud.user.columns.phone_number.read',
                    'crud.user.columns.employee_user_status_id.write',
                ],
            ],
            [
                'name' => 'Support Staff',
                'guard_name' => $this->guardName,
                'permissions' => [
                    'crud.user.view',
                    'crud.user.show',
                    'crud.user.edit',
                    'crud.user.columns.employee_user_status_id.read',
                    'crud.user.columns.employee_call_support_comment.read',
                    'crud.user.columns.roles.read',
                    'crud.user.columns.tasks.read',
                    'crud.user.columns.id.read',
                    'crud.user.columns.name.read',
                    'crud.user.columns.email.read',
                    'crud.user.columns.created_at.read',
                    'crud.user.columns.first_name.read',
                    'crud.user.columns.last_name.read',
                    'crud.user.columns.address.read',
                    'crud.user.columns.phone_number.read',
                    'crud.user.columns.staffer.read',
                    'crud.user.columns.staffer.write',
                ],
            ],
            [
                'name' => 'Support Invest',
                'guard_name' => $this->guardName,
                'permissions' => [
                    'crud.user.view',
                    'crud.user.show',
                    'crud.user.edit',
                    'crud.user.columns.employee_user_status_id.read',
                    'crud.user.columns.employee_call_support_comment.read',
                    'crud.user.columns.roles.read',
                    'crud.user.columns.tasks.read',
                    'crud.user.columns.id.read',
                    'crud.user.columns.name.read',
                    'crud.user.columns.email.read',
                    'crud.user.columns.email_verified_at.read',
                    'crud.user.columns.created_at.read',
                    'crud.user.columns.updated_at.read',
                    'crud.user.columns.first_name.read',
                    'crud.user.columns.last_name.read',
                    'crud.user.columns.address.read',
                    'crud.user.columns.phone_number.read',
                    'crud.user.columns.id.write',
                    'crud.user.columns.name.write',
                    'crud.user.columns.email.write',
                    'crud.user.columns.email_verified_at.write',
                    'crud.user.columns.first_name.write',
                    'crud.user.columns.last_name.write',
                    'crud.user.columns.address.write',
                    'crud.user.columns.phone_number.write',
                    'crud.user.columns.roles.write',
                    'crud.user.columns.employee_project_id.write',
                    'crud.user.columns.receive_external_calls.write',
                    'crud.user.columns.profile_address.write',
                    'crud.user.columns.profile_city.write',
                    'crud.user.columns.profile_state_id.write',
                    'crud.user.columns.profile_zip.write',
                    'crud.user.columns.profile_authorized_to_work.write',
                    'crud.user.columns.profile_adult.write',
                    'crud.user.columns.profile_higher_education.write',
                    'crud.user.columns.employee_user_status_id.read',
                ],
            ],
            [
                'name' => 'Call support',
                'guard_name' => $this->guardName,
                'permissions' => [
                    'crud.user.view',
                    'crud.user.show',
                    'crud.user.callsupport',
                    'crud.user.columns.employee_user_status_id.read',
                    'crud.user.columns.employee_call_support_comment.read',
                    'crud.user.columns.roles.read',
                    'crud.user.columns.tasks.read',
                    'crud.user.columns.id.read',
                    'crud.user.columns.name.read',
                    'crud.user.columns.email.read',
                    'crud.user.columns.email_verified_at.read',
                    'crud.user.columns.password.read',
                    'crud.user.columns.deleted_at.read',
                    'crud.user.columns.created_at.read',
                    'crud.user.columns.updated_at.read',
                    'crud.user.columns.first_name.read',
                    'crud.user.columns.last_name.read',
                    'crud.user.columns.address.read',
                    'crud.user.columns.phone_number.read',
                    'crud.user.columns.id.write',
                    'crud.user.columns.name.write',
                    'crud.user.columns.email.write',
                    'crud.user.columns.email_verified_at.write',
                    'crud.user.columns.password.write',
                    'crud.user.columns.deleted_at.write',
                    'crud.user.columns.created_at.write',
                    'crud.user.columns.updated_at.write',
                    'crud.user.columns.first_name.write',
                    'crud.user.columns.last_name.write',
                    'crud.user.columns.address.write',
                    'crud.user.columns.phone_number.write',
                    'crud.user.columns.roles.write',
                    'crud.user.columns.employee_project_id.write',
                    'crud.user.columns.receive_external_calls.write',
                    'crud.user.columns.profile_address.write',
                    'crud.user.columns.profile_city.write',
                    'crud.user.columns.profile_state_id.write',
                    'crud.user.columns.profile_zip.write',
                    'crud.user.columns.profile_authorized_to_work.write',
                    'crud.user.columns.profile_adult.write',
                    'crud.user.columns.profile_higher_education.write',
                    'crud.user.columns.employee_user_status_id.read',
                ],
            ],
        ];
    }

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        DB::transaction(function () {
            foreach ($this->permissions as $permission) {
                $permissionItem = DB::table('permissions')->where([
                    'name' => $permission['name'],
                    'guard_name' => $permission['guard_name']
                ])->first();
                if (is_null($permissionItem)) {
                    DB::table('permissions')->insert($permission);
                }
            }

            foreach ($this->roles as $role) {
                $permissions = $role['permissions'];
                unset($role['permissions']);

                $roleItem = DB::table('roles')->where([
                    'name' => $role['name'],
                    'guard_name' => $role['guard_name']
                ])->first();
                if (!is_null($roleItem)) {
                    $roleId = $roleItem->id;

                    $permissionItems = DB::table('permissions')->whereIn('name', $permissions)->where('guard_name',
                        $role['guard_name'])->get();
                    foreach ($permissionItems as $permissionItem) {
                        $roleHasPermissionData = [
                            'permission_id' => $permissionItem->id,
                            'role_id' => $roleId
                        ];
                        $roleHasPermissionItem = DB::table('role_has_permissions')->where($roleHasPermissionData)->first();
                        if (is_null($roleHasPermissionItem)) {
                            DB::table('role_has_permissions')->insert($roleHasPermissionData);
                        }
                    }
                }
            }
        });
        app()['cache']->forget(config('permission.cache.key'));
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        DB::transaction(function () {
            foreach ($this->permissions as $permission) {
                $permissionItem = DB::table('permissions')->where([
                    'name' => $permission['name'],
                    'guard_name' => $permission['guard_name']
                ])->first();
                if (!is_null($permissionItem)) {
                    DB::table('permissions')->where('id', $permissionItem->id)->delete();
                    DB::table('model_has_permissions')->where('permission_id', $permissionItem->id)->delete();
                }
            }
        });
        app()['cache']->forget(config('permission.cache.key'));
    }
}
