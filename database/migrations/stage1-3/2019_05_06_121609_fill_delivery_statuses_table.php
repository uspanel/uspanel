<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\DeliveryStatus;

class FillDeliveryStatusesTable extends Migration
{

    protected $data = [
        [
            'name' => 'bad',
            'title' => 'Bad',
        ],
        [
            'name' => 'slow',
            'title' => 'Slow',
        ],
        [
            'name' => 'medium',
            'title' => 'Medium',
        ],
        [
            'name' => 'fast',
            'title' => 'Fast',
        ],
        [
            'name' => 'super_fast',
            'title' => 'Super Fast',
        ],

    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DeliveryStatus::insert($this->data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->data as $key => $value) {
            DeliveryStatus::where('name', $value['name'])->delete();
        }
    }
}
