<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class FillPermissionsForTask extends Migration
{
    /**
     * @var  \Illuminate\Config\Repository|mixed
     */
    protected $guardName;
    /**
     * @var  array
     */
    protected $permissions;
    /**
     * @var  array
     */
    protected $roles;

    /**
     * FillPermissionsForTask constructor.
     */
    public function __construct()
    {
        $this->guardName = config('admin-auth.defaults.guard');

        $permissions = collect([
            'crud.task.view',
            'crud.task.delete',
            'crud.task.create',
            'crud.task.edit',
            'crud.task.columns.id.read',
            'crud.task.columns.warehouse_id.read',
            'crud.task.columns.warehouse_id.write',
            'crud.task.columns.delivered_at.read',
            'crud.task.columns.delivered_at.write',
            'crud.task.columns.carrier_id.read',
            'crud.task.columns.carrier_id.write',
            'crud.task.columns.track.read',
            'crud.task.columns.track.write',
            'crud.task.columns.carrier_tracking_status.read',
            'crud.task.columns.employee_status_id.read',
            'crud.task.columns.employee_status_id.write',
            'crud.task.columns.comment.read',
            'crud.task.columns.comment.write',
            'crud.task.columns.creator_id.read',
            'crud.task.columns.creator_id.write',
            'crud.task.columns.manager_id.read',
            'crud.task.columns.manager_id.write',
            'crud.task.columns.employee_id.read',
            'crud.task.columns.employee_id.write',
            'crud.task.columns.label.read',
            'crud.task.columns.label.write',
            'crud.task.columns.barcode.read',
            'crud.task.columns.barcode.write',
            'crud.task.columns.receipt.read',
            'crud.task.columns.receipt.write',
            'crud.task.columns.invoice.read',
            'crud.task.columns.invoice.write',
            'crud.task.columns.content.read',
            'crud.task.columns.content.write',
        ]);

        //Add New permissions
        $this->permissions = $permissions->map(function ($permission) {
            return [
                'name' => $permission,
                'guard_name' => $this->guardName,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ];
        })->toArray();

        //Role should already exists
        $this->roles = [
            [
                'name' => 'Administrator',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
            [
                'name' => 'HR Manager Staff',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
            [
                'name' => 'Support Staff',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
            [
                'name' => 'Staffer',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
            [
                'name' => 'Employee Staff',
                'guard_name' => $this->guardName,
                'permissions' => [
                    'crud.task.view',
                    'crud.task.columns.id.read',
                    'crud.task.columns.delivered_at.read',
                    'crud.task.columns.carrier_id.read',
                    'crud.task.columns.track.read',
                    'crud.task.columns.carrier_tracking_status.read',
                    'crud.task.columns.employee_status_id.read',
                    'crud.task.columns.employee_status_id.write',
                    'crud.task.columns.comment.read',
                    'crud.task.columns.creator_id.read',
                    'crud.task.columns.manager_id.read',
                    'crud.task.columns.employee_id.read',
                    'crud.task.columns.label.read',
                    'crud.task.columns.barcode.read',
                    'crud.task.columns.receipt.read',
                    'crud.task.columns.receipt.write',
                    'crud.task.columns.invoice.read',
                    'crud.task.columns.invoice.write',
                    'crud.task.columns.content.read',
                    'crud.task.columns.content.write',
                ],
            ]
        ];
    }

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        DB::transaction(function () {
            foreach ($this->permissions as $permission) {
                $permissionItem = DB::table('permissions')->where([
                    'name' => $permission['name'],
                    'guard_name' => $permission['guard_name']
                ])->first();
                if (is_null($permissionItem)) {
                    DB::table('permissions')->insert($permission);
                }
            }

            foreach ($this->roles as $role) {
                $permissions = $role['permissions'];
                unset($role['permissions']);

                $roleItem = DB::table('roles')->where([
                    'name' => $role['name'],
                    'guard_name' => $role['guard_name']
                ])->first();
                if (!is_null($roleItem)) {
                    $roleId = $roleItem->id;

                    $permissionItems = DB::table('permissions')->whereIn('name', $permissions)->where('guard_name',
                        $role['guard_name'])->get();
                    foreach ($permissionItems as $permissionItem) {
                        $roleHasPermissionData = [
                            'permission_id' => $permissionItem->id,
                            'role_id' => $roleId
                        ];
                        $roleHasPermissionItem = DB::table('role_has_permissions')->where($roleHasPermissionData)->first();
                        if (is_null($roleHasPermissionItem)) {
                            DB::table('role_has_permissions')->insert($roleHasPermissionData);
                        }
                    }
                }
            }
        });
        app()['cache']->forget(config('permission.cache.key'));
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        DB::transaction(function () {
            foreach ($this->permissions as $permission) {
                $permissionItem = DB::table('permissions')->where([
                    'name' => $permission['name'],
                    'guard_name' => $permission['guard_name']
                ])->first();
                if (!is_null($permissionItem)) {
                    DB::table('permissions')->where('id', $permissionItem->id)->delete();
                    DB::table('model_has_permissions')->where('permission_id', $permissionItem->id)->delete();
                }
            }
        });
        app()['cache']->forget(config('permission.cache.key'));
    }
}
