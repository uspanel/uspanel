<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionBusinessProcessEventRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_business_process_event_role', function (Blueprint $table) {
            $table->unsignedInteger('action_business_process_event_id');
            $table->unsignedInteger('role_id');
            $table->foreign('action_business_process_event_id', 'abperole_id_foreign')
            ->references('id')->on('action_business_process_events')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_business_process_event_role');
    }
}
