<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Action;

class FillActionsTable extends Migration
{

    protected $data = [
        [
            'name' => 'App\Libs\Actions\NotifyByEmail',
            'title' => 'Notify by Email',
        ],
        [
            'name' => 'App\Libs\Actions\NotifyByNotificationCenter',
            'title' => 'Notify by Notification Center',
        ],
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Action::insert($this->data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->data as $key => $value) {
            Action::where('name', $value['name'])->delete();
        }
    }
}
