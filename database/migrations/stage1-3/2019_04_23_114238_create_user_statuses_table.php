<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('title');
            $table->timestamps();
        });
        \App\UserStatus::create([
            'name' => 'new',
            'title' => 'New'
        ]);
        \App\UserStatus::create([
            'name' => 'wait_docs',
            'title' => 'Wait docs'
        ]);
        \App\UserStatus::create([
            'name' => 'applicant',
            'title' => 'Applicant'
        ]);
        \App\UserStatus::create([
            'name' => 'approved',
            'title' => 'Approved'
        ]);
        \App\UserStatus::create([
            'name' => 'rejected',
            'title' => 'Rejected'
        ]);
        \App\UserStatus::create([
            'name' => 'test',
            'title' => 'Test'
        ]);
        \App\UserStatus::create([
            'name' => 'first_pack',
            'title' => 'Recieve first package'
        ]);
        \App\UserStatus::create([
            'name' => 'bad',
            'title' => 'Bad'
        ]);
        \App\UserStatus::create([
            'name' => 'slow',
            'title' => 'Slow'
        ]);
        \App\UserStatus::create([
            'name' => 'medium',
            'title' => 'Medium'
        ]);
        \App\UserStatus::create([
            'name' => 'fast',
            'title' => 'Fast'
        ]);
        \App\UserStatus::create([
            'name' => 'super_fast',
            'title' => 'Super fast'
        ]);
        \App\UserStatus::create([
            'name' => 'voicemail',
            'title' => 'Voice mail'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_statuses');
    }
}
