<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterviewFormAndFields extends Migration
{
    protected $types = [
        'text',
        'textarea',
        'select',
        'date',
        'checkbox',
        'file',
        'number',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->form = \CodersStudio\FormCreator\Form::create([
            'name' => 'interview'
        ]);
        foreach ($this->types as $value) {
            \CodersStudio\FormCreator\FieldType::create([
                'name' => $value
            ]);
        }
        $fields = [
            [
                'name' => 'adult',
                'title' => 'Are you over 21 years old?',
                'rules' => 'required',
                'type' => 'select',
                'options' => [
                    '1' => 'Yes',
                    '0' => 'No',
                ],
            ],
            [
                'name' => 'status',
                'title' => 'Are you USA Citizen or Resident?',
                'rules' => 'required',
                'type' => 'select',
                'options' => [
                    'citizen' => 'Citizen',
                    'resident' => 'Resident',
                ],
            ],
            [
                'name' => 'eligible_to_work',
                'title' => 'Are you eligible to work in the USA?',
                'rules' => 'required',
                'type' => 'select',
                'options' => [
                    '1' => 'Yes',
                    '0' => 'No',
                ],
            ],
            [
                'name' => 'education',
                'title' => 'What is your education?',
                'rules' => 'required|string',
                'type' => 'text',
                'searchable' => true,
                'sortable' => true,
                'visible' => true,
                'exportable' => true
            ],
            [
                'name' => 'employed',
                'title' => 'Are you currently employed?',
                'rules' => 'required',
                'type' => 'select',
                'options' => [
                    '1' => 'Yes',
                    '0' => 'No',
                ],
            ],
            [
                'name' => 'business',
                'title' => 'Are You a Business Owner or an Entrepreneur?',
                'rules' => 'required',
                'type' => 'select',
                'options' => [
                    'business_owner' => 'Business Owner',
                    'entrepreneur' => 'Entrepreneur',
                    '0' => 'No'
                ],
            ],
            [
                'name' => 'reasons',
                'title' => 'Why do you want this job?',
                'rules' => 'required',
                'searchable' => true,
                'sortable' => true,
                'visible' => true,
                'type' => 'textarea',
                'exportable' => true
            ],
            [
                'name' => 'working_conditions',
                'title' => 'Are you aware this is strictly an online and work from home job?',
                'rules' => 'required',
                'type' => 'select',
                'options' => [
                    '1' => 'Yes',
                    '0' => 'No',
                ],
            ],
            [
                'name' => 'hours',
                'title' => 'How many hours will you be able to devote to the company daily?',
                'rules' => 'required',
                'type' => 'number',
                'searchable' => true,
                'sortable' => true,
                'visible' => true,
                'exportable' => true
            ],
            [
                'name' => 'description',
                'title' => 'How would you describe yourself?',
                'rules' => 'required',
                'searchable' => true,
                'sortable' => true,
                'visible' => true,
                'type' => 'textarea',
                'exportable' => true
            ],
            [
                'name' => 'example',
                'title' => 'Give a good example of the way you set goals and get them.',
                'rules' => 'required',
                'searchable' => true,
                'sortable' => true,
                'visible' => true,
                'type' => 'textarea',
                'exportable' => true
            ],
            [
                'name' => 'last_project',
                'title' => 'What was the last project you headed up, and what was its outcome?',
                'rules' => 'required',
                'searchable' => true,
                'sortable' => true,
                'visible' => true,
                'type' => 'textarea',
                'exportable' => true
            ],
            
            [
                'name' => 'training',
                'title' => 'How much training do you think you need to become a productive employee? (We offer three training days once you employed)',
                'rules' => 'required|string',
                'type' => 'text',
                'searchable' => true,
                'sortable' => true,
                'visible' => true,
                'exportable' => true
            ],
            [
                'name' => 'skills',
                'title' => 'Give an example that best describes your organizational skills?',
                'rules' => 'required',
                'searchable' => true,
                'sortable' => true,
                'visible' => true,
                'type' => 'textarea',
                'exportable' => true
            ],
            [
                'name' => 'pressure',
                'title' => 'How do you handle work under pressure?',
                'rules' => 'required',
                'searchable' => true,
                'sortable' => true,
                'visible' => true,
                'type' => 'textarea',
                'exportable' => true
            ],
            [
                'name' => 'goals',
                'title' => 'What kind of goals would you have in mind if you got this job?',
                'rules' => 'required',
                'searchable' => true,
                'sortable' => true,
                'visible' => true,
                'type' => 'textarea',
                'exportable' => true
            ],
            [
                'name' => 'why_the_company_should_hire',
                'title' => 'Give three genuine and specific reasons why the company should hire you?',
                'rules' => 'required',
                'searchable' => true,
                'sortable' => true,
                'visible' => true,
                'type' => 'textarea',
                'exportable' => true
            ],
            
        ];
        $this->setFields($fields);
    }

    protected function setFields($fields) {
        foreach ($fields as $value) {
            $type = \CodersStudio\FormCreator\FieldType::where('name', $value['type'])->firstOrFail();
            $this->form->fields()->create([
                'field_type_id' => $type->id,
                'name' => $value['name'],
                'title' => $value['title'],
                'rules' => $value['rules'],
                'options' => json_encode($value['options'] ?? []),
                'sortable' => $value['sortable'] ?? false,
                'searchable' => $value['searchable'] ?? false,
                'visible' => $value['visible'] ?? false,
                'enabled' => (isset($value['enabled']) && $value['enabled'] == false) ? false : true,
                'exportable' => $value['exportable'] ?? false
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interview_form_and_fields');
    }
}
