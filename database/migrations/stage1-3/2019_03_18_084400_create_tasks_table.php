<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('delivered_at')->nullable();
            $table->unsignedInteger('warehouse_id')->nullable();
            $table->string('carrier_id')->nullable();
            $table->string('track')->nullable();
            $table->unsignedInteger('employee_status_id')->nullable();
            $table->string('comment')->nullable();
            $table->unsignedInteger('creator_id')->nullable();
            $table->unsignedInteger('manager_id')->nullable();
            $table->unsignedInteger('employee_id')->nullable();
            $table->unsignedInteger('delivery_status_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('warehouse_id')->references('id')->on('warehouses');
            $table->foreign('creator_id')->references('id')->on('users');
            $table->foreign('manager_id')->references('id')->on('users');
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('delivery_status_id')->references('id')->on('delivery_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
