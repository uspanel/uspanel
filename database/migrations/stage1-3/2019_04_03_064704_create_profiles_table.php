<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('city')->nullable();
            $table->unsignedInteger('state_id')->nullable();
            $table->string('address')->nullable();
            $table->string('referrer')->nullable();
            $table->string('zip')->nullable();
            $table->boolean('higher_education')->default(false);
            $table->boolean('authorized_to_work')->default(false);
            $table->boolean('adult')->default(false);
            $table->date('date_of_birth')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
