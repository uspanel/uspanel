<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryMailingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_mailings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mailing_id')->unsigned();
            $table->integer('email_mailing_id')->unsigned();
            $table->integer('mailing_status_id')->unsigned();
            $table->integer('mailing_sending_id')->unsigned();
            $table->foreign('mailing_id')->references('id')->on('mailings');
            $table->foreign('mailing_sending_id')->references('id')->on('mailing_sendings');
            $table->foreign('email_mailing_id')->references('id')->on('email_mailings');
            $table->foreign('mailing_status_id')->references('id')->on('mailing_statuses');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('history_mailings');
        Schema::enableForeignKeyConstraints();
    }
}
