<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class FillPermissionsForPackage extends Migration
{
    /**
     * @var  \Illuminate\Config\Repository|mixed
     */
    protected $guardName;
    /**
     * @var  array
     */
    protected $permissions;
    /**
     * @var  array
     */
    protected $roles;

    /**
     * FillPermissionsForPackage constructor.
     */
    public function __construct()
    {
        $this->guardName = config('admin-auth.defaults.guard');

        $permissions = collect([
            'crud.package.view',
            'crud.package.create',
            'crud.package.edit', 
            'crud.package.delete',
            'crud.package.columns.id.read',
            'crud.package.columns.delivered_at.read',
            'crud.package.columns.delivered_at.write',
            'crud.package.columns.task_id.read',
            'crud.package.columns.task_id.write',
            'crud.package.columns.track.read',
            'crud.package.columns.track.write',
            'crud.package.columns.carrier_id.read',
            'crud.package.columns.carrier_id.write',
            'crud.package.columns.weight.read',
            'crud.package.columns.weight.write',
            'crud.package.columns.name.read',
            'crud.package.columns.name.write',
            'crud.package.columns.courier_id.read',
            'crud.package.columns.courier_id.write',
            'crud.package.columns.employee_id.read',
            'crud.package.columns.employee_id.write',
            'crud.package.columns.carrier_id.read',
            'crud.package.columns.carrier_id.write',
            'crud.package.columns.carrier_tracking_status.read',
            'crud.package.columns.creator_id.read',
            'crud.package.columns.creator_id.write',
            'crud.package.columns.manager_id.read',
            'crud.package.columns.manager_id.write',
            'crud.package.columns.status_id.read',
            'crud.package.columns.status_id.write',
            'crud.package.columns.comment.read',
            'crud.package.columns.comment.write',
            'crud.package.columns.invoice.read',
            'crud.package.columns.invoice.write',
            'crud.package.columns.description.read',
            'crud.package.columns.description.items.read',
            'crud.package.columns.description.write',
        ]);
        //Add New permissions
        $this->permissions = $permissions->map(function ($permission) {
            return [
                'name' => $permission,
                'guard_name' => $this->guardName,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ];
        })->toArray();

        //Role should already exists
        $this->roles = [
            [
                'name' => 'Administrator',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
            [
                'name' => 'Staffer',
                'guard_name' => $this->guardName,
                'permissions' => [
                    'crud.package.view',
                    'crud.package.create',
                    'crud.package.edit',
                    'crud.package.delete',
                    'crud.package.columns.id.read',
                    'crud.package.columns.delivered_at.read',
                    'crud.package.columns.delivered_at.write',
                    'crud.package.columns.task_id.read',
                    'crud.package.columns.task_id.write',
                    'crud.package.columns.track.read',
                    'crud.package.columns.track.write',
                    'crud.package.columns.carrier_id.read',
                    'crud.package.columns.carrier_id.write',
                    'crud.package.columns.weight.read',
                    'crud.package.columns.weight.write',
                    'crud.package.columns.name.read',
                    'crud.package.columns.name.write',
                    'crud.package.columns.courier_id.read',
                    'crud.package.columns.courier_id.write',
                    'crud.package.columns.employee_id.read',
                    'crud.package.columns.employee_id.write',
                    'crud.package.columns.carrier_id.read',
                    'crud.package.columns.carrier_id.write',
                    'crud.package.columns.carrier_tracking_status.read',
                    'crud.package.columns.creator_id.read',
                    'crud.package.columns.creator_id.write',
                    'crud.package.columns.manager_id.read',
                    'crud.package.columns.manager_id.write',
                    'crud.package.columns.status_id.read',
                    'crud.package.columns.status_id.write',
                    'crud.package.columns.comment.read',
                    'crud.package.columns.comment.write',
                    'crud.package.columns.invoice.read',
                    'crud.package.columns.description.read',
                    'crud.package.columns.description.items.read',
                    'crud.package.columns.description.write',
                    ]
            ],
            [
                'name' => 'Support Staff',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
            [
                'name' => 'HR Manager Staff',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
            [
                'name' => 'Employee Staff',
                'guard_name' => $this->guardName,
                'permissions' => [
                    'crud.package.view',
                    'crud.package.columns.id.read',
                    'crud.package.columns.delivered_at.read',
                    'crud.package.columns.task_id.read',
                    'crud.package.columns.track.read',
                    'crud.package.columns.carrier_id.read',
                    'crud.package.columns.weight.read',
                    'crud.package.columns.name.read',
                    'crud.package.columns.courier_id.read',
                    'crud.package.columns.employee_id.read',
                    'crud.package.columns.creator_id.read',
                    'crud.package.columns.manager_id.read',
                    'crud.package.columns.status_id.read',
                    'crud.package.columns.comment.read',
                    'crud.package.columns.invoice.read',
                    'crud.package.columns.invoice.write',
                    'crud.package.columns.description.read',
                    'crud.package.columns.description.items.read',
                    'crud.package.columns.carrier_tracking_status.read',
                ],
            ],
        ];
    }

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        DB::transaction(function () {
            foreach ($this->permissions as $permission) {
                $permissionItem = DB::table('permissions')->where([
                    'name' => $permission['name'],
                    'guard_name' => $permission['guard_name']
                ])->first();
                if (is_null($permissionItem)) {
                    DB::table('permissions')->insert($permission);
                }
            }

            foreach ($this->roles as $role) {
                $permissions = $role['permissions'];
                unset($role['permissions']);

                $roleItem = DB::table('roles')->where([
                    'name' => $role['name'],
                    'guard_name' => $role['guard_name']
                ])->first();
                if (!is_null($roleItem)) {
                    $roleId = $roleItem->id;

                    $permissionItems = DB::table('permissions')->whereIn('name', $permissions)->where('guard_name',
                        $role['guard_name'])->get();
                    foreach ($permissionItems as $permissionItem) {
                        $roleHasPermissionData = [
                            'permission_id' => $permissionItem->id,
                            'role_id' => $roleId
                        ];
                        $roleHasPermissionItem = DB::table('role_has_permissions')->where($roleHasPermissionData)->first();
                        if (is_null($roleHasPermissionItem)) {
                            DB::table('role_has_permissions')->insert($roleHasPermissionData);
                        }
                    }
                }
            }
        });
        app()['cache']->forget(config('permission.cache.key'));
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        DB::transaction(function () {
            foreach ($this->permissions as $permission) {
                $permissionItem = DB::table('permissions')->where([
                    'name' => $permission['name'],
                    'guard_name' => $permission['guard_name']
                ])->first();
                if (!is_null($permissionItem)) {
                    DB::table('permissions')->where('id', $permissionItem->id)->delete();
                    DB::table('model_has_permissions')->where('permission_id', $permissionItem->id)->delete();
                }
            }
        });
        app()['cache']->forget(config('permission.cache.key'));
    }
}
