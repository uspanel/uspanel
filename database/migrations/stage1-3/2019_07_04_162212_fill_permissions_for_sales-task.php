<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class FillPermissionsForSalesTask extends Migration
{
    /**
     * @var  \Illuminate\Config\Repository|mixed
     */
    protected $guardName;
    /**
     * @var  array
     */
    protected $permissions;
    /**
     * @var  array
     */
    protected $roles;

    /**
     * FillPermissionsForSalesTask constructor.
     */
    public function __construct()
    {
        $this->guardName = config('admin-auth.defaults.guard');

        $permissions = collect([
            'crud.sales-task.view',
            'crud.sales-task.delete',
            'crud.sales-task.create',
            'crud.sales-task.edit',
            'crud.sales-task.show',
            'crud.sales-task.columns.id.read',
            'crud.sales-task.columns.warehouse_id.read',
            'crud.sales-task.columns.warehouse_id.write',
            'crud.sales-task.columns.delivered_at.read',
            'crud.sales-task.columns.delivered_at.write',
            'crud.sales-task.columns.carrier_id.read',
            'crud.sales-task.columns.carrier_id.write',
            'crud.sales-task.columns.track.read',
            'crud.sales-task.columns.track.write',
            'crud.sales-task.columns.carrier_tracking_status.read',
            'crud.sales-task.columns.employee_status_id.read',
            'crud.sales-task.columns.employee_status_id.write',
            'crud.sales-task.columns.comment.read',
            'crud.sales-task.columns.comment.write',
            'crud.sales-task.columns.creator_id.read',
            'crud.sales-task.columns.creator_id.write',
            'crud.sales-task.columns.manager_id.read',
            'crud.sales-task.columns.manager_id.write',
            'crud.sales-task.columns.employee_id.read',
            'crud.sales-task.columns.employee_id.write',
            'crud.sales-task.columns.label.read',
            'crud.sales-task.columns.label.write',
            'crud.sales-task.columns.barcode.read',
            'crud.sales-task.columns.barcode.write',
            'crud.sales-task.columns.receipt.read',
            'crud.sales-task.columns.receipt.write',
            'crud.sales-task.columns.invoice.read',
            'crud.sales-task.columns.invoice.write',
            'crud.sales-task.columns.content.read',
            'crud.sales-task.columns.content.write',
        ]);

        //Add New permissions
        $this->permissions = $permissions->map(function ($permission) {
            return [
                'name' => $permission,
                'guard_name' => $this->guardName,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ];
        })->toArray();

        //Role should already exists
        $this->roles = [
            [
                'name' => 'Administrator',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
            [
                'name' => 'HR Manager Staff',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
            [
                'name' => 'HR Manager Invest',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
            [
                'name' => 'Support Staff',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
            [
                'name' => 'Support Invest',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
            [
                'name' => 'Staffer',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
            [
                'name' => 'Employee Staff',
                'guard_name' => $this->guardName,
                'permissions' => [
                    'crud.sales-task.view',
                    'crud.sales-task.columns.id.read',
                    'crud.sales-task.columns.delivered_at.read',
                    'crud.sales-task.columns.carrier_id.read',
                    'crud.sales-task.columns.track.read',
                    'crud.sales-task.columns.carrier_tracking_status.read',
                    'crud.sales-task.columns.employee_status_id.read',
                    'crud.sales-task.columns.employee_status_id.write',
                    'crud.sales-task.columns.comment.read',
                    'crud.sales-task.columns.creator_id.read',
                    'crud.sales-task.columns.manager_id.read',
                    'crud.sales-task.columns.employee_id.read',
                    'crud.sales-task.columns.label.read',
                    'crud.sales-task.columns.barcode.read',
                    'crud.sales-task.columns.receipt.read',
                    'crud.sales-task.columns.receipt.write',
                    'crud.sales-task.columns.invoice.read',
                    'crud.sales-task.columns.invoice.write',
                    'crud.sales-task.columns.content.read',
                    'crud.sales-task.columns.content.write',
                ],
            ],
            [
                'name' => 'Employee Invest',
                'guard_name' => $this->guardName,
                'permissions' => [
                    'crud.sales-task.view',
                ],
            ],
        ];
    }

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        DB::transaction(function () {
            foreach ($this->permissions as $permission) {
                $permissionItem = DB::table('permissions')->where([
                    'name' => $permission['name'],
                    'guard_name' => $permission['guard_name']
                ])->first();
                if (is_null($permissionItem)) {
                    DB::table('permissions')->insert($permission);
                }
            }

            foreach ($this->roles as $role) {
                $permissions = $role['permissions'];
                unset($role['permissions']);

                $roleItem = DB::table('roles')->where([
                    'name' => $role['name'],
                    'guard_name' => $role['guard_name']
                ])->first();
                if (!is_null($roleItem)) {
                    $roleId = $roleItem->id;

                    $permissionItems = DB::table('permissions')->whereIn('name', $permissions)->where('guard_name',
                        $role['guard_name'])->get();
                    foreach ($permissionItems as $permissionItem) {
                        $roleHasPermissionData = [
                            'permission_id' => $permissionItem->id,
                            'role_id' => $roleId
                        ];
                        $roleHasPermissionItem = DB::table('role_has_permissions')->where($roleHasPermissionData)->first();
                        if (is_null($roleHasPermissionItem)) {
                            DB::table('role_has_permissions')->insert($roleHasPermissionData);
                        }
                    }
                }
            }
        });
        app()['cache']->forget(config('permission.cache.key'));
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        DB::transaction(function () {
            foreach ($this->permissions as $permission) {
                $permissionItem = DB::table('permissions')->where([
                    'name' => $permission['name'],
                    'guard_name' => $permission['guard_name']
                ])->first();
                if (!is_null($permissionItem)) {
                    DB::table('permissions')->where('id', $permissionItem->id)->delete();
                    DB::table('model_has_permissions')->where('permission_id', $permissionItem->id)->delete();
                }
            }
        });
        app()['cache']->forget(config('permission.cache.key'));
    }
}
