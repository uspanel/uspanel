<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mailings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('mail_template_id')->unsigned();
            $table->integer('created_by')->unsigned()->nullable();
            $table->datetime('scheduled_at')->nullable();     
            $table->timestamps();
            $table->foreign('mail_template_id')->references('id')->on('mail_templates');
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('mailings');
        Schema::enableForeignKeyConstraints();
    }
}
