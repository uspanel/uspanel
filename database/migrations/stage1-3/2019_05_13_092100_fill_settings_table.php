<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillSettingsTable extends Migration
{
    protected $data = [
        [
            'name' => 'super_fast',
            'value' => '5',
        ],
        [
            'name' => 'fast',
            'value' => '24',
        ],
        [
            'name' => 'medium',
            'value' => '48',
        ],
        [
            'name' => 'did_not_upload_documents',
            'value' => '48',
        ],
        [
            'name' => 'did_not_complete_task',
            'value' => '48',
        ],
        [
            'name' => 'did_not_upload_task_documents',
            'value' => '48',
        ],
        [
            'name' => 'not_responding_to_message',
            'value' => '1',
        ],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Setting::insert($this->data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->data as $key => $value) {
            Setting::where('name', $value['name'])->delete();
        }
    }
}
