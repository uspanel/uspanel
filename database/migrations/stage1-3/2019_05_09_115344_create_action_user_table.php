<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActionUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_business_process_event_user', function (Blueprint $table) {
            $table->unsignedInteger('action_business_process_event_id');
            $table->unsignedInteger('user_id');
            $table->foreign('action_business_process_event_id', 'abpe_id_foreign')
            ->references('id')->on('action_business_process_events')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_business_process_event_user');
    }
}
