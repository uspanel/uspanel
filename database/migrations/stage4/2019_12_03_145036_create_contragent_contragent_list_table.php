<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContragentContragentListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contragent_contragent_list', function (Blueprint $table) {
            $table->unsignedInteger('contragent_id');
            $table->unsignedInteger('contragent_list_id');
            $table->timestamps();

            $table->foreign('contragent_id')->references('id')->on('contragents');
            $table->foreign('contragent_list_id')->references('id')->on('contragent_lists');

            $table->primary(['contragent_id', 'contragent_list_id'],
                'contragent_id_contragent_list_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contragent_contragent_list', function (Blueprint $table) {
            $table->dropForeign(['contragent_id']);
            $table->dropForeign(['contragent_list_id']);
            $table->dropPrimary();
        });
        Schema::dropIfExists('contragent_contragent_list');
    }
}
