<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContragentInvestTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contragent_invest_task', function (Blueprint $table) {
            $table->unsignedInteger('contragent_id');
            $table->unsignedInteger('invest_task_id');
            $table->timestamps();

            $table->foreign('contragent_id')->references('id')->on('contragents');
            $table->foreign('invest_task_id')->references('id')->on('invest_tasks');

            $table->primary(['contragent_id', 'invest_task_id'],
                'contragent_id_invest_task_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contragent_invest_task', function (Blueprint $table) {
            $table->dropForeign(['contragent_id']);
            $table->dropForeign(['invest_task_id']);
            $table->dropPrimary();
        });
        Schema::dropIfExists('contragent_invest_task');
    }
}
