<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class FillPermissionsForInvestTaskTemplate extends Migration
{
    /**
     * @var  \Illuminate\Config\Repository|mixed
     */
    protected $guardName;
    /**
     * @var  array
     */
    protected $permissions;
    /**
     * @var  array
     */
    protected $roles;

    /**
     * FillPermissionsForInvestTaskTemplate constructor.
     */
    public function __construct()
    {
        $this->guardName = config('admin-auth.defaults.guard');

        $permissions = collect([
            'crud.invest-task-template',
            'crud.invest-task-template.index',
            'crud.invest-task-template.create',
            'crud.invest-task-template.view',
            'crud.invest-task-template.show',
            'crud.invest-task-template.edit',
            'crud.invest-task-template.delete',
            'crud.invest-task-template.columns.name.read',
            'crud.invest-task-template.columns.name.write',
            'crud.invest-task-template.columns.theme.read',
            'crud.invest-task-template.columns.theme.write',
            'crud.invest-task-template.columns.text.read',
            'crud.invest-task-template.columns.text.write',
            'crud.invest-task-template.columns.deadline.read',
            'crud.invest-task-template.columns.deadline.write',
            'crud.invest-task-template.columns.time.read',
            'crud.invest-task-template.columns.time.write',
            'crud.invest-task-template.columns.employee_id.read',
            'crud.invest-task-template.columns.employee_id.write',
            'crud.invest-task-template.columns.manager_id.read',
            'crud.invest-task-template.columns.manager_id.write',
            'crud.invest-task-template.columns.creator_id.read',
            'crud.invest-task-template.columns.creator_id.write',
            'crud.invest-task-template.columns.invest_task_status_id.read',
            'crud.invest-task-template.columns.invest_task_status_id.write',
            'crud.invest-task-template.columns.deleted_at.read',
            'crud.invest-task-template.columns.deleted_at.write',
            'crud.invest-task-template.columns.created_at.read',
            'crud.invest-task-template.columns.created_at.write',
            'crud.invest-task-template.columns.updated_at.read',
            'crud.invest-task-template.columns.updated_at.write',

            'crud.invest-task-template.columns.investors.read',
            'crud.invest-task-template.columns.investors.write',
            'crud.invest-task-template.columns.investor_lists.read',
            'crud.invest-task-template.columns.investor_lists.write',
            'crud.invest-task-template.columns.contragents.read',
            'crud.invest-task-template.columns.contragents.write',
            'crud.invest-task-template.columns.contragent_lists.read',
            'crud.invest-task-template.columns.contragent_lists.write',
            'crud.invest-task-template.columns.files.read',
            'crud.invest-task-template.columns.files.write',
            'crud.invest-task-template.columns.mail_template_id.read',
            'crud.invest-task-template.columns.mail_template_id.write',

        ]);

        //Add New permissions
        $this->permissions = $permissions->map(function ($permission) {
            return [
                'name' => $permission,
                'guard_name' => $this->guardName,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ];
        })->toArray();

        //Role should already exists
        $this->roles = [
            [
                'name' => 'Administrator',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
            [
                'name' => 'HR Manager Invest',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
        ];
    }

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        DB::transaction(function () {
            foreach ($this->permissions as $permission) {
                $permissionItem = DB::table('permissions')->where([
                    'name' => $permission['name'],
                    'guard_name' => $permission['guard_name']
                ])->first();
                if (is_null($permissionItem)) {
                    DB::table('permissions')->insert($permission);
                }
            }

            foreach ($this->roles as $role) {
                $permissions = $role['permissions'];
                unset($role['permissions']);

                $roleItem = DB::table('roles')->where([
                    'name' => $role['name'],
                    'guard_name' => $role['guard_name']
                ])->first();
                if (!is_null($roleItem)) {
                    $roleId = $roleItem->id;

                    $permissionItems = DB::table('permissions')->whereIn('name', $permissions)->where('guard_name',
                        $role['guard_name'])->get();
                    foreach ($permissionItems as $permissionItem) {
                        $roleHasPermissionData = [
                            'permission_id' => $permissionItem->id,
                            'role_id' => $roleId
                        ];
                        $roleHasPermissionItem = DB::table('role_has_permissions')->where($roleHasPermissionData)->first();
                        if (is_null($roleHasPermissionItem)) {
                            DB::table('role_has_permissions')->insert($roleHasPermissionData);
                        }
                    }
                }
            }
        });
        app()['cache']->forget(config('permission.cache.key'));
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        DB::transaction(function () {
            foreach ($this->permissions as $permission) {
                $permissionItem = DB::table('permissions')->where([
                    'name' => $permission['name'],
                    'guard_name' => $permission['guard_name']
                ])->first();
                if (!is_null($permissionItem)) {
                    DB::table('permissions')->where('id', $permissionItem->id)->delete();
                    DB::table('model_has_permissions')->where('permission_id', $permissionItem->id)->delete();
                }
            }
        });
        app()['cache']->forget(config('permission.cache.key'));
    }
}
