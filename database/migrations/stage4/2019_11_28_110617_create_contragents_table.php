<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContragentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contragents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->decimal('investing_amount',10,2)->comment('max amount to invest');
            $table->unsignedInteger('user_id');
            $table->string('country')->nullable();
            $table->unsignedSmallInteger('roi')
                ->nullable()
                ->comment('return of investment in percents');

            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contragents');
    }
}
