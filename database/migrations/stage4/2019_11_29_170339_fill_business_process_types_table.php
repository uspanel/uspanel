<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\BusinessProcessType;
use App\Event;

class FillBusinessProcessTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        BusinessProcessType::insert(
            [
                [
                    'name' => 'common',
                    'title' => 'Common',
                ],
                [
                    'name' => 'support',
                    'title' => 'Support',
                ],
                [
                    'name' => 'invest',
                    'title' => 'Invest',
                ],
            ]
        );

        $events = [
//            [
//                'name' => 'NewMessageInChat',
//                'title' => 'New message in chat',
//            ],

            // User
            [
                'name' => 'UserStatusApproved',
                'title' => 'User status approved',
            ],
            [
                'name' => 'UserRegistered',
                'title' => 'User registered',
            ],
            [
                'name' => 'UserPassedInterview',
                'title' => 'User passed interview',
            ],
            [
                'name' => 'UserUploadedDocument',
                'title' => 'User uploaded document',
            ],
            [
                'name' => 'UserStartWork',
                'title' => 'User start work',
            ],
            [
                'name' => 'UserGotFirstTask',
                'title' => 'User got first task',
            ],
            [
                'name' => 'UserHasNotChangedStatusOnFirstTask',
                'title' => 'User has not changed status on first task',
            ],
            [
                'name' => 'UserGotTask',
                'title' => 'User got task',
            ],
            [
                'name' => 'UserDoneFirstTask',
                'title' => 'User done first task',
            ],
        ];

        $common = BusinessProcessType::where('name', 'common')->firstOrFail();
        foreach ($events as $event) {
            $event = Event::firstOrCreate($event);
            $common->events()->attach($event->id);
        }


        $events = [
            // Package
            [
                'name' => 'PackageStatusChanged',
                'title' => 'Package status changed',
            ],
            [
                'name' => 'PackageCreated',
                'title' => 'Package created',
            ],
            [
                'name' => 'PackageEdited',
                'title' => 'Package edited',
            ],

            // Task
            [
                'name' => 'TaskCreated',
                'title' => 'Task created',
            ],
            [
                'name' => 'TaskStatusChanged',
                'title' => 'Task status changed',
            ],
            [
                'name' => 'TaskExpired',
                'title' => 'Task expired',
            ],
        ];
        $support = BusinessProcessType::where('name', 'support')->firstOrFail();
        foreach ($events as $event) {
            $event = Event::firstOrCreate($event);
            $support->events()->attach($event->id);
        }


        $events = [
            // User
            [
                'name' => 'UserGotNewInvestor',
                'title' => 'User got new investor',
            ],
            [
                'name' => 'UserGotNewContragent',
                'title' => 'User got new contragent',
            ],
            [
                'name' => 'UserGotAnswerFromInvestor',
                'title' => 'User got answer from investor',
            ],

            // Task
            [
                'name' => 'TaskCreated',
                'title' => 'Task created',
            ],
            [
                'name' => 'TaskStatusChanged',
                'title' => 'Task status changed',
            ],
            [
                'name' => 'TaskExpired',
                'title' => 'Task expired',
            ],
        ];

        $invest = BusinessProcessType::where('name', 'invest')->firstOrFail();
        foreach ($events as $event) {
            $event = Event::firstOrCreate($event);
            $invest->events()->attach($event->id);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
