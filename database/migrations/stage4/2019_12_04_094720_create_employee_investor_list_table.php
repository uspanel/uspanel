<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeInvestorListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_investor_list', function (Blueprint $table) {
            $table->unsignedInteger('employee_id');
            $table->unsignedInteger('investor_list_id');

            $table
                ->foreign('employee_id')
                ->references('id')
                ->on('employees')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table
                ->foreign('investor_list_id')
                ->references('id')
                ->on('investor_lists')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_investor_list', function (Blueprint $table) {
            $table->dropForeign(['employee_id']);
            $table->dropForeign(['investor_list_id']);
        });
        Schema::dropIfExists('employee_investor_list');
    }
}
