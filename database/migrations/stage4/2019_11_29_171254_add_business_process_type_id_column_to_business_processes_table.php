<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBusinessProcessTypeIdColumnToBusinessProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_processes', function (Blueprint $table) {
            $table
                ->unsignedInteger('business_process_type_id')
                ->nullable();

            $table
                ->foreign('business_process_type_id')
                ->references('id')
                ->on('business_process_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business_processes', function (Blueprint $table) {
            $table->dropForeign(['business_process_type_id']);
            $table->dropColumn('business_process_type_id');
        });
    }
}
