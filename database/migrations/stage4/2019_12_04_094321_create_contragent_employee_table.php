<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContragentEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contragent_employee', function (Blueprint $table) {
            $table->unsignedInteger('employee_id');
            $table->unsignedInteger('contragent_id');

            $table
                ->foreign('employee_id')
                ->references('id')
                ->on('employees')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table
                ->foreign('contragent_id')
                ->references('id')
                ->on('contragents')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contragent_employee', function (Blueprint $table) {
            $table->dropForeign(['employee_id']);
            $table->dropForeign(['contragent_id']);
        });
        Schema::dropIfExists('contragent_employee');
    }
}
