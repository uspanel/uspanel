<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SupportCanApproveEmployees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $permissionItem = DB::table('permissions')->where('name', 'crud.user.columns.employee_user_status_id.write')->where('guard_name',
            config('admin-auth.defaults.guard'))->first();
        $roleHasPermissionData = [
            'permission_id' => $permissionItem->id,
            'role_id' => \App\Role::findByName('Support Staff')->id
        ];
        $roleHasPermissionItem = DB::table('role_has_permissions')->where($roleHasPermissionData)->first();
        if (is_null($roleHasPermissionItem)) {
            DB::table('role_has_permissions')->insert($roleHasPermissionData);
        }
        $roleHasPermissionData = [
            'permission_id' => $permissionItem->id,
            'role_id' => \App\Role::findByName('Support Invest')->id
        ];
        $roleHasPermissionItem = DB::table('role_has_permissions')->where($roleHasPermissionData)->first();
        if (is_null($roleHasPermissionItem)) {
            DB::table('role_has_permissions')->insert($roleHasPermissionData);
        }
        app()['cache']->forget(config('permission.cache.key'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
