<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestTaskInvestorListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invest_task_investor_list', function (Blueprint $table) {
            $table->unsignedInteger('invest_task_id');
            $table->unsignedInteger('investor_list_id');
            $table->timestamps();

            $table->foreign('invest_task_id')->references('id')->on('invest_tasks');
            $table->foreign('investor_list_id')->references('id')->on('investor_lists');

            $table->primary(['invest_task_id', 'investor_list_id'],
                'invest_task_id_investor_list_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invest_task_investor_list', function (Blueprint $table) {
            $table->dropForeign(['invest_task_id']);
            $table->dropForeign(['investor_list_id']);
            $table->dropPrimary();
        });
        Schema::dropIfExists('invest_task_investor_list');
    }
}
