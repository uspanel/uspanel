<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invest_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('theme')->nullable();
            $table->string('text')->nullable();
            $table->string('description')->nullable();
            $table->timestamp('deadline')->nullable();
            $table->unsignedInteger('employee_id')->nullable();
            $table->unsignedInteger('manager_id')->nullable();
            $table->unsignedInteger('creator_id')->nullable();
            $table->unsignedInteger('mail_template_id')->nullable();
            $table->unsignedInteger('invest_task_status_id')
                ->default(\App\InvestTaskStatus::where('name', 'new')->firstOrFail()->id);

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('users');
            $table->foreign('manager_id')->references('id')->on('users');
            $table->foreign('creator_id')->references('id')->on('users');
            $table->foreign('invest_task_status_id')->references('id')->on('invest_task_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invest_tasks', function (Blueprint $table) {
//            $table->dropForeign(['employee_id']);//TODO Ругался: "check that column/key exists"
//            $table->dropForeign(['manager_id']);
            $table->dropForeign(['creator_id']);
            $table->dropForeign(['invest_task_status_id']);
        });
        Schema::dropIfExists('invest_tasks');
    }
}
