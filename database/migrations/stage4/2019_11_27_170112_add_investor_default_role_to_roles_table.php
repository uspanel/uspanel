<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvestorDefaultRoleToRolesTable extends Migration
{
    /**
     * @var \Illuminate\Config\Repository|mixed
     */
    protected $guardName;
    /**
     * @var mixed
     */
    protected $userClassName;
    /**
     * @var
     */
    protected $userTable;

    /**
     * @var array
     */
    protected $permissions;
    /**
     * @var array
     */
    protected $roles;

    /**
     * FillDefaultAdminUserAndPermissions constructor.
     */
    public function __construct()
    {
        $this->guardName = config('admin-auth.defaults.guard');
        $providerName = config('auth.guards.' . $this->guardName . '.provider');
        $provider = config('auth.providers.' . $providerName);
        if ($provider['driver'] === 'eloquent') {
            $this->userClassName = $provider['model'];
        }
        $this->userTable = (new $this->userClassName)->getTable();

        $defaultPermissions = collect([
            'crud.chatroom.view',
            'crud.chatroom.create',
            'crud.chatroom.show',
            'crud.chatroom.edit',
            'crud.chatroom.delete'

        ]);

        //Add new permissions
        $this->permissions = $defaultPermissions->map(function ($permission) {
            return [
                'name' => $permission,
                'guard_name' => $this->guardName,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ];
        })->toArray();

        //Add new roles
        $this->roles = [
            [
                'name' => 'Investor',
                'guard_name' => $this->guardName,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'permissions' => $defaultPermissions,
            ]
        ];

    }


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            if (is_null($this->userClassName)) {
                throw new Exception('Admin user model not defined');
            }
            DB::transaction(function () {
                foreach ($this->permissions as $permission) {
                    $permissionItem = DB::table('permissions')->where([
                        'name' => $permission['name'],
                        'guard_name' => $permission['guard_name']
                    ])->first();
                    if (is_null($permissionItem)) {
                        DB::table('permissions')->insert($permission);
                    }
                }

                foreach ($this->roles as $role) {
                    $permissions = $role['permissions'];
                    unset($role['permissions']);

                    $roleItem = DB::table('roles')->where([
                        'name' => $role['name'],
                        'guard_name' => $role['guard_name']
                    ])->first();
                    if (is_null($roleItem)) {
                        $roleId = DB::table('roles')->insertGetId($role);
                    } else {
                        $roleId = $roleItem->id;
                    }

                    $permissionItems = DB::table('permissions')->whereIn('name', $permissions)->where('guard_name',
                        $role['guard_name'])->get();
                    foreach ($permissionItems as $permissionItem) {
                        $roleHasPermissionData = [
                            'permission_id' => $permissionItem->id,
                            'role_id' => $roleId
                        ];
                        $roleHasPermissionItem = DB::table('role_has_permissions')->where($roleHasPermissionData)->first();
                        if (is_null($roleHasPermissionItem)) {
                            DB::table('role_has_permissions')->insert($roleHasPermissionData);
                        }
                    }
                }
            });
            app()['cache']->forget(config('permission.cache.key'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            if (is_null($this->userClassName)) {
                throw new Exception('Admin user model not defined');
            }
            DB::transaction(function () {
                foreach ($this->roles as $role) {
                    $roleItem = DB::table('roles')->where([
                        'name' => $role['name'],
                        'guard_name' => $role['guard_name']
                    ])->first();
                    if (!is_null($roleItem)) {
                        DB::table('roles')->where('id', $roleItem->id)->delete();
                        DB::table('model_has_roles')->where('role_id', $roleItem->id)->delete();
                    }
                }

                foreach ($this->permissions as $permission) {
                    $permissionItem = DB::table('permissions')->where([
                        'name' => $permission['name'],
                        'guard_name' => $permission['guard_name']
                    ])->first();
                    if (!is_null($permissionItem)) {
                        DB::table('permissions')->where('id', $permissionItem->id)->delete();
                        DB::table('model_has_permissions')->where('permission_id', $permissionItem->id)->delete();
                    }
                }
            });
            app()['cache']->forget(config('permission.cache.key'));
        });
    }
}
