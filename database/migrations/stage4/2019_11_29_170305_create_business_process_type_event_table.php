<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessProcessTypeEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_process_type_event', function (Blueprint $table) {
            $table->unsignedInteger('business_process_type_id');
            $table->unsignedInteger('event_id');

            $table
                ->foreign('business_process_type_id')
                ->references('id')
                ->on('business_process_types')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table
                ->foreign('event_id')
                ->references('id')
                ->on('events')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business_process_type_event', function (Blueprint $table) {
            $table->dropForeign(['business_process_type_id']);
            $table->dropForeign(['event_id']);
        });
        Schema::dropIfExists('business_process_type_event');
    }
}
