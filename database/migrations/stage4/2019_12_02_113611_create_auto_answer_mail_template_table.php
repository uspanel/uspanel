<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoAnswerMailTemplateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_answer_mail_template', function (Blueprint $table) {
            $table->unsignedInteger('auto_answer_id');
            $table->unsignedInteger('mail_template_id');
            $table->unsignedInteger('message_count')->default(1);
            $table->unsignedInteger('delay')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_answer_mail_template');
    }
}
