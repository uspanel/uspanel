<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestTaskInvestorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invest_task_investor', function (Blueprint $table) {
            $table->unsignedInteger('invest_task_id');
            $table->unsignedInteger('investor_id');
            $table->timestamps();

            $table->foreign('invest_task_id')->references('id')->on('invest_tasks');
            $table->foreign('investor_id')->references('id')->on('users');

            $table->primary(['invest_task_id', 'investor_id'],
                'invest_task_id_investor_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invest_task_investor', function (Blueprint $table) {
            $table->dropForeign(['invest_task_id']);
            $table->dropForeign(['investor_id']);
            $table->dropPrimary();
        });
        Schema::dropIfExists('invest_task_investor');
    }
}
