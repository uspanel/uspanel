<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeVoicentStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_voicent_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employee_id');
            $table->string('status')->nullable();
            $table->dateTime('start_time')->nullable();
            $table->string('duration')->nullable();
            $table->string('notes')->nullable();
            $table->string('agent_recording')->nullable();
            $table->decimal('credit')->default(0);
            $table->decimal('transfer_credit')->default(0);
            $table->decimal('tts_credit')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_voicent_stats');
    }
}
