<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FillCarriersTableWithAdditionalCarriers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \App\Carrier::firstOrCreate(
            [
                'name' => 'Other',
                'title' => 'Other'
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $carrier = \App\Carrier::where('name', 'Other')->first();
        $carrier->delete();
    }
}
