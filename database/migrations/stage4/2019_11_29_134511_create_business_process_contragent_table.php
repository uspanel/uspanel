<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessProcessContragentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_process_contragent', function (Blueprint $table) {
            $table->unsignedInteger('contragent_id');
            $table->unsignedInteger('business_process_id');

            $table
                ->foreign('contragent_id')
                ->references('id')
                ->on('contragents')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table
                ->foreign('business_process_id')
                ->references('id')
                ->on('business_processes')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_process_contragent');
    }
}
