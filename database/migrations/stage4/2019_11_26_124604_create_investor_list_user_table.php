<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestorListUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investor_list_user', function (Blueprint $table) {
            $table->unsignedInteger('investor_list_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->foreign('investor_list_id')->references('id')->on('investor_lists');
            $table->foreign('user_id')->references('id')->on('users');

            $table->primary(['investor_list_id', 'user_id'],
                'investor_list_id_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('investor_list_user', function (Blueprint $table) {
            $table->dropForeign(['investor_list_id']);
            $table->dropForeign(['user_id']);
            $table->dropPrimary();
        });
        Schema::dropIfExists('investor_list_user');
    }
}
