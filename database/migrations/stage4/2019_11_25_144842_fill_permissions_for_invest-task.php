<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class FillPermissionsForInvestTask extends Migration
{
    /**
     * @var  \Illuminate\Config\Repository|mixed
     */
    protected $guardName;
    /**
     * @var  array
     */
    protected $permissions;
    /**
     * @var  array
     */
    protected $roles;

    /**
     * FillPermissionsForInvestTask constructor.
     */
    public function __construct()
    {
        $this->guardName = config('admin-auth.defaults.guard');

        $permissions = collect([
            'crud.invest-task',
            'crud.invest-task.index',
            'crud.invest-task.create',
            'crud.invest-task.view',
            'crud.invest-task.show',
            'crud.invest-task.edit',
            'crud.invest-task.delete',
            'crud.invest-task.columns.name.read',
            'crud.invest-task.columns.name.write',
            'crud.invest-task.columns.theme.read',
            'crud.invest-task.columns.theme.write',
            'crud.invest-task.columns.text.read',
            'crud.invest-task.columns.text.write',
            'crud.invest-task.columns.description.read',
            'crud.invest-task.columns.description.write',
            'crud.invest-task.columns.deadline.read',
            'crud.invest-task.columns.deadline.write',
            'crud.invest-task.columns.time.read',
            'crud.invest-task.columns.time.write',
            'crud.invest-task.columns.employee_id.read',
            'crud.invest-task.columns.employee_id.write',
            'crud.invest-task.columns.manager_id.read',
            'crud.invest-task.columns.manager_id.write',
            'crud.invest-task.columns.creator_id.read',
            'crud.invest-task.columns.creator_id.write',
            'crud.invest-task.columns.invest_task_status_id.read',
            'crud.invest-task.columns.invest_task_status_id.write',
            'crud.invest-task.columns.deleted_at.read',
            'crud.invest-task.columns.deleted_at.write',
            'crud.invest-task.columns.created_at.read',
            'crud.invest-task.columns.created_at.write',
            'crud.invest-task.columns.updated_at.read',
            'crud.invest-task.columns.updated_at.write',

            'crud.invest-task.columns.investors.read',
            'crud.invest-task.columns.investors.write',
            'crud.invest-task.columns.investor_lists.read',
            'crud.invest-task.columns.investor_lists.write',
            'crud.invest-task.columns.contragents.read',
            'crud.invest-task.columns.contragents.write',
            'crud.invest-task.columns.contragent_lists.read',
            'crud.invest-task.columns.contragent_lists.write',
            'crud.invest-task.columns.files.read',
            'crud.invest-task.columns.files.write',
            'crud.invest-task.columns.mail_template_id.read',
            'crud.invest-task.columns.mail_template_id.write',

        ]);

        //Add New permissions
        $this->permissions = $permissions->map(function ($permission) {
            return [
                'name' => $permission,
                'guard_name' => $this->guardName,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ];
        })->toArray();

        //Role should already exists
        $this->roles = [
            [
                'name' => 'Administrator',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
            [
                'name' => 'HR Manager Invest',
                'guard_name' => $this->guardName,
                'permissions' => $permissions,
            ],
            [
                'name' => 'Employee Invest',
                'guard_name' => $this->guardName,
                'permissions' => collect([
                    'crud.invest-task',
                    'crud.invest-task.index',
                    'crud.invest-task.view',
                    'crud.invest-task.show',
//                    'crud.invest-task.edit',
                    'crud.invest-task.columns.invest_task_status_id.read',
                    'crud.invest-task.columns.invest_task_status_id.write',
                    'crud.invest-task.columns.name.read',
                    'crud.invest-task.columns.theme.read',
                    'crud.invest-task.columns.text.read',
                    'crud.invest-task.columns.description.read',
                    'crud.invest-task.columns.deadline.read',
                    'crud.invest-task.columns.time.read',
                    'crud.invest-task.columns.employee_id.read',
                    'crud.invest-task.columns.manager_id.read',
                    'crud.invest-task.columns.creator_id.read',
                    'crud.invest-task.columns.deleted_at.read',
                    'crud.invest-task.columns.created_at.read',
                    'crud.invest-task.columns.updated_at.read',

                    'crud.invest-task.columns.investors.read',
                    'crud.invest-task.columns.investor_lists.read',
                    'crud.invest-task.columns.contragents.read',
                    'crud.invest-task.columns.contragent_lists.read',
                    'crud.invest-task.columns.mail_template_id.read',
                    'crud.invest-task.columns.files.read',
//                    'crud.invest-task.columns.files.write',
                ]),
            ],
        ];
    }

    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        DB::transaction(function () {
            foreach ($this->permissions as $permission) {
                $permissionItem = DB::table('permissions')->where([
                    'name' => $permission['name'],
                    'guard_name' => $permission['guard_name']
                ])->first();
                if (is_null($permissionItem)) {
                    DB::table('permissions')->insert($permission);
                }
            }

            foreach ($this->roles as $role) {
                $permissions = $role['permissions'];
                unset($role['permissions']);

                $roleItem = DB::table('roles')->where([
                    'name' => $role['name'],
                    'guard_name' => $role['guard_name']
                ])->first();
                if (!is_null($roleItem)) {
                    $roleId = $roleItem->id;

                    $permissionItems = DB::table('permissions')->whereIn('name', $permissions)->where('guard_name',
                        $role['guard_name'])->get();
                    foreach ($permissionItems as $permissionItem) {
                        $roleHasPermissionData = [
                            'permission_id' => $permissionItem->id,
                            'role_id' => $roleId
                        ];
                        $roleHasPermissionItem = DB::table('role_has_permissions')->where($roleHasPermissionData)->first();
                        if (is_null($roleHasPermissionItem)) {
                            DB::table('role_has_permissions')->insert($roleHasPermissionData);
                        }
                    }
                }
            }
        });
        app()['cache']->forget(config('permission.cache.key'));
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        DB::transaction(function () {
            foreach ($this->permissions as $permission) {
                $permissionItem = DB::table('permissions')->where([
                    'name' => $permission['name'],
                    'guard_name' => $permission['guard_name']
                ])->first();
                if (!is_null($permissionItem)) {
                    DB::table('permissions')->where('id', $permissionItem->id)->delete();
                    DB::table('model_has_permissions')->where('permission_id', $permissionItem->id)->delete();
                }
            }
        });
        app()['cache']->forget(config('permission.cache.key'));
    }
}
