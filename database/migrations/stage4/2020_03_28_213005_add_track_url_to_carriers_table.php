<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTrackUrlToCarriersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carriers', function (Blueprint $table) {
            $table->string('track_url')->nullable();
        });
        $urls = [
            'Fedex' => 'https://www.fedex.com/apps/fedextrack/?action=track&trackingnumber={tracknum}&cntry_code=us&locale=en_US',
            'Ups' => 'https://www.ups.com/track?loc=en_US&tracknum={tracknum}&requester=WT/',
            'Usps' => 'https://tools.usps.com/go/TrackConfirmAction?tLabels={tracknum}',
            'Dhl' => 'http://webtrack.dhlglobalmail.com/?trackingnumber={tracknum}',
        ];
        foreach (\App\Carrier::all() as $carrier) {
            if (!empty($urls[$carrier->name])) {
                $carrier->update(['track_url' => $urls[$carrier->name]]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carriers', function (Blueprint $table) {
            $table->dropColumn('track_url');
        });
    }
}
