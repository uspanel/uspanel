<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EmployeeStaffSeed::class);
        $this->call(MailTemplatesSeeder::class);
        $this->call(InvestSeed::class);
        // $this->call(MailingStructureSeeder::class);
        // $this->call(ItemTableSeeder::class);
        // $this->call(PackagesTableSeeder::class);
        //
        //$this->call(MailSettingsSeeder::class);
        //$this->call(BusinessSeeder::class);
        //$this->call(TaskTableSeeder::class);
    }
}
