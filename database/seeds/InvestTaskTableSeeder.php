<?php

use Illuminate\Database\Seeder;
use App\Task;
use App\Employee;

class InvestTaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\InvestTask::class, 1)->create();
    }
}
