<?php

use Illuminate\Database\Seeder;

class MailingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mailTemplate = \App\MailTemplate::first();
        if (!$mailTemplate) {
            $this->call([MailTemplatesSeeder::class]);
            $mailTemplate = \App\MailTemplate::first();
        }
        $mailing = \App\Mailing::create([
            'name' => 'Test mailing',
            'mail_template_id' => $mailTemplate->id,
            'scheduled_at' => \Carbon\Carbon::now()
        ]);

        $emailGroup = \App\EmailGroup::create([
            'name' => 'Test group',
        ]);

        $mailing->emailGroups()->sync([$emailGroup->id]);

        \App\EmailMailing::create([
            'name' => 'Test Recipient',
            'email' => 'test@email.com',
            'email_group_id' => $emailGroup->id
        ]);
        \App\EmailMailing::create([
            'name' => 'Test2 Recipient',
            'email' => 'test2@email.com',
            'email_group_id' => $emailGroup->id
        ]);
    }
}
