<?php

use Illuminate\Database\Seeder;

class BusinessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bp = \App\BusinessProcess::create([
            'title' => 'test BP',
            'user_id' => 1,
            'active' => 1,
        ]);

        $events = \App\Event::all();
        foreach ($events as $event) {
            $this->doStuff($event->id, $bp);
        }

    }

    protected function doStuff($id, $bp)
    {
        $bpe = \App\BusinessProcessEvent::create([
            'business_process_id' => $bp->id,
            'event_id' => $id,
        ]);

        $abpeNc = \App\ActionBusinessProcessEvent::create([
            'action_id' => 2,
            'business_process_event_id' => $bpe->id,
            'position' => 1
        ]);

        $abpeNc->users()->sync([1]);

        $abpeMail = \App\ActionBusinessProcessEvent::create([
            'action_id' => 1,
            'business_process_event_id' => $bpe->id,
            'position' => 2
        ]);
        $abpeMail->users()->sync([1]);
        $abpeMail->mailTemplates()->sync([1]);
    }
}
