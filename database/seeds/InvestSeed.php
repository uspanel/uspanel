<?php

use Illuminate\Database\Seeder;
use CodersStudio\FormCreator\Form;

class InvestSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $hrManager = factory(\App\User::class)->create([
            'email' => "HrManagerInvest@coders.studio"
        ]);
        $hrManager->assignRole('HR Manager Invest');
        $suppport = factory(\App\User::class)->create([
            'email' => "SupportInvest@coders.studio"
        ]);
        $suppport->assignRole('Support Invest');
        $contragent = factory(\App\Contragent::class)->create([
            'user_id' => $hrManager->id
        ]);

        foreach(range(1, 10) as $value) {
            $hrManager = factory(\App\User::class)->create([
                'email' => "HrManagerInvest$value@coders.studio"
            ]);
            $hrManager->assignRole('HR Manager Invest');
            $suppport = factory(\App\User::class)->create([
                'email' => "SupportInvest$value@coders.studio"
            ]);
            $suppport->assignRole('Support Invest');
            $investor = factory(\App\User::class)->create();
            $investor->assignRole('Investor');
            $investor->investorProfile()->create([]);
            $investor->mailsettings()->create([]);
            $contragent = factory(\App\Contragent::class)->create([
                'user_id' => $hrManager->id
            ]);
        }
        $project = factory(App\Project::class)->create();
        $project->forms()->sync(Form::first()->id);
        $project->users()->attach([
            $suppport->id => [
                'responsible_percent' => 100
            ],
            $hrManager->id => [
                'responsible_percent' => 100
            ]
        ]);
        $employee = factory(\App\Employee::class)->create([
            'project_id' => $project->id,
            'hr_id' => $hrManager->id,
            'support_id' => $suppport->id,
        ]);
        $employee->user->update(['email' => "EmployeeInvest@coders.studio"]);
        $employee->user->assignRole('Employee Invest');
        $investor = factory(\App\User::class)->create();
        $investor->assignRole('Investor');
        $investor->investorProfile()->create([]);
        $investor->mailsettings()->create([]);
        $investor->projects()->attach($project->id);
    }
}
