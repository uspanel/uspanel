<?php

use Illuminate\Database\Seeder;
use App\User;

class MailingStructureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$admin = User::role('Administrator')->first();
        $emailGroup = factory(\App\EmailGroup::class)->create([
        	'created_by' => $admin->id,
        ]);
        $mailTemplate = factory(\App\MailTemplate::class)->create();
        $mailing = factory(\App\Mailing::class)->create([
        	'mail_template_id' => $mailTemplate->id,
        	'created_by' => $admin->id,
        ]);
        for ($a=0; $a < 4; $a++) {
        	$mailingSending = factory(\App\MailingSending::class)->create();
	        for ($i=0; $i < rand(10, 20); $i++) {
	        	$emailMailing = factory(\App\EmailMailing::class)->create([
		        	'email_group_id' => $emailGroup->id,
		        	'created_by' => $admin->id,
		        ]);
		        factory(\App\HistoryMailing::class)->create([
		        	'mailing_id' => $mailing->id,
		        	'email_mailing_id' => $emailMailing->id,
		        	'mailing_sending_id' => $mailingSending->id,
		        ]);
	        }
        }
        
    }
}
