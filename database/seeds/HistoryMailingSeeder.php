<?php

use Illuminate\Database\Seeder;

class HistoryMailingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $emailMailing = \App\EmailMailing::first();
        if (!$emailMailing) {
            $this->call([MailingsSeeder::class]);
            $emailMailing = \App\EmailMailing::first();
        }

        $sending = \App\MailingSending::create([
               'scheduled_at' => \Carbon\Carbon::now()
        ]);

        \App\HistoryMailing::create([
            'mailing_id' => \App\Mailing::first()->id,
            'email_mailing_id' => $emailMailing->id,
            'mailing_sending_id' => $sending->id,
            'mailing_status_id' => \App\MailingStatus::where('name','=','new')->first()->id
        ]);
        \App\HistoryMailing::create([
            'mailing_id' => \App\Mailing::first()->id,
            'email_mailing_id' => $emailMailing->id+1,
            'mailing_sending_id' => $sending->id,
            'mailing_status_id' => \App\MailingStatus::where('name','=','new')->first()->id
        ]);
    }
}
