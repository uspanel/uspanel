<?php

use Illuminate\Database\Seeder;
use App\HelpVideo;

class HelpVideoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(HelpVideo::class, 2)->create();
    }
}
