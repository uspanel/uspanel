<?php

use App\Package;
use Illuminate\Database\Seeder;

class PackagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(range(1, 10) as $value) {
            $pack = factory(Package::class)->create([
                'employee_id' => App\User::Role('Employee Staff')->first()
            ]);
            $pack->items()->sync([1 => ['qty' => 10, 'price' => 100]]);
        }
    }
}
