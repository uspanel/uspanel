<?php

use Illuminate\Database\Seeder;

class MailSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CodersStudio\ImapMailClient\Mailsetting::create([
            'user_id' => 1,
            'smtp_host' => 'smtp.yandex.ru',
            'smtp_port' => 465,
            'smtp_encryption' => 'ssl',
            'imap_host' => 'imap.yandex.ru',
            'imap_port' => 993,
            'imap_encryption' => 'ssl',
            'imap_sent_folder' => 'Sent',
            'imap_inbox_folder' => 'INBOX',
            'login' => 'devcodersstudio@yandex.ru',
            'password' => 'kenny228',
            'default' => 1,
            'validate_cert' => 1
        ]);
    }
}
