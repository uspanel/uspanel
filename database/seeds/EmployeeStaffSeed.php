<?php

use Illuminate\Database\Seeder;
use CodersStudio\FormCreator\Form;

class EmployeeStaffSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $callSupport = factory(\App\User::class)->create([
            'email' => "CallSupport1@coders.studio"
        ]);
        $callSupport->assignRole('Call support');
        $hrManager = factory(\App\User::class)->create([
            'email' => "HrManagerStaff@coders.studio"
        ]);
        $hrManager->assignRole('HR Manager Staff');
        $suppport = factory(\App\User::class)->create([
            'email' => "SupportStaff@coders.studio"
        ]);
        $suppport->assignRole('Support Staff');
        $staffer = factory(\App\User::class)->create([
            'email' => "Staffer@coders.studio"
        ]);
        $staffer->assignRole('Staffer');
        $project = factory(App\Project::class)->create();
        $project->forms()->sync(Form::first()->id);
        $project->users()->attach([
            $suppport->id => [
                'responsible_percent' => 100
            ],
            $hrManager->id => [
                'responsible_percent' => 100
            ]
        ]);
        $employee = factory(\App\Employee::class)->create([
            'project_id' => $project->id,
            'hr_id' => $hrManager->id,
            'support_id' => $suppport->id,
        ]);
        $employee->user->assignRole('Employee Staff');
        $warehouse = factory(\App\Warehouse::class)->create();
        factory(App\Item::class, 40)->create();
        //return;
        $package = factory(\App\Package::class)->create([
            'employee_id' => $employee->id,
        ]);
        $task = factory(\App\Task::class)->create([
            'employee_id' => $employee->id,
            'warehouse_id' => $warehouse->id,
        ]);
        \Illuminate\Support\Facades\Schema::disableForeignKeyConstraints();
        $salesTask = factory(\App\SalesTask::class)->create([
            'employee_id' => $employee->id,
            'warehouse_id' => $warehouse->id,
        ]);
        \Illuminate\Support\Facades\Schema::enableForeignKeyConstraints();

        factory(App\Item::class, 2)->create()->each(function ($item) use ($package, $task, $salesTask) {
            $package->items()->attach([
                $item->id => [
                    'qty' => 100,
                    'price' => 100
                ],
            ]);
            $task->items()->attach([
                $item->id => [
                    'qty' => 100,
                ],
            ]);
            $salesTask->items()->attach([
                $item->id => [
                    'qty' => 2,
                ],
            ]);
        });
    }
}
