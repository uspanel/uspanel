<?php

use Illuminate\Database\Seeder;
use App\Task;
use App\Employee;

class TaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class, 5)
        ->create()
        ->each(factory(\App\Employee::class, 2)
            ->create()
            ->each(function ($user) {
                foreach(range(1, 2) as $value) 
                    $user
                    ->tasks()
                    ->save(factory(Task::class)
                        ->make()
                    );
            })
        );
    }
}
