<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class MailTemplatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mail_templates')->insert([
            'title' => 'test template',
            'name' => 'TEST_TEMPLATE',
            'subject' => 'test subject',
            'body' => '<h1>Hello, [first_name] [last_name]! Your name is [user_name] and email is [email]</h1><p>Static value is [restore_pass_link]</p>' .
//                '<p>[first_name]</p>' .
//                '<p>[last_name]</p>' .
//                '<p>[state]</p>' .
//                '<p>[address]</p>' .
//                '<p>[home_phone]</p>' .
//                '<p>[work_phone]</p>' .
//                '<p>[position]</p>' .
//                '<p>[docs]</p>' .
//                '<p>[zip]</p>' .
//                '<p>[sign_name]</p>' .
//                '<p>[signed_date]</p>' .
//                '<p>[signed]</p>' .
//                '<p>[email]</p>' .
//                '<p>[user_name]</p>' .
//                '<p>[manager]</p>' .
//                '<p>[investors_list]</p>' .
//                '<p>[last_activity]</p>' .
//                '<p>[application]</p>' .
//                '<p>[inv_prj]</p>' .
//                '<p>[project_name]</p>' .
//                '<p>[project_link]</p>' .
//                '<p>[webmail_user]</p>' .
//                '<p>[control_panel_link]</p>' .
//                '<p>[sign_link]</p>' .
//                '<p>[webmail_link]</p>' .
//                '<p>[restore_pass_link]</p>' .
//                '<p>[company_name]</p>' .
//                '<p>[site_link]</p>' .
//                '<p>[sup_email]</p>' .
//                '<p>[sup_name]</p>' .
//                '<p>[company_type]</p>' .
//                '<p>[company_address]</p>' .
//                '<p>[company_city]</p>' .
//                '<p>[company_country]</p>' .
//                '<p>[company_state]</p>' .
//                '<p>[company_zip]</p>' .
//                '<p>[company_phone1]</p>' .
//                '<p>[company_phone2]</p>' .
//                '<p>[company_fax]</p>' .
        ''
        ]);
    }
}
