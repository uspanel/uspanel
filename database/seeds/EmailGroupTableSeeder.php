<?php

use Illuminate\Database\Seeder;
use App\EmailGroup;
use App\User;


class EmailGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::Role('Administrator')->firstOrFail();
        $user->each(
            function ($user) {
                foreach(range(1, 10) as $value) {
                    $user->emailGroups()->save(factory(EmailGroup::class)->make());
                }
            }
        );
    }
}
