<?php

use App\Role;
use App\Permission;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = Permission::firstOrFail();
        foreach(range(1, 10) as $value) {
            $permission->roles()->save(factory(Role::class)->make());
        }
    }
}
